<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.1.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename>../../Assets/Atlases/ui.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>5</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/ui.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>10,12,4,4</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>10,12,4,4</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ActiveUpRect.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>47,4,10,129</rect>
                <key>scale9Paddings</key>
                <rect>26,44,52,88</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Bitmap.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,43,84,86</rect>
                <key>scale9Paddings</key>
                <rect>42,43,84,86</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Body.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>16,16,11,3</rect>
                <key>scale9Paddings</key>
                <rect>11,9,22,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>51,50,24,3</rect>
                <key>scale9Paddings</key>
                <rect>31,26,62,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Btn.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>16,16,9,9</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>89,0,61,180</rect>
                <key>scale9Paddings</key>
                <rect>150,170,300,340</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Card.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>217,553,18,40</rect>
                <key>scale9Paddings</key>
                <rect>113,303,226,606</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Chests.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>214,205,428,410</rect>
                <key>scale9Paddings</key>
                <rect>214,205,428,410</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Coal.png</key>
            <key type="filename">Count.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,40,40</rect>
                <key>scale9Paddings</key>
                <rect>20,20,40,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Combined Shape.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,12,112,24</rect>
                <key>scale9Paddings</key>
                <rect>56,12,112,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">CountStor.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,20,64,40</rect>
                <key>scale9Paddings</key>
                <rect>32,20,64,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Daily/Bonus/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,28,46,56</rect>
                <key>scale9Paddings</key>
                <rect>23,28,46,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">DownloadFullBtn.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>308,104,616,208</rect>
                <key>scale9Paddings</key>
                <rect>308,104,616,208</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Drilla.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>281,226,562,452</rect>
                <key>scale9Paddings</key>
                <rect>281,226,562,452</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EndChest.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>191,134,382,268</rect>
                <key>scale9Paddings</key>
                <rect>191,134,382,268</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GarageUpButtonInactive.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>16,1,16,8</rect>
                <key>scale9Paddings</key>
                <rect>12,9,24,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Group 3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,105,210,210</rect>
                <key>scale9Paddings</key>
                <rect>105,105,210,210</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Group 4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>200,184,128,240</rect>
                <key>scale9Paddings</key>
                <rect>77,77,154,154</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Icon/Chest/2.2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,55,118,110</rect>
                <key>scale9Paddings</key>
                <rect>59,55,118,110</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Icon/Cur/Gold.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,48,48</rect>
                <key>scale9Paddings</key>
                <rect>24,24,48,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Icon/Parts/Boer.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,37,100,74</rect>
                <key>scale9Paddings</key>
                <rect>50,37,100,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Icon/Parts/Energy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,37,68,74</rect>
                <key>scale9Paddings</key>
                <rect>34,37,68,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Icon/Parts/Ladle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,39,68,78</rect>
                <key>scale9Paddings</key>
                <rect>34,39,68,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Icon/Roulette/1 Copy 4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>126,83,252,166</rect>
                <key>scale9Paddings</key>
                <rect>126,83,252,166</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Img.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,74,128,148</rect>
                <key>scale9Paddings</key>
                <rect>64,74,128,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">InactiveBorder.png</key>
            <key type="filename">activeBorder.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>16,16,28,28</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">KickBeamBtn.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>150,170,300,340</rect>
                <key>scale9Paddings</key>
                <rect>150,170,300,340</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">LadleUpBack.png</key>
            <key type="filename">Path.png</key>
            <key type="filename">SpeedLvlBack.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,18,60,36</rect>
                <key>scale9Paddings</key>
                <rect>30,18,60,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">LeftAngleColletion.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,122,226,244</rect>
                <key>scale9Paddings</key>
                <rect>113,122,226,244</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Light.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>180,179,1,1</rect>
                <key>scale9Paddings</key>
                <rect>90,90,180,180</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Mineral.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Navigations/Back.png</key>
            <key type="filename">Navigations/Close.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,54,108,108</rect>
                <key>scale9Paddings</key>
                <rect>54,54,108,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rc.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>4,2,8,4</rect>
                <key>scale9Paddings</key>
                <rect>4,2,8,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rectangle 5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>61,0,2,120</rect>
                <key>scale9Paddings</key>
                <rect>31,30,62,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rectangle Copy 2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,76,132,152</rect>
                <key>scale9Paddings</key>
                <rect>66,76,132,152</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rectangle Copy 4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>44,38,2,2</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rectangle Copy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>28,24,10,1</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">RectangleCollectionOK.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,21,52,42</rect>
                <key>scale9Paddings</key>
                <rect>26,21,52,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">RectangleOnBtn.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,6,1,1</rect>
                <key>scale9Paddings</key>
                <rect>52,66,104,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rude.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,22,52,44</rect>
                <key>scale9Paddings</key>
                <rect>26,22,52,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shape.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,28</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Small Rect.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>0,8,12,7</rect>
                <key>scale9Paddings</key>
                <rect>3,6,6,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">SmallDrill.png</key>
            <key type="filename">SmallDrillInactive.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,26,54,52</rect>
                <key>scale9Paddings</key>
                <rect>27,26,54,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">SmallStore.png</key>
            <key type="filename">SmallStoreInactive.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,25,54,50</rect>
                <key>scale9Paddings</key>
                <rect>27,25,54,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Speed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>8,8,19,11</rect>
                <key>scale9Paddings</key>
                <rect>55,37,110,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Star 2 Copy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,20,26,19</rect>
                <key>scale9Paddings</key>
                <rect>21,21,42,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Stars.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.501603,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>590,1050,68,32</rect>
                <key>scale9Paddings</key>
                <rect>312,533,624,1066</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Stone.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,23,52,46</rect>
                <key>scale9Paddings</key>
                <rect>26,23,52,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Stor.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,42,88,84</rect>
                <key>scale9Paddings</key>
                <rect>44,42,88,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">StorageSeparator.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,6,6</rect>
                <key>scale9Paddings</key>
                <rect>3,3,6,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Topaz.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,19,32,38</rect>
                <key>scale9Paddings</key>
                <rect>16,19,32,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">WhiteBackBeam.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>161,0,86,35</rect>
                <key>scale9Paddings</key>
                <rect>102,48,204,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">concrete1.png</key>
            <key type="filename">concrete2.png</key>
            <key type="filename">concrete3.png</key>
            <key type="filename">concrete4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>320,134,640,268</rect>
                <key>scale9Paddings</key>
                <rect>320,134,640,268</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">emer.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,20,36,40</rect>
                <key>scale9Paddings</key>
                <rect>18,20,36,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">g2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>145,24,39,262</rect>
                <key>scale9Paddings</key>
                <rect>81,74,162,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">l.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,12,34,24</rect>
                <key>scale9Paddings</key>
                <rect>17,12,34,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">purpRect.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.577338</point_f>
                <key>scale9Enabled</key>
                <true/>
                <key>scale9Borders</key>
                <rect>0,282,1280,77</rect>
                <key>scale9Paddings</key>
                <rect>320,362,640,724</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>large-max-texture-size</string>
            <string>unity-importer-4.6.1</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
