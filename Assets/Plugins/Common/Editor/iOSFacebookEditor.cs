﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEditor.iOS.Xcode;
using System.IO;

namespace BlackBears.Facebook
{

    public class iOSFacebookEditor
    {

        private static FacebookSettings settings;

        [PostProcessBuild(103)]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget != BuildTarget.iOS) return;
            PrepareProject(buildPath);
        }

        private static void PrepareProject(string buildPath)
        {
            CheckAsset();
            if (string.IsNullOrEmpty(settings?.FacebookAppId)) return;

            string plistPath = buildPath + "/Info.plist";
            var plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            PlistElementDict rootDict = plist.root;
            rootDict.SetString("FacebookAppID", settings.FacebookAppId);
            rootDict.SetString("FacebookDisplayName", PlayerSettings.productName);

            File.WriteAllText(plistPath, plist.WriteToString());
        }

        [MenuItem("BlackBears/iOS/Edit Facebook")]
        private static void EditFacebook()
        {
            CheckAsset();
            Selection.activeObject = settings;
        }

        private static void CheckAsset()
        {
            if (settings) return;
            settings = UnityEditor.AssetDatabase.LoadAssetAtPath<FacebookSettings>(FacebookSettings.settingsPath);
            if (settings) return;

            settings = ScriptableObject.CreateInstance<FacebookSettings>();
            UnityEditor.AssetDatabase.CreateAsset(settings, FacebookSettings.settingsPath);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

    }

}