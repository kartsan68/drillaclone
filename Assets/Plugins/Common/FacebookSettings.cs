﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Facebook
{

    public class FacebookSettings : ScriptableObject
    {

        public const string settingsPath = "Assets/Editor/FacebookSettings.asset";

        [SerializeField] private string facebookAppId;

        public string FacebookAppId => facebookAppId;

    }

}