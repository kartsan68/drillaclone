
skeleton.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
ht-8-bg
  rotate: true
  xy: 604, 130
  size: 148, 84
  orig: 148, 84
  offset: 0, 0
  index: -1
ht-8-coil_light
  rotate: true
  xy: 690, 201
  size: 77, 95
  orig: 77, 95
  offset: 0, 0
  index: -1
ht-8-elev_light
  rotate: false
  xy: 847, 254
  size: 37, 24
  orig: 37, 24
  offset: 0, 0
  index: -1
ht-8-elevator
  rotate: true
  xy: 822, 174
  size: 44, 66
  orig: 44, 66
  offset: 0, 0
  index: -1
ht-8-gofr_1
  rotate: false
  xy: 690, 139
  size: 60, 60
  orig: 60, 60
  offset: 0, 0
  index: -1
ht-8-gofr_2
  rotate: false
  xy: 604, 44
  size: 105, 84
  orig: 105, 84
  offset: 0, 0
  index: -1
ht-8-high_voltage
  rotate: false
  xy: 771, 93
  size: 51, 46
  orig: 51, 46
  offset: 0, 0
  index: -1
ht-8-light_window_1
  rotate: false
  xy: 787, 220
  size: 58, 58
  orig: 58, 58
  offset: 0, 0
  index: -1
ht-8-light_window_2
  rotate: true
  xy: 711, 64
  size: 73, 58
  orig: 73, 58
  offset: 0, 0
  index: -1
ht-8-light_window_3
  rotate: false
  xy: 752, 141
  size: 68, 58
  orig: 68, 58
  offset: 0, 0
  index: -1
ht-8-lightning
  rotate: true
  xy: 711, 40
  size: 22, 72
  orig: 22, 72
  offset: 0, 0
  index: -1
ht-8-main
  rotate: false
  xy: 2, 2
  size: 600, 276
  orig: 600, 276
  offset: 0, 0
  index: -1
