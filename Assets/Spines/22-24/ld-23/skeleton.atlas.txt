
skeleton.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
ld-23-gofr
  rotate: false
  xy: 262, 57
  size: 39, 16
  orig: 39, 16
  offset: 0, 0
  index: -1
ld-23-holder
  rotate: false
  xy: 2, 11
  size: 175, 113
  orig: 175, 113
  offset: 0, 0
  index: -1
ld-23-holder_part
  rotate: true
  xy: 262, 75
  size: 49, 44
  orig: 49, 44
  offset: 0, 0
  index: -1
ld-23-light
  rotate: false
  xy: 236, 2
  size: 13, 13
  orig: 13, 13
  offset: 0, 0
  index: -1
ld-23-scoop_1
  rotate: false
  xy: 179, 6
  size: 55, 27
  orig: 55, 27
  offset: 0, 0
  index: -1
ld-23-scoop_2
  rotate: false
  xy: 179, 35
  size: 81, 89
  orig: 81, 89
  offset: 0, 0
  index: -1
ld-23-scoop_bg
  rotate: true
  xy: 236, 17
  size: 16, 73
  orig: 16, 73
  offset: 0, 0
  index: -1
