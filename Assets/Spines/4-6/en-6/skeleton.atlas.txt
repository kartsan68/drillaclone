
skeleton.png
size: 512,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
en-6-belt
  rotate: true
  xy: 2, 16
  size: 105, 325
  orig: 105, 325
  offset: 0, 0
  index: -1
en-6-bg_1
  rotate: false
  xy: 2, 561
  size: 327, 460
  orig: 327, 460
  offset: 0, 0
  index: -1
en-6-bg_2
  rotate: false
  xy: 2, 123
  size: 141, 436
  orig: 141, 436
  offset: 0, 0
  index: -1
en-6-body
  rotate: true
  xy: 145, 252
  size: 77, 43
  orig: 77, 43
  offset: 0, 0
  index: -1
en-6-carrot
  rotate: true
  xy: 145, 124
  size: 22, 55
  orig: 22, 55
  offset: 0, 0
  index: -1
en-6-ear_l
  rotate: true
  xy: 310, 6
  size: 8, 40
  orig: 8, 40
  offset: 0, 0
  index: -1
en-6-ear_r
  rotate: false
  xy: 202, 136
  size: 29, 29
  orig: 29, 29
  offset: 0, 0
  index: -1
en-6-head
  rotate: false
  xy: 145, 158
  size: 35, 39
  orig: 35, 39
  offset: 0, 0
  index: -1
en-6-leg_b
  rotate: true
  xy: 204, 8
  size: 6, 51
  orig: 6, 51
  offset: 0, 0
  index: -1
en-6-leg_f
  rotate: true
  xy: 257, 8
  size: 6, 51
  orig: 6, 51
  offset: 0, 0
  index: -1
en-6-plank
  rotate: true
  xy: 145, 331
  size: 228, 38
  orig: 228, 38
  offset: 0, 0
  index: -1
en-6-rope
  rotate: true
  xy: 112, 8
  size: 6, 58
  orig: 6, 58
  offset: 0, 0
  index: -1
en-6-tail
  rotate: true
  xy: 172, 2
  size: 12, 30
  orig: 12, 30
  offset: 0, 0
  index: -1
en-6-track
  rotate: true
  xy: 202, 328
  size: 231, 60
  orig: 231, 60
  offset: 0, 0
  index: -1
en-6-tube
  rotate: false
  xy: 329, 148
  size: 33, 411
  orig: 33, 411
  offset: 0, 0
  index: -1
en-6-tube_holder
  rotate: false
  xy: 2, 2
  size: 108, 12
  orig: 108, 12
  offset: 0, 0
  index: -1
en-6-wheel_1
  rotate: false
  xy: 182, 167
  size: 30, 30
  orig: 30, 30
  offset: 0, 0
  index: -1
en-6-wheel_2
  rotate: false
  xy: 145, 199
  size: 51, 51
  orig: 51, 51
  offset: 0, 0
  index: -1
