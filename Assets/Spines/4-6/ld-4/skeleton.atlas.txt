
skeleton.png
size: 256,128
format: RGBA8888
filter: Linear,Linear
repeat: none
ld-4-basement
  rotate: false
  xy: 148, 17
  size: 60, 60
  orig: 60, 60
  offset: 0, 0
  index: -1
ld-4-rod
  rotate: true
  xy: 210, 2
  size: 75, 15
  orig: 75, 15
  offset: 0, 0
  index: -1
ld-4-scoop
  rotate: false
  xy: 2, 15
  size: 144, 62
  orig: 144, 62
  offset: 0, 0
  index: -1
