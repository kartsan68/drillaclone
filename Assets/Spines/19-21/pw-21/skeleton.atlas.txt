
skeleton.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
pw-21-bg
  rotate: true
  xy: 2, 2
  size: 59, 456
  orig: 59, 456
  offset: 0, 0
  index: -1
pw-21-hose_1
  rotate: true
  xy: 460, 113
  size: 89, 48
  orig: 89, 48
  offset: 0, 0
  index: -1
pw-21-hose_2
  rotate: false
  xy: 361, 66
  size: 84, 45
  orig: 84, 45
  offset: 0, 0
  index: -1
pw-21-light
  rotate: false
  xy: 447, 65
  size: 14, 14
  orig: 14, 14
  offset: 0, 0
  index: -1
pw-21-main
  rotate: true
  xy: 2, 63
  size: 139, 357
  orig: 139, 357
  offset: 0, 0
  index: -1
pw-21-tube
  rotate: false
  xy: 447, 81
  size: 27, 30
  orig: 27, 30
  offset: 0, 0
  index: -1
