
skeleton.png
size: 2048,256
format: RGBA8888
filter: Linear,Linear
repeat: none
en-19-arrow
  rotate: true
  xy: 2, 3
  size: 9, 21
  orig: 9, 21
  offset: 0, 0
  index: -1
en-19-belt_1
  rotate: false
  xy: 629, 43
  size: 85, 211
  orig: 85, 211
  offset: 0, 0
  index: -1
en-19-belt_2
  rotate: true
  xy: 190, 22
  size: 232, 138
  orig: 232, 138
  offset: 0, 0
  index: -1
en-19-cap
  rotate: false
  xy: 839, 10
  size: 327, 102
  orig: 327, 102
  offset: 0, 0
  index: -1
en-19-cyl_1
  rotate: false
  xy: 1242, 161
  size: 72, 93
  orig: 72, 93
  offset: 0, 0
  index: -1
en-19-cyl_2
  rotate: false
  xy: 775, 114
  size: 87, 140
  orig: 87, 140
  offset: 0, 0
  index: -1
en-19-grid
  rotate: true
  xy: 1168, 12
  size: 111, 81
  orig: 111, 81
  offset: 0, 0
  index: -1
en-19-hose_1
  rotate: false
  xy: 716, 45
  size: 121, 57
  orig: 121, 57
  offset: 0, 0
  index: -1
en-19-hose_2
  rotate: false
  xy: 716, 104
  size: 57, 150
  orig: 57, 150
  offset: 0, 0
  index: -1
en-19-light
  rotate: false
  xy: 190, 2
  size: 18, 18
  orig: 18, 18
  offset: 0, 0
  index: -1
en-19-main
  rotate: false
  xy: 330, 26
  size: 297, 228
  orig: 297, 228
  offset: 0, 0
  index: -1
en-19-mid
  rotate: false
  xy: 864, 119
  size: 273, 135
  orig: 273, 135
  offset: 0, 0
  index: -1
en-19-propeller
  rotate: false
  xy: 1251, 69
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
en-19-right_bottom
  rotate: false
  xy: 2, 14
  size: 186, 240
  orig: 186, 240
  offset: 0, 0
  index: -1
en-19-right_top
  rotate: false
  xy: 1139, 125
  size: 101, 129
  orig: 101, 129
  offset: 0, 0
  index: -1
en-19-sth
  rotate: false
  xy: 629, 14
  size: 137, 27
  orig: 137, 27
  offset: 0, 0
  index: -1
en-19-whl_1
  rotate: false
  xy: 1251, 7
  size: 60, 60
  orig: 60, 60
  offset: 0, 0
  index: -1
en-19-whl_2
  rotate: false
  xy: 1316, 179
  size: 75, 75
  orig: 75, 75
  offset: 0, 0
  index: -1
en-19-whl_3
  rotate: false
  xy: 1343, 102
  size: 75, 75
  orig: 75, 75
  offset: 0, 0
  index: -1
en-19-whl_4
  rotate: false
  xy: 768, 16
  size: 27, 27
  orig: 27, 27
  offset: 0, 0
  index: -1
