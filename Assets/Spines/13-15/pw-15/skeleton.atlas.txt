
skeleton.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
pw-15-arrow
  rotate: true
  xy: 291, 3
  size: 6, 11
  orig: 6, 11
  offset: 0, 0
  index: -1
pw-15-light
  rotate: false
  xy: 291, 11
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
pw-15-stairs
  rotate: true
  xy: 163, 7
  size: 28, 126
  orig: 28, 126
  offset: 0, 0
  index: -1
pw-15-tank_1
  rotate: false
  xy: 2, 120
  size: 159, 121
  orig: 159, 121
  offset: 0, 0
  index: -1
pw-15-tank_2
  rotate: false
  xy: 406, 136
  size: 100, 105
  orig: 100, 105
  offset: 0, 0
  index: -1
pw-15-tank_3
  rotate: false
  xy: 163, 37
  size: 159, 105
  orig: 159, 105
  offset: 0, 0
  index: -1
pw-15-tank_4
  rotate: false
  xy: 2, 13
  size: 159, 105
  orig: 159, 105
  offset: 0, 0
  index: -1
pw-15-tube_1
  rotate: false
  xy: 389, 15
  size: 97, 119
  orig: 97, 119
  offset: 0, 0
  index: -1
pw-15-tube_2
  rotate: true
  xy: 163, 144
  size: 97, 241
  orig: 97, 241
  offset: 0, 0
  index: -1
pw-15-tubes
  rotate: true
  xy: 324, 2
  size: 140, 63
  orig: 140, 63
  offset: 0, 0
  index: -1
