
skeleton.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
pw-8-basement
  rotate: true
  xy: 283, 2
  size: 141, 456
  orig: 141, 456
  offset: 0, 0
  index: -1
pw-8-tube
  rotate: true
  xy: 83, 18
  size: 125, 198
  orig: 125, 198
  offset: 0, 0
  index: -1
pw-8-tube_cover
  rotate: true
  xy: 2, 96
  size: 47, 79
  orig: 47, 79
  offset: 0, 0
  index: -1
