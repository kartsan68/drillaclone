
skeleton.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
ld-7-rod
  rotate: true
  xy: 264, 39
  size: 167, 51
  orig: 167, 51
  offset: 0, 0
  index: -1
ld-7-scoop
  rotate: false
  xy: 2, 2
  size: 260, 204
  orig: 260, 204
  offset: 0, 0
  index: -1
