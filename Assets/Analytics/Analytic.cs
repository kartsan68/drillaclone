﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using Facebook.Unity;
using UnityEngine;

namespace BlackBears.DiggIsBig.Analytics
{
    public class Analytic : MonoBehaviour
    {
        void Awake()
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                FB.Init(() =>
                {
                    FB.ActivateApp();
                });
            }
            EventManager.SendFB += SendFB;
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                NotificationsManager.ClearAllNotif();
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    FB.Init(() =>
                    {
                        FB.ActivateApp();
                    });
                }
            }
        }

        public void SendFB(string token) => FB.LogAppEvent(token);
    }
}
