using System;

namespace BlackBears.Clans.Events
{

    public abstract class ClanEvent
    {
        
        public event Block<ClanEvent> OnEventStateChanged;

        public bool IsExecuted { get; private set; }

        public abstract EventType EventType { get; }
        public abstract bool EventInProccess { get; }

        internal event Block<ClanEvent> OnInvalidate; 

        protected void InvokeEventStateChanged() => OnEventStateChanged.SafeInvoke(this);

        internal void CheckEventStatus()
        {
            if (EventInProccess == IsExecuted) return;
            IsExecuted = EventInProccess;
            if (IsExecuted) ExecuteEvent();
        }

        internal void Invalidate() => OnInvalidate.SafeInvoke(this);

        protected virtual void StopEvent() 
        {
            IsExecuted = false;
            InvokeEventStateChanged();
        }

        protected abstract void ExecuteEvent();

    }

}