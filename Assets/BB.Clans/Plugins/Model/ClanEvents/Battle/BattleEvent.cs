using System;
using System.Collections.Generic;
using BlackBears.Clans.Model;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.Utils.Collections;
using SimpleJSON;

namespace BlackBears.Clans.Events.ClanBattle
{
    public class BattleEvent : ClanEvent
    {

        private const string lastTakenRewardBattleIdKey = "rwrd_id";
        private const string lastTakenTopRewardIdKey = "rwrd_top_key";
        private const string lastTopPositionInAnalyticsKey = "lst_top_anltcs_key";

        private const long validTime = 120L;

        private ChronometerFeature chronometer;
        private ClanCore core;
        private Defaults defaults;
        private Networking.ServerConnection connection;

        private long battleId;
        private long mNextBattleTime = -1L;

        private Battle mBattle;
        private bool needNotify;
        private bool onSynchronize;

        private bool synchronizeFailed;
        private long lastSynchronizationTime;

        private long lastTakenRewardBattleId = 0;
        private long lastTakenTopRewardId = 0;
        private long lastTopPositionIdInAnalytics = 0;

        private List<ClanUser> leaderTop = new List<ClanUser>();

        internal BattleEvent(ClanCore core, Defaults defaults, Networking.ServerConnection connection)
        {
            this.chronometer = BBGC.Features.Chronometer();
            this.core = core;
            this.defaults = defaults;
            this.connection = connection;
        }

        public override EventType EventType => EventType.Battle;
        public override bool EventInProccess
        {
            get
            {
                var actualTime = chronometer.ActualTime;
                if (Battle != null)
                {
                    if (Battle.IsOnProcess(actualTime)) return true;
                    if (Battle.EndDate <= actualTime && (!IsBattleRewardTaken(Battle.id) || CanTakeTopReward())) return true;
                }
                if (NextBattleTime < 0) return false;
                var nextBattleTimeLeft = NextBattleTime - actualTime;
                if (nextBattleTimeLeft > 0) return nextBattleTimeLeft <= defaults.timeToAlert;
                return !HaveBattle;
            }
        }

        public bool IsSynchronizeSuccess => !synchronizeFailed;

        public Battle Battle
        {
            get { return mBattle; }
            set { mBattle = value; }
        }

        public long NextBattleTime
        {
            get { return mNextBattleTime; }
            private set { mNextBattleTime = value; }
        }

        public bool HaveBattle { get; private set; }

        public bool IsTopRewardTakeOnProcess { get; private set; }

        public IList<ClanUser> LeaderTop => leaderTop.ToImmutable();

        public event Block OnSynchronizeFinished;

        internal bool IsBattleRewardTaken(long battleId)
        {
            return battleId <= lastTakenRewardBattleId;
        }

        internal bool IsTopRewardTaken(long battleId)
        {
            return battleId <= lastTakenTopRewardId;
        }

        internal bool IsTopRewardPlace(int place) => place > 0 && place <= defaults.numberOfPersonalPrizes;

        internal void UpdateBattleData(long battleId, long nextBattleTime, bool haveBattle)
        {
            if (this.battleId == battleId 
                && this.NextBattleTime == nextBattleTime
                && this.HaveBattle == haveBattle)
            {
                if (Battle != null && Battle.id == battleId) return;
            }

            this.battleId = battleId;
            this.NextBattleTime = nextBattleTime;
            this.HaveBattle = haveBattle;
            SynchronizeBattle(true);
        }

        internal void SynchronizeBattle(bool force)
        {
            if (onSynchronize) return;

            if (!force)
            {
                var time = chronometer.ActualTime;
                if (time < lastSynchronizationTime + validTime) return;
            }

            onSynchronize = true;
            if (Battle != null && battleId != Battle.id) StopEvent();

            if (battleId <= 0 && NextBattleTime <= 0)
            {
                OnBattleSynchronized(null);
                return;
            }

            if (Battle == null) connection.GetBattleInfo(battleId, OnBattleSynchronized, OnSynchronizeFailed);
            else connection.GetBattleInfo(Battle, OnBattleSynchronized, OnSynchronizeFailed);
        }

        internal void TakeBattleReward()
        {
            if (this.Battle == null) return;
            if (IsBattleRewardTaken(this.Battle.id)) return;
            var clansData = this.Battle.ClansData;
            if (clansData == null || clansData.Count == 0) return;

            var clan = core.State.Clan;
            if (clan == null) return;
            int index;
            for (index = 0; index < clansData.Count; index++)
            {
                if (clansData[index] == null) continue;
                if (string.Equals(clan.Id, clansData[index].id)) break;
            }
            if (index >= clansData.Count) return;

            core.State.Game.GiveClanBattleReward(index + 1);
            lastTakenRewardBattleId = this.Battle.id;
            SynchronizeBattle(true);
        }

        internal bool CanTakeTopReward()
        {
            if (Battle == null) return false;
            if (IsTopRewardTaken(this.Battle.id)) return false;
            return IsTopRewardPlace(TakeRewardPosition());
        }

        internal void TakeTopReward()
        {
            if (IsTopRewardTakeOnProcess) return;
            if (!CanTakeTopReward()) return;

            int position = TakeRewardPosition();
            IsTopRewardTakeOnProcess = true;
            long battleId = Battle.id;
            connection.GrabBattleReward(core.State.User.Id, battleId, answer => OnTopRewardTaked(answer, battleId, position), OnTopRewardTakeFailed);
        }

        private void OnTopRewardTaked(NetworkSuccessAnswer answer, long battleId, int position)
        {
            IsTopRewardTakeOnProcess = false;
            if (lastTakenTopRewardId < battleId) lastTakenTopRewardId = battleId;
            if (answer.success) core.State.Game.GiveClanTopReward(position);
            OnSynchronizeFinished.SafeInvoke();
        }

        private void OnTopRewardTakeFailed(long errorCode, string message)
        {
            IsTopRewardTakeOnProcess = false;
            OnSynchronizeFinished.SafeInvoke();
            UnityEngine.Debug.Log($"TakeTopReward: {errorCode}; {message}");
        }

        internal int TakeRewardPosition()
        {
            var userId = core.State.User.Id;
            for (int i = 0; i < leaderTop.Count; i++)
            {
                var user = leaderTop[i];
                if (string.Equals(userId, user.Id)) return i + 1;
            }
            return -1;
        }

        internal bool CanWeSendAnalytics() => TakeRewardPosition() > lastTopPositionIdInAnalytics;

        internal void ReplaceLastTopPositionIdInAnalytics()
        {
            lastTopPositionIdInAnalytics = TakeRewardPosition();
        }

        internal bool IsPlayerOnTop() => IsTopRewardPlace(TakeRewardPosition());

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[lastTakenRewardBattleIdKey] = new PLong(lastTakenRewardBattleId).ToString();
            json[lastTakenTopRewardIdKey] = new PLong(lastTakenTopRewardId).ToString();
            json[lastTopPositionInAnalyticsKey] = new PLong(lastTopPositionIdInAnalytics).ToString();
            return json;
        }

        internal void FromJson(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None) return;
            PLong temp = 0;
            if (PLong.TryParse(json[lastTakenRewardBattleIdKey], out temp)) lastTakenRewardBattleId = temp.Value;
            else lastTakenRewardBattleId = 0;
            temp = 0;
            if (PLong.TryParse(json[lastTakenTopRewardIdKey], out temp)) lastTakenTopRewardId = temp.Value;
            else lastTakenTopRewardId = 0;
            temp = 0;
            if (PLong.TryParse(json[lastTopPositionInAnalyticsKey], out temp)) lastTopPositionIdInAnalytics = temp.Value;
            else lastTopPositionIdInAnalytics = 0;
        }

        protected override void ExecuteEvent() => SynchronizeBattle(false);

        protected override void StopEvent()
        {
            Battle = null;
            base.StopEvent();
        }

        private void OnBattleSynchronized(Battle battle)
        {
            this.needNotify = needNotify || this.Battle != battle;
            this.Battle = battle;
            lastSynchronizationTime = chronometer.ActualTime;
            
            if (this.Battle != null && this.Battle.EndDate <= lastSynchronizationTime)
            {
                this.Battle.FinalBattleDataWasLoaded = true;
            }

            if (battleId < 0)
            {
                leaderTop.Clear();
                OnTopUsersLoaded(leaderTop);
            }
            else
            {
                connection.GetBattleTopUsers(battleId, OnTopUsersLoaded, OnTopUsersLoadFailed, leaderTop);
            }
        }

        private void OnTopUsersLoaded(List<ClanUser> loaded)
        {
            //На самом деле строчка ниже бесполезна, 
            //но чтобы понимать что здесь происходит пусть будет
            leaderTop = loaded;
            leaderTop.Sort(ClanUser.SortByBattleScore);
            synchronizeFailed = false;
            onSynchronize = false;
            bool notify = needNotify;
            needNotify = false;

            core.State.ProfileCache.LoadPlayersAsync(leaderTop, null, null);

            if (notify) InvokeEventStateChanged();
            OnSynchronizeFinished.SafeInvoke();
        }

        private void OnSynchronizeFailed(long errorCode, string errorMessage)
        {
            synchronizeFailed = true;
            onSynchronize = false;
            OnSynchronizeFinished.SafeInvoke();
        }

        private void OnTopUsersLoadFailed(long errorCode, string errorMessage)
        {
            synchronizeFailed = true;
            onSynchronize = false;
            OnSynchronizeFinished.SafeInvoke();
        }

        internal class Defaults
        {

            internal readonly float timeToAlert;
            internal readonly int numberOfPersonalPrizes;

            private readonly Dictionary<string, string> additionalInfo;

            internal Defaults(JSONNode json)
            {
                if (json == null) return;
                timeToAlert = json["time_to_alert"].AsFloat;
                numberOfPersonalPrizes = json["pers_prizes_count"].AsInt;

                var addInfoNode = json["additional_info"];
                additionalInfo = new Dictionary<string, string>();

                foreach (var kvp in addInfoNode)
                {
                    additionalInfo[kvp.Key] = kvp.Value.Value;
                }
            }

            internal string GetAdditionalString(string key, string @default = null)
            {
                string result;
                if (!additionalInfo.TryGetValue(key, out result)) result = @default;

                return result;
            }

            internal float GetAdditionalFloat(string key, float @default = 0f)
            {
                string s = GetAdditionalString(key);
                if (s == null) return @default;
                float result;
                if (!float.TryParse(s, System.Globalization.NumberStyles.Float,
                    System.Globalization.CultureInfo.InvariantCulture, 
                    out result))
                {
                    result = @default;
                }
                return result;
            }

        }

    }

}