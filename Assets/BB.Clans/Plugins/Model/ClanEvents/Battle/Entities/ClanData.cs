using SimpleJSON;

namespace BlackBears.Clans.Events.ClanBattle
{
    
    public class BattleClanData
    {
        
        private const string idKey = "id";
        private const string scoreKey = "score";
        private const string updateTimeKey = "time_upd";

        public readonly string id;

        public double Score { get; private set; }
        public long UpdateTime { get; private set; }

        public BattleClanData(JSONNode json)
        {
            id = json[idKey].Value;
            Score = json[scoreKey].AsDouble;
            UpdateTime = json[updateTimeKey].AsLong;
        }

        public bool IsDataForThisClan(JSONNode json)
        {
            if (json == null) return false;
            return string.Equals(id, json[idKey].Value);
        }

        public void Synchronize(JSONNode json)
        {
            if (!IsDataForThisClan(json)) return;
            var dataTime = json[updateTimeKey].AsLong;
            if (this.UpdateTime > dataTime) return;

            this.UpdateTime = dataTime;
            this.Score = json[scoreKey].AsDouble;
        }

    }
    
}