using System;
using System.Collections.Generic;
using BlackBears.Clans.Model;
using BlackBears.Utils.Collections;
using SimpleJSON;

namespace BlackBears.Clans.Events.ClanBattle
{

    public class Battle
    {

        private const string idKey = "id";
        private const string startDateKey = "start_date";
        private const string endDateKey = "end_date";
        private const string clansDataKey = "clans";

        public readonly long id;    
        
        public long StartDate { get; private set; }
        public long EndDate { get; private set; }
        private List<BattleClanData> clansData = new List<BattleClanData>();
        private IList<BattleClanData> clansDataImmutable;
        
        public bool FinalBattleDataWasLoaded { get; internal set; }

        private Battle()
        {
            clansDataImmutable = clansData.ToImmutable();
        }

        internal Battle(JSONNode data) : this()
        {
            id = data[idKey].AsLong;

            FromJson(data);
        }

        internal IList<BattleClanData> ClansData => clansDataImmutable;

        internal bool FromJson(JSONNode json)
        {
            if (json == null || this.id != json[idKey].AsLong) return false;
            StartDate = json[startDateKey].AsLong;
            EndDate = json[endDateKey].AsLong;

            var tempList = new List<BattleClanData>(clansData);
            clansData.Clear();

            var clansNode = json[clansDataKey].AsArray;
            for (int i = 0; i < clansNode.Count; i++)
            {
                var clanNode = clansNode[i];
                if (clanNode == null || clansNode.Tag == JSONNodeType.None) continue;

                bool founded = false;
                for (int j = 0; j < tempList.Count; j++)
                {
                    if (!tempList[j].IsDataForThisClan(clanNode)) continue;
                    tempList[j].Synchronize(clanNode);
                    clansData.Add(tempList[j]);
                    tempList.RemoveAt(j);
                    founded = true;
                    break;
                }

                if (founded) continue;
                clansData.Add(new BattleClanData(clanNode));
            }
            tempList.Clear();
            clansData.Sort(SortByScore);

            return true;
        }

        internal bool IsOnProcess(long actualTime)
        {
            return StartDate <= actualTime && actualTime <= EndDate;
        }

        private int SortByScore(BattleClanData x, BattleClanData y)
        {
            //null check
            if (x == y) return 0;
            if (x == null) return 1;
            if (y == null) return -1;

            // score check
            if (x.Score > y.Score) return -1;
            if (x.Score < y.Score) return 1;

            // update time check (if scores equal)
            if (x.UpdateTime < y.UpdateTime) return -1;
            if (x.UpdateTime > y.UpdateTime) return 1;

            // clan id check (if update time equals)
            if (x.id == y.id) return 0;
            if (x.id == null) return 1;
            if (y.id == null) return -1;

            var xHash = x.id.GetHashCode();
            var yHash = y.id.GetHashCode();
            if (xHash < yHash) return -1;
            if (xHash > yHash) return 1;

            return 0;
        }

    }

}