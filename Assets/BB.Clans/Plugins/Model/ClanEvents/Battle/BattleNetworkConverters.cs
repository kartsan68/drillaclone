using System.Collections.Generic;
using BlackBears.Clans.Events.ClanBattle;
using BlackBears.Clans.Model;
using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.Clans.Networking
{

    internal class GetBattleInfoConverter : DataConverter<Battle>
    {

        internal GetBattleInfoConverter(Block<Battle> onConvert, FailBlock onFail, Battle battle = null) 
            : base(onConvert, onFail, battle) { }

        protected override bool Convert(ref Battle instance, JSONNode data)
        {
            if (instance == null) 
            {
                instance = new Battle(data["battle"]);
                return true;
            }
            return instance.FromJson(data["battle"]);
        }

    }

    internal class GetBattleTopUsersConverter : DataConverter<List<ClanUser>>
    {

        internal GetBattleTopUsersConverter(Block<List<ClanUser>> onConvert,
            List<ClanUser> list) : base(onConvert, null, list) {}

        protected override bool Convert(ref List<ClanUser> instance, JSONNode data)
        {
            if (instance == null) instance = new List<ClanUser>();
            else instance.Clear();

            var usersArray = data["users"].AsArray;
            for (int i = 0; i < usersArray.Count; i++)
            {
                instance.Add(new ClanUser(usersArray[i]));
            }

            return true;
        }

    }

}