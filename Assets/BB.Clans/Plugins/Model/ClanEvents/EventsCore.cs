using System;
using BlackBears.Clans.View.Events;
using BlackBears.GameCore.Features.CoreDefaults;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.Clans.Events
{
    public class EventsCore
    {

        private const string battleEventKey = "bttlEvnt";

        private ClanCore core;
        private ClanFullSettings fullSettings;

        private ClanEventsSettings EventSettings => fullSettings.configuration.ClanEvents;

        internal EventsCore(ClanCore core, ClanFullSettings fullSettings,
            Networking.ServerConnection connection)
        {
            this.core = core;
            this.fullSettings = fullSettings;
            BattleEvent = new ClanBattle.BattleEvent(core, fullSettings.parameters.battleEvent, connection);

            BattleEvent.OnEventStateChanged += EventStateChanged;
            BattleEvent.OnInvalidate += ValidateBattleInfo;
        }

        internal ClanBattle.BattleEvent BattleEvent { get; private set; }

        internal event Block<EventType> OnEventStateChanged;

        internal void UpdateBattleData(long battleId, long nextBattleId, bool haveBattle)
        {
            BattleEvent.UpdateBattleData(battleId, nextBattleId, haveBattle);
        }

        internal ClanEvent GetEventWithCard()
        {
            if (CheckEventController(BattleEvent, EventSettings.BattleEventEnabled, EventSettings.BattleEventCard))
                return BattleEvent;

            return null;
        }

        internal EventCard GetCardPrefabForEvent(ClanEvent e)
        {
            if (e == null) return null;
            switch (e.EventType)
            {
                case EventType.Battle: return EventSettings.BattleEventCard;
                default: return null;
            }
        }

        internal bool IsEventSupported(EventType eventType) => EventSettings.IsEventSupported(eventType);

        internal void OnClanLeaved() 
        {
            BattleEvent.UpdateBattleData(-1, -1, false);
        }

        internal void FromJson(JSONNode json)
        {
            if (json == null || json.Tag != JSONNodeType.Object) return;
            BattleEvent.FromJson(json[battleEventKey]);
        }

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[battleEventKey] = BattleEvent.ToJson();
            return json;
        }

        private bool CheckEventController(ClanEvent e, bool enabled, MonoBehaviour controller)
        {
            return e != null
                && e.EventInProccess
                && enabled
                && controller != null;
        }

        private void EventStateChanged(ClanEvent e)
        {
            if (!EventSettings.IsEventSupported(e.EventType)) return;

            OnEventStateChanged.SafeInvoke(e.EventType);
            if (e.EventInProccess) e.CheckEventStatus();
        }

        private void ValidateBattleInfo(ClanEvent e)
        {
            core.Validate(null, null);
        }

    }

}