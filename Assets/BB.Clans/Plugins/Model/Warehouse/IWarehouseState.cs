using System.Collections.Generic;

namespace BlackBears.Clans.ResourceShare
{

    internal interface IWarehouseState
    {
        
        WarehouseConfig Config { get; }

        bool IsNeedValidate { get; }
        bool HasIncomingDonations { get; }
        long RequestBlockingTime { get; }

        IList<IncomingDonation> IncomingDonations { get; }
        
    }

}