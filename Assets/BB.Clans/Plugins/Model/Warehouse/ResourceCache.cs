using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.Utils.Collections;

namespace BlackBears.Clans.ResourceShare
{

    internal class ResourceCache
    {

        internal readonly IList<GameResource> resources;

        internal ResourceCache(IGameAdapter game)
        {
            var inResources = game.CreateResourceList();
            var filteredResources = new List<GameResource>(inResources.Count);
            foreach(var resource in inResources)
            {
                if (resource == null) continue;
                filteredResources.Add(resource);
            }
            
            this.resources = filteredResources.ToImmutable();
        }

    }
    
}