using System;
using SimpleJSON;

namespace BlackBears.Clans.ResourceShare
{

    internal class IncomingDonation
    {

        private const string donationIdKey = "donation_id";
        private const string playerIdKey = "player_id";
        private const string valueKey = "value";
        private const string resourceTypeKey = "resource_type";
        private const string resourceIdKey = "resource_id";
        private const string grabTimeKey = "grab_time";

        internal readonly string id;
        internal readonly string playerId;
        internal readonly double value;
        internal readonly int resourceType;
        internal readonly long resourceId;
        internal readonly long grabTime;

        internal IncomingDonation(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None) return;
            this.id = json[donationIdKey].Value;
            this.playerId = json[playerIdKey].Value;
            this.value = json[valueKey].AsDouble;
            this.resourceType = json[resourceTypeKey].AsInt;
            this.resourceId = json[resourceIdKey].AsLong;
            this.grabTime = json[grabTimeKey].AsLong;
        }

        internal IncomingDonation(IncomingDonation prototype, long grabTime)
        {
            this.id = prototype.id;
            this.playerId = prototype.playerId;
            this.value = prototype.value;
            this.resourceType = prototype.resourceType;
            this.resourceId = prototype.resourceId;
            this.grabTime = grabTime;
        }

        internal static int GrabTimeCompare(IncomingDonation x, IncomingDonation y)
        {
            if (x == y) return 0;
            if (x == null || y == null) return x == null ? 1 : -1;
            long xt = x.grabTime;
            long yt = y.grabTime;
            return xt > yt ? 1 : (xt == yt ? 0 : -1);
        }

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[donationIdKey] = this.id;
            json[playerIdKey] = this.playerId;
            json[valueKey] = this.value;
            json[resourceTypeKey] = this.resourceType;
            json[resourceIdKey] = this.resourceId;
            json[grabTimeKey] = this.grabTime;
            return json;
        }

    }

}