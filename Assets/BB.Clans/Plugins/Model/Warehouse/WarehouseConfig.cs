using SimpleJSON;

namespace BlackBears.Clans.ResourceShare
{

    internal class WarehouseConfig
    {

        internal readonly long blockRequestInterval;
        internal readonly double resourceRequestCommonMultiplier;
        internal readonly double resourceRequestRewardCPSMultiplier;
        internal readonly int resourceDonationMaxCount;
        internal readonly long donateInfoLifeTime;
        internal readonly long incomeDonationOldTime;
        internal readonly long incomeDonationForgetTime;
        internal readonly long invalidateInterval;

        internal WarehouseConfig(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None) return;

            blockRequestInterval = json["block_request_interval"].AsLong;
            resourceRequestCommonMultiplier = json["resource_request_common_multiplier"].AsDouble;
            resourceRequestRewardCPSMultiplier = json["resource_request_reward_cps_multiplier"].AsDouble;
            resourceDonationMaxCount = json["resource_donation_max_count"].AsInt;
            donateInfoLifeTime = json["max_donate_info_life_time"].AsLong;

            incomeDonationOldTime = json["income_donation_old_time"].AsLong;
            incomeDonationForgetTime = json["income_donation_forget_time"].AsLong;
            invalidateInterval = json["invalidate_interval"].AsLong;
        }

    }

}