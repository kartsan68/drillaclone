using UnityEngine;

namespace BlackBears.Clans.ResourceShare
{

    public class GameResource
    {
        
        public readonly int type;
        public readonly long id;
        public readonly Sprite icon;
        public readonly string nameKey;

        public GameResource(int type, long id, Sprite icon, string nameKey)
        {
            this.type = type;
            this.id = id;
            this.icon = icon;
            this.nameKey = nameKey;
        }

    }

}