using System;
using System.Collections.Generic;
using BlackBears.Clans.Analytics;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.Model.Timeline;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.GameCore.Features.Save;
using SimpleJSON;

namespace BlackBears.Clans.ResourceShare
{

    internal class Warehouse
    {

        private const int maxIncomeDonationGrab = 90;

        private WarehouseState state;

        internal event Block OnResourceRequested;
        internal event Block OnValidate;
        internal event Block OnIncomingDonationsEmpty;

        internal Warehouse(ClanCore core, ChronometerFeature chronometer,
            WarehouseConfig config)
        {
            state = new WarehouseState(core, chronometer, config);
        }

        internal IWarehouseState State => state;

        internal void FromJson(JSONNode json)
        {
            state.FromJson(json);

            CheckIncomeDonationsForRemoving();
            CheckOutcomeDonationsForRemoving();
        }

        internal JSONNode ToJson() => state.ToJson();

        internal void OnGameConnected()
        {
            if (state.resourceCache != null) return;
            state.resourceCache = new ResourceCache(state.core.State.Game);
            state.analytics = state.core.State.Analytics;
        }

        internal bool IsRequestAvailableByTime() => IsRequestAvailableByTime(state.chronometer.ActualTime);
        internal double GetRequestingAmountOfResources(GameResource resource) => state.core.State.Game.GetRequestingAmountOfResources(resource);

        internal List<GameResource> GenerateAvailableResourcesList()
        {
            var all = state.resourceCache.resources;
            var available = new List<GameResource>();
            var game = state.core.State.Game;
            foreach (var resource in all)
            {
                if (!game.IsAvailableResource(resource)) continue;
                available.Add(resource);
            }
            return available;
        }

        internal GameResource GetResourceWithTypeById(int resourceType, long resourceId)
        {
            foreach (var resource in state.resourceCache.resources)
            {
                if (resource == null) continue;
                if (resource.type == resourceType && resource.id == resourceId) return resource;
            }
            return null;
        }

        internal void ValidateIcncomingDonations()
        {
            if (state.chronometer.ActualTime < state.clearIncomingTime) return;
            CheckIncomeDonationsForRemoving();
        }

        internal void TryToValidateWithServer(Block onSuccess = null, FailBlock onFail = null)
        {
            if (state.IsNeedValidate) ValidateWithServer(onSuccess, onFail);
            else onFail.SafeInvoke();
        }

        internal void ValidateWithServer(Block onSuccess = null, FailBlock onFail = null)
        {
            state.invalidateTime = state.chronometer.ActualTime + state.Config.invalidateInterval;

            state.core.CheckIncomingDonations(requests =>
            {
                List<IncomingDonation> serverDonations = null;
                if (requests != null && requests.Count > 0)
                {
                    serverDonations = new List<IncomingDonation>();
                    foreach (var request in requests)
                    {
                        serverDonations.AddRange(request.donations);
                    }
                }
                if (serverDonations == null || serverDonations.Count == 0)
                {
                    state.invalidateTime = state.chronometer.ActualTime + state.Config.invalidateInterval;
                    onSuccess.SafeInvoke();
                    OnValidate.SafeInvoke();
                    return;
                }

                state.requestsCount = serverDonations.Count / maxIncomeDonationGrab;
                if (serverDonations.Count > state.requestsCount * maxIncomeDonationGrab) state.requestsCount += 1;
                var donationIds = new List<string>();
                foreach (var serverDonation in serverDonations) donationIds.Add(serverDonation.id);

                int counter = state.requestsCount;
                for (int i = 0; i < counter; i++)
                {
                    var donIndex = maxIncomeDonationGrab * i;
                    var donCount = UnityEngine.Mathf.Min(maxIncomeDonationGrab, serverDonations.Count - donIndex);
                    state.core.GrabIncomingDonations(donationIds.GetRange(donIndex, donCount),
                        answer =>
                        {
                            --state.requestsCount;

                            if (answer.success)
                            {
                                if (state.requestsCount > 0) return;
                                IncomeDonationsGrabbed(serverDonations);
                                state.invalidateTime = state.chronometer.ActualTime + state.Config.invalidateInterval;
                                onSuccess.SafeInvoke();
                                OnValidate.SafeInvoke();
                            }
                            else
                            {
                                state.invalidateTime = 0;
                                if (state.requestsCount > 0) return;
                                onFail.SafeInvoke();
                            }
                        }
                    );
                }
            },
            (c, m) =>
            {
                UnityEngine.Debug.Log($"Validate: {c}; {m}");
                if (state.requestsCount > 0) return;
                state.invalidateTime = 0;
                onFail.SafeInvoke();
            });
        }

        internal void RequestResource(GameResource resource, double amount, Block onSuccess, FailBlock onFail)
        {
            var actualTime = state.chronometer.ActualTime;
            UnityEngine.Debug.Log("1");
            if (resource == null || amount < 0f)
            {
                UnityEngine.Debug.Log("2");
                onFail.SafeInvoke();
                return;
            }
UnityEngine.Debug.Log("3");
            if (!IsRequestAvailableByTime(actualTime))
            {
                UnityEngine.Debug.Log("4");
                onFail.SafeInvoke();
                return;
            }

            UnityEngine.Debug.Log("5 " + resource.type + " " + resource.id + " " + amount);

            state.core.RequestResource(resource.type, resource.id, amount, answer =>
            {
                UnityEngine.Debug.Log("6");
                if (answer.success)
                {
                    UnityEngine.Debug.Log("7");
                    state.RequestBlockingTime = actualTime + state.Config.blockRequestInterval;
                    state.analytics.AddClanResourceRequestToUserProfile();
                    OnResourceRequested.SafeInvoke();
                    onSuccess.SafeInvoke();
                }
                else
                {
                    UnityEngine.Debug.Log("8");
                    onFail.SafeInvoke();
                }
            }, onFail);
        }

        internal void SendResource(FeedItem feedItem, Block<ResourceTransition> onSuccess, FailBlock onFail,
            Block<double> onCurrencyAdded)
        {
            FeedInfo info = feedItem?.info;
            if (info == null)
            {
                onFail.SafeInvoke((long)GiveResourceRequestError.other, null);
                return;
            }

            if (!CanDonate(info.requestId))
            {
                onFail.SafeInvoke((long)GiveResourceRequestError.other, null);
                return;
            }

            double amount = CalculateGiveDonationAmount(info);
            if (state.core.State.Game.CanSpendResource(info.resourceType, info.resourceId, amount))
            {
                state.core.SendResource(info.requestId, info.resourceType, info.resourceId, amount,
                transition =>
                {
                    var donation = GetOutcomingDonation(info.requestId);
                    donation.Set(donation.Count + 1, state.chronometer.ActualTime);

                    double reward = state.core.State.Game.CoinsPerSecond * state.Config.resourceRequestRewardCPSMultiplier;
                    if (state.core.State.Game.AddSoftCurrency(reward)) onCurrencyAdded.SafeInvoke(reward);

                    state.core.State.Game.SpendResource(info.resourceType, info.resourceId, amount);
                    onSuccess.SafeInvoke(transition);
                },
                (c, m) =>
                {
                    if ((GiveResourceRequestError)c > GiveResourceRequestError.other)
                    {
                        var donation = GetOutcomingDonation(info.requestId);
                        donation.Set(state.Config.resourceDonationMaxCount,
                            state.chronometer.ActualTime);
                    }
                    onFail.SafeInvoke(c, m);
                });
            }
            else
            {
                onFail.SafeInvoke((long)GiveResourceRequestError.notEnoughResource, null);
            }
        }

        internal bool IsAvailableResource(int resourceType, long resourceId)
            => state.core.State.Game.IsAvailableResource(resourceType, resourceId);

        internal bool CanDonate(string requestId)
        {
            OutcomingDonation donation;
            if (state.requestIdToOutcomingDonation.TryGetValue(requestId, out donation))
            {
                return donation.Count < state.Config.resourceDonationMaxCount;
            }
            return true;
        }

        internal double CalculateGiveDonationAmount(FeedInfo info)
        {
            if (info == null) return 0;
            double amount = info.value * state.Config.resourceRequestCommonMultiplier / state.Config.resourceDonationMaxCount;
            return Math.Max(Math.Floor(amount), 1.0);
        }

        internal bool HasIncomingDonation(IncomingDonation donation) => state.incomingDonationsSet.Contains(donation);

        internal bool SendIncomingDonationToGame(IncomingDonation donation)
        {
            if (donation == null || !state.incomingDonationsSet.Contains(donation)) return false;

            bool success = state.core.State.Game.AddResource(donation.resourceType,
                donation.resourceId, donation.value);

            if (!success) return false;

            state.incomingDonations.Remove(donation);
            state.incomingDonationsSet.Remove(donation);
            if (state.incomingDonations.Count == 0) OnIncomingDonationsEmpty.SafeInvoke();
            return true;
        }

        internal void SendAllIncomingDonationsToGame()
        {
            if (state.incomingDonations.Count == 0) return;

            var game = state.core.State.Game;
            for (int i = 0; i < state.incomingDonations.Count; i++)
            {
                var donation = state.incomingDonations[i];
                if (donation == null)
                {
                    state.incomingDonations.RemoveAt(i);
                    --i;
                    continue;
                }
                bool sended = game.AddResource(donation.resourceType, donation.resourceId, donation.value);
                if (sended)
                {
                    state.incomingDonations.RemoveAt(i);
                    state.incomingDonationsSet.Remove(donation);
                    --i;
                }
            }
            if (state.incomingDonations.Count == 0) OnIncomingDonationsEmpty.SafeInvoke();
        }

        private void IncomeDonationsGrabbed(List<IncomingDonation> serverDonations)
        {
            CheckIncomeDonationsForRemoving();

            if (serverDonations == null || serverDonations.Count == 0) return;

            var actualTime = state.chronometer.ActualTime;
            state.clearIncomingTime = Math.Min(actualTime + state.Config.incomeDonationForgetTime, state.clearIncomingTime);

            foreach (var serverDonation in serverDonations)
            {
                var donation = new IncomingDonation(serverDonation, actualTime);
                state.incomingDonations.Add(donation);
                state.incomingDonationsSet.Add(donation);
            }

            BBGC.Features.GetFeature<SaveFeature>().SaveManager.Save();
        }

        private OutcomingDonation GetOutcomingDonation(string requestId)
        {
            OutcomingDonation donation;
            if (!state.requestIdToOutcomingDonation.TryGetValue(requestId, out donation) || donation == null)
            {
                donation = new OutcomingDonation(requestId);
                state.requestIdToOutcomingDonation[requestId] = donation;
            }
            return donation;
        }

        private void CheckIncomeDonationsForRemoving()
        {
            var actualTime = state.chronometer.ActualTime;
            for (int i = 0; i < state.incomingDonations.Count; ++i)
            {
                var donation = state.incomingDonations[i];
                if (actualTime - donation.grabTime > state.Config.incomeDonationForgetTime)
                {
                    state.incomingDonations.RemoveAt(i);
                    --i;
                }
            }
            
            if (state.incomingDonations.Count == 0) state.clearIncomingTime = long.MaxValue;
        }

        private void CheckOutcomeDonationsForRemoving()
        {
            var toRemove = new List<string>();

            long currentTime = state.chronometer.ActualTime;

            foreach (var kvp in state.requestIdToOutcomingDonation)
            {
                OutcomingDonation d = kvp.Value;
                if (d != null)
                {
                    long lastRequestTime = d.RequestTime;
                    if (lastRequestTime + state.Config.donateInfoLifeTime < currentTime)
                    {
                        toRemove.Add(kvp.Key);
                    }
                }
                else
                {
                    toRemove.Add(kvp.Key);
                }
            }

            foreach (var key in toRemove) state.requestIdToOutcomingDonation.Remove(key);
        }

        private bool IsRequestAvailableByTime(long actualTime) => actualTime >= state.RequestBlockingTime;

    }

}