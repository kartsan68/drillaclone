using System;
using SimpleJSON;

namespace BlackBears.Clans.ResourceShare
{

    internal class OutcomingDonation
    {

        private const string requestIdKey = "req_id";
        private const string countKey = "count";
        private const string requestTimeKey = "request_time";

        internal readonly string requestId;
        
        internal OutcomingDonation(string requestId, int count, long requestTime)
        {
            this.requestId = requestId;
            this.Count = count;
            this.RequestTime = requestTime;
        }

        internal OutcomingDonation(string requestId) : this(requestId, 0, 0L) {}

        internal OutcomingDonation(OutcomingDonation other)
        {
            this.requestId = other.requestId;
            this.Count = other.Count;
            this.RequestTime = other.RequestTime;
        }

        internal OutcomingDonation(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None)
            {
                this.requestId = null;
                this.Count = 0;
                this.RequestTime = 0;
            }
            else
            {
                this.requestId = json[requestIdKey].Value;
                this.Count = json[countKey].AsInt;
                this.RequestTime = json[requestTimeKey].AsLong;
            }
        }

        internal int Count { get; private set; }
        internal long RequestTime { get; private set; }

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[requestIdKey] = requestId;
            json[countKey] = Count;
            json[requestTimeKey] = RequestTime;
            return json;
        }

        internal void Set(int count, long requestTime)
        {
            this.Count = count;
            this.RequestTime = requestTime;
        }
        
    }

}