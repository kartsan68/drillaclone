namespace BlackBears.Clans.ResourceShare
{
    public enum GiveResourceRequestError
    {
        notEnoughResource = -2,
        other = -1,
        nonExistentRequest = 9,
        expiredRequest = 12,
        donationLimit = 13
    }

}