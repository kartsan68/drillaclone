using System;
using System.Collections.Generic;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.GameCore.Features.Save;
using BlackBears.Utils.Collections;
using SimpleJSON;

using CultureInfo = System.Globalization.CultureInfo;
using NumberStyles = System.Globalization.NumberStyles;


namespace BlackBears.Clans.ResourceShare
{

    internal class WarehouseState : IWarehouseState
    {

        private const string requestBlockingTimeKey = "rqst_blck_tm";
        private const string incomingDonationsKey = "in_dnts";
        private const string outcomingDonationsKey = "out_dnts";

        internal readonly ClanCore core;
        internal readonly ChronometerFeature chronometer;

        internal IAnalyticsAdapter analytics;
        internal ResourceCache resourceCache;

        internal long clearIncomingTime = long.MaxValue;
        internal long invalidateTime;
        internal int requestsCount;

        internal List<IncomingDonation> incomingDonations = new List<IncomingDonation>();
        internal HashSet<IncomingDonation> incomingDonationsSet = new HashSet<IncomingDonation>();
        internal Dictionary<string, OutcomingDonation> requestIdToOutcomingDonation = new Dictionary<string, OutcomingDonation>();

        private IList<IncomingDonation> incomingDonationsImmutable;

        internal WarehouseState(ClanCore core, ChronometerFeature chronometer, 
            WarehouseConfig config)
        {
            this.core = core;
            this.chronometer = chronometer;
            this.Config = config;
        }

        internal WarehouseState(WarehouseConfig config)
        {
            this.Config = config;

        }

        public WarehouseConfig Config { get; private set; }

        public bool IsNeedValidate => chronometer.ActualTime > invalidateTime;
        public long RequestBlockingTime { get; internal set; }
        public bool HasIncomingDonations => incomingDonations.Count > 0;

        public IList<IncomingDonation> IncomingDonations
        {
            get
            {
                if (incomingDonationsImmutable == null) incomingDonationsImmutable = incomingDonations.ToImmutable();
                return incomingDonationsImmutable;
            }
        }

        internal void FromJson(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None) return;
            RequestBlockingTime = json[requestBlockingTimeKey].AsLong;

            requestIdToOutcomingDonation.Clear();
            var outDonationsArray = json[outcomingDonationsKey].AsArray;
            foreach (var dJson in outDonationsArray.Children)
            {
                var donation = new OutcomingDonation(dJson);
                requestIdToOutcomingDonation[donation.requestId] = donation;
            }

            incomingDonations.Clear();
            incomingDonationsSet.Clear();
            var inDonationsArray = json[incomingDonationsKey].AsArray;
            foreach(var dJson in inDonationsArray.Children)
            {
                var donation = new IncomingDonation(dJson);
                incomingDonations.Add(donation);
                clearIncomingTime = Math.Min(donation.grabTime + Config.incomeDonationForgetTime, clearIncomingTime);
            }
            incomingDonationsSet.UnionWith(incomingDonations);
        }

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[requestBlockingTimeKey] = RequestBlockingTime;

            var outDonations = new JSONArray();
            json[outcomingDonationsKey] = outDonations;
            foreach (var kvp in requestIdToOutcomingDonation)
            {
                outDonations.Add(kvp.Value.ToJson());
            }

            var inDonations = new JSONArray();
            json[incomingDonationsKey] = inDonations;
            foreach(var inDonation in incomingDonations)
            {
                inDonations.Add(inDonation.ToJson());
            }

            return json;
        }

    }

}