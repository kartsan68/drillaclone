using SimpleJSON;

namespace BlackBears.Clans
{

    public class ClanParams
    {

        internal readonly string server;
        internal readonly string jellyServer;

        internal readonly PDouble updatePlayerCost;
        internal readonly PDouble createClanCost;
        internal readonly PDouble updateClanCost;

        internal readonly float updateRequestDelay;
        internal readonly float updateRequestImportantDelay;

        internal readonly bool resourceRequestButtonShow;

        internal readonly Events.ClanBattle.BattleEvent.Defaults battleEvent;
        internal readonly ResourceShare.WarehouseConfig warehouse;

        public ClanParams(JSONNode json)
        {
            server = json["server"].Value;
            jellyServer = json["jelly_server"].Value;

            updatePlayerCost = new PDouble(json["update_player_cost"].AsDouble);
            createClanCost = new PDouble(json["create_clan_cost"].AsDouble);
            updateClanCost = new PDouble(json["update_clan_cost"].AsDouble);

            updateRequestDelay = json["update_request_delay"].AsFloat;
            updateRequestImportantDelay = json["update_request_important_delay"].AsFloat;

            resourceRequestButtonShow = json["resource_request_button_show"].AsBool;

            warehouse = new ResourceShare.WarehouseConfig(json["warehouse"]);
            var eventsNode = json["events"];
            battleEvent = new Events.ClanBattle.BattleEvent.Defaults(eventsNode["battle"]);
        }

    }

}