using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.CoreDefaults;

namespace BlackBears.Clans
{

    public class ClanFullSettings
    {
        
        internal readonly ClansConfiguration configuration;
        internal readonly ClanParams parameters;

        internal readonly SpritePack softCurPack;
        internal readonly SpritePack hadrCurPack;       
        internal readonly SpritePack scoreIconPack;
        internal readonly SubscriptionSpritesContainer subsSprites;

        public ClanFullSettings(ClansConfiguration configuration, ClanParams parameters, 
            SpritePack softPack, SpritePack hardPack, SpritePack scoreIconPack,
            SubscriptionSpritesContainer subsSprites)
        {
            this.configuration = configuration;
            this.parameters = parameters;
            this.softCurPack = softPack;
            this.hadrCurPack = hardPack;
            this.scoreIconPack = scoreIconPack;
            this.subsSprites = subsSprites;
        }

    }

}