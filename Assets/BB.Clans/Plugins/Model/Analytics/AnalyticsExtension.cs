using System.Collections.Generic;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;

namespace BlackBears.Clans.Analytics
{

    internal static class AnalyticsExtension
    {

        internal static void OnClanCreated(this IAnalyticsAdapter adapter, string clanId, string clanName)
        {
            adapter?.PostEvent(EventKeys.createClan, CreateClanData(clanId, clanName));
        }

        internal static void OnClanUpdated(this IAnalyticsAdapter adapter, string clanId, string clanName)
        {
            adapter?.PostEvent(EventKeys.updateClan, CreateClanData(clanId, clanName));
        }

        internal static void OnJoinToClan(this IAnalyticsAdapter adapter, string clanId, string clanName)
        {
            adapter?.PostEvent(EventKeys.joinClan, CreateClanData(clanId, clanName));
        }

        internal static void OnDisbandClan(this IAnalyticsAdapter adapter, string clanId, string clanName)
        {
            adapter?.PostEvent(EventKeys.disbandClan, CreateClanData(clanId, clanName));
        }

        internal static void OnLeaveClan(this IAnalyticsAdapter adapter, string clanId, string clanName)
        {
            adapter?.PostEvent(EventKeys.leaveClan, CreateClanData(clanId, clanName));
        }

        internal static void AddClansMessageToUserProfile(this IAnalyticsAdapter adapter)
        {
            adapter?.AddUserProfileCounter(EventKeys.messageInClan, 1);
        }

        internal static void AddClanResourceRequestToUserProfile(this IAnalyticsAdapter adapter)
        {
            adapter?.AddUserProfileCounter(EventKeys.resourceRequest, 1);
        }

        private static Dictionary<string, object> CreateClanData(string clanId, string clanName)
        {
            return new Dictionary<string, object>
            {
                [ParametersKeys.clanId] = clanId, 
                [ParametersKeys.clanName] = clanName
            };
        }

    }

}