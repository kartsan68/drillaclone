namespace BlackBears.Clans.Analytics
{

    internal static class ParametersKeys
    {
        
        internal const string clanId = "clan_id";
        internal const string clanName = "clan_name";

    }

}