namespace BlackBears.Clans.Analytics
{
    
    internal static class EventKeys
    {

        internal const string createClan = "bbgc_clans_clan_created";
        internal const string updateClan = "bbgc_clans_clan_updated";
        internal const string joinClan = "bbgc_clans_join_clan";
        internal const string disbandClan = "bbgc_clans_disband_clan";
        internal const string leaveClan = "bbgc_clans_leave_clan";

        internal const string messageInClan = "Сообщений в клане";
        internal const string resourceRequest = "Запросы ресурсов";

    }

}