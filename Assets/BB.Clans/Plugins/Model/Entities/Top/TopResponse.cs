using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    internal class TopResponse
    {

        internal readonly Clan userClan;
        internal readonly List<Clan> clans = new List<Clan>();

        internal TopResponse(JSONNode data, bool clansIsDictionary)
        {
            if (data == null || data.Tag == JSONNodeType.None) return;
            var userClanNode = data["user_clan"];
            if (userClanNode.Tag != JSONNodeType.None) userClan = new Clan(userClanNode);

            if (clansIsDictionary) ParseTopAsDictionary(data["clans"]);
            else ParseTopAsArray(data["clans"]);
        }

        internal static TopResponse Build(JSONNode data, bool clansIsDictionary)
        {
            if (data == null) return new TopResponse(data, clansIsDictionary);
            if (WorldTopResponse.IsDataForWorldTop(data)) return new WorldTopResponse(data, clansIsDictionary);
            else return new TopResponse(data, clansIsDictionary);
        }

        private void ParseTopAsDictionary(JSONNode node)
        {
            if (node == null || node.Tag == JSONNodeType.None) return;
            foreach(var kvp in node) clans.Add(new Clan(kvp.Value));
        }

        private void ParseTopAsArray(JSONNode node)
        {
            if (node == null || node.Tag == JSONNodeType.None) return;
            foreach(var clanNode in node.Children) clans.Add(new Clan(clanNode));
        }

    }

}