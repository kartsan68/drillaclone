using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    internal class WorldTopResponse : TopResponse
    {

        private const string countryTopKey = "country_top";

        internal readonly List<CountryTopEntry> countries = new List<CountryTopEntry>();

        internal WorldTopResponse(JSONNode data, bool clansIsDictionary) : base(data, clansIsDictionary)
        {
            var countriesNode = data[countryTopKey].AsArray;
            for (int i = 0; i < countriesNode.Count; i++)
            {
                countries.Add(new CountryTopEntry(countriesNode[i]));
            }
        }

        internal static bool IsDataForWorldTop(JSONNode data)
        {
            if (data == null) return false;
            return data[countryTopKey].Tag == JSONNodeType.Array;
        }

    }
    
}