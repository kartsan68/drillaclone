using System;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    internal class CountryTopEntry : IComparable<CountryTopEntry>
    {
        
        internal readonly string name;
        internal readonly double total;

        internal CountryTopEntry(JSONNode node)
        {
            name = node["country"].Value;
            total = node["total"].AsDouble;
        }

        public int CompareTo(CountryTopEntry other)
        {
            if (other == null) return 1;
            if (total < other.total) return 1;
            if (total > other.total) return -1;
            return 0;
        }

    }

}