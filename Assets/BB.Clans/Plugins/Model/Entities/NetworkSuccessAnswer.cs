namespace BlackBears.Clans.Model
{

    public class NetworkSuccessAnswer
    {

        public static readonly NetworkSuccessAnswer Success = new NetworkSuccessAnswer(true);
        public static readonly NetworkSuccessAnswer NotSuccess = new NetworkSuccessAnswer(false);

        public readonly bool success;

        private NetworkSuccessAnswer(bool success)
        {
            this.success = success;
        }

    }

}