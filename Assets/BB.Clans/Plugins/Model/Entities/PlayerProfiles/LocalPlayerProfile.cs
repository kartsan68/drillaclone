namespace BlackBears.Clans.Model.PlayerProfiles
{

    internal class LocalPlayerProfile : IPlayerProfile
    {

        private ClanCore core;
        private Icon icon;
        private bool iconValid = false;

        internal LocalPlayerProfile(ClanCore core)
        {
            this.core = core;
        }

        public string Id { get { return core.State.Game.PlayerId; } }
        public bool IsLocal { get { return true; } }
        public string Nickname { get { return core.State.Game.Nickname; } }
        public double Score { get { return core.State.Game.Score; } }
        public long League { get { return core.State.Game.League; } }
        public long RegisterDate { get { return core.State.Game.StartGameDate; } }
        public string Country { get { return core.State.Game.Country; } }
        public Icon Icon { get { return (iconValid && icon != null) ? icon : icon = Icon.UpdateIcon(icon, core.State.Game.Icon); } }
        public double WeeklyScore { get { return core.State.Game.WeeklyScore; } }
        public string SubscriptionId { get { return core.State.Game.SubscriptionId; } }

        internal void ResetIcon() { iconValid = false; }

    }

}