using System;
using BlackBears.GameServer;

namespace BlackBears.Clans.Model.PlayerProfiles
{

    internal class PlayerProfile : IPlayerProfile
    {

        private const long validTime = 10L * 60L;

        private bool profileValid;
        private long lastSyncTime;
        private IPlayer gamePlayer;
        private Icon icon;

        internal PlayerProfile(string playerId)
        {
            Id = playerId;
            Invalidate();
        }

        public bool IsLocal { get { return false; } }

        public string Id { get; private set; }
        public string Nickname { get { return gamePlayer != null ? gamePlayer.Nick : string.Empty; } }
        public long RegisterDate { get { return gamePlayer != null ? gamePlayer.RegisterDate : -1L; } }
        public double Score { get { return gamePlayer != null ? gamePlayer.Score : 0.0; } }
        public long League { get { return gamePlayer != null ? gamePlayer.League : 0L; } }
        public Icon Icon
        {
            get
            {
                if (icon == null) UpdateIcon();
                return icon;
            }
        }

        public double WeeklyScore { get { return gamePlayer != null ? gamePlayer.WeeklyScore : 0.0; } }
        public string SubscriptionId { get { return gamePlayer?.SubscriptionId; } }

        public string Country { get { return gamePlayer != null ? gamePlayer.Country : null; } }

        internal bool IsValid(long actualTime)
        {
            if (!profileValid) return false;
            if (actualTime - lastSyncTime > validTime) Invalidate();
            return profileValid;
        }

        internal void Apply(IPlayer gamePlayer, long currentTime)
        {
            if (gamePlayer == null || !string.Equals(Id, gamePlayer.Id)) return;

            this.gamePlayer = gamePlayer;
            lastSyncTime = currentTime;
            UpdateIcon();
            profileValid = true;
        }

        internal void Invalidate() => profileValid = false;

        private void UpdateIcon()
        {
            icon = Icon.UpdateIcon(icon, gamePlayer.Icon);
        }

    }

}