using System;
using System.Collections.Generic;
using BlackBears.Chronometer;
using BlackBears.GameCore.Features.GameServer;
using BlackBears.GameServer;

namespace BlackBears.Clans.Model.PlayerProfiles
{

    internal class PlayerProfileCache
    {

        private GameServerFeature gameServer;
        private Dictionary<string, PlayerProfile> cachedProfiles = new Dictionary<string, PlayerProfile>();

        public LocalPlayerProfile LocalPlayer { get; private set; }

        internal PlayerProfileCache(ClanCore core, GameServerFeature gameServer)
        {
            this.gameServer = gameServer;
            LocalPlayer = new LocalPlayerProfile(core);
        }

        internal void LoadPlayersForClanAsync(Clan clan, bool onlyClanMembers, Block onSuccess, FailBlock onFail)
        {
            if (clan == null)
            {
                onFail.SafeInvoke(-1, "Clan is null");
                return;
            }
            if (clan.Users == null || clan.Users.Count == 0)
            {
                onSuccess.SafeInvoke();
                return;
            }

            List<string> playerIds = new List<string>();
            foreach(var user in clan.Users) 
            {
                if (!onlyClanMembers || user.Role.IsClanMember()) playerIds.Add(user.Id);
            }
            LoadPlayersAsync(playerIds, onSuccess, onFail);
        }

        internal void GetPlayerAsync(string playerId, Block<IPlayerProfile> onSuccess, FailBlock onFail)
        {
            if (string.IsNullOrEmpty(playerId))
            {
                onFail.SafeInvoke(-1, "Incorrect PlayerId");
                return;
            }

            if (IsLocalPlayer(playerId))
            {
                onSuccess.Invoke(LocalPlayer);
                return;
            }

            PlayerProfile profile = null;
            if (!cachedProfiles.TryGetValue(playerId, out profile)) 
            {
                profile = new PlayerProfile(playerId);
                cachedProfiles[playerId] = profile;
            }

            if (!profile.IsValid(TimeModule.ActualTime))
            {
                gameServer.Connection.GetPlayerById(profile.Id, gp => {
                    ValidateProfile(profile, gp);
                    onSuccess.SafeInvoke(profile);
                }, onFail);
            }
            else
            {
                onSuccess.SafeInvoke(profile);
            }
        }

        internal void LoadPlayersAsync(List<ClanUser> users, Block onSuccess, FailBlock onFail)
        {
            var ids = new List<string>();
            foreach(var user in users) 
            {
                if (user != null) ids.Add(user.Id);
            }
            LoadPlayersAsync(ids, onSuccess, onFail);
        }

        internal void LoadPlayersAsync(List<string> playerIds, Block onSuccess, FailBlock onFail)
        {
            List<string> invalidatedIds = new List<string>();
            
            long actualTime = TimeModule.ActualTime;
            foreach(var id in playerIds)
            {
                if (IsLocalPlayer(id)) continue;
                
                var player = GetPlayerProfile(id);
                if (!player.IsValid(actualTime)) invalidatedIds.Add(id);
            }

            if (invalidatedIds.Count == 0)
            {
                onSuccess.SafeInvoke();
                return;
            }

            gameServer.Connection.GetPlayersByIds(invalidatedIds.ToArray(), gps => {
                long validateTime = TimeModule.ActualTime;
                foreach(var gp in gps)
                {
                    var player = GetPlayerProfile(gp.Id);
                    player.Apply(gp, validateTime);
                }
                onSuccess.SafeInvoke();
            }, onFail);
        }

        internal IPlayerProfile GetCachedPlayer(string playerId)
        {
            if (IsLocalPlayer(playerId)) return LocalPlayer;
            return GetPlayerProfile(playerId);
        }

        private bool IsLocalPlayer(string playerId)
        {
            return string.Equals(LocalPlayer.Id, playerId);
        }

        private PlayerProfile GetPlayerProfile(string playerId)
        {
            if (IsLocalPlayer(playerId)) return null;
            PlayerProfile profile;
            if (!cachedProfiles.TryGetValue(playerId, out profile))
            {
                profile = new PlayerProfile(playerId);
                cachedProfiles[playerId] = profile;
            }

            return profile;
        }

        private void ValidateProfile(PlayerProfile profile, IPlayer gamePlayer)
        {
            if (profile != null) profile.Apply(gamePlayer, TimeModule.ActualTime);
        }

    }

}