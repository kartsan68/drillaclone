namespace BlackBears.Clans.Model.PlayerProfiles
{

    internal interface IPlayerProfile
    {

        string Id { get; }
        bool IsLocal { get; }
        string Nickname { get; }
        long RegisterDate { get; }
        double Score { get; }
        double WeeklyScore { get; }
        string Country { get; }
        long League { get; }
        Icon Icon { get; }
        string SubscriptionId { get; }

    }

}