using System.Collections.Generic;
using BlackBears.Chronometer;
using BlackBears.Clans.Model;

namespace BlackBears.Clans.Model
{
    public class ClanCache
    {

        private ClanCore core;
        private Dictionary<string, Clan> cachedClans = new Dictionary<string, Clan>();

        internal ClanCache(ClanCore core)
        {
            this.core = core;
        }

        internal void LoadClanAsync(string clanId, bool force, Block<Clan> onSuccess, FailBlock onFail)
        {
            if (string.IsNullOrEmpty(clanId))
            {
                onFail.SafeInvoke(-1, "Incorrect ClanId");
                return;
            }

            Clan clan = GetClan(clanId);

            if (force || !clan.IsValid(TimeModule.ActualTime))
            {
                core.GetClan(clan, _ =>
                {
                    ValidateClan(clan);
                    onSuccess.SafeInvoke(clan);
                }, onFail);
            }
            else
            {
                onSuccess.SafeInvoke(clan);
            }
        }

        private Clan GetClan(string clanId)
        {
            Clan clan;
            if (!cachedClans.TryGetValue(clanId, out clan))
            {
                clan = new Clan(clanId);
                cachedClans[clanId] = clan;
            }

            return clan;
        }

        private void ValidateClan(Clan clan)
        {
            if (clan != null) clan.Apply(TimeModule.ActualTime);
        }

    }
}