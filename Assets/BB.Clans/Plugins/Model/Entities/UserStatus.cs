namespace BlackBears.Clans.Model
{

    internal enum UserStatus
    {
        InClan, Kicked, Left, NoClan, ClanDisbanded
    }

    internal static class UserStatusUtils
    {
        internal static bool InClan(this UserStatus status) 
        {
            return status == UserStatus.InClan;
        }
    }

}