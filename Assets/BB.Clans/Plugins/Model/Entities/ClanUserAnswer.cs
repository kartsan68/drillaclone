using SimpleJSON;

namespace BlackBears.Clans.Model
{
    
    internal class ClanUserAnswer
    {

        private const string statusKey = "state";
        private const string clanUserKey = "clan_user";
        private const string clanTitleKey = "clan_title";
        private const string clanIconKey = "clan_icon";
        private const string kickerKey = "kicked_by";

        internal readonly ClanUser user;
        internal readonly UserStatus status;
        internal readonly string clanTitle;
        internal readonly string kickerId;
        internal readonly Icon clanIcon;

        internal ClanUserAnswer(JSONNode data)
        {
            status = (UserStatus)data[statusKey].AsInt;
            switch(status)
            {
                case UserStatus.InClan: 
                    user = new ClanUser(data[clanUserKey]);
                    break;
                case UserStatus.Kicked: 
                case UserStatus.ClanDisbanded:
                    clanTitle = data[clanTitleKey].Value;
                    kickerId = data[kickerKey].Value;
                    clanIcon = Icon.Build(data[clanIconKey]);
                    break;
                case UserStatus.Left:
                    clanTitle = data[clanTitleKey].Value;
                    break;
            }
        }

    }

}