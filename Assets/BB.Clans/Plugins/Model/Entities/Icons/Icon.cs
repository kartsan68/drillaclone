using System;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    internal enum IconType
    {
        PredefinedIcon,
        CustomIcon
    }

    public abstract class Icon
    {

        private const string typeKey = "t";

        internal abstract IconType Type { get; }

        internal static Icon CreateDefaultIcon()
        {
            return new PredefinedIcon();
        }

        internal static Icon Build(string icon)
        {
            return Build(JSON.Parse(icon));
        }

        internal static Icon Build(JSONNode node)
        {
            if (node == null) return new PredefinedIcon();
            
            var type = (IconType)node[typeKey].AsInt;
            switch (type)
            {
                case IconType.PredefinedIcon: return new PredefinedIcon(node);
                default: return new PredefinedIcon();
            }
        }

        internal static Icon UpdateIcon(Icon original, string iconData)
        {
            if (string.IsNullOrEmpty(iconData)) return PredefinedIcon.defaultIcon;
            return UpdateIcon(original, JSON.Parse(iconData));
        }

        internal static Icon UpdateIcon(Icon original, JSONNode node)
        {
            if (node == null) return PredefinedIcon.defaultIcon;
            if (original == null || !original.Update(node)) return Build(node);
            return original;
        }

        internal JSONNode ToJson()
        {
            JSONNode json = new JSONObject();
            json[typeKey] = (int)Type;
            FillJson(json);
            return json;
        }

        internal virtual bool Update(JSONNode node)
        {
            if (node == null || (IconType)node[typeKey].AsInt != Type) return false;
            return true;
        }

        internal abstract Icon Clone();

        protected abstract void FillJson(JSONNode json);

        public sealed override bool Equals(object obj)
        {
            return Equals(obj as Icon);
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }

        public virtual bool Equals(Icon other)
        {
            if (other == null) return false;
            return this.Type == other.Type;
        }

        public static bool Equals(Icon a, Icon b)
        {
            if (a == b) return true;
            if (a == null && b != null) return false;
            return a.Equals(b);
        }

    }

}