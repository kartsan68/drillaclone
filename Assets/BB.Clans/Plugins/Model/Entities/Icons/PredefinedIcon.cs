using SimpleJSON;

namespace BlackBears.Clans.Model
{

    internal sealed class PredefinedIcon : Icon
    {

        private const string idKey = "id";

        internal static readonly PredefinedIcon defaultIcon = new PredefinedIcon(-1);

        private int id;

        internal PredefinedIcon() {}

        internal PredefinedIcon(PredefinedIcon prototype)
        {
            if (prototype != null) this.id = prototype.id;
        }

        internal PredefinedIcon(JSONNode node)
        {
            Update(node);
        }

        private PredefinedIcon(int id) 
        {
            this.id = id;
        }

        internal override IconType Type { get { return IconType.PredefinedIcon; } }

        internal int Id
        {
            get { return id; }
            set
            {
                if (value < 0) return;
                id = value;
            }
        }

        protected override void FillJson(JSONNode json)
        {
            json[idKey] = id;
        }

        internal override bool Update(JSONNode node)
        {
            if (!base.Update(node)) return false;
            id = node[idKey].AsInt;
            return true;
        }

        internal override Icon Clone()
        {
            return new PredefinedIcon(this);
        }

        public override bool Equals(Icon inOther)
        {
            if (!base.Equals(inOther)) return false;
            var other = inOther as PredefinedIcon;
            if (other == null) return false;

            return this.id == other.id;
        }
        
        public override int GetHashCode()
        {
            int hash = 5;
            hash = hash * 7 + id.GetHashCode();
            hash = hash * 7 + base.GetHashCode();
            return hash;
        }

    }

}