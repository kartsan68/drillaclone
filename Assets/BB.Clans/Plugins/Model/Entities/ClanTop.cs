using SimpleJSON;

namespace BlackBears.Clans.Model
{

    public struct ClanTop
    {

        public readonly long rewardDate;
        public readonly int rewardPlace;

        public ClanTop(JSONNode data)
        {
            rewardDate = data["date_reward"].AsLong;
            rewardPlace = data["place_reward"].AsInt;
        }

    }

}