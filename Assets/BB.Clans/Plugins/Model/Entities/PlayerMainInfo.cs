using System.Collections.Generic;
using BlackBears.Utils.Collections;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    public class PlayerMainInfo
    {

        public readonly long lastEntry;
        public readonly long unread;
        public readonly long donationsAmount;
        public readonly ClanTop clanTop;
        public readonly long nextBattleTime;
        public readonly bool haveBattle;
        public readonly JSONNode events;

        private List<long> givenResources = new List<long>();

        public PlayerMainInfo(JSONNode data)
        {
            lastEntry = data["last_entry"].AsLong;
            unread = data["unread"].AsLong;
            donationsAmount = data["amount_donations"].AsLong;
            nextBattleTime = data["next_battle_time"].GetLongOrDefault(-1);
            haveBattle = data["have_battle"].AsBool;
            
            var resourcesData = data["given_resources"].AsArray;
            for(int i = 0; i < resourcesData.Count; i++)
                givenResources.Add(resourcesData[i].AsLong);

            var topData = data["top"];
            if (topData.Tag != JSONNodeType.None) clanTop = new ClanTop(topData);
            var eventsData = data["events"];
            if (eventsData.Tag != JSONNodeType.None) events = eventsData;
        }

        public IList<long> GivenResources { get { return givenResources.ToImmutable(); } }
        
    }

}