using System;

namespace BlackBears.Clans.Model.Timeline
{

    internal struct PlayerScore
    {

        internal readonly string id;
        internal readonly double score;

        internal PlayerScore(string id, double score)
        {
            this.id = id;
            this.score = score;
        }

    }

}