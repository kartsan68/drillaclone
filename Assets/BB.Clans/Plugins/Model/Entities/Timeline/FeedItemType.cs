namespace BlackBears.Clans.Model.Timeline
{
    
    internal enum FeedItemType
    {
        SavePlace = 301, //Не используется.
        UserKicked = 302, //OK
        JoinRequest = 303, //OK
        JoinConfirmed = 304, //OK
        ResourceRequest = 305, //OK
        ClanInTop = 308, //Не планировалось
        UserPromoted = 309, //OK
        UserDemoted = 310, //OK
        Message = 311, //OK
        UserLeft = 312, //OK
        UserAccepted = 313, //OK
        AdministrationMessage = 314, //OK
        NewLeader = 315, //OK
        RatingRise = 316, //OK
        PhotoMessage = 317 // OK
    }

    internal static class FeedItemTypeUtils
    {

        internal static bool IsAnyMessage(this FeedItemType type)
        {
            return type == FeedItemType.Message;
        }

    }
    
}