﻿using SimpleJSON;

namespace BlackBears.Clans.Model.Timeline
{

    internal class FeedItem
    {

        internal readonly long id;
        internal readonly string playerId;
        internal readonly long date;
        internal readonly FeedItemType feedType;
        internal readonly FeedInfo info;

        internal FeedItem(JSONNode node)
        {
            id = node["id"].AsLong;
            playerId = node["player_id"].Value;
            date = node["date"].AsLong;
            feedType = (FeedItemType)node["type"].AsInt;
            info = new FeedInfo(node["info"]);
        }

    }

}