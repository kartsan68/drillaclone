using System;
using BlackBears.Clans.Model.ResourceTransaction;
using SimpleJSON;

namespace BlackBears.Clans.Model.Timeline
{
    internal class FeedInfo
    {
        internal readonly string message;
        internal readonly string moderatorId; //admin_id

        internal readonly string requestId; //request_id
        internal readonly int resourceType; //resource_type
        internal readonly long resourceId; //resource_id
        internal readonly double value; //value

        internal readonly long place; //place
        internal readonly long daysRemain; //days_remain

        internal readonly PlayerScore[] playerScores;

        internal FeedInfo(JSONNode node)
        {
            message = node["message"].Value;
            moderatorId = node["admin_id"].Value;
            
            requestId = node["request_id"].Value;
            resourceType = node["resource_type"].AsInt;
            resourceId = node["resource_id"].AsLong;
            value = node["value"].AsDouble;
            CurrentValue = node["cur_value"].AsDouble;
            
            place = node["place"].AsLong;
            daysRemain = node["days_remain"].AsLong;

            var playerTopArray = node["player_top"];
            if (playerTopArray.Tag == JSONNodeType.Array)
            {
                playerScores = new PlayerScore[playerTopArray.Count];
                for (int i = 0; i < playerScores.Length; i++)
                {
                    var scoreNode = playerTopArray[i];
                    playerScores[i] = new PlayerScore(scoreNode["player_id"].Value, scoreNode["score"].AsDouble);
                }
            }
        }

        internal double CurrentValue { get; private set; } //cur_value

        internal void ApplyTransition(ResourceTransition transition)
        {
            if (transition == null || !string.Equals(this.requestId, transition.requestId)) return;
            CurrentValue = transition.resourceCollected;
        }

    }

}