using System.Collections.Generic;
using BlackBears.Clans.ResourceShare;
using BlackBears.Utils.Collections;
using SimpleJSON;

namespace BlackBears.Clans.Model.ResourceTransaction
{

    internal class ClanResourceRequest
    {

        internal readonly string id;
        internal readonly string requestId;
        internal readonly string requesterId;
        internal readonly string clanId;
        internal readonly long resourceId;
        internal readonly double currentValue;
        internal readonly double value;
        internal readonly long expireDate;
        internal readonly IList<IncomingDonation> donations;

        internal ClanResourceRequest(string id, JSONNode data)
        {
            this.id = id;
            requestId = data["request_id"].Value;
            requesterId = data["requester_id"].Value;
            clanId = data["clan_id"].Value;
            resourceId = data["resource_id"].AsLong;
            currentValue = data["cur_value"].AsDouble;
            value = data["value"].AsDouble;
            expireDate = data["expires_in"].AsLong;

            var donationsList = new List<IncomingDonation>();
            var donationsData = data["donations"].AsArray;
            for (int i = 0; i < donationsData.Count; i++)
            {
                donationsList.Add(new IncomingDonation(donationsData[i]));
            }
            donations = donationsList.ToImmutable();
        }

    }

}