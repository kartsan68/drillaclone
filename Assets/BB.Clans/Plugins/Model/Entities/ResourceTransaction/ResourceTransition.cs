using SimpleJSON;

namespace BlackBears.Clans.Model.ResourceTransaction
{

    internal class ResourceTransition
    {

        internal readonly string requestId;
        internal readonly double resourceCollected;
        internal readonly int transitionPart;

        internal ResourceTransition(string requestId, JSONNode json)
        {
            this.requestId = requestId;

            if (json == null || json.Tag == JSONNodeType.None) return;
            resourceCollected = json["resource_collected"].AsDouble;
            transitionPart = json["part"].AsInt;
        }

    }

}