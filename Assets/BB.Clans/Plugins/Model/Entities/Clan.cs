using System;
using System.Collections.Generic;
using SimpleJSON;
using BlackBears.Utils.Collections;

namespace BlackBears.Clans.Model
{

    public class Clan
    {

        private const long validTime = 2L * 60L;

        private List<ClanUser> users = new List<ClanUser>();

        private long lastSyncTime;
        private bool clanValid;
        private int _joinRequests;

        internal string Id { get; private set; }
        internal string Title { get; private set; }
        internal string Description { get; private set; }
        internal bool IsPublic { get; private set; }

        internal Icon Icon { get; private set; }

        internal int Amount { get; private set; }
        internal int Capacity { get; private set; }

        internal double Score { get; private set; }

        internal long WorldPlace { get; private set; }
        internal long OldPlace { get; private set; }
        internal long LocalPlace { get; private set; }

        internal long CreateUtcDate { get; private set; }
        internal double JoinRequirement { get; private set; }

        internal int JoinRequests
        {
            get { return _joinRequests; }
            set
            {
                if (_joinRequests == value) return;
                _joinRequests = value;
                OnJoinRequestsCountChanged.SafeInvoke();
            }
        }

        internal string Country { get; private set; }

        internal string Tag { get; private set; }

        public IList<ClanUser> Users { get; private set; }
        public bool IsRideHigherInTop
        {
            get
            {
                if (OldPlace == 0 || WorldPlace == 0 || WorldPlace == OldPlace) return false;
                return WorldPlace < OldPlace;
            }
        }

        internal event Block OnUpdated;
        internal event Block OnJoinRequestsCountChanged;

        public Clan()
        {
            Users = users.ToImmutable();
            clanValid = false;
        }

        ~Clan()
        {
            OnUpdated = null;
            OnJoinRequestsCountChanged = null;
        }

        public Clan(string clanId) : this()
        {
            this.Id = clanId;
        }

        public Clan(JSONNode data) : this()
        {
            FromJson(data);
        }

        internal void Apply(long currentTime)
        {
            lastSyncTime = currentTime;
            clanValid = true;
        }

        internal bool IsValid(long currentTime)
        {
            if (!clanValid) return false;
            if (currentTime - lastSyncTime > validTime) Invalidate();
            return clanValid;
        }

        internal void UpdateScore(UpdateScoreAnswer answer)
        {
            if (answer == null || !answer.scoreUpdated) return;
            Score = answer.clanScore;
        }

        internal int GetUserPlace(string userId)
        {
            int index = users.FindIndex(user => string.Equals(userId, user.Id));
            return index >= 0 ? index + 1 : index;
        }

        internal ClanUser GetUser(string userId)
        {
            return users.Find(user => string.Equals(userId, user.Id));
        }

        internal ClanRole GetUserClanRole(string userId)
        {
            var user = GetUser(userId);
            return user != null ? user.Role : ClanRole.None;
        }

        internal void FromJson(JSONNode data)
        {
            Id = data["id"].GetStringOrDefault(Id);
            Title = data["title"].GetStringOrDefault(Title);
            Description = data["description"].GetStringOrDefault(Description);
            Amount = data["amount_users"].GetIntOrDefault(Amount);
            Capacity = data["max_users"].GetIntOrDefault(Capacity);
            IsPublic = data["is_public"].GetBoolOrDefault(IsPublic);
            Score = data["score"].GetDoubleOrDefault(Score);
            WorldPlace = data["place"].GetLongOrDefault(WorldPlace);
            OldPlace = data["old_place"].GetLongOrDefault(OldPlace);
            LocalPlace = data["local_place"].GetLongOrDefault(LocalPlace);
            CreateUtcDate = data["created"].GetLongOrDefault(CreateUtcDate);
            JoinRequirement = data["join_requirement"].GetDoubleOrDefault(JoinRequirement);
            Country = data["country"].GetStringOrDefault(Country);
            Tag = data["tag"].GetStringOrDefault(Tag);

            Icon = Icon.Build(data["icon"]);

            ReadMembers(data["clan_members"]);
            OnUpdated.SafeInvoke();
        }

        private void ReadMembers(JSONNode data)
        {
            if (data == null || data.Tag == JSONNodeType.None) return;

            var usersById = new Dictionary<string, ClanUser>();
            foreach (var user in users) usersById[user.Id] = user;
            users.Clear();
            var joinRequests = 0;

            for (int i = 0; i < data.Count; i++)
            {
                var userData = data[i];
                string userId = ClanUser.IdFromUserData(userData);
                ClanUser user;
                if (usersById.TryGetValue(userId, out user)) user.FromJson(userData);
                else user = new ClanUser(data[i]);

                users.Add(user);
                joinRequests += user.Role.IsClanMember() ? 0 : 1;
            }

            users.Sort(ClanUser.SortByScore);
            JoinRequests = joinRequests;
        }

        internal void RemoveUser(string playerId)
        {
            for (int i = 0; i < users.Count; i++)
            {
                var user = users[i];
                if (user == null || !string.Equals(user.Id, playerId)) continue;
                users.RemoveAt(i);
                RecalculateJoinRequests();
                break;
            }
        }

        internal void RecalculateJoinRequests()
        {
            var joinRequests = 0;
            foreach (var user in users) joinRequests += user.Role.IsClanMember() ? 0 : 1;
            JoinRequests = joinRequests;
        }

        internal void AddClanUser(ClanUser inUser)
        {
            if (inUser == null || users.Contains(inUser)) return;
            if (!string.Equals(Id, inUser.ClanId)) return;
            users.Add(inUser);
            users.Sort(ClanUser.SortByScore);

            int userAmount = 0;
            foreach (var user in users)
            {
                if (user.Role.IsClanMember()) userAmount += 1;
            }
            Amount = userAmount;
        }

        private bool Invalidate() => clanValid = false;

    }

}