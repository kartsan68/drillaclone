namespace BlackBears.Clans.Model
{

    internal class ClanEdit
    {

        internal readonly string adminId;

        internal ClanEdit(string adminId)
        {
            this.adminId = adminId;
            Icon = new PredefinedIcon();
            IsPublic = true;
        }

        internal ClanEdit(string adminId, Clan clan) : this(adminId)
        {
            if (clan == null) return;
            Title = clan.Title;
            Description = clan.Description;
            if (clan.Icon != null) Icon = clan.Icon.Clone();
            Country = clan.Country;
            JoinRequirement = clan.JoinRequirement;
            IsPublic = clan.IsPublic;
        }

        private ClanEdit(ClanEdit prototype) : this(prototype.adminId)
        {
            Title = prototype.Title;
            Description = prototype.Description;
            Icon = prototype.Icon.Clone();
            Country = prototype.Country;
            JoinRequirement = prototype.JoinRequirement;
            IsPublic = prototype.IsPublic;
        }

        internal string Title { get; set; }
        internal string Description { get; set; }
        internal Icon Icon { get; set; }
        internal string Country { get; set; }
        internal double JoinRequirement { get; set; }
        internal bool IsPublic { get; set; }

        internal static bool Equals(ClanEdit a, ClanEdit b)
        {
            if (a == b) return true;
            if (a == null || b == null) return a == b;

            if (!string.Equals(a.Title, b.Title)) return false;
            if (!string.Equals(a.Description, b.Description)) return false;
            if (!Icon.Equals(a.Icon, b.Icon)) return false;
            if (!string.Equals(a.Country, b.Country)) return false;
            if (a.JoinRequirement != b.JoinRequirement) return false;
            if (a.IsPublic != b.IsPublic) return false;

            return true;
        }

        internal ClanEdit Clone()
        {
            return new ClanEdit(this);
        }

    }


}