using System;
using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    public class PlayersClanDataUnion
    {
        
        private ClanData[] clansData;
        private Dictionary<string, int> indexByPlayerId = new Dictionary<string, int>();

        public PlayersClanDataUnion(JSONNode json)
        {
            ParseJson(json);
        }

        public ClanData this[string index] => GetClanDataByPlayerId(index);

        public void ParseJson(JSONNode json)
        {
            Reset();
            if (json == null) return;

            var dataArray = json["clans"].AsArray;
            clansData = new ClanData[dataArray.Count];
            for (int i = 0; i < clansData.Length; ++i) clansData[i] = new ClanData(dataArray[i]);

            var relationsNode = json["relations"];
            foreach (var kvp in relationsNode) indexByPlayerId[kvp.Key] = kvp.Value.AsInt;
        }

        public void Reset()
        {
            clansData = null;
            indexByPlayerId.Clear();
        }

        public ClanData GetClanDataByPlayerId(string playerId)
        {
            if (clansData == null) return null;
            if (!indexByPlayerId.ContainsKey(playerId)) return null;
            return clansData[indexByPlayerId[playerId]];
        }
    }

    public class ClanData
    {
        public readonly string id;
        public readonly string title;
        public readonly Icon icon; 

        public ClanData(JSONNode json)
        {
            this.id = json["clan_id"].Value;
            this.title = json["title"].Value;
            icon = Icon.Build(json["icon"]);
        }
    }


}