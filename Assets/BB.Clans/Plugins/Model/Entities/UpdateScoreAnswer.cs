using SimpleJSON;

namespace BlackBears.Clans.Model
{

    public class UpdateScoreAnswer
    {

        public readonly bool scoreUpdated;
        public readonly double clanScore;

        public UpdateScoreAnswer(JSONNode data)
        {
            scoreUpdated = data["score_updated"].AsBool;
            clanScore = data["clan_score"].AsDouble;
        }

    }

}