namespace BlackBears.Clans.Model
{

    internal enum ClanRole
    {
        None, Member, Moderator, Administrator
    }

    internal static class ClanRoleUtils
    {
        internal static bool IsClanMember(this ClanRole role) { return (int)role >= (int)ClanRole.Member; }
        internal static bool IsModerator(this ClanRole role) { return (int)role >= (int)ClanRole.Moderator; }
        internal static bool IsAdministrator(this ClanRole role) { return role == ClanRole.Administrator; }

        /// <summary>
        /// Получаем роль в клане на уровень выше, но не выше модератора
        /// </summary>
        /// <param name="role">текущая роль</param>
        /// <returns></returns>
        internal static ClanRole Promote(this ClanRole role)
        {
            return (ClanRole)UnityEngine.Mathf.Min((int)role + 1, (int)ClanRole.Moderator);
        }

        internal static ClanRole Demote(this ClanRole role)
        {
            return (ClanRole)UnityEngine.Mathf.Max((int)role - 1, (int)ClanRole.Member);
        }

    }

}