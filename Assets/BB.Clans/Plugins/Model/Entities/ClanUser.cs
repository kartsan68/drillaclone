using System;
using SimpleJSON;

namespace BlackBears.Clans.Model
{

    public class ClanUser
    {

        private const string idKey = "player_id";
        private const string clanIdKey = "clan_id";
        private const string roleKey = "role";
        private const string joinDateKey = "date_clan_join";
        private const string onlineDateKey = "last_online_date";
        private const string scoreKey = "score";
        private const string donationCountKey = "donation_count";
        private const string donationsGivenKey = "donations_given";
        private const string donationsRequestedKey = "donations_requested";
        private const string pushesEnabledKey = "accept_pushes";
        private const string battleIdKey = "battle_id";
        private const string battleStartScoreKey = "battle_start_score";
        private const string battleScoreKey = "battle_score";

        internal string Id { get; private set; }
        internal string ClanId { get; private set; }
        internal ClanRole Role { get; private set; }
        internal long OnlineDate { get; private set; }
        internal long JoinDate { get; private set; }
        internal double Score { get; private set; }
        internal ulong DonationCount { get; private set; }
        internal long DonationsGiven { get; private set; }
        internal long DonationsRequested { get; private set; }
        internal bool PushesEnabled { get; set; }

        internal long BattleId { get; private set; }
        internal double BattleStartScore { get; private set; }
        internal double BattleScore { get; private set; }

        internal ClanUser(JSONNode userData)
        {
            FromJson(userData);
        }

        internal static string IdFromUserData(JSONNode userData)
        {
            if (userData == null) return null;
            return userData[idKey].Value;
        }

        internal static int SortById(ClanUser x, ClanUser y) => string.Compare(x?.Id, y?.Id);

        internal static int SortByScore(ClanUser x, ClanUser y)
        {
            if (x == null || y == null) return x == y ? 0 : (x == null ? -1 : 1);
            return x.Score < y.Score ? 1 : (x.Score > y.Score ? -1 : 0);
        }

        internal static int SortByBattleScore(ClanUser x, ClanUser y)
        {
            if (x == null || y == null) return x == y ? 0 : (x == null ? -1 : 1);
            if (x.BattleScore == y.BattleScore) return SortById(x, y);
            return x.BattleScore < y.BattleScore ? 1 : -1;
        }

        internal void Promote() { Role = Role.Promote(); }
        internal void Demote() { Role = Role.Demote(); }

        internal void FromJson(JSONNode userData)
        {
            if (userData == null || userData.Tag == JSONNodeType.None) return;

            Id = userData[idKey].GetStringOrDefault(Id);
            ClanId = userData[clanIdKey].GetStringOrDefault(ClanId);
            Role = (ClanRole)userData[roleKey].GetIntOrDefault((int)Role);
            OnlineDate = userData[onlineDateKey].GetLongOrDefault(OnlineDate);
            JoinDate = userData[joinDateKey].GetLongOrDefault(JoinDate);
            Score = userData[scoreKey].GetDoubleOrDefault(Score);

            DonationCount = userData[donationCountKey].GetULongOrDefault(DonationCount);
            DonationsGiven = userData[donationsGivenKey].GetLongOrDefault(DonationsGiven);
            DonationsRequested = userData[donationsRequestedKey].GetLongOrDefault(DonationsRequested);
            PushesEnabled = userData[pushesEnabledKey].GetBoolOrDefault(PushesEnabled);

            BattleId = userData[battleIdKey].GetLongOrDefault(-1L);
            BattleStartScore = userData[battleStartScoreKey].GetDoubleOrDefault(0);
            BattleScore = userData[battleScoreKey].GetDoubleOrDefault(0);
        }

    }

}