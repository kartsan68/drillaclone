using BlackBears.Clans.Model.Helpers;

namespace BlackBears.Clans.Model
{

    internal struct ClanSearch
    {
        
        internal string search;
        internal string country;
        internal bool publicOnly;
        internal bool canJoin;

        internal ClanSearch(string search, string country, bool publicOnly, bool canJoin)
        {
            this.search = search;
            this.country = country;
            this.publicOnly = publicOnly;
            this.canJoin = canJoin;
        }

        internal bool CanSearch
        {
            get { return search != null && search.Length >= ValidateHelper.clanNameMinLength; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ClanSearch)) return false;
            return Equals((ClanSearch)obj);
        }

        public override int GetHashCode()
        {
            int hash = 23;
            hash *= country.GetHashCode() * 5;
            hash *= search.GetHashCode() * 3;
            hash *= publicOnly.GetHashCode() * 2;
            hash *= canJoin.GetHashCode();
            return hash;
        }

        internal bool Equals(ClanSearch other)
        {
            if (publicOnly != other.publicOnly) return false;
            if (canJoin != other.canJoin) return false;
            if (!string.Equals(search, other.search)) return false;
            if (!string.Equals(country, other.country)) return false;
            return true;
        }

    }

}