using BlackBears.Clans.Events;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using SimpleJSON;

namespace BlackBears.Clans
{

    internal class State : IState
    {

        private const string registeredKey = "rgstrd";
        private const string eventsCoreKey = "evntsCr";
        private const string warehouseKey = "wrhs";

        private Clan _clan;
        private long _unreadedMessageCount;

        internal long lastUpdateTime;

        public State()
        {
            Registered = false;
        }

        public bool Registered { get; set; }
        public bool Initialized { get; set; }
        public bool Validated { get; set; }
        public UserStatus UserStatus { get; set; }
        public ClanUser User { get; set; }
        public string KickerId { get; set; }
        public string ClanTitle { get; set; }
        public Icon ClanIcon { get; set; }
        public long LastFeedItemId { get; set; }
        public JSONNode Events { get; set; }

        public Clan Clan
        {
            get { return _clan; }
            set
            {
                UnsubscribeFromClan();
                _clan = value;
                SubscribeToClan();
                OnClanChanged.SafeInvoke(_clan);
            }
        }

        public bool IsUserInClan { get { return UserStatus.InClan() && User != null && Clan != null; } }

        public EventsCore EventsCore { get; internal set; }
        public Warehouse Warehouse { get; internal set; }

        /// <summary>
        /// Количество запросов на вступление в клан.
        /// </summary>
        /// <value>Возвращает количество запросов если уровень пользователя в клане moderator+, в противном случае 0.</value>
        public int JoinRequestsCount
        {
            get
            {
                if (!IsUserInClan || !User.Role.IsModerator()) return 0;
                return Clan.JoinRequests;
            }
        }

        public long UnreadedMessageCount
        {
            get { return _unreadedMessageCount; }
            internal set
            {
                if (_unreadedMessageCount == value) return;
                _unreadedMessageCount = value;
                OnNewMessagesCountChanged.SafeInvoke();
            }
        }

        public IGameAdapter Game { get; set; }
        public IAnalyticsAdapter Analytics { get; set; }
        public PlayerProfileCache ProfileCache { get; set; }
        public ClanCache ClanCache { get; set; }

        public event Block<Clan> OnClanChanged;

        public event Block OnNewMessagesCountChanged;
        public event Block OnJoinRequestsCountChanged;
        public event Block OnUserPerformImportantAction;

        public void FromJson(JSONNode json)
        {
            if (json == null || json.Tag != JSONNodeType.Object) return;
            Registered = json[registeredKey];
            EventsCore.FromJson(json[eventsCoreKey]);
            Warehouse.FromJson(json[warehouseKey]);
        }

        public bool IsUserClan(Clan clan)
        {
            if (clan == null || this.Clan == null) return false;
            return string.Equals(clan.Id, this.Clan.Id);
        }

        public bool IsUserClan(string clanId)
        {
            if (this.Clan == null) return false;
            return string.Equals(clanId, this.Clan.Id);
        }

        public bool IsCurrentUser(ClanUser user)
        {
            if (user == null || this.User == null) return false;
            return string.Equals(user.Id, this.User.Id);
        }

        internal void UserPerformImportantAction() => OnUserPerformImportantAction.SafeInvoke();

        internal JSONNode ToJSon()
        {
            var json = new JSONObject();
            json[registeredKey] = Registered;
            json[eventsCoreKey] = EventsCore.ToJson();
            json[warehouseKey] = Warehouse.ToJson();

            return json;
        }

        private void SubscribeToClan()
        {
            if (Clan == null) return;
            UnityEngine.Debug.Log($"JoinRequests: {Clan.JoinRequests}");
            Clan.OnJoinRequestsCountChanged += OnRequestsCountChanged;
            OnRequestsCountChanged();
        }

        private void UnsubscribeFromClan()
        {
            if (Clan != null) Clan.OnJoinRequestsCountChanged -= OnRequestsCountChanged;
        }

        private void OnRequestsCountChanged()
        {
            if (!IsUserInClan || !User.Role.IsModerator()) return;

            OnJoinRequestsCountChanged.SafeInvoke();
        }

    }

}