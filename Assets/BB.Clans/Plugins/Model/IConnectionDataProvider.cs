using SimpleJSON;

namespace BlackBears.Clans
{

    internal interface IConnectionDataProvider
    {

        string Server { get; }
        string Secret { get; }
        string PlayerKey { get; }
        string ApiVersion { get; }

    }

}