using System;

namespace BlackBears.Clans.Model.Helpers
{

    internal static class ValidateHelper
    {

        internal const int nicknameMinLength = 3;
        internal const int nicknameMaxLength = 15;
        internal const int nicknameMaxLengthAlert = 2;

        internal const int clanNameMinLength = 3;
        internal const int clanNameMaxLength = 25;
        internal const int clanDescMaxLength = 255;

        internal static string GetPlayerNameValidationError(string nickname)
        {
            if (string.IsNullOrEmpty(nickname) || nickname.Length < nicknameMinLength)
                return LocalizationKeys.errorShortNickname;

            return null;
        }

        internal static string GetClanNameValidationError(string clanName)
        {
            if (string.IsNullOrEmpty(clanName) || clanName.Length < clanNameMinLength)
                return  LocalizationKeys.errorShortClanname;
            if (clanName.Length > clanNameMaxLength)
                return LocalizationKeys.errorLongClanname;

            return null;
        }

        internal static string PlayerIdToTempNick(string playerId) 
        {
            var defaultNick = ClansLocalization.Get(LocalizationKeys.defaultNickname);
            return $"{defaultNick}#{playerId}";
        }

        internal static string ToMessageDate(this DateTime dateTime) { return dateTime.ToString("hh:mm:ss MM.dd"); }

        internal static string PlaceToString(long place)
        {
            return place > 0 ? string.Format("#{0}", place) : "#?";
        }

        internal static string ValidateCountry(string country)
        {
            return string.IsNullOrEmpty(country) ? "unk" : country;
        }

        internal static string FormatClanTag(string tag)
        {
            if (string.IsNullOrEmpty(tag)) return tag;
            if (tag[0] == '#') return tag;
            return string.Format("#{0}", tag);
        }

    }

}