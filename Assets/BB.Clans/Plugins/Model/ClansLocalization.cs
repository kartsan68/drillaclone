using System;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Localization;

namespace BlackBears.Clans
{

    internal class ClansLocalization
    {

        private static LocalizationFeature localization;

        private static LocalizationFeature Localization 
        {
            get
            {
                if (localization != null) return localization;
                localization = BBGC.Features.GetFeature<LocalizationFeature>();
                return localization;
            }
        }

        internal static string Get(string text, Modificator modificator = Modificator.DontModify)
        {
            var l = Localization;
            return l != null ? l.Get(text, modificator) : text;
        }

        internal static string Get(string text, long count, Modificator modificator = Modificator.DontModify)
        {
            var l = Localization;
            return l != null ? l.Get(text, count, modificator) : text;
        }

    }

}