using BlackBears.Clans.Events;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using SimpleJSON;

namespace BlackBears.Clans
{
    
    internal interface IState
    {
        
        bool Registered { get; }
        bool Initialized { get; }
        bool Validated { get; }

        UserStatus UserStatus { get; }
        ClanUser User { get; }
        string KickerId { get; }
        string ClanTitle { get; }
        Icon ClanIcon { get; }
        Clan Clan { get; }
        long LastFeedItemId { get; }
        bool IsUserInClan { get; }
        IGameAdapter Game { get; }
        IAnalyticsAdapter Analytics { get; }
        EventsCore EventsCore { get; }
        Warehouse Warehouse { get; }

        PlayerProfileCache ProfileCache { get; }
        ClanCache ClanCache { get; }

        int JoinRequestsCount { get; }
        long UnreadedMessageCount { get; }
        JSONNode Events { get; }

        bool IsUserClan(Clan clan);
        bool IsUserClan(string clanId);
        bool IsCurrentUser(ClanUser user);
        
        event Block<Clan> OnClanChanged;

        event Block OnNewMessagesCountChanged;
        event Block OnJoinRequestsCountChanged;
        event Block OnUserPerformImportantAction;

    }

}