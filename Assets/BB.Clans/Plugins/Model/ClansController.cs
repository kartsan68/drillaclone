using System;
using System.Collections.Generic;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Controller.Panels;
using BlackBears.Clans.Controller.Panels.PlayerRegister;
using UnityEngine;

using UObject = UnityEngine.Object;

namespace BlackBears.Clans.Controller
{

    internal class ClansController
    {

        private readonly ClanCore core;
        private readonly ClanFullSettings settings;

        private bool isShowed;
        private ClanPanel currentPanel;
        private RectTransform root;

        internal bool IsShowed
        {
            get { return isShowed; }
            private set
            {
                if (isShowed == value) return;
                isShowed = value;
                OnShowStateChanged.SafeInvoke(value);
            }
        }

        internal event Block<bool> OnShowStateChanged;

        internal ClansController(ClanCore core, ClanFullSettings settings)
        {
            this.core = core;
            this.settings = settings;
        }

        internal IState State { private get; set; }

        private ClansConfiguration Config { get { return settings.configuration; } }

        internal void Open(RectTransform root)
        {
            if (IsShowed) return;
            IsShowed = true;
            this.root = root;
            if (State.Registered) OpenClans();
            else OpenRegistration();
        }

        internal void OpenFragment(RectTransform root, FragmentKind kind)
        {
            if (IsShowed) return;
            IsShowed = true;
            this.root = root;

            OpenFragment(kind);
        }

        internal void OpenFragment(RectTransform root, string fragmentKey, Dictionary<string, object> data, bool checkShowed = true)
        {
            if (checkShowed && IsShowed) return;
            IsShowed = true;
            this.root = root;

            OpenFragment(fragmentKey, data);
        }

        internal void PanelClosed(ClanPanel panel)
        {
            if (!IsShowed || panel == null || currentPanel != panel) return;
            currentPanel = null;
            IsShowed = false;
            root = null;
            ResourceUnloadManager.RequestUnload();
        }

        internal void SwitchRegisterToClans()
        {
            if (currentPanel == null || !(currentPanel is PlayerRegisterClanPanel) || !State.Registered) return;
            var panel = currentPanel;
            OpenClans();
            panel.Close();
        }

        private void OpenRegistration()
        {
            currentPanel = UObject.Instantiate(Config.Panels.PlayerRegister, root);
            currentPanel.Inject(core, this, settings);
            currentPanel.Open();
        }

        private void OpenClans()
        {
            currentPanel = UObject.Instantiate(Config.Panels.Landing, root);
            currentPanel.Inject(core, this, settings);
            currentPanel.Open();
        }

        private void OpenFragment(FragmentKind kind)
        {
            var panel = UObject.Instantiate(Config.Panels.FragmentsPanel, root);
            panel.Inject(core, this, settings);
            panel.ShowFragment(kind);

            currentPanel = panel;
        }

        private void OpenFragment(string fragmentKey, Dictionary<string, object> data)
        {
            var panel = UObject.Instantiate(Config.Panels.FragmentsPanel, root);
            panel.Inject(core, this, settings);
            panel.ShowCustomFragment(fragmentKey, data);

            currentPanel = panel;
        }

    }

}