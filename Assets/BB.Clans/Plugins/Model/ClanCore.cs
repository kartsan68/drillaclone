using System;
using System.Collections.Generic;
using System.Linq;
using BlackBears.Clans.Analytics;
using BlackBears.Clans.Controller;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.GameCore.Features.GameServer;
using BlackBears.GameCore.Features.JellyB;
using BlackBears.GameCore.Features.Referals;
using BlackBears.GameCore.Features.Save;
using BlackBears.Referals;
using SimpleJSON;
using UnityEngine;

using Logger = BlackBears.GameCore.Logger;

namespace BlackBears.Clans
{

    public class ClanCore : IDisposable
    {

        public enum UpdateImportance { Default, Important, Force }

        private const string logTag = "ClanCore";

        private static ClanCore instance;

        private ChronometerFeature chronometer;

        private State state;
        private ClansController controller;
        private Networking.ServerConnection connection;
        private ClanFullSettings settings;

        public bool IsClansShowed => controller.IsShowed;
        public event Block<bool> OnClansShowStateChanged
        {
            add { controller.OnShowStateChanged += value; }
            remove { controller.OnShowStateChanged -= value; }
        }

        public Networking.ServerConnection Connection => connection;

        public PredefinedIconsContainer ProfileIcons => settings.configuration.ProfileIcons;

        private ClanCore(ClanFullSettings settings)
        {
            chronometer = BBGC.Features.Chronometer();

            state = new State();
            this.settings = settings;

            connection = new Networking.ServerConnection(settings, BBGC.Features.GetFeature<JellyBFeature>());
            controller = new ClansController(this, settings);
            state.Warehouse = new ResourceShare.Warehouse(this, chronometer, settings.parameters.warehouse);

            connection.State = state;
            controller.State = state;

            state.EventsCore = new Events.EventsCore(this, settings, connection);
        }

        public bool Initialized { get { return state != null && state.Initialized; } }

        internal IState State { get { return state; } }
        internal ClanParams Params { get { return settings.parameters; } }

        public static ClanCore Init(ClanFullSettings settings)
        {
            if (instance == null) instance = new ClanCore(settings);
            else Logger.Warning(logTag, "'ClanCore.Init' called twice.");

            return instance;
        }

        public void InitState(JSONNode save) => state.FromJson(save);

        public void Dispose()
        {
            instance = null;
        }

        /// <summary>
        /// Вызывать не чаще раза в 5 минут.
        /// </summary>
        public void CheckCounters() { ValidatePlayer(true, null, null); }

        public void Show(RectTransform root) => controller.Open(root);
        public void ShowFragment(RectTransform root, FragmentKind kind) => controller.OpenFragment(root, kind);

        public void ShowFragment(RectTransform root, string fragmentKey, Dictionary<string, object> data, bool checkShowed = true)
        {
            controller.OpenFragment(root, fragmentKey, data, checkShowed);
        }

        public JSONNode ToJson() => state.ToJSon();

        public void UpdateScore(UpdateImportance importance)
        {
            var actualTime = chronometer.ActualTime;
            var lastUpdateTime = state.lastUpdateTime;
            float delay = 0f;
            switch (importance)
            {
                case UpdateImportance.Default: delay = Params.updateRequestDelay; break;
                case UpdateImportance.Important: delay = Params.updateRequestImportantDelay; break;
            }

            var diff = actualTime - lastUpdateTime;
            if (diff < delay) return;
            UpdateScore(actualTime, null, null);
        }

        internal void ConnectWithGame(IGameAdapter gameAdapter, IAnalyticsAdapter analyticsAdapter)
        {
            state.Game = gameAdapter;
            state.Analytics = analyticsAdapter;
            state.ProfileCache = new PlayerProfileCache(this, BBGC.Features.GetFeature<GameServerFeature>());
            state.ClanCache = new ClanCache(this);
            state.Warehouse.OnGameConnected();

            connection.PlayerKeyAcquired(gameAdapter.PlayerKey);
            if (state.Registered) FullStateInitialize(false, null, null);
        }

        internal void RegisterPlayer(string nickname, Icon inIcon, string country, Block onSuccess, FailBlock onFail)
        {
            if (state.Registered || state.Game == null)
            {
                onFail.SafeInvoke();
                return;
            }

            var icon = inIcon ?? Icon.CreateDefaultIcon();
            state.Game.RegisterPlayer(nickname, icon.ToJson().ToString(), country, () =>
            {
                state.Registered = true;
                state.ProfileCache.LocalPlayer.ResetIcon();
                FullStateInitialize(true, onSuccess, (c, m) =>
                {
                    //TODO: Show notification
                    Debug.LogFormat("RegisterPlayer: {0}; {1}", c, m);
                    onSuccess.SafeInvoke();
                });
            }, onFail);
        }

        internal void UpdatePlayerRequest(string nickname, Icon icon, string country, Block onSuccess, FailBlock onFail)
        {
            if (!state.Registered || state.Game == null)
            {
                onFail.SafeInvoke();
                return;
            }

            state.Game.PrepareForSpendHardCurrency(Params.updatePlayerCost,
                () => UpdatePlayer(nickname, icon, country, onSuccess, onFail), onFail);
        }

        private void UpdatePlayer(string nickname, Icon inIcon, string country, Block onSuccess, FailBlock onFail)
        {
            var icon = inIcon ?? new PredefinedIcon();
            state.Game.UpdatePlayer(nickname, icon.ToJson().ToString(), country, () =>
            {
                state.Game.SpendHardCurrency(Params.updatePlayerCost, PlaceOfSpendResources.UpdatePlayer);
                state.ProfileCache.LocalPlayer.ResetIcon();
                onSuccess.SafeInvoke();
            }, onFail);
        }

        internal void FullStateInitialize(bool force, Block onSuccess, FailBlock onFail)
        {
            if (!force && state.Initialized) return;

            ValidatePlayer(true, onSuccess, onFail);
        }

        internal void Validate(Block onSuccess, FailBlock onFail)
        {
            ValidatePlayer(false, onSuccess, onFail);
        }

        internal void GetClan(Clan clan, Block<Clan> onSuccess, FailBlock onFail)
        {
            if (clan == null)
            {
                onFail.SafeInvoke();
                return;
            }
            connection.GetClan(clan, onSuccess, onFail);
        }

        internal void GetIncomingPlayers(Block<ClanUser[]> onSuccess, FailBlock onFail)
        {
            if (!state.IsUserInClan || !state.User.Role.IsModerator())
            {
                onFail.SafeInvoke();
                return;
            }
            connection.GetIncomingPlayers(state.ProfileCache.LocalPlayer.Id, state.Clan, onSuccess, onFail);
        }

        internal void GetClanShortDataByPlayerIds(List<string> playerIds,
            Block<PlayersClanDataUnion> onSuccess, FailBlock onFail, PlayersClanDataUnion dataUnion)
        {
            connection.GetClanShortDataByPlayerIds(playerIds, onSuccess, onFail, dataUnion);
        }

        //TODO: Делать это через кеш кланов
        internal void GetClanAndValidateProfiles(string clanId, Block<Clan, bool> onSucces, FailBlock onFail, bool onlyClanMembers = false)
        {
            GetClanAndValidateProfiles(new Clan(clanId), onSucces, onFail, onlyClanMembers);
        }

        //TODO: Делать это через кеш кланов
        internal void GetClanAndValidateProfiles(Clan clan, Block<Clan, bool> onSuccess, FailBlock onFail, bool onlyClanMembers = false)
        {
            GetClan(clan, c => ValidateClanUsers(c, onlyClanMembers, onSuccess), onFail);
        }

        internal void CreateClanRequest(ClanEdit edit, Block onSuccess, FailBlock onFail)
        {
            if (state.UserStatus.InClan() || state.Clan != null)
            {
                onFail.SafeInvoke();
                return;
            }

            state.Game.PrepareForSpendHardCurrency(Params.createClanCost,
                () => CreateClan(edit, onSuccess, onFail), onFail);
        }

        internal void UpdateClanRequest(ClanEdit edit, Block onSuccess, FailBlock onFail)
        {
            if (!state.UserStatus.InClan() || state.Clan == null || state.User == null || !state.User.Role.IsModerator())
            {
                onFail.SafeInvoke();
                return;
            }

            state.Game.PrepareForSpendHardCurrency(Params.updateClanCost,
                () => UpdateClan(edit, onSuccess, onFail), onFail);
        }

        internal void JoinClan(Clan clan, Block onSuccess, FailBlock onFail)
        {
            if (clan == null || state.UserStatus.InClan())
            {
                onFail.SafeInvoke();
                return;
            }

            var gameData = state.Game.GenerateAdditionalScoreData();
            connection.JoinToClan(state.Game.PlayerId, clan.Id,
            state.Game.Score, gameData, joinAnswer =>
            {
                if (joinAnswer.success)
                {
                    LoadFullUserInfo(clan, () =>
                    {
                        clan.AddClanUser(state.User);
                        onSuccess.SafeInvoke();
                    }, onFail);
                    state.Analytics.OnJoinToClan(clan.Id, clan.Title);
                }
                else
                {
                    onFail.SafeInvoke();
                }
            }, onFail);
        }

        internal void LeaveClan(Block onSuccess, FailBlock onFail)
        {
            if (!state.UserStatus.InClan() || state.Clan == null)
            {
                onFail.SafeInvoke();
                return;
            }

            connection.LeaveClan(state.Game.PlayerId, state.Clan.Id, successAnswer =>
            {
                if (successAnswer.success)
                {
                    if (state.User.Role.IsAdministrator())
                    {
                        //TODO: вынести в отдельный метод + зачищать lastReadedMessage и сообщения
                        state.UserStatus = UserStatus.ClanDisbanded;
                        state.KickerId = state.Game.PlayerId;
                        state.ClanTitle = state.Clan.Title;
                        state.ClanIcon = state.Clan.Icon;
                        state.Analytics.OnDisbandClan(state.Clan.Id, state.Clan.Title);
                    }
                    else
                    {
                        state.UserStatus = UserStatus.Left;
                        state.Analytics.OnLeaveClan(state.Clan.Id, state.Clan.Title);
                    }

                    state.Clan = null;
                    state.User = null;
                    state.EventsCore.OnClanLeaved();

                    onSuccess.SafeInvoke();
                }
                else
                {
                    state.Validated = false;
                    onFail.SafeInvoke();
                }
            }, onFail);
        }

        internal void SwitchPushes(bool enabled, Block onSuccess, FailBlock onFail)
        {
            if (!state.IsUserInClan)
            {
                onFail.SafeInvoke();
                return;
            }

            connection.SwitchPushes(state.Game.PlayerId, enabled, answer =>
            {
                if (answer.success)
                {
                    state.User.PushesEnabled = !state.User.PushesEnabled;
                    onSuccess.SafeInvoke();
                }
                else
                {
                    onFail.SafeInvoke();
                }
            }, onFail);
        }

        internal void DeleteClan(Block onSuccess, FailBlock onFail)
        {
            if (!state.IsUserInClan || !state.User.Role.IsAdministrator())
            {
                onFail.SafeInvoke();
                return;
            }

            connection.DeleteClan(state.Clan.Id, state.Game.PlayerId, answer =>
            {
                if (answer.success)
                {
                    state.UserStatus = UserStatus.ClanDisbanded;
                    state.KickerId = state.Game.PlayerId;
                    state.ClanTitle = state.Clan.Title;
                    state.ClanIcon = state.Clan.Icon;

                    state.Analytics.OnDisbandClan(state.Clan.Id, state.Clan.Title);

                    state.Clan = null;
                    state.User = null;
                    state.EventsCore.OnClanLeaved();

                    onSuccess.SafeInvoke();
                }
                else
                {
                    onFail.SafeInvoke();
                }
            }, onFail);
        }

        internal void GetWorldTop(int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            string clanId = null;
            if (state.Clan != null) clanId = state.Clan.Id;
            connection.GetWorldTop(clanId, page, onSuccess, onFail);
        }

        internal void GetLocalTop(int page, string country, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            string clanId = null;
            if (state.Clan != null) clanId = state.Clan.Id;
            connection.GetLocalTop(clanId, country, page, onSuccess, onFail);
        }

        internal void GetNewbiesTop(int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            string clanId = null;
            if (state.Clan != null) clanId = state.Clan.Id;
            connection.GetNewbiesTop(clanId, page, onSuccess, onFail);
        }

        internal void GetRecommendedClans(Block<TopResponse> onSuccess, FailBlock onFail)
        {
            var player = state.ProfileCache.LocalPlayer;
            var clanId = state.Clan?.Id;
            connection.GetRecommendedClans(player.Id, clanId, ValidateHelper.ValidateCountry(player.Country),
                player.Score, onSuccess, onFail);
        }

        internal void SearchClan(ClanSearch search, int page, Block<List<Clan>> onSuccess, FailBlock onFail)
        {
            var player = state.ProfileCache.LocalPlayer;
            connection.SearchClan(search, player.Score, page, onSuccess, onFail);
        }

        internal void CheckIncomingDonations(Block<List<ClanResourceRequest>> onSuccess, FailBlock onFail)
        {
            var player = state.ProfileCache.LocalPlayer;
            connection.CheckResources(player.Id, onSuccess, onFail);
        }

        internal void GrabIncomingDonations(List<string> donationIds, Block<NetworkSuccessAnswer> onComplete)
        {
            if (donationIds == null || donationIds.Count == 0)
            {
                onComplete.SafeInvoke(NetworkSuccessAnswer.NotSuccess);
            }
            else
            {
                var player = state.ProfileCache.LocalPlayer;
                connection.GrabIncomingDonations(player.Id, donationIds, onComplete,
                (c, m) =>
                {
                    Debug.Log($"GrabIncomingDonations: {c}; {m}");
                    onComplete.SafeInvoke(NetworkSuccessAnswer.NotSuccess);
                });
            }
        }

        internal void RequestResource(int resourceType, long resourceId, double amount, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            var player = state.ProfileCache.LocalPlayer;
            connection.RequestResource(player.Id, resourceType, resourceId, amount, onSuccess, onFail);
        }

        internal void SendResource(string requestId, int resourceType, long resourceId, double value,
            Block<ResourceTransition> onSuccess, FailBlock onFail)
        {
            var player = state.ProfileCache.LocalPlayer;
            connection.SendResource(player.Id, requestId, resourceType, resourceId, value, onSuccess, onFail);
        }

        private void UpdateScore(Block onSuccess = null, FailBlock onFail = null) => UpdateScore(chronometer.ActualTime, onSuccess, onFail);

        private void UpdateScore(long actualTime, Block onSuccess, FailBlock onFail)
        {
            state.lastUpdateTime = actualTime;

            if (!state.IsUserInClan || !state.User.Role.IsClanMember())
            {
                onFail.SafeInvoke();
                return;
            }

            double score = state.ProfileCache.LocalPlayer.Score;
            var gameData = state.Game.GenerateAdditionalScoreData();
            connection.UpdateScore(state.Game.PlayerId, state.Clan.Id,
            score, gameData, answer =>
            {
                state.Clan.UpdateScore(answer);
                onSuccess.SafeInvoke();
            }, onFail);
        }

        internal void GetTimeline(int page, Block<List<FeedItem>> onSuccess, FailBlock onFail)
        {
            if (!state.UserStatus.InClan() || (int)state.User.Role < (int)ClanRole.Member)
            {
                onFail.SafeInvoke();
                return;
            }
            connection.GetTimeline(state.Game.PlayerId, state.Clan.Id, page, items =>
            {
                var maxId = items.Count > 0 ? items.Max(item => item.id) : 0L;
                state.LastFeedItemId = Math.Max(maxId, state.LastFeedItemId);
                if (page <= 1) state.UnreadedMessageCount = 0;
                onSuccess.SafeInvoke(items);
            }, (c, m) =>
            {
                if (c == NetworkInvalidateCodes.loadFeedInvalidateCode) state.Validated = false;
                onFail.SafeInvoke(c, m);
            });
        }

        internal void SendMessage(string message, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (string.IsNullOrEmpty(message) || !state.UserStatus.InClan() || (int)state.User.Role < (int)ClanRole.Member)
            {
                onFail.SafeInvoke();
                return;
            }
            connection.SendMessage(state.Game.PlayerId, message,
                answer =>
                {
                    onSuccess.SafeInvoke(answer);
                    state.Analytics.AddClansMessageToUserProfile();
                },
                (c, m) =>
                {
                    if (c == NetworkInvalidateCodes.sendMessageInvalidateCode) state.Validated = false;
                    onFail.SafeInvoke(c, m);
                });
        }

        internal void PromotePlayer(string playerId, Block onSuccess, FailBlock onFail)
        {
            if (!state.IsUserInClan || !state.User.Role.IsModerator())
            {
                onFail.SafeInvoke();
                return;
            }

            connection.PromoteUser(playerId, state.Game.PlayerId, state.Clan.Id, answer =>
            {
                if (answer.success)
                {
                    var user = state.Clan.GetUser(playerId);
                    if (user != null) user.Promote();
                    state.Clan.RecalculateJoinRequests();
                    onSuccess.SafeInvoke();
                    state.UserPerformImportantAction();
                }
                else onFail.SafeInvoke();
            }, onFail);
        }

        internal void DemotePlayer(string playerId, Block onSuccess, FailBlock onFail)
        {
            if (!state.IsUserInClan || !state.User.Role.IsModerator())
            {
                onFail.SafeInvoke();
                return;
            }

            connection.DemoteUser(playerId, state.Game.PlayerId, state.Clan.Id, answer =>
            {
                var user = state.Clan.GetUser(playerId);
                if (user != null) user.Demote();
                state.Clan.RecalculateJoinRequests();
                onSuccess.SafeInvoke();
                state.UserPerformImportantAction();
            }, onFail);
        }

        internal void KickFromClan(string playerId, Block onSuccess, FailBlock onFail)
        {
            if (!state.IsUserInClan || !state.User.Role.IsModerator())
            {
                onFail.SafeInvoke();
                return;
            }

            connection.KickFromClan(playerId, state.Game.PlayerId, state.Clan.Id, answer =>
            {
                if (answer.success)
                {
                    state.Clan.RemoveUser(playerId);
                    onSuccess.SafeInvoke();
                    state.UserPerformImportantAction();
                }
                else
                {
                    onFail.SafeInvoke();
                }
            }, onFail);
        }

        internal void TakeInviteReward(RewardedItem item, Block<RewardTakeAnswer> onSuccess, FailBlock onFail)
        {
            var feature = BBGC.Features.GetFeature<ReferalsFeature>();
            if (feature == null)
            {
                onFail.SafeInvoke(-1, "Referals not available");
                return;
            }


            feature.Referals.TakeReward(state.Game.PlayerId, item.InstallationId, answer =>
            {
                if (answer.rewardCount > 0)
                {
                    double reward = feature.Referals.CalculateReward(answer.rewardCount);
                    state.Game.AddHardCurrency(reward);
                    onSuccess.SafeInvoke(answer);
                }
            }, onFail);
        }

        private void ValidatePlayer(bool forceCheck, Block onSuccess, FailBlock onFail)
        {
            string playerId = state.Game.PlayerId;
            connection.GetClanUser(playerId, playerId, answer =>
            {
                bool inClan = answer.status.InClan();
                inClan = inClan && answer.user != null;
                inClan = inClan && !string.IsNullOrEmpty(answer.user.ClanId);

                if (inClan)
                {
                    GetClanAndValidateProfiles(answer.user.ClanId, (clan, validated) =>
                    {
                        connection.GetUserMainInfo(state.Game.PlayerId, true,
                            info => FullInitializeFinished(answer, clan, info, onSuccess), onFail);
                    }, onFail);
                }
                else
                {
                    FullInitializeFinished(answer, null, null, onSuccess);
                }
            }, onFail);
        }

        private void CreateClan(ClanEdit edit, Block onSuccess, FailBlock onFail)
        {
            var gameData = state.Game.GenerateAdditionalDataForCreateClan();
            connection.CreateClan(edit, gameData, clan =>
            {
                state.Game.SpendHardCurrency(Params.createClanCost, PlaceOfSpendResources.CreateClan);
                LoadFullUserInfo(clan, () =>
                {
                    clan.AddClanUser(state.User);
                    onSuccess.SafeInvoke();
                }, onFail);
                UpdateScore();
                state.Analytics.OnClanCreated(clan.Id, clan.Title);
            }, onFail);
        }

        private void UpdateClan(ClanEdit edit, Block onSuccess, FailBlock onFail)
        {
            connection.UdpateClan(edit, state.Clan, _ =>
            {
                state.Game.SpendHardCurrency(Params.updateClanCost, PlaceOfSpendResources.UpdateClan);
                onSuccess.SafeInvoke();
                UpdateScore();
                state.Analytics.OnClanUpdated(state.Clan.Id, state.Clan.Title);
            }, onFail);
        }

        private void ValidateClanUsers(Clan clan, bool onlyClanMembers, Block<Clan, bool> onSuccess)
        {
            state.ProfileCache.LoadPlayersForClanAsync(clan, onlyClanMembers,
                () => onSuccess.SafeInvoke(clan, true),
                (c, m) =>
                {
                    Debug.Log($"ValidateClanUsers: {c}; {m}");
                    onSuccess.SafeInvoke(clan, false);
                });
        }

        private void LoadFullUserInfo(Clan clan, Block onSuccess, FailBlock onFail)
        {
            string playerId = state.Game.PlayerId;
            connection.GetClanUser(playerId, playerId, userAnswer =>
            {
                connection.GetUserMainInfo(state.Game.PlayerId, true, info =>
                {
                    state.Warehouse.TryToValidateWithServer();
                    UpdateFullUserInfo(userAnswer, clan, info);
                    onSuccess.SafeInvoke();
                }, onFail);
            }, onFail);
        }

        private void FullInitializeFinished(ClanUserAnswer answer, Clan clan, PlayerMainInfo info, Block onSuccess)
        {
            state.Initialized = true;
            state.Validated = true;
            state.Warehouse.TryToValidateWithServer();
            UpdateFullUserInfo(answer, clan, info);
            onSuccess.SafeInvoke();

            UpdateScore(UpdateImportance.Important);
        }

        private void UpdateFullUserInfo(ClanUserAnswer answer, Clan clan, PlayerMainInfo info)
        {
            state.UserStatus = answer.status;
            state.User = answer.user;
            state.KickerId = answer.kickerId;
            state.ClanTitle = answer.clanTitle;
            state.ClanIcon = answer.clanIcon;
            state.Clan = clan;

            long battleId = state.User != null ? state.User.BattleId : -1L;
            long nextBattleTime = info != null ? info.nextBattleTime : -1L;
            bool haveBattle = info != null ? info.haveBattle : false;
            state.EventsCore.UpdateBattleData(battleId, nextBattleTime, haveBattle);

            if (info != null)
            {
                state.LastFeedItemId = info.lastEntry;
                state.UnreadedMessageCount = info.unread;
                state.Events = info.events;
            }
            else
            {
                state.UnreadedMessageCount = 0;
            }
        }

    }

}