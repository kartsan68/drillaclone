using System;
using System.Collections.Generic;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore.Features.CoreDefaults;
using SimpleJSON;

namespace BlackBears.GameCore
{

    public partial interface IGameAdapter
    {
        void GiveClanBattleReward(int battlePosition);
        void GiveClanTopReward(int topPosition);
        
        JSONNode GenerateAdditionalScoreData();
        JSONNode GenerateAdditionalDataForCreateClan();
        List<GameResource> CreateResourceList();

        void GetInGameResourceDependInfo(int resType, long resId, int maxRequired, Block<ResourceDependInfo> onSuccess);

        void LeagueTakeReward(int playerLeague);

        bool IsAvailableResource(GameResource resource);
        bool IsAvailableResource(int resourceType, long resourceId);
        
        double GetRequestingAmountOfResources(GameResource resource);
        bool CanSpendResource(int resourceType, long resourceId, double amount);
        bool AddResource(int resourceType, long resourceId, double value);
        bool SpendResource(int resourceType, long resourceId, double amount);
    }

}