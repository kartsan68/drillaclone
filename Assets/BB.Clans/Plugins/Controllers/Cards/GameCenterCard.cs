using UnityEngine;

namespace BlackBears.Clans.Controller.Cards
{

    internal sealed class GameCenterCard : BaseCard
    {

        protected override void Start()
        {
            base.Start();
            
            #if UNITY_IOS
            gameObject.SetActive(Configuration.GameCenterEnabled);
            #else
            gameObject.SetActive(false);
            #endif
        }

        protected override void OnClick()
        {
            Debug.Log("Not supported yet.");
        }
        
    }

}