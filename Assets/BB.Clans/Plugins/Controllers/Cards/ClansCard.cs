using BlackBears.Clans.Model;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Cards
{
    internal sealed class ClansCard : BaseCard
    {

        protected override void Start()
        {
            base.Start();
            Core.State.OnClanChanged += OnClanChanged;
            OnClanChanged(Core.State.Clan);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Core.State.OnClanChanged -= OnClanChanged;
        }

        private void OnClanChanged(Clan clan)
        {
            var mark = Mark;
            if (mark != null) mark.EventsCount = (clan == null) ? 1 : 0;
        }

        protected override void OnClick()
        {
            FragmentsController.ShowClanTop();
        }
        
    }
}