using UnityEngine;

namespace BlackBears.Clans.Controller.Cards
{
    internal sealed class GPlayLeaderboardCard : BaseCard
    {

        protected override void Start()
        {
            base.Start();

#if UNITY_ANDROID
            gameObject.SetActive(Configuration.GPlayEnabled);
#else
            gameObject.SetActive(false);
#endif
        }

        protected override void OnClick()
        {
            Debug.Log("Not supported yet.");
        }

    }
}