using BlackBears.Clans.Controller.Fragments;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Cards
{

    internal abstract class BaseCard : MonoBehaviour 
    {

        [SerializeField] private Button button;

        private CardActivityMark _mark;

        public CardActivityMark Mark { get { return _mark ?? (_mark = GetComponent<CardActivityMark>()); } }
        
        protected ClanCore Core { get; private set; }
        protected ClansConfiguration Configuration { get; private set; }
        protected FragmentsController FragmentsController { get; private set; }

        protected bool Disposed { get; private set; }

        internal void Inject(ClanCore core, ClansConfiguration configuration, FragmentsController fragmentsController)
        {
            Core = core;
            Configuration = configuration;
            FragmentsController = fragmentsController;
        }

        internal virtual void CheckForUpdates() {}

        protected virtual void Start()
        {
            button.onClick.AddListener(OnClick);
        }

        protected virtual void OnDestroy()
        {
            Disposed = true;
        }

        protected abstract void OnClick();

    }

}