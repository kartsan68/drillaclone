using BlackBears.GameCore;
using BlackBears.GameCore.Features.Referals;
using BlackBears.Referals;
using UnityEngine;

namespace BlackBears.Clans.Controller.Cards
{
    internal sealed class FriendsCard : BaseCard
    {

        private ReferalsModule referals;

        private void Awake()
        {
            referals = BBGC.Features.GetFeature<ReferalsFeature>().Referals;
        }

        protected override void Start()
        {
            base.Start();
            CheckForUpdates();
        }

        internal override void CheckForUpdates()
        {
            base.CheckForUpdates();

            referals.LoadRewardInformation(Core.State.ProfileCache.LocalPlayer.Id,
                OnRewardLoaded, OnRewardLoadFailed);
        }

        protected override void OnClick()
        {
            FragmentsController.ShowInvites();
        }

        private void OnRewardLoaded(RewardInformation information)
        {
            if (Disposed) return;
            var mark = Mark;
            if (mark != null) mark.EventsCount = information.NotTakenCount;
        }

        private void OnRewardLoadFailed(long errorCode, string errorMessage)
        {
            if (Disposed) return;
            var mark = Mark;
            if (mark != null) mark.EventsCount = 0;
        }

    }
}