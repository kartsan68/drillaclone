using UnityEngine;

namespace BlackBears.Clans.Controller.Cards
{
    internal sealed class SupportCard : BaseCard
    {

        protected override void OnClick()
        {
            FragmentsController.ShowSupport();
        }
        
    }
}