﻿using System.Collections.Generic;
using BlackBears.Clans.Controller.Fragments;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Cards
{

    internal class CardsListController : MonoBehaviour
    {

        [SerializeField] private Button movePrevButton;
        [SerializeField] private Button moveNextButton;
        [SerializeField] private ScrollRect scroll;
        [SerializeField] private HorizontalLayoutGroup contentGroup;

        private ClanCore core;
        private ClansConfiguration configuration;
        private FragmentsController fragmentsController;

        private RectTransform scrollRt;
        private float prevScrollX;
        private List<BaseCard> cards = new List<BaseCard>();
        private List<CardActivityMark> marks = new List<CardActivityMark>();

        private int prevIndex = -1;
        private int nextIndex = -1;

        internal void Inject(ClanCore core, ClansConfiguration configuration, 
            FragmentsController fragmentsController)
        {
            this.core = core;
            this.configuration = configuration;
            this.fragmentsController = fragmentsController;
        }

        private void Start()
        {
            scrollRt = scroll.GetComponent<RectTransform>();
            prevScrollX = scroll.content.anchoredPosition.x;

            scroll.onValueChanged.AddListener(OnScroll);
            movePrevButton.onClick.AddListener(MovePreviousItem);
            moveNextButton.onClick.AddListener(MoveNextItem);

            cards.AddRange(GetComponentsInChildren<BaseCard>(true));
            foreach (var card in cards)
            {
                card.Inject(core, configuration, fragmentsController);

                var mark = card.Mark;
                if (mark != null)
                {
                    marks.Add(mark);
                    mark.OnActivityChange.AddListener(FullCheckMoveButtons);
                }
            }
            marks.Sort();
            FullCheckMoveButtons();
        }

        private void OnDestroy()
        {
            LeanTween.cancel(scroll.content.gameObject);
        }

        internal void OnCardAdded(List<BaseCard> inCards)
        {
            if (inCards == null) return;
            foreach (var card in inCards)
            {
                if (card == null || cards.Contains(card)) continue;
                card.Inject(core, configuration, fragmentsController);
                cards.Add(card);
                var mark = card.Mark;
                if (mark != null)
                {
                    marks.Add(mark);
                    mark.OnActivityChange.AddListener(FullCheckMoveButtons);
                }
            }
            marks.Sort();
            FullCheckMoveButtons();
        }

        internal void UpdateCards()
        {
            foreach (var card in cards) card.CheckForUpdates();
        }

        private void FullCheckMoveButtons()
        {
            var portWidth = scrollRt.rect.width;

            bool searchLeft = true;
            var prevIndex = -1;
            var nextIndex = -1;
            for (int i = 0; i < marks.Count; i++)
            {
                if (!marks[i].gameObject.activeSelf || !marks[i].MarkActive) continue;

                if (searchLeft)
                {
                    if (MarkOnLeftSide(marks[i].RectTransform, prevScrollX)) prevIndex = i;
                    else searchLeft = false;
                }

                if (!searchLeft)
                {
                    if (MarkOnRightSide(marks[i].RectTransform, prevScrollX, portWidth))
                    {
                        nextIndex = i;
                        break;
                    }
                }
            }

            this.prevIndex = prevIndex;
            this.nextIndex = nextIndex;

            UpdateButtons();
        }

        private void OnScroll(Vector2 _)
        {
            var newScrollX = scroll.content.anchoredPosition.x;
            var portWidth = scrollRt.rect.width;
            if (prevScrollX > newScrollX) OnScrollRight(newScrollX, portWidth);
            else OnScrollLeft(newScrollX, portWidth);
            prevScrollX = newScrollX;

            UpdateButtons();
        }

        private void OnScrollRight(float contentPos, float portWidth)
        {
            int i;
            if (nextIndex >= 0)
            {
                for (i = nextIndex; i < marks.Count; i++)
                {
                    var mark = marks[i];
                    if (!mark.gameObject.activeSelf || !mark.MarkActive) continue;
                    if (MarkOnRightSide(mark.RectTransform, contentPos, portWidth)) break;
                }
                nextIndex = i < marks.Count ? i : -1;
            }

            int prevStartIndex = prevIndex >= 0 ? prevIndex + 1 : 0;
            bool breakOnFirstCheck = prevIndex >= 0;
            int newPrevIndex = prevIndex;

            for (i = prevStartIndex; i < marks.Count; i++)
            {
                var mark = marks[i];
                if (!mark.gameObject.activeSelf || !mark.MarkActive) continue;
                if (MarkOnLeftSide(mark.RectTransform, contentPos)) newPrevIndex = i;
                if (breakOnFirstCheck) break;
            }
            prevIndex = newPrevIndex;
        }

        private void OnScrollLeft(float contentPos, float portWidth)
        {
            int i;
            if (prevIndex >= 0)
            {
                for (i = prevIndex; i >= 0; i--)
                {
                    var mark = marks[i];
                    if (!mark.gameObject.activeSelf || !mark.MarkActive) continue;
                    if (MarkOnLeftSide(mark.RectTransform, contentPos)) break;
                }
                prevIndex = i;
            }

            int nextStartIndex = (nextIndex > 0 ? nextIndex : marks.Count) - 1;
            bool breakOnFirstCheck = nextIndex > 0;
            int newNextIndex = nextIndex;

            for (i = nextStartIndex; i >= 0; i--)
            {
                var mark = marks[i];
                if (!mark.gameObject.activeSelf || !mark.MarkActive) continue;
                if (MarkOnRightSide(mark.RectTransform, contentPos, portWidth)) newNextIndex = i;
                if (breakOnFirstCheck) break;
            }
            nextIndex = newNextIndex;
        }

        private bool MarkOnLeftSide(RectTransform markRt, float contentPos)
        {
            float markPos = contentPos + markRt.anchoredPosition.x + (1 - markRt.pivot.x) * markRt.rect.width;
            return markPos < 0f;
        }

        private bool MarkOnRightSide(RectTransform markRt, float contentPos, float portWidth)
        {
            float markPos = contentPos + markRt.anchoredPosition.x - markRt.pivot.x * markRt.rect.width;
            return markPos > portWidth;
        }

        private void UpdateButtons()
        {
            movePrevButton.gameObject.SetActive(prevIndex >= 0);
            moveNextButton.gameObject.SetActive(nextIndex >= 0);
        }

        private void MovePreviousItem()
        {
            if (prevIndex < 0 || prevIndex >= marks.Count)
            {
                movePrevButton.gameObject.SetActive(false);
                return;
            }
            MoveToIndex(prevIndex);
        }

        private void MoveNextItem()
        {
            if (nextIndex < 0 || nextIndex >= marks.Count)
            {
                moveNextButton.gameObject.SetActive(false);
                return;
            }
            MoveToIndex(nextIndex);
        }

        private void MoveToIndex(int index)
        {
            LeanTween.cancel(scroll.content.gameObject);

            var mark = marks[index];
            var contentPos = scroll.content.anchoredPosition;
            var portWidth = scrollRt.rect.width;
            var markRt = mark.RectTransform;
            float stopPos = -markRt.anchoredPosition.x + markRt.pivot.x * markRt.rect.width
                + contentGroup.spacing / 2f;

            stopPos = Mathf.Clamp(stopPos, -scroll.content.rect.width + portWidth, 0f);
            LeanTween.moveAnchored(scroll.content,
                new Vector2(stopPos, contentPos.y), 0.2f).setEaseInOutQuad()
                .setOnComplete(() => scroll.velocity = Vector2.zero);
        }

    }

}