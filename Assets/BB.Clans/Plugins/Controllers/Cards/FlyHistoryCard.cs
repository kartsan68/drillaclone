using UnityEngine;

namespace BlackBears.Clans.Controller.Cards
{
    internal sealed class FlyHistoryCard : BaseCard
    {

        protected override void Start()
        {
            base.Start();
            gameObject.SetActive(false);
        }

        protected override void OnClick() 
        {
            Debug.Log("Not supported yet :(");
        }

    }
}