﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Cards
{

    internal class CardActivityMark : MonoBehaviour, System.IComparable<CardActivityMark>
    {

        [SerializeField] private GameObject mark;
        [SerializeField] private Text counter;

        private bool started;

        private RectTransform _rectTransform;
        private int _eventsCount;

        private UnityEvent onActivityChange = new UnityEvent();

        public RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }

        internal int EventsCount
        {
            get { return _eventsCount; }
            set
            {
                if(_eventsCount == value) return;
                _eventsCount = value;
                
                if (!started) return;
                UpdateViews();
                onActivityChange.Invoke();
            }
        }

        internal bool MarkActive => _eventsCount > 0;

        internal UnityEvent OnActivityChange { get { return onActivityChange; } }

        public int CompareTo(CardActivityMark other)
        {
			return RectTransform.GetSiblingIndex() - other.RectTransform.GetSiblingIndex();
        }

        private void Start()
        {
            started = true;
            UpdateViews();
        }

        private void UpdateViews()
        {
            mark.SetActive(MarkActive);
            if (!MarkActive || counter == null) return;

            if (EventsCount < 10) counter.text = EventsCount.ToString();
            else counter.text = "!";
        }

    }

}