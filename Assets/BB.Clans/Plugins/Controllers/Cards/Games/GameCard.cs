using System;
using BlackBears.GameCore.Features.BBGames;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Cards.Games
{
    internal class GameCard : BaseCard
    {

        [SerializeField] private RawImage icon;
        [SerializeField] private CanvasGroup loader;

        private BannerData bannerData;

        private bool destroyed;

        protected override void OnDestroy()
        {
            base.OnDestroy();
            LeanTween.cancel(icon.gameObject);
            LeanTween.cancel(loader.gameObject);
            destroyed = true;
        }

        internal void SetData(BannerData bannerData, BBGamesFeature games)
        {
            this.bannerData = bannerData;
            games.LoadBanner(bannerData, OnTextureLoaded, null);
            if (bannerData == null) return;
            Mark.EventsCount = (!bannerData.IsOld) ? 1 : 0;
        }

        private void OnTextureLoaded(Texture texture)
        {
            if (Disposed) return;
            icon.texture = texture;
            LeanTween.alphaGraphic(icon, 1f, 0.2f).setOnComplete(HideLoader);
        }

        protected override void OnClick()
        {
            if (bannerData == null) return;
            bannerData.IsOld = true;
            Mark.EventsCount = 0;
            
            NativeUtils.OpenApplicationStore(bannerData.link);
        }

        private void HideLoader()
        {
            if (destroyed) return;

            LeanTween.alphaCanvas(loader, 0f, 0.2f);
        }

    }
}