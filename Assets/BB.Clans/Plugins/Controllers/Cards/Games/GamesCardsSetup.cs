﻿using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.BBGames;
using UnityEngine;

namespace BlackBears.Clans.Controller.Cards.Games
{

    public class GamesCardsSetup : MonoBehaviour
    {

        [SerializeField] private CardsListController listController;

        [SerializeField] private GameCard cardPrefab;

        [SerializeField] private int startIndex;
        [SerializeField] private int count;

        private BBGamesFeature games;
        private bool disposed;

        private void Start()
        {
            games = BBGC.Features.GetFeature<BBGamesFeature>();
            if (games.DataLoaded) SetupCards();
            else games.OnBannersLoaded += SetupCards;
        }

        private void OnDestroy()
        {
            if (games != null) games.OnBannersLoaded -= SetupCards;
            Destroy(this.gameObject);
            disposed = true;
        }

        private void SetupCards()
        {
            if (disposed) return;

            int start = startIndex;
            int end = count >= 0 ? startIndex + count : games.BannersCount;
            end = Mathf.Min(end, games.BannersCount);
            if (end - start > 0)
            {
                List<BaseCard> cards = new List<BaseCard>();
                for (int i = start; i < end; i++)
                {
                    var card = Instantiate(cardPrefab, transform.parent, false);
                    card.transform.SetSiblingIndex(transform.GetSiblingIndex() + 1);
                    card.SetData(games[i], games);
                    cards.Add(card);
                }
                listController.OnCardAdded(cards);
            }
            Destroy(this.gameObject);
        }

    }

}