using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.Utils;
using UnityEngine;

namespace BlackBears.Clans.Controller.Cards
{
    internal sealed class AppStoreCard : BaseCard
    {
        
        protected override void Start()
        {
            base.Start();
            gameObject.SetActive(Configuration.AppStoreEnabled);
        }

        protected override void OnClick()
        {
            NativeUtils.OpenStore(BBGC.Features.CoreDefaults().DevStoreId);
        }

    }
}