using System;
using System.Collections.Generic;
using BlackBears.Utils.View.SafeArea;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller
{

    [RequireComponent(typeof(RectTransform), typeof(PinchZoomUI), typeof(ScrollRect))]
    [RequireComponent(typeof(CanvasGroup))]
    internal class PhotoController : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler,
        IPointerClickHandler
    {

        private const float maxScale = 1f;
        private const float reactDelta = 100f;
        private const float closeDelta = 150f;

        [SerializeField] private Image background;
        [SerializeField] private RawImage image;

        private RectTransform _rectTransform;
        private PinchZoomUI _pinchZoomUI;
        private ScrollRect _scrollRect;
        private CanvasGroup _canvasGroup;

        private SafeAreaData safeAreaData;

        private List<int> pointers = new List<int>();
        private float minScale;
        private float maxDownscale;
        private float closeScale;
        private bool draged;
        private bool closeBlocked;
        private bool onClose;

        private int clickCount;

        private float ScaleFactor
        {
            get
            {
                float scaleFactor = 1f;
                if (safeAreaData != null && safeAreaData.ScaleFactor != 0) scaleFactor = safeAreaData.ScaleFactor;
                return scaleFactor;
            }
        }

        private RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }

        private PinchZoomUI PinchZoomUI
        {
            get { return _pinchZoomUI ?? (_pinchZoomUI = GetComponent<PinchZoomUI>()); }
        }

        private ScrollRect ScrollRect
        {
            get { return _scrollRect ?? (_scrollRect = GetComponent<ScrollRect>()); }
        }

        private CanvasGroup CanvasGroup
        {
            get { return _canvasGroup ?? (_canvasGroup = GetComponent<CanvasGroup>()); }
        }

        private void Start()
        {
            SafeAreaDataContainer.TryGetData(Keys.safeAreaCoreDataKey, out safeAreaData);

            PinchZoomUI.OnScaleDiffChanged.AddListener(OnManualScale);
            PinchZoomUI.OnPinchStarted.AddListener(PinchStarted);
            PinchZoomUI.OnPinchEnded.AddListener(PinchEnded);

            PinchZoomUI.scaleSpeed /= ScaleFactor;
        }

        private void OnDestroy()
        {
            CancelTweens();
        }

        internal void Show()
        {
            CancelTweens();

            var rectSize = RectTransform.rect.size;
            image.SetNativeSize();
            var imageSize = image.rectTransform.rect.size;
            if (imageSize.x == 0f || imageSize.y == 0f) return;

            minScale = Mathf.Min(rectSize.x / imageSize.x, rectSize.y / imageSize.y);
            closeScale = minScale * 0.65f;
            maxDownscale = minScale * 0.5f;
            image.rectTransform.localScale = new Vector3(minScale, minScale, 1f);
            pointers.Clear();
            clickCount = 0;
            draged = false;
            closeBlocked = false;
            onClose = false;

            CanvasGroup.alpha = 0f;
            CanvasGroup.blocksRaycasts = true;
            var bgc = background.color;
            bgc.a = 1f;
            background.color = bgc;

            gameObject.SetActive(true);
            LeanTween.alphaCanvas(CanvasGroup, 1f, 0.2f);
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            draged = true;
            if (PinchZoomUI.InPinch) { closeBlocked = true; }
            if (closeBlocked) return;
            if (!Mathf.Approximately(image.rectTransform.localScale.x, minScale)) return;

            float delta = Mathf.Abs(eventData.position.y - eventData.pressPosition.y) / ScaleFactor;

            if (delta < reactDelta)
            {
                if (onClose) onClose = false;
                return;
            }
            if (!onClose)
            {
                onClose = true;
                PinchZoomUI.DetectEnabled = false;
            }
            var color = background.color;
            color.a = Mathf.Lerp(1, 0, (delta - reactDelta) / (closeDelta - reactDelta));
            background.color = color;
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            LeanTween.cancel(gameObject);
            pointers.Add(eventData.pointerId);
            if (pointers.Count == 1) draged = false;
            LeanTween.cancel(image.gameObject);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            pointers.Remove(eventData.pointerId);
            if (pointers.Count > 0) return;

            closeBlocked = false;
            LeanTween.alphaGraphic(background, 1f, 0.2f);
            PinchZoomUI.DetectEnabled = true;
            if (onClose)
            {
                onClose = false;
                Close();
            }
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (draged)
            {
                clickCount = 0;
                return;
            }
            clickCount += 1;
            if (clickCount == 1)
            {
                LeanTween.delayedCall(gameObject, 0.2f, ClickClose);
            }
            else if (clickCount == 2)
            {
                LeanTween.cancel(gameObject);
                clickCount = 0;
                Zoom();
            }
        }

        private void PinchStarted()
        {
            ScrollRect.enabled = false;
            LeanTween.cancel(image.gameObject);
        }

        private void PinchEnded()
        {
            ScrollRect.enabled = true;
            if (image.rectTransform.localScale.x <= closeScale)
            {
                ZoomOut();
                Close();
            }
            else if (image.rectTransform.localScale.x < minScale)
            {
                ZoomOut();
            }
        }

        private void ClickClose()
        {
            clickCount = 0;
            Close();
        }

        private void Zoom()
        {
            if (Mathf.Approximately(image.rectTransform.localScale.x, minScale)) ZoomIn();
            else ZoomOut();
        }

        private void ZoomIn()
        {
            LeanTween.scale(image.gameObject, new Vector3(maxScale, maxScale, 1f), 0.2f).setEaseOutQuad();
        }

        private void ZoomOut()
        {
            LeanTween.scale(image.gameObject, new Vector3(minScale, minScale, 1f), 0.2f).setEaseOutQuad();
        }

        private void Close()
        {
            CancelTweens();
            CanvasGroup.blocksRaycasts = false;
            LeanTween.alphaCanvas(CanvasGroup, 0f, 0.2f)
                .setOnComplete(() => gameObject.SetActive(false));
        }

        private void CancelTweens()
        {
            LeanTween.cancel(CanvasGroup.gameObject);
            LeanTween.cancel(background.gameObject);
            LeanTween.cancel(image.gameObject);
            LeanTween.cancel(gameObject);
        }

        private void OnManualScale(float deltaScale)
        {
            if (deltaScale == 0f) return;

            var imageRT = image.rectTransform;
            var scale = imageRT.localScale.x;
            scale -= deltaScale;
            scale = Mathf.Min(scale, maxScale);
            scale = Mathf.Max(scale, maxDownscale);

            var xPos = ValidatePos(imageRT.anchoredPosition.x, scale, imageRT.rect.size.x);
            var yPos = ValidatePos(imageRT.anchoredPosition.y, scale, imageRT.rect.size.y);
            imageRT.anchoredPosition = new Vector2(xPos, yPos);
            imageRT.localScale = new Vector3(scale, scale, 1);
        }

        private float ValidatePos(float pos, float scale, float size)
        {
            var halfSize = size / 2f;
            var maxPos = halfSize * scale - halfSize * minScale;
            if (maxPos < 0) maxPos = 0;

            if (pos > maxPos) pos = maxPos;
            else if (pos < -maxPos) pos = -maxPos;

            return pos;
        }

    }

}