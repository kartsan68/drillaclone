using System.Collections;
using BlackBears.GameCore.Graphic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments
{

    [RequireComponent(typeof(CanvasGroup))]
    internal class FragmentControls : MonoBehaviour, IDragHandler, IEndDragHandler
    {

        [SerializeField] private GameObject closeKeyboardButton;
        [SerializeField] private Button closeButton;
        [SerializeField] private Button backButton;

        private bool defaultMode;
        private int count;
        private CanvasGroup canvasGroup;
        private FragmentsController controller;
        private float scale;

        internal void Inject(FragmentsController controller)
        {
            this.controller = controller;
        }

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0f;
        }

        private void Start()
        {
            controller.OnFragmentsCountChanged.AddListener(FragmentsCountUpdate);
            FragmentsCountUpdate(controller.FragmentsCount);

            backButton.onClick.AddListener(controller.BackRequest);
            closeButton.onClick.AddListener(controller.CloseAll);

            scale = UICache.Instance.TakeSelector(CoreCache.key).CurrentScale;
        }

        private void OnEnable()
        {
            if (TouchScreenKeyboard.isSupported)
            {
                StartCoroutine(CheckKeyboardModes());
            }
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void Show()
        {
            LeanTween.cancel(gameObject);
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
            SetDefaultMode();
        }

        internal void Hide()
        {
            LeanTween.cancel(gameObject);
            LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f);
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            var fragment = controller.CurrentFragment;
            if (fragment != null) fragment.OnDrag(CalculateDragDelta(eventData));
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            var fragment = controller.CurrentFragment;
            if (fragment != null) fragment.OnEndDrag(CalculateDragDelta(eventData));
        }

        private void SetDefaultMode()
        {
            defaultMode = true;
            CheckButtons();
        }

        private void SetKeyboardMode()
        {
            defaultMode = false;
            CheckButtons();
            closeButton.interactable = false;
            backButton.interactable = false;
        }

        private void FragmentsCountUpdate(int count)
        {
            this.count = count;
            CheckButtons();
        }

        private void CheckButtons()
        {
            if (defaultMode)
            {
                closeButton.interactable = count > 1;
                backButton.interactable = count > 0;
                closeKeyboardButton.SetActive(false);
            }
            else
            {
                closeButton.interactable = false;
                backButton.interactable = false;
                closeKeyboardButton.SetActive(true);
            }
        }

        private IEnumerator CheckKeyboardModes()
        {
            while (true)
            {
                yield return null;
                if (TouchScreenKeyboard.visible == defaultMode)
                {
                    if (defaultMode) SetKeyboardMode();
                    else SetDefaultMode();
                }
            }
        }

        private float CalculateDragDelta(PointerEventData eventData)
        {
            return (eventData.position.x - eventData.pressPosition.x) / scale;
        }

    }

}