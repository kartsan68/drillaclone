﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.Clans.Controller.ContextMenu;
using BlackBears.Clans.Controller.Fragments.ClanJoinRequests;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.ViewModel.ContextMenu;
using BlackBears.GameCore;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Views;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    internal class ClanInfoFragment : Fragment
    {

        [SerializeField] private SpritePackView[] scoreImages;

        [SerializeField] private Image background;
        [SerializeField] private ClanInfoRollable headerRollable;
        [SerializeField] private ClanInfoControllerLandscape controllerLandscape;
        [SerializeField] private GameObject inviteContainer;
        [SerializeField] private UsersListController usersController;

        [SerializeField] private Text requestsCountText;
        [SerializeField] private Button inviteButton;
        [SerializeField] private Text inviteCodeText;

        [Header("Portrait")]
        [SerializeField] private Button requestsButton;

        [Header("Landscape")]
        [SerializeField] private ScrollRect scrollRequests;
        [SerializeField] private GameObject requestsGameObject;
        [SerializeField] private LandscapeSwitchController landscapeSwitchController;
        [SerializeField] private JoinRequestCell cellPrefab;

        [SerializeField] private GameObject clanInfoRoot;
        [SerializeField] private GameObject usersRoot;
        [SerializeField] private GameObject requestsRoot;

        private InfoType currentTab = InfoType.Info;
        private ClanCore core;
        private NotificationsFeature notifications;
        private AlertsFeature alerts;
        private ContextController contextController;
        private FragmentsController fragmentsController;
        private Clan clan;
        private Menu.Builder menuBuilder;
        private ClanUser[] requestUsers;
        private PredefinedIconsContainer profileIcons;
        private SpritePack scorePack;

        internal override FragmentKind Kind { get { return FragmentKind.ClanInfo; } }
        internal ScrollRect ScrollRequests { get { return scrollRequests; } }

        internal void Inject(ClanCore core, ClanFullSettings settings,
            NotificationsFeature notifications, AlertsFeature alerts,
            ContextController contextController, FragmentsController fragmentsController, Clan clan,
            PredefinedIconsContainer profileIcons,
            SpritePack scorePack)
        {
            this.core = core;
            this.notifications = notifications;
            this.alerts = alerts;
            this.contextController = contextController;
            this.fragmentsController = fragmentsController;
            this.clan = clan;
            this.profileIcons = profileIcons;
            this.scorePack = scorePack;

            var config = settings.configuration;
#if BBGC_LANDSCAPE
            controllerLandscape.Inject(core, config.Countries, config.ClanIcons);
#else
            headerRollable.Inject(core, config.Countries, config.ClanIcons);
#endif
            usersController.Inject(core, config.ProfileIcons, settings.scoreIconPack,
                settings.subsSprites, this);

            foreach (var scoreImage in scoreImages) scoreImage.SetSpritePack(settings.scoreIconPack);
        }

        protected override void Start()
        {
            base.Start();
#if BBGC_LANDSCAPE
            controllerLandscape.EditButton.onClick.AddListener(OnEditClanRequest);
            controllerLandscape.ExitButton.onClick.AddListener(OnExitRequest);
            controllerLandscape.InviteButton.onClick.AddListener(fragmentsController.ShowInvites);
            controllerLandscape.JoinCancelButton.onClick.AddListener(OnExitRequest);
            controllerLandscape.JoinButtonController.Button.onClick.AddListener(OnJoinRequest);
            landscapeSwitchController.OnTopTypeChanged.AddListener(OnTabChanged);
#else
            headerRollable.EditButton.onClick.AddListener(OnEditClanRequest);
            headerRollable.ExitButton.onClick.AddListener(OnExitRequest);
            headerRollable.InviteButton.onClick.AddListener(fragmentsController.ShowInvites);
            headerRollable.JoinCancelButton.onClick.AddListener(OnExitRequest);
            headerRollable.JoinButtonController.Button.onClick.AddListener(OnJoinRequest);
            requestsButton.onClick.AddListener(fragmentsController.ShowClanJoinRequests);
#endif
            inviteButton.onClick.AddListener(fragmentsController.ShowInvites);

            if (clan != null) ReloadClan();
            else UpdateViews();
        }

        internal override void OnFocus()
        {
            base.OnFocus();
            UpdateViews();
        }

        internal override void LostFocus()
        {
            base.LostFocus();
            usersController.LostFocus();
        }

        internal void OnUserSelected(ClanUser user, IPlayerProfile userProfile, Vector2 position, Camera cam)
        {
            if (user == null) return;
            bool openPlayer = core.State.IsCurrentUser(user);
            openPlayer = openPlayer || !core.State.IsUserClan(clan);
            openPlayer = openPlayer || (core.State.IsUserInClan && !core.State.User.Role.IsModerator());

            if (openPlayer) fragmentsController.ShowPlayerInfo(user.Id, user, clan);
            else ShowContextMenuForUser(user, userProfile, position, cam);
        }

        protected override void OnScrollToTopFinished()
        {
            base.OnScrollToTopFinished();
            usersController.CheckMarkForShow();
        }

        private void OnTabChanged(InfoType type)
        {
            if (type == currentTab) return;

            clanInfoRoot.SetActive(type == InfoType.Info);
            usersRoot.SetActive(type == InfoType.Users);
            requestsRoot.SetActive(type == InfoType.Requests);

            currentTab = type;
        }

        private void ShowContextMenuForUser(ClanUser otherUser, IPlayerProfile otherUserProfile, Vector2 screenPosition, Camera cam)
        {
            bool isUserClan = core.State.IsUserClan(clan);
            ClanRole userRole = core.State.User.Role;

            var builder = menuBuilder ?? (menuBuilder = new Menu.Builder(contextController.Preferences));
            builder.Reset();
            builder.AddPlayerMenuItem(otherUser.Id, () => fragmentsController.ShowPlayerInfo(otherUser.Id,
                otherUser, clan), true);

            if (isUserClan)
            {
                if (userRole.IsAdministrator())
                {
                    if (otherUser.Role.IsModerator()) builder.AddDemoteItem(() => DemoteUserRequest(otherUser));
                    else builder.AddPromoteItem(() => PromoteUserRequest(otherUser));
                }
                if (userRole.IsAdministrator() || (userRole.IsModerator() && !otherUser.Role.IsModerator()))
                {
                    builder.AddKickItem(() => KickUserRequest(otherUser, otherUserProfile));
                }
            }
            contextController.ShowContextMenu(builder.Build(), screenPosition, cam);
        }

        private void UpdateViews()
        {
#if BBGC_LANDSCAPE
            controllerLandscape.Clan = clan;
#else
            headerRollable.Clan = clan;
#endif
            usersController.Users = clan.Users;
            bool isUserClan = core.State.IsUserClan(clan);
            if (isUserClan)
            {
                var user = core.State.User;
                if (user == null) isUserClan = false;
                else isUserClan = user.Role.IsClanMember();
            }
            if (isUserClan) usersController.UpdateViews();

            inviteContainer.SetActive(isUserClan);
#if BBGC_LANDSCAPE
            requestsGameObject.SetActive(isUserClan && core.State.User.Role.IsModerator() && clan.JoinRequests > 0);
#else
            requestsButton.gameObject.SetActive(isUserClan && core.State.User.Role.IsModerator() && clan.JoinRequests > 0);
#endif

            requestsCountText.text = clan.JoinRequests.ToString();

            inviteCodeText.text = ValidateHelper.FormatClanTag(clan.Tag);
        }

        private void OnEditClanRequest()
        {
            fragmentsController.ShowClanEdit(UpdateViews, CloseRequest);
        }

        private void ReloadClan()
        {
            if (Disposed) return;
            ShowLoader(string.Empty, background.color);
            core.GetClanAndValidateProfiles(clan, OnClanReloaded, OnClanReloadFailed, true);
        }

        private void OnClanReloaded(Clan clan, bool profilesValidated)
        {
            if (Disposed) return;

            this.clan = clan;
            UpdateViews();
            if (!profilesValidated)
            {
                core.State.ProfileCache.LoadPlayersForClanAsync(clan, true, OnProfilesValidated,
                (c, m) => Debug.LogFormat("OnClanReloaded: {0}; {1}", c, m));
            }
#if BBGC_LANDSCAPE
            bool isUserClan = core.State.IsUserClan(clan);
            if (isUserClan && core.State.User.Role.IsModerator() && clan.JoinRequests > 0) LoadRequestPlayers();
            else LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
#else
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
#endif

        }

        private void OnProfilesValidated()
        {
            if (!Disposed) usersController.UpdateViews();
        }

        private void OnClanReloadFailed(long errorCode, string message)
        {
            Debug.LogFormat("OnClanReloadFailed: {0}; {1}", errorCode, message);
            if (Disposed) return;

            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;

                HideLoader();
                ShowError(ReloadClan);
            });
        }


        private void LoadRequestPlayers()
        {
            core.GetIncomingPlayers(OnClanUsersLoaded, OnClanUsersLoadingFailed);
        }

        private void OnClanUsersLoaded(ClanUser[] users)
        {
            if (Disposed) return;

            List<string> ids = new List<string>();
            this.requestUsers = users;
            foreach (var user in this.requestUsers) ids.Add(user.Id);
            core.State.ProfileCache.LoadPlayersAsync(ids, OnProfilesLoaded, OnProfilesLoadFailed);
        }

        private void OnProfilesLoaded()
        {
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);

            if (requestUsers == null) return;

            foreach (var user in requestUsers)
            {
                if (user == null) continue;

                var cell = Instantiate(cellPrefab);
                cell.RectTransform.SetParent(ScrollRequests.content, false);
                cell.RectTransform.SetAsLastSibling();
                cell.Inject(FragmentsController, core, notifications, user, core.State.ProfileCache.GetCachedPlayer(user.Id),
                    profileIcons, scorePack);
            }
        }

        private void OnClanUsersLoadingFailed(long errorCode, string erorMessage)
        {
            Debug.LogFormat("OnClanUsersLoadingFailed: {0}; {1}", errorCode, erorMessage);
            if (Disposed) return;

            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;

                HideLoader();
                ShowError(() =>
                {
                    ShowLoader(string.Empty, background.color);
                    LoadRequestPlayers();
                });
            });
        }

        private void OnProfilesLoadFailed(long errorCode, string erorMessage)
        {
            Debug.LogFormat("OnProfilesLoadFailed: {0}; {1}", errorCode, erorMessage);
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
        }


        private void OnExitRequest()
        {
            if (alerts != null)
            {
                Alert alert;
                if (core.State.User.Role.IsAdministrator())
                    alert = AlertsBuilder.CreateDestroyClanAlert(ExitFromClan, null);
                else
                    alert = AlertsBuilder.CreateLeaveClanAlert(ExitFromClan, null);
                alerts.AddAlert(alert);
            }
        }

        private void ExitFromClan()
        {
            ExitFromClan(CloseRequest, ExitFromClan);
        }

        private void ExitFromClan(Block successBlock, Block repeatBlock)
        {
            ShowLoader(string.Empty, background.color);
            core.LeaveClan(successBlock, (c, m) => OnExitFailed(c, m, repeatBlock));
        }

        private void OnJoinRequest()
        {
#if BBGC_LANDSCAPE
            controllerLandscape.JoinButtonController.ShowLoader(JoinClan);
#else
            headerRollable.JoinButtonController.ShowLoader(JoinClan);
#endif
        }

        private void JoinClan()
        {
            if (Disposed) return;
            if (core.State.IsUserInClan)
            {
                Alert alert;
                if (core.State.User.Role.IsAdministrator())
                {
                    alert = AlertsBuilder.CreateDestroyClanAlert(LeaveOldClanAndJoinCurrent,
                        OnLeaveOldAndJoinCurrentCanceled);
                }
                else
                {
                    alert = AlertsBuilder.CreateLeaveClanAlert(LeaveOldClanAndJoinCurrent,
                        OnLeaveOldAndJoinCurrentCanceled);
                }

                alerts.AddAlert(alert);
            }
            else
            {
                core.JoinClan(clan, CloseRequest, OnJoinFailed);
            }
        }

        private void LeaveOldClanAndJoinCurrent()
        {
            if (Disposed) return;
            ExitFromClan(JoinClan, LeaveOldClanAndJoinCurrent);
        }

        private void OnLeaveOldAndJoinCurrentCanceled()
        {
            if (Disposed) return;

#if BBGC_LANDSCAPE
            controllerLandscape.JoinButtonController.HideLoader(false);
#else
            headerRollable.JoinButtonController.HideLoader(false);
#endif
        }

        private void PromoteUserRequest(ClanUser user)
        {
            ShowLoader(string.Empty, background.color);
            core.PromotePlayer(user.Id, ReloadClan, (c, m) =>
            {
                Debug.LogFormat("PromoteUserRequest: {0}; {1}", c, m);
                if (Disposed) return;
                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorPromotePlayer)));
                LeanTween.delayedCall(gameObject, loaderDelay, () =>
                {
                    if (Disposed) return;
                    HideLoader();
                    ShowError(() => PromoteUserRequest(user));
                });
            });
        }

        private void DemoteUserRequest(ClanUser user)
        {
            ShowLoader(string.Empty, background.color);
            core.DemotePlayer(user.Id, ReloadClan, (c, m) =>
            {
                Debug.LogFormat("DemoteUserRequest: {0}; {1}", c, m);
                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorDemotePlayer)));
                LeanTween.delayedCall(gameObject, loaderDelay, () =>
                {
                    HideLoader();
                    ShowError(() => DemoteUserRequest(user));
                });
            });
        }

        private void KickUserRequest(ClanUser user, IPlayerProfile profile)
        {
            alerts.AddAlert(AlertsBuilder.CreateKickUserAlert(profile.Nickname, () => KickUser(user), null));
        }

        private void KickUser(ClanUser user)
        {
            ShowLoader(string.Empty, background.color);
            core.KickFromClan(user.Id, ReloadClan, (c, m) =>
            {
                Debug.LogFormat("KickUserRequest: {0}; {1}", c, m);
                if (Disposed) return;

                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorKickUser)));
                LeanTween.delayedCall(gameObject, loaderDelay, () =>
                {
                    if (Disposed) return;

                    HideLoader();
                    ShowError(() => KickUser(user));
                });
            });
        }

        private void OnJoinFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnJoinFailed: {0}; {1}", errorCode, errorMessage);
            if (Disposed) return;

            if (errorCode == ClanErrorCodes.joinClanNoSlotsErrorCode)
            {
                ReloadClan();
                return;
            }

            notifications.ShowNotification(new Notification(NotificationType.Warning,
                ClansLocalization.Get(LocalizationKeys.notificationErrorJoinClan)));
#if BBGC_LANDSCAPE
            controllerLandscape.JoinButtonController.HideLoader(false);
#else
            headerRollable.JoinButtonController.HideLoader(false);
#endif
            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;
                HideLoader();
                ShowError(JoinClan);
            });
        }

        private void OnValidateAfterExitTry()
        {
            if (Disposed) return;

            if (core.State.IsUserInClan && core.State.IsUserClan(clan))
            {
                LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
            }
            else
            {
                CloseRequest();
            }
        }

        private void OnExitFailed(long errorCode, string errorMessage, Block repeatBlock)
        {
            if (Disposed) return;

            if (!core.State.Validated)
            {
                core.Validate(OnValidateAfterExitTry, OnValidateFailed);
            }
            else
            {
                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorClanExitFailed)));
                Debug.LogFormat("OnExitFailed: {0}; {1}", errorCode, errorMessage);
                LeanTween.delayedCall(gameObject, loaderDelay, () =>
                {
                    if (Disposed) return;

                    HideLoader();
                    ShowError(repeatBlock);
                });
            }
        }

        private void OnValidateFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnValidateFailed: {0}; {1}", errorCode, errorMessage);
            if (!Disposed) HideLoader();
        }

    }

}