using System;
using System.Collections;
using BlackBears.Clans.Model;
using BlackBears.Clans.View.Loader;
using BlackBears.GameCore.Graphic;
using BlackBears.Resolutor;
using BlackBears.Utils.Components;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    [RequireComponent(typeof(Button))]
    internal class JoinButtonController : MonoBehaviour
    {

        private const float delay = 0.6f;

        [SerializeField] private Image buttonIcon;
        [SerializeField] private Text title;
        [SerializeField] private Text subtitle;
        [SerializeField] private ButtonLoaderAnimator loaderAnimator;

        [Header("Icon sprites")]
        [SerializeField] private string publicClanIconName;
        [SerializeField] private string privateClanIconName;

        private Atlas _atlas;
        private Button button;

        internal Atlas Atlas { get { return _atlas ?? (_atlas = UICache.Instance.TakeAtlas(CoreCache.key)); } }
        internal Button Button { get { return button ?? (button = GetComponent<Button>()); } }

        internal void SetDataFromClan(Clan clan)
        {
            if (clan == null) return;
            title.text = ClansLocalization.Get(clan.IsPublic ? 
                LocalizationKeys.joinButtonPublicClanTitle : LocalizationKeys.joinButtonPrivateClanTitle);
            subtitle.text = ClansLocalization.Get(clan.IsPublic ? 
                LocalizationKeys.joinButtonPublicClanSubtitle : LocalizationKeys.joinButtonPrivateClanSubtitle);
                
            buttonIcon.sprite = Atlas[clan.IsPublic ? publicClanIconName : privateClanIconName];
        }

        internal void ShowLoader(Block onShowed)
        {
            Button.interactable = false;
            loaderAnimator.StartLoadingAnimation();
            StartCoroutine(PostCallback(onShowed));
        }

        internal void HideLoader(bool success = true)
        {
            StartCoroutine(HideLoaderDelayed(success));
        }

        private IEnumerator PostCallback(Block callback)
        {
            yield return new WaitForSecondsRealtime(delay);
            callback.SafeInvoke();
        }

        private IEnumerator HideLoaderDelayed(bool success)
        {
            yield return new WaitForSecondsRealtime(delay);
            loaderAnimator.StopLoadingAnimation(success: success, onStopped: () => Button.interactable = true);
        }

    }

}