using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    internal class EngagedButtonController : MonoBehaviour
    {

        [SerializeField] private GameObject scoreContainer;
        [SerializeField] private GameObject userAmountContainer;
        [SerializeField] private Text scoreText;

        internal void SetBlockedByUserAmount()
        {
            scoreContainer.SetActive(false);
            userAmountContainer.SetActive(true);
        }

        internal void SetBlockedByScore(double score)
        {
            scoreContainer.SetActive(true);
            scoreText.text = score.ToString();
            userAmountContainer.SetActive(false);
        }

    }

}