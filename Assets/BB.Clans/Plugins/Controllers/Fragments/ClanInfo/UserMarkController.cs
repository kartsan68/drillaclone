using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    internal class UserMarkController : MonoBehaviour
    {

        [SerializeField] private Text nameText;
        [SerializeField] private Text placeText;
        [SerializeField] private PredefinedIconView userIcon;

        private PredefinedIconsContainer icons;

        internal void Inject(PredefinedIconsContainer icons)
        {
            this.icons = icons;
        }

        internal void SetUser(IPlayerProfile profile, int place)
        {
            nameText.text = profile.Nickname;
            placeText.text = string.Format("#{0}", place);
            userIcon.SetIcon(profile.Icon, icons);
        }

    }

}