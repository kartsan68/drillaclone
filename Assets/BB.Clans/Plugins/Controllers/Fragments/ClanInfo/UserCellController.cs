using System;
using BlackBears.Chronometer;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Graphic;
using BlackBears.GameCore.Views;
using BlackBears.Resolutor;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    [RequireComponent(typeof(RectTransform))]
    internal class UserCellController : MonoBehaviour, IPointerClickHandler
    {

        [SerializeField] private Text nameText;
        [SerializeField] private Image moderatolUnderline;
        [SerializeField] private SpritePackView scoreImage;
        [SerializeField] private Text placeText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text weeklyScoreText;
        [SerializeField] private Text onlineDateText;
        [SerializeField] private Text joinDateText;
        [SerializeField] private PredefinedIconView iconView;

        [SerializeField] private RectTransform playerMarkContainer;
        [SerializeField] private GameObject playerMarkLine;
        [SerializeField] private RectTransform playerMark;

        [SerializeField] private Image roleMark;
        [SerializeField] private string roleAdminSpriteName;
        [SerializeField] private string roleModeratorSpriteName;

        [SerializeField] private GameObject subscriptionContainer;
        [SerializeField] private Image subscriptionIcon;

        [Header("Colors for side mark")]
        [SerializeField] private Color adminUnderlineColor = ColorUtils.FromHex(0xEBD677FF);
        [SerializeField] private Color moderatorUnderlineColor = ColorUtils.FromHex(0xE9BD88FF);

        private Atlas atlas;

        private UsersListController listController;
        private PredefinedIconsContainer profileIcons;
        private SubscriptionSpritesContainer subsSprites;

        private bool isPlayer;
        private int place;
        private RectTransform rectTransform;
        private float xSizeScrollRect;

        internal ClanUser User { get; private set; }
        internal IPlayerProfile Profile { get; private set; }
        internal RectTransform RectTransform { get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); } }

        internal void Inject(UsersListController listController, PredefinedIconsContainer profileIcons,
            SpritePack scorePack, SubscriptionSpritesContainer subsSprites, float xSizeScrollRect)
        {
            this.listController = listController;
            this.profileIcons = profileIcons;
            this.subsSprites = subsSprites;
            this.xSizeScrollRect = xSizeScrollRect;
            scoreImage.SetSpritePack(scorePack);
        }

        private void Awake()
        {
            atlas = UICache.Instance.TakeAtlas(CoreCache.key);
        }

        private void OnDestroy()
        {
            User = null;
            LeanTween.cancel(playerMark.gameObject);
        }

        internal void SetData(ClanUser user, int place, bool isPlayer, IPlayerProfile profile)
        {
            this.User = user;
            this.place = place;
            this.Profile = profile;
            this.isPlayer = isPlayer;
            UpdateView();
            ShowPlayerMark();
        }

        internal void HidePlayerMark() => HidePlayerMark(false);

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            listController.OnItemSelected(this, eventData.position, eventData.enterEventCamera);
        }

        private void UpdateView()
        {
            if (User == null) return;

            moderatolUnderline.gameObject.SetActive(true);
            if (User.Role.IsAdministrator()) moderatolUnderline.color = adminUnderlineColor;
            else if (User.Role.IsModerator()) moderatolUnderline.color = moderatorUnderlineColor;
            else moderatolUnderline.gameObject.SetActive(false);

            nameText.text = string.IsNullOrEmpty(Profile.Nickname) ? ValidateHelper.PlayerIdToTempNick(User.Id) : Profile.Nickname;
            placeText.text = place.ToString();

            scoreText.text = User.Score.ToString();
            var onlineTs = System.TimeSpan.FromSeconds(TimeModule.ActualTime - User.OnlineDate);

            var joinTs = System.TimeSpan.FromSeconds(TimeModule.ActualTime - User.JoinDate);
            // Если чувак отыграл меньше дня, ставим span в 1 день чтобы вывелось сообщение про 1 день
            if (joinTs.TotalDays < 1) joinTs = TimeSpan.FromDays(1.0);

            weeklyScoreText.gameObject.SetActive(Profile.WeeklyScore != 0 && onlineTs.Days < 7);
            weeklyScoreText.text = Profile.WeeklyScore.ToString("+#;-#;0");
            var onlineTm = DateUtils.ToTimedMessage(onlineTs);
            if (onlineTm.key == TimedMessage.justNowKey)
            {
                onlineDateText.text = ClansLocalization.Get(LocalizationKeys.online);
            }
            else
            {
                var offline = ClansLocalization.Get(LocalizationKeys.offline);
                var message = ClansLocalization.Get(onlineTm.key, onlineTm.valueCount);
                onlineDateText.text = string.Format("{0} {1}", offline, message);
            }

            var joinTm = DateUtils.ToTimedMessage(joinTs);
            joinDateText.text = string.Format("{0} {1}", ClansLocalization.Get(LocalizationKeys.inClan),
                ClansLocalization.Get(joinTm.key, joinTm.valueCount));
            iconView.SetIcon(Profile.Icon, profileIcons);

#if BBGC_LANDSCAPE
            playerMarkLine.SetActive(false);
            Debug.Log("xSizeScrollRect " + xSizeScrollRect);
            playerMarkContainer.sizeDelta = new Vector2(xSizeScrollRect, playerMarkContainer.sizeDelta.y);
#endif
            playerMarkContainer.gameObject.SetActive(isPlayer);

            HidePlayerMark(true);

            string roleSpriteName = null;
            switch (User.Role)
            {
                case ClanRole.Moderator: roleSpriteName = roleModeratorSpriteName; break;
                case ClanRole.Administrator: roleSpriteName = roleAdminSpriteName; break;
            }

            Sprite roleSprite = null;
            if (roleSpriteName != null) roleSprite = atlas[roleSpriteName];
            roleMark.sprite = roleSprite;
            roleMark.gameObject.SetActive(roleMark.sprite != null);

            var subPack = subsSprites[Profile.SubscriptionId];
            subscriptionIcon.sprite = subPack?.SimpleSprite;
            subscriptionContainer.SetActive(subscriptionIcon.sprite != null);
        }

        private void ShowPlayerMark()
        {
            if (!isPlayer) return;
            var pos = Vector2.zero;
            LeanTween.cancel(playerMark.gameObject);
            LeanTween.moveAnchored(playerMark, pos, 0.2f).setEaseOutQuad();
        }

        private void HidePlayerMark(bool force)
        {
            if (!isPlayer) return;
            LeanTween.cancel(playerMark.gameObject);
            var pos = new Vector2(playerMark.rect.size.x, 0);
            if (force) playerMark.anchoredPosition = pos;
            else LeanTween.moveAnchored(playerMark, pos, 0.2f).setEaseInQuad();
        }

    }

}