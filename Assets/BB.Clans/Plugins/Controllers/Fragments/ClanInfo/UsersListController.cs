using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.Clans.Controller.Common;
using BlackBears.Clans.Controller.ContextMenu;
using BlackBears.Clans.Model;
using BlackBears.GameCore.Configurations;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    internal class UsersListController : MonoBehaviour
    {

        [SerializeField] private ScrollRect scroll;
        [SerializeField] private UserCellController cellPrefab;
        [SerializeField] private EdgeAnchoredMark userTopMark;
        [SerializeField] private EdgeAnchoredMark userBottomMark;
        [SerializeField] private UserMarkController[] markControllers;

        private ClanCore core;
        private PredefinedIconsContainer profileIcons;
        private SpritePack scoreIcon;
        private SubscriptionSpritesContainer subsSprites;
        private ClanInfoFragment fragment;
        private UserCellController userItem;
        private List<UserCellController> items = new List<UserCellController>();
        private List<ClanUser> users = new List<ClanUser>();

        internal IList<ClanUser> Users
        {
            set
            {
                users.Clear();
                if (value != null)
                {
                    foreach (var user in value)
                    {
                        if (user.Role.IsClanMember()) users.Add(user);
                    }
                    users.Sort(SortUsers);
                }
                UpdateViews();
            }
        }

        internal void Inject(ClanCore core, PredefinedIconsContainer profileIcons, SpritePack scoreIcon,
            SubscriptionSpritesContainer subsSprites, ClanInfoFragment fragment)
        {
            this.core = core;
            this.profileIcons = profileIcons;
            this.scoreIcon = scoreIcon;
            this.subsSprites = subsSprites;
            this.fragment = fragment;
            foreach (var mark in markControllers) mark.Inject(profileIcons);
        }

        internal void CheckMarkForShow()
        {
            if (userItem == null) return;

            var position = userItem.RectTransform.anchoredPosition;
            position.y += scroll.content.anchoredPosition.y;

            if (position.y > 0) userTopMark.Show(false);
            else userTopMark.Hide(false);

            if (position.y < -scroll.viewport.rect.height) userBottomMark.Show(false);
            else userBottomMark.Hide(false);
        }

        private void Start()
        {
            scroll.onValueChanged.AddListener(_ => CheckMarkForShow());
        }

        internal void OnItemSelected(UserCellController cell, Vector2 position, Camera cam)
        {
            if (cell != null) fragment.OnUserSelected(cell.User, cell.Profile, position, cam);
        }

        internal void UpdateViews()
        {
            int usersCount = users.Count;

            UserCellController userItem = null;
            for (int i = 0; i < usersCount; i++)
            {
                if (items.Count <= i)
                {
                    var item = Instantiate(cellPrefab);
                    item.Inject(this, profileIcons, scoreIcon, subsSprites, scroll.viewport.rect.width);
                    item.RectTransform.SetParent(scroll.content, false);
                    item.RectTransform.SetAsLastSibling();
                    items.Add(item);
                }
                var user = users[i];
                bool isUser = core.State.IsCurrentUser(user);
                int place = i + 1;
                var profile = core.State.ProfileCache.GetCachedPlayer(user.Id);
                items[i].SetData(user, place, isUser, profile);
                if (isUser)
                {
                    userItem = items[i];
                    foreach (var mark in markControllers) mark.SetUser(profile, place);
                }
            }

            this.userItem = userItem;
            userTopMark.Hide(true);
            userBottomMark.Hide(true);

            for (int i = usersCount; i < items.Count; i++) Destroy(items[i].gameObject);
            items.RemoveRange(usersCount, items.Count - usersCount);

            if (userItem != null) StartCoroutine(CheckMarkForShowAsync());
        }

        internal void LostFocus()
        {
            if(userItem != null) userItem.HidePlayerMark();
        }

        private IEnumerator CheckMarkForShowAsync()
        {
            yield return new WaitForEndOfFrame();
            CheckMarkForShow();
        }

        private int SortUsers(ClanUser x, ClanUser y)
        {
            if (x.Role > y.Role) return -1;
            if (x.Role < y.Role) return 1;

            if (x.Score > y.Score) return -1;
            if (x.Score < y.Score) return 1;
            return 0;
        }

    }

}