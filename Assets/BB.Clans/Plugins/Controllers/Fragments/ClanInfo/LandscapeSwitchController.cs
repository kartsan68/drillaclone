using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    internal class TypeUnityEvent : UnityEvent<InfoType> {}

    internal class LandscapeSwitchController : MonoBehaviour
    {

        [SerializeField] private Toggle info;
        [SerializeField] private Toggle users;
        [SerializeField] private Toggle requests;

        private InfoType topType = InfoType.Info;
        private TypeUnityEvent onTopTypeChanged = new TypeUnityEvent();

        internal InfoType TopType { get { return topType; } }
        internal TypeUnityEvent OnTopTypeChanged { get { return onTopTypeChanged; } }

        private void Start()
        {
            info.onValueChanged.AddListener(isOn => OnTopChanged(InfoType.Info, isOn));
            users.onValueChanged.AddListener(isOn => OnTopChanged(InfoType.Users, isOn));
            requests.onValueChanged.AddListener(isOn => OnTopChanged(InfoType.Requests, isOn));
        }

        private void OnTopChanged(InfoType topType, bool isOn)
        {
            if (!isOn) return;
            this.topType = topType;
            onTopTypeChanged.Invoke(this.topType);
        }

    }

}