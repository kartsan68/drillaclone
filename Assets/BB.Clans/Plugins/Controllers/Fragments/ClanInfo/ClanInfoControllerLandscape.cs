using System.Collections;
using BlackBears.Chronometer;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Icons;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanInfo
{

    internal class ClanInfoControllerLandscape : MonoBehaviour
    {

        [Header("Поля клана")]
        [SerializeField]
        private Text[] worldPlaceTexts;
        [SerializeField] private Text localPlaceText;
        [SerializeField] private Text localPlaceCountryText;
        [SerializeField] private Image countryFlagImage;
        [SerializeField] private Text createDateText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text userAmountText;
        [SerializeField] private Text clanNameText;
        [SerializeField] private Text clanDescText;
        [SerializeField] private Text joinRequirementText;
        [SerializeField] private PredefinedIconView iconView;
        [SerializeField] private GameObject privacyMarker;

        [Header("Кнопки")]
        [SerializeField]
        private GameObject inClanButtonsContainer;
        [SerializeField] private Button editButton;
        [SerializeField] private Button inviteButton;
        [SerializeField] private Button exitButton;

        [SerializeField] private JoinButtonController joinButtonController;
        [SerializeField] private Button joinCancelButton;
        [SerializeField] private EngagedButtonController engagedButton;

        private ClanCore core;
        private CountriesContainer countries;
        private PredefinedIconsContainer clanIcons;
        private Clan clan;
        private bool disposed;

        internal Clan Clan
        {
            get { return clan; }
            set
            {
                if (core.State.IsUserClan(value)) value = core.State.Clan;
                clan = value;
                UpdateViews();
            }
        }

        internal Button EditButton { get { return editButton; } }
        internal Button InviteButton { get { return inviteButton; } }
        internal Button ExitButton { get { return exitButton; } }
        internal JoinButtonController JoinButtonController { get { return joinButtonController; } }
        internal Button JoinCancelButton { get { return joinCancelButton; } }

        internal void Inject(ClanCore core, CountriesContainer countries, PredefinedIconsContainer clanIcons)
        {
            this.core = core;
            this.countries = countries;
            this.clanIcons = clanIcons;
        }

        private void Start()
        {
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
            disposed = true;
        }

        internal void UpdateViews()
        {
            bool isUserClan = core.State.IsUserInClan && core.State.IsUserClan(clan);
            bool isUserClanMember = isUserClan && core.State.User.Role.IsClanMember();
            UpdateTexts();
            UpdateControlButtons(isUserClan, isUserClanMember);
            countryFlagImage.sprite = countries[clan.Country];
            //TODO: Включить обратно когда пуши серверные будут
            // pushNotificationContainer.SetActive(isUserClanMember);
        }

        private void UpdateTexts()
        {
            string worldPlace = ValidateHelper.PlaceToString(clan.WorldPlace);
            foreach (var wpt in worldPlaceTexts) wpt.text = worldPlace;

            localPlaceText.text = ValidateHelper.PlaceToString(clan.LocalPlace);
            localPlaceCountryText.text =  ClansLocalization.Get(clan.Country);
            var actualTime = TimeModule.ActualTime;
            var createMessage = DateUtils.SecondsToTimedMessage(actualTime - clan.CreateUtcDate);
            createDateText.text = ClansLocalization.Get(createMessage.key, createMessage.valueCount);
            scoreText.text = clan.Score.ToString();
            userAmountText.text = string.Format("{0}/{1}", clan.Amount, clan.Capacity);
            clanNameText.text = clan.Title;
            clanDescText.text = clan.Description;
            joinRequirementText.text = clan.JoinRequirement.ToString();
            iconView.SetIcon(clan.Icon, clanIcons);
            privacyMarker.SetActive(!clan.IsPublic);
        }

        private void UpdateControlButtons(bool isUserClan, bool isUserClanMember)
        {
            if (isUserClan)
            {
                if (isUserClanMember)
                {
                    inClanButtonsContainer.SetActive(true);
                    joinButtonController.gameObject.SetActive(false);
                    engagedButton.gameObject.SetActive(false);
                    joinCancelButton.gameObject.SetActive(false);

                    UpdateClanManipulateButtons();
                }
                else
                {
                    inClanButtonsContainer.SetActive(false);
                    joinButtonController.gameObject.SetActive(false);
                    engagedButton.gameObject.SetActive(false);
                    joinCancelButton.gameObject.SetActive(true);
                }
            }
            else if (clan.Amount >= clan.Capacity)
            {
                inClanButtonsContainer.SetActive(false);
                joinButtonController.gameObject.SetActive(false);
                engagedButton.gameObject.SetActive(true);
                joinCancelButton.gameObject.SetActive(false);
                engagedButton.SetBlockedByUserAmount();
            }
            else if (clan.JoinRequirement > core.State.ProfileCache.LocalPlayer.Score)
            {
                inClanButtonsContainer.SetActive(false);
                joinButtonController.gameObject.SetActive(false);
                engagedButton.gameObject.SetActive(true);
                joinCancelButton.gameObject.SetActive(false);
                engagedButton.SetBlockedByScore(clan.JoinRequirement);
            }
            else
            {
                inClanButtonsContainer.SetActive(false);
                joinButtonController.gameObject.SetActive(true);
                engagedButton.gameObject.SetActive(false);
                joinCancelButton.gameObject.SetActive(false);

                joinButtonController.SetDataFromClan(clan);
            }
        }

        private void UpdateClanManipulateButtons()
        {
            ClanRole role = core.State.User.Role;
            inviteButton.gameObject.SetActive(role.IsClanMember());
            editButton.gameObject.SetActive(role.IsModerator());
        }
    }

}