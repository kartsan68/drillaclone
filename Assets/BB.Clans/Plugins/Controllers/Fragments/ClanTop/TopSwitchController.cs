using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    internal class TopTypeUnityEvent : UnityEvent<TopType> {}

    internal class TopSwitchController : MonoBehaviour
    {

        [SerializeField] private Toggle localTop;
        [SerializeField] private Toggle worldTop;
        [SerializeField] private Toggle newTop;

        private TopType topType = TopType.Local;
        private TopTypeUnityEvent onTopTypeChanged = new TopTypeUnityEvent();

        internal TopType TopType { get { return topType; } }
        internal TopTypeUnityEvent OnTopTypeChanged { get { return onTopTypeChanged; } }

        private void Start()
        {
            localTop.onValueChanged.AddListener(isOn => OnTopChanged(TopType.Local, isOn));
            worldTop.onValueChanged.AddListener(isOn => OnTopChanged(TopType.World, isOn));
            newTop.onValueChanged.AddListener(isOn => OnTopChanged(TopType.Recomendations, isOn));
        }

        private void OnTopChanged(TopType topType, bool isOn)
        {
            if (!isOn) return;
            this.topType = topType;
            onTopTypeChanged.Invoke(this.topType);
        }

    }

}