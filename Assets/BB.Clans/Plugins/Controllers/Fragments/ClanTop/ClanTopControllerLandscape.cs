using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.Clans.Controller.CountrySelection;
using BlackBears.Clans.Model;
using BlackBears.Clans.View.ClanTop;
using BlackBears.Clans.View.Common;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    internal class ClanTopControllerLandscape : MonoBehaviour
    {

        [Header("Параметры топа")]

        [SerializeField] private TopSwitchController topSwitch;

        [SerializeField] private Image playerCountryImage;
        [SerializeField] private Text playerCountryName;
        [SerializeField] private CountryTopView[] countryTopViews;

        [SerializeField] private GameObject[] searchContainers;
        [SerializeField] private GameObject[] topContainers;
        [SerializeField] private GameObject topTabsContainer;
        [SerializeField] private GameObject countryTopContainer;
        [SerializeField] private GameObject createClanContainer;

        [SerializeField] private Button createClanButton;

        private ClanCore core;
        private CountriesContainer countries;
        private SearchingController searching;
        private TopType topType;
        private bool hasCountryTop;
        private string currentCountry;

        internal Button CreateClanButton { get { return createClanButton; } }
        internal TopSwitchController TopSwitch { get { return topSwitch; } }

        internal void Inject(ClanCore core, CountriesContainer countries, SearchingController searching, string currentCountry)
        {
            this.core = core;
            this.searching = searching;
            this.countries = countries;
            this.currentCountry = currentCountry;
        }

        private void Start()
        {
            searching.OnSearchingStarted.AddListener(OnSearchingStarted);
            searching.OnSearchingFinished.AddListener(OnSearchingFinished);
            topSwitch.OnTopTypeChanged.AddListener(OnTopChanged);

            playerCountryImage.sprite = countries[currentCountry];
            playerCountryName.text = ClansLocalization.Get(currentCountry).ToUpperInvariant();

            CheckCreateClanButton();
            SwitchTop(topSwitch.TopType);
        }

        internal void UpdateCountriesTop(List<CountryTopEntry> countriesEntry)
        {
            int index = 0;

            if (countriesEntry != null)
            {
                for (index = 0; index < countriesEntry.Count; index++)
                {
                    if (index >= countryTopViews.Length) break;
                    var view = countryTopViews[index];
                    view.gameObject.SetActive(true);

                    var sprite = countries[countriesEntry[index].name];
                    view.SetView(sprite, ClansLocalization.Get(countriesEntry[index].name));
                }
            }

            hasCountryTop = index > 0;
            if (!hasCountryTop)
            {
                countryTopContainer.SetActive(false);
            }
            else
            {
                if (topSwitch.TopType == TopType.World && !countryTopContainer.activeSelf)
                {
                    countryTopContainer.SetActive(true);
                }

                for (; index < countryTopViews.Length; index++)
                {
                    countryTopViews[index].gameObject.SetActive(false);
                }
            }
        }

        private void CheckCreateClanButton()
        {
            createClanContainer.gameObject.SetActive(!core.State.IsUserInClan);
        }

        private void SwitchTop(TopType topType)
        {
            if (this.topType == TopType.World) DisableWorld();
            if (topType == TopType.World) EnableWorld();

            this.topType = topType;
        }

        private void EnableWorld()
        {
            countryTopContainer.SetActive(hasCountryTop);
        }

        private void DisableWorld()
        {
            countryTopContainer.SetActive(false);
        }

        private void OnTopChanged(TopType topType)
        {
            if (this.topType == topType) return;
            SwitchTop(topType);
        }

        private void OnSearchingStarted()
        {
            foreach (var container in searchContainers) container.SetActive(true);
            foreach (var container in topContainers) container.SetActive(false);
            topTabsContainer.SetActive(false);
            countryTopContainer.SetActive(false);
            createClanContainer.SetActive(false);

        }

        private void OnSearchingFinished()
        {
            foreach (var container in searchContainers) container.SetActive(false);
            foreach (var container in topContainers) container.SetActive(true);
            topTabsContainer.SetActive(true);
            CheckCreateClanButton();
            SwitchTop(topType);
        }



    }

}