using System;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Views;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    [RequireComponent(typeof(RectTransform))]
    internal class ClanCellController : MonoBehaviour
    {

        [SerializeField] private Text titleText;
        [SerializeField] private Text placeText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text userAmountText;
        [SerializeField] private GameObject privacyMarker;
        [SerializeField] private GameObject userClanMarker;
        [SerializeField] private RectTransform userClanMoveMark;
        [SerializeField] private Button button;
        [SerializeField] private PredefinedIconView iconView;
        [SerializeField] private SpritePackView scoreImage;

        private RectTransform _rectTransform;

        private TopListController listController;
        private PredefinedIconsContainer clanIcons;
        private Clan clan;
        private long place;
        private bool isUserClan;

        internal RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }

        internal void Inject(TopListController listController, PredefinedIconsContainer clanIcons, SpritePack scorePack)
        {
            this.listController = listController;
            this.clanIcons = clanIcons;
            scoreImage.SetSpritePack(scorePack);
        }

        private void Start()
        {
            button.onClick.AddListener(OnClick);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(userClanMoveMark.gameObject);
        }

        internal void SetData(Clan clan, long place, bool isUserClan, bool canShowUserMark)
        {
            this.clan = clan;
            this.place = place;
            this.isUserClan = isUserClan;
            UpdateViews();
            if (canShowUserMark) ShowUserClanMoveMark(true);
        }

        internal void ShowUserClanMoveMark() => ShowUserClanMoveMark(false);
        internal void HideUserClanMoveMark() => HideUserClanMoveMark(false);

        private void UpdateViews()
        {
            if (clan == null) return;
            titleText.text = clan.Title;
            placeText.text = ValidateHelper.PlaceToString(place);
            scoreText.text = clan.Score.ToString();
            userAmountText.text = string.Format("{0}/{1}", clan.Amount, clan.Capacity);
            privacyMarker.SetActive(!clan.IsPublic);
            userClanMarker.SetActive(isUserClan);
            iconView.SetIcon(clan.Icon, clanIcons);
            HideUserClanMoveMark(true);
        }

        private void ShowUserClanMoveMark(bool force)
        {
            LeanTween.cancel(userClanMoveMark.gameObject);
            
            var pos = Vector2.zero;
            if (force) userClanMoveMark.anchoredPosition = pos;
            else LeanTween.moveAnchored(userClanMoveMark, pos, 0.2f).setEaseOutQuad();
        }

        private void HideUserClanMoveMark(bool force)
        {
            LeanTween.cancel(userClanMoveMark.gameObject);

            var pos = new Vector2(userClanMoveMark.rect.size.x, 0);
            if (force) userClanMoveMark.anchoredPosition = new Vector2(userClanMoveMark.rect.size.x, 0);
            else LeanTween.moveAnchored(userClanMoveMark, pos, 0.2f).setEaseInQuad();
        }

        private void OnClick()
        {
            listController.OnClanSelected(clan);
        }

    }

}