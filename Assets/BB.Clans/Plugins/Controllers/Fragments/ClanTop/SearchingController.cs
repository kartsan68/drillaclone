using System;
using System.Collections;
using BlackBears.Clans.Controller.CountrySelection;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    internal sealed class SearchingEvent : UnityEvent<ClanSearch> { }

    internal class SearchingController : MonoBehaviour
    {

        [UnityEngine.Serialization.FormerlySerializedAs("openSearchButton")]
        [SerializeField] private Button searchButton;
        [SerializeField] private GameObject inactiveSearchButton;
        [SerializeField] private StateExtendedInputField clanNameInputField;
        [SerializeField] private Toggle publicClanToggle;
        [SerializeField] private Toggle canJoinToggle;
        [SerializeField] private SearchCountrySelector countrySelector;
        [SerializeField] private Button searchButtonLandscape;

        private string baseCountry;
        private CountriesContainer countriesContainer;
        private bool countrySelectorInitiated = false;

        private UnityEvent onSearchingStarted = new UnityEvent();
        private UnityEvent onSearchingFinished = new UnityEvent();
        private SearchingEvent onSearchParamsChanged = new SearchingEvent();

        internal bool IsSearchEnabled { get; private set; }
        internal bool IsInputFocused { get; private set; }

        internal UnityEvent OnSearchingStarted { get { return onSearchingStarted; } }
        internal UnityEvent OnSearchingFinished { get { return onSearchingFinished; } }
        internal SearchingEvent OnSearchParamsChanged { get { return onSearchParamsChanged; } }

        internal void Inject(CountriesContainer countriesContainer, string baseCountry, string[] importantCountries)
        {
            this.countriesContainer = countriesContainer;
            this.baseCountry = baseCountry;
            countrySelector.Inject(countriesContainer, importantCountries);
        }

        private void Start()
        {
            searchButton.onClick.AddListener(OnOpenSearchClick);
#if BBGC_LANDSCAPE
            searchButtonLandscape.onClick.AddListener(OnOpenSearchClick);
#endif
            clanNameInputField.inputType = InputField.InputType.AutoCorrect;
            clanNameInputField.characterLimit = ValidateHelper.clanNameMaxLength;

            clanNameInputField.onValueChanged.AddListener(_ => CheckSearchButtonActivity());
            publicClanToggle.onValueChanged.AddListener(_ => CheckSearchButtonActivity());
            canJoinToggle.onValueChanged.AddListener(_ => CheckSearchButtonActivity());
            countrySelector.OnCountryChange.AddListener(_ => CheckSearchButtonActivity());
        }

        internal void CloseSearch()
        {
            if (!IsSearchEnabled) return;
            IsSearchEnabled = false;
            clanNameInputField.gameObject.SetActive(false);
            onSearchingFinished.Invoke();
            StopAllCoroutines();
            CheckSearchButtonActivity();
        }

        private void OnOpenSearchClick()
        {
            if (!IsSearchEnabled) EnableSearch();
            else SearchNotify();
        }

        private void EnableSearch()
        {
            IsSearchEnabled = true;
            clanNameInputField.text = string.Empty;
            clanNameInputField.gameObject.SetActive(true);
            clanNameInputField.Select();
            onSearchingStarted.Invoke();
            publicClanToggle.isOn = true;
            canJoinToggle.isOn = true;

            StartCoroutine(PostInitialize(!countrySelectorInitiated));
            countrySelectorInitiated = true;
        }

        private void SearchNotify()
        {
            string text = clanNameInputField.text;
            string country = countrySelector.Selected;
            country = countriesContainer.IsAnyCountryFlag(country) ? null : country;
            bool onlyPublic = !publicClanToggle.isOn;
            bool canJoin = !canJoinToggle.isOn;
            onSearchParamsChanged.Invoke(new ClanSearch(text, country, onlyPublic, canJoin));
        }

        private IEnumerator PostInitialize(bool initCountrySelector)
        {
            yield return new WaitForEndOfFrame();
            if (initCountrySelector) countrySelector.Initiate(baseCountry);
            else countrySelector.Select(baseCountry);
            CheckSearchButtonActivity();
            clanNameInputField.UpdateVisualState();
        }

        private void CheckSearchButtonActivity()
        {
            bool buttonActive = !IsSearchEnabled;
            buttonActive = buttonActive || !publicClanToggle.isOn;
            buttonActive = buttonActive || !canJoinToggle.isOn;
            // Check country selector
            if (!buttonActive)
            {
                var selected = countrySelector.Selected;
                buttonActive = !countriesContainer.IsAnyCountryFlag(selected);
            }
            // Check input field
            if (!buttonActive)
            {
                var clanName = clanNameInputField.text;
                buttonActive = ValidateHelper.GetClanNameValidationError(clanName) == null;
            }
            searchButton.gameObject.SetActive(buttonActive);
            inactiveSearchButton.SetActive(!buttonActive);
#if BBGC_LANDSCAPE
            searchButtonLandscape.gameObject.SetActive(buttonActive);
#endif
        }

    }

}