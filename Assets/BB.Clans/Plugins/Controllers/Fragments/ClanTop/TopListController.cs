using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.Clans.Controller.Common;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.Loader;
using BlackBears.GameCore.Configurations;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    internal class TopListController : MonoBehaviour
    {

        [SerializeField] private ClanCellController cellPrefab;
        [SerializeField] private RectTransform root;
        [SerializeField] private ScrollRect scroll;
        [SerializeField] private EdgeAnchoredMark topEdgeMark;
        [SerializeField] private EdgeAnchoredMark bottomEdgeMark;
        [SerializeField] private TopMarkController[] markControllers;
        [SerializeField] private GameObject clansNotFoundedAlert;
        [SerializeField] private GameObject recomendationsTitle;
        [SerializeField] private RectTransform loaderTransform;
        [SerializeField] private LayoutElement loaderLayoutElement;
        [SerializeField] private float loaderBaseHeight = 68;
        [SerializeField] private CircleLoader loader;

        private ClanCore core;
        private PredefinedIconsContainer clanIcons;
        private SpritePack scorePack;
        private ClanTopFragment fragment;
        private SearchingController searching;

        private TopType currentTop;
        private long place;
        private int currentPage;
        private bool _allLoaded = false;
        private bool onLoad = true;

        private bool onSearchMode;
        private ClanSearch search;

        private TopResponse recomendationsResponse;

        private Clan userClan;
        private ClanCellController userClanCell;
        private List<ClanCellController> cells = new List<ClanCellController>();

        private bool disposed;

        private bool AllLoaded
        {
            get { return _allLoaded; }
            set
            {
                _allLoaded = value;
                loaderLayoutElement.minHeight = value ? 0 : loaderBaseHeight;
            }
        }

        internal void Inject(ClanCore core, PredefinedIconsContainer clanIcons, SpritePack scorePack,
            ClanTopFragment fragment, SearchingController searching)
        {
            this.core = core;
            this.clanIcons = clanIcons;
            this.scorePack = scorePack;
            this.fragment = fragment;
            this.searching = searching;
        }

        private void Start()
        {
            topEdgeMark.Hide(true);
            bottomEdgeMark.Hide(true);
            scroll.onValueChanged.AddListener(OnScroll);
            searching.OnSearchingStarted.AddListener(OnStartSearching);
            searching.OnSearchingFinished.AddListener(OnFinishSearching);
            searching.OnSearchParamsChanged.AddListener(OnSearchParamsChanged);

            loader.OnShow.AddListener(LoadNextPage);
        }

        private void OnDestroy()
        {
            disposed = true;
        }

        internal void OnFocus()
        {
            if (userClanCell != null) userClanCell.ShowUserClanMoveMark();
        }

        internal void OnLostFocus()
        {
            if (userClanCell != null) userClanCell.HideUserClanMoveMark();
        }

        #region TopMode

        internal void SwitchToTopType(TopType top, bool force)
        {
            if (!force && currentTop == top) return;

            currentTop = top;
            HideClanCells();
            fragment.OnListUpdateStarted();

            AllLoaded = false;
            currentPage = 1;
            LoadTop(currentTop, currentPage, true);
        }

        internal void OnClanSelected(Clan clan)
        {
            fragment.OpenClan(clan);
        }

        internal void InvalidateCache()
        {
            recomendationsResponse = null;
        }

        private void LoadTop(TopType top, int page, bool reload)
        {
            if (onSearchMode) return;
            if ((AllLoaded || onLoad) && !reload) return;
            onLoad = true;

            Block<TopResponse> onSuccess = response =>
            {
                if (disposed) return;
                StartCoroutine(OnTopLoadFinished(response, top, page, reload));
            };
            FailBlock onFail = (c, m) => OnPageLoadFailed(c, m, top, page, reload);

            switch (top)
            {
                case TopType.Local:
                    core.GetLocalTop(page, GetLocalCountry(), onSuccess, onFail);
                    break;
                case TopType.World:
                    core.GetWorldTop(page, onSuccess, onFail);
                    break;
                case TopType.Recomendations:
                    if (recomendationsResponse == null) core.GetRecommendedClans(onSuccess, onFail);
                    else onSuccess(recomendationsResponse);
                    break;
            }
        }

        private IEnumerator OnTopLoadFinished(TopResponse response, TopType top, int page, bool reload)
        {
            yield return new WaitForSecondsRealtime(0.25f);
            if (disposed) yield break;

            if (reload)
            {
                fragment.OnListUpdateFinished();
                userClanCell = null;
                topEdgeMark.Hide(true);
                bottomEdgeMark.Hide(true);
            }

            if (currentTop != top || currentPage != page || onSearchMode) yield break;
            onLoad = false;
            var clans = response.clans;
            if (top == TopType.Recomendations)
            {
                AllLoaded = true;
                if (searching.IsSearchEnabled) recomendationsTitle.SetActive(true);
                this.recomendationsResponse = response;
            }
            else
            {
                recomendationsTitle.SetActive(false);
            }

            var userClan = response.userClan;
            UpdateList(clans, userClan, reload, false);

            if (reload && userClanCell != null) userClanCell.ShowUserClanMoveMark();

            var wtr = response as WorldTopResponse;
            if (page == 1 && wtr != null)
            {
                fragment.OnCountriesTopCome(wtr.countries);
            }

            loader.Hide();
        }

        private void OnPageLoadFailed(long errorCode, string errorMessage, TopType top, int page, bool reload)
        {
            Debug.LogFormat("OnPageLoadFailed: {0}; {1}", errorCode, errorMessage);
            if (disposed) return;
            if (reload) fragment.OnListUpdateFailed(() => SwitchToTopType(top, true));
            onLoad = false;

            loader.Hide();
        }

        #endregion TopMode

        #region SearchMode

        private void OnStartSearching()
        {
            if (currentTop != TopType.Recomendations)
            {
                fragment.OnListUpdateStarted();
                HideClanCells();
                this.currentTop = TopType.Recomendations;
                this.currentPage = 1;
                LoadTop(TopType.Recomendations, 1, true);
            }
            else
            {
                recomendationsTitle.SetActive(true);
            }
            clansNotFoundedAlert.SetActive(false);
        }

        private void OnFinishSearching()
        {
            onSearchMode = false;
            HideClanCells();
            SwitchToTopType(currentTop, true);
            clansNotFoundedAlert.SetActive(false);
            recomendationsTitle.SetActive(false);
        }

        private void OnSearchParamsChanged(ClanSearch search)
        {
            onSearchMode = true;
            this.currentPage = 1;
            this.search = search;
            AllLoaded = false;

            SearchClans(search, currentPage, true);
        }

        private void SearchClans(ClanSearch search, int page, bool reload)
        {
            if (onLoad && !reload) return;
            onLoad = true;
            if (reload) fragment.OnListUpdateStarted();
            core.SearchClan(search, currentPage, clans => OnSearchClanLoadFinished(clans, search, page, reload),
                (c, m) => OnSearchClanLoadFailed(c, m, search, page, reload));
        }

        private void OnSearchClanLoadFinished(List<Clan> clans, ClanSearch search, int page, bool reload)
        {
            if (!onSearchMode || currentPage != page || !this.search.Equals(search))
            {
                if (reload) fragment.OnListUpdateFinished();
                return;
            }

            onLoad = false;
            loader.Hide();

            bool showNotFoundInfo = reload && (clans == null || clans.Count == 0);
            clansNotFoundedAlert.SetActive(showNotFoundInfo);
            recomendationsTitle.SetActive(showNotFoundInfo);

            if (showNotFoundInfo)
            {
                onSearchMode = false;
                LoadTop(TopType.Recomendations, 1, true);
            }
            else
            {
                UpdateList(clans, null, reload, true);
                if (reload) fragment.OnListUpdateFinished();
            }
        }

        private void OnSearchClanLoadFailed(long errorCode, string errorMessage, ClanSearch search, int page, bool reload)
        {
            Debug.LogFormat("OnSearchClanLoadFailed: {0}; {1}", errorCode, errorMessage);
            if (reload) fragment.OnListUpdateFinished();
            loader.Hide();
            if (!onSearchMode) return;
            if (currentPage != page || !this.search.Equals(search)) return;
            onLoad = false;
        }

        #endregion SearchMode

        internal void CheckMarksForShow()
        {
            if (userClan == null) return;
            if (userClanCell == null)
            {
                bottomEdgeMark.Show(false);
                topEdgeMark.Hide(false);
            }
            else
            {
                var position = userClanCell.RectTransform.anchoredPosition;
                position.y += scroll.content.anchoredPosition.y;

                if (position.y > 0) topEdgeMark.Show(false);
                else topEdgeMark.Hide(false);

                if (position.y < -scroll.viewport.rect.height) bottomEdgeMark.Show(false);
                else bottomEdgeMark.Hide(false);
            }
        }

        private string GetLocalCountry()
        {
            return core.State.IsUserInClan ? core.State.Clan.Country : core.State.ProfileCache.LocalPlayer.Country;
        }

        private void UpdateList(List<Clan> clans, Clan userClan, bool reload, bool useWorldPlace)
        {
            if (clans == null || clans.Count == 0)
            {
                AllLoaded = true;
                CheckMarksForShow();
                return;
            }

            int currentIndex = 0;
            if (reload)
            {
                this.userClan = userClan;
                if (userClan != null)
                {
                    long userClanPlace = 0;
                    switch (currentTop)
                    {
                        case TopType.World: userClanPlace = userClan.WorldPlace; break;
                        case TopType.Local: userClanPlace = userClan.LocalPlace; break;
                    }
                    foreach (var mark in markControllers) mark.SetClan(userClan, userClanPlace);
                }

                place = 0;
                for (currentIndex = 0; currentIndex < clans.Count; currentIndex++)
                {
                    if (cells.Count <= currentIndex) break;
                    PrepareCell(cells[currentIndex], clans[currentIndex], userClan, reload, useWorldPlace);
                }
            }

            while (currentIndex < clans.Count)
            {
                var cell = Instantiate(cellPrefab);
                cell.Inject(this, clanIcons, scorePack);
                cell.transform.SetParent(root, false);
                cells.Add(cell);
                PrepareCell(cell, clans[currentIndex], userClan, reload, useWorldPlace);

                currentIndex++;
            }

            if (reload && currentIndex < cells.Count)
            {
                for (int i = currentIndex; i < cells.Count; i++)
                {
                    Destroy(cells[i].gameObject);
                }
                cells.RemoveRange(currentIndex, cells.Count - currentIndex);
            }

            CheckMarksForShow();
            loaderTransform.SetAsLastSibling();
        }

        private void PrepareCell(ClanCellController cell, Clan clan, Clan userClan,
            bool reload, bool useWorldPlace)
        {
            if (!useWorldPlace) place += 1;

            bool isUserClan = clan != null && userClan != null && string.Equals(clan.Id, userClan.Id);
            cell.SetData(clan, useWorldPlace ? clan.WorldPlace : place, isUserClan, !reload);
            cell.gameObject.SetActive(true);
            if (isUserClan) userClanCell = cell;
        }

        private void HideClanCells() { foreach (var cell in cells) cell.gameObject.SetActive(false); }

        private void OnScroll(Vector2 pos)
        {
            CheckMarksForShow();

            if (onLoad || AllLoaded || pos.y > 0.01f) return;
            if (loader.IsWork) return;

            loader.Show();
        }

        private void LoadNextPage()
        {
            if (disposed || onLoad || AllLoaded) return;

            currentPage += 1;
            if (onSearchMode) SearchClans(search, currentPage, false);
            else LoadTop(currentTop, currentPage, false);
        }

    }

}