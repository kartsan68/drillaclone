﻿using System;
using System.Collections.Generic;
using BlackBears.Clans.Model;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    internal class ClanTopFragment : Fragment
    {

        [SerializeField] private Text title;
        [SerializeField] private Image background;
        [SerializeField] private ClanTopRollable rollable;
        [SerializeField] private ClanTopControllerLandscape controllerLandscape;
        [SerializeField] private TopListController topList;
        [SerializeField] private SearchingController searching;

        private ClanCore core;
        private FragmentsController fragmentsController;

        internal override FragmentKind Kind { get { return FragmentKind.ClanTop; } }

        internal void Inject(ClanCore core, ClanFullSettings settings,
            FragmentsController fragmentsController)
        {
            this.core = core;
            this.fragmentsController = fragmentsController;

            string clanCountry = core.State.IsUserInClan ? core.State.Clan.Country : null;
            string userCountry = core.State.ProfileCache.LocalPlayer.Country;

            var config = settings.configuration;

#if BBGC_LANDSCAPE
            controllerLandscape.Inject(core, config.Countries, searching, clanCountry ?? userCountry);
#else
            rollable.Inject(core, config.Countries, searching, clanCountry ?? userCountry);
#endif

            topList.Inject(core, config.ClanIcons, settings.scoreIconPack, this, searching);

            string[] importantCountries;
            if (clanCountry != null && !string.Equals(clanCountry, userCountry))
                importantCountries = new string[] { userCountry, clanCountry };
            else
                importantCountries = new string[] { userCountry };

            searching.Inject(config.Countries, config.Countries.AnyCountry, importantCountries);
        }

        protected override void Start()
        {
            base.Start();

#if BBGC_LANDSCAPE
            controllerLandscape.TopSwitch.OnTopTypeChanged.AddListener(OnTopChanged);
            controllerLandscape.CreateClanButton.onClick.AddListener(OnCreateClanRequest);
            topList.SwitchToTopType(controllerLandscape.TopSwitch.TopType, true);
#else
            rollable.TopSwitch.OnTopTypeChanged.AddListener(OnTopChanged);
            rollable.CreateClanButton.onClick.AddListener(OnCreateClanRequest);
            topList.SwitchToTopType(rollable.TopSwitch.TopType, true);
#endif
            core.State.OnClanChanged += OnClanChanged;


        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            core.State.OnClanChanged -= OnClanChanged;
        }

        internal override void OnFocus()
        {
            base.OnFocus();
            topList.OnFocus();
        }

        internal override void LostFocus()
        {
            base.LostFocus();
            topList.OnLostFocus();
        }

        internal override bool BackRequest()
        {
            return TryToMoveBackInternal();
        }

        internal void OpenClan(Clan clan)
        {
            if (clan == null) return;

            fragmentsController.ShowClanInfo(clan);
        }

        internal void OnListUpdateStarted()
        {
            ShowLoader(title.text, background.color);
        }

        internal void OnListUpdateFinished()
        {
            if (Disposed) return;
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
        }

        internal void OnListUpdateFailed(Block repeatBlock)
        {
            if (Disposed) return;
            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;
                HideLoader();
                ShowError(repeatBlock);
            });
        }

        protected override bool ProcessBackPress()
        {
            if (TryToMoveBackInternal()) return true;
            return base.ProcessBackPress();
        }

        protected override void OnScrollToTopFinished()
        {
            base.OnScrollToTopFinished();
            topList.CheckMarksForShow();
        }

        private bool TryToMoveBackInternal()
        {
            if (searching.IsSearchEnabled)
            {
                searching.CloseSearch();
                return true;
            }
            return false;
        }

        private void OnTopChanged(TopType top) => topList.SwitchToTopType(top, false);
        private void OnClanChanged(Clan clan)
        {
            if (searching.IsSearchEnabled) searching.CloseSearch();
            topList.InvalidateCache();
#if BBGC_LANDSCAPE
            topList.SwitchToTopType(controllerLandscape.TopSwitch.TopType, true);
#else
            topList.SwitchToTopType(rollable.TopSwitch.TopType, true);
#endif
        }

        private void OnCreateClanRequest()
        {
            if (core.State.IsUserInClan) return;
            fragmentsController.ShowClanEdit(CloseRequest, null);
        }

        internal void OnCountriesTopCome(List<CountryTopEntry> countries)
        {
#if BBGC_LANDSCAPE
            controllerLandscape.UpdateCountriesTop(countries);
#else
            rollable.UpdateCountriesTop(countries);
#endif
        }
    }

}