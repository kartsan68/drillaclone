﻿using BlackBears.Clans.Model;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanTop
{

    internal class TopMarkController : MonoBehaviour
    {

        [SerializeField] private Text nameText;
        [SerializeField] private Text placeText;

        internal void SetClan(Clan clan, long place)
        {
            nameText.text = clan.Title;
            placeText.text = string.Format("#{0}", place);
        }

    }

}