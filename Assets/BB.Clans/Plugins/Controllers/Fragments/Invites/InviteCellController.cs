using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Views;
using BlackBears.Referals;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Invites
{

    internal class InviteCellController : MonoBehaviour
    {

        [SerializeField] private PredefinedIconView icon;
        [SerializeField] private Text nicknameText;
        [SerializeField] private SpritePackView currencyIconView;

        private PredefinedIconsContainer iconsContainer;
        private IPlayerProfile profile;

        internal void Inject(PredefinedIconsContainer iconsContainer,
            SpritePack currencyIconPack)
        {
            this.iconsContainer = iconsContainer;
            if (currencyIconView != null)
                currencyIconView.SetSpritePack(currencyIconPack);
        }

        internal void SetData(IPlayerProfile profile)
        {
            this.profile = profile;

            UpdateView();
        }

        private void UpdateView()
        {
            icon.SetIcon(profile.Icon, iconsContainer);
            nicknameText.text = profile.Nickname;
        }

    }

}