﻿using System;
using System.Collections.Generic;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.Common;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Features.Referals;
using BlackBears.GameCore.Views;
using BlackBears.Referals;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Invites
{

    internal class InvitesFragment : Fragment
    {

        [SerializeField] private InviteCellController availablePrefab;
        [SerializeField] private InviteCellController collectedPrefab;
        [SerializeField] private UncollectedInviteController uncollectedPrefab;

        [SerializeField] private Text title;
        [SerializeField] private Image background;
        [SerializeField] private GameObject clanCodeContainer;
        [SerializeField] private Text clanCodeDescription;
        [SerializeField] private Button clanCodeButton;
        [SerializeField] private Text linkText;
        [SerializeField] private Button linkCodeButton;
        [SerializeField] private Text rewardText;

        [SerializeField] private RectTransform availableRoot;
        [SerializeField] private RectTransform collectedRoot;
        [SerializeField] private RectTransform uncollectedRoot;

        [SerializeField] private SpritePackView currencyImage;

        [SerializeField] private GameObject noEventsCap;

        private ClanCore core;
        private ReferalsFeature referals;
        private PredefinedIconsContainer playerIcons;
        private NotificationsFeature notifications;
        private RewardInformation rewardInformation;
        private SpritePack currencyIconPack;

        private Dictionary<string, RewardController> rewardButtons = new Dictionary<string, RewardController>();

        internal override FragmentKind Kind { get { return FragmentKind.Invites; } }

        internal void Inject(ClanCore core, ReferalsFeature referals,
            PredefinedIconsContainer playerIcons, SpritePack currencyIconPack,
            NotificationsFeature notifications)
        {
            this.core = core;
            this.referals = referals;
            this.playerIcons = playerIcons;
            this.notifications = notifications;
            this.currencyIconPack = currencyIconPack;
            currencyImage.SetSpritePack(currencyIconPack);
            InviteRewardCheck();
        }

        protected override void Start()
        {
            base.Start();

            if (referals == null)
            {
                CloseRequest();
                return;
            }

            clanCodeButton.onClick.AddListener(OnCopyClanCodeClick);
            linkCodeButton.onClick.AddListener(OnCopyLinkClick);

            LoadAll();
        }

        private void LoadAll()
        {
            ShowLoader(title.text, background.color);
            if (!referals.Referals.HasConnectionLink) LoadLink();
            else if (rewardInformation == null) LoadInvites();
            else ValidateProfiles();
        }

        private void UpdateView()
        {
            linkText.text = referals.Referals.ConnectionLink;
            rewardText.text = string.Format("+{0}", referals.Referals.InviteReward);
            UpdateClanCodeViews();
            UpdateRewardList();
        }

        private void InviteRewardCheck()
        {
            if (referals.Referals.InviteReward <= 0)
                rewardText.transform.parent.gameObject.SetActive(false);
        }


        private void UpdateClanCodeViews()
        {
            var inClan = core.State.IsUserInClan;
            clanCodeContainer.SetActive(inClan);
            if (inClan)
            {
                clanCodeDescription.text = string.Format(ClansLocalization.Get(LocalizationKeys.clanSearchHelp),
                    ValidateHelper.FormatClanTag(core.State.Clan.Tag));
            }
        }

        private void UpdateRewardList()
        {
            bool hasEvents = false;
            foreach (var item in rewardInformation.rewardedItems)
            {
                hasEvents = true;
                InviteCellController controller;

                if (item.RewardTaken)
                {
                    controller = Instantiate(collectedPrefab);
                    controller.transform.SetParent(collectedRoot, false);
                }
                else
                {
                    controller = Instantiate(availablePrefab);
                    var buttonController = controller.GetComponent<RewardController>();
                    buttonController.Inject(item, referals.Referals.InviteReward);
                    buttonController.OnClick.AddListener(TakeRewardRequest);
                    controller.transform.SetParent(availableRoot, false);
                    rewardButtons[item.InstallationId] = buttonController;
                }

                controller.Inject(playerIcons, currencyIconPack);
                controller.SetData(core.State.ProfileCache.GetCachedPlayer(item.InstallerId));
            }

            for (int i = 0; i < rewardInformation.clickTimestamps.Count; i++)
            {
                hasEvents = true;

                var controller = Instantiate(uncollectedPrefab);
                controller.transform.SetParent(uncollectedRoot, false);
                controller.SetTimestamp(rewardInformation.clickTimestamps[i]);
            }

            noEventsCap.SetActive(!hasEvents);
        }

        private void LoadLink()
        {
            referals.Referals.LoadLink(core.State.ProfileCache.LocalPlayer.Id, OnLinkLoaded, OnLinkLoadFailed);
        }

        private void LoadInvites()
        {
            referals.Referals.LoadRewardInformation(core.State.ProfileCache.LocalPlayer.Id,
                OnInvitesLoaded, OnInvitesLoadFailed);
        }

        private void ValidateProfiles()
        {
            List<string> playerIds = new List<string>();
            foreach (var item in rewardInformation.rewardedItems) playerIds.Add(item.InstallerId);
            core.State.ProfileCache.LoadPlayersAsync(playerIds, OnProfilesValidated, OnProfilesValidateFailed);
        }

        private void OnLinkLoaded(string link)
        {
            if (Disposed) return;
            LoadInvites();
        }

        private void TakeRewardRequest(RewardedItem item)
        {
            core.TakeInviteReward(item, OnRewardTaken, OnRewardTakeFailed);
        }

        private void OnCopyClanCodeClick()
        {
            NativeUtils.ShareUrl(null, string.Format(ClansLocalization.Get(LocalizationKeys.shareClanTagMessage),
                ValidateHelper.FormatClanTag(core.State.Clan.Tag)), null, null);
        }
        private void OnCopyLinkClick()
        {
            NativeUtils.ShareUrl(referals.Referals.ConnectionLink,
                ClansLocalization.Get(LocalizationKeys.shareConnectionLinkMessage),
                null, null);
        }

        private void OnInvitesLoaded(RewardInformation information)
        {
            if (Disposed) return;
            this.rewardInformation = information;
            ValidateProfiles();
        }

        private void OnProfilesValidated()
        {
            if (Disposed) return;
            UpdateView();
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
        }

        private void OnRewardTaken(RewardTakeAnswer answer)
        {
            if (Disposed) return;

            foreach (var reward in answer.rewards)
            {
                RewardController controller;
                rewardButtons.TryGetValue(reward.InstallationId, out controller);
                if (controller == null) continue;
                controller.Item.Take();
                controller.RewardTaken();
            }
        }

        private void OnLinkLoadFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnLinkLoadFailed: {0}; {1}", errorCode, errorMessage);
            if (Disposed) return;
            ShowDelayedError();
        }

        private void OnInvitesLoadFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnInvitesLoadFailed: {0}; {1}", errorCode, errorMessage);
            if (Disposed) return;
            ShowDelayedError();
        }

        private void OnProfilesValidateFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnProfilessValidateFailed: {0}; {1}.", errorCode, errorMessage);
            if (Disposed) return;
            ShowDelayedError();
        }

        private void OnRewardTakeFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnRewardTakeFailed: {0}; {1}.", errorCode, errorMessage);
            if (Disposed) return;
            HideLoader();
            notifications.ShowNotification(new Notification(NotificationType.Warning,
                ClansLocalization.Get(LocalizationKeys.notificationErrorTakeInviteReward)));
        }

        private void ShowDelayedError()
        {
            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                HideLoader();
                ShowError(LoadAll);
            });
        }

    }

}