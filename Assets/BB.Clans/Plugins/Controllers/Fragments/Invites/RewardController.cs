using BlackBears.Clans.View.Loader;
using BlackBears.Referals;
using BlackBears.Utils.Components;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Invites
{

    internal class RewardController : MonoBehaviour
    {
        
        internal class RewardedItemEvent : UnityEvent<RewardedItem> {}

        [SerializeField] private Text rewardText;
        [SerializeField] private Button button;
        [SerializeField] private ButtonLoaderAnimator buttonAnimator;

        [SerializeField] private CanvasGroup awardGroup;
        [SerializeField] private CanvasGroup takenGroup;

        private RewardedItemEvent onClick = new RewardedItemEvent();

        internal RewardedItem Item { get; private set; }
        internal RewardedItemEvent OnClick { get { return onClick; } }

        internal void Inject(RewardedItem item, double reward)
        {
            Item = item;
            rewardText.text = string.Format("+{0}", reward);
        }

        private void Start()
        {
            awardGroup.alpha = 1f;
            takenGroup.alpha = 0f;
            button.onClick.AddListener(OnButtonClick);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
            LeanTween.cancel(awardGroup.gameObject);
            LeanTween.cancel(takenGroup.gameObject);
        }

        internal void RewardTaken()
        {
            buttonAnimator.StopLoadingAnimation(pauseTime: 0.5f, hideButtonOnSuccess: false, 
                onStopped: SwitchStateAsync);
        }

        internal void RewardNotTaken()
        {
            button.interactable = true;
            buttonAnimator.StopLoadingAnimation(success: false, hideButtonOnSuccess: false);
        }

        private void OnButtonClick()
        {
            button.interactable = false;
            buttonAnimator.StartLoadingAnimation();
            onClick.Invoke(Item);
        }

        private void SwitchStateAsync()
        {
            LeanTween.delayedCall(gameObject, 0.5f, SwitchState);
        }

        private void SwitchState()
        {
            LeanTween.alphaCanvas(awardGroup, 0f, 0.2f);
            LeanTween.alphaCanvas(takenGroup, 1f, 0.2f);
        }

    }

}