using BlackBears.Clans.Model.Helpers;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Invites
{

    internal class UncollectedInviteController : MonoBehaviour
    {
        
        [SerializeField] private Text timeText;

        internal void SetTimestamp(long timestamp)
        {
            timeText.text = DateUtils.FromUnixTimestamp(timestamp).ToMessageDate();
        }

    }
    
}