﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments
{

    [RequireComponent(typeof(CanvasGroup))]
    internal class FragmentErrorController : MonoBehaviour
    {

        [SerializeField] private Text titleText;
        [SerializeField] private Text messageText;
        [SerializeField] private Button repeatButton;

        private CanvasGroup canvasGroup;
        private Block repeatBlock;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0f;
            gameObject.SetActive(false);
            repeatButton.onClick.AddListener(OnRepeatButtonClick);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void Show(Block inRepeatBlock, string title = LocalizationKeys.errorCommonTitle,
            string message = LocalizationKeys.errorCommonDescription)
        {
            if (repeatBlock != null || inRepeatBlock == null) return;

            repeatBlock = inRepeatBlock;
            titleText.text =  ClansLocalization.Get(title);
            messageText.text = ClansLocalization.Get(message);
            StartShowAnimation();
        }

        private void OnRepeatButtonClick()
        {
            if (this.repeatBlock == null) return;

			var repeatBlock = this.repeatBlock;
			this.repeatBlock = null;
            StartHideAnimation();
            repeatBlock();
        }

        private void StartShowAnimation()
        {
            LeanTween.cancel(gameObject);
            gameObject.SetActive(true);
			transform.SetAsLastSibling();
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        private void StartHideAnimation()
        {
            LeanTween.cancel(gameObject);
            LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f)
                .setOnComplete(() => gameObject.SetActive(false));
        }

    }

}