﻿using System.Collections.Generic;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments
{

	[RequireComponent(typeof(CanvasGroup))]
    internal class FragmentLoaderController : MonoBehaviour
    {

		[SerializeField] private Text titleText;
		[SerializeField] private Image background;

		private CanvasGroup canvasGroup;
		private HashSet<object> requesters = new HashSet<object>();
		private bool initiated = false;

        internal Color TextColor => titleText.color;

        private void OnDestroy()
		{
			LeanTween.cancel(gameObject);
		}

		private void FirstInit()
		{
			canvasGroup = GetComponent<CanvasGroup>();
			canvasGroup.alpha = 0f;
			gameObject.SetActive(false);
			initiated = true;
		}

		internal void ShowRequest(object requester, string text, Color bgColor, Color textColor)
		{
			if (!initiated) FirstInit();

			if (requester == null || !requesters.Add(requester)) return;
			titleText.text = text;
			titleText.color = textColor;
			transform.SetAsLastSibling();
			background.color = bgColor;
			if (requesters.Count == 1) Show();
		}

		internal void HideRequest(object requester)
		{
			if (!requesters.Remove(requester)) return;
			if (requesters.Count == 0) Hide();
		}

		private void Show()
		{
			LeanTween.cancel(gameObject);
			gameObject.SetActive(true);
			LeanTween.alphaCanvas(canvasGroup, 1f, 0.1f);
		}

		private void Hide()
		{
			LeanTween.cancel(gameObject);
			LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f)
				.setOnComplete(() => gameObject.SetActive(false));
		}

    }

}