using System.Collections.Generic;

namespace BlackBears.Clans.Controller.Fragments
{
    public interface IDataInjectingFragment
    {
        void Inject(Dictionary<string, object> data);
    }
}