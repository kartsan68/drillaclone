using BlackBears.GameCore;
using BlackBears.GameCore.Features.CoreDefaults;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanEdit
{

    internal class ClanEditSubmitControl : MonoBehaviour
    {

        [System.NonSerialized] internal bool isClanCreate;

        [UnityEngine.Serialization.FormerlySerializedAs("balanceText")]
        [SerializeField] private Text balance;
        [SerializeField] private Text[] neededAmountTexts;
        [SerializeField] private Button submitButton;
        [SerializeField] private GameObject disabledButton;

        private IGameAdapter gameAdapter;
        private ClanParams clanParams;

        internal Button SubmitButton { get { return submitButton; } }
        internal bool ButtonActive
        {
            get { return submitButton.gameObject.activeSelf; }
            set
            {
                submitButton.gameObject.SetActive(value);
                disabledButton.SetActive(!value);
            }
        }

        internal void Inject(IGameAdapter gameAdapter, ClanParams clanParams)
        {
            this.gameAdapter = gameAdapter;
            this.clanParams = clanParams;
        }

        private void Start()
        {
            balance.text = gameAdapter.CurrencyAmount;

            double cost = isClanCreate ? clanParams.createClanCost : clanParams.updateClanCost;
            foreach (var nat in neededAmountTexts) nat.text = gameAdapter.AmountToPrettyString(cost);
        }

    }

}