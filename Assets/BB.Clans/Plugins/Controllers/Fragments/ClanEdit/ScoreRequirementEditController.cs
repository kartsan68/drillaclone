using System;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanEdit
{

    internal class ScoreRequirementEditController : MonoBehaviour
    {

        [SerializeField] private Button prevScoreButton;
        [SerializeField] private Button nextScoreButton;
        [SerializeField] private Text selectedScore;

        [SerializeField] private Image[] prevButtonImages;
        [SerializeField] private Image[] nextButtonImages;
        [SerializeField] private Color interactableColor = Color.white;
        [SerializeField] private Color notInteractableColor = ColorUtils.FromHex(0xFFFFFF80);

        private double minScore;
        private double maxScore;
        private double stepScore;
        private double currentScore;

        private UnityEvent onScoreChanged = new UnityEvent();

        internal double Score 
        { 
            get { return currentScore; }
            set
            {
                if(value >= minScore && value <= maxScore)
                {
                    currentScore = value;
                    SetupView();

                    onScoreChanged.Invoke();
                }
            }
        }

        internal UnityEvent OnScoreChanged { get { return onScoreChanged; } }

        internal void Inject(double minScore, double maxScore, double stepScore)
        {
            if(minScore > maxScore)
            {
                double tempScore = minScore;
                minScore = maxScore;
                maxScore = tempScore;
            }

            this.minScore = minScore;
            this.maxScore = maxScore;
            this.stepScore = stepScore;
        }

        private void Start()
        {
            prevScoreButton.onClick.AddListener(OnPrevScoreSelected);
            nextScoreButton.onClick.AddListener(OnNextScoreSelected);
        }

        private void SetupView()
        {
            bool hideAll = minScore == maxScore;
            prevScoreButton.interactable = !hideAll && currentScore > minScore;
            nextScoreButton.interactable = !hideAll && currentScore < maxScore;
            selectedScore.text = currentScore.ToString();

            SetupButtonImages(prevButtonImages, prevScoreButton.IsInteractable());
            SetupButtonImages(nextButtonImages, nextScoreButton.IsInteractable());
        }

        private void OnPrevScoreSelected()
        {
            Score = Math.Max(Score - stepScore, minScore);
        }

        private void OnNextScoreSelected()
        {
            Score = Math.Min(Score + stepScore, maxScore);
        }

        private void SetupButtonImages(Image[] images, bool interactable)
        {
            if (images == null) return;
            var color = interactable ? interactableColor : notInteractableColor;
            for(int i = 0; i < images.Length; i++) images[i].CrossFadeColor(color, 0.2f, true, true);
        }

    }

}