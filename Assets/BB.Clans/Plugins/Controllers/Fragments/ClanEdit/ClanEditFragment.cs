using System;
using System.Collections;
using BlackBears.Clans.Controller.CountrySelection;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View;
using BlackBears.Clans.View.Common;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Views;
using UnityEngine;
using UnityEngine.UI;

using Edit = BlackBears.Clans.Model.ClanEdit;

namespace BlackBears.Clans.Controller.Fragments.ClanEdit
{

    internal class ClanEditFragment : Fragment
    {

        [Header("Blocks")]
        [SerializeField] private GameObject clanNameContainer;
        [SerializeField] private GameObject clanDescContainer;
        [SerializeField] private GameObject clanIconContainer;
        [SerializeField] private GameObject countryContainer;
        [SerializeField] private GameObject entryContainer;
        [SerializeField] private GameObject privacyContainer;
        [SerializeField] private GameObject disbandClanContainer;

        [Header("Views And Controllers")]
        [SerializeField] private Image background;
        [SerializeField] private Text title;
        [SerializeField] private Text[] submitButtonTitles;
        [SerializeField] private StateExtendedInputField nameInput;
        [SerializeField] private ErrorInputView nameError;
        [SerializeField] private InputField descInput;
        [SerializeField] private ErrorInputView descError;
        [SerializeField] private IconEditController iconEdit;
        [SerializeField] private CountrySelector countrySelector;
        [SerializeField] private Toggle clanPublicToggle;
        [SerializeField] private Toggle clanPrivateToggle;
        [SerializeField] private ClanEditSubmitControl submitControl;
        [SerializeField] private Button disbandClanButton;
        [SerializeField] private ScoreRequirementEditController scoreEdit;
        [SerializeField] private float fullSettingsOffset;
        [SerializeField] private RectTransform contentRectTransform;
        [SerializeField] private float transitionStep = 0.2f;

        [SerializeField] private SpritePackView scoreImage;
        [SerializeField] private SpritePackView[] currencyImages;

        [SerializeField] private LayoutElement scoreLayout;


        private ClanCore core;
        private NotificationsFeature notifications;
        private AlertsFeature alerts;
        private Block onClanCreateOrUpdate;
        private Block onClanDelete;

        private ClanRole role;
        private Edit oldEdit;
        private Edit edit;

        private bool canShowErrors = false;

        internal override FragmentKind Kind { get { return FragmentKind.ClanEdit; } }

        internal void Inject(ClanCore core, ClanFullSettings settings, Block onClanCreateOrUpdate,
            Block onClanDelete)
        {
            this.core = core;
            this.onClanCreateOrUpdate = onClanCreateOrUpdate;
            this.onClanDelete = onClanDelete;

            var config = settings.configuration;
            countrySelector.Inject(config.Countries);
            submitControl.Inject(core.State.Game, core.Params);
            iconEdit.Inject(config.ClanIcons);
            scoreEdit.Inject(config.MinScoreInviteRequirement, config.MaxScoreInviteRequirement, config.StepScoreInviteRequirement);

            scoreImage.SetSpritePack(settings.scoreIconPack);
#if BBGC_LANDSCAPE
            scoreLayout.minHeight = 24;
            scoreLayout.minWidth = 24;
#endif
            foreach (var currencyImage in currencyImages) currencyImage.SetSpritePack(settings.hadrCurPack);
        }

        private void Awake()
        {
            var features = BBGC.Features;
            notifications = features.GetFeature<NotificationsFeature>();
            alerts = features.GetFeature<AlertsFeature>();

            submitControl.SubmitButton.onClick.AddListener(OnSubmitClick);
            disbandClanButton.onClick.AddListener(OnDisbandClick);
            nameInput.inputType = InputField.InputType.AutoCorrect;
            descInput.inputType = InputField.InputType.AutoCorrect;
            nameInput.characterLimit = ValidateHelper.clanNameMaxLength;
            descInput.characterLimit = ValidateHelper.clanDescMaxLength;
        }

        protected override void Start()
        {
            base.Start();

            role = ClanRole.None;
            if (core.State.User != null) role = core.State.User.Role;

            Prepare(role);

            submitControl.ButtonActive = true;

            nameInput.onValueChanged.AddListener(OnNameEdit);
            nameInput.onEndEdit.AddListener(t => 
            {
                OnNameEditEnd(t);
                if (submitControl.ButtonActive)
                {
                    var pos = contentRectTransform.anchoredPosition;
                    pos.y = fullSettingsOffset;
                    LeanTween.moveAnchored(contentRectTransform, pos, transitionStep).setEaseInOutQuad();
                }
            });

            descInput.onValueChanged.AddListener(OnDescEdit);
            descInput.onEndEdit.AddListener(OnDescEditEnd);

            clanPublicToggle.onValueChanged.AddListener(OnPublicToggleChanged);
            clanPrivateToggle.onValueChanged.AddListener(OnPrivateToggleChanged);

            scoreEdit.OnScoreChanged.AddListener(OnScoreRequirementChanged);

            iconEdit.OnIconChange.AddListener(() => CheckEditValidity());
            countrySelector.OnCountryChange.AddListener(OnCountryChanged);

            if (string.IsNullOrEmpty(edit.Title)) nameInput.Select();

            CheckEditValidity();

            StartCoroutine(PostInit(role));
        }

        private IEnumerator PostInit(ClanRole role)
        {
            if (!countryContainer.activeSelf) yield break;

            yield return new WaitForEndOfFrame();
            countrySelector.Initiate(role.IsAdministrator() ? core.State.Clan.Country : core.State.ProfileCache.LocalPlayer.Country);

            canShowErrors = true;
            nameInput.UpdateVisualState();
        }

        private void Prepare(ClanRole role)
        {
            PrepareContainers(role);
            var player = core.State.ProfileCache.LocalPlayer;
            if (role.IsClanMember())
            {
                edit = new Edit(player.Id, core.State.Clan);
                oldEdit = edit.Clone();
            }
            else
            {
                edit = new Edit(player.Id);
            }

            title.text = ClansLocalization.Get(role.IsClanMember() ?
                LocalizationKeys.editClanTitle : LocalizationKeys.createClanTitle);

            var submitText = ClansLocalization.Get(role.IsClanMember() ?
                    LocalizationKeys.save : LocalizationKeys.createClanSubmit);
            for (int i = 0; i < submitButtonTitles.Length; i++)
            {
                submitButtonTitles[i].text = submitText;
            }

            submitControl.isClanCreate = !role.IsClanMember();
            nameInput.text = edit.Title;
            descInput.text = edit.Description;
            clanPublicToggle.isOn = edit.IsPublic;
            clanPrivateToggle.isOn = !edit.IsPublic;
            iconEdit.SetIcon(edit.Icon);
            if (role.IsClanMember()) scoreEdit.Score = edit.JoinRequirement;
            else scoreEdit.Score = 0;
        }

        private void PrepareContainers(ClanRole role)
        {
            clanNameContainer.SetActive(role.IsAdministrator() || !role.IsClanMember());
            clanDescContainer.SetActive(role.IsAdministrator() || !role.IsClanMember());
            clanIconContainer.SetActive(role.IsModerator() || !role.IsClanMember());
            countryContainer.SetActive(role.IsAdministrator() || !role.IsClanMember());
            entryContainer.SetActive(role.IsModerator() || !role.IsClanMember());
            privacyContainer.SetActive(role.IsAdministrator() || !role.IsClanMember());
            disbandClanContainer.SetActive(role.IsAdministrator());
        }

        private void CreateClan()
        {
            ShowLoader(string.Empty, background.color);

            LeanTween.delayedCall(gameObject, loaderDelay, ProcessCreateClan);
        }

        private void ProcessCreateClan()
        {
            if (Disposed) return;

            core.CreateClanRequest(edit, () =>
            {
                if (Disposed) return;

                onClanCreateOrUpdate.SafeInvoke();
                CloseRequest();
            }, (c, m) =>
            {
                Debug.LogFormat("Create error: {0}; {1}.", c, m);
                if (Disposed) return;

                HideLoader();

                if (c != (long)GameErrorCodes.NotEnoughMoney)
                {
                    notifications.ShowNotification(new Notification(NotificationType.Warning,
                        ClansLocalization.Get(LocalizationKeys.notificationErrorCreateClan)));
                    ShowError(CreateClan);
                }
            });
        }

        private void UpdateClan()
        {
            ShowLoader(string.Empty, background.color);

            LeanTween.delayedCall(gameObject, loaderDelay, ProcessUpdateClan);
        }

        private void ProcessUpdateClan()
        {
            if (Disposed) return;

            core.UpdateClanRequest(edit, () =>
            {
                if (Disposed) return;

                onClanCreateOrUpdate.SafeInvoke();
                CloseRequest();
            }, (c, m) =>
            {
                Debug.LogFormat("Update error: {0}; {1}.", c, m);
                if (Disposed) return;

                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorUpdateClan)));
                HideLoader();
                ShowError(UpdateClan);
            });
        }

        private void DisbandClan()
        {
            ShowLoader(string.Empty, background.color);

            LeanTween.delayedCall(gameObject, loaderDelay, ProcessDisbandClan);
        }

        private void ProcessDisbandClan()
        {
            if (Disposed) return;

            core.DeleteClan(() =>
            {
                if (Disposed) return;

                onClanDelete.SafeInvoke();
                CloseRequest();
            }, (c, m) =>
            {
                Debug.LogFormat("Disband error: {0}; {1}", c, m);
                if (Disposed) return;

                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorDisbandClan)));
                HideLoader();
                ShowError(DisbandClan);
            });
        }

        private void CheckEditValidity()
        {
            if (oldEdit != null && Edit.Equals(oldEdit, edit))
            {
                nameError.IsError = false;
                descError.IsError = false;
                submitControl.ButtonActive = false;
                return;
            }

            string titleError = ValidateHelper.GetClanNameValidationError(edit.Title);

            if (!canShowErrors || string.IsNullOrEmpty(titleError))
            {
                submitControl.ButtonActive = true;
                nameError.IsError = false;
            }
            else
            {
                submitControl.ButtonActive = false;
                nameError.ErrorMessage = ClansLocalization.Get(titleError);
                nameError.IsError = true;
            }

            descError.IsError = false;
        }

        private void OnSubmitClick()
        {
            if (role.IsClanMember()) UpdateClan();
            else CreateClan();
        }

        private void OnDisbandClick()
        {
            if (role.IsAdministrator())
            {
                alerts.AddAlert(AlertsBuilder.CreateDestroyClanAlert(DisbandClan, null));
            }
        }

        private void OnNameEdit(string text)
        {
            CheckLengthAlert(text.Length, ValidateHelper.clanNameMaxLength, nameError);
        }

        private void OnNameEditEnd(string text)
        {
            edit.Title = text;
            CheckEditValidity();
        }

        private void OnDescEdit(string text)
        {
            CheckLengthAlert(text.Length, ValidateHelper.clanDescMaxLength, descError);
        }

        private void OnDescEditEnd(string text)
        {
            edit.Description = text;
            CheckEditValidity();
        }

        private void OnCountryChanged(string country)
        {
            edit.Country = country;
            CheckEditValidity();
        }

        private void OnScoreRequirementChanged()
        {
            edit.JoinRequirement = scoreEdit.Score;
            CheckEditValidity();
        }

        private void OnPublicToggleChanged(bool isOn) { UpdatePrivacy(true, isOn); }
        private void OnPrivateToggleChanged(bool isOn) { UpdatePrivacy(false, isOn); }

        private void UpdatePrivacy(bool isPublic, bool isOn)
        {
            if (!isOn) return;
            edit.IsPublic = isPublic;
            CheckEditValidity();
        }

        private void CheckLengthAlert(int currentLength, int maxLength, ErrorInputView errorInput)
        {
            int alertLength = maxLength - 10;
            alertLength = Mathf.Max(0, alertLength);

            if (currentLength < alertLength)
            {
                errorInput.IsError = false;
            }
            else
            {
                errorInput.ErrorMessage = string.Format("{0}/{1}", currentLength, maxLength);
                errorInput.IsError = true;
            }
        }

    }

}