﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.Clans.Model;
using BlackBears.Clans.View.Loader;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.BaseTop
{

    public abstract class BaseTopFragment : Fragment
    {

        [SerializeField] private TopSwitchController topSwitchController;
        
        [UnityEngine.Serialization.FormerlySerializedAs("loader")]
        [SerializeField] private CircleLoader circleLoader;

        internal sealed override FragmentKind Kind => FragmentKind.InGameFragment;
        public bool IsCircleLoaderWork => circleLoader.IsWork;

        internal TopSwitchController TopSwitchController { get { return topSwitchController; } }

        protected override void Start()
        {
            base.Start();
            TopSwitchController.OnTopSwitched.AddListener(index => SwitchTop(index, false));
            SwitchTop(0, true);
            circleLoader.OnShow.AddListener(OnCircleLoaderShowed);
        }

        protected void ShowCircleLoader() => circleLoader.Show();
        protected void HideCircleLoader() => circleLoader.Hide();
        protected virtual void OnCircleLoaderShowed() {}
        
        protected abstract void SwitchTop(int index, bool force);
    }

}
