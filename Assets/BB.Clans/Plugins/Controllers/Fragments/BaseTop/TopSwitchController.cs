using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.BaseTop
{

    internal class TopSwitchController : MonoBehaviour
    {

        [SerializeField] private Toggle[] toggleList;

        private int index = 0;
        private IntUnityEvent onTopSwitched = new IntUnityEvent();

        internal int Index { get { return index; } }
        internal IntUnityEvent OnTopSwitched => onTopSwitched;

        private void Start()
        {
            if(toggleList == null || toggleList.Length == 0) return;

            for(int i = 0; i < toggleList.Length; i++)
            {
                int index = i;
                toggleList[i].onValueChanged.AddListener(isOn => OnTopChanged(index, isOn));
            }
        }

        private void OnTopChanged(int index, bool isOn)
        {
            if (!isOn) return;
            this.index = index;
            onTopSwitched.Invoke(this.index);
        }

    }

}