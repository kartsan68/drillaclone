using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Profile
{

    internal class ClanInfo : MonoBehaviour
    {

        [SerializeField] private GameObject notInClanContainer;
        [SerializeField] private GameObject inClanContainer;

        [SerializeField] private Text clanNameText;
        [SerializeField] private Text placeText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text userAmountText;
        [SerializeField] private PredefinedIconView iconView;
        [SerializeField] private GameObject privacyMarker;

        private PredefinedIconsContainer clanIcons;
        private Clan clan;
        
        internal Clan Clan
        {
            get { return clan; }
            set
            {
                clan = value;
                UpdateView();
            }
        }

        internal void Inject(PredefinedIconsContainer clanIcons)
        {
            this.clanIcons = clanIcons;
        }

        private void UpdateView()
        {
            notInClanContainer.SetActive(clan == null);
            inClanContainer.SetActive(clan != null);
            iconView.SetIcon(PredefinedIcon.defaultIcon, clanIcons);            

            if (clan == null) return;
            clanNameText.text = clan.Title;
			placeText.text = ValidateHelper.PlaceToString(clan.WorldPlace);
            scoreText.text = clan.Score.ToString();
			userAmountText.text = string.Format("{0}/{1}", clan.Amount, clan.Capacity);
            iconView.SetIcon(clan.Icon, clanIcons);
            privacyMarker.SetActive(!clan.IsPublic);
        }

    }

}