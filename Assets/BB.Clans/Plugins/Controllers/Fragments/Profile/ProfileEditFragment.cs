﻿using System;
using System.Collections;
using BlackBears.Clans.Controller.CountrySelection;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Common;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Views;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Profile
{

    internal class ProfileEditFragment : Fragment
    {

        [SerializeField] private Text title;
        [SerializeField] private Image background;
        [SerializeField] private InputField nameInput;
        [SerializeField] private ErrorInputView nameError;
        [SerializeField] private IconEditController editController;
        [SerializeField] private ProfileCompleteEditSubmitControl submitControl;
        [SerializeField] private CountrySelector countrySelector;

        [SerializeField] private SpritePackView[] currencyImages;

        private ClanCore core;

        internal override FragmentKind Kind { get { return FragmentKind.PlayerEdit; } }

        internal void Inject(ClanCore core, SpritePack hardCurrencyIcon,
            PredefinedIconsContainer playerIcons, CountriesContainer countries)
        {
            this.core = core;
            editController.Inject(playerIcons);
            submitControl.Inject(core.State.Game, core.Params);
            countrySelector.Inject(countries);

            foreach (var currencyImage in currencyImages) currencyImage.SetSpritePack(hardCurrencyIcon);
        }

        protected override void Start()
        {
            base.Start();
            
            submitControl.ButtonActive = false;
            submitControl.SubmitButton.onClick.AddListener(OnUpdateRequest);

            var player = core.State.ProfileCache.LocalPlayer;

            nameInput.inputType = InputField.InputType.AutoCorrect;
            nameInput.onValueChanged.AddListener(OnNameEdit);
            nameInput.onEndEdit.AddListener(_ => CheckChanges());
            nameInput.characterLimit = ValidateHelper.nicknameMaxLength;
            nameInput.text = player.Nickname;
            editController.SetIcon(player.Icon.Clone());
            editController.OnIconChange.AddListener(CheckChanges);
            countrySelector.OnCountryChange.AddListener(_ => CheckChanges());

            StartCoroutine(PostInit(player));
        }

        private IEnumerator PostInit(LocalPlayerProfile player)
        {
            yield return new WaitForEndOfFrame();
            countrySelector.Initiate(player.Country);
        }

        private void CheckChanges()
        {
            if (CheckForNicknameError())
            {
                submitControl.ButtonActive = false;
                return;
            }

            var player = core.State.ProfileCache.LocalPlayer;
            bool hasChanges;
            hasChanges = !string.Equals(player.Nickname, nameInput.text);
            hasChanges = hasChanges || !string.Equals(player.Country, countrySelector.Selected);
            hasChanges = hasChanges || !Icon.Equals(editController.Icon, player.Icon);

            if (hasChanges) submitControl.ButtonActive = true;
            else submitControl.ButtonActive = false;
        }

        private bool CheckForNicknameError()
        {
            string error = ValidateHelper.GetPlayerNameValidationError(nameInput.text);

            if (!string.IsNullOrEmpty(error))
            {
                nameError.ErrorMessage = ClansLocalization.Get(error);
                nameError.IsError = true;
                return true;
            }
            else
            {
                nameError.IsError = false;
                return false;
            }
        }

        private void OnNameEdit(string text)
        {
            int alertLength = ValidateHelper.nicknameMaxLength - ValidateHelper.nicknameMaxLengthAlert;
            alertLength = Mathf.Max(0, alertLength);

            if (text.Length < alertLength)
            {
                nameError.IsError = false;
            }
            else
            {
                nameError.ErrorMessage = string.Format("{0}/{1}", text.Length, ValidateHelper.nicknameMaxLength);
                nameError.IsError = true;
            }
        }

        private void OnUpdateRequest()
        {
            ShowLoader(string.Empty, background.color);

            LeanTween.delayedCall(gameObject, loaderDelay, ProcessUpdatePlayer);
        }

        private void ProcessUpdatePlayer()
        {
            if (Disposed) return;


            core.UpdatePlayerRequest(nameInput.text, editController.Icon, countrySelector.Selected, 
                CloseRequest, OnUpdateFailed);
        }

        private void OnUpdateFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnUpdateFailed: {0}; {1}", errorCode, errorMessage);
            if (Disposed) return;

            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;
                
                HideLoader();

                if (errorCode != (long)GameErrorCodes.NotEnoughMoney) ShowError(OnUpdateRequest);
            });
        }

    }

}