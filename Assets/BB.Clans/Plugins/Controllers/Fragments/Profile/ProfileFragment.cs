﻿using System;
using BlackBears.Clans.Model;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;
using BlackBears.Chronometer;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Icons;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.Controller.Common;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Views;
using BlackBears.Clans.Model.Helpers;
using System.Collections.Generic;

namespace BlackBears.Clans.Controller.Fragments.Profile
{

    internal class ProfileFragment : Fragment
    {

        [SerializeField] private SpritePackView[] scoreImages;

        [SerializeField] private Image background;
        [SerializeField] private Text nicknameText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text weeklyScoreText;
        [SerializeField] private Text countryText;
        [SerializeField] private Text inGameText;
        [SerializeField] private Text onlineText;
        [SerializeField] private Image countryImage;
        [SerializeField] private PredefinedIconView iconView;

        [SerializeField] private GameObject countryContainer;
        [SerializeField] private GameObject onlineContainer;

        [SerializeField] private ClanInfo clanInfo;
        [SerializeField] private Button clanButton;
        [SerializeField] private SubmitControl submitControl;

        [SerializeField] private Toggle pushToggle;
        [SerializeField] private GameObject pushRoot;
        [SerializeField] private List<GameObject> clansObjects;


        private ClanCore core;
        private NotificationsFeature notifications;
        private PredefinedIconsContainer profileIcons;
        private CountriesContainer countries;
        private string playerId;
        private ClanUser user;
        private Clan clan;
        private IPlayerProfile profile;
        private bool disposed;

        internal override FragmentKind Kind { get { return FragmentKind.Player; } }

        internal void Inject(ClanCore core, ClanFullSettings settings,
            NotificationsFeature notifications, string playerId, ClanUser user, Clan clan)
        {
            this.core = core;
            this.notifications = notifications;
            this.playerId = playerId;
            this.clan = clan;
            if (user != null) this.user = user;
            else if (clan != null) this.user = clan.GetUser(playerId);

            var config = settings.configuration;
            profileIcons = config.ProfileIcons;
            countries = config.Countries;
            clanInfo.Inject(config.ClanIcons);

            foreach (var scoreImage in scoreImages)
                scoreImage.SetSpritePack(settings.scoreIconPack);
        }

        protected override void Start()
        {
            base.Start();

            clanButton.onClick.AddListener(OnOpenClanRequest);
            submitControl.SubmitButton.onClick.AddListener(OnEditPlayerRequest);
            OnFocus();
#if BBGC_LANDSCAPE
            pushToggle.onValueChanged.AddListener(OnPushSwitched);
#endif
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
            disposed = true;
        }

        internal override void OnFocus()
        {
            base.OnFocus();
            LoadPlayer();
        }

        private void LoadPlayer()
        {
            ShowLoader(string.Empty, background.color);
            core.State.ProfileCache.GetPlayerAsync(playerId, OnPlayerGetted, OnPlayerGetFailed);
        }

        private void UpdateProfile()
        {
            nicknameText.text = string.IsNullOrEmpty(profile.Nickname) ?
                ValidateHelper.PlayerIdToTempNick(profile.Id) : profile.Nickname;
            double score = 0.0;
            if (user != null) score = user.Score;
            else score = profile.Score;

            scoreText.text = score.ToString("0.");

            var actualTime = TimeModule.ActualTime;

            if (user != null)
            {
                var onlineTs = System.TimeSpan.FromSeconds(TimeModule.ActualTime - user.OnlineDate);
                var onlineTm = DateUtils.ToTimedMessage(onlineTs);
                weeklyScoreText.gameObject.SetActive(profile.WeeklyScore != 0 && onlineTs.Days < 7);
                weeklyScoreText.text = profile.WeeklyScore.ToString("+#;-#;0");

                onlineContainer.SetActive(true);
                if (onlineTm.key == TimedMessage.justNowKey)
                {
                    onlineText.text = ClansLocalization.Get(LocalizationKeys.online);
                }
                else
                {
                    var offline = ClansLocalization.Get(LocalizationKeys.offline);
                    var message = ClansLocalization.Get(onlineTm.key, onlineTm.valueCount);
                    onlineText.text = string.Format("{0} {1}", offline, message);
                }
            }
            else
            {
                weeklyScoreText.gameObject.SetActive(false);
                onlineContainer.SetActive(false);
            }

            if (string.IsNullOrEmpty(profile.Country))
            {
                countryContainer.SetActive(false);
            }
            else
            {
                countryContainer.SetActive(true);
                countryText.text = ClansLocalization.Get(profile.Country);
                countryImage.sprite = countries[profile.Country];
            }

            var inGameMessageTs = System.TimeSpan.FromSeconds(actualTime - profile.RegisterDate);
            // Если чувак отыграл меньше минуты, ставим span в 1 минуту чтобы вывелось сообщение про 1 минуту
            if (inGameMessageTs.TotalMinutes < 1) inGameMessageTs = TimeSpan.FromMinutes(1.01);
            var inGameMessage = DateUtils.ToTimedMessage(inGameMessageTs);
            inGameText.text = ClansLocalization.Get(inGameMessage.key, inGameMessage.valueCount);

            iconView.SetIcon(profile.Icon, profileIcons);

            clanInfo.Clan = clan;
            submitControl.Hide(true);
            if (profile.IsLocal && core.State.Registered) submitControl.Show();

            bool isUserClan = core.State.IsUserInClan && core.State.IsUserClan(clan);
            bool isUserClanMember = isUserClan && core.State.User.Role.IsClanMember();

#if BBGC_LANDSCAPE

            foreach (var clanObject in clansObjects)
            {
                clanObject.SetActive(clan != null);
            }
            //TODO: Включить обратно когда пуши серверные будут
            // pushNotificationContainer.SetActive(isUserClanMember);
            pushRoot.SetActive(false);
            pushToggle.interactable = false;
            pushToggle.isOn = !(isUserClanMember && core.State.User.PushesEnabled);
            pushToggle.interactable = true;
#endif
        }

        private void OnPlayerGetted(IPlayerProfile profile)
        {
            if (Disposed) return;

            this.profile = profile;
            UpdateProfile();
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
        }

        private void OnPlayerGetFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnPlayerGetFailed: {0}; {1}", errorCode, errorMessage);
            if (Disposed) return;

            notifications.ShowNotification(new Notification(NotificationType.Warning,
                ClansLocalization.Get(LocalizationKeys.notificationErrorNetworkConnection)));
            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;

                HideLoader();
                ShowError(LoadPlayer);
            });
        }

        private void OnOpenClanRequest()
        {
            if (clan == null) return;
            FragmentsController.ShowClanInfo(clan);
        }

        private void OnEditPlayerRequest()
        {
            FragmentsController.ShowPlayerEdit();
        }
#if BBGC_LANDSCAPE
        private void OnPushSwitched(bool isOn)
        {
            if (!pushToggle.interactable) return;

            Debug.LogFormat("TOGGLE: {0}", !isOn);
            pushToggle.interactable = false;
            core.SwitchPushes(!isOn, OnPushSwitchSuccess, OnPushSwitchFailed);
        }

        private void OnPushSwitchSuccess()
        {
            LeanTween.delayedCall(gameObject, 1f, () =>
            {
                if (disposed) return;
                pushToggle.interactable = true;
            });
        }

        private void OnPushSwitchFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnPushSwitchFailed: {0}; {1}.", errorCode, errorMessage);
            LeanTween.delayedCall(gameObject, 1f, () =>
            {
                if (disposed) return;
                pushToggle.isOn = !pushToggle.isOn;
                pushToggle.interactable = true;
            });
        }
#endif

    }

}