using System;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.BackPress;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments
{

    [RequireComponent(typeof(RectTransform))]
    public abstract class Fragment : MonoBehaviour, IBackPressListener
    {

        protected const float loaderDelay = 0.25f;
        protected const float minScroll = 200f;
        private const float closeDragCoeff = 0.5f;

        [SerializeField] private RectTransform container;
        [SerializeField] private ScrollRect scroll;

        private RectTransform rectTransform;
        private ScrollButtonControl scrollControl;
        private bool canScrollToTop;

        private FragmentLoaderController loader;
        private FragmentErrorController error;

        internal RectTransform RectTransform
        {
            get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); }
        }

        internal ScrollButtonControl ScrollControl
        {
            private get { return scrollControl; }
            set
            {
                scrollControl = value;
                if (scrollControl != null) scrollControl.SetClickCallback(ScrollToTopRequest);
                CheckScroll();
            }
        }

        internal bool Disposed { get; private set; }

        internal FragmentLoaderController LoaderPrefab { private get; set; }
        internal FragmentErrorController ErrorPrefab { private get; set; }
        internal FragmentsController FragmentsController { get; set; }

        protected ScrollRect Scroll { get { return scroll; } }

        private bool CanScrollToTop
        {
            get { return canScrollToTop; }
            set
            {
                if (canScrollToTop == value) return;
                canScrollToTop = value;
                CheckScroll();
            }
        }

        public virtual int FragmentId => (int)Kind;
        internal abstract FragmentKind Kind { get; }

        protected virtual void Start()
        {
            BBGC.Features.GetFeature<BackPressFeature>().AddListener(this);
            if (scroll != null) scroll.onValueChanged.AddListener(OnScroll);
        }

        protected virtual void OnDestroy()
        {
            BBGC.Features.GetFeature<BackPressFeature>().RemoveListener(this);
            LeanTween.cancel(container);
            LeanTween.cancel(gameObject);
            if (scroll != null) LeanTween.cancel(scroll.gameObject);
            ResourceUnloadManager.RequestUnload();
            Disposed = true;
        }

        internal void OnDrag(float delta)
        {
            var pos = container.anchoredPosition;
            pos.x = Mathf.Clamp(delta, 0, container.rect.width);
            container.anchoredPosition = pos;
        }

        internal void OnEndDrag(float delta)
        {
            OnDrag(delta);
            if (delta > container.rect.width * closeDragCoeff) CloseRequest();
            else Show(false);
        }

        internal void Show(bool moveToStartPosition = true)
        {
            if (moveToStartPosition)
            {
                var pos = container.anchoredPosition;
                pos.x = container.rect.width;
                container.anchoredPosition = pos;
            }

            LeanTween.moveAnchored(container, Vector2.zero, 0.2f).setEaseOutQuad();
        }

        internal void Hide(Block onHide)
        {
            var pos = container.anchoredPosition;
            pos.x = container.rect.width;

            LeanTween.moveAnchored(container, pos, 0.2f)
                .setEaseInQuad()
                .setOnComplete(() => onHide.SafeInvoke());
        }

        internal virtual void OnFocus() { }

        internal virtual void LostFocus() { }

        internal virtual bool BackRequest() { return false; }

        bool IBackPressListener.OnBackPress()
        {
            return ProcessBackPress();
        }

        protected virtual bool ProcessBackPress()
        {
            CloseRequest();
            return true;
        }

        protected void ShowLoader(string title, Color backgroundColor)
        {
            if (!Disposed && CheckLoaderController()) loader.ShowRequest(this, title, backgroundColor, loader.TextColor);
        }

        protected void ShowLoader(string title, Color backgroundColor, Color textColor)
        {
            if (!Disposed && CheckLoaderController()) loader.ShowRequest(this, title, backgroundColor, textColor);
        }

        protected void HideLoader()
        {
            if (!Disposed && CheckLoaderController()) loader.HideRequest(this);
        }

        protected void ShowError(Block repeatBlock)
        {
            if (!Disposed && CheckErrorController()) error.Show(repeatBlock);
        }

        protected void ShowError(Block repeatBlock, string title, string message)
        {
            if (!Disposed && CheckErrorController()) error.Show(repeatBlock, title, message);
        }
    
        protected virtual void OnScrollToTopFinished()
        {
            scroll.enabled = true;
        }

        protected void CloseRequest()
        {
            if (!Disposed && FragmentsController != null) FragmentsController.Close(this);
        }

        private void OnScroll(Vector2 _) { CanScrollToTop = scroll.content.anchoredPosition.y > minScroll; }

        private void ScrollToTopRequest()
        {
            if (scroll == null || !scroll.enabled) return;
            CanScrollToTop = false;

            scroll.enabled = false;
            LeanTween.moveScrollNormalized(scroll, Vector2.up, 0.2f).setOnComplete(OnScrollToTopFinished);
        }

        private bool CheckLoaderController()
        {
            if (loader != null) return true;
            if (LoaderPrefab == null) return false;
            loader = Instantiate(LoaderPrefab);
            loader.transform.SetParent(container, false);
            return true;
        }

        private bool CheckErrorController()
        {
            if (error != null) return true;
            if (ErrorPrefab == null) return false;
            error = Instantiate(ErrorPrefab);
            error.transform.SetParent(container, false);
            return true;
        }

        private void CheckScroll()
        {
            if (scrollControl == null) return;
            if (CanScrollToTop) scrollControl.Show();
            else scrollControl.Hide();
        }

    }

}