using System;
using System.Collections.Generic;
using BlackBears.Clans.Controller.ContextMenu;
using BlackBears.Clans.Model;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Features.Referals;
using UnityEngine;
using UnityEngine.Events;

using EventType = BlackBears.Clans.Events.EventType;

namespace BlackBears.Clans.Controller.Fragments
{

    [RequireComponent(typeof(RectTransform))]
    public class FragmentsController : MonoBehaviour
    {

        public const string coreDataKey = "core";
        public const string settingsDataKey = "settings";

        [SerializeField] private FragmentControls controls;
        [SerializeField] private ScrollButtonControl scrollControl;
        [SerializeField] private FragmentLoaderController loaderPrefab;
        [SerializeField] private FragmentErrorController errorPrefab;

        private ClanCore core;
        private ClanFullSettings settings;
        private ContextController contextController;
        private RectTransform rectTransform;
        private LinkedList<Fragment> fragments = new LinkedList<Fragment>();
        private IntUnityEvent onFragmentsCountChanged = new IntUnityEvent();
        private UnityEvent onClosed = new UnityEvent();

        internal bool Showed { get; private set; }

        internal int FragmentsCount { get { return fragments.Count; } }
        internal IntUnityEvent OnFragmentsCountChanged { get { return onFragmentsCountChanged; } }
        internal UnityEvent OnClosed { get { return onClosed; } }

        internal Fragment CurrentFragment { get { return fragments.Count != 0 ? fragments.First.Value : null; } }

        private ClansConfiguration Config { get { return settings.configuration; } }

        private RectTransform RectTransform
        {
            get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); }
        }

        internal void Inject(ClanCore core, ClanFullSettings settings, ContextController contextController)
        {
            this.core = core;
            this.settings = settings;
            this.contextController = contextController;
        }

        private void Awake()
        {
            controls.Inject(this);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void ShowClanEdit(Block onCreateOrUpdate = null, Block onClanDelete = null)
        {
            var fragment = Instantiate(Config.Fragments.ClanEdit);
            fragment.Inject(core, settings, onCreateOrUpdate, onClanDelete);
            AddFragment(fragment);
        }

        internal void ShowClanInfo(Clan clan)
        {
            var fragment = Instantiate(Config.Fragments.ClanInfo);

            fragment.Inject(core, settings, BBGC.Features.GetFeature<NotificationsFeature>(),
                BBGC.Features.GetFeature<AlertsFeature>(), contextController, this, clan,
                Config.ProfileIcons, settings.scoreIconPack);
            AddFragment(fragment);
        }

        internal void ShowClanTop()
        {
            var fragment = Instantiate(Config.Fragments.ClanTop);
            fragment.Inject(core, settings, this);
            AddFragment(fragment);
        }

        internal void ShowClanJoinRequests()
        {
            var fragment = Instantiate(Config.Fragments.ClanJoinRequests);
            fragment.Inject(core, Config.ProfileIcons, settings.scoreIconPack,
                BBGC.Features.GetFeature<NotificationsFeature>());

            AddFragment(fragment);
        }

        internal void ShowPlayerInfo(string playerId, ClanUser user, Clan clan)
        {
            var fragment = Instantiate(Config.Fragments.Profile);
            fragment.Inject(core, settings, BBGC.Features.GetFeature<NotificationsFeature>(),
                playerId, user, clan);
            AddFragment(fragment);
        }

        internal void ShowPlayerEdit()
        {
            var fragment = Instantiate(Config.Fragments.ProfileEdit);
            fragment.Inject(core, settings.hadrCurPack, Config.ProfileIcons, Config.Countries);
            AddFragment(fragment);
        }

        internal void ShowInvites()
        {
            var fragment = Instantiate(Config.Fragments.Invites);
            var features = BBGC.Features;
            fragment.Inject(core, features.GetFeature<ReferalsFeature>(), Config.ProfileIcons,
                settings.hadrCurPack, features.GetFeature<NotificationsFeature>());
            AddFragment(fragment);
        }

        internal void ShowResourceRequests()
        {
            var fragment = Instantiate(Config.Fragments.ResourceRequest);
            fragment.Inject(core.State.Warehouse, core.State.Game);
            AddFragment(fragment);
        }

        internal void ShowResourceTake()
        {
            var fragment = Instantiate(Config.Fragments.ResourceTake);
            fragment.Inject(core.State.Warehouse, core.State.ProfileCache,
                settings.configuration.ProfileIcons, core.State.Game,
                BBGC.Features.GetFeature<ChronometerFeature>());
            AddFragment(fragment);
        }

        internal void ShowFragment(FragmentKind kind)
        {
            switch (kind)
            {
                case FragmentKind.Invites: ShowInvites(); break;
                case FragmentKind.PlayerEdit: ShowPlayerEdit(); break;
            }
        }

        internal void ShowSupport()
        {
            var fragment = Instantiate(Config.Fragments.Support);
            AddFragment(fragment);
        }

        internal void ShowBattleEvent()
        {
            if (!core.State.EventsCore.IsEventSupported(EventType.Battle)) return;
            var fragmentPrefab = Config.ExternalFragments.BattleEvent;
            if (fragmentPrefab == null) return;
            
            var fragment = Instantiate(fragmentPrefab);
            fragment.Inject(core, settings);
            AddFragment(fragment);
        }

        internal void ShowCustomFragment(string fragmentKey, Dictionary<string, object> data)
        {
            var fragmentPrefab = Config.ExternalFragments.GetFragmentWithKey(fragmentKey);
            if (fragmentPrefab == null) return;

            var fragment = Instantiate(fragmentPrefab);
            var diFragment = fragment as IDataInjectingFragment;
            if (diFragment != null)
            {
                data[coreDataKey] = core;
                data[settingsDataKey] = settings;
                diFragment?.Inject(data);
            }
            AddFragment(fragment);
        }

        internal void BackRequest()
        {
            if (fragments.Count == 0) return;
            if (!fragments.First.Value.BackRequest()) CloseCurrent();
        }

        internal void Close(Fragment fragment)
        {
            if (fragment == null) return;
            if (fragments.Count == 0) return;
            if (fragments.First.Value == fragment)
            {
                CloseCurrent();
                return;
            }
            if (!fragments.Remove(fragment)) return;

            DestroyFragment(fragment);
            onFragmentsCountChanged.Invoke(fragments.Count);
            CheckForHide();
        }

        internal void CloseCurrent()
        {
            if (fragments.Count == 1)
            {
                CloseAll();
                return;
            }

            var fragment = fragments.First.Value;
            fragments.RemoveFirst();
            fragments.First.Value.OnFocus();
            DestroyFragment(fragment);
            onFragmentsCountChanged.Invoke(fragments.Count);
            CheckForHide();
        }

        internal void CloseAll()
        {
            foreach (var fragment in fragments) DestroyFragment(fragment);
            fragments.Clear();
            onFragmentsCountChanged.Invoke(fragments.Count);
            Hide();
        }

        private void Show()
        {
            if (Showed) return;
            Showed = true;
            gameObject.SetActive(true);

            controls.Show();
        }

        private void CheckForHide()
        {
            if (Showed && fragments.Count == 0) Hide();
        }

        private void Hide()
        {
            if (!Showed) return;
            Showed = false;
            controls.Hide();
            LeanTween.delayedCall(gameObject, 0.21f, () =>
            {
                gameObject.SetActive(false);
                onClosed.Invoke();
            });
        }

        private void AddFragment(Fragment fragment)
        {
            Show();
            DestroyFragmentsWithFragmentId(fragment.FragmentId);

            if (fragments.Count > 0)
            {
                var oldFragment = fragments.First.Value;
                oldFragment.ScrollControl = null;
                oldFragment.LostFocus();
            }

            fragments.AddFirst(fragment);
            var scale = fragment.RectTransform.localScale;
            var sizeDelta = fragment.RectTransform.sizeDelta;
            fragment.RectTransform.SetParent(RectTransform);
            fragment.RectTransform.SetAsLastSibling();
            fragment.RectTransform.localScale = scale;
            fragment.RectTransform.sizeDelta = sizeDelta;
            fragment.RectTransform.anchoredPosition = Vector2.zero;
            fragment.FragmentsController = this;
            fragment.ScrollControl = scrollControl;
            fragment.LoaderPrefab = loaderPrefab;
            fragment.ErrorPrefab = errorPrefab;
            fragment.Show();

            onFragmentsCountChanged.Invoke(fragments.Count);
        }

        private void DestroyFragmentsWithFragmentId(int fragmentId)
        {
            LinkedListNode<Fragment> node;
            LinkedListNode<Fragment> nextNode = fragments.Last;
            Fragment fragment;

            while (nextNode != null)
            {
                node = nextNode;
                nextNode = node.Previous;
                fragment = node.Value;
                if (fragment.FragmentId != fragmentId) continue;

                fragments.Remove(node);
                DestroyFragment(fragment);
            }
        }

        private void DestroyFragment(Fragment fragment)
        {
            if (fragment == null) return;
            fragment.ScrollControl = null;
            if (fragment.gameObject.activeSelf) fragment.Hide(() => Destroy(fragment.gameObject));
            else Destroy(fragment.gameObject);
        }

    }

}