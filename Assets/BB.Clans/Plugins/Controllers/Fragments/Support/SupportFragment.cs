﻿using BlackBears.Clans.Model.Helpers;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.Support
{

    internal class SupportFragment : Fragment
    {

        [SerializeField] private Text osVersionLabel;
        [SerializeField] private Text cloudNameLabel;

        [SerializeField] private Text playerIdText;
        [SerializeField] private Text gameVersionText;
        [SerializeField] private Text osVersionText;
        [SerializeField] private Text deviceModelText;

        [SerializeField] private Text vkGroupLinkText;
        [SerializeField] private Text mailLinkText;

        [SerializeField] private Button supportButton;
        [SerializeField] private Button vkButton;
        [SerializeField] private Button mailButton;

        private CoreDefaultsFeature coreDefaults;

        internal override FragmentKind Kind { get { return FragmentKind.Support; } }

        protected override void Start()
        {
            base.Start();
            coreDefaults = BBGC.Features.CoreDefaults();
            
            osVersionLabel.text = ClansLocalization.Get(LocalizationKeys.osVersion);
            cloudNameLabel.text = ClansLocalization.Get(LocalizationKeys.cloudName);

            playerIdText.text = ValidateHelper.PlayerIdToTempNick(coreDefaults.PlayerId);
            gameVersionText.text = Application.version;
            osVersionText.text = NativeUtils.GetOSVersion();
            deviceModelText.text = SystemInfo.deviceModel;

            vkGroupLinkText.text = coreDefaults.VkSupportLink.Replace("https://", "");
            mailLinkText.text = coreDefaults.SupportMail;

            supportButton.onClick.AddListener(SendMail);
            vkButton.onClick.AddListener(OpenVK);
            mailButton.onClick.AddListener(SendMail);
        }

        private void SendMail()
        {
            var message = EmailUtils.GenerateSupportEmailMessage(playerIdText.text);
            NativeUtils.SendEmail(coreDefaults.SupportMail, coreDefaults.SupportMailSubject, message);
        }

        private void OpenVK()
        {
            Application.OpenURL(coreDefaults.VkSupportLink);
        }

    }

}