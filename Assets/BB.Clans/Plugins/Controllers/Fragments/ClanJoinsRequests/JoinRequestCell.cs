using System;
using BlackBears.Chronometer;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Icons;
using BlackBears.Clans.View.Loader;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Views;
using BlackBears.Utils;
using BlackBears.Utils.Components;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanJoinRequests
{

    [RequireComponent(typeof(RectTransform))]
    internal class JoinRequestCell : MonoBehaviour
    {

        [SerializeField] private Text nameText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text weeklyScoreText;
        [SerializeField] private Text onlineDateText;
        [SerializeField] private PredefinedIconView playerIcon;
        [SerializeField] private SpritePackView scoreImage;

        [SerializeField] private Button profileButton;
        [SerializeField] private Button acceptButton;
        [SerializeField] private Button declineButton;
        [SerializeField] private ButtonLoaderAnimator acceptLoader;
        [SerializeField] private ButtonLoaderAnimator declineLoader;
        [SerializeField] private GameObject acceptedIcon;
        [SerializeField] private GameObject declinedIcon;

        [SerializeField] private CanvasGroup buttonsGroup;
        [SerializeField] private CanvasGroup completeGroup;

        private FragmentsController fragmentsController;
        private ClanCore core;
        private NotificationsFeature notifications;
        private PredefinedIconsContainer userIcons;
        private ClanUser user;
        private IPlayerProfile profile;

        private bool disposed;

        private RectTransform _rectTransform;

        internal RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }

        internal void Inject(FragmentsController fragmentsController, 
            ClanCore core, NotificationsFeature notifications, ClanUser user, 
            IPlayerProfile profile, PredefinedIconsContainer userIcons, SpritePack scorePack)
        {
            this.fragmentsController = fragmentsController;
            this.core = core;
            this.notifications = notifications;
            this.userIcons = userIcons;
            this.user = user;
            this.profile = profile;

            scoreImage.SetSpritePack(scorePack);
        }

        private void Start()
        {
            if (profile == null || user == null)
            {
                gameObject.SetActive(false);
                return;
            }

            profileButton.onClick.AddListener(OnProfileClick);
            acceptButton.onClick.AddListener(OnAcceptClick);
            declineButton.onClick.AddListener(OnDeclineClick);

            nameText.text = string.IsNullOrEmpty(profile.Nickname) ? ValidateHelper.PlayerIdToTempNick(profile.Id) : profile.Nickname;

            scoreText.text = user.Score.ToString();
            var onlineTs = System.TimeSpan.FromSeconds(TimeModule.ActualTime - user.OnlineDate);
            var onlineTm = DateUtils.ToTimedMessage(onlineTs);

            weeklyScoreText.gameObject.SetActive(profile.WeeklyScore != 0 && onlineTs.Days < 7);
            weeklyScoreText.text = profile.WeeklyScore.ToString("+#;-#;0");
            onlineDateText.text = string.Format("{0} {1}", ClansLocalization.Get(LocalizationKeys.online),
                ClansLocalization.Get(onlineTm.key, onlineTm.valueCount));

            playerIcon.SetIcon(profile.Icon, userIcons);

            buttonsGroup.blocksRaycasts = true;
            buttonsGroup.alpha = 1f;
            completeGroup.blocksRaycasts = false;
            completeGroup.alpha = 0f;
        }

        private void OnDestroy()
        {
            disposed = true;

            LeanTween.cancel(gameObject);
            LeanTween.cancel(buttonsGroup.gameObject);
            LeanTween.cancel(completeGroup.gameObject);
        }

        private void OnProfileClick()
        {
            fragmentsController.ShowPlayerInfo(user.Id, user, null);
        }

        private void OnAcceptClick()
        {
            buttonsGroup.blocksRaycasts = false;
            acceptLoader.StartLoadingAnimation();
            declineButton.interactable = false;

            core.PromotePlayer(user.Id, OnAcceptSuccess, OnAcceptPlayerFailed);
        }

        private void OnDeclineClick()
        {
            buttonsGroup.blocksRaycasts = false;
            declineLoader.StartLoadingAnimation();
            acceptButton.interactable = false;

            core.KickFromClan(user.Id, OnDeclineSuccess, OnDeclinePlayerFailed);
        }

        private void OnAcceptSuccess()
        {
            LeanTween.delayedCall(gameObject, 1f, () => OnRequestCompleted(true));
        }

        private void OnDeclineSuccess()
        {
            LeanTween.delayedCall(gameObject, 1f, () => OnRequestCompleted(false));
        }

        private void OnRequestCompleted(bool accepted)
        {
            if (disposed) return;
            
            acceptedIcon.SetActive(accepted);
            declinedIcon.SetActive(!accepted);
            ButtonLoaderAnimator buttonLoader = null;
            if (accepted) buttonLoader = acceptLoader;
            else buttonLoader = declineLoader;

            if (buttonLoader == null) ShowCompleteGroup();
            else buttonLoader.StopLoadingAnimation(onStopped: ShowCompleteGroup);
        }

        private void ShowCompleteGroup()
        {
            completeGroup.blocksRaycasts = true;
            LeanTween.alphaCanvas(buttonsGroup, 0f, 0.4f);
            LeanTween.alphaCanvas(completeGroup, 1f, 0.4f);
        }

        private void OnAcceptPlayerFailed(long errorCode, string errorMessage)
        {
            notifications.ShowNotification(new Notification(NotificationType.Warning, 
                ClansLocalization.Get(LocalizationKeys.notificationErrorAcceptPlayer)));
            OnRequestFailed(errorCode, errorMessage);
        }

        private void OnDeclinePlayerFailed(long errorCode, string errorMessage)
        {
            notifications.ShowNotification(new Notification(NotificationType.Warning,
                ClansLocalization.Get(LocalizationKeys.notificationErrorKickUser)));
            OnRequestFailed(errorCode, errorMessage);
        }

        private void OnRequestFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnRequestFailed: {0}; {1}", errorCode, errorMessage);

            ButtonLoaderAnimator loader = null;
            if (acceptLoader.LoadingStarted) loader = acceptLoader;
            else if (declineLoader.LoadingStarted) loader = declineLoader;

            if (loader == null) ResetViews();
            else loader.StopLoadingAnimation(success: false, hideButtonOnSuccess: false, onStopped: ResetViews);
        }

        private void ResetViews()
        {
            buttonsGroup.blocksRaycasts = true;
            acceptButton.interactable = true;
            declineButton.interactable = true;
        }

    }
}