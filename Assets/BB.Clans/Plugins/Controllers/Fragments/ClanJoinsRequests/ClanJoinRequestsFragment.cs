using System.Collections.Generic;
using BlackBears.Clans.Model;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ClanJoinRequests
{

    internal class ClanJoinRequestsFragment : Fragment
    {

        [SerializeField] private JoinRequestCell cellPrefab;
        [SerializeField] private Image background;
        [SerializeField] private Text title;

        private ClanCore core;
        private NotificationsFeature notifications;
        private PredefinedIconsContainer profileIcons;
        private SpritePack scorePack;
        private ClanUser[] users;

        internal override FragmentKind Kind { get { return FragmentKind.ClanJoinRequests; } }

        internal void Inject(ClanCore core, PredefinedIconsContainer profileIcons,
            SpritePack scorePack, NotificationsFeature notifications)
        {
            this.core = core;
            this.notifications = notifications;
            this.profileIcons = profileIcons;
            this.scorePack = scorePack;
        }

        protected override void Start()
        {
            base.Start();
            LoadPlayers();
        }

        private void LoadPlayers()
        {
            ShowLoader(title.text, background.color);
            core.GetIncomingPlayers(OnClanUsersLoaded, OnClanUsersLoadingFailed);
        }

        private void OnClanUsersLoaded(ClanUser[] users)
        {
            if (Disposed) return;

            List<string> ids = new List<string>();
            this.users = users;
            foreach (var user in this.users) ids.Add(user.Id);
            core.State.ProfileCache.LoadPlayersAsync(ids, OnProfilesLoaded, OnProfilesLoadFailed);
        }

        private void OnProfilesLoaded()
        {
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
            if (users == null) return;

            foreach (var user in users)
            {
                if (user == null) continue;

                var cell = Instantiate(cellPrefab);
                cell.RectTransform.SetParent(Scroll.content, false);
                cell.RectTransform.SetAsLastSibling();
                cell.Inject(FragmentsController, core, notifications, user, core.State.ProfileCache.GetCachedPlayer(user.Id),
                    profileIcons, scorePack);
            }
        }

        private void OnClanUsersLoadingFailed(long errorCode, string erorMessage)
        {
            Debug.LogFormat("OnClanUsersLoadingFailed: {0}; {1}", errorCode, erorMessage);
            if (Disposed) return;

            LeanTween.delayedCall(gameObject, loaderDelay, () =>
            {
                if (Disposed) return;

                HideLoader();
                ShowError(LoadPlayers);
            });
        }

        private void OnProfilesLoadFailed(long errorCode, string erorMessage)
        {
            Debug.LogFormat("OnProfilesLoadFailed: {0}; {1}", errorCode, erorMessage);
            LeanTween.delayedCall(gameObject, loaderDelay, HideLoader);
        }

    }

}