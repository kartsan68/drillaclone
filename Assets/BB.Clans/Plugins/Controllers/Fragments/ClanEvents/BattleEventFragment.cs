using System.Collections.Generic;
using BlackBears.Clans.Events;
using BlackBears.Clans.Events.ClanBattle;
using BlackBears.Clans.Model;
using BlackBears.Utils;
using UnityEngine;

namespace BlackBears.Clans.Controller.Fragments.ClanEvents
{

    public abstract class BattleEventFragment : Fragment
    {

        private const float tickPeriod = 1f;

        [SerializeField] private Color loaderColor = ColorUtils.FromHex(0x385680FF);

        private float tickTimer = tickPeriod;

        internal override FragmentKind Kind => FragmentKind.BattleEvent;

        protected ClanCore ClanCore { get; private set; }
        protected ClanFullSettings Settings { get; private set; }

        protected PredefinedIconsContainer ProfileIcons => Settings.configuration.ProfileIcons;
        protected PredefinedIconsContainer ClanIcons => Settings.configuration.ClanIcons;

        protected EventsCore EventsCore => ClanCore.State.EventsCore;
        protected BattleEvent BattleEvent => EventsCore.BattleEvent;

        internal void Inject(ClanCore core, ClanFullSettings settings)
        {
            this.ClanCore = core;
            this.Settings = settings;
            OnInjectFinished();
        }

        protected virtual void OnInjectFinished() { }

        protected override void Start()
        {
            base.Start();
            BattleEvent.OnSynchronizeFinished += OnEventSyncrhonized;
        }

        protected virtual void Update()
        {
            tickTimer -= Time.deltaTime;
            if (tickTimer > 0f) return;
            tickTimer = tickPeriod;
            OnTick();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            BattleEvent.OnSynchronizeFinished -= OnEventSyncrhonized;
        }

        internal void OpenClanFragment(Clan clan)
        {
            if (clan != null) FragmentsController.ShowClanInfo(clan);
        }

        internal void OpenPlayerFragment(ClanUser user)
        {
            if (user == null) return;

            ShowLoader(null, loaderColor);
            ClanCore.State.ClanCache.LoadClanAsync(user.ClanId, false,
                clan =>
                {
                    FragmentsController.ShowPlayerInfo(user.Id, user, clan);
                    HideLoader();
                }, (c, m) =>
                {
                    Debug.Log($"OpenPlayerFragment: {c}; {m}");
                    //TODO!: показать нотификацию
                    HideLoader();
                });
        }

        protected virtual void OnEventSyncrhonized()
        {
            LoadingRatingData();
        }

        protected virtual void OnTick() { }
        protected virtual void OnClanLoaded(int position, Clan clan, BattleClanData data) { }
        protected virtual void OnStartLoadingRatingData() { }
        protected virtual void SetupClansData(IList<BattleClanData> clansData) { }

        private void LoadingRatingData()
        {
            OnStartLoadingRatingData();

            if (BattleEvent.Battle == null) return;
            var clansData = BattleEvent.Battle.ClansData;
            if (clansData == null) return;
            SetupClansData(clansData);

            for (int i = 0; i < clansData.Count; i++)
            {
                LoadClan(i + 1, clansData[i]);
            }
        }

        private void LoadClan(int position, BattleClanData data)
        {
            if (data == null) return;

            Block<Clan> clanLoadedCallback = clan => OnClanLoaded(position, clan, data);
            FailBlock clanLoadFailedCallback = (c, m) => UnityEngine.Debug.LogWarning($"LoadClan: {c}; {m}");

            //TODO: Загружать всё через кеш (else ветка). А это костыль.
            if (ClanCore.State.IsUserClan(data.id))
            {
                ClanCore.GetClan(ClanCore.State.Clan, clanLoadedCallback, clanLoadFailedCallback);
            }
            else
            {
                ClanCore.State.ClanCache.LoadClanAsync(data.id, false, clanLoadedCallback, clanLoadFailedCallback);
            }
        }

    }

}