﻿using System;
using BlackBears.Clans.Model;
using BlackBears.Clans.Networking;
using BlackBears.GameCore.Networking;
using UnityEngine;

namespace BlackBears.GameServer
{

    internal partial class ConnectionBehaviour : MonoBehaviour
    {

        internal void ActivateLeagues(string playerId, double score, string privateKey, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException();

            string url = GenerateUrl("player/activate-leagues");
            var converter = new SuccessResponceConverter(onSuccess, "success");

            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);

            string key = string.Format("{0}{1}{2}", playerId, score, privateKey);
            AddKeyToRequest(key, request);

            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("score", score);
            
            AddDefaultKeys(request);

            StartCoroutine(request.Start());
        }

        internal void GetLeague(string currentUserId, Block<LeagueResponse> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException();

            string url = GenerateUrl("player/get-league");
            var converter = new LeagueDataConverter(playersManager, onSuccess);

            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            if (currentUserId != null) request.AddRequestValue("player_id", currentUserId);

            AddDefaultKeys(request);

            StartCoroutine(request.Start());
        }

    }

}


