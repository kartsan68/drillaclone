﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.GameCore.Features.Leagues;

namespace BlackBears.GameServer
{

    public class LeagueResponse
    {

        internal List<ServerPlayer> PlayersList { get; set; }
        internal long LeagueNum { get; set; }
        internal int LeagueType { get; set; }
        internal long NextRecalcTime { get; set; }
        internal List<int> FlagsDelta { get; set; }
        internal List<LeagueBoundaries> LeagueBoundaries { get; set; }

        public LeagueResponse()
        {
            PlayersList = new List<ServerPlayer>();
            FlagsDelta = new List<int>();
            LeagueBoundaries = new List<LeagueBoundaries>();
        }

    }

}


