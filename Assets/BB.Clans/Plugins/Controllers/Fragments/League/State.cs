﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BlackBears.GameServer;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Leagues
{

    internal class State : IState
    {

        private const string rewardTakenKey = "rwrdTk";
        private const string oldRewardTimeKey = "orwdTm";
        private const string rewardTimeKey = "rwrdTm";
        private const string isLeaguesActivatedKey = "ileagact";

        internal long lastUpdateTime;
        internal PLong oldRewardTime;
        internal PLong rewardTime;
        internal int rewardTakenCount;

        internal List<ServerPlayer> playersList;
        internal long leagueNumber;
        internal int leagueType;
        internal List<int> flagsDelta;
        internal List<LeagueBoundaries> leagueBoundaries;

        internal bool isLeaguesActivated;

        public void FromJson(JSONNode json)
        {
            if (json == null || json.Tag != JSONNodeType.Object) return;

            rewardTakenCount = json[rewardTakenKey].AsInt;
            oldRewardTime = PLong.Parse(json[oldRewardTimeKey].Value);
            rewardTime = PLong.Parse(json[rewardTimeKey].Value);
            isLeaguesActivated = json[isLeaguesActivatedKey].AsBool;
        }

        long IState.LeagueNumber => leagueNumber;
        int IState.LeagueType => leagueType;
        long IState.OldRewardTime => oldRewardTime;
        long IState.RewardTime => rewardTime;
        int IState.RewardTakenCount => rewardTakenCount;
        IList<ServerPlayer> IState.PlayersList => playersList;
        bool IState.IsLeaguesActivated => isLeaguesActivated;
        IList<int> IState.FlagsDelta => flagsDelta;
        IList<LeagueBoundaries> IState.LeagueBoundaries => leagueBoundaries;

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[rewardTakenKey] = rewardTakenCount;
            json[oldRewardTimeKey] = oldRewardTime.ToString();
            json[rewardTimeKey] = rewardTime.ToString();
            json[isLeaguesActivatedKey] = isLeaguesActivated;

            return json;
        }

    }

}
