using System.Collections.Generic;
using BlackBears.GameServer;

namespace BlackBears.GameCore.Features.Leagues
{

    public interface IState
    {

        long LeagueNumber { get; }
        long OldRewardTime { get; }
        long RewardTime { get; }
        int RewardTakenCount { get; }
        int LeagueType { get; }
        IList<ServerPlayer> PlayersList { get; }
        bool IsLeaguesActivated { get; }
        IList<int> FlagsDelta { get; }
        IList<LeagueBoundaries> LeagueBoundaries { get; }
    }

}