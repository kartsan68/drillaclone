using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.GameServer
{

    public class LeagueBoundaries
    {
        public int LeagueType;
        public int StartBound;
        public int EndBound;

        public LeagueBoundaries(JSONNode node)
        {
            LeagueType = node["type"].AsInt;
            StartBound = node["start_bound"].AsInt;
            EndBound = node["end_bound"].AsInt;
        }
    }
}


