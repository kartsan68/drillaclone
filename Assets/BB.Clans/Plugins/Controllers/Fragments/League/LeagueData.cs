﻿using BlackBears.GameServer;
using SimpleJSON;
using System.Collections.Generic;

namespace BlackBears.GameCore.Features.Leagues
{

    public class LeagueData
    {
        private const string playerIdInKey = "player_id";
        private const string iconKey = "icon";
        private const string nickKey = "nick";
        private const string countryKey = "country";
        private const string leagueInKey = "league";
        private const string registerTimeKey = "time";
        private const string gameDataInKey = "data";
        private const string scoreKey = "score";


        internal readonly bool enabled;
        internal readonly long invalidateInterval;
        internal readonly long firstRewardDelay;
        internal readonly long ongoingRewardDelay;
        internal readonly int playerCountInLeague;
        internal readonly List<ServerPlayer> fakePlayers;

        internal LeagueData(JSONNode json)
        {
            if (json == null) return;
            enabled = json["enabled"].AsBool;
            invalidateInterval = json["invalidate_interval"].AsLong;
            firstRewardDelay = json["first_reward_delay"].AsLong;
            ongoingRewardDelay = json["ongoing_reward_delay"].AsLong;
            playerCountInLeague = json["league_player_count"].AsInt;


            var fakePlayersData = json["league_fake_players"].AsArray;
            fakePlayers = new List<ServerPlayer>();
            for (int i = 0; i < fakePlayersData.Count; i++)
            {
                var jsonPlayer = fakePlayersData[i].AsObject;
                ServerPlayer player = new ServerPlayer();
                player.Id = jsonPlayer[playerIdInKey];
                player.Nick = jsonPlayer[nickKey];
                player.Country = jsonPlayer[countryKey];
                player.Icon = jsonPlayer[iconKey];
                player.League = jsonPlayer[leagueInKey];
                player.RegisterDate = jsonPlayer[registerTimeKey];
                player.Score = jsonPlayer[scoreKey];
                fakePlayers.Add(player);
            }
        }

    }

}


