﻿using System;
using System.Collections.Generic;
using BlackBears.Clans.Model;
using BlackBears.GameCore.Features.JellyB;
using UnityEngine;

namespace BlackBears.GameServer
{

    public partial class ServerConnection : IConnectionDataProvider
    {

        public void ActivateLeagues(string playerId, double score, string privateKey, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.ActivateLeagues(playerId, score, privateKey, onSuccess, onFail);
        }

        public void GetLeague(string currentUserId, Block<LeagueResponse> onSuccess, FailBlock onFail)
        {
            behaviour.GetLeague(currentUserId, onSuccess, onFail);
        }

    }

}

