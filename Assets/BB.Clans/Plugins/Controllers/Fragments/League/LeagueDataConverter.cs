﻿using System.Collections.Generic;
using BlackBears.GameCore.Features.Leagues;
using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.GameServer
{

    internal class LeagueDataConverter : DataConverter<LeagueResponse> 
    {

        private PlayersManager playersManager;

        public LeagueDataConverter(PlayersManager playersManager, Block<LeagueResponse> onConvert)
            : base(onConvert, null)
        { 
            this.playersManager = playersManager;
        }

        protected override bool Convert(ref LeagueResponse instance, JSONNode data)
        {
            if (instance == null) instance = new LeagueResponse();

            var playersData = data["players"];
            for (int i = 0; i < playersData.Count; i++)
            {
                instance.PlayersList.Add(playersManager.CreateServerPlayer(playersData[i]));
            }
            var league = data["league_num"];
            instance.LeagueNum = league;

            instance.LeagueType = data["league_type"].AsInt;

            var jsonFlagsDelta = data["flags_delta"].AsArray;
            for (int i = 0; i < jsonFlagsDelta.Count; i++)
            {
                instance.FlagsDelta.Add(jsonFlagsDelta[i].AsInt);
            }

            var jsonBoundariesArray = data["leagues_boundaries"].AsArray;
            for (int i = 0; i < jsonBoundariesArray.Count; i++)
            {
                instance.LeagueBoundaries.Add(new LeagueBoundaries(jsonBoundariesArray[i]));
            }

            instance.NextRecalcTime = data["next_recalc_time"].AsLong;

            return true;
        }


    }

}

