namespace BlackBears.Clans.Controller.Fragments
{
    
    public enum FragmentKind
    {
        ClanEdit,
        Player,
        PlayerEdit,
        ClanInfo,
        ClanTop,
        ClanJoinRequests,
        Invites,
        Support,
        ResourceRequest,
        ResourceTake,
        League,

        BattleEvent,

        InGameFragment // Должен всегда быть последним
    }

}