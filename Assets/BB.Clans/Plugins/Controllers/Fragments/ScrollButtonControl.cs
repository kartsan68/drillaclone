using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments
{

    [RequireComponent(typeof(Button))]
    internal class ScrollButtonControl : MonoBehaviour
    {

        private Button button;
        private Block onClickCallback;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(OnClick);
        }

        internal void SetClickCallback(Block callback) { onClickCallback = callback; }
        internal void Show() { button.interactable = true; }
        internal void Hide() { button.interactable = false; }

        private void OnDestroy()
        {
            onClickCallback = null;
        }

        private void OnClick()
        {
            onClickCallback.SafeInvoke();
        }

    }

}