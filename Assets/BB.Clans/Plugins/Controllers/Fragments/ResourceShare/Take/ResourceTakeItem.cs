using System;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ResourceShare;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ResourceShare
{

    internal class ResourceTakeItem : MonoBehaviour
    {

        [SerializeField] private PredefinedIconView playerIcon;
        [SerializeField] private Text nicknameText;
        [SerializeField] private Text amountText;
        [SerializeField] private Text timerText;
        [SerializeField] private Image resourceImage;
        [SerializeField] private CanvasGroup defaultCanvas;
        [SerializeField] private CanvasGroup lockedCanvas;

        private PredefinedIconsContainer playerIcons;
        private PlayerProfileCache profileCache;
        private WarehouseConfig config;
        private IGameAdapter game;

        private bool disposed;

        internal bool Initialized { get; private set; }
        internal int Index { get; private set; }
        internal IncomingDonation Donation { get; private set; }

        private RectTransform _rectTransform;
        internal RectTransform RectTransform => _rectTransform ?? (_rectTransform = GetComponent<RectTransform>());

        internal void Inject(PredefinedIconsContainer playerIcons, PlayerProfileCache profileCache,
            WarehouseConfig config, IGameAdapter game)
        {
            this.playerIcons = playerIcons;
            this.profileCache = profileCache;
            this.config = config;
            this.game = game;
            Initialized = true;
        }

        private void OnDestroy()
        {
            disposed = true;
            LeanTween.cancel(gameObject);
            LeanTween.cancel(defaultCanvas.gameObject);
            LeanTween.cancel(lockedCanvas.gameObject);
        }

        internal void SetData(int index, IncomingDonation donation, GameResource resource, long actualTime)
        {
            if (disposed) return;

            this.Index = index;
            this.Donation = donation;
            if (donation == null)
            {
                gameObject.SetActive(false);
                return;
            }

            nicknameText.text = ValidateHelper.PlayerIdToTempNick(donation.playerId);
            amountText.text = $"+{game.AmountToPrettyString(donation.value)}";
            if (resource != null)
            {
                resourceImage.sprite = resource.icon;
                resourceImage.enabled = true;
            }
            else
            {
                resourceImage.enabled = false;
            }

            UpdateTimer(actualTime);
            profileCache.GetPlayerAsync(donation.playerId, OnPlayerLoaded, null);
            gameObject.SetActive(true);
        }

        internal void UpdateTimer(long actualTime)
        {
            if (disposed || Donation == null) return;
            if (actualTime - Donation.grabTime > config.incomeDonationForgetTime)
            {
                gameObject.SetActive(false);
            }
            else
            {
                timerText.text = DateUtils.SecondsToHHMMSS(Donation.grabTime
                    + config.incomeDonationForgetTime - actualTime);
            }
        }

        internal void Lock(Block onComplete)
        {
            if (disposed)
            {
                onComplete.SafeInvoke();
                return;
            }

            LeanTween.alphaCanvas(defaultCanvas, 0f, 0.2f);
            LeanTween.alphaCanvas(lockedCanvas, 1f, 0.2f).setOnComplete(() => onComplete.SafeInvoke());
        }

        private void OnPlayerLoaded(IPlayerProfile profile)
        {
            if (disposed || Donation == null ||
                profile == null || 
                !string.Equals(Donation.playerId, profile.Id))
            {
                return;
            }

            nicknameText.text = profile.Nickname;
            playerIcon.SetIcon(profile.Icon, playerIcons);
        }

    }

}