
using System.Collections;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.Fragments.ResourceShare
{

    public class ResourceTakeFragment : Fragment
    {

        private const float tickPeriod = 1f;

        [SerializeField] private Color loaderColor = ColorUtils.FromHex(0xC54343FF);
        [SerializeField] private Color loaderTextColor = ColorUtils.FromHex(0xFFFFFFFF);
        [SerializeField] private VerticalInfiniteController infiniteController;
        [SerializeField] private Button takeActiveButton;
        [SerializeField] private Button takeInactiveButton;
        [SerializeField] private RectTransform takeContainer;
        [SerializeField] private CanvasGroup scrollCanvasGroup;

        private Warehouse warehouse;
        private ResourceTakeInfiniteAdapter adapter;

        internal override FragmentKind Kind => FragmentKind.ResourceTake;

        internal void Inject(Warehouse warehouse, PlayerProfileCache profileCache, 
            PredefinedIconsContainer playerIcons, IGameAdapter game,
            ChronometerFeature chronometer)
        {
            this.warehouse = warehouse;
            adapter = new ResourceTakeInfiniteAdapter(warehouse, profileCache, playerIcons,
                game, chronometer, scrollCanvasGroup);
        }

        protected override void Start()
        {
            base.Start();
            
            ShowLoader(ClansLocalization.Get(LocalizationKeys.resourceTakeTitle).ToUpper(), loaderColor, loaderTextColor);
            warehouse.ValidateWithServer(OnValidateFinished, OnValidateFailed);

            takeActiveButton.onClick.AddListener(TakeResources);
            takeInactiveButton.gameObject.SetActive(false);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            LeanTween.cancel(takeContainer.gameObject);
            adapter.Dispose();
        }

        private void Update()
        {
            //TODO: Тикать раз в секунду
            if (adapter != null) adapter.Tick();
        }

        private void TakeResources()
        {
            takeInactiveButton.gameObject.SetActive(true);
            takeActiveButton.gameObject.SetActive(false);

            takeActiveButton.interactable = false;
            adapter.TakeResources(Closing);
        }

        private void OnValidateFinished() 
        {
            if (Disposed) return;
            adapter.Validate(OnAdapterValidateFinished);
        }

        private void OnValidateFailed(long code, string message)
        {
            Debug.Log($"OnValidateFailed: {code}; {message}");
            OnValidateFinished();
        }

        private void OnAdapterValidateFinished()
        {
            if (Disposed) return;
            HideLoader();
            StartCoroutine(PostInitiate());
        }

        private void Closing()
        {
            LeanTween.moveAnchoredY(takeContainer, -takeContainer.rect.height, 0.2f).setEaseInQuad();
            LeanTween.delayedCall(takeContainer.gameObject, 0.4f, CloseRequest);
        }

        private IEnumerator PostInitiate()
        {
            yield return new WaitForFixedUpdate();
            infiniteController.Adapter = adapter;
            infiniteController.Initiate();
        }

    }

}