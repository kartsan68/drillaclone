﻿using System;
using System.Collections.Generic;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using UnityEngine;
using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.Fragments.ResourceShare
{

    internal class ResourceTakeInfiniteAdapter : InfiniteAdapter, IDisposable
    {

        private float scrollSpeed = 320;
        private float scrollMaxMoveTime = 0.6f;
        private float moveItemYSpeed = 240;

        private const int typeResource = 0;
        private const int typeNewMessage = 1;

        private Warehouse warehouse;
        private PredefinedIconsContainer playerIcons;
        private PlayerProfileCache profileCache;
        private IGameAdapter game;
        private ChronometerFeature chronometer;
        private CanvasGroup canvasGroup;

        private int newMessageIndex = -1;
        private List<IncomingDonation> donations;

        private bool disposed;
        private bool onTaking;
        private float currentTakePosition;
        private int lockedItemsCount = 0;
        private Block onComplete;
        private List<ResourceTakeItem> items = new List<ResourceTakeItem>();
        private GameObject newMessage;

        public ResourceTakeInfiniteAdapter(Warehouse warehouse, PlayerProfileCache profileCache,
            PredefinedIconsContainer playerIcons, IGameAdapter game,
            ChronometerFeature chronometer, CanvasGroup canvasGroup)
        {
            this.warehouse = warehouse;
            this.profileCache = profileCache;
            this.playerIcons = playerIcons;
            this.game = game;
            this.chronometer = chronometer;
            this.canvasGroup = canvasGroup;

            donations = new List<IncomingDonation>(warehouse.State.IncomingDonations);
            donations.Sort(IncomingDonation.GrabTimeCompare);

            var grabMinTime = chronometer.ActualTime - warehouse.State.Config.incomeDonationOldTime;
            for (int i = 0; i < donations.Count; i++)
            {
                if (donations[i].grabTime < grabMinTime) continue;
                newMessageIndex = i;
                break;
            }
            // Если надпись новые должна быть в начале - выключаем её
            if (newMessageIndex == 0) newMessageIndex = -1;
        }

        public override int GetTypeByIndex(int index)
        {
            if (index == newMessageIndex) return typeNewMessage;
            return typeResource;
        }

        public override GameObject InstantiatePrefab(GameObject prefab) =>
            UnityEngine.Object.Instantiate(prefab);

        public override bool IsValidIndex(int index)
        {
            if (index < 0) return false;
            if (donations == null || donations.Count == 0) return false;
            if (newMessageIndex > 0) return index <= donations.Count;
            else return index < donations.Count;
        }

        internal void Validate(Block onComplete)
        {
            var playerIds = new List<string>();
            foreach (var d in donations) playerIds.Add(d.playerId);
            profileCache.LoadPlayersAsync(playerIds, onComplete, (c, m) => onComplete.SafeInvoke());
        }

        public override void BindItem(GameObject itemObject, int index, int type)
        {
            itemObject.SetActive(true);

            if (type == typeResource)
            {
                var item = itemObject.GetComponent<ResourceTakeItem>();
                if (!item.Initialized)
                {
                    item.Inject(playerIcons, profileCache, warehouse.State.Config, game);
                }
                int donationIndex = index;
                if (newMessageIndex > 0 && index > newMessageIndex) --donationIndex;
                var donation = donations[donationIndex];
                var resource = warehouse.GetResourceWithTypeById(donation.resourceType, donation.resourceId);
                item.SetData(index, donation, resource, chronometer.ActualTime);
                items.Add(item);
                if (onTaking) OnResourceItemAppear();
            }
            else if (type == typeNewMessage)
            {
                this.newMessage = itemObject;
            }
        }

        public override void UnbindItem(GameObject itemObject, int index, int type)
        {
            if (onTaking) return;
            if (type == typeResource)
            {
                var item = itemObject.GetComponent<ResourceTakeItem>();
                items.Remove(item);
            }
            else if (type == typeNewMessage)
            {
                newMessage = null;
            }
        }

        public void Dispose()
        {
            if (disposed) return;

            disposed = true;
            RectTransform content = Controller?.Scroll?.content;
            if (content != null) LeanTween.cancel(content);
            if (newMessage != null) LeanTween.cancel(newMessage);
        }

        internal void Tick()
        {
            var actualTime = chronometer.ActualTime;
            for (int i = 0; i < items.Count; i++) items[i].UpdateTimer(actualTime);
        }

        internal void TakeResources(Block onComplete)
        {
            if (items.Count == 0)
            {
                onComplete.SafeInvoke();
                return;
            }

            this.onComplete = onComplete;
            Controller.Scroll.enabled = true;
            canvasGroup.blocksRaycasts = false;

            warehouse.SendAllIncomingDonationsToGame();

            float moveTime = Mathf.Abs(Controller.Scroll.content.anchoredPosition.y / scrollSpeed);
            moveTime = Mathf.Min(moveTime, scrollMaxMoveTime);

            LeanTween.moveAnchoredY(Controller.Scroll.content, 0, moveTime)
                .setEaseOutQuad()
                .setOnComplete(CloseShowedObjects);
        }

        public override void SetupParent(RectTransform item, RectTransform parent)
        {
            if (item == null || item.parent == parent) return;
            item.SetParent(parent, false);
        }

        private void CloseShowedObjects()
        {
            if (onTaking) return;

            onTaking = true;
            if (newMessage != null)
            {
                var canvasGroup = newMessage.GetComponent<CanvasGroup>();
                if (canvasGroup != null) LeanTween.alphaCanvas(canvasGroup, 0, 0.2f);
                else newMessage.SetActive(false);
            }

            items.Sort(SortItemsByY);
            currentTakePosition = items[0].RectTransform.anchoredPosition.y;
            for (int i = 0; i < items.Count; i++) AnimateItem(items[i]);
        }

        private void OnResourceItemAppear()
        {
            var cur = items[items.Count - 1].RectTransform;
            var prev = items[items.Count - 2].RectTransform;
            var pos = cur.anchoredPosition;
            pos.y = prev.anchoredPosition.y - prev.rect.height;
            cur.anchoredPosition = pos;

            AnimateItem(items[items.Count - 1]);
        }

        private bool debug = false;

        private void AnimateItem(ResourceTakeItem item)
        {
            var rt = item.RectTransform;
            float time = (-rt.anchoredPosition.y + currentTakePosition) / moveItemYSpeed;
            LeanTween.moveAnchoredY(rt, currentTakePosition, time);
            bool locked = item.Donation == null;
            if (!locked) locked = warehouse.HasIncomingDonation(item.Donation);
            if (locked)
            {
                LeanTween.delayedCall(item.gameObject, time, () =>
                {
                    lockedItemsCount += 1;
                    item.Lock(OnItemLocked);
                });
                currentTakePosition -= item.RectTransform.rect.height;
            }
            else
            {
                LeanTween.moveAnchoredX(rt, -320, 0.2f).setDelay(time)
                    .setEaseInQuad().setOnComplete(() => OnItemAnimated(item));
            }
            debug = !debug;
        }

        private void OnItemAnimated(ResourceTakeItem item)
        {
            if (disposed) return;

            if (-currentTakePosition > Controller.Scroll.content.rect.height) return;

            bool nextAdded = Controller.ForceAddNext();
            if (nextAdded)
            {
                Controller.ForceReturnItem(item.RectTransform, item.Index);
            }
            else if (item == items[items.Count - 1])
            {
                onComplete.SafeInvoke();
                onComplete = null;
            }
            else
            {
                Controller.ForceReturnItem(item.RectTransform, item.Index);
            }
        }

        private void OnItemLocked()
        {
            if (disposed) return;
            lockedItemsCount -= 1;
            if (lockedItemsCount == 0 &&
                -currentTakePosition > Controller.Scroll.content.rect.height)
            {
                onComplete.SafeInvoke();
                onComplete = null;
            }
        }

        private int SortItemsByY(ResourceTakeItem x, ResourceTakeItem y)
        {
            return (int)Mathf.Sign(-(x.RectTransform.anchoredPosition.y - y.RectTransform.anchoredPosition.y));
        }

    }

}