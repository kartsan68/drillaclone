﻿using BlackBears.Clans.ResourceShare;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Fragments.ResourceShare
{

    internal class GameResourceItem : MonoBehaviour
    {

		[SerializeField] private Text nameText;
		[SerializeField] private Text amountText;
		[SerializeField] private Image resourceImage;
		[SerializeField] private Button button;

		internal Block<GameResourceItem> onClick;

        internal GameResource GameResource { get; private set; }
        internal double Amount { get; private set; }

        private void Awake()
		{
			button.onClick.AddListener(() => onClick.SafeInvoke(this));
		}

		private void OnDestroy()
		{
			onClick = null;
		}

		internal void SetData(GameResource gameResource, double amount, string amountStr)
		{
			this.GameResource = gameResource;
			this.Amount = amount;
			UpdateView(amountStr);
		}

		private void UpdateView(string amountStr)
		{
			if (GameResource == null) return;
			nameText.text = ClansLocalization.Get(GameResource.nameKey).ToUpper();
			amountText.text = amountStr;
			resourceImage.sprite = GameResource.icon;
		}


    }

}