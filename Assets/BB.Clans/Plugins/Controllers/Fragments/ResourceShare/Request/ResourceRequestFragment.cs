using System;
using System.Collections;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.Fragments.ResourceShare
{

    internal class ResourceRequestFragment : Fragment
    {

        [SerializeField] private Color backgroundColor = ColorUtils.FromHex(0x786656FF);
        [SerializeField] private VerticalInfiniteController controller;

        private RequestResourceInfiniteAdapter adapter;

        internal override FragmentKind Kind => FragmentKind.ResourceRequest;

        internal void Inject(Warehouse warehouse, IGameAdapter game)
        {
            adapter = new RequestResourceInfiniteAdapter(this, warehouse, game);
        }

        protected override void Start()
        {
            base.Start();

            StartCoroutine(PostInitiate());
        }

        internal void ShowLoader() => ShowLoader(string.Empty, backgroundColor);
        internal new void HideLoader() => base.HideLoader();
        internal void Close() => CloseRequest();
        
        private IEnumerator PostInitiate()
        {
            yield return new WaitForFixedUpdate();
            controller.Adapter = adapter;
            controller.Initiate();
        }

    }

}