using System.Collections.Generic;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;
using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.Fragments.ResourceShare
{

    internal class RequestResourceInfiniteAdapter : InfiniteAdapter
    {

        private const int typeTitle = 0;
        private const int typeResource = 1;

        private ResourceRequestFragment fragment;
        private Warehouse warehouse;
        private IGameAdapter game;
        private List<GameResource> resources;
        private bool requestsLocked = false;

        internal RequestResourceInfiniteAdapter(ResourceRequestFragment fragment, Warehouse warehouse,
            IGameAdapter game)
        {
            this.fragment = fragment;
            this.warehouse = warehouse;
            this.game = game;
            resources = warehouse.GenerateAvailableResourcesList();
        }

        public override int GetTypeByIndex(int index)
        {
#if BBGC_LANDSCAPE
            return 0;
#else
            if (index == 0) return typeTitle;
            return typeResource;
#endif

        }

        public override GameObject InstantiatePrefab(GameObject prefab) => Object.Instantiate(prefab);

        public override bool IsValidIndex(int index)
        {
            if (index < 0) return false;
#if !BBGC_LANDSCAPE
            if (index == 0) return true;
            if (resources == null) return false;
            return (index - 1) < resources.Count;
#else
            if (resources == null) return false;
            return index < resources.Count;
#endif
        }

        public override void BindItem(GameObject itemObject, int index, int type)
        {
            itemObject.SetActive(true);

#if !BBGC_LANDSCAPE
            if (type != typeResource) return;
#endif

            var resourceItem = itemObject.GetComponent<GameResourceItem>();
#if BBGC_LANDSCAPE
            var resource = resources[index];
#else
            var resource = resources[index - 1];
#endif
            double amount = warehouse.GetRequestingAmountOfResources(resource);
            resourceItem.SetData(resource, amount, game.AmountToPrettyString(amount));
            if (resourceItem.onClick == null) resourceItem.onClick = OnResourceSelected;
        }

        private void OnResourceSelected(GameResourceItem item)
        {
            if (requestsLocked || item == null) return;
            requestsLocked = true;
            fragment.ShowLoader();
            warehouse.RequestResource(item.GameResource, item.Amount, () =>
            {
                fragment.Close();
            },
            (c, m) =>
            {
                fragment.HideLoader();

                var notifications = BBGC.Features.GetFeature<NotificationsFeature>();
                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.errorCommonTitle)));

                Debug.LogError($"OnResourceSelected: {c}; {m}");
            });
        }

    }

}