using System.Collections;
using BB.GameCore.Plugins;
using BlackBears.Clans.Controller.CountrySelection;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Loader;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Additional;

using Logger = BlackBears.GameCore.Logger;

namespace BlackBears.Clans.Controller.Panels.PlayerRegister
{

    internal class PlayerRegisterClanPanel : ClanPanel
    {

        [Header("Параметры регистрации")]
        [SerializeField] private RectTransform settingsRoot;
        [SerializeField] private RectTransform profilePickContainer;
        [SerializeField] private CanvasGroup profilePickCanvasGroup;
        [SerializeField] private float nickSettingKeyboardOffset;
        [SerializeField] private float fullSettingsKeyboardOffset;
        [SerializeField] private float fullSettingsOffset;

        [SerializeField] private RectTransform nextButtonContainer;
        [SerializeField] private CanvasGroup nextButtonGroup;
        [SerializeField] private LeanTweenType nextButtonEase;

        [SerializeField] private StateExtendedInputField nameInput;
        [SerializeField] private ErrorInputView nameError;
        [SerializeField] private CanvasGroup otherSettingsGroup;
        [SerializeField] private IconEditController iconEdit;
        [SerializeField] private LoaderAnimator loader;
        [UnityEngine.Serialization.FormerlySerializedAs("selector")]
        [SerializeField] private CountrySelector countrySelector;

        [SerializeField] private Button closeButton;
        [SerializeField] private Button saveButton;
        [SerializeField] private GameObject saveButtonInactive;
        [SerializeField] private Image logo;

        private CountryListContainerAdapter countryAdapter;
        private NotificationsFeature notifications;

        private bool onRegister;

        protected override void InjectFinished()
        {
            base.InjectFinished();
            iconEdit.Inject(Configuration.ProfileIcons);
            countrySelector.Inject(Configuration.Countries);
        }

        private void Start()
        {
            notifications = BBGC.Features.GetFeature<NotificationsFeature>();

            closeButton.onClick.AddListener(() => CloseRequest());
            saveButton.onClick.AddListener(SaveRequest);
            SetSaveButtonActive(false);
#if !BBGC_LANDSCAPE
            otherSettingsGroup.alpha = 0f;
            otherSettingsGroup.gameObject.SetActive(false);
#endif

            nameInput.inputType = InputField.InputType.AutoCorrect;
            nameInput.characterLimit = ValidateHelper.nicknameMaxLength;
            nameInput.ActivateInputField();
            nameInput.onValueChanged.AddListener(OnNameChanged);
            nameInput.onEndEdit.AddListener(ValidateInput);
            countrySelector.Initiate(SQBA.SQBA.CurrentLocale());
            StartCoroutine(PostInit());

            KeyboardUtility.OnKeyboardHeightRatioChanged.AddListener(OnHeightRatioChanged);

            loader.Hide(true);
        }

        private IEnumerator PostInit()
        {
            yield return new WaitForEndOfFrame();
            nameInput.UpdateVisualState();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            CancelTransition();
        }

        protected override void Show(bool notify)
        {
            base.Show(false);
            var posEnd = new Vector2(nextButtonContainer.anchoredPosition.x, 0);
            var posStart = posEnd;
            posStart.y = -nextButtonContainer.rect.height;
            nextButtonContainer.anchoredPosition = posStart;
            LeanTween.moveAnchored(nextButtonContainer, posEnd, transitionStep)
                .setDelay(transitionStep)
                .setEase(nextButtonEase)
                .setOnComplete(Showed);
        }

        protected override void CancelTransition()
        {
            base.CancelTransition();
            LeanTween.cancel(nextButtonContainer.gameObject);
            LeanTween.cancel(otherSettingsGroup.gameObject);
            LeanTween.cancel(settingsRoot.gameObject);
            if (logo != null) LeanTween.cancel(logo.gameObject);
            if (profilePickCanvasGroup != null) LeanTween.cancel(profilePickCanvasGroup.gameObject);
        }

        protected override bool ProcessBackPress()
        {
            if (onRegister) return true;
            return base.ProcessBackPress();
        }

        protected override bool CloseRequest()
        {
            bool processed = base.CloseRequest();
            if (processed) nameInput.DeactivateInputField();
            return processed;
        }

        private void ShowIconPicker()
        {
#if !BBGC_LANDSCAPE
            otherSettingsGroup.gameObject.SetActive(true);
            LeanTween.alphaCanvas(otherSettingsGroup, 1f, transitionStep);
#endif
            var pos = profilePickContainer.anchoredPosition;
            pos.y = fullSettingsOffset;
            LeanTween.moveAnchored(profilePickContainer, pos, transitionStep).setEaseInOutQuad();
        }

        private void OnNameChanged(string text)
        {
            int alertLength = ValidateHelper.nicknameMaxLength - ValidateHelper.nicknameMaxLengthAlert;
            alertLength = Mathf.Max(0, alertLength);

            if (text.Length < alertLength)
            {
                nameError.IsError = false;
            }
            else
            {
                nameError.ErrorMessage = string.Format("{0}/{1}", text.Length, ValidateHelper.nicknameMaxLength);
                nameError.IsError = true;
            }
        }

        private void ValidateInput(string message)
        {
            string error = ValidateHelper.GetPlayerNameValidationError(message);
            if (!string.IsNullOrEmpty(error))
            {
                nameError.ErrorMessage = ClansLocalization.Get(error);
                nameError.IsError = true;
                SetSaveButtonActive(false);
                return;
            }

            nameError.IsError = false;
#if !BBGC_LANDSCAPE
            if (!otherSettingsGroup.gameObject.activeSelf) ShowIconPicker();
#else
            ShowIconPicker();
#endif
            SetSaveButtonActive(true);
        }

        private void SaveRequest()
        {
            onRegister = true;
            loader.Show();
            closeButton.interactable = false;
            nextButtonGroup.blocksRaycasts = false;

            LeanTween.cancel(nextButtonContainer.gameObject);
            var posEnd = nextButtonContainer.anchoredPosition;
            posEnd.y -= nextButtonContainer.rect.height;
            LeanTween.moveAnchored(nextButtonContainer, posEnd, transitionStep)
                .setEase(nextButtonEase);

#if BBGC_LANDSCAPE
            
            LeanTween.alphaCanvas(profilePickCanvasGroup, 0, transitionStep);
            LeanTween.alphaGraphic(logo, 0, transitionStep);

#endif

            Core.RegisterPlayer(nameInput.text, iconEdit.Icon, countrySelector.Selected,
                Controller.SwitchRegisterToClans, OnRegistrationFailed);
        }

        private void OnRegistrationFailed(long errorCode, string errorMessage)
        {
            Logger.Warning("PlayerRegisterClanPanel", string.Format("RegisterFailed: {0}; {1}", errorCode, errorMessage));
            onRegister = false;
            loader.Hide();
            closeButton.interactable = true;
            nextButtonGroup.blocksRaycasts = true;

            var posEnd = new Vector2(nextButtonContainer.anchoredPosition.x, 0);
            var posStart = posEnd;
            LeanTween.cancel(nextButtonContainer.gameObject);
            posStart.y = -nextButtonContainer.rect.height;
            nextButtonContainer.anchoredPosition = posStart;
            LeanTween.moveAnchored(nextButtonContainer, posEnd, transitionStep)
                .setDelay(transitionStep)
                .setEase(nextButtonEase)
                .setOnComplete(Showed);

#if BBGC_LANDSCAPE
            
            LeanTween.alphaCanvas(profilePickCanvasGroup, 1, transitionStep);
            LeanTween.alphaGraphic(logo, 1, transitionStep);

#endif

            notifications.ShowNotification(new Notification(NotificationType.Warning,
                ClansLocalization.Get(LocalizationKeys.notificationErrorCreatePlayer)));
        }

        private void SetSaveButtonActive(bool active)
        {
            saveButton.gameObject.SetActive(active);
            saveButtonInactive.SetActive(!active);
        }

        private void OnHeightRatioChanged(float heightRatio)
        {
            var pos = settingsRoot.anchoredPosition;
            pos.y = settingsRoot.rect.height * heightRatio;
            if (!otherSettingsGroup.gameObject.activeSelf) pos.y -= nickSettingKeyboardOffset;
            else pos.y -= fullSettingsKeyboardOffset;
            pos.y = Mathf.Max(0f, pos.y);

            LeanTween.cancel(settingsRoot.gameObject);
            LeanTween.moveAnchored(settingsRoot, pos, transitionStep).setEaseInOutQuad();
        }

    }

}