using System.Collections.Generic;
using BlackBears.Clans.Controller.Fragments;
using UnityEngine;

namespace BlackBears.Clans.Controller.Panels.Fragments
{

    internal class FragmentsPanel : ClanPanel
    {
        [SerializeField] private FragmentsController fragmentsController;

        public void ShowFragment(FragmentKind kind)
        {
            fragmentsController.ShowFragment(kind);
        }

        public void ShowCustomFragment(string fragmentKey, Dictionary<string, object> data)
        {
            fragmentsController.ShowCustomFragment(fragmentKey, data);
        }

        protected override void InjectFinished()
        {
            base.InjectFinished();
            fragmentsController.Inject(Core, Settings, null);
            fragmentsController.OnClosed.AddListener(Hided);
        }
    }

}