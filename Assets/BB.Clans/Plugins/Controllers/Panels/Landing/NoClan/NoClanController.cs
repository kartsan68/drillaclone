﻿using System;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.LandingPanel;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Panels.Landing.NoClan
{

    internal class NoClanController : LandingBaseController
    {

        [SerializeField] private GameObject newbiePanel;
        [SerializeField] private GameObject kickedPanel;
        [SerializeField] private GameObject disbandedPanel;
        [SerializeField] private GameObject clanRequestPanel;

        [SerializeField] private NoClanViewsContainer kickedViews;
        [SerializeField] private NoClanViewsContainer clanDisbandedViews;
        [SerializeField] private RequestClanViewsContainer requestClanViews;

        [SerializeField] private GameObject searchButtonsContainer;

        [Header("Buttons")]
        [SerializeField] private Button createClanButton;
        [SerializeField] private Button findClanButton;
        [SerializeField] private Button cancelClanRequestButton;

        private ClanCore core;
        private LandingClanPanel panel;
        private FragmentsController fragmentsController;
        private PredefinedIconsContainer clanIcons;
        private bool disposed;

        internal void Inject(ClanCore core, ClansConfiguration configuration,
            LandingClanPanel panel, FragmentsController fragmentsController)
        {
            this.core = core;
            this.panel = panel;
            this.fragmentsController = fragmentsController;
            this.clanIcons = configuration.ClanIcons;
            requestClanViews.clanIcon.Inject(core, clanIcons);
        }

        private void Start()
        {
            createClanButton.onClick.AddListener(CreateClanRequest);
            findClanButton.onClick.AddListener(FindClanRequest);
            cancelClanRequestButton.onClick.AddListener(CancelClanRequest);
            requestClanViews.pullToRefresh.OnRefreshStarted.AddListener(RefreshClanInfoRequest);
        }

        internal void Show()
        {
            SwitchStatusPanels(core.State.UserStatus);
            RunShowTransition();
        }

        internal void Hide()
        {
            RunHideTransition();
        }

        private void SwitchStatusPanels(UserStatus status)
        {
            newbiePanel.SetActive(status == UserStatus.NoClan || status == UserStatus.Left);
            kickedPanel.SetActive(status == UserStatus.Kicked);
            disbandedPanel.SetActive(status == UserStatus.ClanDisbanded);

            NoClanViewsContainer views;
            switch(status)
            {
                case UserStatus.Kicked: views = kickedViews; break;
                case UserStatus.ClanDisbanded: views = clanDisbandedViews; break;
                default: views = null; break;
            }

            if (views != null) SetDataToNoClanViews(views);

            var inClan = status.InClan();
            if (inClan) SetDataToRequestViews();
            clanRequestPanel.SetActive(inClan);
            searchButtonsContainer.SetActive(!inClan);
        }

        private void SetDataToNoClanViews(NoClanViewsContainer views)
        {
            var state = core.State;
            var player = state.ProfileCache.LocalPlayer;

            if (views.playerName != null) views.playerName.text = player.Nickname;
            if (views.clanName != null) views.clanName.text = state.ClanTitle;
            views.clanIcon.SetIcon(state.ClanIcon, clanIcons);

            if (views.kickerName)
            {
                views.kickerName.text = ValidateHelper.PlayerIdToTempNick(state.KickerId);
                state.ProfileCache.GetPlayerAsync(state.KickerId, kicker => {
                    if (Disposed || string.IsNullOrEmpty(kicker.Nickname)) return;
                    views.kickerName.text = kicker.Nickname;
                }, (c, m) => {
                    Debug.LogFormat("SetDataToNoClanViews: {0}; {1}", c, m);
                });
            }
        }

        private void SetDataToRequestViews()
        {
            requestClanViews.clanName.text = core.State.Clan.Title;
        }

        private void CreateClanRequest()
        {
            if (!core.State.UserStatus.InClan()) fragmentsController.ShowClanEdit();
        }

        private void FindClanRequest()
        {
            fragmentsController.ShowClanTop();
        }

        private void CancelClanRequest()
        {
            panel.CancelClanRequest(Show);
        }

        private void RefreshClanInfoRequest() 
        { 
            panel.ReloadClanInfo(OnRefreshSuccess, OnRefreshFailed);
        }

        private void OnRefreshSuccess()
        {
            if (requestClanViews.pullToRefresh.OnRefresh) requestClanViews.pullToRefresh.FinishRefresh();
        }

        private void OnRefreshFailed(long errorCode, string errorMessage)
        {
            Debug.LogFormat("OnRefreshFailed: {0}; {1}.", errorCode, errorMessage);
            if (requestClanViews.pullToRefresh.OnRefresh) requestClanViews.pullToRefresh.FinishRefresh();
        }

    }

}