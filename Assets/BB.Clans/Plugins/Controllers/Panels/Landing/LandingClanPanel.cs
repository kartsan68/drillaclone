﻿using System;
using System.Collections;
using System.Collections.Generic;
using BB.GameCore.Plugins;
using BlackBears.Clans.Controller.Cards;
using BlackBears.Clans.Controller.Chat;
using BlackBears.Clans.Controller.ContextMenu;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Controller.Panels.Landing.NoClan;
using BlackBears.Clans.Model;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    internal class LandingClanPanel : ClanPanel
    {

        [Header("Параметры лэндинга")]
        [SerializeField] private RectTransform contentRectTransform;
        [SerializeField] private ContextController contextController;
        [SerializeField] private FragmentsController fragmentsController;

        [SerializeField] private PlayerInfoController playerInfo;
        [SerializeField] private NoClanController noClan;
        [SerializeField] private FeedController feed;
        [SerializeField] private ErrorController error;
        [SerializeField] private LoaderController loader;
        [SerializeField] private ClanInfoPlate clanPlate;
        [SerializeField] private RectTransform keyboardOffsetRect;

        [Header("Portrait")]
        [SerializeField] private CardsListController cards;
        [SerializeField] private HeaderController header;
        [SerializeField] private ClanRequestController clanRequests;

        [Header("Кнопки")]
        [SerializeField] private Button profileButton;
        [SerializeField] private Button clanButton;
        [SerializeField] private Button clanJoinRequestsButton;

        [SerializeField] private Button ourGamesButton;
        [SerializeField] private Button supportButton;
        [SerializeField] private Button clansButton;
        [SerializeField] private Button[] closeButtons;

        [Header("Landscape")]
        [SerializeField] private Button[] clanButtons;
        [SerializeField] private RectTransform gameSectionRoot;

        private NotificationsFeature notifications;
        private GameSection gameSection;

        private int previousFragmentsCount = 0;

        private void Start()
        {
            fragmentsController.OnFragmentsCountChanged.AddListener(count =>
            {
                if (previousFragmentsCount > 0 && count == 0) OnFragmentsClosed(); //ShowClanActualState();
                else if (previousFragmentsCount == 0 && count > 0) OnFragmentsOpened();
                previousFragmentsCount = count;
            });

            for (int i = 0; i < closeButtons.Length; i++)
                closeButtons[i].onClick.AddListener(() => CloseRequest());

            profileButton.onClick.AddListener(OpenProfileRequest);
            clanButton.onClick.AddListener(OpenClanRequest);

            clanJoinRequestsButton.onClick.AddListener(OnClanJoinRequestButtonPressed);

#if BBGC_LANDSCAPE
            foreach (var item in clanButtons)
            {
                item.onClick.AddListener(OpenClanRequest);
            }
            ourGamesButton.onClick.AddListener(OnOurGamesButtonPressed);
            supportButton.onClick.AddListener(OnSupportButtonPressed);
            clansButton.onClick.AddListener(OnclansButtonPressed);
#endif

            KeyboardUtility.OnKeyboardHeightRatioChanged.AddListener(OnKeyboardRatioChanged);

            if (!Core.State.Initialized) ShowErrorNoInternet(ReinitializeRequest);
            else RevalidateClan();

            feed.OnOpenInvitesRequest.AddListener(fragmentsController.ShowInvites);

            if(Settings.configuration.LandigGameSection != null)
            {
                gameSection = Instantiate(Settings.configuration.LandigGameSection, Vector2.zero, Quaternion.identity, gameSectionRoot);
                gameSection.SetData(this);
            }
        }

        internal void CancelClanRequest(Block onSuccess)
        {
            loader.Show(this);
            Core.LeaveClan(() =>
            {
                loader.Hide(this);
                onSuccess.SafeInvoke();
            },
            (c, m) =>
            {
                if (!Core.State.Validated)
                {
                    RevalidateClan();
                }
                else
                {
                    loader.Hide(this);
                    notifications.ShowNotification(new Notification(NotificationType.Warning,
                        ClansLocalization.Get(LocalizationKeys.notificationErrorClanExitFailed)));

                    Debug.LogFormat("CancelClanRequest: {0}; {1}", c, m);
                }
            });
        }

        internal void ReloadClanInfo(Block onSuccess, FailBlock onFail)
        {
            RevalidateClan(false, onSuccess, onFail);
        }

        internal void CheckStateForValidity()
        {
            if (Core.State.Validated) return;
            RevalidateClan();
        }

        protected override void InjectFinished()
        {
            base.InjectFinished();

            contextController.Inject(Core.State.ProfileCache, Configuration.ProfileIcons);
            fragmentsController.Inject(Core, Settings, contextController);
#if !BBGC_LANDSCAPE
            header.Inject(Core, Settings, fragmentsController, error);
            cards.Inject(Core, Configuration, fragmentsController);
#endif
            noClan.Inject(Core, Configuration, this, fragmentsController);
            feed.Inject(Core, Settings, this, contextController, fragmentsController, loader);
            clanPlate.Inject(Core, Configuration.ClanIcons, Settings.scoreIconPack);


            playerInfo.Inject(Core.State.ProfileCache.LocalPlayer, Configuration.ProfileIcons, Settings.subsSprites);
        }

        private void RevalidateClan(bool showLoader = true, Block onSuccess = null, FailBlock onFail = null)
        {
            error.Hide();
            if (showLoader) loader.Show(this);
            Core.Validate(() =>
            {
                loader.Hide(this);
                ShowClanActualState();
                onSuccess.SafeInvoke();
            }, (c, m) =>
            {
                loader.Hide(this);
                onFail.SafeInvoke();
                ShowErrorNoInternet(() => RevalidateClan(showLoader, onSuccess, onFail));
            });
        }

        private void ReinitializeRequest()
        {
            error.Hide();
            loader.Show(this);
            Core.FullStateInitialize(false, () =>
            {
                loader.Hide(this);
                ShowClanActualState();
            }, (c, m) =>
            {
                Debug.LogFormat("{0}; {1}", c, m);
                loader.Hide(this);
                ShowErrorNoInternet(ReinitializeRequest);
            });
        }

        private void OpenProfileRequest()
        {
            fragmentsController.ShowPlayerInfo(Core.State.ProfileCache.LocalPlayer.Id, Core.State.User, Core.State.Clan);
        }

        private void OpenClanRequest()
        {
            fragmentsController.ShowClanInfo(Core.State.Clan);
        }

        private void ShowClanActualState()
        {
#if !BBGC_LANDSCAPE
            cards.UpdateCards();
#endif
            if(gameSection != null) gameSection.UpdateInfo();

            playerInfo.UpdateView();
            bool inClan = Core.State.UserStatus.InClan() && Core.State.User.Role.IsClanMember();
            clanPlate.Validate();

            if (inClan) ShowChat();
            else ShowNotInClan();
        }

        private void ShowChat()
        {
            feed.Show();
            noClan.Hide();
        }

        private void ShowNotInClan()
        {
            feed.Hide();
            feed.Clear();
            noClan.Show();
        }

        private void ShowErrorNoInternet(Block callback)
        {
            error.Show(ClansLocalization.Get(LocalizationKeys.errorCommonDescription), callback);
        }

        private void OnFragmentsOpened()
        {
#if !BBGC_LANDSCAPE
            header.OnFragmentsOpened();
#endif
        }

        private void OnFragmentsClosed()
        {
#if !BBGC_LANDSCAPE
            header.OnFragmentsClosed();
#endif
            ShowClanActualState();
        }

        private void OnClanJoinRequestButtonPressed()
        {
#if !BBGC_LANDSCAPE
            if (clanRequests.CanShowRequests) fragmentsController.ShowClanJoinRequests();
            else OpenClanRequest();
#endif
        }

        private void OnOurGamesButtonPressed()
        {
            NativeUtils.OpenStore(BBGC.Features.CoreDefaults().DevStoreId);
        }

        private void OnSupportButtonPressed()
        {
            fragmentsController.ShowSupport();
        }

        private void OnclansButtonPressed()
        {
            fragmentsController.ShowClanTop();
        }

        private void OnKeyboardRatioChanged(float heightRatio)
        {

            var pos = contentRectTransform.anchoredPosition;
            if (fragmentsController.FragmentsCount == 0)
                pos.y = Mathf.Max(0f, contentRectTransform.rect.height * heightRatio - keyboardOffsetRect.rect.height);
            else
                pos.y = 0;

            LeanTween.cancel(contentRectTransform.gameObject);
            LeanTween.moveAnchored(contentRectTransform, pos, transitionStep).setEaseInOutQuad();
        }

    }

}