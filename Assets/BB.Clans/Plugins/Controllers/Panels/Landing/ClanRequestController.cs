﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    public class ClanRequestController : MonoBehaviour
    {
        
        [SerializeField] private RectTransform clanInfoRect;
        [SerializeField] private Image backgroundImage;
        [SerializeField] private Image animationImage;
        [SerializeField] private Text requestCountText;
        [SerializeField] private int buttonHorizontalShift;
        [SerializeField] private Color noRequestsColor;
        [SerializeField] private Color hasRequestsColor;

        private bool moved;
        private bool canShowRequests;
        private int requestsCount;

        internal bool CanShowRequests { get { return canShowRequests && requestsCount > 0; } }

        private void Awake()
        {
            moved = false;
            canShowRequests = false;
            requestsCount = 0;
            backgroundImage.color = noRequestsColor;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(clanInfoRect.gameObject);
            LeanTween.cancel(gameObject);
        }

        internal void SetRequests(int requestsCount, bool canShow)
        {
            if(requestsCount < 0) requestsCount = 0;
            if(canShowRequests == canShow && this.requestsCount == requestsCount) return;

            this.requestsCount = requestsCount;
            canShowRequests = canShow;

            LeanTween.delayedCall(gameObject, 0.4f, UpdateView);
        }

        private void UpdateView()
        {
            if(!canShowRequests) return;

            if(!moved && requestsCount > 0)
            {
                var position = clanInfoRect.anchoredPosition;
                position.x -= buttonHorizontalShift; 
                LeanTween.cancel(clanInfoRect);
                LeanTween.moveAnchored(clanInfoRect, position, 0.2f).setEaseInOutQuad();

                backgroundImage.color = hasRequestsColor;
                requestCountText.text = requestsCount.ToString();
                animationImage.gameObject.SetActive(true);

                moved = true;
            }
            else if(moved && requestsCount <= 0)
            {
                var position = clanInfoRect.anchoredPosition;
                position.x += buttonHorizontalShift; 
                LeanTween.cancel(clanInfoRect);
                LeanTween.moveAnchored(clanInfoRect, position, 0.2f).setEaseInOutQuad();

                backgroundImage.color = noRequestsColor;
                requestCountText.text = string.Empty;
                animationImage.gameObject.SetActive(false);

                moved = false;
            }

            requestCountText.gameObject.SetActive(moved);
        }

    }

}