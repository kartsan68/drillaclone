﻿using System.Collections.Generic;
using BlackBears.Clans.Controller.Common;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.View.Common;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Views;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    internal class ClanInfoPlate : MonoBehaviour
    {

        [SerializeField] private Text nameText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text placeText;
        [SerializeField] private Image placeChangeIcon;
        [SerializeField] private Text playerPlaceText;
        [SerializeField] private PlayerClanIconController iconController;
        [SerializeField] private SpritePackView scoreImage;

        [Header("Portrait")]
        [SerializeField] private ClanRequestController clanRequestController;

        [Header("Landscape")]
        [SerializeField] private GameObject clanRequestGameObject;
        [SerializeField] private Text requestCountText;
        [SerializeField] private List<GameObject> noClanObjects;
        [SerializeField] private List<GameObject> clanObjects;


        private ClanCore core;
        private Vector2 startPosition;
        private bool showed;

        private bool Disposed;

        internal void Inject(ClanCore core, PredefinedIconsContainer clanIcons,
            SpritePack scoreIcon)
        {
            this.core = core;
            iconController.Inject(core, clanIcons);
            scoreImage.SetSpritePack(scoreIcon);
        }

        private void Awake()
        {
#if BBGC_LANDSCAPE
            foreach (var item in noClanObjects)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in clanObjects)
            {
                item.gameObject.SetActive(false);
            }
#else
            gameObject.SetActive(false);
#endif
        }

        private void OnDestroy()
        {
            Disposed = true;
        }

        internal void Validate()
        {
            if (Disposed) return;

            var status = core.State.UserStatus;
            var user = core.State.User;
            if (status.InClan() && user != null && user.Role.IsClanMember())
            {
#if BBGC_LANDSCAPE
                foreach (var item in noClanObjects)
                {
                    item.gameObject.SetActive(false);
                }

                foreach (var item in clanObjects)
                {
                    item.gameObject.SetActive(true);
                }
#else
            gameObject.SetActive(true);
#endif
                UpdateViews(core.State.Clan, core.State.User);
            }
            else
            {
#if BBGC_LANDSCAPE
                foreach (var item in noClanObjects)
                {
                    item.gameObject.SetActive(true);
                }

                foreach (var item in clanObjects)
                {
                    item.gameObject.SetActive(false);
                }
#else
            gameObject.SetActive(false);
#endif
            }
        }

        private void UpdateViews(Clan clan, ClanUser user)
        {
            if (Disposed) return;

            nameText.text = clan.Title;
            scoreText.text = clan.Score.ToString();
            placeText.text = ValidateHelper.PlaceToString(clan.WorldPlace);
            placeChangeIcon.gameObject.SetActive(clan.IsRideHigherInTop);

            int playerPlace = clan.GetUserPlace(user.Id);
            playerPlaceText.text = string.Format(ClansLocalization.Get(LocalizationKeys.playerLocalFromTotalPlace),
                playerPlace, clan.Amount);

#if BBGC_LANDSCAPE
            if (user.Role.IsModerator() && clan.JoinRequests > 0)
            {
                clanRequestGameObject.gameObject.SetActive(true);
                requestCountText.text = clan.JoinRequests.ToString();
            }
            else
            {
                clanRequestGameObject.gameObject.SetActive(false);
            }
#else
            clanRequestController.SetRequests(clan.JoinRequests, user.Role.IsModerator());
#endif
        }

    }

}