using UnityEngine;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    [RequireComponent(typeof(RectTransform))]
    internal class ClanPlateDataFoldController : MonoBehaviour
    {

        private RectTransform rectTransform;
        private Vector2 startPos;

        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            startPos = rectTransform.anchoredPosition;            
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void MoveToOrigin(float origin)
        {
            Vector2 endPos = startPos;
            endPos.x += rectTransform.rect.width;
            rectTransform.anchoredPosition = Vector2.Lerp(startPos, endPos, origin);
        }

        internal void ResetToStart()
        {
            LeanTween.cancel(gameObject);
            LeanTween.moveAnchored(rectTransform, startPos, 0.2f);
        }

    }

}