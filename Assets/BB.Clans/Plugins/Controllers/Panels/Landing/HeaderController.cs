﻿using System;
using BlackBears.Clans.Controller.Chat;
using BlackBears.Clans.Controller.Fragments;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    [RequireComponent(typeof(RectTransform))]
    internal class HeaderController : MonoBehaviour
    {

        [SerializeField] private FeedController feed;
        [SerializeField] private ClanPlateDataFoldController foldController;
        [SerializeField] private EventCardsContainer eventCardsContainer;

        [SerializeField] private RectTransform headerMenu;
        [SerializeField] private float additionalHeight = 10f;
        [SerializeField] private RectTransform foldRect;
        [SerializeField] Image shadow;
        [SerializeField] private float moveOrigin = 2f;
        [SerializeField] private float tweenTime = 0.2f;

        private RectTransform rectTransform;
        private bool isShowed = true;

        internal void Inject(ClanCore core, ClanFullSettings settings,
            FragmentsController fragmentsController, ErrorController errorController)
        {
            eventCardsContainer.Inject(core, settings, fragmentsController, errorController);
        }

        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();

            feed.OnFeedShowStateChanged.AddListener(OnFeedStateChanged);
            feed.OnFeedScrolled.AddListener(OnFeedScrolled);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void OnFragmentsOpened() => eventCardsContainer.BlockErrors();
        internal void OnFragmentsClosed() => eventCardsContainer.UnblockErrors();

        private void Show()
        {
            if (isShowed) return;
            isShowed = true;
            LeanTween.cancel(gameObject);

            var to = rectTransform.anchoredPosition;
            to.y = 0;
            LeanTween.moveAnchored(rectTransform, to, tweenTime).setEaseOutQuad();
            foldController.ResetToStart();
        }

        private void OnFeedStateChanged(bool isFeedShowed)
        {
            if (isFeedShowed) return;
            Show();
        }

        private void OnFeedScrolled(float value)
        {
            if (!feed.IsFeedShowed) return;
            float height = headerMenu.rect.height;// + additionalHeight;
            float maxOffset = height + additionalHeight;
            float y = Mathf.Max(0, Mathf.Min(maxOffset, -value * moveOrigin));
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, y);

            float foldY = Mathf.Clamp(height - y, -additionalHeight, 0);
            foldRect.anchoredPosition = new Vector2(foldRect.anchoredPosition.x, foldY);

            float clanDataOrigin = Mathf.Max(0, y - 0.5f * maxOffset) / maxOffset * 2;
            foldController.MoveToOrigin(clanDataOrigin);
            var shadowColor = shadow.color;
            float shadowOrigin = Mathf.Max(0, y - 0.8f * maxOffset) / maxOffset * 5f;
            shadowColor.a = Mathf.Lerp(1, 0, shadowOrigin);
            shadow.color = shadowColor;
        }

    }

}