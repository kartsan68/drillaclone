﻿using UnityEngine;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    [RequireComponent(typeof(RectTransform))]
    public class JoinRequestsGlareController : MonoBehaviour
    {

        [SerializeField] private float period = 2f;
        [SerializeField] private float animateTime = 0.3f;
        [SerializeField] private Vector3 from = new Vector3(0, -55);
        [SerializeField] private Vector3 to = new Vector3(0, 55);

        private RectTransform rectTransform;

        private float timer;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        private void Start()
        {
            timer = period;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        void Update()
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = period;
                LeanTween.moveFromToAnchored(rectTransform, from, to, animateTime);
            }
        }
    }

}