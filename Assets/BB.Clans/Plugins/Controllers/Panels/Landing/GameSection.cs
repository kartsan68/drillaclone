using UnityEngine;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    internal abstract class GameSection : MonoBehaviour
    {
        internal abstract void SetData(LandingClanPanel landingClanPanel);
        internal abstract void UpdateInfo();
    }

}