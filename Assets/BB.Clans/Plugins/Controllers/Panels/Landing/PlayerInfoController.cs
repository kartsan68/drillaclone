﻿using System;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore.Configurations;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    internal class PlayerInfoController : MonoBehaviour
    {

        [SerializeField] private Text nickname;
        [SerializeField] private PredefinedIconView profileIcon;
        [SerializeField] private Image subscriptionImage;

        private LocalPlayerProfile profile;
        private PredefinedIconsContainer icons;
        private SubscriptionSpritesContainer subsIcons;

        private bool destroyed;

        internal void Inject(LocalPlayerProfile profile, PredefinedIconsContainer profileIcons,
            SubscriptionSpritesContainer subsIcons)
        {
            this.profile = profile;
            this.icons = profileIcons;
            this.subsIcons = subsIcons;
        }

        private void Start()
        {
            UpdateView();
        }

        private void OnDestroy()
        {
            destroyed = true;
        }

        internal void UpdateView()
        {
            if(profile == null || subsIcons == null || destroyed) return;

            nickname.text = profile.Nickname;
            profileIcon.SetIcon(profile.Icon, icons);
            var subPack = subsIcons[profile.SubscriptionId];
            subscriptionImage.sprite = subPack?.SimpleSprite;
            subscriptionImage.gameObject.SetActive(subscriptionImage.sprite != null);
        }
        
    }

}