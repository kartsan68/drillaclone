using UnityEngine;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    [RequireComponent(typeof(CanvasGroup))]
    internal abstract class LandingBaseController : MonoBehaviour
    {

        private CanvasGroup canvasGroup;
        private bool showed;

        protected CanvasGroup CanvasGroup
        {
            get { return canvasGroup ?? (canvasGroup = GetComponent<CanvasGroup>()); }
        }

        protected bool Disposed { get; private set; }

        protected virtual void OnDestroy()
        {
            LeanTween.cancel(gameObject);
            Disposed = true;
        }

        protected virtual void RunShowTransition()
        {
            if (Disposed) return;

            if (showed) return;
            showed = true;
            gameObject.SetActive(true);
            LeanTween.cancel(gameObject);
            LeanTween.alphaCanvas(CanvasGroup, 1f, 0.3f);
        }

        protected virtual void RunHideTransition()
        {
            if (Disposed) return;

            if (!showed) return;
            showed = false;
            LeanTween.cancel(gameObject);
            LeanTween.alphaCanvas(CanvasGroup, 0f, 0.3f)
                .setOnComplete(() => gameObject.SetActive(false));
        }

    }

}