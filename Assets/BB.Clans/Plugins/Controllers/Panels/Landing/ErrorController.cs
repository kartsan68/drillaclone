using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Panels.Landing
{

    [RequireComponent(typeof(CanvasGroup))]
    internal sealed class ErrorController : LandingBaseController
    {

        [SerializeField] private Text errorDescription;
        [SerializeField] private Button repeatButton;
        [SerializeField] private Button closeButton;

        private Block repeatBlock;

        private void Awake()
        {
            CanvasGroup.alpha = 0f;
        }

        private void Start()
        {
            repeatButton.onClick.AddListener(Repeat);
        }

        protected override void OnDestroy()
        {
            repeatBlock = null;
            base.OnDestroy();
        }

        internal void Show(string message, Block repeat)
        {
            errorDescription.text = message;
            repeatBlock = repeat;
            RunShowTransition();
        }

        internal void Hide()
        {
            RunHideTransition();
        }

        protected override void RunShowTransition()
        {
            if (Disposed) return;

            repeatButton.gameObject.SetActive(repeatBlock != null);
            closeButton.interactable = true;
            repeatButton.interactable = true;

            base.RunShowTransition();
        }

        protected override void RunHideTransition()
        {
            if (Disposed) return;

            repeatBlock = null;
            base.RunHideTransition();
        }

        private void Repeat()
        {
            closeButton.interactable = false;
            repeatButton.interactable = false;
            repeatBlock.SafeInvoke();
        }

    }

}