using System.Collections.Generic;

namespace BlackBears.Clans.Controller.Panels.Landing
{
    
    internal sealed class LoaderController : LandingBaseController
    {

        private HashSet<object> requesters = new HashSet<object>();

        internal void Show(object requester)
        {
            if (requester == null) return;
            if (!requesters.Add(requester)) return;
            if (requesters.Count == 1) RunShowTransition();
        }

        internal void Hide(object requester)
        {
            if (!requesters.Remove(requester)) return;
            if (requesters.Count == 0) RunHideTransition();
        }

    }

}