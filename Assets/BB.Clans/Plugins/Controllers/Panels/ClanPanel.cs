﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.BackPress;
using UnityEngine;

namespace BlackBears.Clans.Controller.Panels
{

    internal class ClanPanel : MonoBehaviour, IBackPressListener
    {


		[Header("Базовые параметры панели")]
		[SerializeField] private CanvasGroup backgroundGroup;
		[SerializeField] private CanvasGroup contentGroup;
		[SerializeField] private RectTransform contentRoot;
		[SerializeField] private LeanTweenType contentEase;
		[SerializeField] protected float transitionStep = 0.2f;

		private bool processing = false;

		protected ClanCore Core { get; private set; }
		protected ClansController Controller { get; private set; }
		protected ClanFullSettings Settings { get; private set; }

		protected ClansConfiguration Configuration { get { return Settings.configuration; } }

        internal void Inject(ClanCore core, ClansController clansController, ClanFullSettings settings)
        {
			this.Core = core;
			this.Controller = clansController;
			this.Settings = settings;
			InjectFinished();
        }

		protected virtual void OnDestroy()
		{
			BBGC.Features.GetFeature<BackPressFeature>().RemoveListener(this);
			if(contentRoot != null) LeanTween.cancel(contentRoot.gameObject);
		}

        internal void Open() { Show(true); }
		internal void Close() { Hide(true); }

        bool IBackPressListener.OnBackPress() { return ProcessBackPress(); }

		protected virtual bool ProcessBackPress()
		{
			CloseRequest();
			return true;
		}

		protected virtual void InjectFinished() 
		{
			BBGC.Features.GetFeature<BackPressFeature>().AddListener(this);
		}

		protected virtual void Show(bool notify)
		{
			CancelTransition();
			processing = true;

			backgroundGroup.alpha = 0f;
			contentGroup.alpha = 0f;
			LeanTween.alphaCanvas(backgroundGroup, 1f, transitionStep);
			LeanTween.alphaCanvas(contentGroup, 1f, transitionStep);
			
			Vector2 pos = contentRoot.anchoredPosition;
			pos.y = -2 * contentRoot.rect.height;
			contentRoot.anchoredPosition = pos;
			var tween = LeanTween.moveAnchored(contentRoot, Vector2.zero, transitionStep)
				.setEase(contentEase);
			if (notify) tween.setOnComplete(Showed);
		}

        protected void Hide(bool notify)
		{
			CancelTransition();

			LeanTween.alphaCanvas(backgroundGroup, 0f, transitionStep);
			LeanTween.alphaCanvas(contentGroup, 0f, transitionStep);
			
			Vector2 pos = contentRoot.anchoredPosition;
			pos.y = -2 * contentRoot.rect.height;
			var tween = LeanTween.moveAnchored(contentRoot, pos, transitionStep);
			if (notify) tween.setOnComplete(Hided);
		}

		protected virtual void CancelTransition()
		{
			LeanTween.cancel(backgroundGroup.gameObject);
			LeanTween.cancel(contentGroup.gameObject);
		}

		protected virtual bool CloseRequest()
		{
			if (processing) return false;
			Hide(true);
			return true;
		}

		protected void Showed()
		{
			processing = false;
		}

		protected virtual void Hided()
		{
			if (Controller != null) Controller.PanelClosed(this);
			Destroy(this.gameObject);
		}

    }

}