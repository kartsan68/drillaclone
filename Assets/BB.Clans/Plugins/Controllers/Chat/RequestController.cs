﻿using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat
{

    internal class RequestController : MonoBehaviour
    {

        private const float updatePeriod = 1f;

        [SerializeField] private Button requestButton;
        [SerializeField] private Button takeButton;

        [SerializeField] private GameObject requestLocked;
        [SerializeField] private Text timerText;

        private float updateTimer;
        private bool resourceRequestButtonShow;

        private Warehouse warehouse;
        private ChronometerFeature chronometer;
        private ChatCommandHandler commandHandler;

        internal void Inject(Warehouse warehouse, ChronometerFeature chronometer,
            ChatCommandHandler commandHandler, bool resourceRequestButtonShow)
        {
            this.warehouse = warehouse;
            this.chronometer = chronometer;
            this.commandHandler = commandHandler;
            this.resourceRequestButtonShow = resourceRequestButtonShow;
        }

        private void Awake()
        {
            if (resourceRequestButtonShow)
            {
                requestButton.onClick.AddListener(OnRequestClick);
                takeButton.onClick.AddListener(OnTakeClick);
                UpdateView();

                this.warehouse.OnResourceRequested += UpdateView;
                this.warehouse.OnValidate += UpdateView;
                this.warehouse.OnIncomingDonationsEmpty += UpdateView;
            }
            else
            {
                requestButton.gameObject.SetActive(false);
                takeButton.gameObject.SetActive(false);
            }

        }

        private void OnDestroy()
        {
            this.warehouse.OnResourceRequested -= UpdateView;
            this.warehouse.OnValidate -= UpdateView;
            this.warehouse.OnIncomingDonationsEmpty -= UpdateView;
        }

        private void Update()
        {
            updateTimer -= Time.deltaTime;
            if (updateTimer <= 0f) UpdateByTime();
        }

        private void UpdateView()
        {
            if (warehouse.State.HasIncomingDonations)
            {
                takeButton.gameObject.SetActive(true);
                requestButton.gameObject.SetActive(false);
                requestLocked.SetActive(false);
            }
            else
            {
                takeButton.gameObject.SetActive(false);
                requestButton.gameObject.SetActive(warehouse.IsRequestAvailableByTime());
                requestLocked.SetActive(!requestButton.gameObject.activeSelf);
            }

            if (requestLocked.activeSelf) UpdateByTime();
        }

        private void UpdateByTime()
        {
            updateTimer = updatePeriod;

            if (requestLocked.activeSelf)
            {
                var actualTime = chronometer.ActualTime;
                var diff = warehouse.State.RequestBlockingTime - actualTime;
                if (diff < 0) UpdateView();
                else timerText.text = DateUtils.SecondsToHHMMSS(diff);
            }
            else if (takeButton.gameObject.activeSelf)
            {
                warehouse.ValidateIcncomingDonations();
                if (!warehouse.State.HasIncomingDonations) UpdateView();
            }
        }

        private void OnRequestClick() => commandHandler.OpenRequests();
        private void OnTakeClick() => commandHandler.OpenStorage();

    }

}