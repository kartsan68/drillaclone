﻿using BlackBears.Clans.Controller.Chat.Messages;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackBears.Clans.Controller.Chat
{

    [RequireComponent(typeof(Message))]
    internal class MessageWithContextMenu : MonoBehaviour, IPointerDownHandler, IPointerClickHandler
    {

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {}
        
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            var message = GetComponent<Message>();
            message.CommandHandler.TryShowContextMenu(message.FeedItem.playerId, eventData.position, eventData.enterEventCamera);
        }
		
    }

}