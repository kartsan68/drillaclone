﻿using System;
using System.Collections.Generic;
using BlackBears.Clans.Components;
using BlackBears.Clans.Controller.Chat.Messages;
using BlackBears.Clans.Controller.ContextMenu;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Controller.Panels.Landing;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.ViewModel.Chat;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat
{

    internal class FeedController : MonoBehaviour
    {

        private const int minReloadMessagesCount = 10;
        private const double nextUpdateInterval = 60.0;

        [SerializeField] private GameObject emptyFeedCap;
        [SerializeField] private GameObject feedContainer;
        [SerializeField] private ScrollRect emptyFeedScroll;
        [SerializeField] private ScrollRect feedScroll;
        [SerializeField] private PullToRefresh pullToRefresh;
        [SerializeField] private ChatingControl chatingControl;
        [SerializeField] private RequestController requestControl;
        [SerializeField] private RectTransform newMessagesDivider;
        [SerializeField] private Button scrollToBottomButton;
        [SerializeField] private PhotoController photoController;
        [SerializeField] private Button inviteButton;
        [SerializeField] private Text clanTagText;

        private ClanCore core;
        private ClanFullSettings settings;
        private LandingClanPanel landing;
        private LoaderController loader;
        private MessageStorage storage;
        private ChatCommandHandler commandHandler;
        private ScrollRect _activeScroll;

        private long localLastFeedItemId;
        private DateTime nextUpdateTime;
        private bool needToReload;
        private int page;
        private bool pagesEnded = false;
        private bool onPageLoading = false;
        private bool disposed = false;

        private NotificationsFeature notifications;
        private LinkedList<Message> showedMessages = new LinkedList<Message>();
        private List<FeedGroup> feedGroups = new List<FeedGroup>();

        private BoolUnityEvent onFeedShowStateChanged = new BoolUnityEvent();
        private FloatUnityEvent onFeedScrolled = new FloatUnityEvent();

        internal bool IsFeedShowed { get { return _activeScroll == feedScroll; } }
        internal ChatingControl ChatingControl { get { return chatingControl; } }

        internal BoolUnityEvent OnFeedShowStateChanged { get { return onFeedShowStateChanged; } }
        internal FloatUnityEvent OnFeedScrolled { get { return onFeedScrolled; } }
        internal UnityEvent OnOpenInvitesRequest { get { return inviteButton.onClick; } }

        private ScrollRect ActiveScroll
        {
            get { return _activeScroll; }
            set
            {
                if (_activeScroll == value) return;
                _activeScroll = value;
                onFeedShowStateChanged.Invoke(IsFeedShowed);
                pullToRefresh.Scroll = _activeScroll;
            }
        }

        private bool CanLoadNextPage { get { return !pagesEnded && !onPageLoading; } }

        internal void Inject(ClanCore core, ClanFullSettings settings, LandingClanPanel landing,
            ContextController context, FragmentsController fragments, LoaderController loader)
        {
            this.core = core;
            this.settings = settings;
            this.landing = landing;
            this.loader = loader;
            chatingControl.Inject(this);
            commandHandler = new ChatCommandHandler(core, this, loader, context, fragments, photoController);
            requestControl.Inject(core.State.Warehouse, BBGC.Features.GetFeature<ChronometerFeature>(), commandHandler, core.Params.resourceRequestButtonShow);
            storage = new MessageStorage(settings.configuration.Messages);
        }

        private void Awake()
        {
            needToReload = true;
            notifications = BBGC.Features.GetFeature<NotificationsFeature>();
            emptyFeedScroll.onValueChanged.AddListener(OnScroll);
            feedScroll.onValueChanged.AddListener(OnScroll);
            scrollToBottomButton.onClick.AddListener(ScrollFeedToBottom);
            pullToRefresh.OnRefreshStarted.AddListener(OnRefreshStarted);

            core.State.OnClanChanged += ClanChanged;
            core.State.OnUserPerformImportantAction += OnUserImportantAction;
            core.State.Warehouse.OnResourceRequested += OnResourceRequested;
            ClanChanged(core.State.Clan);
        }

        private void OnDestroy()
        {
            disposed = true;
            storage.ClearCache();
            LeanTween.cancel(feedScroll.content.gameObject);
            core.State.OnClanChanged -= ClanChanged;
            core.State.OnUserPerformImportantAction -= OnUserImportantAction;
            core.State.Warehouse.OnResourceRequested -= OnResourceRequested;
        }

        internal void Show()
        {
            if (disposed) return;
            if (gameObject.activeSelf) OnEnable();
            else gameObject.SetActive(true);
        }
        internal void Hide() { gameObject.SetActive(false); }

        internal void Clear()
        {
            if (disposed) return;

            foreach (var message in showedMessages) storage.ReturnMessage(message);
            showedMessages.Clear();
            storage.ClearCache();
        }

        internal void AnswerToPlayer(string playerId)
        {
            var profile = core.State.ProfileCache.GetCachedPlayer(playerId);
            chatingControl.AnswerToPlayer(playerId, profile);
        }

        internal void SendMessageRequest(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                chatingControl.MessageSendingFailed();
                return;
            }

            core.SendMessage(message, OnMessageSended, (c, m) =>
            {
                if (!core.State.Validated)
                {
                    landing.CheckStateForValidity();
                }
                else
                {
                    notifications.ShowNotification(new Notification(NotificationType.Warning,
                            ClansLocalization.Get(LocalizationKeys.notificationErrorSendMessage)));
                    Debug.LogFormat("Send Message Fail: {0}; {1}", c, m);
                    chatingControl.MessageSendingFailed();
                }
            });
        }

        internal void ReloadFeed(bool hide, bool updateLastFeedId, bool showLoader)
        {
            needToReload = false;

            if (updateLastFeedId) localLastFeedItemId = core.State.LastFeedItemId;
            emptyFeedCap.SetActive(hide ? false : emptyFeedCap.activeSelf);
            feedContainer.SetActive(hide ? false : feedContainer.activeSelf);
            LoadFeed(1, true, showLoader, true);
        }

        private void OnEnable()
        {
            var now = DateTime.UtcNow;
            if (!needToReload && now < nextUpdateTime) return;

            ReloadFeed(true, true, true);
        }

        private void LoadNextFeedPage()
        {
            LoadFeed(page + 1, false, false, false);
        }

        private void LoadFeed(int page, bool clearOld, bool showLoader, bool reloading)
        {
            if (showLoader) loader.Show(this);
            onPageLoading = true;
            core.GetTimeline(page, items => OnFeedLoaded(items, page, clearOld, reloading), OnLoadFeedFailed);
        }

        private void OnFeedLoaded(List<FeedItem> items, int page, bool clearOld, bool reloading)
        {
            nextUpdateTime = DateTime.UtcNow.AddSeconds(nextUpdateInterval);
            ValidateFeedProfiles(items, page, clearOld, reloading);
        }

        private void ValidateFeedProfiles(List<FeedItem> items, int page, bool clearOld, bool reloading)
        {
            if (disposed) return;

            HashSet<string> profiles = new HashSet<string>();
            foreach (var item in items)
            {
                if (!string.IsNullOrEmpty(item.playerId)) profiles.Add(item.playerId);
                if (!string.IsNullOrEmpty(item.info.moderatorId)) profiles.Add(item.info.moderatorId);
            }
            core.State.ProfileCache.LoadPlayersAsync(new List<string>(profiles), () => UpdateFeed(items, page, clearOld, reloading),
                (c, m) =>
                {
                    Debug.LogFormat("ValidateFeedProfiles: {0}; {1}", c, m);
                    if (disposed) return;
                    UpdateFeed(items, page, clearOld, reloading);
                });
        }

        //TODO: Разбить на методы
        private void UpdateFeed(List<FeedItem> items, int page, bool clearOld, bool reloading)
        {
            if (disposed) return;

            this.page = page;
            if (clearOld)
            {
                newMessagesDivider.gameObject.SetActive(false);
                foreach (var message in showedMessages) storage.ReturnMessage(message);
                showedMessages.Clear();
                feedGroups.Clear();
            }

            if (items.Count == 0)
            {
                if (page == 1)
                {
                    emptyFeedCap.SetActive(true);
                    feedContainer.SetActive(false);
                    ActiveScroll = emptyFeedScroll;
                }
                else if (reloading)
                {
                    emptyFeedCap.SetActive(false);
                    feedContainer.SetActive(true);
                    ActiveScroll = feedScroll;
                    storage.ClearCache();
                }

                pagesEnded = true;
            }
            else
            {
                int processedMessagesCount = 0;
                int throughFeedIterator = -1;
                int groupStartIndex = feedGroups.Count - 1;
                int feedStartIndex = 0;
                if (groupStartIndex >= 0) feedStartIndex = feedGroups[groupStartIndex].feedItems.Count;
                FeedGroup.UpdateGroups(items, feedGroups);
                if (groupStartIndex >= 0 && feedStartIndex >= 0)
                {
                    var group = feedGroups[groupStartIndex];
                    if (feedStartIndex + 1 < group.feedItems.Count && showedMessages.Count > 0)
                    {
                        var message = showedMessages.Last.Value;
                        message.SetData(message.FeedItem, false, message.IsPlayerMessage);
                    }
                }

                for (int groupIndex = Mathf.Max(groupStartIndex, 0); groupIndex < feedGroups.Count; groupIndex++)
                {
                    var groupItems = feedGroups[groupIndex].feedItems;
                    bool isPlayerGroup = false;

                    for (int feedIndex = feedStartIndex; feedIndex < groupItems.Count; feedIndex++)
                    {
                        throughFeedIterator += 1;
#if BBGC_LANDSCAPE
                        bool isGroupHead = true;
#else
                        bool isGroupHead = feedIndex == groupItems.Count - 1;
#endif

                        var item = groupItems[feedIndex];
                        if (feedIndex == feedStartIndex)
                        {
                            var pid = core.State.Game.PlayerId;
                            isPlayerGroup = string.Equals(item.playerId, pid);
                            isPlayerGroup = isPlayerGroup || string.Equals(item.info.moderatorId, pid);
                        }

                        if (item.id > localLastFeedItemId && isPlayerGroup)
                        {
                            localLastFeedItemId = item.id;
                        }

                        bool showDivider = item.id == localLastFeedItemId;
                        showDivider = showDivider && (page > 1 || throughFeedIterator > 0);
                        // showDivider = showDivider && !isPlayerGroup;
                        showDivider = showDivider && processedMessagesCount > 0;


                        if (showDivider)
                        {
                            newMessagesDivider.SetAsFirstSibling();
                            newMessagesDivider.gameObject.SetActive(true);
                        }

                        var message = storage.PrepareMessageForFeedItem(item);
                        if (message == null) continue;
                        message.Inject(commandHandler, core.State.ProfileCache, settings);
                        message.SetData(item, isGroupHead, isPlayerGroup);

                        showedMessages.AddLast(message);
                        message.RectTransform.SetParent(feedScroll.content, false);
                        message.RectTransform.SetAsFirstSibling();
                        message.gameObject.SetActive(true);
                        processedMessagesCount += 1;
                    }

                    feedStartIndex = 0;
                }

                if (reloading && showedMessages.Count < minReloadMessagesCount)
                {
                    LoadFeed(page + 1, false, true, true);
                    return;
                }

                emptyFeedCap.SetActive(false);
                feedContainer.SetActive(true);
                ActiveScroll = feedScroll;
                storage.ClearCache();
                pagesEnded = false;
            }

            loader.Hide(this);
            onPageLoading = false;
            if (pullToRefresh.OnRefresh) pullToRefresh.FinishRefresh();
        }

        private void ScrollFeedToBottom()
        {
            if (ActiveScroll == null) return;
            ActiveScroll.StopMovement();

            LeanTween.cancel(ActiveScroll.content);
            LeanTween.moveScrollNormalized(ActiveScroll, Vector2.zero, 0.3f).setEaseInOutQuad();
        }

        private void OnLoadFeedFailed(long errorCode, string errorMessage)
        {
            if (disposed) return;

            if (!core.State.Validated)
            {
                landing.CheckStateForValidity();
            }
            else
            {
                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorLoadFeed)));
                Debug.LogFormat("OnLoadFeedFailed: {0}; {1}", errorCode, errorMessage);
                onPageLoading = false;
            }
            loader.Hide(this);
            if (pullToRefresh.OnRefresh) pullToRefresh.FinishRefresh();
        }

        private void OnMessageSended(NetworkSuccessAnswer answer)
        {
            if (answer.success)
            {
                ReloadFeed(false, true, true);
                chatingControl.MessageSendingSuccess();
            }
            else
            {
                chatingControl.MessageSendingFailed();
                notifications.ShowNotification(new Notification(NotificationType.Warning,
                    ClansLocalization.Get(LocalizationKeys.notificationErrorSendMessage)));
            }
        }

        private void OnScroll(Vector2 position)
        {
            if (ActiveScroll == null) return;

            if (!pullToRefresh.OnRefresh)
            {
                if (CanLoadNextPage && position.y > 0.98) LoadNextFeedPage();
            }

            onFeedScrolled.Invoke(ActiveScroll.content.anchoredPosition.y);
        }

        private void OnRefreshStarted() { ReloadFeed(false, true, false); }

        private void ClanChanged(Clan clan)
        {
            clanTagText.text = clan != null ? ValidateHelper.FormatClanTag(clan.Tag) : null;
            needToReload = true;
        }

        private void OnUserImportantAction() => needToReload = true;

        private void OnResourceRequested()
        {
            needToReload = true;
            nextUpdateTime = DateTime.UtcNow;
        }

    }

}