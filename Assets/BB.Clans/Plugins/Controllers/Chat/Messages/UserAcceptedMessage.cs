using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class UserAcceptedMessage : AdminPlayerMessage
    {

        [SerializeField] private Text moderatorName;
        [SerializeField] private PredefinedIconView moderatorIcon;
        [SerializeField] private GameObject moderatorSubsContainer;
        [SerializeField] private Image moderatorSubsImage;

        [SerializeField] private Text userName;
        [SerializeField] private PredefinedIconView userIcon;
        [SerializeField] private GameObject userSubsContainer;
        [SerializeField] private Image userSubsImage;

        [Header("Landscape")]
        [SerializeField] private HorizontalLayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;

        internal override FeedItemType MessageType { get { return FeedItemType.UserAccepted; } }

        protected override void UpdateViews()
        {
            if (FeedItem == null) return;
            moderatorName.text = ValidateHelper.PlayerIdToTempNick(FeedItem.info.moderatorId);
            userName.text = ValidateHelper.PlayerIdToTempNick(FeedItem.playerId);
            moderatorIcon.Color = Color.clear;
            userIcon.Color = Color.clear;
            base.UpdateViews();

            GetProfile(FeedItem.info.moderatorId, OnModeratorLoaded);
            GetProfile(FeedItem.playerId, OnUserLoaded);

#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
            userSubsImage.rectTransform.sizeDelta = new Vector2(16, 16);
#endif
        }

        private void OnModeratorLoaded(IPlayerProfile profile)
        {
            OnProfileLoaded(profile, moderatorName, moderatorIcon, moderatorSubsContainer, moderatorSubsImage);
        }

        private void OnUserLoaded(IPlayerProfile profile)
        {
            OnProfileLoaded(profile, userName, userIcon, userSubsContainer, userSubsImage);
        }

        private void OnProfileLoaded(IPlayerProfile profile, Text text, PredefinedIconView iconView,
            GameObject subsContainer, Image subsImage)
        {
            if (Disposed || profile == null || string.IsNullOrEmpty(profile.Nickname)) return;
            text.text = profile.Nickname;
            iconView.Color = Color.white;
            SetIcon(iconView, profile.Icon);
            SetSubscription(subsContainer, subsImage, profile.SubscriptionId);
        }

    }

}