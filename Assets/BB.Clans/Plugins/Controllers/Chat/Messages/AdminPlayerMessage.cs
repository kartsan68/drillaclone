using BlackBears.Clans.Model.PlayerProfiles;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal abstract class AdminPlayerMessage : Message
    {
        
        [SerializeField] private Button playerProfileButton;
        [SerializeField] private Button adminProfileButton;

        protected override void Awake()
        {
            base.Awake();
            if (playerProfileButton != null) playerProfileButton.onClick.AddListener(OpenPlayerProfile);
            if (adminProfileButton != null) adminProfileButton.onClick.AddListener(OpenAdminProfile);
        }

        private void OpenPlayerProfile() { OpenProfile(FeedItem.playerId); }
        private void OpenAdminProfile() { OpenProfile(FeedItem.info.moderatorId); }

    }
    
}