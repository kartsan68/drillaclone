using System;
using System.Collections;
using BlackBears.Clans.Presentation.UI.DropItems;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.ResourceShare;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.DropItemSystem;
using BlackBears.GameCore.Graphic;
using BlackBears.Utils;
using BlackBears.Utils.Components;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class ResourceRequestMessage : Message
    {

        [SerializeField] private Text userName;
        [SerializeField] private PredefinedIconView userIconView;

        [SerializeField] private Image resourceImage;
        [SerializeField] private Text resourceNameText;
        [SerializeField] private Text resourceCountText;

        [SerializeField] private Slider slider;

        [SerializeField] private Text[] giveTexts;
        [SerializeField] private Button activeButton;
        [SerializeField] private GameObject inactiveButton;

        [Header("Контейнер данных")]
        [SerializeField] private LayoutElement dataContainerLayout;
        [SerializeField] private float minDataContainerHeight;
        [SerializeField] private float maxDataContainerHeight;

        [Header("Активная кнопка отправки ресурса")]
        [SerializeField] private Image giveButtonBg;
        [SerializeField] private Text giveButtonText;
        [SerializeField] private Color regularTextGiveButtonColor = ColorUtils.FromHex(0x1C1C3BFF);
        [SerializeField] private Color warningTextGiveButtonColor = ColorUtils.FromHex(0xFFFFFFFF);
        [SerializeField] private string regularBgName;
        [SerializeField] private string warningBgName;
        [SerializeField] private StateButtonLoaderAnimator buttonLoader;

        [Header("Крафт")]
        [SerializeField] private GameObject craftContainer;
        [SerializeField] private VerticalLayoutGroup craftContent;
        [SerializeField] private int craftContentRightOffset;
        [SerializeField] private Text craftTitleText;
        [SerializeField] private GameObject craftIconsContainer;
        [SerializeField] private DetailIconController[] detailImages;

        [Header("Landscape")]
        [SerializeField] private HorizontalLayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;

        private Sprite regularBgGiveButtonSprite;
        private Sprite warningBgGiveButtonSprite;

        internal override FeedItemType MessageType => FeedItemType.ResourceRequest;

        private void Start()
        {
            activeButton.onClick.AddListener(SendResource);
        }

        protected override void OnDestroy()
        {
            LeanTween.cancel(gameObject);
            LeanTween.cancel(activeButton.gameObject);
            LeanTween.cancel(inactiveButton);

            base.OnDestroy();
        }

        protected override void UpdateViews()
        {
            if (FeedItem == null) return;
            userName.text = ValidateHelper.PlayerIdToTempNick(FeedItem.playerId);
            userIconView.Color = Color.clear;

            var warehouse = CommandHandler.Warehouse;
            var resource = warehouse.GetResourceWithTypeById(FeedItem.info.resourceType, FeedItem.info.resourceId);

            if (resource == null)
            {
                resourceImage.enabled = false;
                resourceImage.sprite = null;
                resourceNameText.text = string.Empty;
            }
            else
            {
                resourceImage.enabled = true;
                resourceImage.sprite = resource.icon;
                resourceNameText.text = ClansLocalization.Get(resource.nameKey).ToUpper();
            }

            UpdateSlider();
            UpdateButtons();
            UpdateButtonsAmount();
            UpdateCraftContainer();

            GetProfile(FeedItem.playerId, OnUserLoaded);

#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
#endif
        }

        private void UpdateSlider()
        {
            if (Math.Abs(FeedItem.info.value) < 0.001) slider.value = 0f;
            else slider.value = (float)(FeedItem.info.CurrentValue / FeedItem.info.value);

            var curValStr = CommandHandler.AmountToPrettyString(FeedItem.info.CurrentValue);
            var valStr = CommandHandler.AmountToPrettyString(FeedItem.info.value);
            resourceCountText.text = $"{curValStr} / {valStr}";
        }

        private void UpdateButtons()
        {
            if (string.Equals(FeedItem.playerId, CommandHandler.CurrentPlayerId))
            {
                activeButton.gameObject.SetActive(false);
                inactiveButton.SetActive(false);

                dataContainerLayout.minHeight = minDataContainerHeight;
            }
            else
            {
                var buttonEnabled = CommandHandler.Warehouse.CanDonate(FeedItem.info.requestId);
                if (buttonEnabled && !CommandHandler.Warehouse.IsAvailableResource(FeedItem.info.resourceType, FeedItem.info.resourceId))
                {
                    buttonEnabled = false;
                }
                buttonEnabled &= FeedItem.info.CurrentValue < FeedItem.info.value;

                var activeCanvasGroup = activeButton.gameObject.GetComponent<CanvasGroup>();
                var inactiveCanvasGroup = inactiveButton.GetComponent<CanvasGroup>();
                var animate = !buttonEnabled && activeButton.gameObject.activeSelf;
                var hasCanvasGroup = activeCanvasGroup != null && inactiveCanvasGroup != null;

                if (animate && hasCanvasGroup && !Disposed)
                {
                    LeanTween.alphaCanvas(activeCanvasGroup, 0f, 0.2f).setEaseOutQuad().setOnComplete(() =>
                    {
                        activeCanvasGroup.gameObject.SetActive(false);
                        inactiveCanvasGroup.gameObject.SetActive(true);
                        inactiveCanvasGroup.alpha = 0;

                        LeanTween.alphaCanvas(inactiveCanvasGroup, 1f, 0.2f).setEaseInQuad();
                    });
                }
                else
                {
                    activeButton.gameObject.SetActive(buttonEnabled);
                    inactiveButton.SetActive(!buttonEnabled);
                }

                dataContainerLayout.minHeight = maxDataContainerHeight;
            }
        }

        private void UpdateButtonsAmount()
        {
            double amount = 0f;
            amount = FeedItem != null ? CommandHandler.Warehouse.CalculateGiveDonationAmount(FeedItem.info) : 0.0;

            var text = string.Format(ClansLocalization.Get(LocalizationKeys.resourceRequestSendButton),
                CommandHandler.AmountToPrettyString(amount));
            for (var i = 0; i < giveTexts.Length; i++)
            {
                giveTexts[i].text = text;
            }
        }

        private void UpdateCraftContainer()
        {
            var game = BBGC.Features.CoreDefaults().GameAdapter;
            game.GetInGameResourceDependInfo(FeedItem.info.resourceType, FeedItem.info.resourceId,
                detailImages.Length, GetInGameResourceDependInfoResponse);
        }

        private void GetInGameResourceDependInfoResponse(ResourceDependInfo dependInfo)
        {
            if (dependInfo == null || !dependInfo.HasText && !dependInfo.HasSprites)
            {
                UpdateGiveButtonColor(false);
                craftContainer.SetActive(false);
                return;
            }

            UpdateGiveButtonColor(true);
            craftContainer.SetActive(true);

            if (!dependInfo.HasSprites || detailImages == null || detailImages.Length == 0)
            {
                craftTitleText.text = dependInfo.text;
                craftContent.padding.right = craftContentRightOffset;
                craftIconsContainer.SetActive(false);
                return;
            }

            craftTitleText.text = ClansLocalization.Get(LocalizationKeys.resourceCraftTitle);
            craftContent.padding.right = 0;

            craftIconsContainer.SetActive(true);
            for (int i = 0; i < detailImages.Length; i++)
            {
                if (i < dependInfo.sprites.Count)
                {
                    detailImages[i].IconSprite = dependInfo.sprites[i];
                    detailImages[i].gameObject.SetActive(true);
                }
                else
                {
                    detailImages[i].gameObject.SetActive(false);
                }
            }
        }

        private void UpdateGiveButtonColor(bool warning)
        {
            if (warningBgGiveButtonSprite == null || regularBgGiveButtonSprite == null)
            {
                var atlas = UICache.Instance.TakeAtlas(CoreCache.key);
                if (atlas == null) return;

                warningBgGiveButtonSprite = atlas[warningBgName];
                regularBgGiveButtonSprite = atlas[regularBgName];
            }

            giveButtonBg.sprite = warning ? warningBgGiveButtonSprite : regularBgGiveButtonSprite;
            giveButtonText.color = warning ? warningTextGiveButtonColor : regularTextGiveButtonColor;
        }

        private void OnUserLoaded(IPlayerProfile profile)
        {
            if (Disposed || profile == null || string.IsNullOrEmpty(profile.Nickname)) return;
            userName.text = profile.Nickname;
            userIconView.Color = Color.white;
            SetIcon(userIconView, profile.Icon);
        }

        private void SendResource()
        {
            activeButton.interactable = false;
            buttonLoader.StartLoadingAnimation();

            CommandHandler.SendResource(FeedItem, transition =>
            {
                if (Disposed) return;
                FeedItem.info.ApplyTransition(transition);
                LeanTween.delayedCall(gameObject, 0.2f, () => OnResourceSent(true));
            }, (c, m) =>
            {
                Debug.Log($"SendResource: {c}; {m}");
                if (Disposed) return;
                LeanTween.delayedCall(gameObject, 0.2f, () => OnResourceSent(false));
            });

        }

        private void OnResourceSent(bool success)
        {
            buttonLoader.StopLoadingAnimation(success: success, behaviour: OnStoppedBehaviour.Reset, onStopped: () =>
            {
                if (success)
                {
                    OnSuccessResourceSent();
                    UpdateSlider();
                }
                UpdateButtons();

                var isRequestFulfilled = FeedItem.info.CurrentValue >= FeedItem.info.value;
                activeButton.interactable = !isRequestFulfilled;
            });
        }

        private void OnSuccessResourceSent()
        {
            var dropSystem = BBGC.Features.GetFeature<DropItemSystemFeature>()?.System;
            if (dropSystem != null) dropSystem.DropItem(new SoftCurrencyClanDropItemData(activeButton.transform.position));
        }

    }

}