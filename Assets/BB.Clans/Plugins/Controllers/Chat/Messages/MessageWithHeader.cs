using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal abstract class MessageWithHeader : AdminPlayerMessage
    {

        [SerializeField] private GameObject header;
        [SerializeField] private Text dateText;

        protected override void UpdateViews()
        {
            base.UpdateViews();
            
            if (FeedItem == null) return;
            if (header != null) header.SetActive(IsGroupHead);
            if (dateText != null) dateText.text = DateUtils.FromUnixTimestamp(FeedItem.date).ToMessageDate();
        }

    }

}