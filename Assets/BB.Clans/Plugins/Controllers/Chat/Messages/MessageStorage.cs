﻿using System.Collections.Generic;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class MessageStorage
    {

		private MessageBuilder builder;
		private Dictionary<FeedItemType, LinkedList<Message>> messageByType;

		internal MessageStorage(MessagePrefabsContainer prefabs)
		{
			builder = new MessageBuilder(prefabs);
			messageByType = new Dictionary<FeedItemType, LinkedList<Message>>();
		}

		internal Message PrepareMessageForFeedItem(FeedItem item)
		{
			if (item == null) return null;
			var message = GetMessageInstance(item);
			return message;
		}

		internal void ReturnMessage(Message message)
		{
			if (message == null) return;
			messageByType[message.MessageType].AddFirst(message);
			message.gameObject.SetActive(false);
		}

		internal void ClearCache()
		{
			foreach(var messages in messageByType.Values)
			{
				foreach(var message in messages)
				{
					UnityEngine.Object.Destroy(message.gameObject);
				}
				messages.Clear();
			}
		}

		private Message GetMessageInstance(FeedItem item)
		{
			LinkedList<Message> messages;
			if (!messageByType.TryGetValue(item.feedType, out messages))
			{
				messages = new LinkedList<Message>();
				messageByType[item.feedType] = messages;
			}

			Message message;
			if (messages.Count > 0)
			{
				message = messages.First.Value;
				messages.RemoveFirst();
			}
			else
			{
				message = builder.Build(item);
			}

			return message;
		}

    }

}