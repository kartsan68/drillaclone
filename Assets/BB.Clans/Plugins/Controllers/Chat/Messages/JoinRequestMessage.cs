using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class JoinRequestMessage : AdminPlayerMessage
    {

        [SerializeField] private Text playerName;
        [SerializeField] private PredefinedIconView playerIconView;
        [SerializeField] private GameObject subsContainer;
        [SerializeField] private Image subsImage;
        [SerializeField] private Button acceptButton;
        [SerializeField] private Button denyButton;
        [Header("Landscape")]
        [SerializeField] private HorizontalLayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;

        internal override FeedItemType MessageType { get { return FeedItemType.JoinRequest; } }

        private void Start()
        {
            acceptButton.onClick.AddListener(OnAcceptClick);
            denyButton.onClick.AddListener(OnDenyClick);
        }

        protected override void UpdateViews()
        {
            if (FeedItem == null) return;
            playerName.text = ValidateHelper.PlayerIdToTempNick(FeedItem.playerId);
            playerIconView.Color = Color.clear;
            GetProfile(FeedItem.playerId, OnProfileLoaded);

#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
            subsImage.rectTransform.sizeDelta = new Vector2(16, 16);
#endif

            base.UpdateViews();
        }

        protected override void UpdateInteractableViews()
        {
            acceptButton.interactable = Interactable;
            acceptButton.interactable = Interactable;
        }

        private void OnProfileLoaded(IPlayerProfile profile)
        {
            if (Disposed || profile == null || string.IsNullOrEmpty(profile.Nickname)) return;
            playerName.text = profile.Nickname;
            SetIcon(playerIconView, profile.Icon);
            SetSubscription(subsContainer, subsImage, profile.SubscriptionId);
        }

        private void OnAcceptClick() { CommandHandler.PlayerAccept(this); }
        private void OnDenyClick() { CommandHandler.PlayerDeny(this); }

    }

}