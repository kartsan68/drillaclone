﻿using BlackBears.Clans.Model.Timeline;
using UnityEngine;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class MessageBuilder
    {

		private MessagePrefabsContainer prefabs;

		internal MessageBuilder(MessagePrefabsContainer prefabs)
		{
			this.prefabs = prefabs;
		}

		internal Message Build(FeedItem feedItem)
		{
			if (feedItem == null) return null;
			switch(feedItem.feedType)
			{
				case FeedItemType.Message: return Object.Instantiate(prefabs.TextMessage);
				// case FeedItemType.JoinRequest: return Object.Instantiate(prefabs.JoinRequestMessage);
				case FeedItemType.JoinConfirmed: return Object.Instantiate(prefabs.JoinConfirmedMessage);
				case FeedItemType.UserLeft: return Object.Instantiate(prefabs.UserLeftMessage);
				case FeedItemType.UserKicked: return Object.Instantiate(prefabs.UserKickedMessage);
				case FeedItemType.UserAccepted: return Object.Instantiate(prefabs.UserAcceptedMessage);
				case FeedItemType.UserPromoted: return Object.Instantiate(prefabs.UserPromotedMessage);
				case FeedItemType.UserDemoted: return Object.Instantiate(prefabs.UserDemotedMessage);
				case FeedItemType.AdministrationMessage: return Object.Instantiate(prefabs.AdministrationMessage);
				case FeedItemType.NewLeader: return Object.Instantiate(prefabs.NewLeaderMessage);
				case FeedItemType.ResourceRequest: return Object.Instantiate(prefabs.ResourceRequestMessage);
				// case FeedItemType.RatingRise: return Object.Instantiate(prefabs.RatingRiseMessage);
				// case FeedItemType.PhotoMessage: return Object.Instantiate(prefabs.PhotoMessage);
				default: return null;
			}
		}

    }

}