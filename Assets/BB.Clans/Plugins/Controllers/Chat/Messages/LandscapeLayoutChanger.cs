using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{
    public class LandscapeLayoutChanger : MonoBehaviour
    {
        [Header("Landscape")]
        [SerializeField] private LayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;

        private void Awake()
        {
#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
#endif
        }
    }
}