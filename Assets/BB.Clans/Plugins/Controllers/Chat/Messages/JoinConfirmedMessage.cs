using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class JoinConfirmedMessage : AdminPlayerMessage
    {

        [SerializeField] private Text playerName;
        [SerializeField] private PredefinedIconView playerIcon;
        [SerializeField] private GameObject subsContainer;
        [SerializeField] private Image subsImage;
        [Header("Landscape")]
        [SerializeField] private HorizontalLayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;

        internal override FeedItemType MessageType { get { return FeedItemType.JoinConfirmed; } }

        protected override void UpdateViews()
        {
            if (FeedItem == null) return;
            playerName.text = ValidateHelper.PlayerIdToTempNick(FeedItem.playerId);
            playerIcon.Color = Color.clear;
            GetProfile(FeedItem.playerId, OnProfileLoaded);

#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
            subsImage.rectTransform.sizeDelta = new Vector2(16, 16);
#endif

            base.UpdateViews();
        }

        private void OnProfileLoaded(IPlayerProfile profile)
        {
            if (Disposed || profile == null || string.IsNullOrEmpty(profile.Nickname)) return;
            playerName.text = profile.Nickname;
            playerIcon.Color = Color.white;
            SetIcon(playerIcon, profile.Icon);
            SetSubscription(subsContainer, subsImage, profile.SubscriptionId);
        }

    }

}