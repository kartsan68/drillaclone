using BlackBears.Clans.Model.Timeline;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class AdministrationMessage : MessageWithHeader
    {

        [SerializeField] Text message;
        [Header("Landscape")]
        [SerializeField] private HorizontalLayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;

        internal override FeedItemType MessageType { get { return FeedItemType.AdministrationMessage; } }

        protected override void UpdateViews()
        {
            base.UpdateViews();
            if (FeedItem == null) return;
            message.text = FeedItem.info.message;
#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
#endif
        }

    }

}