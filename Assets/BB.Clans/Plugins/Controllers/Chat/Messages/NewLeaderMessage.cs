﻿using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.View.Common;
using BlackBears.Clans.View.Icons;
using BlackBears.GameCore.Views;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class NewLeaderMessage : Message
    {

        [SerializeField] private Button[] userButtons;
        [SerializeField] private GameObject[] userItems;
        [SerializeField] private PredefinedIconView[] icons;
        [SerializeField] private Text[] nicknames;
        [SerializeField] private Text[] scoreTexts;
        [SerializeField] private SpritePackView[] scoreImages;

        internal override FeedItemType MessageType { get { return FeedItemType.NewLeader; } }

        internal override void Inject(ChatCommandHandler commandHandler, 
            PlayerProfileCache profileCache, ClanFullSettings settings)
        {
            base.Inject(commandHandler, profileCache, settings);
            foreach(var si in scoreImages) si.SetSpritePack(settings.scoreIconPack);
        }

        protected override void Awake()
        {
            base.Awake();
            for (int i = 0; i < userButtons.Length; i++)
            {
                int index = i;
                userButtons[i].onClick.AddListener(() => OnUserClicked(index));
            }
        }

        protected override void UpdateViews()
        {
            if (FeedItem == null) return;

            var scores = FeedItem.info.playerScores;
            int scoresCount = scores != null ? scores.Length : 0;

            for (int i = 0; i < scoresCount; i++)
            {
                if (i >= userItems.Length) continue;
                int index = i;
                userItems[i].SetActive(true);
                if (i < scoreTexts.Length) scoreTexts[i].text = scores[i].score.ToString();
                if (i < icons.Length) icons[i].gameObject.SetActive(false);
                if (i < nicknames.Length)
                {
                    nicknames[i].text = ValidateHelper.PlayerIdToTempNick(scores[i].id);
                    GetProfile(scores[i].id, profile => OnProfileCome(index, profile));
                }
            }
            for (int i = scoresCount; i < userItems.Length; i++) userItems[i].SetActive(false);
        }

        private void OnProfileCome(int index, IPlayerProfile profile)
        {
            if (Disposed) return;

            nicknames[index].text = profile.Nickname;
            if (index < icons.Length) 
            {
                icons[index].gameObject.SetActive(true);
                SetIcon(icons[index], profile.Icon);
            }
        }

        private void OnUserClicked(int index) => OpenProfile(FeedItem.info.playerScores[index].id);

    }

}