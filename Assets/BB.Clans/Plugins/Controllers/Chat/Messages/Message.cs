﻿using System;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.View.Icons;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    [RequireComponent(typeof(RectTransform))]
    internal abstract class Message : MonoBehaviour
    {

        [SerializeField] private GameObject playerMessageMark;

        private bool interactable = true;
        private RectTransform rectTransform;
        private LayoutGroup layoutGroup;
        private PlayerProfileCache profileCache;

        private int withHeadPadding;
        private int withoutHeadPadding = 4;

        internal RectTransform RectTransform
        {
            get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); }
        }

        internal FeedItem FeedItem { get; private set; }
        internal bool IsGroupHead { get; private set; }
        internal bool IsPlayerMessage { get; private set; }

        internal bool Interactable
        {
            get { return interactable; }
            set
            {
                interactable = value;
                UpdateInteractableViews();
            }
        }

        internal ChatCommandHandler CommandHandler { get; private set; }
        protected ClanFullSettings Settings { get; private set; }

        internal abstract FeedItemType MessageType { get; }

        protected bool Disposed { get; private set; }

        internal virtual void Inject(ChatCommandHandler commandHandler,
            PlayerProfileCache profileCache, ClanFullSettings settings)
        {
            this.CommandHandler = commandHandler;
            this.profileCache = profileCache;
            this.Settings = settings;
        }

        protected virtual void Awake()
        {
            layoutGroup = GetComponent<LayoutGroup>();
            if (layoutGroup != null) withHeadPadding = layoutGroup.padding.top;
        }

        protected virtual void OnDestroy()
        {
            Disposed = true;
        }

        internal void SetData(FeedItem item, bool isGroupHead, bool isPlayerMessage)
        {
            FeedItem = item;
            IsGroupHead = isGroupHead;
            IsPlayerMessage = isPlayerMessage;
            UpdateViews();
        }

        protected virtual void UpdateViews()
        {
            if (layoutGroup != null) layoutGroup.padding.top = IsGroupHead ? withHeadPadding : withoutHeadPadding;
            if (playerMessageMark != null) playerMessageMark.SetActive(IsPlayerMessage);
            UpdateInteractableViews();
        }

        protected void SetIcon(PredefinedIconView iconView, Icon icon)
        {
            if (iconView != null) iconView.SetIcon(icon, Settings.configuration.ProfileIcons);
        }

        protected void SetSubscription(GameObject subsContainer, Image subsImage, string subscriptionId)
        {
            if (subsImage == null) return;
            subsImage.sprite = Settings.subsSprites[subscriptionId]?.OutlinedSprite;
#if BBGC_LANDSCAPE
            subsImage.rectTransform.sizeDelta = new Vector2(16, 16);
#endif
            subsContainer?.SetActive(subsImage.sprite != null);
        }

        protected virtual void UpdateInteractableViews() { }

        protected void GetProfile(string playerId, Block<IPlayerProfile> callback)
        {
            profileCache.GetPlayerAsync(playerId, callback, (c, m) =>
            {
                callback.SafeInvoke(profileCache.GetCachedPlayer(playerId));
            });
        }

        protected void OpenProfile(string playerId)
        {
            CommandHandler.OpenPlayerProfile(playerId);
        }

    }

}