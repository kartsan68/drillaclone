using BlackBears.Clans.Model.Timeline;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class RatingRiseMessage : Message
    {

        internal override FeedItemType MessageType { get { return FeedItemType.RatingRise; } }
        
    }

}