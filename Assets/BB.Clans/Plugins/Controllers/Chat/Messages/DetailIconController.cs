﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class DetailIconController : MonoBehaviour 
    {

        [SerializeField] private Image icon;

        internal Sprite IconSprite
        {
            get { return icon.sprite; }
            set { icon.sprite = value; }
        }

    }

}


