using BlackBears.Clans.Model.Timeline;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{
    internal class PhotoMessage : Message
    {

        [SerializeField] private Button photoButton;

        internal override FeedItemType MessageType { get { return FeedItemType.PhotoMessage; } }

        protected override void Awake()
        {
            base.Awake();
            photoButton.onClick.AddListener(OnPhotoClick);
        }

        private void OnPhotoClick()
        {
            CommandHandler.ShowPhoto();
        }

    }
}