﻿using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat.Messages
{

    internal class TextMessage : MessageWithHeader
    {

        [SerializeField] private Text nicknameText;
        [SerializeField] private PredefinedIconView iconView;
        [SerializeField] private GameObject subsContainer;
        [SerializeField] private Image subsImage;
        [SerializeField] private Text messageText;

        [Header("Landscape")]
        [SerializeField] private HorizontalLayoutGroup layoutGroup;
        [SerializeField] private int leftOffset;
        [SerializeField] private int rightOffset;


        internal override FeedItemType MessageType { get { return FeedItemType.Message; } }

        protected override void UpdateViews()
        {
            base.UpdateViews();
            if (FeedItem == null) return;
            nicknameText.text = ValidateHelper.PlayerIdToTempNick(FeedItem.playerId);
            iconView.Color = Color.clear;
            messageText.text = FeedItem.info.message;
            base.UpdateViews();
            GetProfile(FeedItem.playerId, OnProfileLoaded);

#if BBGC_LANDSCAPE
            layoutGroup.padding.left = leftOffset;
            layoutGroup.padding.right = rightOffset;
            subsImage.rectTransform.sizeDelta = new Vector2(16, 16);
#endif
        }

        private void OnProfileLoaded(IPlayerProfile profile)
        {
            if (Disposed || profile == null || string.IsNullOrEmpty(profile.Nickname)) return;
            nicknameText.text = profile.Nickname;
            iconView.Color = Color.white;
            SetIcon(iconView, profile.Icon);
            SetSubscription(subsContainer, subsImage, profile.SubscriptionId);
        }

    }

}