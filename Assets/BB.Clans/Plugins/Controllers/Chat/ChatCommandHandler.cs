using System;
using BlackBears.Clans.Controller.Chat.Messages;
using BlackBears.Clans.Controller.ContextMenu;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Controller.Panels.Landing;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.ResourceShare;
using BlackBears.Clans.ViewModel.ContextMenu;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;

namespace BlackBears.Clans.Controller.Chat
{

    internal class ChatCommandHandler
    {

        private AlertsFeature alerts;
        private NotificationsFeature notifications;
        private ClanCore core;

        private FeedController feed;
        private LoaderController loader;
        private ContextController context;
        private FragmentsController fragments;
        private PhotoController photoController;

        private Menu.Builder contextMenuBuilder;

        internal ChatCommandHandler(ClanCore core, FeedController feed, LoaderController loader,
            ContextController context, FragmentsController fragments, PhotoController photoController)
        {
            this.alerts = BBGC.Features.Alerts();
            this.notifications = BBGC.Features.Notifications();
            this.core = core;
            this.feed = feed;
            this.loader = loader;
            this.context = context;
            this.fragments = fragments;
            this.photoController = photoController;
        }

        internal string CurrentPlayerId => core.State.ProfileCache.LocalPlayer.Id;
        internal Warehouse Warehouse => core.State.Warehouse;

        internal void PlayerAccept(Message message)
        {
            if (!IsValidMessage(message, FeedItemType.JoinRequest)) return;

            message.Interactable = false;
            core.PromotePlayer(message.FeedItem.playerId, ReloadFeed, (c, m) => OnCommandFailed("AcceptPlayer", c, m, message));
        }

        internal void PlayerDeny(JoinRequestMessage message)
        {
            if (!IsValidMessage(message, FeedItemType.JoinRequest)) return;

            message.Interactable = false;
            var profile = core.State.ProfileCache.GetCachedPlayer(message.FeedItem.playerId);
            string playerNickname = profile != null ? profile.Nickname : ValidateHelper.PlayerIdToTempNick(message.FeedItem.playerId);

            var alert = AlertsBuilder.CreateKickUserAlert(
                playerNickname,
                () => core.KickFromClan(message.FeedItem.playerId, ReloadFeed, (c, m) => OnCommandFailed("PlayerDeny", c, m, message)),
                null);
            alerts.AddAlert(alert);
        }

        internal void OpenPlayerProfile(string playerId) { fragments.ShowPlayerInfo(playerId, null, core.State.Clan); }
        internal void AnswerToPlayer(string playerId) { feed.AnswerToPlayer(playerId); }

        internal void TryShowContextMenu(string playerId, Vector2 clickPos, Camera camera)
        {
            if (!core.State.IsUserInClan) return;
            if (string.Equals(core.State.Game.PlayerId, playerId)) return;

            var userRole = core.State.User.Role;
            var otherRole = core.State.Clan.GetUserClanRole(playerId);

            var builder = contextMenuBuilder ?? (contextMenuBuilder = new Menu.Builder(context.Preferences));
            builder.Reset();

            builder.AddPlayerMenuItem(playerId, () => OpenPlayerProfile(playerId), true);
            if (otherRole.IsClanMember()) builder.AddAnswerItem(() => AnswerToPlayer(playerId));

            if (userRole.IsAdministrator() && otherRole.IsClanMember())
            {
                if (otherRole.IsModerator()) builder.AddDemoteItem(() => DemotePlayerFromContextMenu(playerId));
                else builder.AddPromoteItem(() => PromotePlayerFromContextMenu(playerId));
            }

            if (otherRole.IsClanMember() && (userRole.IsAdministrator() || userRole.IsModerator() && !otherRole.IsModerator()))
            {
                builder.AddKickItem(() => KickPlayerFromContextMenu(playerId));
            }

            context.ShowContextMenu(builder.Build(), clickPos, camera);
        }

        internal string AmountToPrettyString(double amount) => core.State.Game.AmountToPrettyString(amount);

        internal void ShowPhoto()
        {
            photoController.Show();
        }

        internal void OpenRequests() => fragments.ShowResourceRequests();
        internal void OpenStorage() => fragments.ShowResourceTake();

        internal void SendResource(FeedItem feedItem, Block<ResourceTransition> onSuccess, FailBlock onFail)
        {
            Warehouse.SendResource(feedItem, onSuccess, (c, m) =>
            {
                var error = (GiveResourceRequestError)c;
                switch (error)
                {
                    case GiveResourceRequestError.other:
                    case GiveResourceRequestError.nonExistentRequest:
                    case GiveResourceRequestError.expiredRequest:
                        notifications.ShowNotification(new Notification(NotificationType.Information,
                            ClansLocalization.Get(LocalizationKeys.notificationDonationExpired)));
                        break;
                    case GiveResourceRequestError.donationLimit:
                        notifications.ShowNotification(new Notification(NotificationType.Information,
                                ClansLocalization.Get(LocalizationKeys.notificationResourceGiveLimited)));
                        break;
                    case GiveResourceRequestError.notEnoughResource:
                        notifications.ShowNotification(new Notification(NotificationType.Information,
                                ClansLocalization.Get(LocalizationKeys.notificationDonationResourceNotEnough)));
                        break;
                }
                onFail.SafeInvoke();
            },
            (amount) =>
            {
                notifications.ShowNotification(new Notification(NotificationType.Positive,
                    string.Format(ClansLocalization.Get(LocalizationKeys.notificationDonationReward), 
                    AmountToPrettyString(amount))));
            });
        }

        private void PromotePlayerFromContextMenu(string playerId)
        {
            loader.Show(this);
            core.PromotePlayer(playerId, ContextCommandCompleted, (c, m) => OnCommandFailed("PromotePlayerFromContextMenu", c, m));
        }

        private void DemotePlayerFromContextMenu(string playerId)
        {
            loader.Show(this);
            core.DemotePlayer(playerId, ContextCommandCompleted, (c, m) => OnCommandFailed("DemotePlayerFromContextMenu", c, m));
        }

        private void KickPlayerFromContextMenu(string playerId)
        {
            var profile = core.State.ProfileCache.GetCachedPlayer(playerId);
            string playerNickname = profile != null ? profile.Nickname : ValidateHelper.PlayerIdToTempNick(playerId);

            var alert = AlertsBuilder.CreateKickUserAlert(
                playerNickname,
                () =>
                {
                    loader.Show(this);
                    core.KickFromClan(playerId, ContextCommandCompleted, (c, m) => OnCommandFailed("KickPlayerFromContextMenu", c, m));
                },
                null);
            alerts.AddAlert(alert);
        }

        private void ContextCommandCompleted()
        {
            ReloadFeed();
            loader.Hide(this);
        }

        private bool IsValidMessage(Message message, FeedItemType expectedType)
        {
            return message != null && message.MessageType == expectedType
                && message.FeedItem != null && message.FeedItem.feedType == expectedType;
        }

        private void ReloadFeed() { feed.ReloadFeed(true, false, true); }

        private void OnCommandFailed(string method, long errorCode, string errorMessage, Message message = null)
        {
            UnityEngine.Debug.LogFormat("{0}: {1}; {2}", method, errorCode, errorMessage);
            if (message != null) message.Interactable = true;
        }

    }

}