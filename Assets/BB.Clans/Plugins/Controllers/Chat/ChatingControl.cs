﻿using System;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Chat
{

    internal class ChatingControl : MonoBehaviour
    {

        [SerializeField] private GameObject buttonsContainer;
        [SerializeField] private GameObject inputContainer;

        [SerializeField] private Button writeMessageButton;
        [SerializeField] private SubmitableInputField messageInputField;

        private FeedController feed;

        internal void Inject(FeedController feed)
        {
            this.feed = feed;
        }

        private void Awake()
        {
            messageInputField.inputType = InputField.InputType.AutoCorrect;
            writeMessageButton.onClick.AddListener(OnWriteMessageRequest);
            messageInputField.onEndEdit.AddListener(OnInputFieldLostFocus);

            buttonsContainer.SetActive(true);
            inputContainer.SetActive(false);
        }

        internal void AnswerToPlayer(string playerId, IPlayerProfile profile)
        {
            var text = messageInputField.text;
            int index = text.IndexOf(',');
            if (index >= 0 && index <= ValidateHelper.nicknameMaxLength + 1) return;
            string playerName = null;
            if (profile != null) playerName = profile.Nickname;
            if (playerName == null) playerName = ValidateHelper.PlayerIdToTempNick(playerId);
            messageInputField.text = string.Format("{0}, {1}", playerName, text);

            OnWriteMessageRequest();
        }

        private void OnWriteMessageRequest()
        {
            buttonsContainer.SetActive(false);
            inputContainer.SetActive(true);
            messageInputField.Select();
        }

        private void OnInputFieldLostFocus(string message)
        {
            if (messageInputField.IsSubmitNow)
            {
                feed.SendMessageRequest(message);
            }
            else
            {
                CloseWriting();
            }
        }

        internal void CloseWriting()
        {
            buttonsContainer.SetActive(true);
            inputContainer.SetActive(false);
        }

        internal void MessageSendingSuccess()
        {
            messageInputField.text = null;
            buttonsContainer.SetActive(true);
            inputContainer.SetActive(false);
        }

        internal void MessageSendingFailed()
        {
            buttonsContainer.SetActive(true);
            inputContainer.SetActive(false);
        }

    }

}