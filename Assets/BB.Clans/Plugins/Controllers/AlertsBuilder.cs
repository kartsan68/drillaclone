using BlackBears.GameCore.Features.Alerts;

using L10n = BlackBears.Clans.ClansLocalization;

namespace BlackBears.Clans.Controller
{

    internal static class AlertsBuilder
    {

        internal static Alert CreateLeaveClanAlert(Block onAccept, Block onCancel)
        {
            return new YesNoAlert(AlertType.Negative,
                L10n.Get(LocalizationKeys.alertExitClanTitle), 
                L10n.Get(LocalizationKeys.alertExitClanDescription),
                L10n.Get(LocalizationKeys.alertExitClanAction), 
                L10n.Get(LocalizationKeys.cancel),
                onAccept, onCancel);
        }

        internal static Alert CreateDestroyClanAlert(Block onAccept, Block onCancel)
        {
            return new YesNoAlert(AlertType.Destructive,
                L10n.Get(LocalizationKeys.alertDestroyClanTitle),
                L10n.Get(LocalizationKeys.alertDestroyClanDescription),
                L10n.Get(LocalizationKeys.alertDestroyClanAction), 
                L10n.Get(LocalizationKeys.cancel),
                onAccept, onCancel);
        }

        internal static Alert CreateKickUserAlert(string userName, Block onAccept, Block onCancel)
        {
            return new YesNoAlert(AlertType.Negative,
                L10n.Get(LocalizationKeys.alertKickUserTitle),
                string.Format(L10n.Get(LocalizationKeys.alertKickUserDescription), userName),
                L10n.Get(LocalizationKeys.alertKickUserAction),
                L10n.Get(LocalizationKeys.cancel),
                onAccept, onCancel);
        }

    }

}