﻿using System;
using BlackBears.Clans.Model;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using Random = UnityEngine.Random;

namespace BlackBears.Clans.Controller
{

    internal class IconEditController : MonoBehaviour
    {

        [SerializeField] private Button previousButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button randomButton;
        [SerializeField] private Image iconImage;

        private PredefinedIconsContainer iconsContainer;
        private PredefinedIcon icon;

        private UnityEvent onIconChange = new UnityEvent();

        internal Icon Icon { get { return icon; } }
        internal UnityEvent OnIconChange { get { return onIconChange; } }

        internal void Inject(PredefinedIconsContainer iconsContainer)
        {
            this.iconsContainer = iconsContainer;
        }

        private void Start()
        {
            previousButton.onClick.AddListener(SelectPrevious);
            nextButton.onClick.AddListener(SelectNext);
            randomButton.onClick.AddListener(SelectRandom);

            if (icon == null) SelectRandom();
        }

        internal void SetIcon(Icon inIcon)
        {
            var icon = inIcon as PredefinedIcon;
            if (icon == null) return;
            this.icon = icon;
            onIconChange.Invoke();
            SetIconToImage();
        }

        private void SelectPrevious()
        {
            CheckIconForCreate();

            icon.Id = ((icon.Id - 1) + iconsContainer.Count) % iconsContainer.Count;
            onIconChange.Invoke();
            SetIconToImage();
        }

        private void SelectNext()
        {
            CheckIconForCreate();
            
            icon.Id = (icon.Id + 1) % iconsContainer.Count;
            onIconChange.Invoke();
            SetIconToImage();
        }

        private void SelectRandom()
        {
            CheckIconForCreate();
            
            int id = icon.Id;
            for (int i = 0; i < 3; i++)
            {
                id = Random.Range(0, iconsContainer.Count);
                if (id != icon.Id) break;
            }
            if (icon.Id == id)
            {
                SelectNext();
            } 
            else 
            {
                icon.Id = id;
                onIconChange.Invoke();
                SetIconToImage();
            }
        }

        private void CheckIconForCreate()
        {
            if (icon == null) icon = new PredefinedIcon();
        }

        private void SetIconToImage()
        {
            iconImage.sprite = iconsContainer.GetIconByIndexOrDefault(icon.Id).BigIcon;
        }

    }

}