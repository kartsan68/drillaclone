﻿using System;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Controller.Panels.Landing;
using BlackBears.Clans.Events;
using BlackBears.Clans.View.Events;
using UnityEngine;
using UnityEngine.UI;
using EventType = BlackBears.Clans.Events.EventType;

namespace BlackBears.Clans.Controller
{

    internal class EventCardsContainer : MonoBehaviour
    {

        private ClanCore core;
        private ClanFullSettings settings;
        private FragmentsController fragmentsController;
        private EventCard card;
        private ErrorController errorController;

        private bool isErrorsBlocked;

        internal void Inject(ClanCore core, ClanFullSettings settings,
            FragmentsController fragmentsController, ErrorController errorController)
        {
            this.core = core;
            this.settings = settings;
            this.fragmentsController = fragmentsController;
            this.errorController = errorController;
        }

        internal void DestroyCurrentCard()
        {
            if (card == null) return;
            Destroy(card.gameObject);
            card = null;
        }

        private void Start()
        {
            core.State.EventsCore.OnEventStateChanged += OnEventStateChanged;
            OnEventStateChanged(EventType.Undefined);
        }

        private void OnDestroy()
        {
            core.State.EventsCore.OnEventStateChanged -= OnEventStateChanged;
            if (card != null) card.OnError -= OnError;
        }

        internal bool IsCurrentCard(EventCard card) => this.card == card;

		internal void BlockErrors()
		{
			if (isErrorsBlocked) return;
			isErrorsBlocked = true;
		}

		internal void UnblockErrors()
		{
			if (!isErrorsBlocked) return;
			isErrorsBlocked = false;
			card?.ExternalCheck();
		}

        private void OnEventStateChanged(EventType eventType)
        {
            ClanEvent e = core.State.EventsCore.GetEventWithCard();
            if (card != null)
            {
                if (card.UpdateCardWithEvent(e)) return;
                card.OnError -= OnError;
                Destroy(card.gameObject);
                card = null;
            }
            if (e == null) return;

            var cardPrefab = core.State.EventsCore.GetCardPrefabForEvent(e);
            if (cardPrefab == null) return;

            card = Instantiate(cardPrefab, transform, false);
            card.Inject(this, core, settings, fragmentsController);
            card.OnError += OnError;
            card.UpdateCardWithEvent(e);
        }

        private void OnError(Block repeatBlock)
        {
			if (isErrorsBlocked) return;

            errorController.Show(ClansLocalization.Get(LocalizationKeys.errorCommonDescription), () =>
            {
                errorController.Hide();
                repeatBlock.SafeInvoke();
            });
        }

    }

}