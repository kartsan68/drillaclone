namespace BlackBears.Clans.Controller.CountrySelection
{
    internal class SearchCountrySelector : BaseCountrySelector
    {

        private string[] countries;

        internal void Inject(CountriesContainer container, string[] countries)
        {
            this.countries = countries;
            Setup(container);
        }

        protected override CountryListBaseAdapter CreateAdapter(CountriesContainer container)
        {
            var flagsArray = container.GetCountriesSorted(NeedAnyCountry, this.countries);
            return new CountryListArrayAdapter(container, flagsArray);
        }

    }
}