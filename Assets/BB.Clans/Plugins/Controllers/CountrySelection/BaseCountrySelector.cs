﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.CountrySelection
{

    internal abstract class BaseCountrySelector : MonoBehaviour
    {

        [SerializeField] private HorizontalInfiniteController infiniteController;
        [SerializeField] private ScrollPaginator paginator;
        [SerializeField] private Text countryText;
        [SerializeField] private bool needAnyCountry = false;

        private CountryListBaseAdapter adapter;
        private StringUnityEvent onCountryChange = new StringUnityEvent();

        internal string Selected { get; private set; }
        internal StringUnityEvent OnCountryChange { get { return onCountryChange; } }

        protected bool NeedAnyCountry { get { return needAnyCountry; } }

        protected void Setup(CountriesContainer countries)
        {
            adapter = CreateAdapter(countries);
            infiniteController.Adapter = adapter;
        }

        internal void Initiate(string locale)
        {
            paginator.OnPageSelected.AddListener(OnCountrySelected);

            int index = adapter.GetCountryIndex(locale);
            infiniteController.Initiate(index);
            paginator.indexOffset = index;
            OnCountrySelected(index);
        }

        internal void Select(string locale)
        {
            int index = adapter.GetCountryIndex(locale);
            infiniteController.ToIndex(index);
            paginator.indexOffset = index;
            OnCountrySelected(index);
        }

        protected abstract CountryListBaseAdapter CreateAdapter(CountriesContainer countries);

        private void OnCountrySelected(int index)
        {
            var country = adapter.GetCountryByIndex(index);
            countryText.text =  ClansLocalization.Get(country);
            Selected = country;
            onCountryChange.Invoke(country);
        }

    }

}