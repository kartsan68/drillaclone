﻿using System;
using BlackBears.Clans.View;
using UnityEngine;
using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.CountrySelection
{

    internal sealed class CountryListContainerAdapter : CountryListBaseAdapter
    {

        private CountriesContainer countries;
        private int startIndex;

        internal CountryListContainerAdapter(CountriesContainer countries, bool needAnyCountry)
        {
            this.countries = countries;
            this.startIndex = countries.GetIndexOffset(needAnyCountry);
        }

        public override void BindItem(GameObject itemObject, int index, int type)
        {
            itemObject.GetComponent<FlagView>().Sprite = countries[index];
        }

        public override void UnbindItem(GameObject itemObject, int index, int type)
        {
            itemObject.GetComponent<FlagView>().Sprite = null;
        }

        public override bool IsValidIndex(int index) { return index >= startIndex && index < countries.Count; }
        
        public override GameObject InstantiatePrefab(GameObject prefab) => 
            UnityEngine.Object.Instantiate(prefab);

        internal override int GetCountryIndex(string country)
        {
            int index = countries.GetCountryIndex(country);
            return index >= 0 ? index : countries.DefaultCountryIndex;
        }

        internal override string GetCountryByIndex(int index)
        {
            index = Mathf.Clamp(index, startIndex, countries.Count - 1);
            return countries[index].name;
        }

    }

}