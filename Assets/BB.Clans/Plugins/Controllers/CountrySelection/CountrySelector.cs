namespace BlackBears.Clans.Controller.CountrySelection
{
    internal class CountrySelector : BaseCountrySelector
    {

        public void Inject(CountriesContainer container)
        {
            Setup(container);
        }

        protected override CountryListBaseAdapter CreateAdapter(CountriesContainer countries)
        {
            return new CountryListContainerAdapter(countries, NeedAnyCountry);
        }

    }
}