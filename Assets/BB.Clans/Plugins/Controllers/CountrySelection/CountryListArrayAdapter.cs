using BlackBears.Clans.View;
using UnityEngine;

namespace BlackBears.Clans.Controller.CountrySelection
{
    internal class CountryListArrayAdapter : CountryListBaseAdapter
    {

        private CountriesContainer container;
        private Sprite[] countries;

        internal CountryListArrayAdapter(CountriesContainer container, Sprite[] countries)
        {
            this.container = container;
            this.countries = countries;
        }

        public override GameObject InstantiatePrefab(GameObject prefab) => 
            UnityEngine.Object.Instantiate(prefab);

        public override void BindItem(GameObject itemObject, int index, int type)
        {
            itemObject.GetComponent<FlagView>().Sprite = countries[index];
        }

        public override void UnbindItem(GameObject itemObject, int index, int type)
        {
            itemObject.GetComponent<FlagView>().Sprite = null;
        }

        public override bool IsValidIndex(int index) { return 0 <= index && index < countries.Length; }

        internal override int GetCountryIndex(string country) { return container.GetCountryIndex(countries, country); }
        internal override string GetCountryByIndex(int index)
        {
            return countries[Mathf.Clamp(index, 0, countries.Length - 1)].name;
        }

    }
}