using UnityEngine.UI.Additional;

namespace BlackBears.Clans.Controller.CountrySelection
{
    internal abstract class CountryListBaseAdapter : InfiniteAdapter
    {

        internal abstract int GetCountryIndex(string country);
        internal abstract string GetCountryByIndex(int index);

    }
}