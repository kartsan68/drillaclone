﻿using BlackBears.Clans.ViewModel.ContextMenu;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.ContextMenu
{

    internal class IconTextMenuItemController : MenuItemController
    {

        [SerializeField] private Image iconImage;
        [SerializeField] private Text text;

        private IconTextMenuItem iconTextItem;

        internal IconTextMenuItem IconTextItem
        {
            get { return iconTextItem; }
			set
			{
				iconTextItem = value;
				UpdateView();
			}
        }

        internal override MenuItem MenuItem { get { return IconTextItem; } }

		private void UpdateView()
		{
            iconImage.sprite = IconTextItem.icon;
            text.text =  ClansLocalization.Get(IconTextItem.text);
            text.color = IconTextItem.textColor;
		}

    }

}