﻿using System;
using BlackBears.Clans.Model.Helpers;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.View.Icons;
using BlackBears.Clans.ViewModel.ContextMenu;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.ContextMenu
{

    internal class ProfileMenuItemController : MenuItemController
    {

        [SerializeField] private PredefinedIconView icon;
		[SerializeField] private Text nicknameText;

        private PlayerProfileCache profileCache;
        private PredefinedIconsContainer profileIcons;
        private ProfileMenuItem profileItem;

        internal ProfileMenuItem ProfileItem
        {
            get { return profileItem; }
            set
            {
                profileItem = value;
                UpdateView();
            }
        }

        internal override MenuItem MenuItem { get { return ProfileItem; } }

        internal void Inject(PlayerProfileCache profileCache, PredefinedIconsContainer profileIcons)
        {
            this.profileCache = profileCache;
            this.profileIcons = profileIcons;
        }

        private void UpdateView()
        {
			nicknameText.text = ValidateHelper.PlayerIdToTempNick(ProfileItem.playerId);
            icon.ClearIcon();
            icon.gameObject.SetActive(false);
            var profile = profileCache.GetCachedPlayer(ProfileItem.playerId);
            if (profile != null && !string.IsNullOrEmpty(profile.Nickname)) 
            {
                nicknameText.text = profile.Nickname;
                icon.SetIcon(profile.Icon, profileIcons);
                icon.gameObject.SetActive(true);
            }
        }
    }

}