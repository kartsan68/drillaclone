﻿using BlackBears.Clans.ViewModel.ContextMenu;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackBears.Clans.Controller.ContextMenu
{

	[RequireComponent(typeof(RectTransform))]
    internal abstract class MenuItemController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {

		internal MenuController menu;
		private RectTransform rectTransform;

		internal RectTransform RectTransform { get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); } }
		internal abstract MenuItem MenuItem { get; }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {}
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData) {}

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
			if (menu != null) menu.OnItemSelected(MenuItem);
        }

    }

}