﻿using System;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ViewModel.ContextMenu;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackBears.Clans.Controller.ContextMenu
{

    [RequireComponent(typeof(CanvasGroup))]
    internal class ContextController : MonoBehaviour, IPointerDownHandler
    {

        [SerializeField] private MenuBuilderPreferences preferences;
        [SerializeField] private MenuController menuController;

        private CanvasGroup canvasGroup;

        internal MenuBuilderPreferences Preferences { get { return preferences; } }

        internal void Inject(PlayerProfileCache profileCache, PredefinedIconsContainer profileIcons)
        {
            menuController.Inject(profileCache, profileIcons);
        }

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();

            menuController.Context = this;
            gameObject.SetActive(false);
            canvasGroup.alpha = 0;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void ShowContextMenu(Menu menu, Vector2 screenPosition, Camera camera)
        {
            if (menu == null || menuController.Menu != null) return;
            menuController.Menu = menu;

            var center = new Vector2(Screen.width, Screen.height) / 2f;
            var pivot = new Vector2(screenPosition.x < center.x ? 0 : 1, screenPosition.y < center.y ? 0 : 1);
            menuController.RectTransform.pivot = pivot;
            Vector3 position = default(Vector2);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(menuController.RectTransform, screenPosition, 
                camera, out position);
            menuController.RectTransform.position = position;

            Show();
        }

        internal void Close()
        {
            menuController.Menu = null;
            Hide();
        }

        private void Show()
        {
            gameObject.SetActive(true);
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        private void Hide()
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f).setOnComplete(() => gameObject.SetActive(false));
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            Close();
        }

    }

}