﻿using System;
using System.Collections.Generic;
using BlackBears.Clans.Model.PlayerProfiles;
using BlackBears.Clans.ViewModel.ContextMenu;
using UnityEngine;

namespace BlackBears.Clans.Controller.ContextMenu
{

	[RequireComponent(typeof(RectTransform))]
    internal class MenuController : MonoBehaviour
    {

		[SerializeField] private RectTransform itemsRoot;
        [SerializeField] private ProfileMenuItemController profileItem;

		[Header("Префаб")]
        [SerializeField] private IconTextMenuItemController iconTextItem;

        private Menu menu;
        private List<IconTextMenuItemController> iconTextControllers = new List<IconTextMenuItemController>();
		private RectTransform rectTransform;

        internal ContextController Context { get; set; }
		internal RectTransform RectTransform { get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); } }

        internal Menu Menu
        {
            get { return menu; }
            set
            {
                menu = value;
                UpdateViews();
            }
        }

        internal void Inject(PlayerProfileCache profileCache, PredefinedIconsContainer profileIcons)
        {
			profileItem.menu = this;
            profileItem.Inject(profileCache, profileIcons);
        }

        internal void OnItemSelected(MenuItem menuItem)
        {
            if (menuItem == null) return;
            Context.Close();
            menuItem.onSelect.SafeInvoke();
        }

        private void UpdateViews()
        {
            if (menu == null) return;
			profileItem.gameObject.SetActive(false);
            int iconItextItemIndex = 0;
            foreach (var item in menu.Items)
            {
                bool added = false;
                if (TryAddAsIconTextMenu(item, iconItextItemIndex))
                {
                    added = true;
                    iconItextItemIndex += 1;
                }
                if (!added) added = TryAddAsProfileMenu(item);
            }
			RemoveIconTextItemsFromIndex(iconItextItemIndex);
        }

		private bool TryAddAsProfileMenu(MenuItem inItem)
		{
			var item = inItem as ProfileMenuItem;
			if(item == null) return false;

			profileItem.ProfileItem = item;
			profileItem.gameObject.SetActive(true);
			return true;
		}

        private bool TryAddAsIconTextMenu(MenuItem inItem, int index)
        {
            var item = inItem as IconTextMenuItem;
            if (item == null) return false;

            IconTextMenuItemController controller;
            if (index < iconTextControllers.Count)
            {
                controller = iconTextControllers[index];
            }
            else
			{
				controller = Instantiate(iconTextItem);
				controller.menu = this;
				controller.RectTransform.SetParent(itemsRoot, false);
				controller.RectTransform.SetAsLastSibling();
				iconTextControllers.Add(controller);
			}
			controller.IconTextItem = item;
			return true;
        }

		private void RemoveIconTextItemsFromIndex(int index)
		{
			if (index >= iconTextControllers.Count) return;
			for (int i = index; i < iconTextControllers.Count; i++)
			{
				Destroy(iconTextControllers[i].gameObject);
			}
			iconTextControllers.RemoveRange(index, iconTextControllers.Count - index);
		}

    }

}