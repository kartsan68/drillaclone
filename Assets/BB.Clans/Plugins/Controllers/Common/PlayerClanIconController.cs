﻿using System;
using BlackBears.Clans.Model;
using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Common
{

    [RequireComponent(typeof(PredefinedIconView))]
    internal class PlayerClanIconController : MonoBehaviour
    {

        private ClanCore core;
        private PredefinedIconsContainer icons;
        private PredefinedIconView iconView;

        private Clan clan;
        private bool started = false;

        internal void Inject(ClanCore core, PredefinedIconsContainer icons)
        {
            this.core = core;
            this.icons = icons;
            if (started) Setup();
        }

        private void Start()
        {
            iconView = GetComponent<PredefinedIconView>();
            started = true;

            if (core == null || icons == null) return;
            Setup();
        }

        private void OnDestroy()
        {
            core.State.OnClanChanged -= ClanChanged;
            UnsubscribeFromClan();
            clan = null;
        }

        private void Setup()
        {
            core.State.OnClanChanged += ClanChanged;
            ClanChanged(core.State.Clan);
        }

        private void ClanChanged(Clan clan)
        {
            UnsubscribeFromClan();
            this.clan = clan;
            SubscribeToClan();
			UpdateView();
        }

        private void SubscribeToClan()
        {
            if (clan == null) return;
            clan.OnUpdated += UpdateView;
        }

        private void UnsubscribeFromClan()
        {
            if (clan == null) return;
            clan.OnUpdated -= UpdateView;
        }

        private void UpdateView()
        {
			Icon icon = clan != null ? clan.Icon : null;
			iconView.SetIcon(icon, icons);
        }

    }

}