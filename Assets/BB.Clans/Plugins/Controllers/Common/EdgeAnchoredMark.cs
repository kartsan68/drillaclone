﻿using UnityEngine;

namespace BlackBears.Clans.Controller.Common
{

    [RequireComponent(typeof(RectTransform))]
    internal class EdgeAnchoredMark : MonoBehaviour
    {

        [SerializeField] private bool anchoredToTop;
        private RectTransform rectTransform;
		private bool showed = false;

        internal RectTransform RectTransform
        {
            get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); }
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void Show(bool force)
        {
			if (showed && !force) return;

			showed = true;
            LeanTween.cancel(gameObject);
            gameObject.SetActive(true);
			if (force) RectTransform.anchoredPosition = Vector2.zero;
			else LeanTween.moveAnchored(RectTransform, Vector2.zero, 0.2f).setEaseOutQuad();
        }

        internal void Hide(bool force)
        {
			if (!showed && !force) return;

			showed = false;
            LeanTween.cancel(gameObject);
            var hidePos = RectTransform.sizeDelta;
            hidePos.x = RectTransform.anchoredPosition.x;
            if (!anchoredToTop) hidePos.y = -hidePos.y;

            if (force)
            {
                RectTransform.anchoredPosition = hidePos;
                gameObject.SetActive(false);
            }
            else
            {
                LeanTween.moveAnchored(RectTransform, hidePos, 0.2f)
                    .setEaseOutQuad()
                    .setOnComplete(() => gameObject.SetActive(false));
            }
        }

    }

}