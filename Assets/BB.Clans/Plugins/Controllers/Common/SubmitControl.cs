using System;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller.Common
{

    [RequireComponent(typeof(RectTransform))]
    internal class SubmitControl : MonoBehaviour
    {

        [SerializeField] private Button submitButton;

        private RectTransform rectTransform;

        private bool showed;
        
        internal Button SubmitButton { get { return submitButton; } }

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            showed = true;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void Show()
        {
            if (showed) return;
            showed = true;

            gameObject.SetActive(true);
            submitButton.interactable = true;
            LeanTween.cancel(gameObject);
            var pos = Vector2.zero;

            LeanTween.moveAnchored(rectTransform, pos, 0.2f)
                .setEaseOutQuad();
        }

        internal void Hide(bool force = false)
        {
            if (!showed && !force) return;
            showed = false;

            submitButton.interactable = false;
            LeanTween.cancel(gameObject);
            var pos = rectTransform.anchoredPosition;
            pos.y = -rectTransform.rect.height;
            if (force)
            {
                rectTransform.anchoredPosition = pos;
                gameObject.SetActive(false);
            }
            else
            {
                LeanTween.moveAnchored(rectTransform, pos, 0.2f)
                    .setEaseInQuad()
                    .setOnComplete(() => gameObject.SetActive(false));
            }
        }

    }

}