﻿using BlackBears.Clans.Controller.Panels;
using UnityEngine;

namespace BlackBears.Clans
{

    [CreateAssetMenu(fileName = "ClanPanels", menuName = "BlackBears/Clans/Panels", order = 6)]
    public class ClanPanels : ScriptableObject
    {

        [SerializeField] private Controller.Panels.PlayerRegister.PlayerRegisterClanPanel playerRegister;
        [SerializeField] private Controller.Panels.Landing.LandingClanPanel landing;
        [SerializeField] private BlackBears.Clans.Controller.Panels.Fragments.FragmentsPanel fragmentsPanel;

        internal Controller.Panels.PlayerRegister.PlayerRegisterClanPanel PlayerRegister { get { return playerRegister; } }
        internal Controller.Panels.Landing.LandingClanPanel Landing { get { return landing; } }
        internal BlackBears.Clans.Controller.Panels.Fragments.FragmentsPanel FragmentsPanel { get { return fragmentsPanel; } }
    }

}