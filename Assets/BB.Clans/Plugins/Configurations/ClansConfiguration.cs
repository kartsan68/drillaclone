using BlackBears.Clans.Events;
using BlackBears.Clans.ResourceShare;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Serialization;

namespace BlackBears.Clans
{

    [CreateAssetMenu(fileName = "Clans", menuName = "BlackBears/Clans/Configuration", order = 5)]
    public class ClansConfiguration : ScriptableObject
    {
        [SerializeField] private string secret;
        [SerializeField] private string jellySecret;
        [SerializeField] private string serverApiVersion = "1.0.0";
        [SerializeField] private ClanPanels panels;
        [SerializeField] private ClanFragments fragments;
        [SerializeField] private ClanExternalFragments externalFragments;
        [SerializeField] private MessagePrefabsContainer messages;
        [SerializeField] private PredefinedIconsContainer profileIcons;
        [SerializeField] private PredefinedIconsContainer clanIcons;
        [SerializeField] private CountriesContainer countries;
        [SerializeField] private ClanEventsSettings clanEvents;
        [SerializeField] private double minScoreInviteRequirement;
        [SerializeField] private double maxScoreInviteRequirement;
        [SerializeField] private double stepScoreInviteRequirement;

        [SerializeField] private bool gameCenterEnabled;
        [SerializeField] private bool gplayEnabled;
        [SerializeField] private bool appStoreEnabled;
        
        [Header("Landscape")]
        [SerializeField] private BlackBears.Clans.Controller.Panels.Landing.GameSection landigGameSection;


        internal string Secret { get { return secret; } }
        internal string JellySecret { get { return jellySecret; } }
        internal string ServerApiVersion { get { return serverApiVersion; } }
        internal ClanPanels Panels { get { return panels; } }
        internal ClanFragments Fragments { get { return fragments; } }
        internal BlackBears.Clans.Controller.Panels.Landing.GameSection LandigGameSection { get { return landigGameSection; } }
        internal ClanExternalFragments ExternalFragments { get { return externalFragments; } }
        internal MessagePrefabsContainer Messages { get { return messages; } }
        internal PredefinedIconsContainer ProfileIcons { get { return profileIcons; } }
        internal PredefinedIconsContainer ClanIcons { get { return clanIcons; } }
        internal CountriesContainer Countries { get { return countries; } }
        internal ClanEventsSettings ClanEvents => clanEvents;
        internal double MinScoreInviteRequirement { get { return minScoreInviteRequirement; } }
        internal double MaxScoreInviteRequirement { get { return maxScoreInviteRequirement; } }
        internal double StepScoreInviteRequirement { get { return stepScoreInviteRequirement; } }

        internal bool GameCenterEnabled { get { return gameCenterEnabled; } }
        internal bool GPlayEnabled { get { return gplayEnabled; } }
        internal bool AppStoreEnabled { get { return appStoreEnabled; } }

    }

}