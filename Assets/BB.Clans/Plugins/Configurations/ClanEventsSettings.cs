﻿using System;
using BlackBears.Clans.View.Events;
using UnityEngine;

namespace BlackBears.Clans.Events
{

	[CreateAssetMenu(menuName = "BlackBears/Clans/Events settings", fileName = "ClanEventsSettings")]
    public class ClanEventsSettings : ScriptableObject
    {

        [SerializeField] private bool battleEventEnabled;
        [SerializeField] private BattleEventCard battleEventCard;

        public bool BattleEventEnabled => battleEventEnabled;
        public EventCard BattleEventCard => battleEventCard;

        internal bool IsEventSupported(EventType eventType)
        {
            switch(eventType)
            {
                case EventType.Battle: return battleEventEnabled;
                default: return false;
            }
        }

    }

}