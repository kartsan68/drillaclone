using UnityEngine;

namespace BlackBears.Clans
{
    
    [CreateAssetMenu(fileName = "ClanFragments", menuName = "BlackBears/Clans/Fragments", order = 7)]
    public class ClanFragments : ScriptableObject
    {

        [SerializeField] private Controller.Fragments.ClanEdit.ClanEditFragment clanEdit;
        [SerializeField] private Controller.Fragments.Profile.ProfileFragment profile;
        [SerializeField] private Controller.Fragments.Profile.ProfileEditFragment profileEdit;
        [SerializeField] private Controller.Fragments.ClanInfo.ClanInfoFragment clanInfo;
        [SerializeField] private Controller.Fragments.ClanTop.ClanTopFragment clanTop;
        [SerializeField] private Controller.Fragments.ClanJoinRequests.ClanJoinRequestsFragment clanJoinRequests;
        [SerializeField] private Controller.Fragments.Invites.InvitesFragment invites;
        [SerializeField] private Controller.Fragments.Support.SupportFragment support;
        [SerializeField] private Controller.Fragments.ResourceShare.ResourceRequestFragment resourceRequest;
        [SerializeField] private Controller.Fragments.ResourceShare.ResourceTakeFragment resourceTake;

        internal Controller.Fragments.ClanEdit.ClanEditFragment ClanEdit => clanEdit;
        internal Controller.Fragments.Profile.ProfileFragment Profile => profile;
        internal Controller.Fragments.Profile.ProfileEditFragment ProfileEdit => profileEdit;
        internal Controller.Fragments.ClanInfo.ClanInfoFragment ClanInfo => clanInfo;
        internal Controller.Fragments.ClanTop.ClanTopFragment ClanTop => clanTop;
        internal Controller.Fragments.ClanJoinRequests.ClanJoinRequestsFragment ClanJoinRequests => clanJoinRequests;
        internal Controller.Fragments.Invites.InvitesFragment Invites => invites;
        internal Controller.Fragments.Support.SupportFragment Support => support;
        internal Controller.Fragments.ResourceShare.ResourceRequestFragment ResourceRequest => resourceRequest;
        internal Controller.Fragments.ResourceShare.ResourceTakeFragment ResourceTake => resourceTake;

    }

}