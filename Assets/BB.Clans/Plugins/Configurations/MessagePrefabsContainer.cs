using BlackBears.Clans.Controller.Chat.Messages;
using UnityEngine;

namespace BlackBears.Clans
{

    [CreateAssetMenu(fileName = "Messages", menuName = "BlackBears/Clans/Messages", order = 6)]
    public class MessagePrefabsContainer : ScriptableObject
    {

        [SerializeField] private TextMessage textMessage;
        [SerializeField] private JoinRequestMessage joinRequestMessage;
        [SerializeField] private JoinConfirmedMessage joinConfirmedMessage;
        [SerializeField] private UserLeftMessage userLeftMessage;
        [SerializeField] private UserKickedMessage userKickedMessage;
        [SerializeField] private UserAcceptedMessage userAcceptedMessage;
        [SerializeField] private UserPromotedMessage userPromotedMessage;
        [SerializeField] private UserDemotedMessage userDemotedMessage;
        [SerializeField] private AdministrationMessage administrationMessage;
        [SerializeField] private NewLeaderMessage newLeaderMessage;
        [SerializeField] private RatingRiseMessage ratingRiseMessage;
        [SerializeField] private PhotoMessage photoMessage;
        [SerializeField] private ResourceRequestMessage resourceRequestMessage;

        internal TextMessage TextMessage { get { return textMessage; } }
        internal JoinRequestMessage JoinRequestMessage { get { return joinRequestMessage; } }
        internal JoinConfirmedMessage JoinConfirmedMessage  { get { return joinConfirmedMessage; } }
        internal UserLeftMessage UserLeftMessage { get { return userLeftMessage; } }
        internal UserKickedMessage UserKickedMessage { get { return userKickedMessage; } }
        internal UserAcceptedMessage UserAcceptedMessage { get { return userAcceptedMessage; } }
        internal UserPromotedMessage UserPromotedMessage { get { return userPromotedMessage; } }
        internal UserDemotedMessage UserDemotedMessage { get { return userDemotedMessage; } }
        internal AdministrationMessage AdministrationMessage { get { return administrationMessage; } }
        internal NewLeaderMessage NewLeaderMessage { get { return newLeaderMessage; } }
        internal RatingRiseMessage RatingRiseMessage { get { return ratingRiseMessage; } }
        internal PhotoMessage PhotoMessage { get { return photoMessage; } }
        internal ResourceRequestMessage ResourceRequestMessage { get { return resourceRequestMessage; } }
        
    }

}