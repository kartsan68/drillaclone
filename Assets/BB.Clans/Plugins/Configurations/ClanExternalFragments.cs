using System.Collections.Generic;
using BlackBears.Clans.Controller.Fragments;
using UnityEngine;

namespace BlackBears.Clans
{

    [CreateAssetMenu(fileName = "ClanExternalFragments", menuName = "BlackBears/Clans/External Fragments", order = 8)]
    public class ClanExternalFragments : ScriptableObject
    {
        
        [SerializeField] private Controller.Fragments.ClanEvents.BattleEventFragment battleEvent;
        [SerializeField] private FragmentWithKey[] customFragments;

        private Dictionary<string, Controller.Fragments.Fragment> customFragmentsDictionary;

        internal Controller.Fragments.ClanEvents.BattleEventFragment BattleEvent => battleEvent;

        internal Controller.Fragments.Fragment GetFragmentWithKey(string key)
        {
            if (customFragmentsDictionary == null)
            {
                customFragmentsDictionary = new Dictionary<string, Controller.Fragments.Fragment>();
                for(int i = 0; i < customFragments.Length; ++i)
                {
                    var fwk = customFragments[i];
                    if (fwk.key == null || fwk.fragment == null)
                    {
                        Debug.LogError($"No key or custom fragment with index = {i}");
                        continue;
                    }

                    customFragmentsDictionary[fwk.key] = fwk.fragment;
                }
            }

            Fragment fragment;
            customFragmentsDictionary.TryGetValue(key, out fragment);
            return fragment;
        }

        [System.Serializable]
        private struct FragmentWithKey
        {
            public string key;
            public Controller.Fragments.Fragment fragment;
        }

    }

}