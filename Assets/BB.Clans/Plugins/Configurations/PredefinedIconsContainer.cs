﻿using BlackBears.GameCore.Configurations;
using UnityEngine;

namespace BlackBears.Clans
{

	[CreateAssetMenu(menuName = "BlackBears/Clans/Predefined Icons", fileName = "PredefinedIcons")]
    public class PredefinedIconsContainer : ScriptableObject
    {

		[SerializeField] private SpritePack defaultIcon;
		[SerializeField] private SpritePack[] icons;

        public int Count { get { return icons.Length; } }

		internal SpritePack DefaultIcon => defaultIcon;

        internal SpritePack GetIconByIndexOrDefault(int index)
		{
			if (icons == null) return DefaultIcon;
			return (index >= 0 &&  index < icons.Length) ? icons[index] : DefaultIcon;
		}

    }

}