﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Clans
{

    [CreateAssetMenu(menuName = "BlackBears/Clans/Countries", fileName = "Countries")]
    internal class CountriesContainer : ScriptableObject
    {

        [SerializeField] private string anyCountry = "any";
        [SerializeField] private string defaultCountry = "unk";
        [SerializeField] private string defaultCountryCodeName = "bb";
        [SerializeField] private Sprite[] countries;

        internal String AnyCountry { get { return anyCountry; } }
        internal String DefaultCountry { get { return defaultCountry; } }
        // Список сортируется при запуске игры, поэтому индекс всегда равен 1.
        internal int DefaultCountryIndex { get { return 1; } }
        internal int Count { get { return countries.Length; } }

        internal Sprite this[int index] { get { return countries[index]; } }
        internal Sprite this[string country] { get { return this[GetCountryIndex(country)]; } }

        private void OnEnable()
        {
            Array.Sort(countries, SpriteByNameComparison);
        }


        internal string GetCountryCodeName(string name)
        {
            if (string.Equals(defaultCountry, name)) return defaultCountryCodeName;
            return name;
        }

        internal bool IsAnyCountryFlag(string country) { return string.Equals(AnyCountry, country); }

        internal int GetIndexOffset(bool needAnyCountry) { return needAnyCountry ? 0 : 1; }

        internal int GetCountryIndex(string country) { return GetCountryIndex(countries, country); }

        internal int GetCountryIndex(Sprite[] countries, string country)
        {
            if (countries == null || country == null) return -1;

            country = country.ToLowerInvariant();
            for (int i = 0; i < countries.Length; i++)
            {
                if (string.Equals(countries[i].name, country)) return i;
            }
            return -1;
        }

        internal Sprite[] GetCountriesSorted(bool needAnyCountry, string[] importantCountries = null)
        {
            int countriesCount = countries.Length;
            if (!needAnyCountry) countriesCount -= 1;

            Sprite[] result = new Sprite[countriesCount];
            Array.Copy(countries, countries.Length - countriesCount, result, 0, countriesCount);

            if (importantCountries != null && importantCountries.Length > 0)
            {
                Array.Sort(result, (x, y) =>
                {
                    if (x == null || y == null) return SpriteByNameComparison(x, y);
                    if (string.Equals(anyCountry, x.name)) return -1;
                    if (string.Equals(anyCountry, y.name)) return 1;
                    if (string.Equals(defaultCountry, x.name)) return -1;
                    if (string.Equals(defaultCountry, y.name)) return 1;

                    for (int i = 0; i < importantCountries.Length; i++)
                    {
                        if (importantCountries[i] == null) continue;
                        if (string.Equals(importantCountries[i], x.name)) return -1;
                        if (string.Equals(importantCountries[i], y.name)) return 1;
                    }
                    return string.Compare(x.name, y.name);
                });
            }

            return countries;
        }

        private int SpriteByNameComparison(Sprite x, Sprite y)
        {
            if (x == null || y == null)
            {
                if (x == y) return 0;
                if (x == null) return 1;
                return -1;
            }

            if (string.Equals(anyCountry, x.name)) return -1;
            if (string.Equals(anyCountry, y.name)) return 1;
            if (string.Equals(defaultCountry, x.name)) return -1;
            if (string.Equals(defaultCountry, y.name)) return 1;
            return string.Compare(x.name, y.name);
        }

    }

}