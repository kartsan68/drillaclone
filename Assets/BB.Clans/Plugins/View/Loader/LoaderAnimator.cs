﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Clans.View.Loader
{

	[RequireComponent(typeof(RectTransform), typeof(CanvasGroup))]
	public class LoaderAnimator : MonoBehaviour 
	{

		[SerializeField] private RectTransform glareRect;

		[SerializeField] private float glareOffsetX = 43f;
		[SerializeField] private float containerOffsetY = -2f;

		private RectTransform rectTransform;
		private CanvasGroup canvasGroup;

		private Vector2 glareAnchoredPos;
		private Vector2 containerAnchoredPos;

		private bool started;
		private int transitionId = -1;

		private RectTransform RectTransform 
		{  
			get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); }
		}
		private CanvasGroup CanvasGroup
		{
			get { return canvasGroup ?? (canvasGroup = GetComponent<CanvasGroup>()); }
		}		

		private void Start()
		{
			glareAnchoredPos = glareRect.anchoredPosition;
			containerAnchoredPos = RectTransform.anchoredPosition;
            StartAnimate();

            started = true;
        }

        internal void Show(bool force = false)
        {
			if (transitionId >= 0) 
			{
				LeanTween.cancel(transitionId);
				transitionId = -1;
			}
			gameObject.SetActive(true);

            if (force)
			{
				CanvasGroup.alpha = 1f;
			}
			else
			{
				transitionId = LeanTween.alphaCanvas(CanvasGroup, 1f, 0.2f)
					.setOnComplete(() => transitionId = -1).uniqueId;
			}
        }

        internal void Hide(bool force = false)
        {
			if (transitionId >= 0)
			{
				LeanTween.cancel(transitionId);
				transitionId = -1;
			}

			if (force)
			{
				gameObject.SetActive(false);
				CanvasGroup.alpha = 0f;
			}
			else
			{
				transitionId = LeanTween.alphaCanvas(CanvasGroup, 0f, 0.2f)
					.setOnComplete(() => {
						gameObject.SetActive(false);
						transitionId = -1;
					}).uniqueId;
			}
        }

        private void OnEnable()
        {
            if (started) StartAnimate();
        }

        private void OnDisable()
        {
			CancelAnimations();
        }

		private void OnDestroy()
		{
			CancelAnimations();	
		}

		private void StartAnimate()
		{
			var containerNewPos = new Vector2(containerAnchoredPos.x, containerAnchoredPos.y - containerOffsetY);
			LeanTween.moveAnchored(RectTransform, containerNewPos, 0.3f);
			LeanTween.moveAnchoredX(glareRect, glareRect.anchoredPosition.x + glareOffsetX, 0.4f).setEaseOutQuad()
					 .setOnComplete(EndAnimate);
		}

        private void EndAnimate()
		{
			LeanTween.moveAnchored(RectTransform, containerAnchoredPos, 0.3f).setEaseInOutQuad();
			LeanTween.delayedCall(glareRect.gameObject, 0.5f, () =>
			{
				glareRect.anchoredPosition = glareAnchoredPos;
				StartAnimate();
			});
		}

		private void CancelAnimations()
		{
			LeanTween.cancel(RectTransform);
			LeanTween.cancel(glareRect);
			transitionId = -1;
		}

    }

}