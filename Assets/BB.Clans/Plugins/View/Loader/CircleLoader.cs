﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.View.Loader
{

    [RequireComponent(typeof(Image), typeof(RectTransform))]
    internal class CircleLoader : MonoBehaviour
    {

        private enum LoaderState { Hided, Showing, Showed, Hiding };

        private Image _image;
        private RectTransform _rectTransform;

        private LoaderState state;
        private bool waitForShowing;
        private bool waitForHiding;

        private UnityEvent onShow = new UnityEvent();
        private UnityEvent onHide = new UnityEvent();

        internal UnityEvent OnShow { get { return onShow; } }
        internal UnityEvent OnHide { get { return onHide; } }

        internal bool IsWork { get { return state != LoaderState.Hided; } }

        private Image Image { get { return _image ?? (_image = GetComponent<Image>()); } }
        private RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void Show()
        {
            if (state == LoaderState.Hided)
            {
                StartShow();
            }
            else if (state == LoaderState.Hiding)
            {
                waitForShowing = true;
                waitForHiding = false;
            }
        }

        internal void Hide()
        {
            if (state == LoaderState.Showed)
            {
                StartHide();
            }
            else if (state == LoaderState.Showing)
            {
                waitForHiding = true;
                waitForShowing = false;
            }
        }

        private void StartShow()
        {
            waitForShowing = false;
            state = LoaderState.Showing;
            LeanTween.alphaGraphic(Image, 1f, 0.3f);
            LeanTween.rotateAroundLocal(RectTransform, Vector3.back, 180, 0.3f)
                .setEaseInQuad()
                .setOnComplete(ProcessShow);
        }

        private void ProcessShow()
        {
            LeanTween.rotateAround(RectTransform, Vector3.back, 360, 0.3f)
                .setOnComplete(CheckForRotate);
        }

        private void CheckForRotate()
        {
            if (state == LoaderState.Showing)
            {
                state = LoaderState.Showed;
                onShow.Invoke();
            }

            if (!waitForHiding && state == LoaderState.Showed) ProcessShow();
            else if (waitForHiding) StartHide();
        }

        private void StartHide()
        {
            waitForHiding = false;
            state = LoaderState.Hiding;

            LeanTween.rotateAroundLocal(RectTransform, Vector3.back, 180, 0.3f)
                .setEaseOutQuad();
            LeanTween.alphaGraphic(Image, 0, 0.3f)
                .setDelay(0.3f)
                .setOnComplete(OnHideComplete);
        }

        private void OnHideComplete()
        {
            state = LoaderState.Hided;
            if (waitForShowing) StartShow();
            onHide.Invoke();
        }

    }

}