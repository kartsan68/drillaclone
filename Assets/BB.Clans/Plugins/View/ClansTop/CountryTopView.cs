using BlackBears.Clans.Model;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.ClanTop
{

    internal class CountryTopView : MonoBehaviour
    {

        [SerializeField] private Image image;
        [SerializeField] private Text nameCountry;

        internal Sprite FlagSprite
        {
            get { return image.sprite; }
            private set { image.sprite = value; }
        }

        internal void SetView(Sprite sprite, string name = null)
        {
            FlagSprite = sprite;
            if(name != null) nameCountry.text = name;
        }

    }

}