﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.Common
{

    internal class RollableItem : MonoBehaviour
    {

        [SerializeField] private LayoutElement layoutElement;
        [SerializeField] private Button rolldownButton;
        [SerializeField] private CanvasGroup rolldownCanvas;
        [SerializeField] private Button rollupButton;
        [SerializeField] private float unrolledHeight;
        [SerializeField] private RectTransform content;

        private LayoutGroup contentLayoutGroup;

        private bool isRollup = true;
        private RectTransform rectTransform;

        internal bool IsRollup
        {
            get { return isRollup; }
        }

        private RectTransform RectTransform { get { return rectTransform ?? (rectTransform = GetComponent<RectTransform>()); } }

        private void Awake()
        {
            contentLayoutGroup = content.GetComponent<LayoutGroup>();

            rolldownButton.onClick.AddListener(() => SetRollup(false, true));
            if (rollupButton != null) rollupButton.onClick.AddListener(() => SetRollup(true, true));
        }

        private void OnDisable()
        {
            CancelTweenAnimations();
        }

        public void SetRollup(bool isRollup, bool force = false)
        {
            if (this.isRollup == isRollup && !force) return;
            this.isRollup = isRollup;
            if (isRollup) Rollup();
            else Rolldown();
        }

        protected virtual void Rolldown()
        {
            CancelTweenAnimations();
            LeanTween.alphaCanvas(rolldownCanvas, 0f, 0.1f).setOnComplete(() =>
            {
                rolldownButton.gameObject.SetActive(false);
            });
            LeanTween.sizeMin(layoutElement, new Vector2(RectTransform.sizeDelta.x, contentLayoutGroup.preferredHeight), 0.3f)
                     .setEaseOutQuad();
        }

        protected virtual void Rollup()
        {
            CancelTweenAnimations();

            rolldownButton.gameObject.SetActive(true);
            LeanTween.alphaCanvas(rolldownCanvas, 1f, 0.3f).setDelay(0.15f).setEaseOutQuad();
            LeanTween.sizeMin(layoutElement, new Vector2(RectTransform.sizeDelta.x, unrolledHeight), 0.3f)
                     .setEaseOutQuad();
        }

        protected virtual void CancelTweenAnimations()
        {
            LeanTween.cancel(layoutElement.gameObject);
            LeanTween.cancel(rolldownCanvas.gameObject);
        }

    }

}