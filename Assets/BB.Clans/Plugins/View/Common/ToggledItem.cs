﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.Common
{

	public class ToggledItem : MonoBehaviour 
	{

		[SerializeField] private Toggle toggle;
		[SerializeField] private GameObject item;
		[SerializeField] private bool isInverse = false;

		private void Awake()
		{
			toggle.onValueChanged.AddListener(OnToggleValueChaged);
		}

		private void OnToggleValueChaged(bool isOn)
		{
			bool activity = isInverse ? !isOn : isOn;
			item.SetActive(activity);
		}

	}

}