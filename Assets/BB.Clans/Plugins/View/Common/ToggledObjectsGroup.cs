﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine;

namespace BlackBears.Clans.View.Common
{

	public class ToggledObjectsGroup : MonoBehaviour 
	{

		[SerializeField] private Toggle toggle;
		[SerializeField] private List<GameObject> toggledObjects = new List<GameObject>();

		private void Awake()
		{
			toggle.onValueChanged.AddListener(OnToggleValueChaged);
		}

		private void OnToggleValueChaged(bool isOn)
		{
			foreach (var toggledObject in toggledObjects)
			{
				toggledObject.SetActive(isOn);
			}
		}

	}

}