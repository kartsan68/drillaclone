﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine;
using BlackBears.Utils;

namespace BlackBears.Clans.View.Common
{

	public class ToggledLabel : MonoBehaviour 
	{

		[SerializeField] private Color inactiveColor = ColorUtils.FromHex(0x382E28FF);
        [SerializeField] private Color activeColor = ColorUtils.FromHex(0x8D2109FF);

		[SerializeField] private Toggle toggle;
		[SerializeField] private Text textLabel;

		private void Awake()
		{
			toggle.onValueChanged.AddListener(OnToggleValueChanged);
		}

		private void OnToggleValueChanged(bool isOn)
		{
			textLabel.color = isOn ? activeColor : inactiveColor;
		}

	}

}