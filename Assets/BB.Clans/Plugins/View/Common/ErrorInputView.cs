using System.Collections;
using System.Collections.Generic;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.Common
{

	public class ErrorInputView : MonoBehaviour 
	{

		[SerializeField] private Text title;
		[SerializeField] private Color titleNormalColor = ColorUtils.FromHex(0xFFFFFFFF);
		[SerializeField] private Color titleErrorColor = ColorUtils.FromHex(0x8D2109FF);

		[SerializeField] private StateExtendedInputField inputField;

		[SerializeField] private GameObject errorTextContainer;
		[SerializeField] private Text errorText;

		private bool isError;
		private string errorMessage;

		public bool IsError 
		{
			get { return isError; }
			set
			{
				if (value == isError) return;
				isError = value;
				UpdateValues();
			}
		}

		public string ErrorMessage
		{
			get { return errorMessage; }
			set
			{
				if (value == errorMessage) return;
				errorMessage = value;
				errorText.text = errorMessage;
			}
		}

		private void UpdateValues()
		{
			title.color = isError ? titleErrorColor : titleNormalColor;
			inputField.IsOnError = isError;
			errorTextContainer.SetActive(isError);
		}

	}

}
