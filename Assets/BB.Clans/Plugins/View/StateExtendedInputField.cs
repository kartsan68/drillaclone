﻿using BlackBears.GameCore.Graphic;
using BlackBears.Resolutor;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackBears.Clans.View
{

    internal sealed class StateExtendedInputField : InputField
    {

        private enum State { undefined = -1, empty, filled, error, active }

        [SerializeField] private string emptyFieldBgSpriteName;
        [SerializeField] private string filledFieldBgSpriteName;
        [SerializeField] private string errorFieldBgSpriteName;
        [SerializeField] private string activeFieldBgSpriteName;

        [SerializeField] private Color emptyFieldColor = ColorUtils.FromHex(0xF9F9F9FF);
        [SerializeField] private Color filledFieldColor = Color.white;
        [SerializeField] private Color errorFieldColor = Color.white;
        [SerializeField] private Color activeFieldColor = Color.white;

        [SerializeField] private Color emptyHintTextColor = ColorUtils.FromHex(0xACB6B3FF);
        [SerializeField] private Color filledHintTextColor = ColorUtils.FromHex(0xACB6B3FF);
        [SerializeField] private Color errorHintTextColor = ColorUtils.FromHex(0x8D2109FF);
        [SerializeField] private Color activeHintTextColor = ColorUtils.FromHex(0xACB6B3FF);

        private bool isOnError = false;
        private State currentState = State.undefined;

		private Atlas atlas;

        public bool IsOnError
        {
            get { return isOnError; }
            set
            {
                if (isOnError == value) return;
                isOnError = value;
                UpdateVisualState();
            }
        }

        protected override void Start()
        {
			atlas = UICache.Instance.TakeAtlas(CoreCache.key);
			
            base.Start();
            UpdateVisualState();
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);
            UpdateVisualState();
        }


        internal void UpdateVisualState()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
			if (atlas == null) return;

            var newState = default(State);
            if (IsOnError) newState = State.error;
            else if (isFocused) newState = State.active;
            else if (string.IsNullOrEmpty(text)) newState = State.empty;
            else newState = State.filled;

            if (currentState == newState) return;
            currentState = newState;
            switch (currentState)
            {
                case State.filled: SetFilledState(); break;
                case State.active: SetActiveState(); break;
                case State.empty: SetEmptyState(); break;
                case State.error: SetErrorState(); break;
            }
        }

        private void SetEmptyState() { SetState(emptyFieldBgSpriteName, emptyFieldColor, emptyHintTextColor); }
        private void SetFilledState() { SetState(filledFieldBgSpriteName, filledFieldColor, filledHintTextColor); }
        private void SetActiveState() { SetState(activeFieldBgSpriteName, activeFieldColor, activeHintTextColor); }
        private void SetErrorState() { SetState(errorFieldBgSpriteName, errorFieldColor, errorHintTextColor); }

        private void SetState(string bgName, Color bgColor, Color hintColor)
        {
			var img = image;
            if (img == null) return;
			if (atlas != null) img.sprite = atlas[bgName];
            img.color = bgColor;
			placeholder.color = hintColor;
        }

    }

}