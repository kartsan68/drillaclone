﻿using System;
using BlackBears.Clans.Model;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.Icons
{

	[RequireComponent(typeof(Image))]
    internal class PredefinedIconView : MonoBehaviour
    {

		[SerializeField] private bool useBigIcon;

		private bool disposed = false;

        internal Color Color
		{
			get { return Image.color; }
			set { Image.color = value; }
		}

		private Image m_image;
        private Image Image { get { return m_image ?? (m_image = GetComponent<Image>()); } }

		private void OnDestroy() => disposed = true;

		internal void SetIcon(Icon icon, PredefinedIconsContainer container)
		{
			if (disposed) return;
			
			var predefined = icon as PredefinedIcon;
			if (predefined == null || container == null)
			{
				Image.enabled = false;
			}
			else
			{
				var iconData = container.GetIconByIndexOrDefault(predefined.Id);
				Image.sprite = useBigIcon ? iconData.BigIcon : iconData.SmallIcon;
				Image.enabled = true;
			}
		}

        internal void ClearIcon() => SetIcon(null, null);
    }

}