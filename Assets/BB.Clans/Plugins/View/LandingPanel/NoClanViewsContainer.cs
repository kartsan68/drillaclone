using BlackBears.Clans.View.Icons;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.LandingPanel
{
    
    internal class NoClanViewsContainer : MonoBehaviour
    {

        [SerializeField] internal Text playerName;
        [SerializeField] internal Text kickerName;
        [SerializeField] internal Text clanName;
        [SerializeField] internal PredefinedIconView clanIcon;

    }

}