﻿using System.Collections;
using UnityEngine;

namespace BlackBears.Clans.View.LandingPanel
{

    internal class ProfileIconView : MonoBehaviour
    {

		[SerializeField] private CanvasGroup iconGroup;
		[SerializeField] private CanvasGroup gearGroup;
		[SerializeField] private RectTransform gearTransform;

		public float pauseTime = 2f;
		public float gearTime = 0.4f;
		public float switchTime = 0.2f;

		private void Start()
		{
			gearTime = Mathf.Min(gearTime, pauseTime);
			switchTime = Mathf.Min(switchTime, pauseTime);

			iconGroup.alpha = 1f;
			gearGroup.alpha = 0f;
			StartCoroutine(Animate());
		}

		private void OnDestroy()
		{
			LeanTween.cancel(iconGroup.gameObject);
			LeanTween.cancel(gearGroup.gameObject);
			LeanTween.cancel(gearTransform.gameObject);
		}

		private IEnumerator Animate()
		{
			var pauseInstruction = new WaitForSeconds(pauseTime);
			while(true)
			{
				yield return pauseInstruction;
				LeanTween.alphaCanvas(iconGroup, 0, switchTime);
				LeanTween.alphaCanvas(gearGroup, 1, switchTime);
				gearTransform.localRotation = Quaternion.Euler(Vector3.zero);
				LeanTween.rotateAroundLocal(gearTransform.gameObject, Vector3.back, 90, gearTime)
					.setEaseOutBack();
				yield return pauseInstruction;
				LeanTween.alphaCanvas(iconGroup, 1, switchTime);
				LeanTween.alphaCanvas(gearGroup, 0, switchTime);
			}
		}

    }

}