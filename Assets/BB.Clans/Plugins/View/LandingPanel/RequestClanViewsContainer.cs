﻿using BlackBears.Clans.Components;
using BlackBears.Clans.Controller.Common;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.LandingPanel
{

    internal class RequestClanViewsContainer : MonoBehaviour
    {
        [SerializeField] internal Text clanName;
        [SerializeField] internal PlayerClanIconController clanIcon;
        [SerializeField] internal PullToRefresh pullToRefresh;

    }

}