﻿using UnityEngine;

namespace BlackBears.Clans.View.ClanHelp
{

	[RequireComponent(typeof(RectTransform))]
    internal class NewMessagesLineAnimator : MonoBehaviour
    {

		[SerializeField] private float speed = 100f;

		private RectTransform rt;

		private void Awake()
		{
			rt = GetComponent<RectTransform>();
		}

		private void Update()
		{
			float x = -Mathf.Repeat(-rt.anchoredPosition.x + Time.deltaTime * speed, rt.rect.width * 0.5f);
			rt.anchoredPosition = new Vector2(x, rt.anchoredPosition.y);
		}

    }

}