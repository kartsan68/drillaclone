using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View
{

    internal class FlagView : MonoBehaviour
    {

        [SerializeField] private Image image;

        internal Sprite Sprite
        {
            get { return image.sprite; }
            set { image.sprite = value; }
        }

    }

}