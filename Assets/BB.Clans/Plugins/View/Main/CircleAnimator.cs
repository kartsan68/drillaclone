using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.View.Main
{
	[RequireComponent(typeof(Image), typeof(RectTransform))]
	public class CircleAnimator : MonoBehaviour 
	{

		[SerializeField] private float startScale = 1.0f;
		[SerializeField] private float startAlpha = 0.0f;
		[SerializeField] private float finishScale = 0.4f;
		[SerializeField] private float finishAlpha = 1.0f;
		[SerializeField] private float animTime = 2.0f;
		[SerializeField] private float startDelay = 0.0f;

		private Image image;
		private RectTransform rectTransform;

		private bool started;

		private RectTransform RectTransform
		{
			get
			{
				if (rectTransform == null) rectTransform = GetComponent<RectTransform>();
				return rectTransform;
			}
		}

		private Image Image
		{
			get
			{
				if (image == null) image = GetComponent<Image>();
				return image;
			}
		}
		

		private void Start()
		{
			StartAnimate();
			started = true;
		}

		private void OnEnable()
		{
			if (started) StartAnimate();			
		}

		private void OnDisable()
		{
			LeanTween.cancel(gameObject);
		}
		private void OnDestroy()
		{
			LeanTween.cancel(gameObject);
		}

		private void StartAnimate()
		{
			var newScale = new Vector3(finishScale, finishScale, 1.0f);
			LeanTween.scale(gameObject, newScale, animTime).setDelay(startDelay).setOnComplete(EndAnimate);
			LeanTween.alphaGraphic(Image, finishAlpha, animTime).setDelay(startDelay);
		}

		private void EndAnimate()
		{
			startDelay = 0.0f;

			var color = Image.color;
			color.a = startAlpha;
			Image.color = color;

			var scale = RectTransform.localScale;
			scale.x = startScale;
			scale.y = startScale;
			RectTransform.localScale = scale;

			StartAnimate();
		}

	}

}
