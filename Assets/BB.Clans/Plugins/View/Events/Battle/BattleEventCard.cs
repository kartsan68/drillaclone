using System.Collections.Generic;
using BlackBears.Clans.Events;
using BlackBears.Clans.Events.ClanBattle;
using BlackBears.Clans.Model;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

using EventType = BlackBears.Clans.Events.EventType;

namespace BlackBears.Clans.View.Events
{

    public abstract class BattleEventCard : EventCard
    {

        protected enum CardState { Undefined, CountDown, WaitBattle, InBattle, EndingBattle, EndBattle, Close }

        private const float tickPeriod = 1f;
        private const int synchronizePeriod = 30;

        private float tickTimer = tickPeriod;
        private float synchronizeTimer = synchronizePeriod;
        private CardState state;

        private long battleId = -1;

        protected BattleEvent BattleEvent { get; private set; }
        protected CardState State
        {
            get { return state; }
            private set
            {
                if (state == value) return;
                state = value;
                OnStateChanged();
            }
        }

        protected ChronometerFeature Chronometer { get; private set; }

        protected override void OnInjectFinish()
        {
            base.OnInjectFinish();
            Chronometer = BBGC.Features.GetFeature<ChronometerFeature>();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (this.BattleEvent == null) return;
            this.BattleEvent.OnSynchronizeFinished -= OnEventSynchronized;
        }

        internal override bool UpdateCardWithEvent(ClanEvent e)
        {
            if (e?.EventType != EventType.Battle) return false;
            BattleEvent battleEvent = e as BattleEvent;

            if (battleId > 0)
            {
                if (battleEvent.Battle == null || battleEvent.Battle.id != battleId)
                {
                    return false;
                }
                battleId = battleEvent.Battle.id;
            }

            if (this.BattleEvent == null)
            {
                this.BattleEvent = battleEvent;
                this.BattleEvent.OnSynchronizeFinished += OnEventSynchronized;
            }

            State = DetectNewState();
            OnTick();
            LoadingRatingData(battleEvent);

            return true;
        }

        protected virtual void OnTick()
        {
            var newState = DetectNewState();
            if (State != newState)
            {
                if (newState != CardState.EndingBattle)
                {
                    BattleEvent?.Invalidate();
                    synchronizeTimer = synchronizePeriod;
                }
                else
                {
                    BattleEvent?.SynchronizeBattle(true);
                    synchronizeTimer = synchronizePeriod;
                }

                if (newState == CardState.Close || (State != CardState.Undefined && newState == CardState.CountDown))
                {
                    DestroyCard();
                    return;
                }

                State = newState;
                return;
            }
            synchronizeTimer -= 1;
            if (synchronizeTimer <= 0)
            {
                synchronizeTimer = synchronizePeriod;
                BattleEvent?.SynchronizeBattle(false);
            }
        }

        protected void OpenBattle() => FragmentsController.ShowBattleEvent();

        protected virtual void OnStateChanged() { }

        protected abstract void OnStartLoadingRatingData();
        protected abstract void SetupClansData(IList<BattleClanData> clansData);
        protected abstract void OnClanLoaded(int position, Clan clan, BattleClanData data);

        private void Update()
        {
            tickTimer -= Time.deltaTime;
            if (tickTimer > 0f) return;
            tickTimer = tickPeriod;
            OnTick();
        }

        private void OnEventSynchronized()
        {
            if (BattleEvent.IsSynchronizeSuccess)
            {
                LoadingRatingData(this.BattleEvent);
            }
            else
            {
                ShowError(() => BattleEvent.SynchronizeBattle(true));
            }
        }

        private void LoadingRatingData(BattleEvent be)
        {
            OnStartLoadingRatingData();

            var clansData = be.Battle?.ClansData;
            if (clansData == null) return;
            SetupClansData(clansData);
            for (int i = 0; i < clansData.Count; i++)
            {
                LoadClan(i + 1, clansData[i]);
            }
        }

        private CardState DetectNewState()
        {
            if (BattleEvent == null) return CardState.Undefined;

            var actualTime = Chronometer.ActualTime;
            var battle = BattleEvent.Battle;
            if (battle != null && battle.IsOnProcess(actualTime) && BattleEvent.HaveBattle) return CardState.InBattle;

            if (actualTime > BattleEvent.NextBattleTime)
            {
                return BattleEvent.HaveBattle ? CardState.Close : CardState.WaitBattle;
            }
            if (battle != null)
            {
                if (!battle.FinalBattleDataWasLoaded) return CardState.EndingBattle;
                if (!BattleEvent.IsBattleRewardTaken(battle.id)) return CardState.EndBattle;
                if (!BattleEvent.IsTopRewardTaken(battle.id) && BattleEvent.CanTakeTopReward()) return CardState.EndBattle;
            }
            if (BattleEvent.NextBattleTime > actualTime) return CardState.CountDown;
            return BattleEvent.HaveBattle ? CardState.Undefined : CardState.CountDown;
        }

        private void LoadClan(int position, BattleClanData data)
        {
            if (data == null) return;

            Block<Clan> clanLoadedCallback = clan => OnClanLoaded(position, clan, data);
            FailBlock clanLoadFailedCallback = (c, m) => UnityEngine.Debug.LogWarning($"LoadClan: {c}; {m}");

            //TODO: Загружать всё через кеш (else ветка). А это костыль.
            if (Core.State.IsUserClan(data.id))
            {
                OnClanLoaded(position, Core.State.Clan, data);
            }
            else
            {
                Core.State.ClanCache.LoadClanAsync(data.id, false, clanLoadedCallback, clanLoadFailedCallback);
            }
        }



    }

}