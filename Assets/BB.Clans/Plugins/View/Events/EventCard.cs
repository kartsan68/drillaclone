using System;
using BlackBears.Clans.Controller;
using BlackBears.Clans.Controller.Fragments;
using BlackBears.Clans.Events;
using UnityEngine;

namespace BlackBears.Clans.View.Events
{

    public abstract class EventCard : MonoBehaviour
    {

        private EventCardsContainer cardsContainer;
        private bool disposed;

        internal FragmentsController FragmentsController { get; private set; }

        protected ClanCore Core { get; private set; }
        protected ClanFullSettings Settings { get; private set; }

        protected bool Disposed => disposed;

        internal event Block<Block> OnError;

        internal void Inject(EventCardsContainer cardsContainer, ClanCore core, 
            ClanFullSettings settings, FragmentsController fragmentsController)
        {
            this.cardsContainer = cardsContainer;
            this.Core = core;
            this.Settings = settings;
            this.FragmentsController = fragmentsController;

            OnInjectFinish();
        }

        protected virtual void Awake() {}

        protected virtual void OnDestroy()
        {
            disposed = true;
            OnError = null;
        }

        protected void DestroyCard()
        {
            if (disposed) return;

            if (cardsContainer.IsCurrentCard(this)) 
                cardsContainer.DestroyCurrentCard();
            else
                Destroy(this.gameObject);
        }        

        protected void ShowError(Block repeatBlock) => OnError.SafeInvoke(repeatBlock);

        protected virtual void OnInjectFinish() {}

        internal abstract void ExternalCheck();
        internal abstract bool UpdateCardWithEvent(ClanEvent e);

    }

}