﻿using UnityEngine;

namespace BlackBears.Clans.Components
{

    internal class BBClansRotator : MonoBehaviour
    {

		public float speed = 270f;

        void Update()
        {
			transform.Rotate(Vector3.back, speed * Time.deltaTime);
        }

    }

}