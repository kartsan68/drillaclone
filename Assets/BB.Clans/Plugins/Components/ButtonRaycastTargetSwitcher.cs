﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Components
{

	[RequireComponent(typeof(Button), typeof(Graphic))]
    internal class ButtonRaycastTargetSwitcher : MonoBehaviour
    {

		private Button button;
		private Graphic graphic;
		private bool interactable = false;
		private bool forceUdpate = true;

		private void Awake()
		{
			button = GetComponent<Button>();
			graphic = GetComponent<Graphic>();
		}

		private void Update()
		{
			if (button.interactable != interactable || forceUdpate)
			{
				forceUdpate = false;
				interactable = button.interactable;
				graphic.raycastTarget = interactable;
			}
		}

    }

}