﻿using UnityEngine;

namespace BlackBears.Clans.Components
{

    internal abstract class PullToRefreshLoader : MonoBehaviour
    {

        internal abstract void SetRefreshReady(float value);
        internal abstract void OnStartRefresh();
        internal abstract void OnFinishRefresh();

    }

}