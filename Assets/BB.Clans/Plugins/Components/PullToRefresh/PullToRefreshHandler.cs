using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BlackBears.Clans.Components
{
    internal class PullToRefreshHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,
        IBeginDragHandler, IEndDragHandler
    {

        private UnityEvent onPointerDown = new UnityEvent();
        private UnityEvent onPointerUp = new UnityEvent();
        private UnityEvent onBeginDrag = new UnityEvent();
        private UnityEvent onEndDrag = new UnityEvent();

        internal UnityEvent OnPointerDown => onPointerDown;
        internal UnityEvent OnPointerUp => onPointerUp;
        internal UnityEvent OnBeginDrag => onBeginDrag;
        internal UnityEvent OnEndDrag => onEndDrag;

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData) => onPointerDown.Invoke();
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData) => onPointerUp.Invoke();
        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) => onBeginDrag.Invoke();
        void IEndDragHandler.OnEndDrag(PointerEventData eventData) => onEndDrag.Invoke();

    }
}