using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Clans.Components
{

    internal class PullToRefresh : MonoBehaviour
    {

        private enum RefreshState { Default, OnRefresh, Refreshed }

        private const float baseReloadOffset = 75;

        [SerializeField] private float reloadOffset = baseReloadOffset;
        [SerializeField] private ScrollRect scroll;

        private PullToRefreshLoader loader;

        private RefreshState state;
        private bool started;
        private bool pressed;

        private UnityEvent onRefreshStarted = new UnityEvent();

        internal ScrollRect Scroll
        {
            get { return scroll; }
            set
            {
                ResetScroll();
                scroll = value;
                if (started) PrepareScroll();
            }
        }

        internal bool OnRefresh { get { return state == RefreshState.OnRefresh; } }

        internal UnityEvent OnRefreshStarted { get { return onRefreshStarted; } }

        private void Start()
        {
            started = true;
            if (Scroll != null) PrepareScroll();
        }

        internal void FinishRefresh()
        {
            if (!OnRefresh) return;
            if (pressed) state = RefreshState.Refreshed;
            else state = RefreshState.Default;
            if (loader != null) loader.OnFinishRefresh();
        }

        private void PrepareScroll()
        {
            if (scroll == null) return;

            scroll.onValueChanged.AddListener(OnScroll);
            var handler = scroll.gameObject.AddComponent<PullToRefreshHandler>();
            handler.OnPointerDown.AddListener(OnPressDown);
            handler.OnBeginDrag.AddListener(OnPressDown);
            handler.OnPointerUp.AddListener(OnPressUp);
            handler.OnEndDrag.AddListener(OnPressUp);
            loader = scroll.GetComponent<PullToRefreshLoader>();
        }

        private void ResetScroll()
        {
            if (scroll == null) return;

            scroll.onValueChanged.RemoveListener(OnScroll);
            var handler = scroll.gameObject.GetComponent<PullToRefreshHandler>();
            if (handler != null) Destroy(handler);
            if (loader != null) loader.OnFinishRefresh();
            loader = null;
        }

        private void OnScroll(Vector2 value)
        {
            if (!pressed || state != RefreshState.Default) return;

            var contentPos = Scroll.content.anchoredPosition.y;
            if (loader != null)
            {
                float reloadValue = 0f;
                if (reloadOffset != 0f) 
                {
                    reloadValue = Mathf.Clamp01((contentPos - reloadOffset / 2) / reloadOffset * 2);
                }
                loader.SetRefreshReady(reloadValue);
            }

            if (contentPos < reloadOffset) return;
            state = RefreshState.OnRefresh;
            onRefreshStarted.Invoke();
            if (loader != null) loader.OnStartRefresh();
        }

        private void OnPressDown()
        {
            pressed = true;
        }

        private void OnPressUp()
        {
            if (!pressed) return;

            pressed = false;
            if (state == RefreshState.Refreshed) state = RefreshState.Default;
            if (loader != null) loader.SetRefreshReady(0f);
        }

    }

}