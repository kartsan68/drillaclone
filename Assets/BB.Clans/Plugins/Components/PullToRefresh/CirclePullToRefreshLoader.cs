using BlackBears.Clans.View.Loader;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Components
{
    internal class CirclePullToRefreshLoader : PullToRefreshLoader
    {

        [SerializeField] private ScrollRect scroll;
        [SerializeField] private CircleLoader loader;
        [SerializeField] private Image loaderImage;

        private void Start()
        {
            loader.OnHide.AddListener(EnableScroll);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(scroll.content.gameObject);
        }

        internal override void SetRefreshReady(float value)
        {
            if (loader.IsWork) return;
            var c = loaderImage.color;
            c.a = value;
            loaderImage.color = c;
        }

        internal override void OnStartRefresh()
        {
            loader.Show();
            scroll.enabled = false;
        }

        internal override void OnFinishRefresh() { loader.Hide(); }

        private void EnableScroll() 
        { 
            scroll.enabled = true; 
            LeanTween.moveAnchored(scroll.content, Vector2.zero, 0.2f).setEaseOutQuad();
        }

    }
}