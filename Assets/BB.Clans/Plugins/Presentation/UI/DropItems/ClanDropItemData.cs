using UnityEngine;
using BlackBears.GameCore.Features.DropItemSystem;

namespace BlackBears.Clans.Presentation.UI.DropItems
{

    public abstract class ClanDropItemData : DropItemData
    {

        protected ClanDropItemData(ClanDropItemType itemType, Vector3 globalPos, Camera targetCamera = null,
            float separationRadius = 0, float pauseBetweenItems = -1)
            : base(BlackBears.Keys.dropSystemTagKey, globalPos, targetCamera, separationRadius, pauseBetweenItems)
        {
            this.DropType = itemType;
        }

        public ClanDropItemType DropType { get; private set; }
    }

    public sealed class SoftCurrencyClanDropItemData : ClanDropItemData
    {

        private const int minItemsCount = 5;
        private const int maxItemsCount = 10;

        public SoftCurrencyClanDropItemData(Vector3 globalPos, Camera targetCamera = null)
            : base(ClanDropItemType.SoftCurrency, globalPos, targetCamera) { }

        public override int MinItemsCount { get { return minItemsCount; } }
        public override int MaxItemsCount { get { return maxItemsCount; } }

    }

    public sealed class HardCurrencyClanDropItemData : ClanDropItemData
    {

        private const int minItemsCount = 5;
        private const int maxItemsCount = 10;

        public HardCurrencyClanDropItemData(Vector3 globalPos, Camera targetCamera = null)
            : base(ClanDropItemType.HardCurrency, globalPos, targetCamera) { }

        public override int MinItemsCount { get { return minItemsCount; } }
        public override int MaxItemsCount { get { return maxItemsCount; } }

    }

}