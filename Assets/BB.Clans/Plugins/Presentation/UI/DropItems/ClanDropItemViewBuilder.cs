using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.DropItemSystem;
using BlackBears.GameCore.Configurations;
using BlackBears.Utils.Scaling;

namespace BlackBears.Clans.Presentation.UI.DropItems
{

    /// <summary>
    /// Должен включаться при инициализации ClansFeature
    /// </summary>
    internal class ClanDropItemViewBuilder : DropItemViewBuilder
    {

        [SerializeField] private string dropTag;
        [SerializeField] private RectTransform parent;

        private SpritePack softCurrencyIconPack;
        private SpritePack hardCurrencyIconPack;

        public override string DropTag { get { return dropTag; } }
        public override RectTransform Parent { get { return parent; } }

        protected override void Start()
        {
            dropTag = Keys.dropSystemTagKey;
            softCurrencyIconPack = BBGC.Features.CoreDefaults().SoftCurrencyIconPack;
            hardCurrencyIconPack = BBGC.Features.CoreDefaults().HardCurrencyIconPack;

            base.Start();
        }

        internal override DropItemView TakeItem(DropItemData itemData, RectTransform parent = null)
        {
            var clanItemData = itemData as ClanDropItemData;
            if (clanItemData == null) return null;

            DropItemView itemView;
            if (viewPool.Count == 0) itemView = GenerateNewItem(clanItemData, parent);
            else itemView = viewPool.Dequeue();
            if(itemView == null) return null;

            itemView.gameObject.SetActive(true);

            var sprite = TakeSprite(clanItemData);
            itemView.mainImage.sprite = sprite;

            itemView.mainImage.SetNativeSize();
            itemView.rectTransform.sizeDelta = new Vector2(sprite.rect.width / sprite.pixelsPerUnit,
            sprite.rect.height / sprite.pixelsPerUnit);
            itemView.mainImage.raycastTarget = false;

            itemView.mainImage.color = Color.white;
            itemView.canvasGroup.alpha = 0;

            return itemView;
        }

        internal DropItemView GenerateNewItem(ClanDropItemData itemData, RectTransform parent = null)
        {
            var go = new GameObject("DropItem");
            var rt = go.AddComponent<RectTransform>();
            rt.SetParent(parent ?? this.parent);
            rt.localScale = Vector3.one;

            var canvasGroup = go.AddComponent<CanvasGroup>();

            var mainIconGO = new GameObject("Main");
            var mainIconRT = mainIconGO.AddComponent<RectTransform>();
            var mainIconImage = mainIconGO.AddComponent<Image>();
            mainIconRT.SetParent(rt, false);

            return new DropItemView(go, rt, mainIconImage, canvasGroup);
        }

        private Sprite TakeSprite(ClanDropItemData itemData)
        {
            if (softCurrencyIconPack == null || hardCurrencyIconPack == null) return null;

            switch (itemData.DropType)
            {
                case ClanDropItemType.SoftCurrency: return softCurrencyIconPack.SmallIcon;
                case ClanDropItemType.HardCurrency: return hardCurrencyIconPack.SmallIcon;
            }

            return null;
        }

    }

}