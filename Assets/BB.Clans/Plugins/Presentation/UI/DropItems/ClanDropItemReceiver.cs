using BlackBears.GameCore.Features.DropItemSystem;
using UnityEngine;

namespace BlackBears.Clans.Presentation.UI.DropItems
{

    public class ClanDropItemReceiver : DropItemReceiver
    {

        [SerializeField] private ClanDropItemType dropType;
        [SerializeField] private string dropTag = Keys.dropSystemTagKey;

        public ClanDropItemType DropType { get { return dropType; } }
        public override string DropTag { get { return dropTag; } }

        public override bool TypeEquals(DropItemData itemData)
        {
            var clanItemData = itemData as ClanDropItemData;
            return (clanItemData != null && clanItemData.DropType == this.DropType);
        }

    }

}
