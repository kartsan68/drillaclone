using UnityEngine;

namespace BlackBears.Clans.ViewModel.ContextMenu
{

    internal class MenuItem 
    {
        internal readonly Block onSelect;

        protected MenuItem(Block onSelect)
        {
            this.onSelect = onSelect;
        }
    }

    internal sealed class IconTextMenuItem : MenuItem
    {
        internal readonly Sprite icon;
        internal readonly string text;
        internal readonly Color textColor;

        internal IconTextMenuItem(MenuItemPreferences preferences, Block onSelect) : base(onSelect)
        {
            this.icon = preferences.Icon;
            this.text = preferences.Text;
            this.textColor = preferences.TextColor;
        }

        internal IconTextMenuItem(Sprite icon, string text, Color textColor, Block onSelect) 
            : base(onSelect)
        {
            this.icon = icon;
            this.text = text;
            this.textColor = textColor;
        }

    }

    internal sealed class ProfileMenuItem : MenuItem
    {

        internal readonly string playerId;

        internal ProfileMenuItem(string playerId, Block onSelect) : base(onSelect)
        {
            this.playerId = playerId;
        }
        
    }

}