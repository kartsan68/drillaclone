using System;
using System.Collections.Generic;
using BlackBears.Utils.Collections;
using UnityEngine;

namespace BlackBears.Clans.ViewModel.ContextMenu
{

    internal class Menu
    {

        internal IEnumerable<MenuItem> Items { get; private set; }

        private Menu(IEnumerable<MenuItem> items)
        {
            if (items == null) items = new List<MenuItem>();
            Items = items;
        }

        internal class Builder
        {

            private MenuBuilderPreferences preferences;
            private LinkedList<MenuItem> items = new LinkedList<MenuItem>();

            internal Builder(MenuBuilderPreferences preferences)
            {
                this.preferences = preferences;
            }

            internal void AddPlayerMenuItem(string playerId, Block onSelect, bool withDefaultItem)
            {
                ProfileMenuItem item = new ProfileMenuItem(playerId, onSelect);
                if (items.Count != 0 && (items.First.Value as ProfileMenuItem) != null) 
                {
                    items.RemoveFirst();
                }
                items.AddFirst(item);

                if (withDefaultItem) AddProfileItem(onSelect);
            }

            internal void AddProfileItem(Block onSelect) { AddMenuItem(preferences.ProfileMenuItemPreferences, onSelect); }
            internal void AddAnswerItem(Block onSelect) { AddMenuItem(preferences.AnswerMenuItemPreferences, onSelect); }
            internal void AddPromoteItem(Block onSelect) { AddMenuItem(preferences.PromoteMenuItemPreferences, onSelect); }
            internal void AddDemoteItem(Block onSelect) { AddMenuItem(preferences.DemoteMenuItemPreferences, onSelect); }
            internal void AddKickItem(Block onSelect) { AddMenuItem(preferences.KickMenuItemPreferences, onSelect); }

            internal void AddCustomMenuItem(Sprite icon, string text, Color textColor, Block onSelect)
            {
                items.AddLast(new IconTextMenuItem(icon, text, textColor, onSelect));
            }

            internal void Reset()
            {
                items.Clear();
            }

            internal Menu Build()
            {
                return new Menu(new List<MenuItem>(items));
            }

            private void AddMenuItem(MenuItemPreferences itemPrefs, Block onSelect)
            {
                items.AddLast(new IconTextMenuItem(itemPrefs, onSelect));
            }
        }

    }

}