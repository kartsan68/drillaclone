using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.ViewModel.ContextMenu
{

    [CreateAssetMenu(menuName = "BlackBears/Clans/ContextMenu Builder Prefs", fileName = "ContextMenuBuilderPrefs")]
    internal class MenuBuilderPreferences : ScriptableObject
    {

        [SerializeField] private MenuItemPreferences profileMenuItemPreferences;
        [SerializeField] private MenuItemPreferences answerMenuItemPreferences;
        [SerializeField] private MenuItemPreferences promoteMenuItemPreferences;
        [SerializeField] private MenuItemPreferences demoteMenuItemPreferences;
        [SerializeField] private MenuItemPreferences kickMenuItemPreferences;

        internal MenuItemPreferences ProfileMenuItemPreferences { get { return profileMenuItemPreferences; } }
        internal MenuItemPreferences AnswerMenuItemPreferences { get { return answerMenuItemPreferences; } }
        internal MenuItemPreferences PromoteMenuItemPreferences { get { return promoteMenuItemPreferences; } }
        internal MenuItemPreferences DemoteMenuItemPreferences { get { return demoteMenuItemPreferences; } }
        internal MenuItemPreferences KickMenuItemPreferences { get { return kickMenuItemPreferences; } }

    }

    [System.Serializable]
    internal class MenuItemPreferences
    {
        [SerializeField] private Sprite icon;
        [SerializeField] private string text;
        [SerializeField] private Color textColor;

        internal Sprite Icon { get { return icon; } }
        internal string Text { get { return text; } }
        internal Color TextColor { get { return textColor; } }

    }

}