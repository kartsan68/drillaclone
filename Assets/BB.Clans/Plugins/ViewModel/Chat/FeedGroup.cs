using System.Collections.Generic;
using BlackBears.Clans.Model.Timeline;

namespace BlackBears.Clans.ViewModel.Chat
{

    internal class FeedGroup
    {

        private const long MaxDateDiff = 5L * 60L;

        internal List<FeedItem> feedItems = new List<FeedItem>();

        private FeedGroup() {}

        private FeedGroup(FeedItem item)
        {
            feedItems.Add(item);
        }

        internal static void UpdateGroups(List<FeedItem> newItems, List<FeedGroup> buffer)
        {
            FeedGroup currentGroup;
            if (buffer.Count == 0) 
            {
                currentGroup = new FeedGroup();
                buffer.Add(currentGroup);
            }
            else 
            {
                currentGroup = buffer[buffer.Count - 1];
            }
            
            for(int i = 0; i < newItems.Count; i++)
            {
                var item = newItems[i];
                if (!currentGroup.TryAddItem(item))
                {
                    currentGroup = new FeedGroup(item);
                    buffer.Add(currentGroup);
                }
            }
        }

        private bool TryAddItem(FeedItem item)
        {
            if (feedItems.Count == 0)
            {
                feedItems.Add(item);
                return true;
            }

            if (!item.feedType.IsAnyMessage()) return false;

            FeedItem lastItem = feedItems[feedItems.Count - 1];
            var dateDiff = lastItem.date - item.date;
            if (dateDiff > MaxDateDiff) return false;
            if (!string.Equals(lastItem.playerId, item.playerId)) return false;

            feedItems.Add(item);
            return true;
        }

    }

}