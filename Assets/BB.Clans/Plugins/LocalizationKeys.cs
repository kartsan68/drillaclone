namespace BlackBears.Clans
{

    internal static class LocalizationKeys
    {

#if UNITY_IOS
        internal const string osVersion = "CLANS_SUPPORT_IOS_VERSION";
        internal const string cloudName = "CLANS_SUPPORT_IOS_CLOUD_SAVE";
#elif UNITY_ANDROID
        internal const string osVersion = "CLANS_SUPPORT_ANDROID_VERSION";
        internal const string cloudName = "CLANS_SUPPORT_ANDROID_CLOUD_SAVE";
#endif

        internal const string save = "CLANS_SAVE";
        internal const string cancel = "CANCEL";
        internal const string online = "CLANS_ONLINE";
        internal const string offline = "CLANS_OFFLINE";

        internal const string defaultNickname = "CLANS_DEFAULT_NICK";

        internal const string inClan = "CLANS_IN_CLAN";
        internal const string notInClan = "CLANS_NOT_IN_CLAN";
        internal const string numberedPlace = "CLANS_NUMBERED_PLACE";

        internal const string errorCommonTitle = "CLANS_ERROR_COMMON_TITLE";
        internal const string errorCommonDescription = "CLANS_ERROR_COMMON_DESCRIPTION";
        internal const string errorShortNickname = "CLANS_ERROR_SHORT_NICKNAME";
        internal const string errorShortClanname = "CLANS_ERROR_SHORT_CLANNAME";
        internal const string errorLongClanname = "CLANS_ERROR_LONG_CLANNAME";

        internal const string createClanTitle = "CLANS_CREATE_CLAN_TITLE";
        internal const string editClanTitle = "CLANS_EDIT_CLAN_TITLE";
        internal const string createClanSubmit = "CLANS_CREATE";

        internal const string alertExitClanTitle = "CLANS_ALERT_EXIT_CLAN_T";
        internal const string alertExitClanDescription = "CLANS_ALERT_EXIT_CLAN_D";
        internal const string alertExitClanAction = "CLANS_ALERT_EXIT_CLAN_ACT";
        internal const string alertKickUserTitle = "CLANS_ALERT_KICK_USER_T";
        internal const string alertKickUserDescription = "CLANS_ALERT_KICK_USER_D";
        internal const string alertKickUserAction = "CLANS_ALERT_KICK_USER_ACT";
        internal const string alertDestroyClanTitle = "CLANS_ALERT_DES_CLAN_T";
        internal const string alertDestroyClanDescription = "CLANS_ALERT_DES_CLAN_D";
        internal const string alertDestroyClanAction = "CLANS_ALERT_DES_CLAN_ACT";

        internal const string playerLocalFromTotalPlace = "CLANS_LOCAL_FROM_TOTAL_PLACE";

        internal const string joinButtonPublicClanTitle = "CLANS_JOIN";
        internal const string joinButtonPrivateClanTitle = "CLANS_APPLY";
        internal const string joinButtonPublicClanSubtitle = "CLANS_TO_CLAN";
        internal const string joinButtonPrivateClanSubtitle = "CLANS_APPLY_REQUEST";

        internal const string notificationErrorNetworkConnection = "CLANS_NOTIF_ERROR_NETWORK_CONNECTION";
        internal const string notificationErrorCreatePlayer = "CLANS_NOTIF_ERROR_CREATE_PLAYER";
        internal const string notificationErrorSendMessage = "CLANS_NOTIF_ERROR_SEND_MESSAGE";
        internal const string notificationErrorLoadFeed = "CLANS_NOTIF_ERROR_LOAD_FEED";
        internal const string notificationErrorCreateClan = "CLANS_NOTIF_ERROR_CREATE_CLAN";
        internal const string notificationErrorUpdateClan = "CLANS_NOTIF_ERROR_UPDATE_CLAN";
        internal const string notificationErrorDisbandClan = "CLANS_NOTIF_ERROR_DISBAND_CLAN";
        internal const string notificationErrorPromotePlayer = "CLANS_NOTIF_ERROR_PROMOTE_PLAYER";
        internal const string notificationErrorDemotePlayer = "CLANS_NOTIF_ERROR_DEMOTE_PLAYER";
        internal const string notificationErrorKickUser = "CLANS_NOTIF_ERROR_KICK_USER";
        internal const string notificationErrorJoinClan = "CLANS_NOTIF_ERROR_JOIN_CLAN";
        internal const string notificationErrorClanExitFailed = "CLANS_NOTIF_ERROR_CLAN_EXIT_FAILED";
        internal const string notificationErrorAcceptPlayer = "CLANS_NOTIF_ERROR_ACCEPT_PLAYER";
        internal const string notificationErrorTakeInviteReward = "CLANS_NOTIF_ERROR_TAKE_INVITE_REWARD";
        internal const string notificationDonationExpired = "CLANS_NOTIF_ERROR_DONATION_EXPIRED"; //CLAN_DONATION_NOTIFICATION_ERROR_EXPIRED
        internal const string notificationResourceGiveLimited = "CLANS_NOTIF_ERROR_GIVE_LIMITED"; //CLANS_RESOURCE_GIVE_LIMITED
        internal const string notificationDonationResourceNotEnough = "CLANS_NOTIF_ERROR_RES_NOT_ENOUGH"; //CLAN_DONATION_NOTIFICATION_ERROR_NOT_ENOUGH_RESOURCES
        internal const string notificationDonationReward = "CLANS_NOTIF_DONATION_REWARD"; //CLAN_DONATION_REWARD_NOTIFICATION

        internal const string resourceRequestSendButton = "CLANS_RESOURCE_REQUEST_SEND_BUTTON";
        internal const string resourceTakeTitle = "CLANS_RESOURCES_TAKE_TITLE";
        internal const string resourceCraftTitle = "CLANS_RESOURCES_CRAFT_TITLE";

        internal const string shareClanTagMessage = "CLANS_SHARE_CLAN_TAG_MESSAGE";
        internal const string shareConnectionLinkMessage = "CLANS_SHARE_CONNECTION_LINK";
        internal const string clanSearchHelp = "CLANS_CLAN_SEARCH_HELP";

        internal const string clanTookPlace = "CLANS_EVENT_CLAN_TOOK_PLACE";

        internal static string leagueTitle = "LEAGUE_TITLE";
        internal static string leagueType = "LEAGUE_TYPE_";
        internal static string leagueNumberText = "LEAGUE_TEXT";
        internal static string leagueNumberTextTitle = "LEAGUE_TEXT_TITLE";
    }

}