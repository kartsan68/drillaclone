using System;
using System.Collections.Generic;
using BlackBears.Clans.Events.ClanBattle;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.Model.Timeline;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore;
using BlackBears.GameCore.Networking;
using SimpleJSON;
using UnityEngine;

using BlackSec = BlackBears.Utils.Security;

namespace BlackBears.Clans.Networking
{

    internal partial class ConnectionBehaviour : MonoBehaviour
    {

        internal IConnectionDataProvider dataProvider;

        #region Clans

        internal void CreateClan(ClanEdit clan, JSONNode gameData, Block<Clan> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (clan == null)
            {
                onFail.SafeInvoke(-1, "Clan info is not valid");
                return;
            }

            var converter = new ClanCreateConverter(onSuccess);
            string url = GenerateUrl("clan/create");
            var request = new NetworkApiRequest(url, gameData, converter.Convert, onFail);

            request.AddRequestValue("clan[player_id]", clan.adminId);
            request.AddRequestValue("clan[title]", clan.Title);
            request.AddRequestValue("clan[description]", clan.Description ?? string.Empty);
            request.AddRequestValue("clan[is_public]", clan.IsPublic ? 1 : 0);
            request.AddRequestValue("clan[icon]", clan.Icon.ToJson());
            request.AddRequestValue("clan[country]", clan.Country);
            request.AddRequestValue("clan[join_requirement]", clan.JoinRequirement);
            AddPlayerKeyToRequest(clan.adminId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void UpdateClan(ClanEdit clanEdit, Clan clanForUpdate, Block<Clan> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");

            if (clanEdit == null || clanForUpdate == null)
            {
                onFail.SafeInvoke(-1, "Incorrect input data");
                return;
            }

            var converter = new ClanUpdateConverter(onSuccess, onFail, clanForUpdate);
            string url = GenerateUrl("clan/update");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);

            request.AddRequestValue("admin_id", clanEdit.adminId);
            request.AddRequestValue("clan[id]", clanForUpdate.Id);
            request.AddRequestValue("clan[title]", clanEdit.Title);
            request.AddRequestValue("clan[description]", clanEdit.Description);
            request.AddRequestValue("clan[is_public]", clanEdit.IsPublic ? 1 : 0);
            request.AddRequestValue("clan[icon]", clanEdit.Icon.ToJson());
            request.AddRequestValue("clan[country]", clanEdit.Country);
            request.AddRequestValue("clan[join_requirement]", clanEdit.JoinRequirement);

            AddPlayerKeyToRequest(clanForUpdate.Id + clanEdit.adminId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void DeleteClan(string clanId, string playerId, Block<NetworkSuccessAnswer> onSuccess,
            FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");

            if (clanId == null || playerId == null)
            {
                onFail.SafeInvoke(-1, "Incorrect input data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "successful");
            string url = GenerateUrl("clan/delete");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);

            request.AddRequestValue("clan_id", clanId);
            request.AddRequestValue("admin_id", playerId);
            AddPlayerKeyToRequest(playerId + clanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void SwitchPushes(string playerId, bool enabled,
            Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "Incorrect input data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "sent_successful");
            string url = GenerateUrl("clan-user/accept-pushes");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);

            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("accept", enabled ? "1" : "0");
            AddPlayerKeyToRequest(playerId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetWorldTop(string playerClanId, int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerClanId == null) playerClanId = string.Empty;

            var converter = new TopResponseConverter(true, onSuccess);
            string url = GenerateUrl("clan/top");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("user_clan_id", playerClanId);
            request.AddRequestValue("page", page);
            AddKeyToRequest(playerClanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetLocalTop(string playerClanId, string country, int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerClanId == null) playerClanId = string.Empty;

            var converter = new TopResponseConverter(true, onSuccess);
            string url = GenerateUrl("clan/local-top");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("user_clan_id", playerClanId);
            request.AddRequestValue("page", page);
            request.AddRequestValue("country", country);
            AddKeyToRequest(playerClanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetNewbiesTop(string playerClanId, int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerClanId == null) playerClanId = string.Empty;

            var converter = new TopResponseConverter(true, onSuccess);
            string url = GenerateUrl("clan/newbies-top");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("user_clan_id", playerClanId);
            request.AddRequestValue("page", page);
            AddKeyToRequest(playerClanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetRecommendedClans(string playerId, string playerClanId, string country, double score,
            Block<TopResponse> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null) playerId = string.Empty;

            var converter = new TopResponseConverter(false, onSuccess);
            string url = GenerateUrl("clan/search-recommend");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("country", country);
            request.AddRequestValue("curuser_id", playerId);
            request.AddRequestValue("score", score);
            if (!string.IsNullOrEmpty(playerClanId)) request.AddRequestValue("clan_id", playerClanId);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void SearchClan(ClanSearch search, double score, int page, Block<List<Clan>> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");

            var converter = new ClanListConverter(onSuccess);
            string url = GenerateUrl("clan/search");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("search", search.search ?? string.Empty);
            request.AddRequestValue("country", search.country ?? string.Empty);
            request.AddRequestValue("public", search.publicOnly ? "1" : "0");
            if (search.canJoin) request.AddRequestValue("score", score);
            request.AddRequestValue("page", page);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetClan(Clan clan, Block<Clan> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (clan == null || clan.Id == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new GetClanRequestConverter(clan, onSuccess);
            string url = GenerateUrl("clan/view");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("id", clan.Id);
            AddKeyToRequest(clan.Id, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetIncomingPlayers(string moderatorId, Clan clan, Block<ClanUser[]> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (moderatorId == null || clan == null || clan.Id == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new GetJoinRequestsConverter(clan, onSuccess, onFail);
            string url = GenerateUrl("clan/incoming-players");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("admin_id", moderatorId);
            request.AddRequestValue("clan_id", clan.Id);
            AddKeyToRequest(clan.Id + moderatorId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetClansByPlayersIds(List<string> playerIds, Block<PlayersClanDataUnion> onSuccess, 
            FailBlock onFail, PlayersClanDataUnion dataUnion)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerIds == null || playerIds.Count == 0)
            {
                if (dataUnion != null) dataUnion.Reset();
                onSuccess.SafeInvoke(dataUnion);
                return;
            }

            var converter = new GetClansByPlayersIdsConverter(onSuccess, onFail, dataUnion);
            string url = GenerateUrl("clan/get-players-clans");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            for (int i = 0; i < playerIds.Count; ++i) request.AddRequestValue($"ids[{i}]", playerIds[i]);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        #endregion Clans

        #region Timeline

        internal void GetTimeline(string playerId, string clanId, int page, Block<List<FeedItem>> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new GetTimelineRequestConverter(onSuccess);
            string url = GenerateUrl("clan/get-timeline");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("clan_id", clanId);
            request.AddRequestValue("page", page);
            AddKeyToRequest(playerId + clanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void SendMessage(string playerId, string message, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "sent_successful");
            string url = GenerateUrl("clan-user/send-message");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("message", message);
            AddPlayerKeyToRequest(playerId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void RequestResource(string playerId, int resourceType, long resourceId, double value, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "requested");
            string url = GenerateUrl("clan-user/request-resource");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("resource_type", resourceType);
            request.AddRequestValue("resource_id", resourceId);
            request.AddRequestValue("value", value);
            AddPlayerKeyToRequest(playerId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void SendResource(string playerId, string requestId, int resourceType, long resourceId, double value,
            Block<ResourceTransition> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || requestId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SendResourceRequestConverter(requestId, onSuccess);
            string url = GenerateUrl("clan-user/give-resource");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("resource_type", resourceType);
            request.AddRequestValue("resource_id", resourceId);
            request.AddRequestValue("request_id", requestId);
            request.AddRequestValue("value", value);
            AddPlayerKeyToRequest(playerId + requestId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void CheckResources(string playerId, Block<List<ClanResourceRequest>> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new CheckResourcesRequestConverter(onSuccess);
            string url = GenerateUrl("clan-user/check-resource");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            AddPlayerKeyToRequest(playerId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GrabIncomingDonations(string playerId, List<string> donationIds, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || donationIds == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "grabbed");
            string url = GenerateUrl("clan-user/grab-resources");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            for (int i = 0; i < donationIds.Count; i++)
            {
                request.AddRequestValue(string.Format("donations[{0}]", i), donationIds[i]); ;
            }
            AddPlayerKeyToRequest(playerId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }


        #endregion Timeline

        #region Users

        internal void GetUserMainInfo(string playerId, bool needTopReward, Block<PlayerMainInfo> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new GetUserMainInfoRequestConverter(onSuccess);
            string url = GenerateUrl("clan-user/main-info");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("with_top_reward", needTopReward ? "1" : "0");
            AddKeyToRequest(playerId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void UpdateScore(string playerId, string clanId, double score, 
            JSONNode gameData, Block<UpdateScoreAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new UpdateScoreRequestConverter(onSuccess);
            string url = GenerateUrl("clan-user/update-score");
            var request = new NetworkApiRequest(url, gameData, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("clan_id", clanId);
            request.AddRequestValue("score", score);
            AddPlayerKeyToRequest(playerId + clanId + score, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetClanUser(string playerId, string currentPlayerId,
            Block<ClanUserAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || currentPlayerId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new GetClanUserRequestConverter(onSuccess);
            string url = GenerateUrl("clan-user/view");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("cur_player_id", currentPlayerId);
            if (string.Equals(playerId, currentPlayerId)) AddPlayerKeyToRequest(playerId, request);
            else AddKeyToRequest(playerId, request);

            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void JoinToClan(string playerId, string clanId, double score, 
            JSONNode gameData, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "successful");
            string url = GenerateUrl("clan-user/join-clan");
            var request = new NetworkApiRequest(url, gameData, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("clan_id", clanId);
            request.AddRequestValue("score", score);
            AddPlayerKeyToRequest(playerId + clanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void LeaveClan(string playerId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "successful");
            string url = GenerateUrl("clan-user/leave-clan");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("clan_id", clanId);
            AddPlayerKeyToRequest(playerId + clanId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void KickFromClan(string playerId, string moderatorId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || moderatorId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "successful");
            string url = GenerateUrl("clan-user/kick-user");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("admin_id", moderatorId);
            request.AddRequestValue("clan_id", clanId);
            AddPlayerKeyToRequest(clanId + moderatorId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void PromoteUser(string playerId, string moderatorId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || moderatorId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "successful");
            string url = GenerateUrl("clan-user/promote-user");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("admin_id", moderatorId);
            request.AddRequestValue("clan_id", clanId);
            AddPlayerKeyToRequest(clanId + moderatorId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void DemoteUser(string playerId, string moderatorId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (playerId == null || moderatorId == null || clanId == null)
            {
                onFail.SafeInvoke(-1, "No required data");
                return;
            }

            var converter = new SuccessResponceConverter(onSuccess, "successful");
            string url = GenerateUrl("clan-user/demote-user");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("admin_id", moderatorId);
            request.AddRequestValue("clan_id", clanId);
            AddPlayerKeyToRequest(clanId + moderatorId, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        #endregion

        #region Battle

        internal void GetBattleInfo(long battleId, Battle battle, Block<Battle> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");

            var converter = new GetBattleInfoConverter(onSuccess, onFail, battle);
            string url = GenerateUrl("clan-user/battle-info");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("battle_id", battleId);
            AddKeyToRequest(battleId.ToString(), request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GetBattleTopUsers(long battleId, List<ClanUser> users,
            Block<List<ClanUser>> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");

            var converter = new GetBattleTopUsersConverter(onSuccess, users);
            string url = GenerateUrl("clan-user/battle-top-users");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("battle_id", battleId);
            AddKeyToRequest(battleId.ToString(), request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void GrabBattleReward(string playerId, long battleId,
            Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");

            var converter = new SuccessResponceConverter(onSuccess, "success");
            string url = GenerateUrl("clan-user/grab-battle-reward");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("battle_id", battleId);
            AddPlayerKeyToRequest($"{playerId}{battleId}", request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        #endregion

        private string GenerateUrl(string command)
        {
            return string.Format("{0}/{1}", dataProvider.Server, command);
        }

        private void AddKeyToRequest(string keyParam, NetworkApiRequest request)
        {
            string randKey = BlackSec.GetRandKey();
            string fullKey = string.Format("{0}{1}{2}", keyParam, randKey, dataProvider.Secret);
            string key = BlackSec.GetMD5Hash(fullKey, BlackSec.MD5Format.lower);

            request.AddRequestValue("rand_key", randKey);
            request.AddRequestValue("key", key);
        }

        private void AddPlayerKeyToRequest(string keyParam, NetworkApiRequest request)
        {
            string randKey = BlackSec.GetRandKey();
            string fullKey = string.Format("{0}{1}{2}{3}", keyParam, randKey,
                dataProvider.PlayerKey, dataProvider.Secret);
            string key = BlackSec.GetMD5Hash(fullKey, BlackSec.MD5Format.lower);

            request.AddRequestValue("rand_key", randKey);
            request.AddRequestValue("key", key);
        }

        private void AddDefaultKeys(NetworkApiRequest request)
        {
            request.AddRequestValue("os", CommonKeys.osName);
            request.AddRequestValue("app_version", dataProvider.ApiVersion);
        }

    }

}