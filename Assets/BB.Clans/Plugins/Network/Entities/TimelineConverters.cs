using System.Collections.Generic;
using BlackBears.Clans.Model.Timeline;
using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.Clans.Networking
{

    internal class GetTimelineRequestConverter : DataConverter<List<FeedItem>>
    {
        public GetTimelineRequestConverter(Block<List<FeedItem>> onConvert) : base(onConvert, null) {}

        protected override bool Convert(ref List<FeedItem> instance, JSONNode data)
        {
            instance = new List<FeedItem>();
            var timeline = data["timeline"].AsArray;
            foreach(JSONNode itemData in timeline) instance.Add(new FeedItem(itemData));
            return true;
        }
    }

}