using System.Collections.Generic;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.ResourceShare;
using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.Clans.Networking
{

    internal class SendResourceRequestConverter : DataConverter<ResourceTransition>
    {

        private readonly string requestId;

        public SendResourceRequestConverter(string requestId, Block<ResourceTransition> onConvert)
            : base(onConvert, null)
        {
            this.requestId = requestId;
        }

        protected override bool Convert(ref ResourceTransition instance, JSONNode data)
        {
            instance = new ResourceTransition(requestId, data);
            return true;
        }

    }

    internal class CheckResourcesRequestConverter : DataConverter<List<ClanResourceRequest>>
    {

        public CheckResourcesRequestConverter(Block<List<ClanResourceRequest>> onConvert) : base(onConvert, null)
        {
        }

        protected override bool Convert(ref List<ClanResourceRequest> instance, JSONNode data)
        {
            instance = new List<ClanResourceRequest>();

            var requestsData = data["requests"];
            foreach (var kvp in requestsData)
            {
                instance.Add(new ClanResourceRequest(kvp.Key, kvp.Value));
            }
            return true;
        }

    }

}