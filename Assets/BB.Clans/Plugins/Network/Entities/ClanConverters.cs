using BlackBears.GameCore.Networking;
using BlackBears.Clans.Model;
using SimpleJSON;
using System.Collections.Generic;

namespace BlackBears.Clans.Networking
{

    internal class ClanCreateConverter : DataConverter<Clan>
    {

        internal ClanCreateConverter(Block<Clan> onConvert, Clan clan = null) : base(onConvert, null, clan) { }

        protected override bool Convert(ref Clan instance, JSONNode data)
        {
            if (instance == null) instance = new Clan();
            instance.FromJson(data);
            return true;
        }

    }

    internal class ClanUpdateConverter : DataConverter<Clan>
    {

        internal ClanUpdateConverter(Block<Clan> onConvert, FailBlock onFail, Clan clan) : base(onConvert, onFail, clan) { }

        protected override bool Convert(ref Clan instance, JSONNode data)
        {
            if (!data["successful"].AsBool) return false;

            if (instance == null) instance = new Clan();
            instance.FromJson(data["updated_clan"]);
            return true;
        }

    }

    internal class SuccessResponceConverter : DataConverter<NetworkSuccessAnswer>
    {

        private string key;

        internal SuccessResponceConverter(Block<NetworkSuccessAnswer> onConvert, string key) : base(onConvert, null)
        {
            this.key = key;
        }

        protected override bool Convert(ref NetworkSuccessAnswer instance, JSONNode data)
        {
            var successful = data[key].AsBool;
            if (successful) instance = NetworkSuccessAnswer.Success;
            else instance = NetworkSuccessAnswer.NotSuccess;
            return true;
        }
    }

    internal class TopResponseConverter : DataConverter<TopResponse>
    {

        private bool clansInDictionary;

        internal TopResponseConverter(bool clansInDictionary, Block<TopResponse> onConvert) : base(onConvert, null)
        {
            this.clansInDictionary = clansInDictionary;
        }

        protected override bool Convert(ref TopResponse response, JSONNode data)
        {
            response = TopResponse.Build(data, clansInDictionary);
            return true;
        }

    }

    internal class ClanListConverter : DataConverter<List<Clan>>
    {
        internal ClanListConverter(Block<List<Clan>> onConvert) : base(onConvert, null) { }

        protected override bool Convert(ref List<Clan> instance, JSONNode data)
        {
            instance = new List<Clan>();
            var clansNode = data["clans"];

            for (int i = 0; i < clansNode.Count; i++) instance.Add(new Clan(clansNode[i]));

            return true;
        }
    }

    internal class GetClanRequestConverter : DataConverter<Clan>
    {
        internal GetClanRequestConverter(Clan clan, Block<Clan> onConvert) : base(onConvert, null, clan) { }

        protected override bool Convert(ref Clan instance, JSONNode data)
        {
            if (instance == null) instance = new Clan();
            instance.FromJson(data["clan"]);
            return true;
        }
    }

    internal class GetUserMainInfoRequestConverter : DataConverter<PlayerMainInfo>
    {

        public GetUserMainInfoRequestConverter(Block<PlayerMainInfo> onConvert) : base(onConvert, null) { }

        protected override bool Convert(ref PlayerMainInfo instance, JSONNode data)
        {
            instance = new PlayerMainInfo(data);
            return true;
        }
    }

    internal class UpdateScoreRequestConverter : DataConverter<UpdateScoreAnswer>
    {

        public UpdateScoreRequestConverter(Block<UpdateScoreAnswer> onConvert) : base(onConvert, null) { }

        protected override bool Convert(ref UpdateScoreAnswer instance, JSONNode data)
        {
            instance = new UpdateScoreAnswer(data);
            return true;
        }

    }

    internal class GetClanUserRequestConverter : DataConverter<ClanUserAnswer>
    {
        public GetClanUserRequestConverter(Block<ClanUserAnswer> onConvert) : base(onConvert, null) { }

        protected override bool Convert(ref ClanUserAnswer instance, JSONNode data)
        {
            instance = new ClanUserAnswer(data);
            return true;
        }

    }

    internal class GetJoinRequestsConverter : DataConverter<ClanUser[]>
    {

        private Clan clan;

        public GetJoinRequestsConverter(Clan clan, Block<ClanUser[]> onConvert, FailBlock onFail)
            : base(onConvert, onFail)
        {
            this.clan = clan;
        }

        protected override bool Convert(ref ClanUser[] instance, JSONNode data)
        {
            var incomingData = data["incoming"];
            if (incomingData.Tag == JSONNodeType.None) return false;

            instance = new ClanUser[incomingData.Count];
            int index = 0;
            foreach (var kvp in incomingData)
            {
                var id = kvp.Key;
                ClanUser user = null;
                if (clan != null)
                {
                    user = clan.GetUser(id);
                    if (user != null) user.FromJson(kvp.Value);
                }
                if (user == null) user = new ClanUser(kvp.Value);
                instance[index] = user;
                index += 1;
            }
            return true;
        }

    }

    internal class GetClansByPlayersIdsConverter : DataConverter<PlayersClanDataUnion>
    {

        internal GetClansByPlayersIdsConverter(Block<PlayersClanDataUnion> onConvert, FailBlock onFail, 
            PlayersClanDataUnion instance) : base(onConvert, onFail, instance) {}

        protected override bool Convert(ref PlayersClanDataUnion instance, JSONNode data)
        {
            if (instance == null) instance = new PlayersClanDataUnion(data);
            else instance.ParseJson(data);
            return true;
        }

    }

}