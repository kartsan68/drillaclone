using System.Collections.Generic;
using BlackBears.Clans.Events.ClanBattle;
using BlackBears.Clans.Model;
using BlackBears.Clans.Model.ResourceTransaction;
using BlackBears.Clans.Model.Timeline;
using BlackBears.GameCore.Features.JellyB;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.Clans.Networking
{

    public partial class ServerConnection : IConnectionDataProvider
    {

        private ConnectionBehaviour behaviour;
        private ClanFullSettings settings;
        private JellyBFeature jellyB;

        private string playerKey;

        internal ServerConnection(ClanFullSettings settings, JellyBFeature jellyB)
        {
            this.settings = settings;
            this.jellyB = jellyB;

            var go = new GameObject("~~ClanServer");
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            Object.DontDestroyOnLoad(go);
#endif
            go.SetActive(false);
            behaviour = go.AddComponent<ConnectionBehaviour>();
            behaviour.dataProvider = this;
            go.SetActive(true);
        }

        internal IState State { private get; set; }

        string IConnectionDataProvider.Server
        {
            get
            {
                switch (jellyB.Workspace)
                {
                    case Workspace.Standart: return settings.parameters.server;
                    default: return settings.parameters.jellyServer;
                }
            }
        }

        string IConnectionDataProvider.Secret
        {
            get
            {
                switch (jellyB.Workspace)
                {
                    case Workspace.Standart: return settings.configuration.Secret;
                    default: return settings.configuration.JellySecret;
                }
            }
        }

        string IConnectionDataProvider.ApiVersion => settings.configuration.ServerApiVersion;

        string IConnectionDataProvider.PlayerKey => playerKey;

        #region  Clan management

        internal void PlayerKeyAcquired(string playerKey) => this.playerKey = playerKey;

        internal void CreateClan(ClanEdit clanEdit, JSONNode gameData, Block<Clan> onSuccess, FailBlock onFail)
        {
            behaviour.CreateClan(clanEdit, gameData, onSuccess, onFail);
        }

        internal void UdpateClan(ClanEdit clanEdit, Clan clanForUpdate, Block<Clan> onSuccess, FailBlock onFail)
        {
            behaviour.UpdateClan(clanEdit, clanForUpdate, onSuccess, onFail);
        }

        internal void DeleteClan(string clanId, string playerId, Block<NetworkSuccessAnswer> onSuccess,
            FailBlock onFail)
        {
            behaviour.DeleteClan(clanId, playerId, onSuccess, onFail);
        }

        internal void SwitchPushes(string playerId, bool enabled,
            Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.SwitchPushes(playerId, enabled, onSuccess, onFail);
        }

        #endregion Clan management

        #region Clan searching

        internal void GetWorldTop(string playerClanId, int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            behaviour.GetWorldTop(playerClanId, page, onSuccess, onFail);
        }

        internal void GetLocalTop(string playerClanId, string country, int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            behaviour.GetLocalTop(playerClanId, country, page, onSuccess, onFail);
        }

        internal void GetRecommendedClans(string playerId, string playerClanId, string country, double score,
            Block<TopResponse> onSuccess, FailBlock onFail)
        {
            behaviour.GetRecommendedClans(playerId, playerClanId, country, score, onSuccess, onFail);
        }

        internal void GetNewbiesTop(string playerClanId, int page, Block<TopResponse> onSuccess, FailBlock onFail)
        {
            behaviour.GetNewbiesTop(playerClanId, page, onSuccess, onFail);
        }

        internal void SearchClan(ClanSearch search, double score, int page, Block<List<Clan>> onSuccess, FailBlock onFail)
        {
            behaviour.SearchClan(search, score, page, onSuccess, onFail);
        }

        internal void GetClan(string clanId, Block<Clan> onSuccess, FailBlock onFail)
        {
            GetClan(new Clan(clanId), onSuccess, onFail);
        }

        internal void GetClan(Clan clan, Block<Clan> onSuccess, FailBlock onFail)
        {
            behaviour.GetClan(clan, onSuccess, onFail);
        }

        internal void GetIncomingPlayers(string playerId, Clan clan, Block<ClanUser[]> onSuccess, FailBlock onFail)
        {
            behaviour.GetIncomingPlayers(playerId, clan, onSuccess, onFail);
        }

        /// <summary>
        /// Возвращает неполную информацию о кланах (id, название, иконка)
        /// </summary>
        /// <param name="playerIds">ид игроков, которым нужны кланы</param>
        /// <param name="onSuccess">Вызывается если получить данные удалось</param>
        /// <param name="onFail">Вызывается если произошла ошибка</param>
        internal void GetClanShortDataByPlayerIds(List<string> playerIds,
            Block<PlayersClanDataUnion> onSuccess, FailBlock onFail, PlayersClanDataUnion dataUnion)
        {
            behaviour.GetClansByPlayersIds(playerIds, onSuccess, onFail, dataUnion);
        }

        #endregion Clan searching

        #region User management

        internal void GetUserMainInfo(string playerId, bool needTopReward, Block<PlayerMainInfo> onSuccess, FailBlock onFail)
        {
            behaviour.GetUserMainInfo(playerId, needTopReward, onSuccess, onFail);
        }

        internal void UpdateScore(string playerId, string clanId, double score,
            JSONNode gameData, Block<UpdateScoreAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.UpdateScore(playerId, clanId, score, gameData, onSuccess, onFail);
        }

        internal void GetClanUser(string playerId, string currentPlayerId,
            Block<ClanUserAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.GetClanUser(playerId, currentPlayerId, onSuccess, onFail);
        }

        internal void JoinToClan(string playerId, string clanId, double score, JSONNode gameData,
            Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.JoinToClan(playerId, clanId, score, gameData, onSuccess, onFail);
        }

        internal void LeaveClan(string playerId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.LeaveClan(playerId, clanId, onSuccess, onFail);
        }

        internal void KickFromClan(string playerId, string moderatorId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.KickFromClan(playerId, moderatorId, clanId, onSuccess, onFail);
        }

        internal void PromoteUser(string playerId, string moderatorId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.PromoteUser(playerId, moderatorId, clanId, onSuccess, onFail);
        }

        internal void DemoteUser(string playerId, string moderatorId, string clanId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.DemoteUser(playerId, moderatorId, clanId, onSuccess, onFail);
        }

        #endregion User management

        #region Timeline management

        internal void GetTimeline(string playerId, string clanId, int page, Block<List<FeedItem>> onSuccess,
            FailBlock onFail)
        {
            behaviour.GetTimeline(playerId, clanId, page, onSuccess, onFail);
        }

        internal void SendMessage(string playerId, string message, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.SendMessage(playerId, message, onSuccess, onFail);
        }

        internal void RequestResource(string playerId, int resourceType, long resourceId, double value, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.RequestResource(playerId, resourceType, resourceId, value, onSuccess, onFail);
        }

        internal void SendResource(string playerId, string requestId, int resourceType, long resourceId, double value,
            Block<ResourceTransition> onSuccess, FailBlock onFail)
        {
            behaviour.SendResource(playerId, requestId, resourceType, resourceId, value, onSuccess, onFail);
        }

        internal void CheckResources(string playerId, Block<List<ClanResourceRequest>> onSuccess, FailBlock onFail)
        {
            behaviour.CheckResources(playerId, onSuccess, onFail);
        }

        internal void GrabIncomingDonations(string playerId, List<string> donationIds, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.GrabIncomingDonations(playerId, donationIds, onSuccess, onFail);
        }

        #endregion Timeline management

        #region Battle Event

        internal void GetBattleInfo(long battleId, Block<Battle> onSuccess, FailBlock onFail)
        {
            behaviour.GetBattleInfo(battleId, null, onSuccess, onFail);
        }

        internal void GetBattleInfo(Battle battle, Block<Battle> onSuccess, FailBlock onFail)
        {
            if (battle == null)
            {
                onFail.SafeInvoke(-1, "Incorrect incoming parameters");
                return;
            }

            behaviour.GetBattleInfo(battle.id, battle, onSuccess, onFail);
        }

        internal void GetBattleTopUsers(long battleId, Block<List<ClanUser>> onSuccess, FailBlock onFail,
            List<ClanUser> list = null)
        {
            behaviour.GetBattleTopUsers(battleId, list, onSuccess, onFail);
        }

        internal void GrabBattleReward(string playerId, long battleId, Block<NetworkSuccessAnswer> onSuccess, FailBlock onFail)
        {
            behaviour.GrabBattleReward(playerId, battleId, onSuccess, onFail);
        }

        #endregion Battle Event



    }

}