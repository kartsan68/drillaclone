﻿using BlackBears.Clans.View;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Edit.View
{

    [CustomEditor(typeof(StateExtendedInputField))]
    public class StateExtendedInputFieldEditor : UnityEditor.UI.InputFieldEditor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DrawGUI();
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawGUI()
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("Background Sprites");
            EditorGUI.indentLevel += 1;
            EditSpriteProp("emptyFieldBgSpriteName", "Empty");
            EditSpriteProp("filledFieldBgSpriteName", "Filled");
            EditSpriteProp("errorFieldBgSpriteName", "Error");
            EditSpriteProp("activeFieldBgSpriteName", "Active");
            EditorGUI.indentLevel -= 1;
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("Background colors");
            EditorGUI.indentLevel += 1;
			EditColorProp("emptyFieldColor", "Empty");
			EditColorProp("filledFieldColor", "Filled");
			EditColorProp("errorFieldColor", "Error");
			EditColorProp("activeFieldColor", "Active");
            EditorGUI.indentLevel -= 1;
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("Placeholder colors");
            EditorGUI.indentLevel += 1;
			EditColorProp("emptyHintTextColor", "Empty");
			EditColorProp("filledHintTextColor", "Filled");
			EditColorProp("errorHintTextColor", "Error");
			EditColorProp("activeHintTextColor", "Active");
            EditorGUI.indentLevel -= 1;
            EditorGUILayout.EndVertical();
        }

        private void EditSpriteProp(string propName, string fieldName)
        {
            var prop = serializedObject.FindProperty(propName);
            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(string.Format("{0}: {1}", fieldName, prop.stringValue));
            var sprite = (Sprite)EditorGUILayout.ObjectField(null, typeof(Sprite), false, GUILayout.Width(50));
            if (sprite != null) prop.stringValue = sprite.name;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void EditColorProp(string propName, string fieldName)
        {
			var prop = serializedObject.FindProperty(propName);
            prop.colorValue = EditorGUILayout.ColorField(fieldName, prop.colorValue);
        }

    }

}