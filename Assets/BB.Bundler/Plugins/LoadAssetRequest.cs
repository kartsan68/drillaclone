﻿using UnityEngine.Networking;
using UnityEngine;
using System.Collections;

namespace BlackBears.Bundler
{
    public class LoadAssetRequest
    {

        private MonoBehaviour behaviour;
        private BundleInfo bundleInfo;
        private string assetPath;
        private System.Type assetType;

        public UnityEngine.Object Asset { get; private set; }

        private LoadAssetAsync operation;

        public LoadAssetRequest(MonoBehaviour behaviour, BundleInfo bundleInfo,
            string assetPath, System.Type assetType)
        {
            this.behaviour = behaviour;
            this.bundleInfo = bundleInfo;
            this.assetPath = assetPath;
            this.assetType = assetType;
        }

        public float LoadPercent { get; private set; }

        public bool IsLoaded { get; private set; }

        public bool IsFailed { get; private set; }

        public IEnumerator Load()
        {
            if (operation != null) return operation;
            operation = new LoadAssetAsync(this);
            return operation;
        }

        private class LoadAssetAsync : CustomYieldInstruction
        {

            LoadAssetRequest request;

            public LoadAssetAsync(LoadAssetRequest request)
            {
                this.request = request;
                request.behaviour.StartCoroutine(Download());
            }

            public override bool keepWaiting { get { return !request.IsLoaded && !request.IsFailed; } }

            private IEnumerator Download()
            {
                var bundleRequest = new LoadBundleRequest(request.behaviour, request.bundleInfo);
                yield return bundleRequest.Load();
                if (bundleRequest.IsFailed) request.IsFailed = true;
                else yield return TakeAssetFromBundle(bundleRequest.ProxyBundle);
            }

            private IEnumerator TakeAssetFromBundle(IBundleProxy bundle)
            {
                var assetPath = request.assetPath.ToLowerInvariant();
                BundleRequestProxy bundleRequest = bundle.LoadAssetAsync(assetPath, request.assetType);
                yield return bundleRequest;

                request.Asset = bundleRequest.asset;

                bundle.Unload(false);
                request.IsLoaded = true;
            }

        }

    }

}