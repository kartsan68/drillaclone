namespace BlackBears.Bundler
{
    
    public class BundleLoadInfo
    {
        
        public readonly IBundleProxy bundle;
        public readonly BundleInfo info;

        public bool Success { get { return bundle != null; } }

        public BundleLoadInfo(IBundleProxy bundle, BundleInfo info)
        {
            this.bundle = bundle;
            this.info = info;
        }

    }

}