﻿using UnityEngine;

namespace BlackBears.Bundler
{
    
    public interface IBundleProxy
    {

        bool Unloaded { get; }

        string[] GetAllAssetNames();

        Object LoadAsset(string name);
        Object LoadAsset(string name, System.Type type);
        T LoadAsset<T>(string name) where T : Object;

        BundleRequestProxy LoadAssetAsync(string name);
        BundleRequestProxy LoadAssetAsync(string name, System.Type type);
        BundleRequestProxy LoadAssetAsync<T>(string name) where T : Object;

        Object[] LoadAssetsWithSubAssets(string name);
        Object[] LoadAssetsWithSubAssets(string name, System.Type type);
        T[] LoadAssetsWithSubAssets<T>(string name) where T : Object;

        BundleRequestProxy LoadAssetWithSubAssetsAsync(string name);
        BundleRequestProxy LoadAssetWithSubAssetsAsync(string name, System.Type type);
        BundleRequestProxy LoadAssetWithSubAssetsAsync<T>(string name) where T : Object;

        void Unload(bool unloadAllLoadedObjects);

    }

    public abstract class BundleRequestProxy : CustomYieldInstruction
    {

        public abstract Object[] allAssets { get; }

        public abstract Object asset { get; }

        public abstract bool IsDone { get; }

    }

}