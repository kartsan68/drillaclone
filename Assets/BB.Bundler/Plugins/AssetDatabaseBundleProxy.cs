﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlackBears.Bundler
{

    internal class AssetDatabaseBundleProxy : IBundleProxy
    {

        private static readonly Object[] emptyObjectArray = new Object[0];

        private string bundleName;

        internal AssetDatabaseBundleProxy(string bundleName)
        {
            this.bundleName = bundleName.ToLowerInvariant();
        }

        public bool Unloaded => false;

        public string[] GetAllAssetNames()
        {
            #if UNITY_EDITOR
            return UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
            #else 
            return new string[0];
            #endif
        }

        public Object LoadAsset(string name)
        {
            #if UNITY_EDITOR
            return UnityEditor.AssetDatabase.LoadMainAssetAtPath(name);
            #else
            return null;
            #endif
        }

        public Object LoadAsset(string name, System.Type type)
        {
            #if UNITY_EDITOR
            return UnityEditor.AssetDatabase.LoadAssetAtPath(name, type);
            #else
            return null;
            #endif
        }

        public T LoadAsset<T>(string name) where T : Object
        {
            #if UNITY_EDITOR
            return UnityEditor.AssetDatabase.LoadAssetAtPath<T>(name);
            #else
            return null;
            #endif
        }

        public BundleRequestProxy LoadAssetAsync(string name)
        {
            return new AssetDatabaseBundleRequestProxy(LoadAsset(name), emptyObjectArray);
        }

        public BundleRequestProxy LoadAssetAsync(string name, System.Type type)
        {
            return new AssetDatabaseBundleRequestProxy(LoadAsset(name, type), emptyObjectArray);
        }

        public BundleRequestProxy LoadAssetAsync<T>(string name) where T : Object
        {
            return new AssetDatabaseBundleRequestProxy(LoadAsset<T>(name), emptyObjectArray);
        }

        public Object[] LoadAssetsWithSubAssets(string name)
        {
            #if UNITY_EDITOR
            return UnityEditor.AssetDatabase.LoadAllAssetsAtPath(name);
            #else
            return emptyObjectArray;
            #endif
        }

        public Object[] LoadAssetsWithSubAssets(string name, System.Type type)
        {
            #if UNITY_EDITOR
            var assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(name);
            var result = new List<Object>();
            foreach(var asset in assets)
            {
                if (asset.GetType() == type) result.Add(asset);
            }
            return result.ToArray();
            #else
            return emptyObjectArray;
            #endif
        }

        public T[] LoadAssetsWithSubAssets<T>(string name) where T : Object
        {
            Object[] assets = LoadAssetsWithSubAssets(name, typeof(T));
            return assets.Select(o => (T)o).ToArray();
        }

        public BundleRequestProxy LoadAssetWithSubAssetsAsync(string name)
        {
            return new AssetDatabaseBundleRequestProxy(null, LoadAssetsWithSubAssets(name));
        }

        public BundleRequestProxy LoadAssetWithSubAssetsAsync(string name, System.Type type)
        {
            return new AssetDatabaseBundleRequestProxy(null, LoadAssetsWithSubAssets(name, type));
        }

        public BundleRequestProxy LoadAssetWithSubAssetsAsync<T>(string name) where T : Object
        {
            return new AssetDatabaseBundleRequestProxy(null, LoadAssetsWithSubAssets(name, typeof(T)));
        }

        public void Unload(bool unloadAllLoadedObjects)
        {
            ResourceUnloadManager.RequestUnload();
        }

        private class AssetDatabaseBundleRequestProxy : BundleRequestProxy
        {

            private Object innerAsset;
            private Object[] innerAssets;

            public override Object asset { get { return innerAsset; } }

            public override Object[] allAssets { get { return innerAssets; } }

            public override bool IsDone { get { return true; } }

            public override bool keepWaiting { get { return false; } }

            internal AssetDatabaseBundleRequestProxy(Object asset, Object[] assets)
            {
                innerAsset = asset;
                innerAssets = assets;
            }

        }

    }

}