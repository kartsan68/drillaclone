namespace BlackBears.Bundler
{

    class BundleUsingInfo
    {
        
        internal readonly BundleInfo bundleInfo;
        internal IBundleProxy bundle;

        internal int Counter { get; private set; }
        internal bool Loaded { get { return bundle != null; } }
        internal bool UnloadCandidate { get { return Counter <= 0; } }

        internal event System.Action<BundleLoadInfo> requesters;

        internal BundleUsingInfo(BundleInfo bundleInfo)
        {
            this.bundleInfo = bundleInfo;
        }

        internal void IncreaseRequestCount()
        {
            Counter += 1;
        }

        internal void DecreaseRequestCount()
        {
            Counter -= 1;
            UnityEngine.Assertions.Assert.IsTrue(Counter >= 0, 
                "Запросов на выгрузку бандла было больше чем запросов на загрузку.");
        }

        internal void InvokeAndClean()
        {
            if (requesters != null) requesters(new BundleLoadInfo(bundle, bundleInfo));
            requesters = null;
        }

        internal void UnloadBundle(bool unloadAllLoadedObjects)
        {
            if (bundle != null) bundle.Unload(unloadAllLoadedObjects);
            bundle = null;
        }

        internal void ResetRequestCount()
        {
            Counter = 0;
        }
    }

}