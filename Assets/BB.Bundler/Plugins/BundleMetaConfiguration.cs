﻿using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.Bundler
{

    public abstract class BundleMetaConfiguration : ScriptableObject {


        public abstract List<BundleInfo> GenerateAllBundleInfoList();
        public abstract List<BundleInfo> GenerateBundleInfoToCacheList();

        public abstract void ApplyPatch(JSONNode node);

        protected void ApplyPatch(BundleConfiguration configuration, JSONNode node)
        { 
            configuration.ApplyPatch(node);
        }

    }

}