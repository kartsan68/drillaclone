﻿using SimpleJSON;
using UnityEngine;

namespace BlackBears.Bundler
{

    [System.Serializable]
    public class BundleInfo
    {

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private uint version;
        [SerializeField] private bool isLocal;
        [SerializeField] private string localPath;
        [SerializeField] private string remotePathIOS;
        [SerializeField] private string remotePathAndroid;
        [SerializeField] private bool isForceCached;

        public int Id { get { return id; } }
        public string Name { get { return name; } }
        public uint Version { get { return version; } }
        public bool IsLocal { get { return isLocal; } }
        public string LocalPath { get { return localPath; } }
        public string RemotePathIOS { get { return remotePathIOS; } }
        public string RemotePathAndroid { get { return remotePathAndroid; } }
        public bool IsForceCached { get { return isForceCached; } }

        public string Path
        {
            get
            {
                if (IsLocal && LocalPath != null) return LocalPath;
#if UNITY_IOS
                return remotePathIOS;
#elif UNITY_ANDROID
                return remotePathAndroid;
#else
                return null;
#endif
            }
        }


        public BundleInfo() { }

        public BundleInfo(int id, string name, uint version, bool isLocal, string localPath)
        {
            this.id = id;
            this.name = name;
            this.version = version;
            this.isLocal = isLocal;
            this.localPath = localPath;
        }

        internal BundleInfo(JSONNode node)
        {
            if (node == null) return;

            id = node["id"].AsInt;
            name = node["name"].Value;
            version = node["version"].AsUInt;
            isLocal = node["is_local"].AsBool;
            localPath = node["local_path"].Value;
            remotePathIOS = node["remote_path_ios"].Value;
            remotePathAndroid = node["remote_path_android"].Value;
            isForceCached = node["is_force_cached"].AsBool;
        }

        internal void ApplyPatch(BundleInfo patch)
        {
            if (patch == null || patch.Id != Id) return;
            name = patch.name;
            version = patch.version;
            isLocal = patch.isLocal;
            localPath = patch.localPath;
            remotePathIOS = patch.remotePathIOS;
            remotePathAndroid = patch.remotePathAndroid;
            isForceCached = patch.isForceCached;
        }

    }

}