﻿using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.Bundler
{
    
    [CreateAssetMenu(menuName = "BB.Bundler/Bundles Config", fileName = "BundlesConfig")]
    public class BundleConfiguration : ScriptableObject
    {

        [SerializeField] private List<BundleInfo> allBundleInfo;

        private Dictionary<int, BundleInfo> idToBundle;

        public IEnumerable<BundleInfo> AllBundleInfo => allBundleInfo;

        public BundleInfo this [int id]
        {
            get
            {
                CheckDictionary();
                return idToBundle[id];
            }
        }

        public bool TryGetBundle(int id, out BundleInfo info)
        {
            CheckDictionary();
            return idToBundle.TryGetValue(id, out info);
        }

        public bool TryGetBundle(string name, out BundleInfo info)
        {
            foreach(var bundleInfo in allBundleInfo)
            {
                if (bundleInfo == null || !string.Equals(name, bundleInfo.Name, 
                    System.StringComparison.InvariantCultureIgnoreCase)) continue;
                
                info = bundleInfo;
                return true;
            }
            info = null;
            return false;
        }

        internal void ApplyPatch(JSONNode node)
        {
            if (node == null) return;

            node = node["data"];
            for (int i = 0; i < node.Count; ++i)
            {
                PatchInfo(new BundleInfo(node[i]));
            }
        }

        private void PatchInfo(BundleInfo info)
        {
            if (info.Id <= 0) return;

            for (int i = 0; i < allBundleInfo.Count; ++i)
            {
                var old = allBundleInfo[i];
                if (old.Id == info.Id)
                {
                    old.ApplyPatch(info);
                    return;
                }
            }
            allBundleInfo.Add(info);
        }

        private void CheckDictionary()
        {
            if (idToBundle != null) return;

            idToBundle = new Dictionary<int, BundleInfo>();
            foreach (var info in allBundleInfo) idToBundle[info.Id] = info;
        }

    }

}