﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.Bundler
{

    /// <summary>
    /// Отвечает за первоначальную подгрузку и выдачу бандлов.
    /// </summary>
    public class BundlerManager : MonoBehaviour
    {

        [SerializeField] private BundleMetaConfiguration configuration;

        private LoadCacheRequest request;

        private int cacheItemIndex;
        private float cachingItemProgress;

        private Dictionary<BundleInfo, BundleUsingInfo> usingInfoByInfo = new Dictionary<BundleInfo, BundleUsingInfo>();
        private Dictionary<IBundleProxy, BundleUsingInfo> usingInfoByBundle = new Dictionary<IBundleProxy, BundleUsingInfo>();

        public bool OnCachePreparing { get; private set; }
        public bool CachePrepared { get; private set; }

        public int CacheItemCount { get; private set; }

        public event BundlerEventHandler OnCachingStateChanged;
        public event CachingStatusEventHandler OnCachingItemStatusChanged;
        public bool ConfigurationIsExist => configuration != null;

        private void Awake()
        {
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            DontDestroyOnLoad(this);
            #endif
            
            #if DEVELOPMENT_BUILD
            Caching.ClearCache();
            #endif
        }

        public void ApplyPatch(JSONNode node)
        {
            configuration.ApplyPatch(node);
        }

        public void PrepareCache()
        {
            if (OnCachePreparing || CachePrepared) return;
            if (configuration == null) return;

            OnCachePreparing = true;
            List<BundleInfo> bundlesToCache = configuration.GenerateBundleInfoToCacheList();
            request = new LoadCacheRequest(this, bundlesToCache);
            StartCoroutine(PrepareCacheAsync());
        }

        public void ClearBundleCache(BundleInfo bundleInfo)
        {
            string assetsPath = null;
            #if !UNITY_EDITOR && UNITY_IOS
            assetsPath = string.Format("file://{0}/{1}", Application.streamingAssetsPath, bundleInfo.Path);
            #elif !UNITY_EDITOR && UNITY_ANDROID
            assetsPath = string.Format("{0}/{1}", Application.streamingAssetsPath, bundleInfo.Path);
            #endif
            
            if (assetsPath != null) Caching.ClearAllCachedVersions(assetsPath);
        }

        private void CleanCacheObservables()
        {
            OnCachingItemStatusChanged = null;
            CacheItemCount = 0;
        }

        private IEnumerator PrepareCacheAsync()
        {
            CacheItemCount = request.Count;
            var statusEventArgs = new CachingStatusEventArgs(request.Current, request.CurrentProgress);
            if (OnCachingItemStatusChanged != null) OnCachingItemStatusChanged(this, statusEventArgs);

            var bundlerEventArgs = new BundlerEventArgs(true);
            if (OnCachingStateChanged != null) OnCachingStateChanged(this, bundlerEventArgs);

            request.Load();

            while (!request.IsFinished)
            {
                if (statusEventArgs.ItemIndex != request.Current || 
                    statusEventArgs.ItemProgress != request.CurrentProgress)
                {
                    statusEventArgs.ItemIndex = request.Current;
                    statusEventArgs.ItemProgress = request.CurrentProgress;
                    if (OnCachingItemStatusChanged != null) OnCachingItemStatusChanged(this, statusEventArgs);
                } 
                
                yield return null;
            }

            if (request.IsSuccess) CachePrepared = true;
            OnCachePreparing = false;

            bundlerEventArgs = new BundlerEventArgs(false, request.IsFailed ? "Caching failed" : null);
            if (OnCachingStateChanged != null) OnCachingStateChanged(this, bundlerEventArgs);

            CleanCacheObservables();
        }

        public void LoadAssetBundle(BundleInfo bundleInfo, Action<BundleLoadInfo> onFinish)
        {
            if (onFinish == null) return;

            if (bundleInfo == null) 
            {
                onFinish(new BundleLoadInfo(null, bundleInfo));
                return;
            }

            bool needLoad = false;
            BundleUsingInfo usingInfo;
            if (!usingInfoByInfo.TryGetValue(bundleInfo, out usingInfo) || usingInfo == null)
            {
                needLoad = true;
                usingInfo = new BundleUsingInfo(bundleInfo);
                usingInfoByInfo.Add(bundleInfo, usingInfo);
            }
            usingInfo.IncreaseRequestCount();
            if (needLoad) 
            {
                usingInfo.requesters += onFinish;
                StartCoroutine(LoadAssetBundleRoutine(usingInfo));
            }
            else if (!usingInfo.Loaded) 
            {
                usingInfo.requesters += onFinish;
            }
            else
            {
                onFinish(new BundleLoadInfo(usingInfo.bundle, bundleInfo));
            }
        }

        public void UnloadAssetBundle(IBundleProxy bundle)
        {
            if (bundle == null) return;

            BundleUsingInfo usingInfo;
            if (!usingInfoByBundle.TryGetValue(bundle, out usingInfo) || usingInfo == null) return;

            usingInfo.DecreaseRequestCount();
            CheckForFullUnload(usingInfo);
        }

        /// <summary>
        ///     Bundle загрженный данным способом не будет учитываться BundlerManager'ом.
        ///     Используй на свой страх и риск.
        /// </summary>
        public LoadBundleRequest LoadAssetBundleAsync(BundleInfo bundleInfo)
        {
            var request = new LoadBundleRequest(this, bundleInfo);
            return request;
        }

        public void LoadAsset<T>(IBundleProxy bundle, string name, Action<AssetLoadInfo<T>> onFinish) 
            where T : UnityEngine.Object
        {
            StartCoroutine(LoadAssetRoutine(bundle, name, onFinish));
        }

        private void CheckForFullUnload(BundleUsingInfo usingInfo)
        {
            if (usingInfo.UnloadCandidate) 
            {
                usingInfoByBundle.Remove(usingInfo.bundle);
                usingInfoByInfo.Remove(usingInfo.bundleInfo);
                usingInfo.UnloadBundle(true);
            }
        }

        private IEnumerator LoadAssetBundleRoutine(BundleUsingInfo usingInfo)
        {
            var request = LoadAssetBundleAsync(usingInfo.bundleInfo);
            yield return request.Load();

            if (request.IsSuccess) 
            {
                usingInfo.bundle = request.ProxyBundle;
                usingInfoByBundle.Add(usingInfo.bundle, usingInfo);
            }
            usingInfo.InvokeAndClean();

            if (request.IsFailed)
            {
                usingInfoByInfo.Remove(usingInfo.bundleInfo);
                usingInfo.ResetRequestCount();
                CheckForFullUnload(usingInfo);
            }
        }

        private IEnumerator LoadAssetRoutine<T>(IBundleProxy bundle, string name, Action<AssetLoadInfo<T>> onFinish)
            where T : UnityEngine.Object
        {
            if (bundle == null || name == null || bundle.Unloaded)
            {
                if (onFinish != null) onFinish(new AssetLoadInfo<T>(null, bundle));
                yield break;
            }
            var request = bundle.LoadAssetAsync<T>(name);
            yield return request;

            T asset = request.asset as T;
            Debug.AssertFormat(asset != null, string.Format("Can't find asset '{0}' on bundle", name));

            if (onFinish != null) onFinish(new AssetLoadInfo<T>(asset, bundle));
        }

    }
}