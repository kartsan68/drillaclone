﻿namespace BlackBears.Bundler
{

    public class AssetLoadInfo<T> where T : UnityEngine.Object
    {

        public readonly T asset;
        public readonly IBundleProxy bundle;

        public bool Success { get { return asset != null; } }

        public AssetLoadInfo(T asset, IBundleProxy bundle)
        {
            this.asset = asset;
            this.bundle = bundle;
        }

    }

}