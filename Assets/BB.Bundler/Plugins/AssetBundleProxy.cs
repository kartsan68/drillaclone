﻿using UnityEngine;

namespace BlackBears.Bundler
{
    
    internal class AssetBundleProxy : IBundleProxy
    {

        private AssetBundle bundle;

        internal AssetBundleProxy(AssetBundle bundle)
        {
            this.bundle = bundle;
        }

        public bool Unloaded => bundle == null;

        public string[] GetAllAssetNames()
        {
            return bundle.GetAllAssetNames();
        }

        public Object LoadAsset(string name)
        {
            return bundle.LoadAsset(name);
        }

        public Object LoadAsset(string name, System.Type type)
        {
            return bundle.LoadAsset(name, type);
        }

        public T LoadAsset<T>(string name) where T : Object
        {
            return bundle.LoadAsset<T>(name);
        }

        public BundleRequestProxy LoadAssetAsync(string name)
        {
            return new AssetBundleRequestProxy(bundle.LoadAssetAsync(name));
        }

        public BundleRequestProxy LoadAssetAsync(string name, System.Type type)
        {
            return new AssetBundleRequestProxy(bundle.LoadAssetAsync(name, type));
        }

        public BundleRequestProxy LoadAssetAsync<T>(string name) where T : Object
        {
            return new AssetBundleRequestProxy(bundle.LoadAssetAsync<T>(name));
        }

        public Object[] LoadAssetsWithSubAssets(string name)
        {
            return bundle.LoadAssetWithSubAssets(name);
        }

        public Object[] LoadAssetsWithSubAssets(string name, System.Type type)
        {
            return bundle.LoadAssetWithSubAssets(name, type);
        }

        public T[] LoadAssetsWithSubAssets<T>(string name) where T : Object
        {
            return bundle.LoadAssetWithSubAssets<T>(name);
        }

        public BundleRequestProxy LoadAssetWithSubAssetsAsync(string name)
        {
            return new AssetBundleRequestProxy(bundle.LoadAssetWithSubAssetsAsync(name));
        }

        public BundleRequestProxy LoadAssetWithSubAssetsAsync(string name, System.Type type)
        {
            return new AssetBundleRequestProxy(bundle.LoadAssetWithSubAssetsAsync(name, type));
        }

        public BundleRequestProxy LoadAssetWithSubAssetsAsync<T>(string name) where T : Object
        {
            return new AssetBundleRequestProxy(bundle.LoadAssetWithSubAssetsAsync<T>(name));
        }

        public void Unload(bool unloadAllLoadedObjects)
        {
            bundle.Unload(unloadAllLoadedObjects);
        }

        private class AssetBundleRequestProxy : BundleRequestProxy
        {

            private AssetBundleRequest request;

            public override Object asset { get { return request.asset; } }

            public override Object[] allAssets { get { return request.allAssets; } }

            public override bool IsDone { get { return request.isDone; } }

            public override bool keepWaiting { get { return !request.isDone; } }

            internal AssetBundleRequestProxy(AssetBundleRequest request)
            {
                this.request = request;
            }

        }

    }

}