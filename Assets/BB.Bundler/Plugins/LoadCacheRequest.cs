﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System;

namespace BlackBears.Bundler
{
 
    public class LoadCacheRequest : IDisposable
    {

        private MonoBehaviour behaviour;
        private List<BundleInfo> infoList;

        private LoadCacheAsync operation;

        private bool disposed;

        public bool IsSuccess { get; private set; }
        public bool IsFailed { get; private set; }
        public bool IsFinished { get { return IsSuccess || IsFailed; } }

        public int Current { get; private set; }
        public int Count { get { return infoList != null ? infoList.Count : 0; } }

        public float CurrentProgress { get; private set; }

        internal LoadCacheRequest(MonoBehaviour behaviour, List<BundleInfo> infoList)
        {
            this.behaviour = behaviour;
            this.infoList = infoList;
        }

        public void Dispose()
        {
            disposed = true;
            operation = null;
        }

        public IEnumerator Load()
        {
            if (disposed) return null;
            if (operation == null) operation = new LoadCacheAsync(this);
            return operation;
        }

        private class LoadCacheAsync : CustomYieldInstruction
        {

            private LoadCacheRequest request;

            public override bool keepWaiting { get { return !request.IsFinished; } }

            public LoadCacheAsync(LoadCacheRequest request)
            {
                this.request = request;
                request.behaviour.StartCoroutine(Load());
            }

            private IEnumerator Load()
            {
                for (request.Current = 0; request.Current < request.Count; ++request.Current)
                {
                    request.CurrentProgress = 0f;
                    var loadBundleRequest = new LoadBundleRequest(request.behaviour, request.infoList[request.Current]);
                    loadBundleRequest.Load();
                    yield return null;

                    while (!loadBundleRequest.IsFinished)
                    {
                        request.CurrentProgress = loadBundleRequest.Progress;
                        yield return null;
                    }

                    if (loadBundleRequest.IsSuccess)
                    {
                        loadBundleRequest.ProxyBundle.Unload(true);
                    }
                    else
                    {
                        request.IsFailed = true;
                        yield break;
                    }

                    yield return null;
                }
                request.IsSuccess = true;
            }

        }

    }

}