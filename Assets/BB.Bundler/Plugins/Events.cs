namespace BlackBears.Bundler
{
    
    public delegate void BundlerEventHandler(object sender, BundlerEventArgs args);
    public delegate void CachingStatusEventHandler(object sender, CachingStatusEventArgs args);

    public class BundlerEventArgs : System.EventArgs
    {
        
        private readonly bool started;
        private readonly string error;

        public bool IsStarted { get { return started; } }
        public bool IsFinished { get { return !started; } }

        public bool HasError { get { return !string.IsNullOrEmpty(error); } }
        public string Error { get { return error; } }

        public BundlerEventArgs(bool started, string error = null)
        {
            this.started = started;
            this.error = error;
        }

    }

    public class CachingStatusEventArgs : System.EventArgs
    {

        public int ItemIndex { get; internal set; }
        public float ItemProgress { get; internal set; }

        public CachingStatusEventArgs(int itemIndex, float itemProgress)
        {
            this.ItemIndex = itemIndex;
            this.ItemProgress = itemProgress;
        }

    }

}