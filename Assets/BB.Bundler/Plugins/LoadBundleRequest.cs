﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace BlackBears.Bundler
{
    
    public class LoadBundleRequest
    {

        private MonoBehaviour behaviour;
        private BundleInfo bundleInfo;

        #if !UNITY_EDITOR
        private AssetBundle bundle;
        #endif

        private IBundleProxy proxy;

        public IBundleProxy ProxyBundle
        {
            get
            {
                if (proxy == null && IsSuccess)
                {
                    #if UNITY_EDITOR
                    proxy = new AssetDatabaseBundleProxy(bundleInfo.LocalPath);
                    #else
                    proxy = new AssetBundleProxy(bundle);
                    #endif
                }
                return proxy;
            }
        }

        private LoadBundleAsync operation;

        internal LoadBundleRequest(MonoBehaviour behaviour, BundleInfo bundleInfo)
        {
            this.behaviour = behaviour;
            this.bundleInfo = bundleInfo;
        }

        public float Progress { get; private set; }

        public bool IsSuccess { get; private set; }

        public bool IsFailed { get; private set; }

        public bool IsFinished { get { return IsSuccess || IsFailed; } }

        public IEnumerator Load()
        {
            if (operation != null) return operation;
            operation = new LoadBundleAsync(this);
            return operation;
        }

        private class LoadBundleAsync : CustomYieldInstruction
        {

            LoadBundleRequest request;

            public LoadBundleAsync(LoadBundleRequest request)
            {
                this.request = request;
                request.behaviour.StartCoroutine(Download());
            }

            public override bool keepWaiting  { get { return !request.IsFinished; } }

            private IEnumerator Download()
            {
                #if UNITY_EDITOR
                yield return EditorDownload();
                #else
                yield return PlatformDownload();
                #endif
            }

            #if UNITY_EDITOR

            private IEnumerator EditorDownload()
            {
                request.IsSuccess = true;
                yield return null;
            }

            #else

            private IEnumerator PlatformDownload()
            {
                #if UNITY_IOS
                yield return iOSDownload();
                #elif UNITY_ANDROID
                yield return AndroidDownload();
                #else
                yield break;
                #endif
            }

            #endif

            #if !UNITY_EDITOR && UNITY_IOS

            private IEnumerator iOSDownload()
            {
                string assetsPath = string.Format("file://{0}/{1}", Application.streamingAssetsPath, request.bundleInfo.Path);

                using (var www = UnityWebRequestAssetBundle.GetAssetBundle(assetsPath, request.bundleInfo.Version, 0))
                {
                    www.SendWebRequest();

                    while (!www.isDone && !www.isNetworkError)
                    {
                        request.Progress = www.downloadProgress;
                        yield return null;
                    }

                    request.Progress = 1f;

                    if (www.isNetworkError)
                    {
                        request.IsFailed = true;
                        yield break;
                    }

                    request.bundle = DownloadHandlerAssetBundle.GetContent(www);
                    request.IsSuccess = true;
                }

            }

            #endif

            #if !UNITY_EDITOR && UNITY_ANDROID
            
            private IEnumerator AndroidDownload()
            {
                string assetsPath = string.Format("{0}/{1}", 
                                        Application.streamingAssetsPath, request.bundleInfo.Path);

                WWW www = WWW.LoadFromCacheOrDownload(assetsPath, (int)request.bundleInfo.Version);

                using (www)
                {
                    yield return www;

                    while (!www.isDone)
                    {
                        request.Progress = www.progress;
                        yield return null;
                    }

                    if (!string.IsNullOrEmpty(www.error))
                    {
                        request.IsFailed = true;
                        yield break;
                    }

                    request.bundle = www.assetBundle;
                    request.IsSuccess = true;
                }
            }

            #endif

        }
    }
}