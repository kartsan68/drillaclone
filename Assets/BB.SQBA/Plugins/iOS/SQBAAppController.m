//  SQBAAppController.m
//  Unity-iPhone
//
//  Created by Ilya S. on 14/12/2017.
//

#import "SQBAAppController.h"
#import "SQBANative.h"

@implementation SQBAAppController
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    [SQBANative processOpenUrl:url];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [SQBANative processOpenUrl:url];

    return YES;
}

@end

#ifdef USE_SQBA_APP_CONTROLLER
IMPL_APP_CONTROLLER_SUBCLASS(SQBAAppController)
#endif
