//
//  OverriddenAppController.m
//  Unity-iPhone
//
//  Created by Ilya S. on 14/12/2017.
//

#import <Foundation/Foundation.h>
#import "UnityAppController.h"


// Overriding Unity AppDelegate methods
@interface SQBAAppController : UnityAppController
@end
