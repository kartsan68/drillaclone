﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace BlackBears.SQBA.Utils
{
    static class Unzipper
    {
        internal static void UnzipArchive(string archivePath, string destinationPath, string password, Block onSuccess, FailBlock onFail)
        {
            ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = 0;
            
            bool isSuccess = false;
            string errorMessage = null;

            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archivePath);
                zf = new ZipFile(fs);

                if (!String.IsNullOrEmpty(password)) zf.Password = password;

                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile) continue;

                    string entryFileName = zipEntry.Name;

                    byte[] buffer = new byte[4096];
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    string fullZipPath = Path.Combine(destinationPath, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipPath);

                    if (directoryName.Length > 0) Directory.CreateDirectory(directoryName);

                    using (FileStream streamWriter = File.Create(fullZipPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }

                isSuccess = true;
            }
            catch (Exception e)
            {
                isSuccess = false;
                errorMessage = e.Message;
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true;
                    zf.Close();
                }
            }

            if (isSuccess) onSuccess.SafeInvoke();
            else onFail.SafeInvoke(FailCode.Unzipping, errorMessage);
        }
    }
}
