﻿using SimpleJSON;
using BlackBears.SQBA.Modules.Purchasing;
using BlackBears.SQBA.General;

namespace BlackBears.SQBA.Utils
{
    
    public delegate void PurchaseItemBlock(PurchaseSQBAItem[] inApps);
    public delegate void PurchasesGlobalBlock(string iapName, FailCode errorCode = FailCode.Common, string errorMessage = null);

    static class Common
    {
        internal const int sdkVersion = 1;
        internal const string stateFileName = "state.dic";
        internal const string sqbaFilesArchivePassword = "";

        internal static string Version { get { return Manager.Instance.State.Configuration.Version; } }
        internal static string ApiUrl { get { return Manager.Instance.State.Configuration.ApiUrl; } }
        internal static string SecretKey { get { return Manager.Instance.State.Configuration.SecretKey; } }

        internal static void SafeInvoke(this FailBlock b, FailCode f, string m)
        {
            if (b != null) b((long)f, m);
        }

        internal static void SafeInvoke(this Block<JSONNode> b, JSONNode n)
        {
            if (b != null) b(n);
        }

        internal static void SafeInvoke(this PurchaseItemBlock b, PurchaseSQBAItem[] a)
        {
            if (b != null) b(a);
        }

        internal static void SafeInvoke(this PurchasesGlobalBlock b, string iapName, FailCode f = FailCode.Common, string m = null)
        {
            if (b != null) b(iapName, f, m);
        }
    }
}
