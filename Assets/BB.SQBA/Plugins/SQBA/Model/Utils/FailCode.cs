﻿namespace BlackBears.SQBA.Utils
{
    public enum FailCode : long
    {
        Common = -1,
        NotInitializedProperly = 1,
        ApiKeyNotProvided = 2,
        ConfigurationNotProvided = 3,
        NetworkRequest = 4,

        //Покупки
        PurchasesModuleNotInitialized = 5,
        PurchaseRestorationInProcess = 6,
        PurchaseRestorationFailed = 7,
        PurchasePriceUpdateFailed = 15,
        FailedPurchase = 8,
        FailedPurchaseValidation = 9,
        
        ProcessingArchive = 10,
        Unzipping = 11,
        EmptyPendingQuery = 12,
        IncorrectPendingCode = 13,
        FailedSubscriptionCheck = 14
    }
}
