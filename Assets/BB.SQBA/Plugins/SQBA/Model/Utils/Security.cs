using BlackSec = BlackBears.Utils.Security;

namespace BlackBears.SQBA.Utils
{

    internal class Security
    {
        
        internal static string GetServerKey(string input, string randKey, string format = "{0}{1}{2}")
        {
            var baseString = string.Format(format, input, randKey, Common.SecretKey);
            return BlackSec.GetMD5Hash(baseString, BlackSec.MD5Format.lower);
        }

        internal static bool IsServerKeyValid(string serverKey, string input, 
            string randKey)
        {
            return string.Equals(serverKey, GetServerKey(input, randKey));
        }

    }

}
