﻿namespace BlackBears.SQBA.Networking
{
    class NetworkKeys
    {
        // version/check
        internal const string kApiKey = "sqba_key";
        internal const string kAppId = "app_id";
        internal const string kAppVersion = "rev";
        internal const string kDeviceId = "udid";
        internal const string kDeviceModel = "res";
        internal const string kFilesVersion = "ver";
        internal const string kFilesGroup = "player_group";
        internal const string kSQBAVersion = "qba_version";
        internal const string kSQBAModules = "mods";
        internal const string kSecurityKey = "key";
        internal const string kSecurityRand = "rand_key";


        //appcheck
        internal const string kAppName = "app_name";
        internal const string kAppIcon = "icon";
        internal const string kUrlScheme = "app_url";


        //inapp.validate
        internal const string kProductAmount = "amount";
        internal const string kProductId = "product_id";
        //facebook
        internal const string kAdvertisingIdentifier = "device_id";
        internal const string kPlayerIdentifier = "game_player_id";
        internal const string kExtInfo = "extinfo";
        //ios
        internal const string kProductReceipt = "trans_receipt";
        internal const string kProductCurrency = "currency";
        internal const string kFacebookTrackingEnabled = "ios_ad_tracking";
        internal const string kNotSave = "not_save";
        internal const string kTransactionId = @"transaction_id";
        //android
        internal const string kProductSignature = "signature";
        internal const string kProductResponseData = "response_data";
        internal const string kIsSubscription = "subs";


        //codes.use
        internal const string kBonusCode = "code";

        //privacy.agreement
        internal const string kCountryCode = "country";
    }
}
