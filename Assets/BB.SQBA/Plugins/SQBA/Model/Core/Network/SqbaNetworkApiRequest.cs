using BlackBears.GameCore.Networking;
using BlackBears.SQBA.Utils;
using SimpleJSON;

namespace BlackBears.SQBA.Networking
{

    internal sealed class SqbaNetworkApiRequest : NetworkApiRequest
    {

        internal SqbaNetworkApiRequest(string command, JSONNode data, Block<JSONNode> onSuccess, FailBlock onFail)
            : base(data, onSuccess, onFail)
        {
            url = string.Format("{0}/{1}", Common.ApiUrl, command);

            AddRequestValue(NetworkKeys.kSQBAVersion, Common.Version);
        }

        protected override void Prepare()
        {
            Logger.LogRequest(url, Data);
            base.Prepare();
        }

        protected override void ProcessError(long code, string message)
        {
            Logger.LogError(code, message);
            base.ProcessError(code, message);
        }

    }

}