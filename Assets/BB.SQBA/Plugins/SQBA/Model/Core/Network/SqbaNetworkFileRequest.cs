using BlackBears.GameCore.Networking;
using BlackBears.SQBA.Utils;

namespace BlackBears.SQBA.Networking
{

    internal sealed class SqbaNetworkFileRequest : NetworkFileRequest
    {

        private const string fileNameTemplate = "sqba_archive_{0}.tmp";

        internal SqbaNetworkFileRequest(string urlString, Block<string> onSuccess, FailBlock onFail)
            : base(fileNameTemplate, urlString, onSuccess, onFail)
        {
            AddRequestValue(NetworkKeys.kSQBAVersion, Common.Version);
        }

        protected override void Prepare()
        {
            base.Prepare();
        }

        protected override void PrepareFinished(string filename)
        {
            Logger.LogMessage("Start downloading file " + filename + " from: " + url);
        }

        protected override void DownloadFinished()
        {
            Logger.LogMessage("Downloaded file size: " + request.downloadedBytes / 1024 + "Kb");
        }


    }

}