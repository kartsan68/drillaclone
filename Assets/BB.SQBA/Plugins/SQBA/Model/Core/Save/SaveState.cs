﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BlackBears.SQBA.Saving
{
    [Serializable]
    class SaveState : ISaveState, IDeserializationCallback
    {
        List<Save> saves = new List<Save>();

        [NonSerialized]
        List<ISavable> savables;

        internal SaveState()
        {
            Initialization();

            Logger.LogMessage("Save State initialized");
        }

        List<Save> ISaveState.Saves
        {
            get { return saves; }
            set { saves = value; }
        }

        List<ISavable> ISaveState.Savables
        {
            get { return savables; }
            set { savables = value; }
        }

        void ISaveState.Clean()
        {
            saves = null;

            Logger.LogMessage("Save State cleaned");
        }

        void IDeserializationCallback.OnDeserialization(object sender)
        {
            Initialization();

            Logger.LogMessage("Save State deserialized");
        }

        void Initialization()
        {
            savables = new List<ISavable>();
        }
    }
}
