﻿namespace BlackBears.SQBA.Saving
{
    interface ISavable
    {
        Save ToSave();
    }
}
