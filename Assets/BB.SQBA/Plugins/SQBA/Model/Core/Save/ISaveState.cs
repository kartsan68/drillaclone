﻿using System.Collections.Generic;

namespace BlackBears.SQBA.Saving
{
    interface ISaveState
    {
        List<ISavable> Savables { get; set; }
        List<Save> Saves { get; set; }

        void Clean();
    }
}
