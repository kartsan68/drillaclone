﻿namespace BlackBears.SQBA.Saving
{
    interface IEncodable
    {
        SimpleJSON.JSONNode EncodeData();
        void DecodeData(SimpleJSON.JSONNode data);
    }
}
