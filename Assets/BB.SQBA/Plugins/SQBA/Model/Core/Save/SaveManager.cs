﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using BlackBears.SQBA.Utils;
using UnityEngine.Events;

namespace BlackBears.SQBA.Saving
{
    class SaveManager
    {
        ISaveState state;

        private string stateFile;
        private string stateTempFile;

        private UnityEvent onSaveFailed = new UnityEvent();

        internal void LoadSaveManager(Block onSuccess)
        {
            Logger.LogMessage("SaveManager initializing ...");

            LoadStateAsync(() =>
            {
                Logger.LogMessage("SaveManager initialized.");
                onSuccess.SafeInvoke();
            }, StateFile);
        }

        internal UnityEvent OnSaveFailed => onSaveFailed;

        string StateFile
        {
            get
            {
                if (stateFile == null) stateFile = $"{Core.Instance.StorageDirectory}/{Common.stateFileName}";
                return stateFile;
            }
        }

        string StateTempFile
        {
            get
            {
                if (stateTempFile == null) stateTempFile = $"{StateFile}.tmp";
                return stateTempFile;
            }
        }

        internal Save GetSave(string key)
        {
            foreach (Save save in state.Saves)
            {
                if (key.Equals(save.Key))
                {
                    return save;
                }
            }

            return null;
        }

        internal void AddSavable(ISavable savable)
        {
            state.Savables.Add(savable);
        }

        internal void SaveState()
        {
            Logger.LogMessage("Trying to save state");

            PrepareForSaving();

            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                using (StreamWriter writer = File.CreateText(StateTempFile))
                {
                    formatter.Serialize(writer.BaseStream, state);
                    writer.Flush();
                }

                if (File.Exists(StateFile)) File.Replace(StateTempFile, StateFile, null);
                else File.Move(StateTempFile, StateFile);
                Logger.LogMessage("State saved");
            }
            catch
            {
                onSaveFailed.Invoke();
            }
        }

        internal void Clean()
        {
            state.Clean();
        }

        void PrepareForSaving()
        {
            List<Save> saves = new List<Save>();
            foreach (ISavable savable in state.Savables)
            {
                if(savable == null) continue;
                Save save = savable.ToSave();
                if (save != null) saves.Add(save);
            }
            state.Saves = saves;
        }

        async void LoadStateAsync(Block onSuccess, string StateFile)
        {
            await Task.Run(() => LoadState(StateFile));
            onSuccess.SafeInvoke();
        }

        void LoadState(string StateFile)
        {
            Logger.LogMessage("Trying to load state file...");

            if (File.Exists(StateFile))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream fs = new FileStream(StateFile, FileMode.Open))
                {
                    state = formatter.Deserialize(fs) as ISaveState;

                    Logger.LogMessage("State file successfully loaded");
                }
            }
            else
            {
                state = new SaveState() as ISaveState;

                Logger.LogMessage("Can't find state file. New state created");
            }
        }
    }
}
