﻿namespace BlackBears.SQBA.Saving
{
    [System.Serializable]
    class Save
    {
        string key;
        string data;

        internal string Key { get { return key; } }
        internal string Data { get { return data; } }

        internal Save(string key, string data)
        {
            this.key = key;
            this.data = data;
        }
    }
}
