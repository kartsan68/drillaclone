﻿namespace BlackBears.SQBA.General
{
    interface IApplicationManager
    {
        void OnPauseApplication();
        void OnResumeApplication();
    }
}
