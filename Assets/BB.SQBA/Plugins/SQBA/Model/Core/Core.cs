﻿using System;
using System.IO;
using UnityEngine;

namespace BlackBears.SQBA
{
    class Core
    {
        static Core instance;
        string storageDirectory;

        internal Core()
        {
            storageDirectory = String.Format("{0}/sqba", Application.persistentDataPath);

            SetupDirectories();
        }

        internal static Core Instance { get { return instance ?? (instance = new Core()); } }

        public static void Dispose()
        {
            instance = null;
        }

        internal string StorageDirectory
        {
            get { return storageDirectory; }
            private set { storageDirectory = value; }
        }

        internal void SetupDirectories()
        {
            if (!Directory.Exists(storageDirectory))
                Directory.CreateDirectory(storageDirectory);
        }
    }
}
