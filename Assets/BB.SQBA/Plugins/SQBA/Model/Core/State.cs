﻿using System.Collections.Generic;
using BlackBears.SQBA.Modules;
using BlackBears.SQBA.Native;
using BlackBears.SQBA.Native.Android;
using BlackBears.SQBA.Native.Editor;
using BlackBears.SQBA.Native.iOS;
using BlackBears.SQBA.Saving;
using BlackBears.SQBA.Utils;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.SQBA.General
{
    class State : IState, IEncodable
    {
        static string kDeviceModel = "model";
        static string kDeviceOS = "system";
        static string kSDKVersion = "version";
        static string kNotFirstSave = "not_first_save";

        Configuration configuration;

        INativeAdapter nativeDataAdapter;

        bool firstLaunch = true;
        bool firstSave = true;
        bool noInternetConection = false;

        string appName;
        string appId;
        string appVersion;

        string deviceModel;
        string deviceId;
        string deviceOS;
        string deviceOSVersion;

        int sdkVersion;

        List<Module> modules;

        bool needForceUpdate;
        bool ratingEnabled;
        bool isProduction;
        bool useLocalFiles;
        long dayXTimestamp;

        public INativeAdapter NativeDataAdapter
        {
            get { return nativeDataAdapter; }
            set { nativeDataAdapter = value; }
        }

        public bool FirstLaunch
        {
            get { return firstLaunch; }
            set { firstLaunch = value; }
        }

        public bool NoInternetConection
        {
            get { return noInternetConection; }
            set { noInternetConection = value; }
        }

        public bool FirstSave
        {
            get { return firstSave; }
            set { firstSave = value; }
        }

        public int SDKVersion
        {
            get { return sdkVersion; }
            set { sdkVersion = value; }
        }

        public List<Module> Modules
        {
            get { return modules; }
            set { modules = value; }
        }

        public string ActiveModulesString
        {
            get
            {
                string activeModulesString = "";
                foreach (Module module in Modules)
                {
                    if (module.State != null && module.State.IsActive) activeModulesString += module.Abbrevation;
                }
                return activeModulesString;
            }
        }


        public bool NeedForceUpdate
        {
            get { return needForceUpdate; }
            set { needForceUpdate = value; }
        }

        public bool RatingEnabled
        {
            get { return ratingEnabled; }
            set { ratingEnabled = value; }
        }

        public bool IsProduction
        {
            get { return isProduction; }
            set { isProduction = value; }
        }

        public bool UseLocalFiles
        {
            get { return useLocalFiles; }
            set { useLocalFiles = value; }
        }

        public long DayXTimestamp
        {
            get { return dayXTimestamp; }
            set { dayXTimestamp = value; }
        }

        public string ApiKey { get { return configuration.ApiKey; } }
        public string UrlScheme { get { return configuration.UrlScheme; } }

        public string AppName { get { return appName; } }
        public string AppId { get { return appId; } }
        public string AppVersion { get { return appVersion; } }

        public string DeviceModel { get { return deviceModel; } }
        public string DeviceId { get { return deviceId; } }
        public string DeviceOS { get { return deviceOS; } }
        public string DeviceOSVersion { get { return deviceOSVersion; } }
        public string CurrentLocale { get; private set; }

        public string AdvertisingIdentifier { get { return nativeDataAdapter.AdvertisingIdentifier; } }
        public string FacebookExtInfo { get { return nativeDataAdapter.FacebookExtInfo; } }

        public Configuration Configuration
        {
            get { return configuration; }
            set { configuration = value; }
        }

        internal State(JSONNode data)
        {
            Initialize();

            DecodeData(data);
        }

        void Initialize()
        {
            InitializeApplicationFields();
            InitializeDeviceFields();

            sdkVersion = Common.sdkVersion;
        }

        public JSONNode EncodeData()
        {
            var json = new JSONObject();
            json[kDeviceModel] = DeviceModel;
            json[kDeviceOS] = DeviceOS;
            json[kSDKVersion] = SDKVersion;
            json[kNotFirstSave] = FirstSave;
            return json;
        }

        public void DecodeData(JSONNode data)
        {
            if (data == null || data.IsNull) return;

            sdkVersion = data[kSDKVersion].AsInt;
            firstSave = !data[kNotFirstSave].AsBool;

            firstLaunch = false;
        }

        void InitializeApplicationFields()
        {
            appVersion = Application.version;
            appId = Application.identifier;
            appName = Application.productName;
        }

        void InitializeDeviceFields()
        {
            deviceModel = SystemInfo.deviceModel;

            InitializeNativeFields();
        }

        void InitializeNativeFields()
        {
#if UNITY_EDITOR
            nativeDataAdapter = new EditorAdapter() as INativeAdapter;
#elif UNITY_IOS
            nativeDataAdapter = new IOSAdapter() as INativeAdapter;
#elif UNITY_ANDROID
            nativeDataAdapter = new AndroidAdapter() as INativeAdapter;
#endif

#if UNITY_IOS
            deviceOS = "ios";
#elif UNITY_ANDROID
            deviceOS = "and";
#endif

            if (nativeDataAdapter != null)
            {
                deviceId = nativeDataAdapter.DeviceId;
                deviceOSVersion = nativeDataAdapter.DeviceOSVersion;
                CurrentLocale = nativeDataAdapter.CurrentLocale;
            }
        }
    }
}
