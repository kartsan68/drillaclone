﻿using BlackBears.SQBA.Native;

namespace BlackBears.SQBA.General
{
    interface IState
    {
        INativeAdapter NativeDataAdapter { get; }

        string AppName { get; }
        string AppId { get; }
        string AppVersion { get; }
        string ApiKey { get; }

        string UrlScheme { get; }

        string DeviceModel { get; }
        string DeviceId { get; }
        string DeviceOS { get; }
        string DeviceOSVersion { get; }
        string CurrentLocale { get; }

        string AdvertisingIdentifier { get; }
        string FacebookExtInfo { get; }

        int SDKVersion { get; }

        string ActiveModulesString { get; }

        bool NeedForceUpdate { get; }
        bool RatingEnabled { get; }
        bool IsProduction { get; }
        bool UseLocalFiles { get; }
        long DayXTimestamp { get; }

        bool NoInternetConection { get; }

        Configuration Configuration { get; }
    }
}
