using SimpleJSON;

namespace BlackBears.SQBA.General
{
	public class PurchaserData
	{	
		public readonly int id;
		public readonly PurchaserType type;
		public readonly string name;
		public readonly float minPurchaseSum;
		public readonly int minDays;

        public PurchaserData(JSONNode node)
        {
			id = node["id"].AsInt;
            type = (PurchaserType)id;
			name = node["name"];
			minPurchaseSum = node["min_purchase"].AsFloat;
			minDays = node["min_days"].AsInt;
        }
	}

    public enum PurchaserType
    {
        NEWBIE = 1,
        MEANIE,
        POORGUY,
        MINNOW,
        DOLPHIN,
        WHALE,
    }

}
