﻿using BlackBears.SQBA.Utils;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.SQBA
{
    class Logger
    {

        public static bool Verbose { get; set; }

        internal static void LogMessage(string message)
        {
            LogFormat("SQBA:: {0}", message);
        }

        internal static void LogError(long errorCode, string message)
        {
            LogError((FailCode)errorCode, message);
        }

        internal static void LogError(FailCode errorCode, string message)
        {
            LogFormat("SQBA:: Error: code: {0} message: {1}", errorCode, message);
        }

        internal static void LogRequest(string command, JSONNode parameters)
        {
            string prettyJson = parameters.ToString().Replace("\\", "");
            prettyJson = prettyJson.Replace("\"{", "{");
            prettyJson = prettyJson.Replace("}\"", "}");

            LogFormat("SQBA:: WEB-Request: command: {0} parameters: {1}", command, prettyJson);
        }

        internal static void LogFormat(string format, params object[] args)
        {
            if (Verbose) Debug.LogFormat(format, args);
        }
    }
}
