﻿using System;
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.SQBA.Modules;
using BlackBears.SQBA.Modules.Codes;
using BlackBears.SQBA.Modules.Files;
using BlackBears.SQBA.Modules.GDPR;
using BlackBears.SQBA.Modules.Purchasing;
using BlackBears.SQBA.Networking;
using BlackBears.SQBA.Saving;
using BlackBears.SQBA.Utils;
using BlackBears.Utils;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Events;

using UnityObject = UnityEngine.Object;

namespace BlackBears.SQBA.General
{
    public class Manager : ISavable, IApplicationManager
    {
        #region Fields
        static string kStateName = "state";
        static string kGameObjectName = "SQBA";
        static string kMaxPurchasedPrice = "max_purchased_price";

        static Manager instance;

        bool isLaunchProcessed;

        State state;
        INetworkBehaviour networkBehaviour;
        ApplicationBehaviour applicationBehaviour;
        SaveManager saveManager;

        FilesModule filesModule;
        PurchasesModule purchasesModule;
        CodesModule codesModule;
        GDPRModule gdprModule;

        Block<JSONNode> bonusDataCallback;
        PurchaserData[] purchasers;
        double maxPurchasedPrice = 0;

        #endregion

        #region Properties
        internal static Manager Instance { get { return instance ?? (instance = new Manager()); } }
        internal IState State { get { return state as IState; } }

        internal UnityEvent OnSaveFailed => saveManager.OnSaveFailed;

        internal FilesModule FilesModule { get { return filesModule; } }
        internal FilesState FilesState { get { return filesModule.State as FilesState; } }

        internal PurchasesModule PurchasesModule { get { return purchasesModule; } }

        internal CodesModule CodesModule { get { return codesModule; } }
        internal CodesState CodesState { get { return codesModule.State as CodesState; } }

        internal GDPRModule GDPRModule { get { return gdprModule; } }

        internal float StoreTax { get; private set; }

        internal Block<JSONNode> BonusDataCallback
        {
            set
            {
                bonusDataCallback = value;
                CheckPendingBonusData();
            }
        }
        #endregion


        #region Methods

        public static void CreateManager(Block<Manager> onSuccess)
        {
            instance = new Manager();
            instance.InitManager(() => onSuccess.SafeInvoke(instance));
        }

        private void InitManager(Block onSuccess)
        {
            Logger.LogMessage("Manager initializing...");

            SetupBehaviours();

            saveManager = new SaveManager();

            saveManager.LoadSaveManager(() =>
           {
               saveManager.AddSavable(this);

               LoadState();

               saveManager.Clean();

               Logger.LogMessage("Manager initialized.");

               onSuccess.SafeInvoke();
           });
        }

        public static void Dispose()
        {
            instance = null;
        }

        public void AddPurchase(double sum)
        {
            maxPurchasedPrice = Math.Max(maxPurchasedPrice, sum);
        }

        internal void FromJson(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None) return;

            maxPurchasedPrice = json[kMaxPurchasedPrice].AsDouble;
        }

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[kMaxPurchasedPrice] = maxPurchasedPrice;
            return json;
        }

        internal PurchaserType GetPurchaserType()
        {
            var totalDays = DateUtils.SecondsToFullDays(BBGC.Features.GetFeature<ChronometerFeature>().TotalTime);

            PurchaserType choseType = PurchaserType.NEWBIE;

            foreach (var purchaser in purchasers)
            {
                if (maxPurchasedPrice < purchaser.minPurchaseSum) continue;
                if (totalDays < purchaser.minDays) continue;
                choseType = purchaser.type;
            }
            return choseType;
        }

        internal void Start(Configuration configuration)
        {
            state.Configuration = configuration;

            var sqbaFiles = BBGC.Features.GetFeature<GameCore.Features.SQBAFiles.SQBAFilesFeature>();
            var deafultConfig = sqbaFiles.TextByName(configuration.ConfigName);
            var node = JSONNode.Parse(deafultConfig);

            var purchasersNode = node["purchasers"].AsArray;
            purchasers = new PurchaserData[purchasersNode.Count];
            for (int i = 0; i < purchasers.Length; i++)
            {
                purchasers[i] = new PurchaserData(purchasersNode[i]);
            }

            StoreTax = node["store_tax"].AsFloat;

            Logger.LogMessage("Starting with API key: " + state.ApiKey);
        }

        internal void VersionCheck(Block onSuccess, FailBlock onFail)
        {
            if (state.FirstLaunch)
            {
                ProcessFirstLaunchStuff(onSuccess, onFail);
            }
            else
            {
                ProcessLaunchStuff(onSuccess, onFail);
            }
        }

        internal void SaveStates()
        {
            saveManager.SaveState();
        }

        internal void SignUpPrivacyPolicy(string playerId, string countryCode, Block<GDPRStatus> onSuccess, FailBlock onFail)
        {
            gdprModule.ProcessPrivacyPolicySignUp(playerId, countryCode, onSuccess, onFail);
        }

        void IApplicationManager.OnPauseApplication()
        {
            Logger.LogMessage("Pause Application");

            foreach (Module module in state.Modules) module.OnPauseApplication();
            saveManager.SaveState();
        }

        void IApplicationManager.OnResumeApplication()
        {
            Logger.LogMessage("Resume Application");

            foreach (Module module in state.Modules) module.OnResumeApplication();
            if (isLaunchProcessed && state.Configuration.VersionCheckOnResume) VersionCheck(null, null);

            CheckPendingBonusData();
        }

        Save ISavable.ToSave()
        {
            return new Save(kStateName, state.EncodeData().ToString());
        }

        void CheckPendingBonusData()
        {
            if (bonusDataCallback == null) return;

            CodesModule.CheckPendingQuery(() =>
            {
                if (CodesState.PendingBonusData == null) return;

                bonusDataCallback.SafeInvoke(CodesState.PendingBonusData);
                CodesState.PendingBonusData = null;
            });
        }

        void SetupBehaviours()
        {
            var go = new GameObject(kGameObjectName);
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityObject.DontDestroyOnLoad(go);
#endif

            networkBehaviour = go.AddComponent<NetworkBehaviour>() as INetworkBehaviour;

            applicationBehaviour = go.AddComponent<ApplicationBehaviour>();
            applicationBehaviour.ApplicationManager = this;
        }

        void ProcessFirstLaunchStuff(Block onSuccess, FailBlock onFail)
        {
            Logger.LogMessage("Processing first launch stuff...");

            state.FirstLaunch = false;

            networkBehaviour.RequestFirstLaunch((JSONNode data) =>
            {
                Logger.LogMessage("First launch processed.");

                ProcessLaunchStuff(onSuccess, onFail);
            }, (errorCode, message) =>
            {
                state.NoInternetConection = true;
                Logger.LogError(errorCode, message);
                onFail.SafeInvoke(errorCode, message);
            });
        }

        void ProcessLaunchStuff(Block onSuccess, FailBlock onFail)
        {
            Logger.LogMessage("Processing launch stuff...");

            JSONObject parameters = new JSONObject();
            if (state.FirstSave) parameters.Add("first", "1");
            if (state.Configuration.ForceFilesCheck) parameters.Add("force_files", "1");
            foreach (Module module in state.Modules)
            {
                module.AddParametersToServerGlobarRequestParameters(parameters);
            }

            networkBehaviour.RequestVersionCheck(parameters, (JSONNode data) =>
            {
                if (state.FirstSave) state.FirstSave = false;

                Logger.LogRequest("version/check", data);

                JSONObject info = data["q"].AsObject;

                ProcessAdditionalInfo(info);

                foreach (Module module in state.Modules)
                {
                    if (FilesState.IsRuntimeReplaceAllowed && module == filesModule)
                    {
                        filesModule.ForceVersionCheckForServerData(data, onSuccess, onFail);
                        continue;
                    }

                    module.ProcessServerGlobalResponse(data);
                }

                if (!FilesState.IsRuntimeReplaceAllowed) onSuccess.SafeInvoke();

                if (info != null)
                {
                    bool needAppInfo = info["need_appinfo"].AsBool;
                    if (needAppInfo) SendAppAdditionalInfo();
                }

                isLaunchProcessed = true;
            }, (errorCode, message) =>
            {
                state.NoInternetConection = true;

                Logger.LogError(errorCode, message);
                onFail.SafeInvoke(errorCode, message);

                isLaunchProcessed = true;
            });
        }

        void ProcessAdditionalInfo(JSONNode info)
        {
            if (info == null) return;

            Logger.LogMessage("Processing additional info.");

            state.NeedForceUpdate = info["need_forced_update"].AsBool;
            state.RatingEnabled = info["show_rating"].AsBool;
            state.DayXTimestamp = info["time_rating_reset"].AsLong;
            state.IsProduction = info["is_prod"].AsBool;
            state.UseLocalFiles = info["use_local_files"].AsBool;
        }

        void SendAppAdditionalInfo()
        {
            Logger.LogMessage("Sending additional app info");

            JSONObject parameters = new JSONObject();
            networkBehaviour.SendAdditionalAppInfo(parameters, (data) =>
            {
                Logger.LogMessage("Additional App Info sended");
            }, Logger.LogError);
        }

        void LoadState()
        {
            Logger.LogMessage("Loading General State");

            Save save = saveManager.GetSave(kStateName);
            if (save != null) state = new State(JSON.Parse(save.Data));
            else state = new State(null);

            bool forceCreation = state.SDKVersion != Common.sdkVersion;
            if (forceCreation) state.SDKVersion = Common.sdkVersion;

            LoadModules(forceCreation);

            Logger.LogMessage("General State loaded");
        }

        void LoadModules(bool forceCreation)
        {
            Logger.LogMessage("Loading modules...");

            ModulesFactory modulesFactory = new ModulesFactory(forceCreation ? null : saveManager);

            List<Module> modules = new List<Module>(ModulesFactory.modulesCount);

            filesModule = modulesFactory.CreateFileModule(networkBehaviour, state);
            modules.Add(filesModule);

            purchasesModule = modulesFactory.CreatePurchasesModule(networkBehaviour);
            if (purchasesModule != null) modules.Add(purchasesModule);

            codesModule = modulesFactory.CreateCodesModule(networkBehaviour);
            modules.Add(codesModule);

            gdprModule = modulesFactory.CreateGDPRModule(networkBehaviour);
            modules.Add(gdprModule);

            state.Modules = modules;

            Logger.LogMessage("Modules loaded.");
        }
        #endregion
    }
}
