﻿using BlackBears.SQBA.Networking;
using BlackBears.SQBA.General;
using BlackBears.SQBA.Utils;
using BlackBears.SQBA.Native;

namespace BlackBears.SQBA.Modules.Codes
{
    class CodesModule : Module
    {
        CodesState codesState;

        internal CodesModule(INetworkBehaviour networkBehaviour) : base(networkBehaviour)
        {
            codesState = new CodesState();

            Activate();

            Logger.LogMessage("Codes module created.");
        }

        internal override string Abbrevation { get { return "c"; } }

        protected override ModuleState ModuleState { get { return codesState; } }

        internal void CheckPendingQuery(Block onSuccess, FailBlock onFail = null)
        {
            INativeAdapter adapter = Manager.Instance.State.NativeDataAdapter;

            string query = adapter.PendingQuery;
            if (query == null)
            {
                onFail.SafeInvoke(FailCode.EmptyPendingQuery, "There is no pending query.");
                return;
            }
            if (!query.Contains("code="))
            {
                onFail.SafeInvoke(FailCode.IncorrectPendingCode, "There is wrong pending code within query");
                return;
            }

            string code = query.Replace("code=", "");
            networkBehaviour.RequestRedeemCode(code, (data) =>
            {
                codesState.PendingBonusData = data["action_data"].AsObject;
                onSuccess.SafeInvoke();
            }, onFail);
        }
    }
}
