﻿using SimpleJSON;

namespace BlackBears.SQBA.Modules.Codes
{
    class CodesState : ModuleState
    {
        JSONNode pendingBonusData;

        internal JSONNode PendingBonusData
        {
            get { return pendingBonusData; }
            set { pendingBonusData = value; }
        }
    }
}
