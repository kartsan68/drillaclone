﻿using BlackBears.SQBA.Modules.Codes;
using BlackBears.SQBA.Modules.Files;
using BlackBears.SQBA.Modules.GDPR;
using BlackBears.SQBA.Modules.Purchasing;
using BlackBears.SQBA.Networking;
using SimpleJSON;

namespace BlackBears.SQBA.Saving
{
    class ModulesFactory
    {
        internal static int modulesCount = 2;

        SaveManager saveManager;

        internal ModulesFactory(SaveManager saveManager)
        {
            this.saveManager = saveManager;
        }

        bool ForceCreate { get { return saveManager == null; } }

        internal FilesModule CreateFileModule(INetworkBehaviour networkBehaviour, General.State SQBAState)
        {
            FilesModule filesModule = null;
            if (ForceCreate) filesModule = new FilesModule(networkBehaviour, SQBAState);
            else
            {
                Save save = saveManager.GetSave(FilesModule.kFilesStateName);
                if (save != null) filesModule = new FilesModule(networkBehaviour, SQBAState, JSON.Parse(save.Data));
                else filesModule = new FilesModule(networkBehaviour, SQBAState);
            }

            saveManager.AddSavable(filesModule);

            return filesModule;
        }

        internal PurchasesModule CreatePurchasesModule(INetworkBehaviour networkBehaviour)
        {
            PurchasesModule purchasesModule = null;
            if (ForceCreate) purchasesModule = CreatePlatformPurchasesModule(networkBehaviour);
            else
            {
                Save save = saveManager.GetSave(PurchasesModule.kPurchasesStateName);
                if (save != null) purchasesModule = CreatePlatformPurchasesModule(networkBehaviour, JSON.Parse(save.Data));
                else purchasesModule = CreatePlatformPurchasesModule(networkBehaviour);
            }

            if(purchasesModule != null) saveManager.AddSavable(purchasesModule);

            return purchasesModule;
        }

        internal CodesModule CreateCodesModule(INetworkBehaviour networkBehaviour)
        {
            return new CodesModule(networkBehaviour);
        }

        internal GDPRModule CreateGDPRModule(INetworkBehaviour networkBehaviour)
        {
            return new GDPRModule(networkBehaviour);
        }

        private PurchasesModule CreatePlatformPurchasesModule(INetworkBehaviour networkBehaviour, JSONNode data = null)
        {
            #if BBGC_SQBA_PURCHASING
            #if !UNITY_EDITOR && UNITY_ANDROID
            return new AndroidPurchasesModule(networkBehaviour, data);
            #else
            return new UnityPurchasesModule(networkBehaviour, data);
            #endif
            #else
            return null;
            #endif

        }

    }
}
