﻿using BlackBears.SQBA.Networking;

public enum GDPRStatus { Success = 1, Duplicate = 2 }

namespace BlackBears.SQBA.Modules.GDPR
{
	class GDPRModule : Module 
	{
        internal GDPRModule(INetworkBehaviour networkBehaviour) : base(networkBehaviour)
		{
            Logger.LogMessage("GDPR module created.");
		}

        internal override string Abbrevation { get { return "g"; } }

        protected override ModuleState ModuleState { get { return null; } }

        internal void ProcessPrivacyPolicySignUp(string playerId, string countryCode, Block<GDPRStatus> onSuccess, FailBlock onFail)
        {
            networkBehaviour.RequestPrivacyPolicyAgreement(playerId, countryCode, (response) => {
                GDPRStatus status = (GDPRStatus)response["status"].AsInt;
                onSuccess.SafeInvoke(status);
            }, onFail);
        }
    }
}
