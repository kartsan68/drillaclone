namespace BlackBears.SQBA.Modules
{
    interface IModuleState
    {
        bool IsActive { get; }
    }
}
