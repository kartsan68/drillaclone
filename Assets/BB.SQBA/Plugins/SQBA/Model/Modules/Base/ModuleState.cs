﻿using BlackBears.SQBA.Saving;
using SimpleJSON;

namespace BlackBears.SQBA.Modules
{
    abstract class ModuleState : IModuleState, IEncodable
    {
        static string kActive = "active";

        protected bool isActive;

        internal bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        bool IModuleState.IsActive { get { return IsActive; } }

        public virtual void DecodeData(JSONNode data)
        {
            if (data == null || data.IsNull) return;

            isActive = data[kActive].AsBool;
        }

        public virtual JSONNode EncodeData()
        {
            var json = new JSONObject();
            json[kActive] = isActive;
            return json;
        }
    }
}
