﻿using BlackBears.SQBA.Networking;
using SimpleJSON;

namespace BlackBears.SQBA.Modules
{
    abstract class Module
    {
        protected INetworkBehaviour networkBehaviour;

        internal Module(INetworkBehaviour networkBehaviour)
        {
            this.networkBehaviour = networkBehaviour;
        }

        internal abstract string Abbrevation { get; }
        internal IModuleState State { get { return ModuleState; } }

        protected abstract ModuleState ModuleState { get; }

        internal void Activate()
        {
            if (!ModuleState.IsActive) ModuleState.IsActive = true;
        }

        internal virtual void AddParametersToServerGlobarRequestParameters(JSONNode data) { }
        internal virtual void ProcessServerGlobalResponse(JSONNode data) { }

        internal virtual void OnPauseApplication() { }
        internal virtual void OnResumeApplication() { }
    }
}
