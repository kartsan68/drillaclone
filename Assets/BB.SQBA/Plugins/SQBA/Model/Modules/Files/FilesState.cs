﻿using System;
using System.IO;
using BlackBears.SQBA.General;
using BlackBears.SQBA.Saving;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.SQBA.Modules.Files
{
    sealed class FilesState : ModuleState, IEncodable
    {
        static string kActiveVersion = "version";
        static string kNextVersion = "next_version";
        static string kAppVersion = "app_version";

        FilesVersion activeVersion;
        string activeVersionDir;
        bool isRuntimeReplaceAllowed;
        FilesVersion nextVersion;

        bool isUpdating;
        bool isProcessingArchive;

        string lastAppVersion;
        string currentAppVersion;

        internal FilesState(JSONNode data)
        {
            ActiveVersion = new FilesVersion(0, 0);

            currentAppVersion = Application.version;

            DecodeData(data);

            string logMessage = String.Format("Files State created {0}", data == null ? "clean" : "with state");
            Logger.LogMessage(logMessage);
        }

        internal FilesVersion ActiveVersion
        {
            get { return activeVersion; }
            set { activeVersion = value; }
        }

        internal string ActiveVersionDir
        {
            get { return activeVersionDir; }
            set { activeVersionDir = value; }
        }

        internal bool IsRuntimeReplaceAllowed
        {
            get { return isRuntimeReplaceAllowed; }
            set { isRuntimeReplaceAllowed = value; }
        }

        internal FilesVersion NextVersion
        {
            get { return nextVersion; }
            set { nextVersion = value; }
        }

        internal bool IsUpdating
        {
            get { return isUpdating; }
            set { isUpdating = value; }
        }

        internal bool IsProcessingArchive
        {
            get { return isProcessingArchive; }
            set { isProcessingArchive = value; }
        }

        internal string GetFilePath(string fileName)
        {
            if (!isActive) isActive = true;

            if (Manager.Instance.State.UseLocalFiles) return null;

            string archiveFileName = String.Format("{0}/{1}", ActiveVersion.ResourceDirectory, fileName);
            if (File.Exists(archiveFileName)) return archiveFileName;

            return null;
        }

        internal bool IsVersionChanged()
        {
            return !currentAppVersion.Equals(lastAppVersion);
        }

        public override JSONNode EncodeData()
        {
            JSONNode data = base.EncodeData();

            data.Add(kAppVersion, currentAppVersion);
            data.Add(kActiveVersion, activeVersion.EncodeData());
            if (nextVersion != null) data.Add(kNextVersion, nextVersion.EncodeData());

            return data;
        }

        public override void DecodeData(JSONNode data)
        {
            if (data == null || data.IsNull) return;

            base.DecodeData(data);

            lastAppVersion = data[kAppVersion].Value;
            activeVersion = new FilesVersion(data[kActiveVersion]);
            if (data[kNextVersion] != null) nextVersion = new FilesVersion(data[kNextVersion]);
        }
    }
}
