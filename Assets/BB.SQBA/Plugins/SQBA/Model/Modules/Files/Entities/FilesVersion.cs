﻿using System;
using BlackBears.SQBA.Saving;
using SimpleJSON;

namespace BlackBears.SQBA.Modules
{
    class FilesVersion : IEncodable
    {
        static string kNumber = "number";
        static string kGroup = "group";

        int number;
        int group;

        string baseDirectory;
        string resourceDirectory;

        internal FilesVersion(JSONNode data)
        {
            DecodeData(data);
        }

        internal FilesVersion(int number, int group)
        {
            Number = number;
            Group = group;
        }

        internal int Number
        {
            get { return number; }
            private set
            {
                number = value;
                UpdateResourceDirectory();
            }
        }

        internal int Group
        {
            get { return group; }
            private set
            {
                group = value;
                UpdateResourceDirectory();
            }
        }

        internal string BaseDirectory
        {
            get { return baseDirectory; }
            private set { baseDirectory = value; }
        }

        internal string ResourceDirectory
        {
            get { return resourceDirectory; }
            private set { resourceDirectory = value; }
        }

        public void DecodeData(JSONNode data)
        {
            if (data == null || data.IsNull) return;

            Number = data[kNumber].AsInt;
            Group = data[kGroup].AsInt;
        }

        public JSONNode EncodeData()
        {
            var json = new JSONObject();
            json[kNumber] = Number;
            json[kGroup] = Group;
            return json;
        }

        internal bool IsNewerThenVersion(FilesVersion version)
        {
            if (Number > version.Number) return true;
            if (Group != version.Group) return true;

            return false;
        }

        internal bool EqualVersion(FilesVersion version)
        {
            return group != version.group || number != version.number;
        }

        void UpdateResourceDirectory()
        {
            BaseDirectory = String.Format("{0}/files/{1}_{2}", Core.Instance.StorageDirectory, Number, Group);
            ResourceDirectory = String.Format("{0}/Resources", BaseDirectory);
        }
    }
}
