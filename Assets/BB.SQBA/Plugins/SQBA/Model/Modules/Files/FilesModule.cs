﻿using System;
using System.IO;
using BlackBears.SQBA.Networking;
using BlackBears.SQBA.Saving;
using BlackBears.SQBA.Utils;
using BlackBears.Utils;
using SimpleJSON;

using BlackSec = BlackBears.Utils.Security;

namespace BlackBears.SQBA.Modules.Files
{
    sealed class FilesModule : Module, ISavable
    {
        internal static string kFilesStateName = "files_state";
        private const string salt = "xu9-jK/%s-Xjkdf3nfd2h([n.NAdAN";


        FilesState state;
        General.State SQBAState;

        internal FilesModule(INetworkBehaviour networkBehaviour, General.State SQBAState, JSONNode data = null) : base(networkBehaviour)
        {
            Logger.LogMessage("Creating files module...");

            state = new FilesState(data);
            this.SQBAState = SQBAState;

            CheckForAppVersion();
            CheckForVersionSwap();

            Logger.LogMessage("Files module created.");
        }

        internal override string Abbrevation { get { return "f"; } }

        protected override ModuleState ModuleState { get { return state; } }

        public Save ToSave()
        {
            return new Save(kFilesStateName, state.EncodeData().ToString());
        }

        internal override void AddParametersToServerGlobarRequestParameters(JSONNode data)
        {
            base.AddParametersToServerGlobarRequestParameters(data);

            FilesVersion lastVersion = state.NextVersion ?? state.ActiveVersion;

            data.Add(NetworkKeys.kFilesVersion, lastVersion.Number.ToString());
            data.Add(NetworkKeys.kFilesGroup, lastVersion.Group.ToString());
        }

        internal override void ProcessServerGlobalResponse(JSONNode data)
        {
            base.ProcessServerGlobalResponse(data);

            ProcessServerGlobalResponse(data, null, null);
        }

        internal void ForceVersionCheckForServerData(JSONNode data, Block onSuccess, FailBlock onFail)
        {
            ProcessServerGlobalResponse(data, onSuccess, onFail);
        }

        internal void UpdateRuntimeReplaceAllowed(bool allowed)
        {
            Activate();
            state.IsRuntimeReplaceAllowed = allowed;
        }

        internal static string DecodeFileByPath(string path)
        {
            if (!File.Exists(path)) return null;

            string md5;
            string fileText;

            using (StreamReader reader = File.OpenText(path))
            {
                var buffer = new char[BlackSec.md5Length];

                reader.ReadBlock(buffer, 0, buffer.Length);
                md5 = new string(buffer);
                fileText = reader.ReadToEnd();
            }

            return DecodeFileText(fileText, md5);
        }

        internal static string DecodeFile(string file)
        {
            string md5;
            string fileText;

            md5 = file.Substring(0, BlackSec.md5Length);
            fileText = file.Substring(BlackSec.md5Length, file.Length - BlackSec.md5Length);

            return DecodeFileText(fileText, md5);
        }

        internal static string DecodeFileText(string fileText, string md5)
        {
            if (!ValidateFile(fileText, md5)) return null;

            var decodedFile = BlackSec.FromBase64(fileText);

            return decodedFile;
        }

        public static void EncryptFile(string filePath, Block onSuccess, FailBlock onFail, string newPath = null)
        {
            if (filePath.Contains(".meta"))
            {
                onSuccess.SafeInvoke();
                return;
            }

            string fileString;

            try
            {
                using (StreamReader reader = File.OpenText(filePath))
                {
                    fileString = reader.ReadToEnd();
                }
            }
            catch
            {
                onFail.SafeInvoke();
                return;
            }

            var backupPath = filePath + "_crypt";
            var encodedState = BlackSec.ToBase64(fileString);
            var md5 = GetSaveMD5(encodedState);
            try
            {
                using (StreamWriter writer = File.CreateText(backupPath))
                {
                    writer.Write(md5);
                    writer.Write(encodedState);
                    writer.Flush();
                }

                if (File.Exists(string.IsNullOrEmpty(newPath) ? filePath : newPath))
                {
                    File.Replace(backupPath, string.IsNullOrEmpty(newPath) ? filePath : newPath, null);
                }
                else
                {
                    File.Move(backupPath, string.IsNullOrEmpty(newPath) ? filePath : newPath);
                }

            }
            catch
            {
                onFail.SafeInvoke();
                return;
            }
        }

        void CheckForAppVersion()
        {
            if (!state.IsVersionChanged()) return;

            string filesPath = String.Format("{0}/files", Core.Instance.StorageDirectory);

            if (Directory.Exists(filesPath)) Directory.Delete(filesPath, true);
            Directory.CreateDirectory(filesPath);

            state = new FilesState(null);
        }

        void CheckForVersionSwap()
        {
            if (state.NextVersion != null && !state.ActiveVersion.EqualVersion(state.NextVersion))
            {
                Logger.LogFormat("Files version update from {0} to {1}", state.ActiveVersion.Number, state.NextVersion.Number);

                RemoveVersion(state.ActiveVersion);

                state.ActiveVersion = state.NextVersion;
                state.NextVersion = null;
            }
        }

        void RemoveVersion(FilesVersion version)
        {
            if (Directory.Exists(version.BaseDirectory)) Directory.Delete(version.BaseDirectory, true);
        }

        void ProcessServerGlobalResponse(JSONNode data, Block onSuccess, FailBlock onFail)
        {
            JSONNode fileData = data["q"]["files"];
            if (fileData.IsNull || fileData == null)
            {
                onSuccess.SafeInvoke();
                return;
            }

            int version = fileData["version"].AsInt;
            int group = fileData["player_group"].AsInt;
            FilesVersion newestVersion = new FilesVersion(version, group);

            bool newerThenActive = newestVersion.IsNewerThenVersion(state.ActiveVersion);
            bool newerThenNext = state.NextVersion == null || newestVersion.IsNewerThenVersion(state.NextVersion);

            string hash = fileData["hash"].Value;
            string archiveUrl = fileData["zip"].Value;

            if (newerThenActive && newerThenNext)
            {
                DownloadNextArchive(archiveUrl, hash, newestVersion, onSuccess, onFail);
            }
            else
            {
                JSONArray resources = fileData["resources_list"].AsArray;
                ValidateResources(resources, newestVersion, onSuccess, () =>
                {
                    if (Directory.Exists(state.ActiveVersion.BaseDirectory) && Directory.Exists(state.ActiveVersion.ResourceDirectory)) Directory.Delete(state.ActiveVersion.ResourceDirectory, true);
                    DownloadNextArchive(archiveUrl, hash, newestVersion, onSuccess, onFail);
                });
            }
        }

        void ValidateResources(JSONArray resources, FilesVersion version, Block onSuccess, Block onFail)
        {
            if (!Directory.Exists(version.ResourceDirectory))
            {
                onFail.SafeInvoke();
                return;
            }

            foreach (JSONNode resource in resources)
            {
                var pathToFile = version.ResourceDirectory + "/" + resource["filename"].Value;
                if (!File.Exists(pathToFile))
                {
                    onFail.SafeInvoke();
                    return;
                }

                string sqbaHash = resource["hash"].Value;
                Logger.LogFormat("file: {0} | hash: {1}", resource["filename"].Value, resource["hash"].Value);

                string fileHash = BlackSec.GetMD5Hash(sqbaHash.Split('-')[0], Common.SecretKey, pathToFile);

                Logger.LogFormat("sqba: {0} | file: {1} | is {2}", sqbaHash, fileHash,
                    BlackSec.EqualHashes(sqbaHash, fileHash) ? "valid" : "invalid");

                if (!BlackSec.EqualHashes(sqbaHash, fileHash))
                {
                    onFail.SafeInvoke();
                    return;
                }
            }

            onSuccess.SafeInvoke();
        }

        void DownloadNextArchive(string urlString, string hashString, FilesVersion version, Block onSuccess, FailBlock onFail)
        {
            Logger.LogMessage("Start downloading next archive...");

            if (state.IsUpdating) return;
            state.IsUpdating = true;

            networkBehaviour.DownloadArchive(urlString, (string fileName) =>
            {
                ProcessReceivedArchive(fileName, hashString, version, onSuccess, onFail);
            }, (errorCode, message) =>
            {
                state.IsUpdating = false;
                onFail.SafeInvoke(errorCode, message);
            });
        }

        void ProcessReceivedArchive(string archiveFileName, string hashString, FilesVersion version, Block onSuccess, FailBlock onFail)
        {
            Logger.LogMessage("Files module start processing received archive...");

            if (state.IsProcessingArchive)
            {
                onFail.SafeInvoke(FailCode.ProcessingArchive, "Already processing archive.");
                return;
            }

            state.IsUpdating = false;
            state.IsProcessingArchive = true;

            string archiveHash = BlackSec.GetMD5Hash(hashString.Split('-')[0], Common.SecretKey, archiveFileName);
            if (!BlackSec.EqualHashes(hashString, archiveHash))
            {
                Logger.LogFormat("Different hashes:: server: {0} | file: {1}", hashString, archiveHash);

                state.IsProcessingArchive = false;
                onFail.SafeInvoke(FailCode.ProcessingArchive, "There is difference in hashes. Archive may be corrupted or changed.");
                return;
            }

            try { Directory.CreateDirectory(version.ResourceDirectory); }
            catch (Exception e)
            {
                state.IsProcessingArchive = false;
                onFail.SafeInvoke(FailCode.ProcessingArchive, e.Message);
                return;
            }

            Unzipper.UnzipArchive(archiveFileName, version.BaseDirectory, Common.sqbaFilesArchivePassword, () =>
            {
                Logger.LogMessage("Archive unzipped: " + archiveFileName);

                UnityEngine.Debug.Log("FilesModule UNZIP " + (this.SQBAState == null));

                if (SQBAState.Configuration.EncryptionFiles)
                {
                    EncryptFiles(version.ResourceDirectory, SQBAState.Configuration.EncryptionFilesExtensions, () =>
                   {
                       Logger.LogMessage("Encrypt files finished");
                       FinishProcessReceivedArchive(archiveFileName, version);
                       onSuccess.SafeInvoke();
                   }, (errorCode, message) =>
                   {
                       Logger.LogMessage("Encrypt files failed: " + message);

                       state.IsProcessingArchive = false;
                       onFail.SafeInvoke(errorCode, message);
                   });
                }
                else
                {
                    FinishProcessReceivedArchive(archiveFileName, version);
                    onSuccess.SafeInvoke();
                }
            }, (errorCode, message) =>
            {
                Logger.LogMessage("Error unzipping archive: " + message);

                state.IsProcessingArchive = false;
                onFail.SafeInvoke(errorCode, message);
            });
        }

        private void FinishProcessReceivedArchive(string archiveFileName, FilesVersion version)
        {
            state.IsProcessingArchive = false;
            File.Delete(archiveFileName);

            if (state.NextVersion != null && !state.ActiveVersion.EqualVersion(state.NextVersion))
            {
                RemoveVersion(state.ActiveVersion);
            }

            state.ActiveVersion = version;

            if (state.IsRuntimeReplaceAllowed) CheckForVersionSwap();

        }

        private static void EncryptFiles(string path, string[] encryptionFilesExtensions, Block onSuccess, FailBlock onFail)
        {
            string[] filePaths = Directory.GetFiles(path);
            foreach (var filePath in filePaths)
            {
                bool exit = true;
                foreach (var extension in encryptionFilesExtensions)
                {
                    if (filePath.Contains(extension))
                    {
                        exit = false;
                        break;
                    }
                }

                if (exit) continue;

                EncryptFile(filePath, null, (c, m) =>
                    {
                        onFail.SafeInvoke(c, m);
                        return;
                    });
            }

            onSuccess.SafeInvoke();
        }

        private static string GetSaveMD5(string input)
        {
            var builder = new System.Text.StringBuilder();
            builder.Append(input).Append(salt);
            return BlackSec.GetMD5Hash(builder.ToString(), BlackSec.MD5Format.upper);
        }

        private static bool ValidateFile(string input, string inMd5)
        {
            var md5 = GetSaveMD5(input);
            return string.Equals(md5, inMd5, System.StringComparison.InvariantCulture);
        }
    }
}
