﻿using SimpleJSON;
using UnityEngine.Purchasing;

using UProduct = UnityEngine.Purchasing.Product;

namespace BlackBears.SQBA.Modules.Purchasing.RemoteValidation
{
    public sealed class PurchaseValidationAndroid : PurchaseValidationInfo
    {

        internal readonly string productId;
        internal readonly string signature;
        internal readonly string responseData;

        public PurchaseValidationAndroid(UProduct product, PurchaseSQBAItem purchaseItem) : base(purchaseItem)
        {
            JSONNode receiptJson = JSON.Parse(product.receipt);
            JSONNode payload = JSON.Parse(receiptJson["Payload"]);

            productId = product.definition.storeSpecificId;
            responseData = payload["json"].Value;
            signature = payload["signature"].Value;
        }

        public PurchaseValidationAndroid(Product product, PurchaseSQBAItem purchaseItem) : base(purchaseItem)
        {
            productId = product.inAppId;
            responseData = product.json;
            signature = product.signature;
        }

        internal string ProductId => productId;
        internal override bool EnoughData => !string.IsNullOrEmpty(signature) || !string.IsNullOrEmpty(responseData);

    }
}
