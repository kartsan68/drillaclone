﻿using BlackBears.SQBA.General;
using BlackBears.SQBA.Utils;
using BlackBears.Utils;
using UnityEngine.Purchasing;

using BlackSec = BlackBears.Utils.Security;

namespace BlackBears.SQBA.Modules.Purchasing.RemoteValidation
{
    public abstract class PurchaseValidationInfo
    {
        string randKey;
        string key;
        string appId;
        string deviceId;
        string sqbaKey;
        string advertisingIdentifier;
        string extInfo;

        PurchaseSQBAItem purchaseItem;

        protected PurchaseValidationInfo(PurchaseSQBAItem purchaseItem)
        {
            IState state = Manager.Instance.State;

            randKey = BlackSec.GetRandKey();
            key = Utils.Security.GetServerKey(state.DeviceId, randKey);
            appId = state.AppId;
            deviceId = state.DeviceId;
            sqbaKey = state.ApiKey;

            advertisingIdentifier = state.AdvertisingIdentifier;
            extInfo = state.FacebookExtInfo;

            this.purchaseItem = purchaseItem;
        }

        internal abstract bool EnoughData { get; }

        internal string RandKey { get { return randKey; } }
        internal string Key { get { return key; } }
        internal string AppId { get { return appId; } }
        internal string DeviceId { get { return deviceId; } }
        internal string SQBAKey { get { return sqbaKey; } }
        internal double Amount { get { return purchaseItem.DollarEquivalent; } }
        internal string Currency { get { return purchaseItem.Payload.Currency; } }
        internal string PlayerId { get { return purchaseItem.Payload.PlayerId; } }
        internal string AdvertisingIdentifier { get { return advertisingIdentifier; } }
        internal string ExtInfo { get { return extInfo; } }

        internal bool IsSubscription { get { return purchaseItem.Type == ProductType.Subscription; } }

    }
}
