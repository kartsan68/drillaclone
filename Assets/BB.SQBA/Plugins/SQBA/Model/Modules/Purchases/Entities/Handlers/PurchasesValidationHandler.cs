﻿using BlackBears.SQBA.Utils;
using UnityEngine.Purchasing;

namespace BlackBears.SQBA.Modules.Purchasing
{
    internal class PurchasesValidationHandler
    {

        private Block onSuccess;
        private FailBlock onFail;

        internal PurchasesValidationHandler(Block onSuccess, FailBlock onFail)
        {
            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }

        internal void CallSuccess() 
        {
            onSuccess.SafeInvoke();
        }

        internal void CallFail(string failMessage)
        {
            onFail.SafeInvoke(FailCode.FailedPurchaseValidation, failMessage);
        }
    }
}