using BlackBears.SQBA.Utils;

namespace BlackBears.SQBA.Modules.Purchasing
{
    class PurchasesRestoreHandler
    {
        Block onSuccess;
        FailBlock onFail;

        internal PurchasesRestoreHandler(Block onSuccess, FailBlock onFail)
        {
            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }

        internal void CallSuccess()
        {
            Logger.LogMessage("Restoration Success");

            onSuccess.SafeInvoke();
        }

        internal void CallFail()
        {
            Logger.LogMessage("Restoration failed.");

            onFail.SafeInvoke(FailCode.PurchaseRestorationFailed, "Failed to restore purchases.");
        }
    }
}
