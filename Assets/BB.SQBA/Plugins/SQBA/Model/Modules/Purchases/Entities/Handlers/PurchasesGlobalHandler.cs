﻿using BlackBears.SQBA.Utils;

namespace BlackBears.SQBA.Modules.Purchasing
{
    class PurchasesGlobalHandler
    {
        PurchasesGlobalBlock onSuccess;
        PurchasesGlobalBlock onCancel;
        PurchasesGlobalBlock onFail;

        internal PurchasesGlobalHandler(PurchasesGlobalBlock onSuccess, PurchasesGlobalBlock onCancel, PurchasesGlobalBlock onFail)
        {
            this.onSuccess = onSuccess;
            this.onCancel = onCancel;
            this.onFail = onFail;
        }

        internal void CallSuccess(string iapName)
        {
            Logger.LogMessage("Global success " + iapName);

            onSuccess.SafeInvoke(iapName);
        }

        internal void CallCancel(string iapName)
        {
            Logger.LogMessage("Global cancel " + iapName);

            onCancel.SafeInvoke(iapName);
        }

        internal void CallFail(string iapName, FailCode failCode, string failMessage)
        {
            Logger.LogMessage("Global failed " + iapName + " " + failMessage);

            onFail.SafeInvoke(iapName, failCode, failMessage);
        }
    }
}
