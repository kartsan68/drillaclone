﻿using BlackBears.SQBA.General;
using BlackBears.SQBA.Native.iOS;
using SimpleJSON;
using UnityEngine.Purchasing;

using UProduct = UnityEngine.Purchasing.Product;

namespace BlackBears.SQBA.Modules.Purchasing.RemoteValidation
{
    public sealed class PurchaseValidationIos : PurchaseValidationInfo
    {
        string receipt;
        string productId;
        string transactionId;
        bool advertisingTrackingEnabled;
        bool isRestoring;

        public PurchaseValidationIos(UProduct product, PurchaseSQBAItem purchaseItem, bool isRestoring) : base(purchaseItem)
        {
            JSONNode receiptJson = JSON.Parse(product.receipt);
            receipt = receiptJson["Payload"].Value;

            productId = product.definition.storeSpecificId;
            transactionId = product.transactionID;

            IState state = Manager.Instance.State;
            advertisingTrackingEnabled = state.NativeDataAdapter.AdvertisingTrackingEnabled;

            this.isRestoring = isRestoring;
        }

        internal override bool EnoughData { get { return !string.IsNullOrEmpty(receipt) || !string.IsNullOrEmpty(productId); } }

        internal string Receipt { get { return receipt; } }
        internal string ProductId { get { return productId; } }
        internal string TransactionId { get { return transactionId; } }
        internal bool AdvertisingTrackingEnabled { get { return advertisingTrackingEnabled; } }
        internal bool IsRestoring { get { return isRestoring; } }
    }
}
