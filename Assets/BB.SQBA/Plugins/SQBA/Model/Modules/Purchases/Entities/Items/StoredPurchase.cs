﻿using System;
using System.Globalization;
using BlackBears.SQBA.Saving;
using SimpleJSON;
using UnityEngine.Purchasing;

namespace BlackBears.SQBA.Modules.Purchasing
{
    class StoredPurchase : IEncodable
    {
        const string kIapId = "iap";
        const string kActive = "active";
        const string kLaunchesToInvalid = "launches";
        const string kInvalidationDate = "date";

        const string kDateFormat = "O";
        const int kValidLaunches = 25;
        const int kValidTime = 86400 * 7;

        internal int launchesToInvalid;

        string iapId;
        bool isActive;
        DateTime invalidationDate;

        internal StoredPurchase(JSONNode data)
        {
            DecodeData(data);
        }

        internal StoredPurchase(string iapId)
        {
            this.iapId = iapId;
        }

        internal string IapId { get { return iapId; } }

        internal bool IsNeedValidation
        {
            get
            {
                bool invalidDate = invalidationDate == default(DateTime) || (invalidationDate - DateTime.UtcNow).TotalSeconds < 0;
                return launchesToInvalid < 0 || invalidDate;
            }
        }

        internal bool IsActive { get { return isActive; } }

        public void DecodeData(JSONNode data)
        {
            iapId = data[kIapId].Value;
            isActive = data[kActive].AsBool;

            int launchesLeft = data[kLaunchesToInvalid].AsInt;
            launchesToInvalid = launchesLeft > kValidLaunches ? 0 : launchesLeft;
            invalidationDate = DateTime.ParseExact(data[kInvalidationDate].Value, kDateFormat, CultureInfo.InvariantCulture);
        }

        public JSONNode EncodeData()
        {
            var json = new JSONObject();
            json[kIapId] = iapId;
            json[kActive] = isActive;
            json[kLaunchesToInvalid] = launchesToInvalid;
            json[kInvalidationDate] = invalidationDate.ToString(kDateFormat);
            return json;
        }

        internal void MakeValidated(bool valid)
        {
            isActive = valid;
            launchesToInvalid = kValidLaunches;
            invalidationDate = DateTime.UtcNow.AddSeconds(kValidTime);
        }
    }
}
