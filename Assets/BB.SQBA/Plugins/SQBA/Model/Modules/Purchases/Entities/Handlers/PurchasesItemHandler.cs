﻿using UnityEngine.Purchasing;
using BlackBears.SQBA.Utils;

namespace BlackBears.SQBA.Modules.Purchasing
{
    class PurchaseItemHandler
    {
        PurchaseSQBAItem item;

        Block<PurchaseAnswer> onSuccess;
        Block onCancel;
        FailBlock onFail;

        internal PurchaseItemHandler(PurchaseSQBAItem item, Block<PurchaseAnswer> onSuccess = null, Block onCancel = null, FailBlock onFail = null)
        {
            this.item = item;

            this.onSuccess = onSuccess;
            this.onCancel = onCancel;
            this.onFail = onFail;
        }

        internal PurchaseSQBAItem Item { get { return item; } }
        internal string IapName { get { return item.IapName; } }

        internal void CallSuccess(PurchaseAnswer answer)
        {
            onSuccess.SafeInvoke(answer);
        }

        internal void CallCancel()
        {
            onCancel.SafeInvoke();
        }

        internal void CallFail(PurchaseFailureReason r) => CallFail(r.ToString());

        internal void CallFail(string failMessage) =>
            onFail.SafeInvoke(FailCode.FailedPurchase, $"Failed with reason: {failMessage}");

        internal void CallFail(FailCode failCode) =>
            onFail.SafeInvoke(failCode, "Failed purchase. Can't pass remote validation.");

    }
}
