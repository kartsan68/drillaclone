﻿using BlackBears.SQBA.Utils;

namespace BlackBears.SQBA.Modules.Purchasing
{
    class PurchasesInitializationHandler
    {
        Block onSuccess;
        FailBlock onFail;

        internal PurchasesInitializationHandler(Block onSuccess, FailBlock onFail)
        {
            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }

        internal void CallSuccess()
        {
            onSuccess.SafeInvoke();
        }

        internal void CallFail(FailCode failCode, string failMessage)
        {
            onFail.SafeInvoke(failCode, failMessage);
        }
    }
}
