﻿using BlackBears.SQBA.Modules.Purchasing.RemoteValidation;
using SimpleJSON;

namespace BlackBears.SQBA.Modules.Purchasing
{
    public struct ValidationPayload
    {
        string playerId;
        string currency;

        public ValidationPayload(string playerId, string currency = "USD")
        {
            this.playerId = playerId;
            this.currency = currency;
        }

        public string PlayerId { get { return playerId; } }
        public string Currency { get { return currency; } }
    }

    public class PurchaseSQBAItem
    {
        static string kPurchaseType = "product_type";
        static string kPurchaseIapName = "iap";
        static string kPurchaseIapIosName = "iap_ios";
        static string kPurchaseIapAndroidName = "iap_android";
        static string kPurchaseDefaultPrice = "default_price";
        static string kPurchaseDollarEquivalent = "equivalent";

        ProductType type;

        string iapName;
        string iosIapName;
        string androidIapName;

        double dollarEquivalent;

        string defaultPrice;
        string localizedCurrency;
        string localizedPrice;
        string localizedTitle;

        ValidationPayload payload;

        public PurchaseSQBAItem(JSONNode node, ValidationPayload payload)
        {
            type = (ProductType)node[kPurchaseType].AsInt;
            iapName = node[kPurchaseIapName].Value;

            iosIapName = node[kPurchaseIapIosName].Value;
            androidIapName = node[kPurchaseIapAndroidName].Value;

            dollarEquivalent = node[kPurchaseDollarEquivalent].AsDouble;

            defaultPrice = node[kPurchaseDefaultPrice].Value;

            this.payload = payload;
        }

        public PurchaseSQBAItem(string iapName, double dollarEquivalent, ProductType type , ValidationPayload payload)
        {
            this.iapName = iapName;
            this.dollarEquivalent = dollarEquivalent;

            this.payload = payload;
            this.type = type;
        }

        public ProductType Type { get { return type; } }
        
        public string IapName { get { return iapName; } }
        public string IosIapName { get { return string.IsNullOrEmpty(iosIapName) ? iapName : iosIapName; } }
        public string AndroidIapName { get { return string.IsNullOrEmpty(androidIapName) ? iapName : androidIapName; } }
        public string PlatformInAppName 
        {
            get
            {
                #if UNITY_ANDROID
                return AndroidIapName;
                #else
                return IosIapName;
                #endif
            }
        }

        public string LocalizedPrice { get { return localizedPrice ?? defaultPrice; } }
        public string LocalizedTitle { get { return localizedTitle; } }
        public string LocalizedCurrency { get { return localizedCurrency; } }
        public double DollarEquivalent { get { return dollarEquivalent; } }
        public ValidationPayload Payload { get { return payload; } }

        internal void Update(ProductMetadata metadata)
        {
            localizedCurrency = metadata.isoCurrencyCode;
            localizedPrice = metadata.localizedPriceString;
            localizedTitle = metadata.localizedTitle;
        }
    }
}