namespace BlackBears.SQBA.Modules.Purchasing
{
    public struct ProductMetadata
    {
        
        public readonly string isoCurrencyCode;
        public readonly string localizedPriceString;
        public readonly string localizedTitle;

        public ProductMetadata(string isoCurrencyCode, string localizedPriceString,
            string localizedTitle)
        {
            this.isoCurrencyCode = isoCurrencyCode;
            this.localizedPriceString = localizedPriceString;
            this.localizedTitle = localizedTitle;
        }

    }
}