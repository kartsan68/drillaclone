﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.SQBA.Utils;

namespace BlackBears.SQBA.Modules.Purchasing
{
	class PurchasesPendingProductsHandler
	{
		PurchasesGlobalBlock onPending;

        internal PurchasesPendingProductsHandler(PurchasesGlobalBlock onPending)
        {
            this.onPending = onPending;
		}

		internal void CallPending(string iapName)
		{
			onPending.SafeInvoke(iapName);
		}
	}
}
