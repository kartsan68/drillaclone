using UnityEngine;

namespace BlackBears.SQBA.Modules.Purchasing.AndroidNative
{

    public class NewShopItem : AndroidJavaProxy
    {

        public const string jClassName = "mobi.blackbears.billing.shop.IShopItem";

        public PurchaseSQBAItem Item { get; private set; }
        public ProductType ProductType => Item.Type;

        public NewShopItem(PurchaseSQBAItem item) : base(jClassName) 
        {
            this.Item = item;
        }

        public string getInAppId() => Item.PlatformInAppName;
        public int getProductType() => (int)Item.Type;
        public bool isSubscription() => Item.Type == ProductType.Subscription;
        public bool isConsumable() => true;
        public void setPrice(string price){}
        public double getEquivalent() => Item.DollarEquivalent;

        public void update(string currencyCode, string price, string title)
        {
            Item.Update(new ProductMetadata(currencyCode, price, title));
        }

    }

}