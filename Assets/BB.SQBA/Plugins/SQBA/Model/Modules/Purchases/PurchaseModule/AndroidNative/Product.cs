using UnityEngine;

namespace BlackBears.SQBA.Modules.Purchasing
{

    public class Product
    {

        public readonly string inAppId;
        public readonly string json;
        public readonly string signature;
        public readonly ProductType productType;
        public readonly AndroidJavaObject purchase;

        public Product(AndroidJavaObject jProduct)
        {
            if (jProduct == null) return;

            inAppId = jProduct.Call<string>("getSku");
            json = jProduct.Call<string>("getJson");
            signature = jProduct.Call<string>("getSignature");
            productType = (ProductType)jProduct.Call<int>("getProductType");
            purchase = jProduct.Call<AndroidJavaObject>("getPurchase");
        }

        public Product(string inAppId, string json, string signature, ProductType productType)
        {
            this.inAppId = inAppId;
            this.json = json;
            this.signature = signature;
            this.productType = productType;
        }
    }

}