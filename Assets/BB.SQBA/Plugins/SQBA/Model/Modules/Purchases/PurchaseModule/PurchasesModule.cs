﻿using System.Collections.Generic;
using BlackBears.SQBA.Networking;
using BlackBears.SQBA.Saving;
using BlackBears.SQBA.Utils;
using SimpleJSON;

namespace BlackBears.SQBA.Modules.Purchasing
{
    internal abstract class PurchasesModule : Module, ISavable
    {

        internal const string kPurchasesStateName = "purchases_state";

        protected PurchasesState state;

        protected PurchasesGlobalHandler globalHandler;
        protected PurchasesRestoreHandler restoreHandler;
        protected PurchasesPendingProductsHandler pendingProductsHandler;

        protected List<PurchaseItemHandler> purchaseHandlers = new List<PurchaseItemHandler>();
        protected bool isRestoringPurchases;

        internal PurchasesModule(INetworkBehaviour networkBehaviour, JSONNode data = null) : base(networkBehaviour)
        {
            Logger.LogMessage("Creating Purchases module...");

            state = new PurchasesState(data);

            Activate();

            Logger.LogMessage("Purchases module created.");
        }

        private static void NotInitializedError(FailBlock onFail)
        {
            onFail.SafeInvoke(FailCode.PurchasesModuleNotInitialized, "Need to call Initialize() before calling another methods");
        }

        internal override string Abbrevation => "i";
        internal abstract bool Initialized { get; }

        protected override ModuleState ModuleState { get { return state; } }

        public Save ToSave()
        {
            return new Save(kPurchasesStateName, state.EncodeData().ToString());
        }

        internal void SetGlobalValidationObserver(PurchasesGlobalBlock onSuccess, PurchasesGlobalBlock onCancel,
            PurchasesGlobalBlock onFail)
        {
            globalHandler = new PurchasesGlobalHandler(onSuccess, onCancel, onFail);
        }

        internal abstract void Initialize(PurchaseSQBAItem[] purchases, Block onSuccess, FailBlock onFail);

        internal abstract void LocalCheckForActiveSubscriptions(Block onSuccess, FailBlock onFail);

        internal void GetProductsInfo(PurchaseItemBlock onSuccess, FailBlock onFail)
        {
            if (!Initialized)
            {
                NotInitializedError(onFail);
                return;
            }

            InvokeGetProductsInfo(onSuccess, onFail);
        }

        internal void PurchaseItem(PurchaseSQBAItem purchaseItem, Block<PurchaseAnswer> onSuccess, 
            Block onCancel, FailBlock onFail)
        {
            if (!Initialized)
            {
                NotInitializedError(onFail);
                return;
            }

            InvokePurchaseItem(purchaseItem, onSuccess, onCancel, onFail);
        }

        internal virtual void ProcessPendingProducts(PurchasesGlobalBlock onPending)
        {
            pendingProductsHandler = new PurchasesPendingProductsHandler(onPending);
        }
        
        internal abstract void RestorePurchases(Block onSuccess, FailBlock onFail);
        internal abstract void CheckPurchaseIsActive(PurchaseSQBAItem purchaseItem, Block<bool> onFinish);
        internal abstract void ValidatePurchase(PurchaseSQBAItem purchaseItem, FailBlock onFail);
        internal abstract void CheckForActiveSubscriptions(string playerId, Block<List<string>> onSuccess, FailBlock onFail);

        protected abstract void InvokeGetProductsInfo(PurchaseItemBlock onSuccess, FailBlock onFail);
        protected abstract void InvokePurchaseItem(PurchaseSQBAItem purchaseItem, Block<PurchaseAnswer> onSuccess, Block onCancel, FailBlock onFail);

        protected PurchaseItemHandler GetPurchaseHandler(string iapName)
        {
            foreach (PurchaseItemHandler purchaseHandler in purchaseHandlers)
            {
                if (purchaseHandler.IapName.Equals(iapName)) return purchaseHandler;
            }
            return null;
        }

    }
}
