namespace BlackBears.SQBA.Modules.Purchasing
{

    public class PurchaseAnswer { }

#if UNITY_IOS

    public class iOSPurchaseAnswer : PurchaseAnswer
    {

        public readonly string bundleReceipt;
        public readonly string transactionId;

        internal iOSPurchaseAnswer(string bundleReceipt, string transactionId)
        {
            this.bundleReceipt = bundleReceipt;
            this.transactionId = transactionId;
        }

    }

#elif UNITY_ANDROID

    public class AndroidPurchaseAnswer : PurchaseAnswer
    {

        public readonly string signature;
        public readonly string responseData;

        internal AndroidPurchaseAnswer(string signature, string responseData)
        {
            this.signature = signature;
            this.responseData = responseData;
        }

    }

#endif

}