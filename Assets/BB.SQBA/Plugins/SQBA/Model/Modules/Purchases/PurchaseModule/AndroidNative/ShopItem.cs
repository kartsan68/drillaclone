using UnityEngine;

namespace BlackBears.SQBA.Modules.Purchasing.AndroidNative
{

    public class ShopItem : AndroidJavaProxy
    {

        public const string jClassName = "mobi.blackbears.unity.purchase.IShopItem";

        public PurchaseSQBAItem Item { get; private set; }
        public ProductType ProductType => Item.Type;

        public ShopItem(PurchaseSQBAItem item) : base(jClassName) 
        {
            this.Item = item;
        }

        public string getInAppId() => Item.PlatformInAppName;
        public int getProductType() => (int)Item.Type;

        public void update(string currencyCode, string price, string title)
        {
            Item.Update(new ProductMetadata(currencyCode, price, title));
        }

    }

}