﻿using System.Collections.Generic;
using BlackBears.SQBA.Saving;
using SimpleJSON;

namespace BlackBears.SQBA.Modules.Purchasing
{
    sealed class PurchasesState : ModuleState, IEncodable
    {
        static string kStoredPurchases = "purchases";
        static string kSubCheckNeedReceipt = "sub_check_need_receipt";

        PurchaseSQBAItem[] currentPurchaseItems;
        List<StoredPurchase> storedPurchases = new List<StoredPurchase>();
        bool subscriptionCheckNeedReceipt = true;

        internal PurchasesState(JSONNode data)
        {
            DecodeData(data);

            foreach (StoredPurchase purchase in storedPurchases) purchase.launchesToInvalid--;
        }

        internal PurchaseSQBAItem[] CurrentPurchaseItems
        {
            get { return currentPurchaseItems; }
            set { currentPurchaseItems = value; }
        }

        internal List<StoredPurchase> StoredPurchases
        {
            get { return storedPurchases; }
            set { storedPurchases = value; }
        }

        internal bool SubscriptionCheckNeedReceipt
        {
            get { return subscriptionCheckNeedReceipt; }
            set { subscriptionCheckNeedReceipt = value; }
        }

        public override JSONNode EncodeData()
        {
            JSONNode data = base.EncodeData();

            JSONArray storedPurchasesSaveArray = new JSONArray();
            foreach (StoredPurchase purchase in storedPurchases)
            {
                JSONNode saveData = purchase.EncodeData();
                storedPurchasesSaveArray.Add(saveData);
            }
            data.Add(kStoredPurchases, storedPurchasesSaveArray);
            
            data.Add(kSubCheckNeedReceipt, subscriptionCheckNeedReceipt);

            return data;
        }

        public override void DecodeData(JSONNode data)
        {
            if (data == null || data.IsNull) return;

            base.DecodeData(data);

            List<StoredPurchase> loadedPurchases = new List<StoredPurchase>();
            foreach (JSONNode purchaseNode in data[kStoredPurchases].AsArray)
            {
                StoredPurchase purchase = new StoredPurchase(purchaseNode);
                loadedPurchases.Add(purchase);
            }
            storedPurchases = loadedPurchases;

            subscriptionCheckNeedReceipt = data[kSubCheckNeedReceipt].AsBool;
        }

        internal StoredPurchase GetStoredPurchase(string iapId)
        {
            foreach (StoredPurchase oldPurchase in storedPurchases)
            {
                if (oldPurchase.IapId.Equals(iapId)) return oldPurchase;
            }
            return null;
        }
    }
}
