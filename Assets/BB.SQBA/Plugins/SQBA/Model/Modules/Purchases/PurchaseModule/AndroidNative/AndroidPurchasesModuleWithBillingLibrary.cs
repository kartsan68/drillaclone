#if !BBGC_SQBA_USE_OLD_GP_BILING && !UNITY_EDITOR && UNITY_ANDROID

using BlackBears.SQBA.Networking;
using BlackBears.SQBA.Utils;
using SimpleJSON;

using JO = UnityEngine.AndroidJavaObject;
using JC = UnityEngine.AndroidJavaClass;
using JRunnable = UnityEngine.AndroidJavaRunnable;
using BlackBears.SQBA.Modules.Purchasing.AndroidNative;
using System.Collections.Generic;
using BlackBears.SQBA.Security;
using UnityEngine;
using BlackBears.SQBA.Modules.Purchasing.RemoteValidation;

namespace BlackBears.SQBA.Modules.Purchasing
{

    internal sealed class AndroidPurchasesModule : PurchasesModule
    {

        private JO resolver;

        private List<NewShopItem> allShopItems = new List<NewShopItem>();
        private List<Product> pendingItems = new List<Product>();

        private List<string> activeSubscriptions;

        internal AndroidPurchasesModule(INetworkBehaviour networkBehaviour, JSONNode data = null) : base(networkBehaviour, data) { }

        internal override bool Initialized => resolver != null && resolver.Call<bool>("isInitiated");

        internal override void Initialize(PurchaseSQBAItem[] purchases, Block onSuccess, FailBlock onFail)
        {
            if (resolver != null) return;

            Logger.LogMessage("Initializing Android Purchases module...");
            state.CurrentPurchaseItems = purchases;

            using (var unity = new JC("com.unity3d.player.UnityPlayer"))
            {
                var currentActivity = unity.GetStatic<JO>("currentActivity");
                resolver = currentActivity.Call<JO>("getNewPurchaseResolver");
            }

            // shopItems = CreateShopItemsArray();

            var purchaseItems = state.CurrentPurchaseItems;

            for (int i = 0; i < purchaseItems.Length; i++)
            {
                var item = new NewShopItem(purchaseItems[i]);
                allShopItems.Add(item);
            }

            resolver.Call("init", new PurchaseResolverDataSupplier(this), new PurchaseListener(this), KeyGenerator.GetKey(), new JRunnable(() =>
            {
                Logger.LogMessage("Purchases module initialized. NEW");
                onSuccess.SafeInvoke();
                GetActiveSubscriptions(activeSubscriptions => this.activeSubscriptions = activeSubscriptions);
                
            }), new JRunnable(() =>
            {
                var failMessage = "Native purchases initialization failed.";
                Logger.LogMessage(failMessage);

                onFail.SafeInvoke(FailCode.PurchasesModuleNotInitialized, failMessage);
            }));
        }

        internal override void CheckPurchaseIsActive(PurchaseSQBAItem purchaseItem, Block<bool> onFinish)
        {
            throw new System.NotImplementedException();
        }

        internal override void ProcessPendingProducts(PurchasesGlobalBlock onPending)
        {
            base.ProcessPendingProducts(onPending);

            foreach (var pendingItem in pendingItems)
            {
                pendingProductsHandler.CallPending(pendingItem.inAppId);
            }
            pendingItems.Clear();
        }

        internal override void RestorePurchases(Block onSuccess, FailBlock onFail)
        {
            throw new System.NotImplementedException();
        }
        internal override void LocalCheckForActiveSubscriptions(Block onSuccess, FailBlock onFail)
        {
            throw new System.NotImplementedException();
        }

        internal override void ValidatePurchase(PurchaseSQBAItem purchaseItem, FailBlock onFail)
        {
            var NewShopItem = GetShopItemWithId(purchaseItem?.PlatformInAppName);
            if (NewShopItem == null)
            {
                onFail.SafeInvoke(FailCode.FailedPurchaseValidation, "There is no NewShopItem with the same product_id");
                return;
            }

            resolver.Call("getProductForShopItem", NewShopItem, new ProductBlockProxy(jProduct =>
            {
                if (jProduct == null)
                {
                    onFail.SafeInvoke(FailCode.FailedPurchaseValidation, "There is no Product with the same product_id");
                    return;
                }

                var product = new Product(jProduct);
                jProduct.Dispose();

                var purchaseHandler = new PurchaseItemHandler(purchaseItem);
                purchaseHandlers.Add(purchaseHandler);

                StorePurchase(product);
            }));
        }

        internal override void CheckForActiveSubscriptions(string playerId, Block<List<string>> onSuccess, FailBlock onFail)
        {
            GetActiveSubscriptions(onSuccess);
        }

        protected override void InvokeGetProductsInfo(PurchaseItemBlock onSuccess, FailBlock onFail)
        {
            resolver.Call("getProductsInfo", new JRunnable(() => onSuccess.SafeInvoke(state.CurrentPurchaseItems)),
                new JRunnable(() => onFail.SafeInvoke(FailCode.PurchasePriceUpdateFailed, "Purchase update failed")));
        }

        protected override void InvokePurchaseItem(PurchaseSQBAItem purchaseItem, Block<PurchaseAnswer> onSuccess, Block onCancel, FailBlock onFail)
        {
            var NewShopItem = GetShopItemWithId(purchaseItem.PlatformInAppName);
            if (NewShopItem == null)
            {
                onFail.SafeInvoke(FailCode.FailedPurchase, "There is no active and available product with: " + purchaseItem.IapName);
                return;
            }
            var purchaseHandler = new PurchaseItemHandler(purchaseItem, onSuccess, onCancel, onFail);
            purchaseHandlers.Add(purchaseHandler);

            // if (purchaseItem.Type == ProductType.Subscription) UpdateSubscription(NewShopItem);
            // else 
            InitiatePurchaseInApp(NewShopItem);
        }

        private void UpdateSubscription(NewShopItem item)
        {
            GetActiveSubscriptions(subs =>
            {
                InitiatePurchaseSubscription(item.Item.PlatformInAppName, subs);
            });
        }

        private void InitiatePurchaseInApp(NewShopItem item) => resolver.Call("requestPurchase", item);

        private void InitiatePurchaseSubscription(string subId, List<string> oldSubIds)
        {
            var jOldSubIds = CreateJavaStringArray(oldSubIds);
            resolver.Call("purchaseSubscription", jOldSubIds, subId);
        }

        private void ProcessPurchase(Product product)
        {
            Logger.LogMessage($"Processing purchase: {product.inAppId}");

            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.inAppId);
            if (purchaseHandler == null) StorePendingProduct(product);
            else StorePurchase(product);
        }

        private void PurchaseFailed(string inAppId, string errorMessage)
        {
            Logger.LogMessage($"Purchase failed: {inAppId}; Message: {errorMessage}");

            var purchaseHandler = GetPurchaseHandler(inAppId);
            globalHandler?.CallFail(inAppId, FailCode.FailedPurchase, $"Failed with reason: {errorMessage}");
            purchaseHandler?.CallFail(errorMessage);
        }

        private void PurchaseCanceled(string inAppId)
        {
            Logger.LogMessage($"Purchase canceled: {inAppId}");

            var purchaseHandler = GetPurchaseHandler(inAppId);
            globalHandler?.CallCancel(inAppId);
            purchaseHandler?.CallCancel();

            purchaseHandlers.Remove(purchaseHandler);
        }

        private void StorePendingProduct(Product product)
        {
            Logger.LogMessage("Need to process pending iap: " + product.inAppId);
            if (pendingProductsHandler != null)
            {
                pendingProductsHandler.CallPending(product.inAppId);
            }
            else
            {
                pendingItems.Add(product);
            }
        }

        private void StorePurchase(Product product)
        {
            Logger.LogMessage("Storing purchase...");

            StoredPurchase purchase = new StoredPurchase(product.inAppId);
            StoredPurchase oldPurchase = state.GetStoredPurchase(purchase.IapId);

            if (oldPurchase != null)
            {
                purchase = oldPurchase;
            }
            else
            {
                if (product.productType == ProductType.NonConsumable)
                {
                    state.StoredPurchases.Add(purchase);
                }
            }

            var validationHandler = new PurchasesValidationHandler(() =>
            {
                Logger.LogMessage("Storing: success validation.");

                SuccessPurchaseValidation(product);
            }, (errorCode, message) =>
            {
                Logger.LogError(errorCode, "Storing: failed validation.");

                FailedPurchaseValidation(product);
            });

            if (purchase.IsNeedValidation)
            {
                ProcessLocalValidation(product, validationHandler);
            }
            else
            {
                Logger.LogMessage("Storing: isNeedValidation == false");

                validationHandler.CallSuccess();
            }
        }

        private void SuccessPurchaseValidation(Product product)
        {
            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.inAppId);
            bool purchaseConfirmed = ConfirmPendingPurchase(product);
            if (purchaseConfirmed)
            {
                globalHandler?.CallSuccess(product.inAppId);
                var answer = new AndroidPurchaseAnswer(product.signature, product.json);
                purchaseHandler?.CallSuccess(answer);
            }
            else
            {
                globalHandler?.CallFail(product.inAppId, FailCode.FailedPurchaseValidation,
                    $"Product {product.inAppId} not confirmed");
                purchaseHandler?.CallFail(FailCode.FailedPurchaseValidation);
            }
            purchaseHandlers.Remove(purchaseHandler);
        }

        private void FailedPurchaseValidation(Product product)
        {
            StoredPurchase storedPurchase = state.GetStoredPurchase(product.inAppId);
            if (storedPurchase != null) storedPurchase.MakeValidated(false);

            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.inAppId);
            if (globalHandler != null)
            {
                globalHandler.CallFail(product.inAppId, FailCode.FailedPurchaseValidation,
                    "Purchase failed. Can't pass remote verification");
            }
            if (purchaseHandler != null)
            {
                purchaseHandler.CallFail(FailCode.FailedPurchaseValidation);
                purchaseHandlers.Remove(purchaseHandler);
            }
        }

        private void ProcessLocalValidation(Product product, PurchasesValidationHandler validationHandler)
        {
            bool isValidPurchase = true;

            isValidPurchase = resolver.Call<bool>("validatePurchase", product.signature, product.json);

            if (isValidPurchase)
            {
                Logger.LogMessage("Purchase local validation passed.");

                ProcessRemoteValidation(product, validationHandler);
            }
            else
            {
                Logger.LogMessage("Purchase local validation failed.");

                StoredPurchase storedPurchase = state.GetStoredPurchase(product.inAppId);
                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail("Local validation failed.");
                }
            }
        }

        private void ProcessRemoteValidation(Product product, PurchasesValidationHandler validationHandler)
        {
            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.inAppId);
            StoredPurchase storedPurchase = state.GetStoredPurchase(product.inAppId);

#if UNITY_ANDROID
            Logger.LogMessage("Processing Android remote validation...");

            var validationInfo = new PurchaseValidationAndroid(product, purchaseHandler.Item);

            if (!validationInfo.EnoughData)
            {
                Logger.LogMessage("Can't start validating. Not enough information provided.");

                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail("Not enough information provided.");
                }
                return;
            }

            networkBehaviour.RequestAndroidPurchaseValidation(validationInfo, (data) =>
            {
                CheckRemoteValidationData(product, validationInfo, data, validationHandler);
            }, (errorCode, message) =>
            {
                Logger.LogError(errorCode, message);

                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail(message);
                }
            });
#else
            if (validationHandler != null)
            {
                validationHandler.CallSuccess();
            }
#endif
        }

        private void CheckRemoteValidationData(Product product, PurchaseValidationInfo validationInfo, JSONNode data,
            PurchasesValidationHandler validationHandler)
        {
            bool validPurchase = true;

            string outKey = data["out_key"].Value;

            bool serverVerified = data["verify"].AsBool;
            string expectedString = string.Format("verify{0}{1}{2}", serverVerified ? "true" : "false", product.inAppId, validationInfo.DeviceId);

            bool isKeyValid = Utils.Security.IsServerKeyValid(outKey, expectedString, validationInfo.RandKey);
            if (!isKeyValid)
            {
                Logger.LogMessage("Remote verification failed because of a different keys.");
                validPurchase = false;
            }
            else
            {
                validPurchase = serverVerified;

                Logger.LogMessage($"Remote server verified: {serverVerified}");
            }

            StoredPurchase storedPurchase = state.GetStoredPurchase(product.inAppId);
            if (storedPurchase != null) storedPurchase.MakeValidated(validPurchase);

            if (validPurchase)
            {
                Logger.LogMessage("Remote validation passed.");

                validationHandler?.CallSuccess();
            }
            else
            {
                Logger.LogError(FailCode.FailedPurchaseValidation, "Remote server validation failed.");

                validationHandler?.CallFail("Remote server validation failed.");
            }
        }

        private bool ConfirmPendingPurchase(Product product)
        {
            if (product.productType == ProductType.Consumable)
            {
                return resolver.Call<bool>("confirmPendingPurchase", product.purchase);
            }
            return true;
        }

        private NewShopItem GetShopItemWithId(string inAppId)
        {
            return allShopItems.Find(item => string.Equals(item.Item.PlatformInAppName, inAppId));
        }

        private void GetActiveSubscriptions(Block<List<string>> onComplete)
        {
            resolver.Call("getActiveSubscriptions", new ProductListBlockProxy(jActiveSubs =>
            {
                List<string> activeSubs = new List<string>();
                if (jActiveSubs != null)
                {
                    int size = jActiveSubs.Call<int>("size");
                    for (int i = 0; i < size; ++i)
                    {
                        activeSubs.Add(jActiveSubs.Call<JO>("get", i).Call<string>("getSku"));
                    }
                    jActiveSubs.Dispose();
                }
                onComplete.SafeInvoke(activeSubs);
            }));
        }

        private JO CreateShopItemsArray()
        {
            var arrayClass = new JC("java.lang.reflect.Array");
            var arrayObject = arrayClass.CallStatic<JO>("newInstance", new JC(NewShopItem.jClassName), allShopItems.Count);

            for (int i = 0; i < allShopItems.Count; i++)
            {
                arrayClass.CallStatic("set", arrayObject, i, allShopItems[i]);
            }
            return arrayObject;
        }

        private JO CreateJavaStringArray(List<string> strings)
        {
            if (strings == null) return null;

            var arrayClass = new JC("java.lang.reflect.Array");
            var arrayObject = arrayClass.CallStatic<JO>("newInstance", new JC("java.lang.String"), strings.Count);

            for (int i = 0; i < strings.Count; i++)
            {
                arrayClass.CallStatic("set", arrayObject, i, new AndroidJavaObject("java.lang.String", strings[i]));
            }

            return arrayObject;
        }

        private class ProductBlockProxy : AndroidJavaProxy
        {
            private Block<JO> block;

            public ProductBlockProxy(Block<JO> block) : base("mobi.blackbears.billing.product.ProductBlock")
            {
                this.block = block;
            }

            private void invoke(JO obj)
            {
                block.SafeInvoke(obj);
            }
        }

        private class ProductListBlockProxy : AndroidJavaProxy
        {

            private Block<JO> block;

            public ProductListBlockProxy(Block<JO> block) : base("mobi.blackbears.billing.product.ProductsListBlock")
            {
                this.block = block;
            }

            private void invoke(JO obj)
            {
                block.SafeInvoke(obj);
            }

        }

        private class PurchaseListener : AndroidJavaProxy
        {
            private AndroidPurchasesModule androidPurchasesModule;

            public PurchaseListener(AndroidPurchasesModule androidPurchasesModule) :
                base("mobi.blackbears.billing.shop.IPurchaseProcessor")
            {
                this.androidPurchasesModule = androidPurchasesModule;
            }

            private void processPurchase(JO jProduct)
            {
                androidPurchasesModule.ProcessPurchase(jProduct != null ? new Product(jProduct) : null);
            }

            private void purchaseFailed(string inAppId, string errorMessage)
            {
                androidPurchasesModule.PurchaseFailed(inAppId, errorMessage);
            }

            private void purchaseCanceled(string inAppId)
            {
                androidPurchasesModule.PurchaseCanceled(inAppId);
            }

        }


        private class PurchaseResolverDataSupplier : AndroidJavaProxy
        {

            private AndroidPurchasesModule androidPurchasesModule;

            public PurchaseResolverDataSupplier(AndroidPurchasesModule androidPurchasesModule) :
                base("mobi.blackbears.billing.purchase.IPurchaseResolverDataSupplier")
            {
                this.androidPurchasesModule = androidPurchasesModule;
            }

            private JO getShopItems()
            {
                return androidPurchasesModule.CreateShopItemsArray();
            }

            private NewShopItem getShopItemBySku(string inAppId)
            {
                return androidPurchasesModule.GetShopItemWithId(inAppId);
            }

            private JO getExistingSubscriptions()
            {
                return androidPurchasesModule.CreateJavaStringArray(androidPurchasesModule.activeSubscriptions);
            }

        }

    }

}

#endif