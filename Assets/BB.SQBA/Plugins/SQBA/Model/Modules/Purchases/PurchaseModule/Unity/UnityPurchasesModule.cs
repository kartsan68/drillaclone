#if (UNITY_EDITOR || !UNITY_ANDROID) && BBGC_SQBA_PURCHASING

using System.Collections.Generic;
using BlackBears.SQBA.General;
using BlackBears.SQBA.Modules.Purchasing.RemoteValidation;
using BlackBears.SQBA.Networking;
using BlackBears.SQBA.Utils;
using SimpleJSON;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

using UProduct = UnityEngine.Purchasing.Product;
using UProductType = UnityEngine.Purchasing.ProductType;
using UProductMetadata = UnityEngine.Purchasing.ProductMetadata;

namespace BlackBears.SQBA.Modules.Purchasing
{

    internal sealed class UnityPurchasesModule : PurchasesModule, IStoreListener
    {

        private PurchasesInitializationHandler initializationHandler;

        private IStoreController controller;
        private IExtensionProvider extensions;

        private List<UProduct> pendingProducts = new List<UProduct>();

        private HashSet<string> validatingTransactions = new HashSet<string>();

        internal UnityPurchasesModule(INetworkBehaviour networkBehaviour, JSONNode data = null) : base(networkBehaviour, data)
        {
        }

        internal override bool Initialized => controller != null && extensions != null;

        private UProduct ActiveSubscription
        {
            get
            {
                UProduct activeSubscription = null;
                foreach (var availableSubscriptionProduct in controller.products.all)
                {
                    if (availableSubscriptionProduct.receipt == null ||
                        availableSubscriptionProduct.definition.type != UProductType.Subscription) continue;

                    SubscriptionInfo subInfo = new SubscriptionManager(availableSubscriptionProduct, null).getSubscriptionInfo();
                    if (subInfo.isSubscribed() == Result.True) activeSubscription = availableSubscriptionProduct;
                }
                return activeSubscription;
            }
        }

        internal override void Initialize(PurchaseSQBAItem[] purchases, Block onSuccess, FailBlock onFail)
        {
            if (initializationHandler != null) return;

            Logger.LogMessage("Initializing Unity Purchases module...");

            state.CurrentPurchaseItems = purchases;

            initializationHandler = new PurchasesInitializationHandler(onSuccess, onFail);

            if(UnityEngine.Application.internetReachability == UnityEngine.NetworkReachability.NotReachable)
            {
                OnFailInit(null);
                return;
            }

            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            foreach (PurchaseSQBAItem purchaseItem in state.CurrentPurchaseItems)
            {
                builder.AddProduct(purchaseItem.IapName, ConvertType(purchaseItem.Type),
                new IDs
                {
                    { purchaseItem.IosIapName, AppleAppStore.Name},
                    { purchaseItem.AndroidIapName, GooglePlay.Name }
                });
            }
            UnityPurchasing.Initialize(this, builder);
        }

        protected override void InvokeGetProductsInfo(PurchaseItemBlock onSuccess, FailBlock onFail)
        {
            foreach (PurchaseSQBAItem purchaseItem in state.CurrentPurchaseItems)
            {
                var product = controller.products.WithID(purchaseItem.IapName);
                if (product != null)
                {
                    purchaseItem.Update(ConvertMetaData(product.metadata));
                }
            }

            onSuccess.SafeInvoke(state.CurrentPurchaseItems);
        }

        protected override void InvokePurchaseItem(PurchaseSQBAItem purchaseItem, Block<PurchaseAnswer> onSuccess,
            Block onCancel, FailBlock onFail)
        {
            var purchaseProduct = controller.products.WithID(purchaseItem.IapName);
            if (purchaseProduct != null && purchaseProduct.availableToPurchase)
            {
                PurchaseItemHandler purchaseHandler = new PurchaseItemHandler(purchaseItem, onSuccess, onCancel, onFail);
                purchaseHandlers.Add(purchaseHandler);

#if UNITY_ANDROID
                if (purchaseProduct.definition.type == UProductType.Subscription) UpdateSubscriptionProduct(purchaseProduct);
                else controller.InitiatePurchase(purchaseHandler.IapName);
#else
                controller.InitiatePurchase(purchaseHandler.IapName);
#endif
            }
            else
            {
                onFail.SafeInvoke(FailCode.FailedPurchase, "There is no active and available product with: " + purchaseItem.IapName);
            }
        }

        internal override void RestorePurchases(Block onSuccess, FailBlock onFail)
        {
            Logger.LogMessage("Restoring purchases...");

#if UNITY_EDITOR || UNITY_ANDROID
            Logger.LogMessage("There is nothing to restore.");

            onSuccess.SafeInvoke();
#elif UNITY_IOS
            if (isRestoringPurchases)
            {
                onFail.SafeInvoke(FailCode.PurchaseRestorationInProcess, "Restoring purchases already in process.");
                return;
            }
            isRestoringPurchases = true;

            foreach (StoredPurchase purchase in state.StoredPurchases) purchase.launchesToInvalid = -1;

            restoreHandler = new PurchasesRestoreHandler(onSuccess, onFail);
            extensions.GetExtension<IAppleExtensions>().RestoreTransactions((result) =>
            {
                if (result) restoreHandler.CallSuccess();
                else restoreHandler.CallFail();

                Logger.LogMessage("Restoring result: " + (result ? "success." : "fail."));

                isRestoringPurchases = false;

                Manager.Instance.SaveStates();
            });
#endif
        }

        internal override void ProcessPendingProducts(PurchasesGlobalBlock onPending)
        {
            base.ProcessPendingProducts(onPending);

            foreach (var pendingProduct in pendingProducts)
            {
                pendingProductsHandler.CallPending(pendingProduct.definition.id);
            }
            pendingProducts.Clear();
        }

        internal override void CheckPurchaseIsActive(PurchaseSQBAItem purchaseItem, Block<bool> onFinish)
        {
            Logger.LogMessage("Start checking purchase: " + purchaseItem.IapName);

            StoredPurchase storedPurchase = state.GetStoredPurchase(purchaseItem.IapName);
            var product = controller.products.WithID(purchaseItem.IapName);

            if (storedPurchase == null)
            {
                Logger.LogMessage("There is no stored purchase: " + purchaseItem.IapName);

                onFinish.SafeInvoke(false);
                return;
            }
            else
            {
                Logger.LogFormat("Launches left: {0} needValidation: {1}", storedPurchase.launchesToInvalid, storedPurchase.IsNeedValidation);
            }

            PurchasesValidationHandler validationHandler = new PurchasesValidationHandler(() =>
            {
                Logger.LogMessage("Item: " + purchaseItem.IapName + (storedPurchase.IsActive ? " active." : " not active."));

                onFinish.SafeInvoke(storedPurchase.IsActive);
            }, (errorCode, message) =>
            {
                Logger.LogMessage("Item: " + purchaseItem.IapName + (storedPurchase.IsActive ? " active." : " not active."));

                onFinish.SafeInvoke(storedPurchase.IsActive);
            });

            if (storedPurchase.IsNeedValidation)
            {
                ProcessLocalValidation(product, validationHandler);
            }
            else
            {
                Logger.LogMessage("Item: " + purchaseItem.IapName + (storedPurchase.IsActive ? " active." : " not active."));

                onFinish.SafeInvoke(storedPurchase.IsActive);
            }
        }

        internal override void ValidatePurchase(PurchaseSQBAItem purchaseItem, FailBlock onFail)
        {
            Logger.LogMessage("Start validating purchase: " + purchaseItem.IapName);

            PurchaseItemHandler purchaseHandler = new PurchaseItemHandler(purchaseItem);
            purchaseHandlers.Add(purchaseHandler);

            var product = controller.products.WithID(purchaseItem.IapName);
            if (product != null) StorePurchase(product);
            else onFail.SafeInvoke(FailCode.FailedPurchaseValidation, "There is no Product with the same product_id");
        }

        internal override void LocalCheckForActiveSubscriptions(Block onSuccess, FailBlock onFail)
        {
#if UNITY_IOS
                extensions.GetExtension<IAppleExtensions>().RestoreTransactions(result => 
                    {
                        if(result) onSuccess.SafeInvoke();
                        else onFail.SafeInvoke();
                    });      
#endif
        }

        internal override void CheckForActiveSubscriptions(string playerId, Block<List<string>> onSuccess, FailBlock onFail)
        {
#if UNITY_IOS && !UNITY_EDITOR
            string randKey = BlackBears.Utils.Security.GetRandKey();
            networkBehaviour.RequestSubscriptionValidation(playerId, randKey, state.SubscriptionCheckNeedReceipt, (response) =>
            {
                Logger.LogMessage("Subscriptions response: " + response.ToString());

                List<string> activeSubs = new List<string>();
                foreach (string sub in response["subs_list"].Children) activeSubs.Add(sub);

                string expectedString = string.Format("verify{0}{1}", string.Join("", activeSubs), Manager.Instance.State.DeviceId);

                bool isKeyValid = Utils.Security.IsServerKeyValid(response["out_key"], expectedString, randKey);
                if (isKeyValid)
                {
                    onSuccess.SafeInvoke(activeSubs);
                }
                else
                {
                    onFail.SafeInvoke(FailCode.FailedSubscriptionCheck, "Something went wrong with verify keys");
                }

                state.SubscriptionCheckNeedReceipt = response["need_receipt"].AsBool;
            }, (errorCode, message) =>
            {
                Logger.LogError(errorCode, message);
                onFail.SafeInvoke(errorCode, message);
            });
#elif UNITY_ANDROID
            List<string> activeSubs = new List<string>();
            if (ActiveSubscription != null)
            {
                Logger.LogMessage("Active subscription found: " + ActiveSubscription.definition.id);
                activeSubs.Add(ActiveSubscription.definition.id);
            }
            onSuccess.SafeInvoke(activeSubs);
#else
            onSuccess.SafeInvoke(new List<string>());
#endif
        }

        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Logger.LogMessage("Purchases module initialized. Unity");

            this.controller = controller;
            this.extensions = extensions;

            initializationHandler.CallSuccess();
            initializationHandler = null;
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason reason)
        {
            OnFailInit(reason.ToString());
        }

        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e)
        {
            Logger.LogMessage($"Processing purchase: {e.purchasedProduct.definition.id} transaction: {e.purchasedProduct.transactionID}");

            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(e.purchasedProduct.definition.id);
            if (purchaseHandler == null)
            {
                StorePendingProduct(e.purchasedProduct);
                return PurchaseProcessingResult.Pending;
            }
            else
            {
                StorePurchase(e.purchasedProduct);
                return PurchaseProcessingResult.Complete;
            }
        }

        void IStoreListener.OnPurchaseFailed(UProduct i, PurchaseFailureReason p)
        {
            Logger.LogMessage("Purchase " + i.definition.id + " failed with reason: " + p);

            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(i.definition.id);
            if (p == PurchaseFailureReason.UserCancelled)
            {
                if (globalHandler != null) globalHandler.CallCancel(i.definition.id);
                if (purchaseHandler != null) purchaseHandler.CallCancel();
            }
            else
            {
                if (globalHandler != null) globalHandler.CallFail(i.definition.id, FailCode.FailedPurchase, "Failed with reason " + p);
                if (purchaseHandler != null) purchaseHandler.CallFail(p);
            }

            if (purchaseHandler != null) purchaseHandlers.Remove(purchaseHandler);
        }

        private void OnFailInit(string reason)
        {
            string failMessage = "Purchases module failed to initialize: " + reason;

            Logger.LogMessage(failMessage);

            initializationHandler.CallFail(FailCode.PurchasesModuleNotInitialized, failMessage);
            initializationHandler = null;
        }

        private void StorePendingProduct(UProduct product)
        {
            Logger.LogMessage("Need to process pending iap: " + product.definition.id);
            if (pendingProductsHandler != null)
            {
                pendingProductsHandler.CallPending(product.definition.id);
            }
            else
            {
                pendingProducts.Add(product);
            }
        }

        private void StorePurchase(UProduct product)
        {
            if (!validatingTransactions.Add(product.transactionID))
            {
                Logger.LogMessage("Purchase already validating now");
                return;
            }

            Logger.LogMessage("Storing purchase...");

            StoredPurchase purchase = new StoredPurchase(product.definition.id);
            StoredPurchase oldPurchase = state.GetStoredPurchase(purchase.IapId);

            if (oldPurchase != null) purchase = oldPurchase;
            else
            {
                if (product.definition.type == UProductType.NonConsumable)
                {
                    state.StoredPurchases.Add(purchase);
                }
            }

            PurchasesValidationHandler validationHandler = new PurchasesValidationHandler(() =>
            {
                Logger.LogMessage("Storing: success validation.");

                validatingTransactions.Remove(product.transactionID);
                SuccessPurchaseValidation(product);
            }, (errorCode, message) =>
            {
                Logger.LogError(errorCode, "Storing: failed validation.");

                validatingTransactions.Remove(product.transactionID);
                FailedPurchaseValidation(product);
            });

            if (purchase.IsNeedValidation)
            {
                ProcessLocalValidation(product, validationHandler);
            }
            else
            {
                Logger.LogMessage("Storing: isNeedValidation == false");

                validationHandler.CallSuccess();
            }
        }

        private void ProcessLocalValidation(UProduct product, PurchasesValidationHandler validationHandler)
        {
            bool validPurchase = true;

#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
            CrossPlatformValidator validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(),
                Manager.Instance.State.AppId);
            try
            {
                IPurchaseReceipt[] resultRecipes = validator.Validate(product.receipt);

                Logger.LogMessage("Receipt is valid. Contents:");
                foreach (IPurchaseReceipt productReceipt in resultRecipes)
                {
                    Logger.LogMessage(productReceipt.productID);
                    Logger.LogMessage(productReceipt.purchaseDate.ToString());
                }
            }
            catch (IAPSecurityException e)
            {
                Logger.LogMessage("Purchase local validation failed with exception: " + e);

                validPurchase = false;
            }
#endif
            if (validPurchase)
            {
                Logger.LogMessage("Purchase local validation passed.");

                ProcessRemoteValidation(product, validationHandler);
            }
            else
            {
                StoredPurchase storedPurchase = state.GetStoredPurchase(product.definition.id);
                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail("Local validation failed.");
                }
            }
        }

        private void ProcessRemoteValidation(UProduct product, PurchasesValidationHandler validationHandler)
        {
            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.definition.id);
            StoredPurchase storedPurchase = state.GetStoredPurchase(product.definition.id);

#if !UNITY_EDITOR && UNITY_IOS
            Logger.LogMessage("Processing iOS remote validation...");

            PurchaseValidationIos validationInfo = new PurchaseValidationIos(product, purchaseHandler.Item, isRestoringPurchases);
            if (!validationInfo.EnoughData)
            {
                Logger.LogMessage("Can't start validating. Not enough information provided.");

                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail("Not enough information provided.");
                }
                return;
            }

            networkBehaviour.RequestIOSPurchaseValidation(validationInfo, (data) =>
            {
                Logger.LogRequest("R: ios/billing", data);

                CheckRemoteValidationData(product, validationInfo, data, validationHandler);
            }, (errorCode, message) =>
            {
                Logger.LogError(errorCode, message);

                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail(message);
                }
            });

#elif !UNITY_EDITOR && UNITY_ANDROID
            Logger.LogMessage("Processing Android remote validation...");

            PurchaseValidationAndroid validationInfo = new PurchaseValidationAndroid(product, purchaseHandler.Item);
            if (!validationInfo.EnoughData)
            {
                Logger.LogMessage("Can't start validating. Not enough information provided.");

                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail("Not enough information provided.");
                }
                return;
            }

            networkBehaviour.RequestAndroidPurchaseValidation(validationInfo, (data) =>
            {
                CheckRemoteValidationData(product, validationInfo, data, validationHandler);
            }, (errorCode, message) =>
            {
                Logger.LogError(errorCode, message);

                if (storedPurchase != null) storedPurchase.MakeValidated(false);

                if (validationHandler != null)
                {
                    validationHandler.CallFail(message);
                }
            });
#else
            if (validationHandler != null)
            {
                validationHandler.CallSuccess();
            }
#endif
        }

#if UNITY_ANDROID
        void UpdateSubscriptionProduct(UProduct newProduct)
        {
            if (ActiveSubscription != null)
            {
                Logger.LogMessage("Old subscription found: " + ActiveSubscription.definition.id);

                IGooglePlayStoreExtensions googlePlayExtension = extensions.GetExtension<IGooglePlayStoreExtensions>();
                var googlePlayCallback = new System.Action<string, string>(googlePlayExtension.UpgradeDowngradeSubscription);
                SubscriptionManager.UpdateSubscriptionInGooglePlayStore(ActiveSubscription, newProduct, googlePlayCallback);
            }
            else controller.InitiatePurchase(newProduct.definition.id);
        }
#endif

        private void CheckRemoteValidationData(UProduct product, PurchaseValidationInfo validationInfo, JSONNode data,
            PurchasesValidationHandler validationHandler)
        {
            bool validPurchase = true;

            string outKey = data["out_key"].Value;

            bool serverVerified = data["verify"].AsBool;
            string expectedString = string.Format("verify{0}{1}{2}", serverVerified ? "true" : "false", product.definition.id, validationInfo.DeviceId);

            bool isKeyValid = Utils.Security.IsServerKeyValid(outKey, expectedString, validationInfo.RandKey);
            if (!isKeyValid)
            {
                Logger.LogMessage("Remote verification failed because of a different keys.");

                validPurchase = false;
            }
            else
            {
                validPurchase = serverVerified;

                Logger.LogFormat("Remote server verified: {0}", serverVerified ? "true" : "false");
            }

            StoredPurchase storedPurchase = state.GetStoredPurchase(product.definition.id);
            if (storedPurchase != null) storedPurchase.MakeValidated(validPurchase);

            if (validPurchase)
            {
                Logger.LogMessage("Remote validation passed.");

                if (validationHandler != null) validationHandler.CallSuccess();
            }
            else
            {
                Logger.LogError(FailCode.FailedPurchaseValidation, "Remote server validation failed.");

                if (validationHandler != null) validationHandler.CallFail("Remote server validation failed.");
            }
        }

        private void SuccessPurchaseValidation(UProduct product)
        {
            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.definition.id);
            if (globalHandler != null) globalHandler.CallSuccess(product.definition.id);
            if (purchaseHandler != null)
            {
                PurchaseAnswer answer;
#if !UNITY_EDITOR && UNITY_IOS
                answer = new iOSPurchaseAnswer(Manager.Instance.State.NativeDataAdapter.BundleReciept, product.transactionID);
#else
                answer = new PurchaseAnswer();
#endif

                purchaseHandler.CallSuccess(answer);
                purchaseHandlers.Remove(purchaseHandler);
            }

            controller.ConfirmPendingPurchase(product);
        }

        private void FailedPurchaseValidation(UProduct product)
        {
            StoredPurchase storedPurchase = state.GetStoredPurchase(product.definition.id);
            if (storedPurchase != null) storedPurchase.MakeValidated(false);

            PurchaseItemHandler purchaseHandler = GetPurchaseHandler(product.definition.id);
            if (globalHandler != null)
            {
                globalHandler.CallFail(product.definition.id, FailCode.FailedPurchaseValidation,
                    "Purchase failed. Can't pass remote verification");
            }
            if (purchaseHandler != null)
            {
                purchaseHandler.CallFail(FailCode.FailedPurchaseValidation);
                purchaseHandlers.Remove(purchaseHandler);
            }

            controller.ConfirmPendingPurchase(product);
        }

        private UProductType ConvertType(ProductType type)
        {
            switch (type)
            {
                case ProductType.Consumable: return UProductType.Consumable;
                case ProductType.NonConsumable: return UProductType.NonConsumable;
                case ProductType.Subscription: return UProductType.Subscription;
                default: return (UProductType)type;
            }
        }

        private ProductMetadata ConvertMetaData(UProductMetadata metadata)
        {
            return new ProductMetadata(metadata.isoCurrencyCode, metadata.localizedPriceString,
                metadata.localizedTitle);
        }

    }

}

#endif