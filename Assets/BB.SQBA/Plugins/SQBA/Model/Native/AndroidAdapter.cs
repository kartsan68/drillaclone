﻿using SimpleJSON;
using UnityEngine;

namespace BlackBears.SQBA.Native.Android
{
    #if UNITY_ANDROID

    public class AndroidAdapter : INativeAdapter
    {
        public string DeviceId { get { return GetDeviceId(); } }
        public string DeviceOSVersion { get { return GetDeviceOSVersion(); } }
        public string PendingQuery { get { return GetIntentQuery(); } }
        public string CurrentLocale { get { return GetCurentLocale(); } }
        
        public bool AdvertisingTrackingEnabled { get { throw new System.NotImplementedException(); } }
        public string AdvertisingIdentifier { get { return GetDeviceIAD(); } }
        public string FacebookExtInfo { get { return GetFacebookExtInfo(); } }

        static string GetDeviceId()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                AndroidJavaObject objResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
                AndroidJavaClass clsSecure = new AndroidJavaClass("android.provider.Settings$Secure");

                return clsSecure.CallStatic<string>("getString", objResolver, "android_id");
            }
        }

        static string GetDeviceOSVersion()
        {
            using (AndroidJavaClass buildVersion = new AndroidJavaClass("android.os.Build$VERSION"))
            {
                return buildVersion.GetStatic<string>("RELEASE");
            }
        }

        static string GetCurentLocale()
        {
            using (AndroidJavaClass localeCls = new AndroidJavaClass("java.util.Locale"))
            {
                using (AndroidJavaObject localeObj = localeCls.CallStatic<AndroidJavaObject>("getDefault"))
                {
                    return localeObj.Call<string>("getCountry").ToLowerInvariant();
                }
            }
        }

        static string GetIntentQuery()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
                AndroidJavaObject uriData = intent.Call<AndroidJavaObject>("getData");

                return uriData != null ? uriData.Call<string>("getQuery") : null;

            }
        }

        static string GetDeviceIAD()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                try 
                {
                    AndroidJavaClass clsAds = new AndroidJavaClass("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                    AndroidJavaObject info = clsAds.CallStatic<AndroidJavaObject>("getAdvertisingIdInfo", currentActivity);
                    return info.Call<string>("getId");
                } 
                catch
                {
                    return string.Empty;
                }
            }
        }

        static string GetFacebookExtInfo()
        {
            JSONArray parameters = new JSONArray();
            parameters.Add("a2");
            parameters.Add(GetPackageName());

            parameters.Add(GetVersionCode());
            parameters.Add(GetVersionName());

            parameters.Add(GetDeviceOSVersion());
            parameters.Add(GetModel());

            parameters.Add(GetLocale());
            parameters.Add(GetTimeZone());
            parameters.Add(GetCarrier());

            parameters.Add(GetDisplayWidth());
            parameters.Add(GetDisplayHeight());
            parameters.Add(GetDisplayDensity());

            parameters.Add(GetCPUCores());

            parameters.Add(GetTotalExternalStorageGB());
            parameters.Add(GetAvailableExternalStorageGB());

            parameters.Add(GetTimeZoneIdentifier());

            return parameters.ToString();
        }

        public string BundleReciept { get { throw new System.NotImplementedException(); } }

        #region Internal
        static AndroidJavaObject GetCurrentActivity()
        {
            using (AndroidJavaClass clsUnity = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                return clsUnity.GetStatic<AndroidJavaObject>("currentActivity");
            }
        }

        #region Facebook

        static string GetPackageName()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                return currentActivity.Call<string>("getPackageName");
            }
        }

        static int GetVersionCode()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                try 
                {
                    AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");
                    AndroidJavaObject packageInfo = packageManager.Call<AndroidJavaObject>("getPackageInfo", GetPackageName(), 0);
                    return packageInfo.Get<int>("versionCode");
                }
                catch
                {
                    return -1;
                }
            }
        }

        static string GetVersionName()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                try 
                {
                    AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");
                    AndroidJavaObject packageInfo = packageManager.Call<AndroidJavaObject>("getPackageInfo", GetPackageName(), 0);
                    return packageInfo.Get<string>("versionName");
                }
                catch
                {
                    return "error";
                }
            }
        }

        static string GetModel()
        {
            using (AndroidJavaClass buildCls = new AndroidJavaClass("android.os.Build"))
            {
                return buildCls.GetStatic<string>("MODEL");
            }
        }

        static string GetLocale()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                AndroidJavaObject localeCls = null;
                try 
                {
                    AndroidJavaObject resourcesCls = currentActivity.Call<AndroidJavaObject>("getResources");
                    AndroidJavaObject confCls = resourcesCls.Call<AndroidJavaObject>("getConfiguration", currentActivity);
                    localeCls = confCls.Get<AndroidJavaObject>("locale");
                } 
                catch 
                {
                    AndroidJavaClass defaultLocaleCls = new AndroidJavaClass("java.util.Locale");
                    localeCls = defaultLocaleCls.CallStatic<AndroidJavaObject>("getDefault");
                }

                string language = localeCls.Call<string>("getLanguage");
                string country = localeCls.Call<string>("getCountry");

                return string.Format("{0}_{1}", language, country);
            }
        }

        static string GetTimeZone()
        {
            using (AndroidJavaClass timeZoneCls = new AndroidJavaClass("java.util.TimeZone"))
            {
                AndroidJavaObject timeZone = timeZoneCls.CallStatic<AndroidJavaObject>("getDefault");
                bool daylight = timeZone.Call<bool>("inDaylightTime", new AndroidJavaObject("java.util.Date"));
                int style = timeZoneCls.GetStatic<int>("SHORT");

                return timeZone.Call<string>("getDisplayName", daylight, style).ToUpper();
            }
        }

        static string GetTimeZoneIdentifier()
        {
            using (AndroidJavaClass timeZoneCls = new AndroidJavaClass("java.util.TimeZone"))
            {
                AndroidJavaObject timeZone = timeZoneCls.CallStatic<AndroidJavaObject>("getDefault");
                return timeZone.Call<string>("getID");
            }
        }

        static string GetCarrier()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                AndroidJavaClass contextCls = new AndroidJavaClass("android.content.Context");
                string telephonyServiceKey = contextCls.GetStatic<string>("TELEPHONY_SERVICE");
                AndroidJavaObject telephonyManager = currentActivity.Call<AndroidJavaObject>("getSystemService", telephonyServiceKey);
                if (telephonyManager != null)
                {
                    return telephonyManager.Call<string>("getNetworkOperatorName");
                }
                else
                {
                    return "NoCarrier";
                }
            }
        }

        static int GetDisplayWidth()
        {
            using (AndroidJavaObject display = GetDisplayMetrics())
            {
                return display != null ? display.Get<int>("widthPixels") : 0;
            }
        }

        static int GetDisplayHeight()
        {
            using (AndroidJavaObject display = GetDisplayMetrics())
            {
                return display != null ? display.Get<int>("heightPixels") : 0;
            }
        }

        static string GetDisplayDensity()
        {
            using (AndroidJavaObject display = GetDisplayMetrics())
            {
                return display != null ? display.Get<float>("density").ToString("0.00") : "0";
            }
        }

        static AndroidJavaObject GetDisplayMetrics()
        {
            using (AndroidJavaObject currentActivity = GetCurrentActivity())
            {
                try
                {
                    AndroidJavaClass contextCls = new AndroidJavaClass("android.content.Context");
                    string telephonyServiceKey = contextCls.GetStatic<string>("WINDOW_SERVICE");
                    AndroidJavaObject windowManager = currentActivity.Call<AndroidJavaObject>("getSystemService", telephonyServiceKey);
                    if (windowManager != null)
                    {
                        AndroidJavaObject display = windowManager.Call<AndroidJavaObject>("getDefaultDisplay");
                        AndroidJavaObject displayMetrics = new AndroidJavaObject("android.util.DisplayMetrics");
                        display.Call("getMetrics", displayMetrics);

                        return displayMetrics;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch
                {
                    return null;
                }
            }
        }

        static int GetCPUCores()
        {
            using (AndroidJavaClass runtimeCls = new AndroidJavaClass("java.lang.Runtime"))
            {
                AndroidJavaObject runtime = runtimeCls.CallStatic<AndroidJavaObject>("getRuntime");
                int cores = runtime.Call<int>("availableProcessors");
                return System.Math.Max(cores, 1);
            }
        }

        static long GetTotalExternalStorageGB()
        {
            return GetExternalStorageGB("getBlockCount");
        }

        static long GetAvailableExternalStorageGB()
        {
            return GetExternalStorageGB("getAvailableBlocks");
        }

        static long GetExternalStorageGB(string storageProperty)
        {
            long storageGB = 0;
            if (ExternalStorageExists())
            {
                try 
                {
                    using (AndroidJavaClass environmentCls = new AndroidJavaClass("android.os.Environment"))
                    {
                        AndroidJavaObject path =  environmentCls.CallStatic<AndroidJavaObject>("getExternalStorageDirectory");
                        AndroidJavaObject stat = new AndroidJavaObject("android.os.StatFs", path.Call<string>("getPath"));
                        long blocksCount = (long)stat.Call<int>(storageProperty);
                        long blockSize = (long)stat.Call<int>("getBlockSize");

                        storageGB = blocksCount * blockSize;
                    }
                }
                catch {}

                storageGB = ConvertBytesToGB((double)storageGB);
            }

            return storageGB;
        }

        static bool ExternalStorageExists()
        {
            using (AndroidJavaClass environmentCls = new AndroidJavaClass("android.os.Environment"))
            {
                string mediaMounted = environmentCls.GetStatic<string>("MEDIA_MOUNTED");
                string externalStorageState = environmentCls.CallStatic<string>("getExternalStorageState");

                return mediaMounted.Equals(externalStorageState);
            }
        }

        static long ConvertBytesToGB(double bytes)
        {
            return (long)System.Math.Round(bytes / (1024.0 * 1024.0 * 1024.0));
        }

        #endregion

        #endregion
    }

    #endif
}