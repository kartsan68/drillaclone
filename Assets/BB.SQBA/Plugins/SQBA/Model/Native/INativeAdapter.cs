﻿namespace BlackBears.SQBA.Native
{
    interface INativeAdapter
    {
        string DeviceId { get; }
        string DeviceOSVersion { get; }
        string CurrentLocale { get; } 
        string PendingQuery { get; }

        bool AdvertisingTrackingEnabled { get; }
        string AdvertisingIdentifier { get; }
        string FacebookExtInfo { get; }

        string BundleReciept { get; }
    }
}
