﻿namespace BlackBears.SQBA.Native.Editor
{
    sealed class EditorAdapter : INativeAdapter
    {
        public string DeviceId { get { return "editor"; } }
        public string DeviceOSVersion { get { return "editor"; } }
        public string PendingQuery { get { return null; } }
        public string CurrentLocale { get { return "unk"; } }

        public bool AdvertisingTrackingEnabled { get { return false; } }
        public string AdvertisingIdentifier { get { return "00000000-0000-0000-0000-000000000000"; } }
        public string FacebookExtInfo { get { return ""; } }

        public string BundleReciept { get { throw new System.NotImplementedException(); } }
    }
}
