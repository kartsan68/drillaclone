﻿using System.Runtime.InteropServices;

namespace BlackBears.SQBA.Native.iOS
{
    #if UNITY_IOS

    sealed class IOSAdapter : INativeAdapter
    {
        public string DeviceId { get { return SQBAGetDeviceId(); } }
        public string DeviceOSVersion { get { return SQBAGetDeviceOSVersion(); } }
        public string PendingQuery { get { return SQBAGetPendingQuery(); } }
        public string CurrentLocale { get { return SQBAGetCurrentLocale(); } }
        public string AdvertisingIdentifier { get { return SQBAGetDeviceIFA(); } }
        public string FacebookExtInfo { get { return SQBAGetFacebookExtInfo(); } }
        public bool AdvertisingTrackingEnabled { get { return SQBAGetAdvertisingTrackingEnabled(); } }
        public string BundleReciept { get { return SQBAGetBundleReceipt(); } }

        [DllImport("__Internal")] static extern string SQBAGetDeviceId();
        [DllImport("__Internal")] static extern string SQBAGetDeviceOSVersion();
        [DllImport("__Internal")] static extern string SQBAGetPendingQuery();
        [DllImport("__Internal")] static extern string SQBAGetCurrentLocale();
        [DllImport("__Internal")] static extern string SQBAGetDeviceIFA();
        [DllImport("__Internal")] static extern string SQBAGetFacebookExtInfo();
        [DllImport("__Internal")] static extern bool SQBAGetAdvertisingTrackingEnabled();
        [DllImport("__Internal")] static extern string SQBAGetBundleReceipt();
    }

    #endif
}
