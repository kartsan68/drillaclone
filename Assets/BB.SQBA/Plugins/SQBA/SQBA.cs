﻿using BlackBears.SQBA.General;
using BlackBears.SQBA.Utils;
using BlackBears.SQBA.Modules.Purchasing;
using BlackBears.SQBA.Modules.GDPR;
using System.Collections.Generic;
using UnityEngine.Events;
using BlackBears.SQBA.Modules.Files;
using System.IO;
using SimpleJSON;

namespace BlackBears.SQBA
{
    public static class SQBA
    {
        #region Core
        public static bool IsStarted { get { return Manager.Instance.State.ApiKey != null; } }
        public static float StoreTax => Manager.Instance.StoreTax;

        public static UnityEvent OnSaveFailed => Manager.Instance.OnSaveFailed;
        public static bool NoInternetConection => Manager.Instance.State.NoInternetConection && Manager.Instance.State.Configuration.WorkWithoutInternet;

        public static bool EncryptionFiles => Manager.Instance.State.Configuration.EncryptionFiles;

        public static void Start(Configuration configuration, Block onSuccess = null, FailBlock onFail = null)
        {
            Logger.Verbose = configuration.VerboseLogging;

            if (configuration == null)
            {
                onFail.SafeInvoke(FailCode.ConfigurationNotProvided, "There is no configuration provided at.");
                return;
            }
            if (configuration.ApiKey == null)
            {
                onFail.SafeInvoke(FailCode.ApiKeyNotProvided, "There is no apiKey provided at.");
                return;
            }

            Manager.CreateManager((manager) =>
            {
                manager.Start(configuration);
                manager.FilesModule.UpdateRuntimeReplaceAllowed(true);
                manager.VersionCheck(onSuccess, onFail);
            });
        }

        public static string CurrentLocale()
        {
            return Manager.Instance.State.CurrentLocale;
        }

        public static string VersionString()
        {
            return Common.Version;
        }

        public static void FromJson(JSONNode json)
        {
            if (!IsStarted) return;

            Manager.Instance.FromJson(json);
        }

        public static JSONNode ToJson()
        {
            if (!IsStarted) return null;

            return Manager.Instance.ToJson();
        }

        public static PurchaserType GetPurchaserType()
        {
            if (!IsStarted) return PurchaserType.NEWBIE;

            return Manager.Instance.GetPurchaserType();
        }
        #endregion

        #region Files

        public static string PathToFile(string fileName)
        {
            if (!IsStarted) return null;
#if UNITY_EDITOR
            if (!Manager.Instance.State.Configuration.LoadFilesInEditor) return null;
#endif

            return Manager.Instance.FilesState.GetFilePath(fileName);
        }

        public static string GetDecodeFile(string path)
        {
            if (!IsStarted) return null;
#if UNITY_EDITOR
            if (!Manager.Instance.State.Configuration.LoadFilesInEditor) return null;
#endif

            return FilesModule.DecodeFileByPath(path);
        }

        public static string DecodeFileByPath(string filePath)
        {
            if (!IsStarted) return null;

            return FilesModule.DecodeFileByPath(filePath);
        }
        public static string DecodeFile(string fileText)
        {
            if (!IsStarted) return null;

            return FilesModule.DecodeFile(fileText);
        }

        public static string GetFile(string fileName)
        {
            if (!IsStarted) return null;
#if UNITY_EDITOR
            if (!Manager.Instance.State.Configuration.LoadFilesInEditor) return null;
#endif

            var path = Manager.Instance.FilesState.GetFilePath(fileName);

            if (string.IsNullOrEmpty(path)) return null;

            if (Manager.Instance.State.Configuration.EncryptionFiles) return GetDecodeFile(path);
            else return File.ReadAllText(path);
        }

        public static string GetFilePath(string fileName)
        {
            if (!IsStarted) return null;
#if UNITY_EDITOR
            if (!Manager.Instance.State.Configuration.LoadFilesInEditor) return null;
#endif

            var path = Manager.Instance.FilesState.GetFilePath(fileName);

            return path;
        }
        #endregion

        #region Purchases
        public static void InitializePurchases(PurchaseSQBAItem[] purchaseItems,
                                               Block onSuccess = null,
                                               FailBlock onFail = null)
        {
            if (!IsStarted)
            {
                NotStartedError(onFail);
                return;
            }

            Manager.Instance.PurchasesModule.Initialize(purchaseItems, onSuccess, onFail);
        }

        public static void SetGlobalValidationObserver(PurchasesGlobalBlock onSuccess = null,
                                                       PurchasesGlobalBlock onCancel = null,
                                                       PurchasesGlobalBlock onFail = null)
        {
            if (!IsStarted) return;

            Manager.Instance.PurchasesModule.SetGlobalValidationObserver(onSuccess, onCancel, onFail);
        }

        public static void GetPurchaseItemsPrices(PurchaseItemBlock onSuccess = null,
                                                  FailBlock onFail = null)
        {
            if (!IsStarted)
            {
                NotStartedError(onFail);
                return;
            }

            Manager.Instance.PurchasesModule.GetProductsInfo(onSuccess, onFail);
        }

        public static void BuyProduct(PurchaseSQBAItem purchaseItems,
                                      Block<PurchaseAnswer> onSuccess = null,
                                      Block onCancel = null,
                                      FailBlock onFail = null)
        {
            if (!IsStarted)
            {
                NotStartedError(onFail);
                return;
            }

            Manager.Instance.PurchasesModule.PurchaseItem(purchaseItems, answer =>
            {
                Manager.Instance.AddPurchase(purchaseItems.DollarEquivalent);
                onSuccess.SafeInvoke(answer);
            }, onCancel, onFail);
        }

        public static void ProcessPendingProducts(PurchasesGlobalBlock onPending, FailBlock onFail = null)
        {
            if (!IsStarted)
            {
                NotStartedError(onFail);
                return;
            }

            Manager.Instance.PurchasesModule.ProcessPendingProducts(onPending);
        }

        public static void RestorePurchases(Block onSuccess, FailBlock onFail)
        {
            if (!IsStarted)
            {
                NotStartedError(onFail);
                return;
            }

            Manager.Instance.PurchasesModule.RestorePurchases(onSuccess, onFail);
        }

        public static void ValidatePurchase(PurchaseSQBAItem purchaseItem, FailBlock onFail)
        {
            if (!IsStarted) return;

            Manager.Instance.PurchasesModule.ValidatePurchase(purchaseItem, onFail);
        }

        public static void CheckPurchaseIsActive(PurchaseSQBAItem purchaseItem, Block<bool> onFinish)
        {
            if (!IsStarted) return;

            Manager.Instance.PurchasesModule.CheckPurchaseIsActive(purchaseItem, onFinish);
        }

        public static void CheckForActiveSubscriptions(string playerId, Block<List<string>> onSuccess, FailBlock onFail)
        {
            if (!IsStarted) return;

            Manager.Instance.PurchasesModule.CheckForActiveSubscriptions(playerId, onSuccess, onFail);
        }

        public static void LocalCheckForActiveSubscriptions(Block onSuccess, FailBlock onFail)
        {
            if (!IsStarted) return;

            Manager.Instance.PurchasesModule.LocalCheckForActiveSubscriptions(onSuccess, onFail);
        }
        #endregion

        #region Codes
        public static void SubscribeForBonusData(Block<SimpleJSON.JSONNode> dataCallback)
        {
            if (!IsStarted) return;

            Manager.Instance.BonusDataCallback = dataCallback;
        }
        public static void UnsubscribeForBonusData()
        {
            if (!IsStarted) return;

            Manager.Instance.BonusDataCallback = null;
        }
        #endregion

        #region GDPR
        public static void SignUpPrivacyPolicy(string playerId, string countryCode, Block<GDPRStatus> onSuccess, FailBlock onFail)
        {
            if (!IsStarted) return;

            Manager.Instance.SignUpPrivacyPolicy(playerId, countryCode, onSuccess, onFail);
        }
        #endregion

        #region Additional Info
        public static bool NeedForceUpdate
        {
            get
            {
                if (!IsStarted) return false;
                return Manager.Instance.State.NeedForceUpdate;
            }
        }

        public static bool RatingEnabled
        {
            get
            {
                if (!IsStarted) return false;
                return Manager.Instance.State.RatingEnabled;
            }
        }

        public static bool IsProduction
        {
            get
            {
                if (!IsStarted) return false;
                return Manager.Instance.State.IsProduction;
            }
        }

        public static long DayXTimestamp
        {
            get
            {
                if (!IsStarted) return 0;
                return Manager.Instance.State.DayXTimestamp;
            }
        }
        #endregion

        #region Common
        static void NotStartedError(FailBlock onFail)
        {
            onFail.SafeInvoke(FailCode.NotInitializedProperly,
                              "SQBA is not started yet. Call Start() before any other SQBA calls.");
        }
        #endregion
    }
}
