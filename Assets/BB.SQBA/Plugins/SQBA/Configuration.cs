﻿using UnityEngine;

namespace BlackBears.SQBA
{
    [CreateAssetMenu(fileName = "SQBAConfiguration", menuName = "BlackBears/SQBA Configuration", order = 3)]
    public class Configuration : ScriptableObject
    {
        [SerializeField] string version;
        [SerializeField] string apiUrl;
        [SerializeField] string apiKey;
        [SerializeField] string secretKey;
        [SerializeField] bool verboseLogging;
        [SerializeField] string urlScheme;
        [SerializeField] bool versionCheckOnResume;
        [SerializeField] bool forceFilesCheck = true;
        [Tooltip("Использовать ли конфиг со скубы если мы в редакторе")]
        [SerializeField] bool loadFilesInEditor;
        [SerializeField] bool workWithoutInternet = true;
        [SerializeField] bool encryptionFiles = true;
        [SerializeField] string configName = "sqba_defaults.json";
        [SerializeField] string[] encryptionFilesExtensions = new string[]{".json"};

        internal string Version => version;
        internal string ApiUrl => apiUrl;
        internal string ApiKey => apiKey;
        internal string SecretKey => secretKey;
        internal bool VerboseLogging => verboseLogging;
        internal string UrlScheme => urlScheme;
        internal bool VersionCheckOnResume => versionCheckOnResume;
        internal bool ForceFilesCheck => forceFilesCheck;
        internal bool LoadFilesInEditor => loadFilesInEditor;
        internal bool WorkWithoutInternet => workWithoutInternet;
        internal bool EncryptionFiles => encryptionFiles;
        internal string ConfigName => configName;
        internal string[] EncryptionFilesExtensions => encryptionFilesExtensions;
    }
}
