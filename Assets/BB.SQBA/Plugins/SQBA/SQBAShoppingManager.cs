using System;
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Analytics;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.Localization;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Features.Save;
using BlackBears.SQBA.Modules.Purchasing;
using BlackBears.SQBA.Utils;
using BlackBears.Utils;
using SimpleJSON;
using SQBAModule = BlackBears.SQBA.SQBA;

namespace BlackBears.SQBA
{
    public abstract class SQBAShoppingManager : ISaveable
    {
        private const string saveKey = "SQBAShoppingManager";
        private const string currentDayKey = "current_day";
        private const string itemsInQueueKey = "item_in_queue";
        private const string idKey = "id";
        private const string iapKey = "iap";
        private NotificationsFeature notifications;
        private LocalizationFeature localization;
        private SQBAShopItem[] shopItems;
        private SQBAShopItem currentSubscription;

        private ShopCurrency hardCurrency = null;
        private Dictionary<int, ShopCurrency> currencies = new Dictionary<int, ShopCurrency>();
        private AnalyticsFeature analytics;
        private ShopItemInQueue shopItemInQueue;
        private int currentDay;

        //Инициализируемся когда вся кора проинициализируется(кидаем сюда все shopItems распаршенные из конфига, 
        // и если есть сабмодуль GameServer то кидаем оттуда playerId)
        public void Init(SQBAShopItem[] items, string playerId = "1")
        {
            analytics = BBGC.Features.Analytics();
            this.shopItems = items;

            SQBAModule.InitializePurchases(items, () => OnSqbaPurchasesInitialized(playerId));
            notifications = BBGC.Features.GetFeature<NotificationsFeature>();
            localization = BBGC.Features.GetFeature<LocalizationFeature>();

            currentDay = DateUtils.SecondsToFullDays(BBGC.Features.GetFeature<ChronometerFeature>().TotalTime) + 1;
            int saveDay = 0;

            var saveManager = BBGC.Features.GetFeature<SaveFeature>().SaveManager;
            saveManager.SetSaveable(this);

            var saveData = BBGC.Features.GetFeature<SaveFeature>().SaveManager.GetSaveDataByKey(saveKey);
            if (saveData != null && saveData.data != null)
            {
                saveDay = saveData.data[currentDayKey].AsInt;
                if (saveData.data[itemsInQueueKey] != null)
                {
                    shopItemInQueue = new ShopItemInQueue(saveData.data[itemsInQueueKey][idKey].AsInt,
                        saveData.data[itemsInQueueKey][iapKey]);
                }
            }

            for (int i = saveDay; i < currentDay; i++)
            {
                analytics.DayHardCurrency(i.ToString(), hardCurrency?.ToValueString());
            }
        }
        //Восстановление покупок(по кнопке, при старте это не надо вызывать)
        public void RestorePurchases(Block onSuccess, FailBlock onFail, string playerId = "1")
        {
            SQBAModule.RestorePurchases(() =>
            {
#if !UNITY_EDITOR && UNITY_IOS
            SQBAModule.CheckForActiveSubscriptions(playerId,
                OnActiveSubscriptionsCome, OnActiveSubscriptionCheckFailed);
#endif
                onSuccess.SafeInvoke();
            }, onFail);
        }

        //Создать валюту
        public ShopCurrency CreateCurrency(int id, IProtectedVariable value, string nameForAnalytics,
            bool alwaysSendAnalytics = false, bool isHard = false)
        {
            if (currencies.ContainsKey(id)) UnityEngine.Debug.LogWarning("Currency with such id is already exists!");

            var item = new ShopCurrency(id, value, alwaysSendAnalytics, isHard, nameForAnalytics, this);
            currencies.Add(id, item);

            if (isHard)
            {
                if (hardCurrency != null) UnityEngine.Debug.LogWarning("Hard currency already exists!");
                else hardCurrency = item;
            }

            return item;
        }

        //Получить значение валюты
        public ShopCurrency GetCurrency(int id)
        {
            return currencies.ContainsKey(id) ? currencies[id] : null;
        }


        //Сделать покупку, additionalInformation - дополнительная информации для аналитики (например: с какого экрана была вызвана покупка...)
        public void PurchaseShopItem(SQBAShopItem shopItem, Block onSuccess, Block onCancel, FailBlock onFail, Dictionary<string, object> additionalInformation)
        {
            if (shopItem == null)
            {
                onFail.SafeInvoke();
                return;
            }

            shopItemInQueue = new ShopItemInQueue(shopItem.id, shopItem.IapName);

            SQBAModule.BuyProduct(shopItem, answer =>
            {
#if BBGC_ANALYTICS_FEATURE
                analytics.Purchase(shopItem.IapName, shopItem.equivalent * SQBAModule.StoreTax,
                    hardCurrency?.ToString(), SQBAModule.GetPurchaserType().ToString(), additionalInformation,
                    shopItem, answer);
#endif
                onSuccess.SafeInvoke();
            }, onCancel, onFail);
        }

        public SaveData ToSaveData()
        {
            var json = new JSONObject();
            json[currentDayKey] = currentDay;

            if (shopItemInQueue != null)
            {
                var itemInQueueNode = new JSONObject();
                itemInQueueNode[idKey] = shopItemInQueue.id;
                itemInQueueNode[iapKey] = shopItemInQueue.iap;
                json[itemsInQueueKey] = itemInQueueNode;
            }

            return new SaveData(saveKey, json);
        }

        public void SendAddCurrencyEvent(string name, string currentAmountCurrency,
            string addAmountCurrency, Dictionary<string, object> additionalInformation)
        {
            analytics.AddCurrency(name, currentAmountCurrency, addAmountCurrency,
                SQBAModule.GetPurchaserType().ToString(), additionalInformation);
        }

        public void SendSpendCurrencyEvent(string name, string currentAmountCurrency,
            string spendAmountCurrency, Dictionary<string, object> additionalInformation)
        {
            analytics.SpendCurrency(name, currentAmountCurrency, spendAmountCurrency,
                SQBAModule.GetPurchaserType().ToString(), additionalInformation);
        }

        public void SendReplaceCurrencyEvent(string name, string currentAmountCurrency,
            string newAmountCurrency, Dictionary<string, object> additionalInformation)
        {
            analytics.ReplaceCurrency(name, currentAmountCurrency, newAmountCurrency,
                SQBAModule.GetPurchaserType().ToString(), additionalInformation);
        }

        //Вызывается когда активная подписка кончилась(в этом методе мы должны обнулять подписку)
        protected abstract void ResetSubscription();

        //Вызывается при восстановлении подписок(iOS) (в этом списке iap'ы подписок которые востановились)
        protected abstract void OnActiveSubscriptionsCome(List<string> subscriptions);

        //Вызывается когда покупка прошла успешно
        protected abstract void ShopItemPurchased(SQBAShopItem shopItem);

        //Отменили покупку
        protected abstract void ShopItemCanceled(SQBAShopItem shopItem);

        //Ошибка при покупке
        protected abstract void ShopItemFailed(SQBAShopItem shopItem);

        protected virtual void SQBAInitFinish()
        {

        }

        private void OnSqbaPurchasesInitialized(string playerId)
        {
            if (SQBAModule.NoInternetConection) return;

            SQBAModule.SetGlobalValidationObserver(OnSQBAPurchaseSucceded,
                OnSQBAPurhcaseCanceled, OnSQBAPurchaseFailed);

            SQBAModule.GetPurchaseItemsPrices(null);

#if UNITY_ANDROID
            ResetSubscription();
#endif

            SQBAModule.ProcessPendingProducts(ProcessPendingProducts);

#if !UNITY_EDITOR && UNITY_IOS
            SQBAModule.CheckForActiveSubscriptions(playerId,
                OnActiveSubscriptionsCome, OnActiveSubscriptionCheckFailed);
#endif
            SQBAInitFinish();
        }

        protected virtual void OnActiveSubscriptionCheckFailed(long errorCode, string errorMessage)
        {
#if !UNITY_EDITOR && UNITY_IOS
            Logger.LogMessage($"Active subscripton checking failed. Code: {errorCode}; Message: {errorMessage}");
            ResetSubscription();
#endif
        }

        private void OnSQBAPurchaseSucceded(string iapName, FailCode errorCode, string errorMessage)
        {
            Logger.LogMessage($"Succeded global: {iapName}");
            var item = FindShopItemByIapName(iapName);
            shopItemInQueue = null;
            ShopItemPurchased(item);
            BBGC.Features.GetFeature<SaveFeature>().SaveManager.Save();
        }

        private void OnSQBAPurhcaseCanceled(string iapName, FailCode errorCode, string errorMessage)
        {
            Logger.LogMessage($"Canceled global: {iapName}");
            var item = FindShopItemByIapName(iapName);
            shopItemInQueue = null;
            ShopItemCanceled(item);
        }

        private void OnSQBAPurchaseFailed(string iapName, FailCode errorCode, string errorMessage)
        {
            notifications.ShowNotification(new BlackBears.GameCore.Features.Notifications.Notification(
                NotificationType.Warning, localization.Get("BBGC_INIT_ERROR_TITLE")));
            var item = FindShopItemByIapName(iapName);
            Logger.LogMessage($"Failed global: {iapName}; Code: {errorCode}");
            shopItemInQueue = null;
            ShopItemFailed(item);
        }



        private void ProcessPendingProducts(string iapName, FailCode errorCode, string errorMessage)
        {
            if (string.IsNullOrEmpty(iapName)) return;
            foreach (var item in shopItems)
            {
                string itemName = null;
#if UNITY_ANDROID
                itemName = item.PlatformInAppName;
#else
                itemName = item.IapName;
#endif
                if (!iapName.Equals(itemName)) continue;

                SQBAModule.ValidatePurchase(item, (code, error) =>
                {
                    Logger.LogMessage($"Failed to validate pending iap: {error}");
                });
                break;
            }
        }

        public SQBAShopItem FindShopItemByIapName(string iapName)
        {
            if (string.IsNullOrEmpty(iapName)) return null;

            int id = shopItemInQueue != null && shopItemInQueue.iap.Contains(iapName) ? shopItemInQueue.id : -1;

            if (id != -1)
            {
                foreach (var item in shopItems)
                {
                    if (item.id == id && item.IapName.Contains(iapName)) return item;
                }
            }

            foreach (var item in shopItems)
            {
                if (string.Equals(iapName, item.IapName)) return item;
            }

            return null;
        }

        public class ShopItemInQueue
        {
            public readonly int id;
            public readonly string iap;

            public ShopItemInQueue(int id, string iap)
            {
                this.id = id;
                this.iap = iap;
            }
        }


    }

}