﻿using BlackBears.SQBA.Utils;
using BlackBears.SQBA.General;
using BlackBears.SQBA.Modules.Purchasing.RemoteValidation;
using SimpleJSON;
using UnityEngine;
using BlackBears.GameCore.Networking;

namespace BlackBears.SQBA.Networking
{
    public class NetworkBehaviour : MonoBehaviour, INetworkBehaviour
    {
        #region Check
        public void RequestFirstLaunch(Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "firstcheck";
            NetworkRequest request = new SqbaNetworkApiRequest(command, null, onSuccess, onFail);

            StartCoroutine(request.Start());
        }

        public void RequestVersionCheck(JSONNode parameters, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "version/check";
            NetworkRequest request = new SqbaNetworkApiRequest(command, parameters, onSuccess, onFail);

            IState state = Manager.Instance.State;

            request.AddRequestValue(NetworkKeys.kApiKey, state.ApiKey);
            request.AddRequestValue(NetworkKeys.kAppId, state.AppId);
            request.AddRequestValue(NetworkKeys.kDeviceId, state.DeviceId);
            request.AddRequestValue(NetworkKeys.kDeviceModel, state.DeviceModel);
            request.AddRequestValue(NetworkKeys.kAppVersion, state.AppVersion);

            request.AddRequestValue(state.DeviceOS, state.DeviceOSVersion);
            request.AddRequestValue(NetworkKeys.kSQBAModules, state.ActiveModulesString);

            StartCoroutine(request.Start());
        }

        public void SendAdditionalAppInfo(JSONNode parameters, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "appcheck";
            NetworkApiRequest request = new SqbaNetworkApiRequest(command, parameters, onSuccess, onFail);

            IState state = Manager.Instance.State as IState;

            request.AddRequestValue(NetworkKeys.kApiKey, state.ApiKey);
            request.AddRequestValue(NetworkKeys.kAppId, state.AppId);
            request.AddRequestValue(NetworkKeys.kAppName, state.AppName);
            request.AddRequestValue(NetworkKeys.kUrlScheme, state.UrlScheme);

            StartCoroutine(request.Start());
        }
        #endregion

        #region Files
        public void DownloadArchive(string urlString, Block<string> onSuccess, FailBlock onFail)
        {
            NetworkFileRequest request = new SqbaNetworkFileRequest(urlString, onSuccess, onFail);

            string randKey = BlackBears.Utils.Security.GetRandKey();
            request.AddRequestValue(NetworkKeys.kSecurityRand, randKey);

            string filePath = urlString.Substring(urlString.LastIndexOf("/archive/") + 1).Replace("archive/", "");
            string key = Utils.Security.GetServerKey(filePath, randKey, "{1}{2}{0}");
            request.AddRequestValue(NetworkKeys.kSecurityKey, key);

            StartCoroutine(request.Start());
        }
        #endregion

        #region Billing
        public void RequestIOSPurchaseValidation(PurchaseValidationIos validationInfo, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "billing/ios";
            NetworkApiRequest request = PurchaseValidationRequest(command, validationInfo, onSuccess, onFail);
            request.useNetHttpClient = true;

            request.AddRequestValue(NetworkKeys.kProductReceipt, validationInfo.Receipt);
            request.AddRequestValue(NetworkKeys.kProductId, validationInfo.ProductId);
            request.AddRequestValue(NetworkKeys.kTransactionId, validationInfo.TransactionId);

            //О, дааа. Так и должно быть иначе подписки работать не будут
            if (validationInfo.IsRestoring) request.AddRequestValue(NetworkKeys.kNotSave, validationInfo.IsRestoring);

            //facebook
            request.AddRequestValue(NetworkKeys.kFacebookTrackingEnabled, validationInfo.AdvertisingTrackingEnabled);

            StartCoroutine(request.Start());
        }

        public void RequestAndroidPurchaseValidation(PurchaseValidationAndroid validationInfo, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "billing/android";
            NetworkApiRequest request = PurchaseValidationRequest(command, validationInfo, onSuccess, onFail);

            request.AddRequestValue(NetworkKeys.kProductId, validationInfo.ProductId);
            request.AddRequestValue(NetworkKeys.kProductSignature, validationInfo.signature);
            request.AddRequestValue(NetworkKeys.kProductResponseData, validationInfo.responseData);
            request.AddRequestValue(NetworkKeys.kIsSubscription, validationInfo.IsSubscription ? "1" : "0");

            StartCoroutine(request.Start());
        }

        NetworkApiRequest PurchaseValidationRequest(string command, PurchaseValidationInfo info, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            NetworkApiRequest request = new SqbaNetworkApiRequest(command, null, onSuccess, onFail);

            request.AddRequestValue(NetworkKeys.kSecurityRand, info.RandKey);
            request.AddRequestValue(NetworkKeys.kSecurityKey, info.Key);
            request.AddRequestValue(NetworkKeys.kAppId, info.AppId);
            request.AddRequestValue(NetworkKeys.kDeviceId, info.DeviceId);
            request.AddRequestValue(NetworkKeys.kApiKey, info.SQBAKey);
            request.AddRequestValue(NetworkKeys.kProductAmount, info.Amount.ToString());
            request.AddRequestValue(NetworkKeys.kProductCurrency, info.Currency);

            //facebook
            request.AddRequestValue(NetworkKeys.kPlayerIdentifier, info.PlayerId);
            request.AddRequestValue(NetworkKeys.kAdvertisingIdentifier, info.AdvertisingIdentifier);
            request.AddRequestValue(NetworkKeys.kExtInfo, info.ExtInfo);

            return request;
        }

        public void RequestSubscriptionValidation(string playerId, string randKey, bool needReceipt, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "billing/iosSubscriptionCheck";
            NetworkApiRequest request = new SqbaNetworkApiRequest(command, null, onSuccess, onFail);
            request.useNetHttpClient = true;

            IState state = Manager.Instance.State as IState;

            request.AddRequestValue(NetworkKeys.kAppId, state.AppId);
            request.AddRequestValue(NetworkKeys.kApiKey, state.ApiKey);
            request.AddRequestValue(NetworkKeys.kDeviceId, state.DeviceId);
            request.AddRequestValue(NetworkKeys.kPlayerIdentifier, playerId);

            if (needReceipt)
            {
                string bundleReceipt = state.NativeDataAdapter.BundleReciept;
                if (!string.IsNullOrEmpty(bundleReceipt)) request.AddRequestValue(NetworkKeys.kProductReceipt, bundleReceipt);
            }

            string key = Utils.Security.GetServerKey(state.DeviceId, randKey);
            request.AddRequestValue(NetworkKeys.kSecurityKey, key);
            request.AddRequestValue(NetworkKeys.kSecurityRand, randKey);

            StartCoroutine(request.Start());
        }
        #endregion

        #region Codes
        public void RequestRedeemCode(string code, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "code/use";
            NetworkApiRequest request = new SqbaNetworkApiRequest(command, null, onSuccess, onFail);

            IState state = Manager.Instance.State as IState;

            request.AddRequestValue(NetworkKeys.kApiKey, state.ApiKey);
            request.AddRequestValue(NetworkKeys.kAppId, state.AppId);
            request.AddRequestValue(NetworkKeys.kDeviceId, state.DeviceId);
            request.AddRequestValue(NetworkKeys.kBonusCode, code ?? "");

            StartCoroutine(request.Start());
        }
        #endregion

        #region GDPR
        public void RequestPrivacyPolicyAgreement(string playerId, string countryCode, Block<JSONNode> onSuccess, FailBlock onFail)
        {
            string command = "privacy/agreement";
            NetworkApiRequest request = new SqbaNetworkApiRequest(command, null, onSuccess, onFail);

            request.AddRequestValue(NetworkKeys.kCountryCode, countryCode);
            request.AddRequestValue(NetworkKeys.kPlayerIdentifier, playerId);

            IState state = Manager.Instance.State as IState;

            request.AddRequestValue(NetworkKeys.kApiKey, state.ApiKey);
            request.AddRequestValue(NetworkKeys.kAppId, state.AppId);
            request.AddRequestValue(NetworkKeys.kDeviceId, state.DeviceId);

            string randKey = BlackBears.Utils.Security.GetRandKey();
            request.AddRequestValue(NetworkKeys.kSecurityRand, randKey);

            string key = Utils.Security.GetServerKey(state.DeviceId + playerId, randKey);
            request.AddRequestValue(NetworkKeys.kSecurityKey, key);

            StartCoroutine(request.Start());
        }
        #endregion
    }
}
