using BlackBears.SQBA.Modules.Purchasing.RemoteValidation;
using BlackBears.SQBA.Utils;
using SimpleJSON;

namespace BlackBears.SQBA.Networking
{
    public interface INetworkBehaviour
    {
        #region Check
        void RequestFirstLaunch(Block<JSONNode> onSuccess, FailBlock onFail);
        void RequestVersionCheck(JSONNode parameters, Block<JSONNode> onSuccess, FailBlock onFail);
        void SendAdditionalAppInfo(JSONNode parameters, Block<JSONNode> onSuccess, FailBlock onFail);
        #endregion

        #region Files
        void DownloadArchive(string urlString, Block<string> onSuccess, FailBlock onFail);
        #endregion

        #region Billing
        void RequestIOSPurchaseValidation(PurchaseValidationIos validationInfo, Block<JSONNode> onSuccess, FailBlock onFail);
        void RequestAndroidPurchaseValidation(PurchaseValidationAndroid validationInfo, Block<JSONNode> onSuccess, FailBlock onFail);
        void RequestSubscriptionValidation(string playerId, string randKey, bool needReceipt, Block<JSONNode> onSuccess, FailBlock onFail);
        #endregion

        #region Codes
        void RequestRedeemCode(string code, Block<JSONNode> onSuccess, FailBlock onFail);
        #endregion

        #region GDPR
        void RequestPrivacyPolicyAgreement(string playerId, string countryCode, Block<JSONNode> onSuccess, FailBlock onFail);
        #endregion
    }
}
