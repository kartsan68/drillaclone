﻿using UnityEngine;

namespace BlackBears.SQBA.General
{
    public class ApplicationBehaviour : MonoBehaviour
    {
        IApplicationManager applicationManager;

        internal IApplicationManager ApplicationManager
        {
            get { return applicationManager; }
            set { applicationManager = value; }
        }

        void OnApplicationPause(bool pause)
        {
            if (applicationManager == null) return;

            if (pause) applicationManager.OnPauseApplication();
            else applicationManager.OnResumeApplication();
        }

        void OnApplicationQuit() 
        {
            applicationManager.OnPauseApplication();
        }
    }
}
