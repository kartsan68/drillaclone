using System.Collections.Generic;
using BlackBears.GameCore.Features.Analytics;

namespace BlackBears.SQBA
{
    public class ShopCurrency
    {
        private int id;
        private IProtectedVariable value;
        private bool alwaysSendAnalytics;
        private string nameForAnalytics;
        private bool isHard;
        private SQBAShoppingManager manager;

        public ShopCurrency(int id, IProtectedVariable value, bool alwaysSendAnalytics, bool isHard,
            string nameForAnalytics, SQBAShoppingManager manager)
        {
            this.id = id;
            this.value = value;
            this.alwaysSendAnalytics = alwaysSendAnalytics;
            this.isHard = isHard;
            this.nameForAnalytics = nameForAnalytics;
            this.manager = manager;
        }

        public T GetValue<T>()
        {
            return (T)value;
        }

        //Добавить к валюте (в additionalInformationForAnalytics обычно пишется место откуда это произошло(для твердой валюты))
        public T AddValue<T>(T value, Dictionary<string, object> additionalInformationForAnalytics = null) where T : IProtectedVariable
        {
            if (value.IsNegative)
            {
                UnityEngine.Debug.LogWarning("Negative value");
                return default(T);
            }

            var lastValue = this.value;
            this.value = this.value.AddValue(value);

            if (alwaysSendAnalytics)
            {
                manager.SendAddCurrencyEvent(nameForAnalytics, lastValue.ToValueString(), value.ToValueString(),
                    additionalInformationForAnalytics);
            }

            return (T)this.value;
        }

        //Вычесть из валюты (в additionalInformationForAnalytics обычно пишется место откуда это произошло(для твердой валюты))
        public T SpendValue<T>(T value, Dictionary<string, object> additionalInformationForAnalytics = null) where T : IProtectedVariable
        {
            if (value.IsNegative)
            {
                UnityEngine.Debug.LogWarning("Negative value");
                return default(T);
            }

            var lastValue = this.value;
            this.value = this.value.SpendValue(value);

            if (alwaysSendAnalytics)
            {
                manager.SendSpendCurrencyEvent(nameForAnalytics, lastValue.ToValueString(), value.ToValueString(),
                    additionalInformationForAnalytics);
            }

            return (T)this.value;
        }

        //Заменить значение в валюте
        public T ReplaceValue<T>(T newValue, Dictionary<string, object> additionalInformationForAnalytics = null) where T : IProtectedVariable
        {
            var lastValue = this.value;
            this.value = newValue;

            manager.SendReplaceCurrencyEvent(nameForAnalytics, lastValue.ToValueString(), value.ToValueString(),
                    additionalInformationForAnalytics);

            return (T)this.value;
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public string ToValueString()
        {
            return value.ToValueString();
        }
    }
}