//
//  SQBANative.m
//  Unity-iPhone
//
//  Created by Ilya S. on 14/12/2017.
//

#import "SQBANative.h"

#import <sys/sysctl.h>
#import <sys/utsname.h>
#import <UIKit/UIKit.h>
#import <AdSupport/ASIdentifierManager.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#define ARRAY_COUNT(x) sizeof(x) / sizeof(x[0])

static const u_int GIGABYTE = 1024 * 1024 * 1024;  // bytes


// Variables
static NSString *pendingQuery;


@implementation SQBANative
+ (NSString *)getDeviceId { return UIDevice.currentDevice.identifierForVendor.UUIDString; }
+ (NSString *)getDeviceOsVersion { return UIDevice.currentDevice.systemVersion; }
+ (NSString *)getPendingQuery
{
    NSString *query = pendingQuery;
    pendingQuery = nil;
    return query;
}

+ (void)processOpenUrl:(NSURL *)url { pendingQuery = url.query; }

+ (NSString *)getCurrentLocale
{
    return [[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode] lowercaseString];
}

+ (NSString *)getDeviceIFA
{
    if (ASIdentifierManager.sharedManager.isAdvertisingTrackingEnabled)
    {
        return ASIdentifierManager.sharedManager.advertisingIdentifier.UUIDString;
    }
    return @"00000000-0000-0000-0000-000000000000";
}

+ (NSString *)getFacebookExtInfo
{
    NSArray *arr = @[
                     @"i2", // version - starts with 'i' for iOS, we'll use 'a' for Android
                     [self _bundleId] ?: @"",
                     [self _longVersion] ?: @"",
                     [self _shortVersion] ?: @"",
                     [self _sysVersion] ?: @"",
                     [self _machine] ?: @"",
                     [self _language] ?: @"",
                     [self _timeZoneAbbrev] ?: @"",
                     [self _carrierName] ?: @"",
                     [self _width] ?: @"",
                     [self _height] ?: @"",
                     [self _densityString] ?: @"",
                     [self _coreCount] ?: @"",
                     [self _totalDiskSpaceGB] ?: @"",
                     [self _remainingDiskSpaceGB] ?: @"",
                     [self _timeZoneName] ?: @""
                     ];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr options:0 error:&error];
    if (error)
    {
        NSAssert(NO, @"Stop right there criminal scum!");
        return @"";
    }
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

+ (BOOL)getAdvertisingTrackingEnabled
{
    return ASIdentifierManager.sharedManager.isAdvertisingTrackingEnabled;
}

+ (NSString *)getBundleReceipt
{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    return [receiptData base64EncodedStringWithOptions:0];
}


#pragma mark - Internal Facebook Info

+ (NSString *)_bundleId
{
    return NSBundle.mainBundle.bundleIdentifier;
}

+ (NSString *)_longVersion
{
    return [NSBundle.mainBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
}

+ (NSString *)_shortVersion
{
    return [NSBundle.mainBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (NSString *)_sysVersion
{
    return UIDevice.currentDevice.systemVersion;
}

+ (NSString *)_machine
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return @(systemInfo.machine);
}

+ (NSString *)_language
{
    return NSLocale.currentLocale.localeIdentifier;
}

+ (NSString *)_timeZoneAbbrev
{
    return NSTimeZone.systemTimeZone.abbreviation;
}

+ (NSString *)_carrierName
{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    return [carrier carrierName] ?: @"NoCarrier";
}

+ (NSNumber *)_width
{
    return @(UIScreen.mainScreen.bounds.size.width);
}

+ (NSNumber *)_height
{
    return @(UIScreen.mainScreen.bounds.size.height);
}

+ (NSString *)_densityString
{
    return [NSString stringWithFormat:@"%.02f", UIScreen.mainScreen.scale];
}

+ (NSNumber *)_coreCount
{
    int mib[2] = {CTL_HW, HW_AVAILCPU};
    uint value;
    size_t size = sizeof value;
    if (0 != sysctl(mib, ARRAY_COUNT(mib), &value, &size, NULL, 0)) {
        return @(0);
    }
    return @(value);
}

+ (NSNumber *)_totalDiskSpaceGB
{
    NSError *error = nil;
    NSDictionary *attrs = [[[NSFileManager alloc] init] attributesOfFileSystemForPath:NSHomeDirectory() error:&error];
    if (error) return @(0);
    
    float totalDiskSpace = [[attrs objectForKey:NSFileSystemSize] floatValue];
    return @(round(totalDiskSpace / GIGABYTE));
}

+ (NSNumber *)_remainingDiskSpaceGB
{
    NSError *error = nil;
    NSDictionary *attrs = [[[NSFileManager alloc] init] attributesOfFileSystemForPath:NSHomeDirectory() error:nil];
    if (error) return @(0);
    
    float remainingDiskSpace =  [[attrs objectForKey:NSFileSystemFreeSize] floatValue];
    return @(round(remainingDiskSpace / GIGABYTE));
}

+ (NSString *)_timeZoneName
{
    return NSTimeZone.systemTimeZone.name;
}
@end


// Helper method to create C string copy
char* unityString(NSString *string)
{
    const char *ch = string.UTF8String;
    
    if (string == NULL) return NULL;
    
    char* res = (char*)malloc(strlen(ch) + 1);
    strcpy(res, ch);
    return res;
}


// Unity => Obj-C
extern "C"
{
    const char* SQBAGetDeviceId() { return unityString([SQBANative getDeviceId]); }
    const char* SQBAGetDeviceOSVersion() { return unityString([SQBANative getDeviceOsVersion]); }
    const char* SQBAGetPendingQuery() { return unityString([SQBANative getPendingQuery]); }
    const char* SQBAGetCurrentLocale() { return unityString([SQBANative getCurrentLocale]); }
    const char* SQBAGetDeviceIFA() { return unityString([SQBANative getDeviceIFA]); }
    const char* SQBAGetFacebookExtInfo() { return unityString([SQBANative getFacebookExtInfo]); }
    const bool SQBAGetAdvertisingTrackingEnabled() { return [SQBANative getAdvertisingTrackingEnabled]; }
    const char* SQBAGetBundleReceipt() { return unityString([SQBANative getBundleReceipt]); }
}
