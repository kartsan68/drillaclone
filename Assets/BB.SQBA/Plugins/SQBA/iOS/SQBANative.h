//
//  SQBANative.h
//  Unity-iPhone
//
//  Created by Ilya S. on 14/12/2017.
//

#import <Foundation/Foundation.h>

// Basic iOS data and methods required by SQBA
@interface SQBANative : NSObject
+ (NSString *)getDeviceId;
+ (NSString *)getDeviceOsVersion;
+ (NSString *)getPendingQuery;
+ (NSString *)getCurrentLocale;
+ (NSString *)getDeviceIFA;
+ (NSString *)getFacebookExtInfo;
+ (BOOL)getAdvertisingTrackingEnabled;

+ (NSString *)getBundleReceipt;

+ (void)processOpenUrl:(NSURL *)url;
@end

