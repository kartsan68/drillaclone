using BlackBears.SQBA.Modules.Purchasing;
using SimpleJSON;

namespace BlackBears.SQBA
{
	public class SQBAShopItem : PurchaseSQBAItem
	{	
		public readonly int id;
		public readonly double equivalent;
		public readonly string name;

        // public float TaxFreeEquivalent => equivalent * 0.67f;

        public SQBAShopItem(JSONNode node, string playerId) : base(node, new ValidationPayload(playerId))
        {
			id = node["id"];
			equivalent = node["equivalent"].AsDouble;
			name = node["name"].Value;
        }

		public SQBAShopItem(double equivalent, string iapName, ProductType type) : base(iapName, equivalent, type, new ValidationPayload("1"))
		{
			
		}
	}

}
