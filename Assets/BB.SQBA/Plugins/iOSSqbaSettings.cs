﻿using UnityEngine;

namespace BlackBears.SQBA
{

    public class iOSSqbaSettings : ScriptableObject
    {

        public const string settingsPath = "Assets/BB.SQBA/Editor/SqbaSettings.asset";

        [SerializeField] private bool useSqbaAppController = true;

        public bool UseSqbaAppController
        {
            get { return useSqbaAppController; }
            set { useSqbaAppController = value; }
        }

    }

}