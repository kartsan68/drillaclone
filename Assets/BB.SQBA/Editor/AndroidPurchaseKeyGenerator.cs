using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BlackBears.SQBA
{

    public class AndroidPurchaseKeyGenerator : EditorWindow
    {

        private string key;

        [MenuItem("BlackBears/Generate Android Purchase Key")]
        public static void OpenGenerateKeyWindow()
        {
            var window = ScriptableObject.CreateInstance<AndroidPurchaseKeyGenerator>();
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 150);
            window.ShowPopup();
        }

        private static void Generate(string key)
        {
            string firstPart = key.Substring(0, 58);
            string thirdPart = key.Substring(key.Length - 58, 58);
            string secondPart = key.Substring(firstPart.Length, key.Length - firstPart.Length - thirdPart.Length);

            firstPart = IncrementEachLetter(firstPart);
            secondPart = Reverse(DecrementEachLetter(secondPart, 1));
            thirdPart = IncrementEachLetter(thirdPart, 1);

            var templatePath = Application.dataPath + "/BB.SQBA/Editor/KeyGenerator.tmpl";
            var template = File.ReadAllText(templatePath);
            template = template.Replace("{first_part}", firstPart);
            template = template.Replace("{second_part}", secondPart);
            template = template.Replace("{third_part}", thirdPart);

            var outputPath = Application.dataPath + "/BB.SQBA/Plugins/SQBA/generated/KeyGenerator.cs";
            var dirPath = Path.GetDirectoryName(outputPath);
            Directory.CreateDirectory(dirPath);

            if (File.Exists(outputPath)) File.Delete(outputPath);

            File.WriteAllText(outputPath, template);

            UnityEditor.AssetDatabase.Refresh();
        }

        private static string IncrementEachLetter(string s, short count = 1)
        {
            char[] chars = s.ToCharArray();
            for (int i = 0; i < chars.Length; i++) chars[i] += (char)count;
            return new string(chars);
        }

        private static string DecrementEachLetter(string s, short count = 1)
        {
            char[] chars = s.ToCharArray();
            for (int i = 0; i < chars.Length; i++) chars[i] -= (char)count;
            return new string(chars);
        }

        private static string Reverse(string s)
        {
            var chars = s.ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Setup base64 key");
            key = EditorGUILayout.TextField(key);
            if (UnityEngine.GUILayout.Button("Generate"))
            {
                Generate(key);
                this.Close();
            }
            if (UnityEngine.GUILayout.Button("Cancel"))
            {
                this.Close();
            }
        }

    }

}