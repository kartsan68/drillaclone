using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace BlackBears.SQBA
{
    public class SQBAPostprocess
    {

        private const string define = "#define USE_SQBA_APP_CONTROLLER";

        private static iOSSqbaSettings settings;

        [MenuItem("BlackBears/SQBA/OpenSettings")]
        public static void EnableSqbaAppControllerCheck()
        {
            CheckSettings();
            Selection.activeObject = settings;
        }

        [PostProcessBuild(104)]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget != BuildTarget.iOS) return;
            PrepareProject(buildPath);
        }

        private static void PrepareProject(string buildPath)
        {
            CheckSettings();

            string preprocessorPath = buildPath + "/Classes/Preprocessor.h";
            var preprocessor = File.ReadAllText(preprocessorPath);

            preprocessor = preprocessor.Replace(define, string.Empty);

            if (settings.UseSqbaAppController) preprocessor = preprocessor + define;

            File.WriteAllText(preprocessorPath, preprocessor);
        }

        private static void CheckSettings()
        {
            if (settings) return;
            settings = UnityEditor.AssetDatabase.LoadAssetAtPath<iOSSqbaSettings>(iOSSqbaSettings.settingsPath);
            if (settings) return;

            settings = ScriptableObject.CreateInstance<iOSSqbaSettings>();
            UnityEditor.AssetDatabase.CreateAsset(settings, iOSSqbaSettings.settingsPath);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

    }
}