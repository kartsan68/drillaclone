﻿using System;
using BlackBears.GameCore.Features.Localization;
using BlackBears.GameCore.Features.SQBAFiles;
using I2.Loc;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Localization
{

    [CreateAssetMenu]
    public class I2Localizator : Localizator
    {

        [SerializeField] private string localizationFileName;

        private GameObject go;
        private Text text;
        private Localize localize;
        private LocalizationParamsManager paramsManager;

        public override string Get(string key, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;

            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);
            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }

        public override string Get(string key, long count0, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;
            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            paramsManager.SetParameterValue("0", count0.ToString(), false);
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);

            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }
        public override string Get(string key, string count0, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;
            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            paramsManager.SetParameterValue("0", count0, false);
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);

            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }

        public override string Get(string key, long count0, long count1, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;
            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            paramsManager.SetParameterValue("0", count0.ToString(), false);
            paramsManager.SetParameterValue("1", count1.ToString(), false);
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);
            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }

        public override string Get(string key, string count0, string count1, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;
            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            paramsManager.SetParameterValue("0", count0, false);
            paramsManager.SetParameterValue("1", count1, false);
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);

            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }

        public override string Get(string key, long count0, long count1, long count2, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;
            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            paramsManager.SetParameterValue("0", count0.ToString(), false);
            paramsManager.SetParameterValue("1", count1.ToString(), false);
            paramsManager.SetParameterValue("2", count2.ToString(), false);
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);

            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }

        public override string Get(string key, string count0, string count1, string count2, Modificator modificator, string launguage = null)
        {
            TryInitialize();

            var currentLanguage = I2.Loc.LocalizationManager.CurrentLanguage;
            if (launguage != null) localize.SetGlobalLanguage(launguage);

            text.text = null;
            paramsManager.SetParameterValue("0", count0, false);
            paramsManager.SetParameterValue("1", count1, false);
            paramsManager.SetParameterValue("2", count2, false);
            localize.PrimaryTermModifier = CastModificatorToI2(modificator);
            localize.SetTerm(key);

            string value = localize.GetMainTargetsText();
            if (launguage != null) localize.SetGlobalLanguage(currentLanguage);
            return value;
        }


        public void ValidateLocalization(SQBAFilesFeature files)
        {
#if !UNITY_EDITOR
      var language = I2.Loc.LocalizationManager.CurrentLanguage;
            I2.Loc.LocalizationManager.Sources[0].Import_CSV(string.Empty, files.TextByName(localizationFileName),
                eSpreadsheetUpdateMode.Replace, ',');
            LocalizationManager.LocalizeAll();
#endif
        }

        private void TryInitialize()
        {
#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying) return;
#endif

            if (go != null) return;

            go = new GameObject(string.Format("~~{0}", name));
            DontDestroyOnLoad(go);
            text = go.AddComponent<Text>();
            text.font = null;
            text.color = Color.clear;

            localize = go.AddComponent<Localize>();
            paramsManager = go.AddComponent<LocalizationParamsManager>();
        }

        private Localize.TermModification CastModificatorToI2(Modificator modificator)
        {
            switch (modificator)
            {
                case Modificator.DontModify: return Localize.TermModification.DontModify;
                case Modificator.ToUpper: return Localize.TermModification.ToUpper;
                case Modificator.ToLower: return Localize.TermModification.ToLower;
                case Modificator.ToUpperFirst: return Localize.TermModification.ToUpperFirst;
                case Modificator.ToTitle: return Localize.TermModification.ToTitle;
                default: return Localize.TermModification.DontModify;
            }
        }

    }

}