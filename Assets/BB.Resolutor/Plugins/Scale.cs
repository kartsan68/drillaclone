namespace BlackBears.Resolutor
{

    public enum Scale { Undefined = 0, x2 = 2, x3 = 3, x4 = 4, x5 = 5 }

    public static class ScaleUtils
    {

#if UNITY_ANDROID
        public const Scale minScale = Scale.x2;
        public const Scale maxScale = Scale.x5;
#elif UNITY_IOS
        public const Scale minScale = Scale.x2;
        public const Scale maxScale = Scale.x4;
#endif

        public static string FolderName(this Scale scale) { return scale.ToString(); }

    }

}