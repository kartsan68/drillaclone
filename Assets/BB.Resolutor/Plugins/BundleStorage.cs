﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.Bundler;
using UnityEngine;

namespace BlackBears.Resolutor
{

	[CreateAssetMenu(menuName = "BB.Resolutor/Bundle Storage", fileName = "BundleStorage")]
	public class BundleStorage : ScriptableObject 
	{
		
		[SerializeField] private StorageItem[] items;

		internal BundleInfo GetBundleInfoForScale(Scale scale)
		{
			for (int i = 0; i < items.Length; i++)
			{
				if (items[i].Scale == scale) return items[i].Info;
			}
			return items[0].Info;
		}

        [Serializable]
		public class StorageItem
		{

			[SerializeField] private Scale scale;
			[SerializeField] private BundleInfo info;

			public Scale Scale { get { return scale; } }
			public BundleInfo Info { get { return info; } }

		}

	}

}