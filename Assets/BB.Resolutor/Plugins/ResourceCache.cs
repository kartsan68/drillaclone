using System.Collections;
using System.Collections.Generic;
using BlackBears.Bundler;
using BlackBears.Resolutor.Scaling;
using BlackBears.Utils.Scaling;
using BlackBears.Utils.View.SafeArea;
using UnityEngine;

namespace BlackBears.Resolutor
{

    public enum CacheState
    {
        InitializeFailed = -1,
        NotInitiated = 0,
        Initialising,
        Initialized,
    }

    public abstract class ResourceCache : MonoBehaviour
    {

        [SerializeField] private BundleStorage storage;
        [SerializeField] private BundlerManager bundler;

        private CacheState state;
        private IBundleProxy bundle;

        public string ErrorMessage { get; private set; }

        public Scale GraphicScale { get { return Selector.GraphicScale; } }
        public CacheState State
        {
            get { return state; }
            private set
            {
                if (state == value) return;
                state = value;
                if (state == CacheState.Initialized) OnInitializeFinished();
                if (OnCacheStateChanged != null) OnCacheStateChanged(this, new ResourceCacheEventArgs(state));
            }
        }

        public bool Initialized { get { return State == CacheState.Initialized; } }

        protected BundleStorage Storage { get { return storage; } }
        protected BundlerManager Bundler { get { return bundler; } }

        protected abstract IScaleSelector Selector { get; }

        public event ResourceCacheEventHandler OnCacheStateChanged;

        protected virtual void Awake()
        {
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            DontDestroyOnLoad(this);
            #endif
        }

        public void Initialize()
        {
            StartCoroutine(InitializeAsync());   
        }

        protected virtual IEnumerator InitializeAsync() 
        {
           // Если уже инициализируемся/проинициализированы - выходим из метода.
            if ((int)state > (int)CacheState.NotInitiated) yield break;
            State = CacheState.Initialising;

            Selector.DetectScale();
            BundleInfo info = storage.GetBundleInfoForScale(Selector.GraphicScale);
            if (info == null) 
            {
                State = CacheState.InitializeFailed;
                yield break;
            }
            yield return StartCoroutine(LoadBundle(info));
        }

        protected virtual void OnInitializeFinished() { }

        protected abstract IEnumerator LoadResourcesFromBundle(IBundleProxy bundle);

        protected IEnumerator LoadBundle(BundleInfo info)
        {
            var request = bundler.LoadAssetBundleAsync(info);
            yield return request.Load();
            if (request.IsFailed)
            {
                ErrorMessage = $"Request for bundleInfo {info?.Name} is Failed";
                State = CacheState.InitializeFailed;
                yield break;
            }
            bundle = request.ProxyBundle;
            if (bundle == null)
            {
                ErrorMessage = $"Bundle for bundleInfo {info?.Name} is Failed";
                State = CacheState.InitializeFailed;
                yield break;
            }
            yield return LoadResourcesFromBundle(bundle);
            bundle.Unload(false);
            State = CacheState.Initialized;
        }

    }

}