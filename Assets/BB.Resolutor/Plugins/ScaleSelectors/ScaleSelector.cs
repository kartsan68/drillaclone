using UnityEngine;

namespace BlackBears.Resolutor.Scaling
{

    public abstract class ScaleSelector : ScriptableObject
    {
        
        public abstract Scale CurrentScale { get; }

        public virtual void DetectScale() {}

    }

}