using UnityEngine;

namespace BlackBears.Resolutor.Scaling
{

    [CreateAssetMenu(menuName = "BB.Resolutor/Scale/DPI Based", fileName = "DpiScale")]
    public sealed class DpiBasedScaleSelector : ScaleSelector
    {

        [SerializeField] private ScaleSelector reserveSelector;
        [SerializeField] private Vector2 referenceResolution = new Vector2(320, 568);
        [SerializeField] private ScalingRule[] rules;

        private Scale scale;

        public override Scale CurrentScale 
        {
            get
            {
                if (scale == 0) DetectScale();
                return scale;
            }
        }

        public override void DetectScale()
        {
            var screenSize = new Vector2(Screen.width, Screen.height);
            var screenCoefficients = new Vector2(screenSize.x / referenceResolution.x,
                screenSize.y / referenceResolution.y);
            float coefficient = Mathf.Max(screenCoefficients.x, screenCoefficients.y);

            for (int i = 0; i < rules.Length; i++)
            {
                if(!rules[i].Check(coefficient)) continue;
                scale = rules[i].scale;
                return;
            }

            if (reserveSelector != null) scale = reserveSelector.CurrentScale;
            else scale = Scale.x2;
        }

        public enum Rule
        {
            Less =  0x1 << 0, 
            Great = 0x1 << 1, 
            Equal = 0x1 << 2,
            LessOrEqual = Less | Equal,
            GreatOrEqual = Great | Equal
        }

        [System.Serializable]
        public struct ScalingRule
        {

            public float dpiValue;
            public Rule rule;
            public Scale scale;

            public bool Check(float dpi)
            {
                if ((rule & Rule.Less) != 0 && dpi < dpiValue) return true;
                if ((rule & Rule.Great) != 0 && dpi > dpiValue) return true;
                if ((rule & Rule.Equal) != 0 && dpi == dpiValue) return true;
                return false;
            }

        }


    }

}