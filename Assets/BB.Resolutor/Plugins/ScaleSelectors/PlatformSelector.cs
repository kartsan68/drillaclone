using UnityEngine;

namespace BlackBears.Resolutor.Scaling
{

    [CreateAssetMenu(menuName = "BB.Resolutor/Scale/Platform Based", fileName = "PlatformScale")]
    public class PlatformSelector : ScaleSelector
    {

        [SerializeField] private ScaleSelector androidSelector;
        [SerializeField] private ScaleSelector iOSSelector;

        private Scale scale = Scale.Undefined;

        public override Scale CurrentScale
        {
            get
            {
                if (scale == Scale.Undefined) DetectScale();
                return scale;
            }
        }

        public override void DetectScale()
        {
            #if UNITY_ANDROID
            scale = androidSelector.CurrentScale;
            #else
            scale = iOSSelector.CurrentScale;
            #endif
            if (scale == Scale.Undefined) scale = Scale.x2;
        }

    }
}