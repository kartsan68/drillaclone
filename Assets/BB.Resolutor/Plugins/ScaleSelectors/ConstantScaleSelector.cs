using UnityEngine;

namespace BlackBears.Resolutor.Scaling
{

    [CreateAssetMenu(menuName = "BB.Resolutor/Scale/Constant", fileName = "ConstantScale")]
    public sealed class ConstantScaleSelector : ScaleSelector
    {

        [SerializeField] private Scale scale;

        public override Scale CurrentScale { get { return scale; } }

    }

}