using UnityEngine;

namespace BlackBears.Resolutor.Scaling
{

    [CreateAssetMenu(menuName = "BB.Resolutor/Scale/Memory Based", fileName = "MemoryScale")]
    public class MemoryBasedSelector : ScaleSelector
    {

        [SerializeField] private int minMemoryMB = 512;
        [SerializeField] private ScaleSelector lessMemorySelector;
        [SerializeField] private ScaleSelector moreMemorySelector;

        private Scale scale;

        public override Scale CurrentScale
        {
            get
            {
                if (scale == 0) DetectScale();
                return scale;
            }
        }

        public override void DetectScale()
        {
            if(SystemInfo.systemMemorySize <= minMemoryMB) scale = lessMemorySelector.CurrentScale;
            else scale = moreMemorySelector.CurrentScale;
        }

    }
}