﻿using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Resolutor
{

    public sealed class EditableAtlas : Atlas
    {

		private Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();

        public override Sprite this[string name]
        {
            get
			{
				Sprite result;
				sprites.TryGetValue(name, out result);
				return result;
			}
        }

		public void AddSprites(Object[] sprites)
		{
			if (sprites == null) return;
			for (int i = 0; i < sprites.Length; i++) AddSprite(sprites[i] as Sprite);
		}

		public void AddSprites(Sprite[] sprites)
		{
			if (sprites == null) return;
			for (int i = 0; i < sprites.Length; i++) AddSprite(sprites[i]);
		}

		public void AddSprite(Sprite sprite)
		{
			if (sprite == null) return;
			sprites[sprite.name] = sprite;
		}

    }

}