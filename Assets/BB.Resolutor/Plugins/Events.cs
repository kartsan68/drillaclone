namespace BlackBears.Resolutor
{

    public delegate void ResourceCacheEventHandler(object sender, ResourceCacheEventArgs args);

    public class ResourceCacheEventArgs : System.EventArgs
    {
        
        public readonly CacheState state;

        public ResourceCacheEventArgs(CacheState state)
        {
            this.state = state;
        }

    }

}