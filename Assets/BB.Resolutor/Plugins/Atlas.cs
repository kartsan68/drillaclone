﻿using UnityEngine;

namespace BlackBears.Resolutor
{

    public abstract class Atlas
    {

        public abstract Sprite this[string name] { get; }

    }

}