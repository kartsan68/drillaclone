using BlackBears.GameCore.Graphic;
using UnityEngine;

namespace BlackBears.Resolutor
{

    public class GraphicSettings : ScriptableObject
    {
        public bool showSprites;
        [SerializeField] private string[] searchPaths;
        [SerializeField] private string[] keys;

        public string PathByKey(string key)
        {
            int index;
            for (index = 0; index < keys.Length; index++)
            {
                if (string.Equals(key, keys[index])) break;
            }
            if (index >= searchPaths.Length) return null;
            return searchPaths[index];
        }

    }

}