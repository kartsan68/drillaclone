using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

namespace BlackBears.EditorPlugins
{

    public static class iOSPostBuild
    {

        private const string teamId = "84MZR4LK7B";

        [PostProcessBuild(1)]
        public static void PostProcessIOS(BuildTarget target, string pathToBuiltProject)
        {
            if (target != BuildTarget.iOS) return;

            string pbxpath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
            string plistPath = pathToBuiltProject + "/Info.plist";
#if UNITY_IOS
            SetupPBX(pbxpath);
            SetupPlist(plistPath);
#endif
        }

#if UNITY_IOS
        private static void SetupPBX(string pbxpath)
        {
            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxpath);

            var target = pbxProject.TargetGuidByName("Unity-iPhone");
            SetupTeamId(pbxProject, target);
            DisableBitcode(pbxProject, target);

            pbxProject.WriteToFile(pbxpath);
        }

        private static void SetupPlist(string plistPath)
        {
            var plistDoc = new PlistDocument();
            plistDoc.ReadFromFile(plistPath);
            PlistElementDict root = plistDoc.root;
            root.SetBoolean("ITSAppUsesNonExemptEncryption", false);
            plistDoc.WriteToFile(plistPath);
        }

        private static void SetupTeamId(PBXProject pbx, string target) => pbx.SetTeamId(target, teamId);

        private static void DisableBitcode(PBXProject pbx, string target) =>
            pbx.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

#endif

    }

}