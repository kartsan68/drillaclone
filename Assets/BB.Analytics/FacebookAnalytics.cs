#if !BBGC_ANALYTICS_FACEBOOK_DISABLED
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Analytics;
using Facebook.Unity;
using UnityEngine;

namespace BlackBears.Localization
{
	[CreateAssetMenu(fileName = "FacebookAnalytics",menuName = "BB.Analytics/FacebookAnalytics")]
	public class FacebookAnalytics : Analytics
	{
		private IGameAdapter gameAdapter;

		public override void Init()
		{
			if (FB.IsInitialized)
			{
				FB.ActivateApp();
			}
			else
			{
				//Handle FB.Init
				FB.Init(FB.ActivateApp);
			}
		}
		
		public override void SendEvent(string name, Dictionary<string, object> parameters)
		{
			FB.LogAppEvent(name,null,CheckForNull(parameters));
		}

		/// <summary>
		/// Замена null значений в коллекции
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private Dictionary<string, object> CheckForNull(Dictionary<string,object> parameters)
		{
			var notNullDict = new Dictionary<string,object>();

			foreach (KeyValuePair<string, object> entry in parameters)
			{
				var entryValue = entry.Value ?? "null";
				
				notNullDict.Add(entry.Key,entryValue);
			}

			return notNullDict;
		}

		public override void SendEvent(string name)
		{
			FB.LogAppEvent(name);

		}

		public override void AddNumberToUserProfile(string key, double value)
		{
		}

		public override void AddStringToUserProfile(string key, string value)
		{
		}

		public override void AddBoolToUserProfile(string key, bool value)
		{
		}

		public override void AddCounterToUserProfile(string key, double value)
		{
		}

		public override void ReportUserProfile()
		{
		}
		

		public override void SetUserProfileID(string id)
		{
		}

		
		public override void ResumeApp()
		{
			// facebook аналитику нужно активировать каждый раз при разворачивании приложения
			Init();
		}
	}
}
#endif