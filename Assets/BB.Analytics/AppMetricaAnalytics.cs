#if !BBGC_ANALYTICS_APPMETRICA_DISABLED
using System.Collections.Generic;
using BlackBears.GameCore.Features.Analytics;
using UnityEngine;

namespace BlackBears.Localization
{
    [CreateAssetMenu(fileName = "AppMetricaAnalytics",menuName = "BB.Analytics/AppMetricaAnalytics")]
    public class AppMetricaAnalytics : Analytics
    {
        private IYandexAppMetrica metrica;
        private YandexAppMetricaUserProfile userProfile;


        public override void Init()
        {
            userProfile = new YandexAppMetricaUserProfile();
            metrica = AppMetrica.Instance;
        }

        public override void SendEvent(string message, Dictionary<string, object> parameters)
        {
            metrica.ReportEvent(message, parameters);
        }

        public override void SendEvent(string message)
        {
            metrica.ReportEvent(message);
        }

        public override void AddBoolToUserProfile(string key, bool value)
        {
            userProfile.Apply(YandexAppMetricaAttribute.CustomBoolean(key).WithValue(value));
        }

        public override void AddCounterToUserProfile(string key, double value)
        {
            userProfile.Apply(YandexAppMetricaAttribute.CustomCounter(key).WithDelta(value));
        }

        public override void AddNumberToUserProfile(string key, double value)
        {
            userProfile.Apply(YandexAppMetricaAttribute.CustomNumber(key).WithValue(value));
        }

        public override void AddStringToUserProfile(string key, string value)
        {
            userProfile.Apply(YandexAppMetricaAttribute.CustomString(key).WithValue(value));
        }

        public override void ReportUserProfile()
        {
            metrica.ReportUserProfile(userProfile);
        }

        public override void SetUserProfileID(string id)
        {
            metrica.SetUserProfileID(id);
        }

        public override void ResumeApp()
        {
            
        }
    }

}
#endif