#if !BBGC_ANALYTICS_FIREBASE_DISABLED
using System.Collections.Generic;
using BlackBears.GameCore.Features.Analytics;
using Firebase.Analytics;
using UnityEngine;

namespace BlackBears.Localization
{
	[CreateAssetMenu(fileName = "FirebaseAnalytics",menuName = "BB.Analytics/FirebaseAnalytics")]
	public class FirebaseAnalytics : Analytics
	{
		public override void Init()
		{
			// firebase не нужно инициализировать
		}

		public override void SendEvent(string name, Dictionary<string, object> parameters)
		{
			// firebase принимает значения параметров только long, double, string
			var paramList = new List<Parameter>();
			foreach(KeyValuePair<string, object> entry in parameters)
			{
				if (entry.Value is string valueString)
				{
					paramList.Add(new Parameter(entry.Key, valueString));
				}
				
				if (entry.Value is double valueDouble)
				{
					paramList.Add(new Parameter(entry.Key, valueDouble));
				}
				
				if (entry.Value is long valueLong)
				{
					paramList.Add(new Parameter(entry.Key, valueLong));
				}
				
				if (entry.Value == null)
				{
					paramList.Add(new Parameter(entry.Key, "null"));
				}
			}
			Firebase.Analytics.FirebaseAnalytics.LogEvent(name,paramList.ToArray());
		}

		public override void SendEvent(string name)
		{
			Firebase.Analytics.FirebaseAnalytics.LogEvent(name);
		}

		public override void AddNumberToUserProfile(string key, double value)
		{
		}

		public override void AddStringToUserProfile(string key, string value)
		{
		}

		public override void AddBoolToUserProfile(string key, bool value)
		{
		}

		public override void AddCounterToUserProfile(string key, double value)
		{
		}

		public override void ReportUserProfile()
		{
		}

		public override void SetUserProfileID(string id)
		{
			Firebase.Analytics.FirebaseAnalytics.SetUserId(id);
		}

		public override void ResumeApp()
		{
		}
	}
}
#endif