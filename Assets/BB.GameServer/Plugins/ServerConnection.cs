using System;
using System.Collections.Generic;
using BlackBears.GameCore.Features.JellyB;
using UnityEngine;

namespace BlackBears.GameServer
{

    public partial class ServerConnection : IConnectionDataProvider
    {

        private static ServerConnection instance;

        private ConnectionBehaviour behaviour;

        private GameServerConfiguration configuration;
        private GameServerParams parameters;
        private JellyBFeature jellyB;

        string IConnectionDataProvider.Server
        {
            get
            {
                switch (jellyB.Workspace)
                {
                    case Workspace.Standart: return parameters.server;
                    default: return parameters.jellyServer;
                }
            }
        }

        string IConnectionDataProvider.Secret
        {
            get
            {
                switch (jellyB.Workspace)
                {
                    case Workspace.Standart: return configuration.Secret;
                    default: return configuration.JellySecret;
                }
            }
        }

        private ServerConnection(GameServerConfiguration configuration,
            GameServerParams parameters, JellyBFeature jellyB)
        {
            this.configuration = configuration;
            this.parameters = parameters;
            this.jellyB = jellyB;

            var go = new GameObject("~~GameServer");
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityEngine.Object.DontDestroyOnLoad(go);
            #endif
            go.SetActive(false);
            behaviour = go.AddComponent<ConnectionBehaviour>();
            behaviour.dataProvider = this;
            go.SetActive(true);
        }

        public static ServerConnection Init(GameServerConfiguration configuration,
            GameServerParams parameters, JellyBFeature jellyB)
        {
            if (instance == null)
            {
                instance = new ServerConnection(configuration, parameters, jellyB);
                return instance;
            }
            return null;
        }

        public static void Dispose()
        {
            instance = null;
        }

        internal void Inject(PlayersManager playersManager)
        {
            behaviour.playersManager = playersManager;
        }

        public void CreatePlayer(LocalPlayer player, Block<LocalPlayer> onSuccess, FailBlock onFail)
        {
            behaviour.CreatePlayer(player, onSuccess, onFail);
        }

        public void UpdatePlayer(LocalPlayer player, string recalcId, string updateId, Block<UpdatePlayerData> onSuccess, FailBlock onFail)
        {
            behaviour.UpdatePlayer(player, recalcId, updateId, onSuccess, onFail);
        }

        public void GetPlayersBySocialIds(string network, string[] socialIds, Block<List<IPlayer>> onSuccess, FailBlock onFail)
        {
            behaviour.GetPlayersBySocialIds(network, socialIds, onSuccess, onFail);
        }

        public void GetPlayerById(string playerId, Block<IPlayer> onSuccess, FailBlock onFail, string currentUserId = null)
        {
            behaviour.GetPlayerById(playerId, currentUserId, onSuccess, onFail);
        }

        public void GetPlayersByIds(string[] playerIds, Block<List<IPlayer>> onSuccess, FailBlock onFail, string currentUserId = null)
        {
            behaviour.GetPlayersByIds(playerIds, currentUserId, onSuccess, onFail);
        }

    }

}