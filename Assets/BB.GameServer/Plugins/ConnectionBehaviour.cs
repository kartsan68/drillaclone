using System;
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Networking;
using SimpleJSON;
using UnityEngine;

using BlackSec = BlackBears.Utils.Security;

namespace BlackBears.GameServer
{

    internal partial class ConnectionBehaviour : MonoBehaviour
    {

        private const string playerIdKey = "Player[player_id]";
        private const string playerNickKey = "Player[nick]";
        private const string playerGameDataKey = "Player[data]";
        private const string playerNetworkKey = "Player[soc][{0}][network]";
        private const string playerNetIdKey = "Player[soc][{0}][soc_id]";
        private const string playerScoreKey = "Player[score]";
        private const string playerHardCurrencyKey = "Player[crystals]";
        private const string playerIconKey = "Player[icon]";
        private const string playerCountryKey = "Player[country]";

#if UNITY_ANDROID
        private const string playerTokenKey = "Player[and_token]";
#else
        private const string playerTokenKey = "Player[token]";
#endif

        internal IConnectionDataProvider dataProvider;
        internal PlayersManager playersManager;

        internal void CreatePlayer(LocalPlayer player, Block<LocalPlayer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException("onSuccess block is \"null\"");
            if (player == null)
            {
                onFail.SafeInvoke(-1, "Player is 'null'");
                return;
            }

            string url = GenerateUrl("player/create");

            var converter = new LocalPlayerDataConverter(onSuccess, player);

            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            AddKeyToRequest(string.Empty, request);
            AddDefaultKeys(request);
            StartCoroutine(request.Start());
        }

        internal void UpdatePlayer(LocalPlayer player, string recalcId, string updateId, Block<UpdatePlayerData> onSuccess, FailBlock onFail)
        {
            if (player == null)
            {
                onFail.SafeInvoke(-1, "Player is 'null'");
                return;
            }

            string url = GenerateUrl("player/update");

            UpdatePlayerData updatePlayerData = new UpdatePlayerData(player.Score, recalcId);
            var converter = new UpdatePlayerDataConverter(onSuccess, updatePlayerData);

            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            string key = string.Format("{0}{1}{2}", player.Id, player.Score, player.PrivateKey);
            AddKeyToRequest(key, request);
            AddPlayerToRequest(player, request);
            AddDefaultKeys(request);

            request.AddRequestValue("recalc_id", recalcId);
            if(updateId == string.Empty) updateId = System.Guid.NewGuid().ToString();
            request.AddRequestValue("update_id", updateId);

            StartCoroutine(request.Start());
        }

        internal void GetPlayersBySocialIds(string network, string[] socialIds, Block<List<IPlayer>> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException();
            if (socialIds == null || socialIds.Length == 0)
            {
                onSuccess(new List<IPlayer>());
                return;
            }

            string url = GenerateUrl("player/getBySoc");
            var converter = new GamePlayerDataListConverter(playersManager, onSuccess);

            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("network", network);
            var idArray = new JSONArray();
            for (int i = 0; i < socialIds.Length; i++) idArray.Add(socialIds[i]);
            request.AddRequestValue("soc_id", idArray);
            AddDefaultKeys(request);

            StartCoroutine(request.Start());
        }

        internal void GetPlayerById(string playerId, string currentUserId, Block<IPlayer> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException();
            if (playerId == null)
            {
                onSuccess.SafeInvoke(null);
                return;
            }

            GetPlayersByIds(new string[] { playerId }, currentUserId,
                list => onSuccess.SafeInvoke((list != null && list.Count > 0) ? list[0] : null),
                onFail);
        }

        internal void GetPlayersByIds(string[] playerIds, string currentUserId, Block<List<IPlayer>> onSuccess, FailBlock onFail)
        {
            if (onSuccess == null) throw new NullReferenceException();
            if (playerIds == null || playerIds.Length == 0)
            {
                onSuccess(new List<IPlayer>());
                return;
            }

            string url = GenerateUrl("player/getByIds");
            var converter = new GamePlayerDataListConverter(playersManager, onSuccess);

            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            if (currentUserId != null) request.AddRequestValue("curuser_id", currentUserId);

            var idArray = new JSONArray();
            for (int i = 0; i < playerIds.Length; i++) idArray.Add(playerIds[i]);
            request.AddRequestValue("player_ids", idArray);
            AddDefaultKeys(request);

            StartCoroutine(request.Start());
        }

        private string GenerateUrl(string command)
        {
            return string.Format("{0}/{1}", dataProvider.Server, command);
        }

        private void AddKeyToRequest(string keyParam, NetworkApiRequest request)
        {
            string randKey = BlackSec.GetRandKey();
            string fullKey = string.Format("{0}{1}{2}", randKey, keyParam, dataProvider.Secret);
            string key = BlackSec.GetMD5Hash(fullKey, BlackSec.MD5Format.lower);

            request.AddRequestValue("rand_key", randKey);
            request.AddRequestValue("key", key);
        }

        private void AddPlayerToRequest(LocalPlayer player, NetworkRequest request)
        {
            if (player == null) return;
            if (player.Id != null) request.AddRequestValue(playerIdKey, player.Id);
            if (player.Nick != null) request.AddRequestValue(playerNickKey, player.Nick);
            if (player.Token != null) request.AddRequestValue(playerTokenKey, player.Token);
            if (player.Icon != null) request.AddRequestValue(playerIconKey, player.Icon);
            if (player.Country != null) request.AddRequestValue(playerCountryKey, player.Country);
            request.AddRequestValue(playerHardCurrencyKey, player.HardCurrency);
            request.AddRequestValue(playerScoreKey, player.Score);
            
            var gameData = playersManager.GenerateGameData(player);
            if (gameData != null) request.AddRequestValue(playerGameDataKey, gameData);

            var networks = player.Networks;
            int networkIndex = 0;
            foreach (var network in networks)
            {
                if (network == null) continue;

                request.AddRequestValue(string.Format(playerNetworkKey, networkIndex), network.Network);
                request.AddRequestValue(string.Format(playerNetIdKey, networkIndex), network.Id);
                networkIndex += 1;
            }

        }

        private void AddDefaultKeys(NetworkApiRequest request)
        {
            request.AddRequestValue("os", CommonKeys.osName);
        }

    }

}