using System;
using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.GameServer
{

    public abstract class PlayersManager
    {

        private const string playerIdInKey = "player_id";
        private const string iconKey = "icon";
        private const string nickKey = "nick";
        private const string countryKey = "country";
        private const string leagueInKey = "league";
        private const string registerTimeKey = "time";
        private const string gameDataInKey = "data";
        private const string scoreKey = "score";
        

        private const string gameWeeklyScoreKey = "core_weekly_score";
        private const string subscriptionIdKey = "subscription_id";

        public ServerPlayer CreateServerPlayer(JSONNode data)
        {
            var instance = CreateInstance();

            UpdateDataInServerPlayer(instance, data);
            UpdateGameDataInServerPlayer(instance, data[gameDataInKey]);

            return instance;
        }

        public void UpdatePlayer(IPlayer player, JSONNode data)
        {
            if (data == null || !CanUpdatePlayer(player)) return;

            var serverPlayer = (ServerPlayer)player;
            UpdateDataInServerPlayer(serverPlayer, data);
            UpdateGameDataInServerPlayer(serverPlayer, data[gameDataInKey]);
        }

        protected virtual bool CanUpdateLocalPlayer(LocalPlayer player) { return player != null; }
        protected virtual bool CanUpdatePlayer(IPlayer player) { return player != null && player is ServerPlayer; }

        protected virtual void UpdateDataInServerPlayer(ServerPlayer player, JSONNode data)
        {
            player.Id = data[playerIdInKey].GetStringOrDefault(player.Id);
            player.Nick = data[nickKey].GetStringOrDefault(player.Nick);
            player.Country = data[countryKey].GetStringOrDefault(player.Country);
            player.Icon = data[iconKey].GetStringOrDefault(player.Icon);
            player.League = data[leagueInKey].GetLongOrDefault(player.League);
            player.RegisterDate = data[registerTimeKey].GetLongOrDefault(player.RegisterDate);
            player.Score = data[scoreKey].GetDoubleOrDefault(player.Score);
        }

        protected virtual void UpdateGameDataInServerPlayer(ServerPlayer player, JSONNode gameData)
        {
            player.WeeklyScore = gameData[gameWeeklyScoreKey].GetDoubleOrDefault(player.WeeklyScore);
            player.SubscriptionId = gameData[subscriptionIdKey].GetStringOrDefault(player.SubscriptionId);
        }

        protected abstract ServerPlayer CreateInstance();

        public virtual JSONNode GenerateGameData(LocalPlayer player)
        {
            JSONNode gameData = new JSONObject();
            gameData[gameWeeklyScoreKey] = player.WeeklyScore;
            if (player.SubscriptionId != null) gameData[subscriptionIdKey] = player.SubscriptionId;
            return gameData;
        }

    }

}