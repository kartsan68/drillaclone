using System;
using System.Collections.Generic;
using BlackBears.Utils;
using SimpleJSON;

namespace BlackBears.GameServer
{

    public abstract class LocalPlayer : IPlayer
    {

        private const string idKey = "lp_pid";
        private const string scoreKey = "lp_score";
        private const string nickKey = "lp_nick";
        private const string countryKey = "lp_country";
        private const string iconKey = "lp_icon";
        private const string privkKey = "lp_privk";
        private const string leagueKey = "lp_league";
        private const string leaguePlaceKey = "lp_league_place";
        private const string registerDateKey = "lp_register";
        private const string networksKey = "lp_networks";

        private const string firstDayTimestampKey = "lp_first_day_ts";
        private const string lastDayTimestampKey = "lp_last_day_ts";
        private const string daysScoreKey = "lp_days_score";

        private PDouble _score;
        private PLong _league;
        private PInt _leaguePlace;
        private PLong _registerDate;

        private PLong firstDayTimestamp = -1;
        private PLong lastDayTimestamp = -1;
        private int daysCount = -1;
        private PDouble[] daysScore = new PDouble[7];

        protected List<SocialNetworkData> networks = new List<SocialNetworkData>();

        protected LocalPlayer() { }

        protected LocalPlayer(LocalPlayer other)
        {
            if (other == null) return;

            this.Id = other.Id;
            this.Nick = other.Nick;
            this.Icon = other.Icon;
            this.Token = other.Token;
            this.Score = other.Score;
            this.PrivateKey = other.PrivateKey;
            this.League = other.League;
            this.LeaguePlace = other.LeaguePlace;

            networks.AddRange(other.Networks);
        }

        protected LocalPlayer(JSONNode save)
        {
            if (save == null) return;
            FromJson(save);
        }

        public string Id { get; protected set; }
        public string Nick { get; protected set; }
        public string Country { get; protected set; }
        public string Icon { get; protected set; }
        public string Token { get; protected set; }
        public double Score
        {
            get { return _score; }
            protected set { _score = value; }
        }
        public double HardCurrency { get; protected set; }
        public string PrivateKey { get; protected set; }
        public long League
        {
            get { return _league; }
            protected set { _league = value; }
        }
        public int LeaguePlace
        {
            get { return _leaguePlace; }
            protected set { _leaguePlace = value; }
        }
        public long RegisterDate
        {
            get { return _registerDate; }
            protected set { _registerDate = value; }
        }

        public string SubscriptionId { get; protected set; }
        public double WeeklyScore { get { return daysCount > 0 ? (double)(daysScore[daysCount] - daysScore[0]) : 0.0; } }

        public IEnumerable<SocialNetworkData> Networks { get { return networks; } }

        public void UpdateWeeklyScore(long currentTimestamp)
        {
            var dayStartTimestamp = DateUtils.GetDayStartTimestamp(currentTimestamp);
            if (firstDayTimestamp < 0)
            {
                ResetWeeklyScore(dayStartTimestamp);
                return;
            }

            if (lastDayTimestamp == dayStartTimestamp)
            {
                daysScore[daysCount] = Score;
                return;
            }

            int daysPassed = DateUtils.GetDaysCount(lastDayTimestamp, dayStartTimestamp);
            int newDaysCount = daysCount + daysPassed;

            if (newDaysCount >= daysScore.Length)
            {
                int offset = newDaysCount - daysScore.Length + 1;
                if (offset > daysCount || offset >= daysScore.Length)
                {
                    ResetWeeklyScore(dayStartTimestamp);
                    return;
                }
                else
                {
                    for (int i = offset; i <= daysCount; i++)
                    {
                        daysScore[i - offset] = daysScore[i];
                    }
                    daysCount -= offset;
                    newDaysCount -= offset;
                    firstDayTimestamp = DateUtils.AddDays(firstDayTimestamp, offset);
                }
            }

            for (int i = daysCount + 1; i < newDaysCount; i++)
            {
                daysScore[i] = daysScore[daysCount];
            }
            daysScore[newDaysCount] = Score;
            daysCount = newDaysCount;
            lastDayTimestamp = dayStartTimestamp;
        }

        internal void ParseServerData(JSONNode data)
        {
            Id = data["player_id"].Value;
            PrivateKey = data["privk"].Value;
        }

        protected virtual void FromJson(JSONNode json)
        {
            Id = json[idKey].GetStringOrDefault(Id);
            _score = json[scoreKey].GetPDoubleOrDefault(_score);
            Nick = json[nickKey].GetStringOrDefault(Nick);
            Country = json[countryKey].GetStringOrDefault(Country);
            Icon = json[iconKey].GetStringOrDefault(Icon);
            PrivateKey = json[privkKey].GetStringOrDefault(PrivateKey);
            _league = json[leagueKey].GetPLongOrDefault(_league);
            _leaguePlace = json[leaguePlaceKey].GetPIntOrDefault(_leaguePlace);
            _registerDate = json[registerDateKey].GetPLongOrDefault(_registerDate);

            var networksArray = json[networksKey].AsArray;
            for (int i = 0; i < networksArray.Count; i++)
            {
                networks.Add(new SocialNetworkData(networksArray[i]));
            }

            firstDayTimestamp = json[firstDayTimestampKey].GetPLongOrDefault(firstDayTimestamp);
            lastDayTimestamp = json[lastDayTimestampKey].GetPLongOrDefault(lastDayTimestamp);
            daysCount = DateUtils.GetDaysCount(firstDayTimestamp, lastDayTimestamp);

            var daysScoreArray = json[daysScoreKey].AsArray;
            for (int i = 0; i < daysScoreArray.Count && i < daysScore.Length; i++)
            {
                daysScore[i] = PDouble.Parse(daysScoreArray[i].Value);
            }
        }

        public virtual JSONNode ToJson()
        {
            var json = new JSONObject();
            json[idKey] = Id ?? string.Empty;
            json[scoreKey] = _score.ToString();
            json[nickKey] = Nick ?? string.Empty;
            json[countryKey] = Country ?? string.Empty;
            json[iconKey] = Icon ?? string.Empty;
            json[privkKey] = PrivateKey ?? string.Empty;
            json[leagueKey] = _league.ToString();
            json[leaguePlaceKey] = _leaguePlace.ToString();
            json[registerDateKey] = _registerDate.ToString();

            var networksArray = new JSONArray();
            json[networksKey] = networksArray;
            foreach (var network in networks) networksArray.Add(network.ToJson());

            json[firstDayTimestampKey] = firstDayTimestamp.ToString();
            json[lastDayTimestampKey] = lastDayTimestamp.ToString();

            var daysScoreArray = new JSONArray();
            json[daysScoreKey] = daysScoreArray;
            foreach (var dayScore in daysScore) daysScoreArray.Add(dayScore.ToString());

            return json;
        }

        private void ResetWeeklyScore(long timestamp)
        {
            firstDayTimestamp = timestamp;
            lastDayTimestamp = timestamp;
            daysCount = 0;
            daysScore[0] = Score;
            for (int i = 1; i < daysScore.Length; i++) daysScore[i] = 0;
        }

    }

}