using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.GameServer
{

    public class ServerPlayer : IPlayer
    {

        public string Id { get; set; }
        public string Nick { get; set; }
        public string Country { get; set; }
        public string Icon { get; set; }
        public double Score { get; set; }
        public long League { get; set; }
        public int LeaguePlace { get; set; }
        public long RegisterDate { get; set; }

        public double WeeklyScore { get; set; }
        public string SubscriptionId { get; set; }

    }

}