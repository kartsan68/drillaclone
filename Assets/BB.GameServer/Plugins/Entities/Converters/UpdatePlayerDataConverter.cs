using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.GameServer
{

    internal class UpdatePlayerDataConverter : DataConverter<UpdatePlayerData>
    {

        public UpdatePlayerDataConverter(Block<UpdatePlayerData> onConvert, UpdatePlayerData instance = null)
            : base(onConvert, null, instance)
        {

        }

        protected override bool Convert(ref UpdatePlayerData instance, JSONNode data)
        {
            if(instance == null) instance = new UpdatePlayerData();
            instance.RecalcId = data["recalc_id"].GetStringOrDefault(string.Empty);
            instance.Score = data["score"].AsDouble;

            return true;
        }

    }

}