using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.GameServer
{

    internal class LocalPlayerDataConverter : DataConverter<LocalPlayer>
    {

        internal LocalPlayerDataConverter(Block<LocalPlayer> onConvert, LocalPlayer instance) : base(onConvert, null, instance) {}

        protected override bool Convert(ref LocalPlayer instance, JSONNode data)
        {
            instance.ParseServerData(data);
            return true;
        }

    }

}