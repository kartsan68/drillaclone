using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.GameServer
{

    internal class ServerPlayerDataConverter : DataConverter<IPlayer>
    {

        private readonly PlayersManager manager;

        public ServerPlayerDataConverter(PlayersManager manager, Block<IPlayer> onConvert, IPlayer instance = null)
            : base(onConvert, null, instance)
        {
            this.manager = manager;
        }

        protected override bool Convert(ref IPlayer instance, JSONNode data)
        {
            if (instance == null) manager.CreateServerPlayer(data);
            else manager.UpdatePlayer(instance, data);

            return true;
        }

    }

}