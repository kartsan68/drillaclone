using System.Collections.Generic;
using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.GameServer
{

    internal class GamePlayerDataListConverter : DataConverter<List<IPlayer>>
    {

        private PlayersManager playersManager;

        public GamePlayerDataListConverter(PlayersManager playersManager, Block<List<IPlayer>> onConvert)
            : base(onConvert, null)
        { 
            this.playersManager = playersManager;
        }

        protected override bool Convert(ref List<IPlayer> instance, JSONNode data)
        {
            if (instance == null) instance = new List<IPlayer>();

            var playersData = data["players"];
            for (int i = 0; i < playersData.Count; i++)
            {
                instance.Add(playersManager.CreateServerPlayer(playersData[i]));
            }
            return true;
        }

    }

}