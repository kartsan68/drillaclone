using System;
using SimpleJSON;

namespace BlackBears.GameServer
{

    public class SocialNetworkData
    {

        public const string fbNetwork = "fb";
        public const string vkNetwork = "vk";

        private const string networkKey = "network";
        private const string idKey = "soc_id";

        public SocialNetworkData(string network, string id) 
        {
            Network = network;
            Id = id;
        }

        public SocialNetworkData(JSONNode node)
        {
            Network = node["network"].Value;
            Id = node["soc_id"].Value;
        }

        public string Network { get; set; }
        public string Id { get; set; }

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[networkKey] = Network;
            json[idKey] = Id;
            return json;
        }

    }

}