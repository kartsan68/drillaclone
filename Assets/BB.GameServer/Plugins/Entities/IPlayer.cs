using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.GameServer
{

    public interface IPlayer
    {

        string Id { get; }
        string Nick { get; }
        string Country { get; }
        string Icon { get; }
        double Score { get; }
        long League { get; }
        int LeaguePlace { get; }
        long RegisterDate { get; }

        double WeeklyScore { get; }
        string SubscriptionId { get; }


    }

}