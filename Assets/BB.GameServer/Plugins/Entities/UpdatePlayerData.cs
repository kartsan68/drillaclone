namespace BlackBears.GameServer
{
    public class UpdatePlayerData
    {
        public double Score;
        public string RecalcId;

        public UpdatePlayerData()
        {
            Score = 0;
            RecalcId = string.Empty;
        }

        public UpdatePlayerData(double Score, string RecalcId)
        {
            this.Score = Score;
            this.RecalcId = RecalcId;
        }
    }
}