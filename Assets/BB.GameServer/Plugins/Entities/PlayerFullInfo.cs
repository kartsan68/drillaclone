using System.Collections.Generic;

namespace BlackBears.GameServer
{

    public class PlayerFullInfo
    {
        public readonly IPlayer player;
        public readonly List<IPlayer> leaguePlayers;

        public PlayerFullInfo(IPlayer player)
        {
            this.player = player;
            this.leaguePlayers = new List<IPlayer>();
        }
    }

}