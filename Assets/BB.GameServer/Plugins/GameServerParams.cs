using SimpleJSON;

namespace BlackBears.GameServer
{

    public class GameServerParams
    {

        internal readonly string server;
        internal readonly string jellyServer;

        public GameServerParams(JSONNode json)
        {
            server = json["server"].Value;
            jellyServer = json["jelly_server"].Value;
        }

    }

}