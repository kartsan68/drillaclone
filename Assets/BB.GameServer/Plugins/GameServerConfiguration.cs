using UnityEngine;

namespace BlackBears.GameServer
{

    [CreateAssetMenu(fileName = "GameServer configuration", menuName = "BlackBears/GameServer configuration", order = 4)]
    public class GameServerConfiguration : ScriptableObject
    {

        [SerializeField] private bool logEnabled;
        [SerializeField] private string secret;
        [SerializeField] private string jellySecret;

        public bool LogEnabled { get { return logEnabled; } set { logEnabled = value; } }

        internal string Secret => secret;
        internal string JellySecret => jellySecret;

    }

}