namespace BlackBears.GameServer
{

    internal interface IConnectionDataProvider
    {

        string Server { get; }
        string Secret { get; }

    }

}