namespace BlackBears.Ads
{
    [System.Serializable]
    public class AdsSettings
    {        
        [UnityEngine.SerializeField] private string applovinSdkKey;

        internal string ApplovinKey { get { return applovinSdkKey; } }
    }
}