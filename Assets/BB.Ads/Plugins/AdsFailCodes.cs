namespace BlackBears.Ads
{
    public enum AdsFailCodes
    {
        NoAds = -2,
        NoDistribution = -1,
        Undefined = 0,
        AdsNetError = 1
    }
}