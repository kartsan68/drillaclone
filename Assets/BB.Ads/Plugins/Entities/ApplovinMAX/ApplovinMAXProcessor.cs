#if !BBGC_ADS_APPLOVIN_MAX_DISABLED
using UnityEngine;

namespace BlackBears.Ads.ApplovinMAX
{
	public class ApplovinMaxProcessor : MonoBehaviour
	{
		private const int MaxLoadTryCount = 5;

		private int loadRewardedAdsTryCount;
		private int loadInterstitialTryCount;
		internal bool IsRewardedReady => MaxSdk.IsRewardedAdReady(maxRewardedKey);
		internal bool IsInterstitialReady => MaxSdk.IsInterstitialReady(maxInterstitialKey);

		private Block rewardedClickHandler;
		private Block rewardedSuccessHandler;
		private FailBlock rewardedFailHandler;
		private Block rewardedSkipHandler;
		private Block interstitialSuccessHandler;
		private FailBlock interstitialFailHandler;
		private string maxSdkKey;
		private string maxRewardedKey;
		private string maxInterstitialKey;
		private bool isReward;

		public void Init(string sdkKey,string rewardedKey,string interstitialKey)
		{
			maxSdkKey = sdkKey;
			maxRewardedKey = rewardedKey;
			maxInterstitialKey = interstitialKey;
			//mediationDebuggerButton.onClick.AddListener(MaxSdk.ShowMediationDebugger);

			MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
			{
				// AppLovin SDK is initialized, configure and start loading ads
				Debug.Log("MAX SDK Initialized");

				if(!string.IsNullOrEmpty(maxInterstitialKey)) InitializeInterstitialAds();
				InitializeRewardedAds();
			};

			MaxSdk.SetSdkKey(maxSdkKey);
			MaxSdk.InitializeSdk();
		}

		#region Interstitial Ad Methods

		private void InitializeInterstitialAds()
		{
			// Attach callbacks
			MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
			MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
			MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
			MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;

			// Load the first interstitial
			LoadInterstitial();
		}

		void LoadInterstitial()
		{
			if(loadInterstitialTryCount > MaxLoadTryCount) return;
			
			MaxSdk.LoadInterstitial(maxInterstitialKey);
		}

		public void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
		{
			if(string.IsNullOrEmpty(maxInterstitialKey))
			{
				onFail.SafeInvoke(); 
				return;
			}
			interstitialSuccessHandler = onSuccess;
			interstitialFailHandler = onFail;
			if (IsInterstitialReady)
			{
				MaxSdk.ShowInterstitial(maxInterstitialKey);
			}
		}

		private void OnInterstitialLoadedEvent(string adUnitId)
		{
			loadInterstitialTryCount = 0;
			// Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
			Debug.Log("Interstitial loaded");
		}

		private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
		{
			loadInterstitialTryCount++;
			// Interstitial ad failed to load. We recommend re-trying in 3 seconds.
			Debug.Log("Interstitial failed to load with error code: " + errorCode);
			Invoke(nameof(LoadInterstitial), 3);
			interstitialFailHandler.SafeInvoke(errorCode,"Error display interstitial");
			interstitialSuccessHandler.SafeInvoke();
		}

		private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
		{
			// Interstitial ad failed to display. We recommend loading the next ad
			Debug.Log("Interstitial failed to display with error code: " + errorCode);
			LoadInterstitial();
			interstitialFailHandler.SafeInvoke(errorCode,"Error display interstitial");
			interstitialSuccessHandler.SafeInvoke();
		}

		private void OnInterstitialDismissedEvent(string adUnitId)
		{
			// Interstitial ad is hidden. Pre-load the next ad
			Debug.Log("Interstitial dismissed");
			LoadInterstitial();
			interstitialSuccessHandler.SafeInvoke();
		}

		#endregion

		#region Rewarded Ad Methods

		private void InitializeRewardedAds()
		{
			// Attach callbacks
			MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
			MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
			MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
			MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
			MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
			MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
			MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

			// Load the first RewardedAd
			LoadRewardedAd();
		}

		private void LoadRewardedAd()
		{
			if(loadRewardedAdsTryCount > MaxLoadTryCount) return;
			
			MaxSdk.LoadRewardedAd(maxRewardedKey);
		}

		public void ShowRewardedAd(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
		{
			rewardedClickHandler = onClick;
			rewardedSuccessHandler = onSuccess;
			rewardedFailHandler = onFail;
			rewardedSkipHandler = onSkip;
			if (IsRewardedReady)
			{
				MaxSdk.ShowRewardedAd(maxRewardedKey);
			}
			else
			{
				onSkip.SafeInvoke();
			}
		}


		private void OnRewardedAdLoadedEvent(string adUnitId)
		{
			// Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
			Debug.Log("Rewarded ad loaded");
			loadRewardedAdsTryCount = 0;
		}

		private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
		{
			// Rewarded ad failed to load. We recommend re-trying in 3 seconds.
			Debug.Log("Rewarded ad failed to load with error code: " + errorCode);
			Invoke(nameof(LoadRewardedAd), 3);
			rewardedFailHandler.SafeInvoke(errorCode,"Fail event rewarded");
			loadRewardedAdsTryCount++;

		}

		private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
		{
			// Rewarded ad failed to display. We recommend loading the next ad
			Debug.Log("Rewarded ad failed to display with error code: " + errorCode);
			LoadRewardedAd();
			rewardedFailHandler.SafeInvoke(errorCode,"Fail display event rewarded");
		}

		private void OnRewardedAdDisplayedEvent(string adUnitId)
		{
			Debug.Log("Rewarded ad displayed");
		}

		private void OnRewardedAdClickedEvent(string adUnitId)
		{
			Debug.Log("Rewarded ad clicked");
			rewardedClickHandler.SafeInvoke();
		}

		private void OnRewardedAdDismissedEvent(string adUnitId)
		{
			// Rewarded ad is hidden. Pre-load the next ad
			Debug.Log("Rewarded ad dismissed");
			LoadRewardedAd();
			if(!isReward)
				rewardedSkipHandler.SafeInvoke();
			isReward = false;
		}

		private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
		{
			// Rewarded ad was displayed and user should receive the reward
			Debug.Log("Rewarded ad received reward");
			isReward = true;
			rewardedSuccessHandler.SafeInvoke();
		}

		#endregion
	}
}
#endif