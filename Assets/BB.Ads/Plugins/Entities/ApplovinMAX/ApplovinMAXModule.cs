using SimpleJSON;
using UnityEngine;

namespace BlackBears.Ads.ApplovinMAX
{
	internal class ApplovinMAXModule : AdsNetModule
	{
#if !BBGC_ADS_APPLOVIN_MAX_DISABLED
		private ApplovinMaxProcessor processor;
#endif
		private readonly string sdkKey;
		private readonly string rewardedId;
		private readonly string interstitialId;

		public ApplovinMAXModule(JSONNode node, string name) : base(node, name)
		{
			sdkKey = node["sdk_key"].Value;
#if UNITY_ANDROID
			rewardedId = node["rewarded_placement_id_android"].Value;
			interstitialId = node["interstitial_placement_id_android"].Value;
#else
			rewardedId = node["rewarded_placement_id_ios"].Value;
			interstitialId = node["interstitial_placement_id_ios"].Value;
#endif
		}

#if !BBGC_ADS_APPLOVIN_MAX_DISABLED

		internal override bool IsRewardedAvailable => processor.IsRewardedReady;
		internal override bool IsInterstitialAvailable => processor.IsInterstitialReady;
#else
		internal override bool IsRewardedAvailable => false;
		internal override bool IsInterstitialAvailable => false;
#endif

		internal override void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
		{
#if !BBGC_ADS_APPLOVIN_MAX_DISABLED
			processor.ShowRewardedAd(onSuccess, onSkip, onClick, onFail);
#else
			onSuccess.SafeInvoke();
#endif
		}

		internal override void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
		{
#if !BBGC_ADS_APPLOVIN_MAX_DISABLED

			processor.ShowInterstitial(onSuccess, onClick, onFail);
#else
			onSuccess.SafeInvoke();
#endif
		}

		protected override void ProcessActivate()
		{
#if !BBGC_ADS_APPLOVIN_MAX_DISABLED
			var go = new GameObject("~~ApplovinMAX");
			Object.DontDestroyOnLoad(go);
			processor = go.AddComponent<ApplovinMaxProcessor>();
			processor.Init(sdkKey, rewardedId, interstitialId);
#endif
		}
	}
}