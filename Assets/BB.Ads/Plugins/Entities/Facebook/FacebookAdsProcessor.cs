#if !BBGC_ADS_FACEBOOK_DISABLED

using UnityEngine;
using AudienceNetwork;
using AudienceNetwork.Utility;

namespace BlackBears.Ads.FacebookAds
{
	internal class FacebookAdsProcessor : MonoBehaviour
	{
		private const int maxLoadTryCount = 3;

		private int loadRewardedAdsTryCount;
		private int loadInterstitialdsTryCount;

		private InterstitialAd interstitialAd;
		private RewardedVideoAd rewardedVideoAd;

		private bool isInterstitialLoaded;
		private bool isRewardedLoaded;

		private Block adsShowedBlock;
		private Block adsSkipBlock;
		private Block adsClickBlock;
		private bool isReward;
		private string rewardedPlacementId;
		private string interstitoalPlacementId;

		public bool IsInterstitialLoaded => isInterstitialLoaded;
		public bool IsRewardedLoaded => isRewardedLoaded;

		public void Activate(string rewardedPlacementId, string interstitoalPlacementId)
		{
			this.rewardedPlacementId = rewardedPlacementId;

			this.interstitoalPlacementId = interstitoalPlacementId;
			LoadRewarded();

			LoadInterstitial();

			Debug.Log("AudienceNetwork ACTIVATED");
		}

		public void ShowInterstitial(Block adsShowedBlock, Block adsClickBlock)
		{
			if (!isInterstitialLoaded) return;

			interstitialAd.Show();

			isInterstitialLoaded = false;
			this.adsShowedBlock = adsShowedBlock;
			this.adsClickBlock = adsClickBlock;
		}

		public void ShowRewardedVideo(Block adsShowedBlock, Block adsClickBlock, Block adsSkipBlock)
		{
			if (!isRewardedLoaded) return;

			rewardedVideoAd.Show();

			isRewardedLoaded = false;
			this.adsShowedBlock = adsShowedBlock;
			this.adsClickBlock = adsClickBlock;
			this.adsSkipBlock = adsSkipBlock;
		}

		private void LoadInterstitial()
		{
			if (loadInterstitialdsTryCount >= maxLoadTryCount) return;
			
			
			interstitialAd = new InterstitialAd(interstitoalPlacementId);

			interstitialAd.Register(gameObject);


			interstitialAd.InterstitialAdDidLoad = () =>
			{
				Debug.Log("AudienceNetwork InterstitialAdDidLoad");
				loadInterstitialdsTryCount = 0;
				isInterstitialLoaded = true;
			};
			interstitialAd.InterstitialAdDidFailWithError = (string error) =>
			{
				Debug.Log("AudienceNetwork InterstitialAdDidFailWithError " + error);
				loadInterstitialdsTryCount += 1;
				isInterstitialLoaded = false;
				LoadInterstitial();
			};
			interstitialAd.InterstitialAdDidClick = () =>
			{
				Debug.Log("AudienceNetwork InterstitialAdDidClick");
				adsClickBlock.SafeInvoke();
			};
			interstitialAd.InterstitialAdDidClose = () =>
			{
				Debug.Log("AudienceNetwork InterstitialAdDidClose");
				isInterstitialLoaded = false;
				adsShowedBlock.SafeInvoke();
				adsShowedBlock = null;
				adsClickBlock = null;
				if (interstitialAd != null) {
					interstitialAd.Dispose();
				}
				LoadInterstitial();
			};
			
			interstitialAd.LoadAd();
			Debug.Log("AudienceNetwork interstitialAd.LoadAd");
		}

		private void LoadRewarded()
		{
			if (loadRewardedAdsTryCount >= maxLoadTryCount) return;


			rewardedVideoAd = new RewardedVideoAd(rewardedPlacementId);

			rewardedVideoAd.Register(gameObject);

			rewardedVideoAd.RewardedVideoAdDidLoad = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdDidLoad");
				loadRewardedAdsTryCount = 0;
				isRewardedLoaded = true;
			};
			rewardedVideoAd.RewardedVideoAdDidFailWithError = (string error) =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdDidFailWithError " + error);

				loadRewardedAdsTryCount += 1;
				LoadRewarded();
			};

			rewardedVideoAd.RewardedVideoAdDidClick = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdDidClick");
				adsClickBlock.SafeInvoke();
			};

			rewardedVideoAd.RewardedVideoAdDidSucceed = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdDidSucceed");
			};

			rewardedVideoAd.RewardedVideoAdDidFail = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdDidFail");
				loadRewardedAdsTryCount += 1;
				isRewardedLoaded = false;
				LoadRewarded();
			};

			rewardedVideoAd.RewardedVideoAdComplete = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdComplete");
#if UNITY_IOS
                isReward = true;
#else
				adsShowedBlock.SafeInvoke();
				adsShowedBlock = null;
				adsClickBlock = null;
				adsSkipBlock = null;
#endif
			};

			rewardedVideoAd.RewardedVideoAdWillClose = () => { Debug.Log("AudienceNetwork RewardedVideoAdWillClose"); };

			rewardedVideoAd.RewardedVideoAdWillLogImpression = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdWillLogImpression");
			};

			rewardedVideoAd.RewardedVideoAdDidClose = () =>
			{
				Debug.Log("AudienceNetwork RewardedVideoAdDidClose");
#if UNITY_IOS
                if (isReward)
                {
                    adsShowedBlock.SafeInvoke();
                    isReward = false;
                }
                else adsSkipBlock.SafeInvoke();
#else
				adsSkipBlock.SafeInvoke();
#endif
				isRewardedLoaded = false;

				if (this.rewardedVideoAd != null) this.rewardedVideoAd.Dispose();
				LoadRewarded();
			};

			rewardedVideoAd.LoadAd();

			Debug.Log("AudienceNetwork rewardedVideoAd.LoadAd");
		}

		private void OnDestroy()
		{
			if (interstitialAd != null) interstitialAd.Dispose();
			if (rewardedVideoAd != null) rewardedVideoAd.Dispose();
		}
	}
}
#endif