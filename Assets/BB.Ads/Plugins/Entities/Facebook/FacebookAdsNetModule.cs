using SimpleJSON;
using UnityEngine;

namespace BlackBears.Ads.FacebookAds
{
    internal sealed class FacebookdAdsNetModule : AdsNetModule
    {
#if UNITY_IOS
        private const string rewardedPlacementIdKey = "rewarded_placement_id_ios";
        private const string interstitialPlacementIdKey = "interstitial_placement_id_ios";
#elif UNITY_ANDROID
        private const string rewardedPlacementIdKey = "rewarded_placement_id_android";
        private const string interstitialPlacementIdKey = "interstitial_placement_id_android";

#endif

        private string rewardedPlacementId;
        private string interstitialPlacementId;
        private bool appPaused;
        private Block blockToCall = null;
#if !BBGC_ADS_FACEBOOK_DISABLED
        private FacebookAdsProcessor processor;
#endif

#if BBGC_ADS_FACEBOOK_DISABLED
        internal override bool IsRewardedAvailable => false;
        internal override bool IsInterstitialAvailable => false;

#else
        internal override bool IsRewardedAvailable => processor.IsRewardedLoaded;
        internal override bool IsInterstitialAvailable => processor.IsInterstitialLoaded;

#endif

        internal FacebookdAdsNetModule(JSONNode node) : base(node, AdsDefaults.facebookAdsKey)
        {
            if (!enabled) return;
            rewardedPlacementId = node[rewardedPlacementIdKey].Value;
            interstitialPlacementId = node[interstitialPlacementIdKey].Value;
        }

        internal override void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_FACEBOOK_DISABLED
            if (!processor.IsRewardedLoaded)
            {
                onFail.SafeInvoke();
                return;
            }

            processor.ShowRewardedVideo(onSuccess, onClick, onSkip);
#endif
        }

        internal override void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_FACEBOOK_DISABLED
            if (!processor.IsInterstitialLoaded)
            {
                onFail.SafeInvoke();
                return;
            }

            processor.ShowInterstitial(onSuccess, onClick);
#endif
        }

        protected override void ProcessActivate()
        {
#if !BBGC_ADS_FACEBOOK_DISABLED && !UNITY_EDITOR
            var go = new GameObject("~~FacebookListener");
            Object.DontDestroyOnLoad(go);
            processor = go.AddComponent<FacebookAdsProcessor>();
            processor.Activate(rewardedPlacementId, interstitialPlacementId);
#endif
        }
    }
}