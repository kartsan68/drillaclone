#if !BBGC_ADS_APPLOVIN_DISABLED
using System;
using UnityEngine;

namespace BlackBears.Ads.ApplovinAds
{

    internal class AppLovinProcessor : MonoBehaviour
    {

        private const int maxLoadTryCount = 3;

        private int loadRewardedAdsTryCount;
        private int loadInterstitialdsTryCount;
        private Block adsShowedBlock;
        private Block onClick;

        internal void Activate()
        {
            AppLovin.SetUnityAdListener(gameObject.name);
            PreloadRewarded();
            PreloadInterstitial();
        }

        internal void ShowRewarded(Block onSuccess, Block onClick)
        {
            if (!AppLovin.IsIncentInterstitialReady()) return;
            adsShowedBlock = onSuccess;
            this.onClick = onClick;
            AppLovin.ShowRewardedInterstitial();
        }

        internal void ShowInterstitial(Block onSuccess, Block onClick)
        {
            if (!AppLovin.IsIncentInterstitialReady()) return;
            adsShowedBlock = onSuccess;
            this.onClick = onClick;
            AppLovin.ShowInterstitial();
        }

        private void PreloadRewarded()
        {
            if (loadRewardedAdsTryCount >= maxLoadTryCount) return;
            AppLovin.LoadRewardedInterstitial();
        }

        private void PreloadInterstitial()
        {
            if (loadInterstitialdsTryCount >= maxLoadTryCount) return;
            AppLovin.PreloadInterstitial();
        }

        private void onAppLovinEventReceived(string e)
        {
            if (string.Equals(e, "LOADEDREWARDED")) OnRewardedAdLoaded();
            else if (string.Equals(e, "LOADREWARDEDFAILED")) OnRewardedAdLoadFailed();
            else if (string.Equals(e, "HIDDENREWARDED")) OnRewardedAdHidden();
            else if (string.Equals(e, "LOADFAILED")) OnAdLoadFailed();
            else if (string.Equals(e, "LOADEDINTER")) OnAdLoaded();
            else if (string.Equals(e, "HIDDENINTER")) OnAdHidden();
            else if (string.Equals(e, "CLICKED")) OnAdClicked();
        }

        private void OnRewardedAdLoaded()
        {
            loadRewardedAdsTryCount = 0;
        }

        private void OnAdClicked()
        {
            onClick.SafeInvoke();
        }

        private void OnRewardedAdLoadFailed()
        {
            loadRewardedAdsTryCount += 1;
            PreloadRewarded();
        }

        private void OnRewardedAdHidden()
        {
            PreloadRewarded();
            adsShowedBlock.SafeInvoke();
            adsShowedBlock = null;
            onClick = null;
        }

        private void OnAdLoaded()
        {
            loadInterstitialdsTryCount = 0;
        }

        private void OnAdLoadFailed()
        {
            loadInterstitialdsTryCount += 1;
            PreloadInterstitial();
        }

        private void OnAdHidden()
        {
            PreloadInterstitial();
            adsShowedBlock.SafeInvoke();
            adsShowedBlock = null;
            onClick = null;
        }

    }

}
#endif