using SimpleJSON;
using UnityEngine;

namespace BlackBears.Ads.ApplovinAds
{

    internal class ApplovinAdsNetModule : AdsNetModule
    {

        private string sdkKey;
#if !BBGC_ADS_APPLOVIN_DISABLED
        private AppLovinProcessor processor;
#endif

        internal ApplovinAdsNetModule(JSONNode node, string sdkKey) : base(node, AdsDefaults.applovinAdsKey)
        {
            if (!enabled) return;
            this.sdkKey = sdkKey;
        }

#if BBGC_ADS_APPLOVIN_DISABLED
        internal override bool IsRewardedAvailable { get { return false; } }
        internal override bool IsInterstitialAvailable { get { return false; } }
#else
        internal override bool IsRewardedAvailable => AppLovin.IsIncentInterstitialReady();

        internal override bool IsInterstitialAvailable => AppLovin.IsIncentInterstitialReady();
#endif

        internal override void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_APPLOVIN_DISABLED
            if (processor == null) onFail.SafeInvoke();
            else processor.ShowRewarded(onSuccess, onClick);
#endif
        }

        internal override void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_APPLOVIN_DISABLED
            if (processor == null) onFail.SafeInvoke();
            else processor.ShowInterstitial(onSuccess, onClick);
#endif
        }

        protected override void ProcessActivate()
        {
#if !BBGC_ADS_APPLOVIN_DISABLED && !UNITY_EDITOR
#if UNITY_IOS
            AppLovin.SetSdkKey(sdkKey);
#endif

            AppLovin.InitializeSdk();

            var go = new GameObject("~~ApplovinListener");
            Object.DontDestroyOnLoad(go);
            processor = go.AddComponent<AppLovinProcessor>();
            processor.Activate();
#endif
        }


    }

}