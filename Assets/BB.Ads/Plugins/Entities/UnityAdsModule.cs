using SimpleJSON;
using UnityEngine.Advertisements;

namespace BlackBears.Ads
{

    internal sealed class UnityAdsNetModule : AdsNetModule
    {

        private const string rewardedVideoKey = "rewardedVideo";
        private const string interstitialVideoKey = "video";

#if UNITY_IOS
        private const string advertiseIdKey = "unity_ios_ad_id";
#elif UNITY_ANDROID
        private const string advertiseIdKey = "unity_and_ad_id";
#endif

        private string advertiseId;


#if BBGC_ADS_UNITY_DISABLED
        internal override bool IsRewardedAvailable => false;

        internal override bool IsInterstitialAvailable => false;
#else
        internal override bool IsRewardedAvailable => Advertisement.IsReady(rewardedVideoKey);

        internal override bool IsInterstitialAvailable => Advertisement.IsReady(interstitialVideoKey);
#endif

        private Block onClick;
        private bool isSdsShowing = false;

        internal UnityAdsNetModule(JSONNode node) : base(node, AdsDefaults.unityAdsKey)
        {
            if (!enabled) return;
            advertiseId = node[advertiseIdKey].Value;
        }

        internal override void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_UNITY_DISABLED
            this.onClick = onClick;
            isSdsShowing = true;
            Advertisement.Show(rewardedVideoKey, new ShowOptions
            {
                resultCallback = result =>
                {
                    switch (result)
                    {
                        case ShowResult.Finished: onSuccess.SafeInvoke(); break;
                        case ShowResult.Skipped: onSkip.SafeInvoke(); break;
                        case ShowResult.Failed: onFail.SafeInvoke((long)AdsFailCodes.AdsNetError, "Unity fail show"); break;
                    }
                    this.onClick = null;
                    isSdsShowing = false;
                }
            });
#else
            onFail.SafeInvoke();
#endif
        }

        internal override void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_UNITY_DISABLED

            this.onClick = onClick;
            isSdsShowing = true;
            Advertisement.Show(interstitialVideoKey, new ShowOptions
            {
                resultCallback = result =>
                {
                    switch (result)
                    {
                        case ShowResult.Finished: onSuccess.SafeInvoke(); break;
                        case ShowResult.Skipped: onSuccess.SafeInvoke(); break;
                        case ShowResult.Failed: onFail.SafeInvoke((long)AdsFailCodes.AdsNetError, "Unity fail show"); break;
                    }

                    this.onClick = null;
                    isSdsShowing = false;
                }
            });
#else
            onFail.SafeInvoke();
#endif
        }

        internal override void OnAppPaused()
        {
            if(isSdsShowing) onClick.SafeInvoke();
        }

        protected override void ProcessActivate()
        {
#if !BBGC_ADS_UNITY_DISABLED
            Advertisement.Initialize(advertiseId);
#endif
        }
    }

}