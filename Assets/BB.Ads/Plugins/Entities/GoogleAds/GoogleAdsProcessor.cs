﻿#if !BBGC_ADS_GOOGLE_DISABLED
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

namespace BlackBears.Ads.GoogleAds
{
    public class GoogleAdsProcessor : MonoBehaviour
    {
        private const int maxLoadTryCount = 3;
        private int loadRewardAdsTryCount;
        private int loadInterstitialAdsTryCount;

        private string rewardAdUnitId;

        private Block adsShowedBlock;
        private Block adsSkipBlock;
        private Block adsClickBlock;
        private RewardBasedVideoAd rewardBasedVideo;
        private InterstitialAd interstitialAd;
        private bool isReward;

        internal bool IsRewardedLoaded
        {
            get
            {
                if (rewardBasedVideo.IsLoaded()) return true;
                else
                {
                    loadRewardAdsTryCount = maxLoadTryCount - 1;
                    PreloadRewarded();
                    return false;
                }
            }
        }

        internal bool IsInterstitialLoaded
        {
            get
            {
                if (interstitialAd.IsLoaded()) return true;
                else
                {
                    loadInterstitialAdsTryCount = maxLoadTryCount - 1;
                    PreloadInterstitial();
                    return false;
                }
            }
        }

        internal void Activate(string rewardAdUnitId, string interstitialAdUnitId)
        {
            this.rewardAdUnitId = rewardAdUnitId;
            this.rewardBasedVideo = RewardBasedVideoAd.Instance;

            rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
            rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
            rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

            this.interstitialAd = new InterstitialAd(interstitialAdUnitId);

            this.interstitialAd.OnAdLoaded += HandleOnAdLoaded;
            this.interstitialAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;
            this.interstitialAd.OnAdClosed += HandleOnAdClosed;
            this.interstitialAd.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

            PreloadRewarded();
            PreloadInterstitial();
        }

        internal void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
        {
            if (!rewardBasedVideo.IsLoaded())
            {
                onFail.SafeInvoke();
                return;
            }
            adsShowedBlock = onSuccess;
            adsSkipBlock = onSkip;
            adsClickBlock= onClick;

            rewardBasedVideo.Show();
        }

        internal void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
        {
            if (!interstitialAd.IsLoaded())
            {
                onFail.SafeInvoke();
                return;
            }
            adsShowedBlock = onSuccess;
            adsSkipBlock = null;
            adsClickBlock = onClick;
            interstitialAd.Show();
        }

        private void PreloadRewarded()
        {
            if (loadRewardAdsTryCount >= maxLoadTryCount) return;
            // AdRequest request = new AdRequest.Builder().AddTestDevice("163252dee1fc03356c232aca4956334a").Build();
            AdRequest request = new AdRequest.Builder().Build();
            this.rewardBasedVideo.LoadAd(request, rewardAdUnitId);
        }

        private void PreloadInterstitial()
        {
            if (loadInterstitialAdsTryCount >= maxLoadTryCount) return;
            // AdRequest request = new AdRequest.Builder().AddTestDevice("163252dee1fc03356c232aca4956334a").Build();
            AdRequest request = new AdRequest.Builder().Build();
            this.interstitialAd.LoadAd(request);
        }

        public void HandleOnAdLoaded(object sender, EventArgs args)
        {
            loadInterstitialAdsTryCount = 0;
        }

        public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            loadInterstitialAdsTryCount += 1;
            PreloadInterstitial();
        }

        public void HandleOnAdClosed(object sender, EventArgs args)
        {
            PreloadInterstitial();
            adsShowedBlock.SafeInvoke();
            adsShowedBlock = null;
            adsClickBlock = null;
        }


        public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
        {
            loadRewardAdsTryCount = 0;
        }

        public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            loadRewardAdsTryCount += 1;
            PreloadRewarded();
        }

        public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
        {
            PreloadRewarded();
#if UNITY_IOS
            if (isReward)
            {
                adsShowedBlock.SafeInvoke();
                isReward = false;
            }
            else
            {
                adsSkipBlock.SafeInvoke();
                adsSkipBlock = null;
            }
            adsShowedBlock = null;
            adsClickBlock = null;
#else
            adsShowedBlock = null;
            adsClickBlock = null;
            adsSkipBlock.SafeInvoke();
            adsSkipBlock = null;
            
#endif

        }

        public void HandleRewardBasedVideoRewarded(object sender, Reward args)
        {
            string type = args.Type;
            double amount = args.Amount;
            // ios выдает success когда реклама еще не закрыта и он отрабатывает на фоне
#if UNITY_IOS
            isReward = true;
#else
            adsShowedBlock.SafeInvoke();
            adsShowedBlock = null;
#endif
            adsSkipBlock = null;
            adsClickBlock = null;
        }

        public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
        {
            adsClickBlock.SafeInvoke();
        }
    }
}
#endif