﻿#if !BBGC_ADS_GOOGLE_DISABLED
using GoogleMobileAds.Api;
#endif
using SimpleJSON;
using UnityEngine;

namespace BlackBears.Ads.GoogleAds
{

    internal class GoogleAdsNetModule : AdsNetModule
    {
        public enum State
        {
            None,
            Showed,
            Skip,
            Fail
        }

#if UNITY_ANDROID
        private const string appIdKey = "android_app_id";
#elif UNITY_IPHONE
        private const string appIdKey = "ios_app_id";
#endif

#if UNITY_ANDROID
        private const string rewardedAdUnitIdKey = "android_reward_ad_unit_id";
        private const string interstitialAdUnitIdKey = "android_interstitial_ad_unit_id";
#elif UNITY_IPHONE
        private const string rewardedAdUnitIdKey = "ios_reward_ad_unit_id";
        private const string interstitialAdUnitIdKey = "ios_interstitial_ad_unit_id";
#endif

        private string appId;
        private string rewardedAdUnitId;
        private string interstitialAdUnitId;
#if !BBGC_ADS_GOOGLE_DISABLED
        private GoogleAdsProcessor processor;
#endif
        private State currentState = State.None;
        private Block onSuccess;
        private Block onSkip;
        private FailBlock onFail;


        internal GoogleAdsNetModule(JSONNode node) : base(node, AdsDefaults.googleAdsKey)
        {
            if (!enabled) return;
            appId = node[appIdKey].Value;
            rewardedAdUnitId = node[rewardedAdUnitIdKey].Value;
            interstitialAdUnitId = node[interstitialAdUnitIdKey].Value;
        }

#if BBGC_ADS_GOOGLE_DISABLED
        internal override bool IsRewardedAvailable => false;
        internal override bool IsInterstitialAvailable => false;
#else
        internal override bool IsRewardedAvailable => processor.IsRewardedLoaded;
        internal override bool IsInterstitialAvailable => processor.IsInterstitialLoaded;
#endif

        internal override void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_GOOGLE_DISABLED
            if (processor == null)
            {
                onFail.SafeInvoke();
                return;
            }

#if UNITY_IOS
            processor.ShowRewarded(onSuccess, onSkip, onClick, onFail);
#else
            processor.ShowRewarded(() => currentState = State.Showed,
                () => currentState = State.Skip, onClick, (c, m) => currentState = State.Fail);
            currentState = State.None;
            this.onSuccess = onSuccess;
            this.onSkip = onSkip;
            this.onFail = onFail;

#endif
#endif
        }

        internal override void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_GOOGLE_DISABLED
            if (processor == null)
            {
                onFail.SafeInvoke();
                return;
            }
#if UNITY_IOS
            processor.ShowInterstitial(onSuccess, onClick, onFail);
#else
            processor.ShowInterstitial(() => currentState = State.Showed, onClick,
                (c, m) => currentState = State.Fail);
            currentState = State.None;
            this.onSuccess = onSuccess;
            this.onFail = onFail;

#endif
#endif
        }

        protected override void ProcessActivate()
        {
#if !BBGC_ADS_GOOGLE_DISABLED && !UNITY_EDITOR

            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(appId);

            var go = new GameObject("~~GoogleAdsListener");
            Object.DontDestroyOnLoad(go);
            processor = go.AddComponent<GoogleAdsProcessor>();
            processor.Activate(rewardedAdUnitId, interstitialAdUnitId);
#endif
        }

#if !UNITY_IOS
        internal override void OnAppResumed()
        {
            switch (currentState)
            {
                case State.None:
                    return;
                case State.Showed:
                    onSuccess.SafeInvoke();
                    break;
                case State.Skip:
                    onSkip.SafeInvoke();
                    break;
                case State.Fail:
                    onFail.SafeInvoke();
                    break;
            }

            this.onSuccess = null;
            this.onSkip = null;
            this.onFail = null;
            currentState = State.None;
        }
#endif
    }

}