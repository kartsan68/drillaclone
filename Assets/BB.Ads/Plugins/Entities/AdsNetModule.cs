using System;
using BlackBears.Ads.GoogleAds;
using SimpleJSON;

namespace BlackBears.Ads
{

    internal abstract class AdsNetModule
    {

        internal readonly bool enabled;
        internal readonly string name;

        protected AdsNetModule(JSONNode node, string name)
        {
            this.name = name;
            enabled = node != null && node.Tag != JSONNodeType.None && node.Tag != JSONNodeType.NullValue;
        }

        internal bool Activated { get; private set; }
        internal abstract bool IsRewardedAvailable { get; }
        internal abstract bool IsInterstitialAvailable { get; }

        internal void Activate()
        {
            if (!enabled || Activated) return;
            ProcessActivate();
            Activated = true;
        }

        internal abstract void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail);
        internal abstract void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail);

        protected abstract void ProcessActivate();

        internal virtual void OnAppPaused() { }
        internal virtual void OnAppResumed() { }

    }

}