using System;
using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.Ads
{
    internal class Distribution
    {

        private const int forcedAdsStartPriority = 10000;
        private const int maxSupportedAdsCount = 6;

        private int activeAdsCount;

        private string[] adsNames = new string[maxSupportedAdsCount];
        private float[] adsPriorities = new float[maxSupportedAdsCount];

        private float[] adsWeightBuffer = new float[maxSupportedAdsCount];
        private int[] adsOrderBuffer = new int[maxSupportedAdsCount];

        internal Distribution(JSONNode node)
        {
            int adsIndex = 0;
            ReadAds(adsIndex++, AdsDefaults.applovinAdsKey, node);
            ReadAds(adsIndex++, AdsDefaults.unityAdsKey, node);
            ReadAds(adsIndex++, AdsDefaults.facebookAdsKey, node);
            ReadAds(adsIndex++, AdsDefaults.ironSrcAdsKey, node);
            ReadAds(adsIndex++, AdsDefaults.googleAdsKey, node);
            ReadAds(adsIndex++, AdsDefaults.appLovinMAX, node);
            
            NormalizePriorities();
        }

        internal bool IsModuleAvailable(string name)
        {
            for (int i = 0; i < adsNames.Length; i++)
            {
                if (!string.Equals(name, adsNames[i])) continue;
                return adsPriorities[i] >= 0f;
            }
            return false;
        }

        internal void PrioritizeAds(List<string> nameBuffer)
        {
            nameBuffer.Clear();
            for (int i = 0; i < maxSupportedAdsCount; i++)
            {
                adsOrderBuffer[i] = i;
                adsWeightBuffer[i] = adsPriorities[i];
                if (adsWeightBuffer[i] < forcedAdsStartPriority)
                {
                    adsWeightBuffer[i] *= UnityEngine.Random.Range(0f, 1f);
                }
            }

            Array.Sort(adsWeightBuffer, adsOrderBuffer);

            for (int i = maxSupportedAdsCount - 1; i >= 0; i--)
            {
                if (adsWeightBuffer[i] < 0f) continue;
                nameBuffer.Add(adsNames[adsOrderBuffer[i]]);
            }
        }

        private void ReadAds(int index, string name, JSONNode node)
        {
            adsNames[index] = name;
            adsPriorities[index] = node[name].GetFloatOrDefault(-1);
        }

        private void NormalizePriorities()
        {
            float maxP = 0f;
            for (int i = 0; i < adsPriorities.Length; i++)
            {
                if (adsPriorities[i] < 0f) continue;
                if (adsPriorities[i] >= forcedAdsStartPriority) continue;

                maxP = UnityEngine.Mathf.Max(maxP, adsPriorities[i]);
            }

            if (maxP == 0f) return;
            for (int i = 0; i < adsPriorities.Length; i++)
            {
                if (adsPriorities[i] < 0f) continue;
                if (adsPriorities[i] >= forcedAdsStartPriority) continue;

                adsPriorities[i] /= maxP;
            }
        }

    }
}