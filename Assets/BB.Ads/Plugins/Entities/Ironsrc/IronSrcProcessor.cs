﻿using System;
using UnityEngine;

namespace BlackBears.Ads.IronSrcAds
{

    internal class IronSrcProcessor : MonoBehaviour
    {
#if !BBGC_ADS_IRONSRC_DISABLED

        private const int maxLoadTryCount = 3;

        public enum State
        {
            None,
            Showed,
            Skip,
            Fail
        }

        public bool skipVideo = true;

        private State currentState = State.None;

        public State CurrentState => currentState;

        private int loadInterstitialdsTryCount = 0;

        internal void Activate()
        {
            IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

            IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
            IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
            IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;

            LoadInterstitial();
        }

        internal void LoadInterstitial()
        {
            if (!IronSource.Agent.isInterstitialReady() && loadInterstitialdsTryCount >= maxLoadTryCount) return;
            loadInterstitialdsTryCount += 1;
            IronSource.Agent.loadInterstitial();
        }

        internal void ShowRewarded()
        {
            if (!IronSource.Agent.isRewardedVideoAvailable()) return;
            ResetState();
            IronSource.Agent.showRewardedVideo();
        }

        internal void ShowInterstitial()
        {
            if (!IronSource.Agent.isInterstitialReady()) return;
            ResetState();
            IronSource.Agent.showInterstitial();
        }

        internal void ResetState()
        {
            skipVideo = true;
            currentState = State.None;
        }


        void RewardedVideoAdOpenedEvent()
        {
            Debug.Log("[BB.IRONSRC] RewardedVideoAdOpenedEvent");
        }

        void RewardedVideoAdClosedEvent()
        {
            Debug.Log("[BB.IRONSRC] RewardedVideoAdClosedEvent skipVideo: " + skipVideo);
            if (skipVideo)
            {
                currentState = State.Skip;
            }
        }

        void RewardedVideoAdStartedEvent()
        {
            Debug.Log("[BB.IRONSRC] RewardedVideoAdStartedEvent");
        }

        void RewardedVideoAdEndedEvent()
        {
            Debug.Log("[BB.IRONSRC] RewardedVideoAdEndedEvent");
        }

        void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
        {
            Debug.Log("[BB.IRONSRC] RewardedVideoAdRewardedEvent");
            currentState = State.Showed;
            skipVideo = false;
        }

        void RewardedVideoAdShowFailedEvent(IronSourceError error)
        {
            currentState = State.Fail;
            skipVideo = false;
        }

        void InterstitialAdLoadFailedEvent(IronSourceError error)
        {
            loadInterstitialdsTryCount += 1;
            LoadInterstitial();
        }

        void InterstitialAdShowSucceededEvent()
        {
            currentState = State.Showed;
        }

        void InterstitialAdShowFailedEvent(IronSourceError error)
        {
            currentState = State.Fail;
        }

        void InterstitialAdClickedEvent()
        {
        }

        void InterstitialAdClosedEvent()
        {
            LoadInterstitial();
        }

        void InterstitialAdReadyEvent()
        {
            loadInterstitialdsTryCount = 0;
        }

        void InterstitialAdOpenedEvent()
        {
        }
#endif
    }

}
