﻿using SimpleJSON;
using UnityEngine;

namespace BlackBears.Ads.IronSrcAds
{

    internal class IronSrcAdsNetModule : AdsNetModule
    {
        private const string appKeyKey = "app_key";

        private string appkey;

        private IronSrcProcessor processor;

        private Block onSuccess;
        private Block onSkip;
        private FailBlock onFail;
        private Block onClick;
        private bool isSdsShowing = false;

        internal IronSrcAdsNetModule(JSONNode node) : base(node, AdsDefaults.ironSrcAdsKey)
        {
            if (!enabled) return;
            this.appkey = node[appKeyKey];
        }

#if BBGC_ADS_IRONSRC_DISABLED
        internal override bool IsRewardedAvailable => false;
        internal override bool IsInterstitialAvailable => false;

#else
        internal override bool IsRewardedAvailable => IronSource.Agent.isRewardedVideoAvailable();
        internal override bool IsInterstitialAvailable => IronSource.Agent.isInterstitialReady();

#endif

        internal override void ShowRewarded(Block onSuccess, Block onSkip, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_IRONSRC_DISABLED
            if (processor == null)
            {
                onFail.SafeInvoke();
                return;
            }

            this.onSuccess = onSuccess;
            this.onSkip = onSkip;
            this.onFail = onFail;
            this.onClick = onClick;
            isSdsShowing = true;
            processor.ShowRewarded();
#endif
        }

        internal override void ShowInterstitial(Block onSuccess, Block onClick, FailBlock onFail)
        {
#if !BBGC_ADS_IRONSRC_DISABLED

            if (processor == null)
            {
                onFail.SafeInvoke();
                return;
            }

            this.onSuccess = onSuccess;
            this.onSkip = null;
            this.onFail = onFail;
            this.onClick = onClick;
            isSdsShowing = true;
            processor.ShowInterstitial();
#endif

        }

        protected override void ProcessActivate()
        {
#if !BBGC_ADS_IRONSRC_DISABLED && !UNITY_EDITOR
            IronSource.Agent.init(appkey, IronSourceAdUnits.REWARDED_VIDEO,  IronSourceAdUnits.INTERSTITIAL);
            var go = new GameObject("~~IronSrcListener");
            Object.DontDestroyOnLoad(go);
            processor = go.AddComponent<IronSrcProcessor>();
            processor.Activate();
#endif
        }

        internal override void OnAppPaused()
        {
#if !BBGC_ADS_IRONSRC_DISABLED
            IronSource.Agent.onApplicationPause(true);
#endif
            if(isSdsShowing) onClick.SafeInvoke();
        }

        internal override void OnAppResumed()
        {
#if !BBGC_ADS_IRONSRC_DISABLED

            IronSource.Agent.onApplicationPause(false);

            if (processor == null) return;

            switch (processor.CurrentState)
            {
                case IronSrcProcessor.State.None:
                    return;
                case IronSrcProcessor.State.Showed:
                    onSuccess.SafeInvoke();
                    break;
                case IronSrcProcessor.State.Skip:
                    onSkip.SafeInvoke();
                    break;
                case IronSrcProcessor.State.Fail:
                    onFail.SafeInvoke();
                    break;
            }

            this.onSuccess = null;
            this.onSkip = null;
            this.onFail = null;
            this.onClick = null;
            isSdsShowing = false;
            processor.ResetState();
#endif

        }

    }

}