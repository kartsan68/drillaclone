using System.Collections.Generic;
using BlackBears.Ads.ApplovinAds;
using BlackBears.Ads.ApplovinMAX;
using BlackBears.Ads.IronSrcAds;
using BlackBears.Ads.GoogleAds;
using BlackBears.Ads.FacebookAds;
using SimpleJSON;

namespace BlackBears.Ads
{

    internal class AdsDefaults
    {

        private const string defaultDistibutionKey = "default";

        internal const string unityAdsKey = "unity";
        internal const string applovinAdsKey = "applovin";
        internal const string facebookAdsKey = "facebook";
        internal const string googleAdsKey = "google";
        internal const string ironSrcAdsKey = "ironsrc";
        internal const string appLovinMAX = "applovin_max";


        private Dictionary<string, Distribution> rewardedDistributionByCountry = new Dictionary<string, Distribution>();
        private Dictionary<string, Distribution> interstitialDistributionByCountry = new Dictionary<string, Distribution>();
        private List<AdsNetModule> modules = new List<AdsNetModule>();

        internal AdsDefaults(JSONNode node, AdsSettings settings)
        {
            var rewardedDistributionNode = node["rewarded_distribution"];
            foreach(var kvp in rewardedDistributionNode)
            {
                rewardedDistributionByCountry[kvp.Key.ToLowerInvariant()] = new Distribution(kvp.Value);
            }

            var interstitialDistributionNode = node["interstitial_distribution"];
            foreach(var kvp in interstitialDistributionNode)
            {
                interstitialDistributionByCountry[kvp.Key.ToLowerInvariant()] = new Distribution(kvp.Value);
            }

            modules.Add(new UnityAdsNetModule(node[unityAdsKey]));
            modules.Add(new ApplovinAdsNetModule(node[applovinAdsKey], settings.ApplovinKey));
            modules.Add(new FacebookdAdsNetModule(node[facebookAdsKey]));
            modules.Add(new IronSrcAdsNetModule(node[ironSrcAdsKey]));
            modules.Add(new GoogleAdsNetModule(node[googleAdsKey]));
            modules.Add(new ApplovinMAXModule(node[appLovinMAX],appLovinMAX));
        }

        internal Distribution GetRewardedDistribution(string country)
        {
            country = country.ToLowerInvariant();
            Distribution distribution;
            rewardedDistributionByCountry.TryGetValue(country, out distribution);
            if (distribution == null && string.Equals(country, defaultDistibutionKey)) return null;
            return distribution ?? GetRewardedDistribution(defaultDistibutionKey);
        }

        internal Distribution GetInterstitialDistribution(string country)
        {
            country = country.ToLowerInvariant();
            Distribution distribution;
            interstitialDistributionByCountry.TryGetValue(country, out distribution);
            if (distribution == null && string.Equals(country, defaultDistibutionKey)) return null;
            return distribution ?? GetInterstitialDistribution(defaultDistibutionKey);
        }

        internal List<AdsNetModule> GetModulesForDistribution(Distribution distribution)
        {
            var list = new List<AdsNetModule>();
            if (distribution != null)
            {
                foreach(var module in modules)
                {
                    if (module == null || !module.enabled) continue;
                    if (!distribution.IsModuleAvailable(module.name)) continue;
                    list.Add(module);
                }
            }
            return list;
        }

    }

}