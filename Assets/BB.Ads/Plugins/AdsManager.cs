using System;
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Analytics;
using SimpleJSON;

namespace BlackBears.Ads
{

    public class AdsManager
    {

        private AdsDefaults defaults;
        private Dictionary<string, AdsNetModule> modulesByName = new Dictionary<string, AdsNetModule>();

        private Distribution rewardedDistribution;
        private Distribution interstitialDistribution;

        private List<string> nameBuffer = new List<string>();

        public AdsManager(AdsSettings settings, string defaultsConfig, string country)
        {
            defaults = new AdsDefaults(JSON.Parse(defaultsConfig), settings);
            rewardedDistribution = defaults.GetRewardedDistribution(country);
            interstitialDistribution = defaults.GetInterstitialDistribution(country);
            var modules = defaults.GetModulesForDistribution(rewardedDistribution);
            if (modules != null)
            {
                foreach (var module in modules)
                {
                    if (module != null) modulesByName[module.name] = module;
                }
            }

            ActivateModules(modules);
        }

        private void ActivateModules(List<AdsNetModule> modules)
        {
            if (modules == null) return;
            foreach (var module in modules)
            {
                if (module != null) module.Activate();
            }
        }

        public void ShowRewarded(Block<string> onPlatformSelected, Block<string> onSuccess,
            Block onSkip, Block onClick, FailBlock onFail, string source = "unknown")
        {

#if BBGC_ANALYTICS_FEATURE
            var analytic = BBGC.Features.Analytics();
            analytic.AdsRewRequest(source);
#endif

            if (rewardedDistribution == null)
            {
                onFail.SafeInvoke((long)AdsFailCodes.NoDistribution, "No distribution for show");
#if BBGC_ANALYTICS_FEATURE
                analytic.AdsRewError("No distribution for show", "unknown");
#endif
                return;
            }
            rewardedDistribution.PrioritizeAds(nameBuffer);

            foreach (var name in nameBuffer)
            {
                var module = GetModuleByName(name);
                if (module == null || !module.IsRewardedAvailable) continue;
                onPlatformSelected.SafeInvoke(name);
#if BBGC_ANALYTICS_FEATURE
                analytic.AdsRewStart(name);
#endif
                module.ShowRewarded(() =>
                {
                    onSuccess.SafeInvoke(name);
#if BBGC_ANALYTICS_FEATURE
                    analytic.AdsRewView(name);
                    analytic.AdsView(name);
#endif
                }, onSkip, onClick, (c, m) =>
                {
                    onFail.SafeInvoke(c, m);
#if BBGC_ANALYTICS_FEATURE
                    analytic.AdsRewError($"Code: {c}; Message: {m}", name);
#endif
                });
                return;
            }

            onFail.SafeInvoke((long)AdsFailCodes.NoAds, "No ads for show");
#if BBGC_ANALYTICS_FEATURE
            analytic.AdsRewNo();
#endif
        }

        public void ShowInterstitial(Block<string> onPlatformSelected, Block<string> onSuccess,
            Block onClick, FailBlock onFail, string source = "unknown")
        {
#if BBGC_ANALYTICS_FEATURE 
            var analytic = BBGC.Features.Analytics();
            analytic.AdsIntRequest(source);
#endif
            if (interstitialDistribution == null)
            {
                onFail.SafeInvoke((long)AdsFailCodes.NoDistribution, "No distribution for show");
#if BBGC_ANALYTICS_FEATURE
                analytic.AdsIntError("No distribution for show", "unknown");
#endif
                return;
            }
            interstitialDistribution.PrioritizeAds(nameBuffer);

            foreach (var name in nameBuffer)
            {
                var module = GetModuleByName(name);
                if (module == null || !module.IsInterstitialAvailable) continue;
                onPlatformSelected.SafeInvoke(name);
#if BBGC_ANALYTICS_FEATURE
                analytic.AdsIntStart(name);
#endif
                module.ShowInterstitial(() =>
                {
                    onSuccess.SafeInvoke(name);

#if BBGC_ANALYTICS_FEATURE
                    analytic.AdsIntView(name);
                    analytic.AdsView(name);
#endif
                }, onClick, (c, m) =>
                {
                    onFail.SafeInvoke(c, m);
#if BBGC_ANALYTICS_FEATURE
                    analytic.AdsIntError($"Code: {c}; Message: {m}", name);
#endif
                });
                return;
            }

            onFail.SafeInvoke((long)AdsFailCodes.NoAds, "No ads for show");
#if BBGC_ANALYTICS_FEATURE
            analytic.AdsIntNo();
#endif

        }

        internal bool IsRewardedAdsAvailable()
        {
            rewardedDistribution.PrioritizeAds(nameBuffer);

            foreach (var name in nameBuffer)
            {
                var module = GetModuleByName(name);
                if (module == null || !module.IsRewardedAvailable) continue;
                return true;
            }

            return false;
        }

        internal bool IsInterstitialAdsAvailable()
        {
            interstitialDistribution.PrioritizeAds(nameBuffer);

            foreach (var name in nameBuffer)
            {
                var module = GetModuleByName(name);
                if (module == null || !module.IsInterstitialAvailable) continue;
                return true;
            }

            return false;
        }

        internal void OnAppPaused()
        {
            foreach (var module in modulesByName.Values) module.OnAppPaused();
        }

        internal void OnAppResumed()
        {
            foreach (var module in modulesByName.Values) module.OnAppResumed();
        }

        private AdsNetModule GetModuleByName(string name)
        {
            AdsNetModule module;
            modulesByName.TryGetValue(name, out module);
            return module;
        }

    }

}