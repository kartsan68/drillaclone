using UnityEngine;

namespace BlackBears.GameCore.Editor
{
    public class BBImageHelperParameters : ScriptableObject
    {
        public const string path = "Assets/Editor/BBImageHelperParameters.asset";
        public BBAtlasChecker[] chekers;
    }

}