﻿using System.IO;
using BB.GameCore.Plugins;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace BlackBears.GameCore.Editor
{

    public static class PostProcessBuildAndoid
    {

        private static AndroidCopyFilesAndDirs publicCopyFiles;
        private static AndroidCopyFilesAndDirs internalCopyFiles;

        [PostProcessBuild(101)]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget != BuildTarget.Android) return;
            if (File.Exists(buildPath)) return; //Unity apk build
            string projectPath = buildPath + "/" + Application.productName;
            CheckFilesForCopy(projectPath);
        }

        private static void CheckFilesForCopy(string projectPath)
        {
            CheckAndroidCopyFiles();
            var assetsPath = Application.dataPath;
            
            CheckFilesForCopy(projectPath, assetsPath, publicCopyFiles);
            CheckFilesForCopy(projectPath, assetsPath, internalCopyFiles);
        }

        private static void CheckFilesForCopy(string projectPath, string assetsPath, AndroidCopyFilesAndDirs files)
        {
            var dataArray = files.data;
            foreach(var data in dataArray)
            {
                if (data == null) continue;
                if (string.IsNullOrEmpty(data.from) || string.IsNullOrEmpty(data.to)) continue;

                var fromPath = assetsPath + "/" + data.from;
                var toPath = projectPath + "/" + data.to;
                if (File.Exists(fromPath) || Directory.Exists(fromPath))
                {
                    FileUtil.CopyFileOrDirectory(fromPath, toPath);
                    if (Directory.Exists(toPath)) ClearFromMetaFiles(toPath);
                }
            }
        }

        [MenuItem("BlackBears/Android/Edit files for copy")]
        private static void EditAndroidCopyFiles()
        {
            CheckAndroidCopyFiles();
            Selection.activeObject = publicCopyFiles;
        }

        private static void CheckAndroidCopyFiles()
        {
            CheckAndroidCopyFiles(ref publicCopyFiles, AndroidCopyFilesAndDirs.configPath);
            CheckAndroidCopyFiles(ref internalCopyFiles, AndroidCopyFilesAndDirs.internalConfigPath);
        }

        private static void CheckAndroidCopyFiles(ref AndroidCopyFilesAndDirs copyFiles, string path)
        {
            if (copyFiles) return;
            copyFiles = UnityEditor.AssetDatabase.LoadAssetAtPath<AndroidCopyFilesAndDirs>(path);
            if (copyFiles) return;

            copyFiles = ScriptableObject.CreateInstance<AndroidCopyFilesAndDirs>();
            UnityEditor.AssetDatabase.CreateAsset(copyFiles, path);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

        //TODO: To helper Class
        private static void ClearFromMetaFiles(string path)
        {
            string filesToDelete = @"*.meta";
            string[] fileList = System.IO.Directory.GetFiles(path, filesToDelete, SearchOption.AllDirectories);
            foreach(string file in fileList)
            {
                FileUtil.DeleteFileOrDirectory(file);
            }
        }

    }

}