
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using BB.GameCore.Plugins;
using UnityEditor;
using UnityEngine;

namespace BlackBears.GameCore.Editor
{
    public class TexturePostProcessor : AssetPostprocessor
    {
        private static bool tick = false;
        private static bool ex = false;
        private static BBAtlasChecker neededChecker;
        public override int GetPostprocessOrder() => 100;
        void OnPreprocessTexture()
        {
            var assetPathToFolder = Path.GetDirectoryName(assetPath);
            
            BBImageHelperParameters parameters = UnityEditor.AssetDatabase.LoadAssetAtPath<BBImageHelperParameters>(BBImageHelperParameters.path);
            if (parameters == null) return;
            foreach (var checker in parameters.chekers)
            {
                var pathToFolderCheker = Path.GetDirectoryName(AssetDatabase.GetAssetPath(checker));
                
                if (assetPathToFolder.Contains(pathToFolderCheker))
                {
                    if(ex) return;
                    tick = true;
                    ex = true;
                    EditorApplication.update += DoTimeConsumingStuff;
                    neededChecker = checker;
                    break;
                }
            }
        }

        private void DoTimeConsumingStuff()
        {
            if(tick)
            {
                tick = false;
                return;
            }
            EditorApplication.update -= DoTimeConsumingStuff;
            ex = false;

            neededChecker.CheckFolder();
        }
    }
}