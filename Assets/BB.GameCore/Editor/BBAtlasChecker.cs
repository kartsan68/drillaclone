using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BlackBears.GameCore.Editor
{

    [CreateAssetMenu(fileName = "BBAtlasChecker", menuName = "BlackBears/BB Atlas Checker")]
    public class BBAtlasChecker : ScriptableObject
    {

        [SerializeField] private List<SpritesParams> sprites = new List<SpritesParams>();

        [SerializeField] public string text = "";

        public string Text => text;

        public void OnEnable()
        {

        }
        public void Clear()
        {
            sprites.Clear();
            text = "";
        }

        public void CheckFolder()
        {
            
            // Debug.Log("filesSprites " + (sprites != null) + " " + sprites.Count + " ");
            List<SpritesParams> newFilesSprites = new List<SpritesParams>();
            // Dictionary<string, bool> addedWithName = new Dictionary<string, bool>();

            var pathToSO = AssetDatabase.GetAssetPath(this);

            var folderName = Path.GetDirectoryName(pathToSO);

            string[] fileEntries = Directory.GetFiles(folderName);

            List<string> neededFiles = new List<string>();

            foreach (var item in fileEntries)
            {
                if (item.Contains(".png.meta")) neededFiles.Add(item);
            }


            foreach (var item in neededFiles)
            {
                string[] lines = File.ReadAllLines(item);

                var guid = lines[1].Substring(6, lines[1].Length - 6);

                Dictionary<string, string> names = new Dictionary<string, string>();

                for (int i = 0; i < lines.Length - 15; i++)
                {
                    var start = lines[i].IndexOf(" name:");
                    if (start > 0)
                    {
                        var name = lines[i].Substring(start + 7, lines[i].Length - (start + 7));


                        int startId = lines[i + 15].IndexOf("internalID:");

                        string id = null;
                        if(startId > 0 && lines[i + 15].Length >(startId + 15)) id = lines[i + 15].Substring(startId + 12, lines[i + 15].Length - (startId + 12));

                        names.Add(name, id);

                        i += 14;
                        continue;
                    }
                }

                for (int i = 0; i < lines.Length - 15; i++)
                {
                    int start = lines[i].IndexOf("213:");
                    if (start > 0)
                    {
                        var id = lines[i].Substring(start + 5, lines[i].Length - (start + 5));

                        int startName = lines[i + 1].IndexOf("second:");
                        var name = lines[i + 1].Substring(startName + 8, lines[i + 1].Length - (startName + 8));

                        if(!names.ContainsKey(name)) continue;
                        if(!string.IsNullOrEmpty(names[name])) continue;

                            
                        names[name] = id;

                        i += 2;
                    }
                }

                for (int i = 0; i < lines.Length - 15; i++)
                {
                    int start = lines[i].IndexOf("213:");
                    if (start > 0)
                    {
                        var id = lines[i].Substring(start + 5, lines[i].Length - (start + 5));

                        int startName = lines[i + 1].IndexOf("second:");
                        var name = lines[i + 1].Substring(startName + 8, lines[i + 1].Length - (startName + 8));

                        if(!names.ContainsKey(name)) continue;
                        if(!string.IsNullOrEmpty(names[name])) continue;

                            
                        names[name] = id;

                        i += 2;
                    }
                }

                if(names.Count == 0)
                {
                    var nameFile = Path.GetFileName(item);
                    names.Add(nameFile.Substring(0, nameFile.IndexOf(".png.meta")), "21300000");//.Substring(0, item.Length - item.IndexOf(".png.meta")), "21300000");
                }

                foreach (var name in names)
                {
                    if(string.IsNullOrEmpty(name.Value)) continue;
                   newFilesSprites.Add(new  SpritesParams() {name = name.Key, fileGUID = guid, idInFile = name.Value} ); 
                }
            }

            text = "";
            foreach (var item in newFilesSprites)
            {
                text += item.name + ":\n";
                text += "- file: " + item.fileGUID + "\n- id: " + item.idInFile + "\n";

            }

            Debug.Log(text);

            var replaceStrings = new Dictionary<string, string>();
// sprites.Clear();
            if (sprites != null)
            {
                Debug.Log("Start checks");

                foreach (var lastSprite in sprites)
                {
                    var newSpriteInfo = newFilesSprites.Find(x => x.name.Equals(lastSprite.name));
                    if (newSpriteInfo != null)
                    {
                        if (!lastSprite.fileGUID.Equals(newSpriteInfo.fileGUID)
                            || !lastSprite.idInFile.Equals(newSpriteInfo.idInFile))
                        {
                            var lastString = $"fileID: {lastSprite.idInFile}, guid: {lastSprite.fileGUID},";
                            var newString = $"fileID: {newSpriteInfo.idInFile}, guid: {newSpriteInfo.fileGUID},";

                            // var lastString = $"fileID: {lastSprite.Value[1]}, guid: {lastSprite.Value[0]},";
                            // var newString = $"fileID: {newFilesSprites[lastSprite.Key][1]}, guid: {newFilesSprites[lastSprite.Key][0]},";

                            Debug.Log("Replace: " + lastSprite.name + "\n" + lastString + "\n" + newString);
                            replaceStrings.Add(lastString, newString);
                        }
                    }
                }
            }

            if (replaceStrings.Count > 0)
            {
                List<string> paths = new List<string>();
                paths.AddRange(Directory.EnumerateFiles(Application.dataPath, "*.prefab", SearchOption.AllDirectories));
                paths.AddRange(Directory.EnumerateFiles(Application.dataPath, "*.unity", SearchOption.AllDirectories));
                paths.AddRange(Directory.EnumerateFiles(Application.dataPath, "*.asset", SearchOption.AllDirectories));

                foreach (string filePath in paths)
                {
                    string lines = File.ReadAllText(filePath);

                    foreach (var replaceString in replaceStrings)
                    {
                        lines = lines.Replace(replaceString.Key, replaceString.Value);
                    }

                    File.WriteAllText(filePath, lines);
                }
            }

            sprites.Clear();
            sprites.AddRange(newFilesSprites);

            Debug.Log("Check sprites FINISHED! " + sprites.Count);

            EditorUtility.SetDirty(this);

            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

        }
    }

    [System.Serializable]
    public class SpritesParams
    {
        public string name;
        public string fileGUID;
        public string idInFile;
    }
}