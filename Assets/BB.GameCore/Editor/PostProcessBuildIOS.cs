﻿using System.IO;
using System.Linq;
using BB.GameCore.Plugins;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace BlackBears.GameCore.Editor
{

    public static class PostProcessBuildIOS
    {

        private static iOSQueriesSchemes schemes;

        [PostProcessBuild(101)]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget != BuildTarget.iOS) return;
            PrepareProject(buildPath);
        }

        private static void PrepareProject(string buildPath)
        {
            string projPath = Path.Combine(buildPath, "Unity-iPhone.xcodeproj/project.pbxproj");

            PBXProject project = new PBXProject();
            project.ReadFromString(File.ReadAllText(projPath));
            string target = project.TargetGuidByName("Unity-iPhone");
            project.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
            project.AddBuildProperty(target, "OTHER_LDFLAGS", "-lxml2");

            var bundleIdentifier = PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS);
            var nameEntitlements = $"{bundleIdentifier.Substring(bundleIdentifier.LastIndexOf(".") + 1)}.entitlements";
            var pathToEntitlements = buildPath + "/" + nameEntitlements;
            if (!File.Exists(pathToEntitlements))
            {
                using (StreamWriter writer = File.CreateText(pathToEntitlements))
                {
                    writer.Write(
@"<?xml version=""1.0"" encoding=""UTF-8""?>
<!DOCTYPE plist PUBLIC ""-//Apple//DTD PLIST 1.0//EN"" ""http://www.apple.com/DTDs/PropertyList-1.0.dtd"">
<plist version=""1.0"">
<dict>
</dict>
</plist>");
                    writer.Flush();
                }

                project.AddBuildProperty(target, "CODE_SIGN_ENTITLEMENTS", pathToEntitlements);
            }

            string state;
            using (StreamReader reader = File.OpenText(pathToEntitlements))
            {
                state = reader.ReadToEnd();
            }

            File.Delete(pathToEntitlements);

#if BBGC_SAVE_CLOUD
            state = state.Replace("</dict>", "\t<key>com.apple.developer.icloud-container-identifiers</key>\n\t<array/>\n</dict>");
#endif

            using (StreamWriter writer = File.CreateText(pathToEntitlements))
            {
                writer.Write(state);
                writer.Flush();
            }

            File.WriteAllText(projPath, project.WriteToString());
        }

        [MenuItem("BlackBears/iOS/Edit queries schemes")]
        private static void EditiOSQueriesSchemes()
        {
            CheckSchemes();
            Selection.activeObject = schemes;
        }

        private static void CheckSchemes()
        {
            if (!System.IO.Directory.Exists(Application.dataPath + "/Editor"))
            {
                AssetDatabase.CreateFolder("Assets", "Editor");
            }

            if (schemes) return;
            schemes = UnityEditor.AssetDatabase.LoadAssetAtPath<iOSQueriesSchemes>(iOSQueriesSchemes.configPath);
            if (schemes) return;

            schemes = ScriptableObject.CreateInstance<iOSQueriesSchemes>();
            UnityEditor.AssetDatabase.CreateAsset(schemes, iOSQueriesSchemes.configPath);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

        [PostProcessBuild(102)]
        private static void SetupPlists(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget != BuildTarget.iOS) return;
            CheckSchemes();
            if (PostProcessBuildIOS.schemes == null) return;
            if (PostProcessBuildIOS.schemes.schemes == null || PostProcessBuildIOS.schemes.schemes.Length == 0) return;
            var schemes = PostProcessBuildIOS.schemes.schemes.Distinct().ToArray();
            if (schemes.Length == 0) return;

            string plistPath = buildPath + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            PlistElementDict rootDict = plist.root;
            var buildKey = "LSApplicationQueriesSchemes";
            PlistElementArray schemesArray;
            {
                var plistElement = rootDict[buildKey];
                if (plistElement != null) schemesArray = plistElement.AsArray();
                else schemesArray = rootDict.CreateArray(buildKey);
            }

            var oldSchemes = schemesArray.values.Select(e => e.AsString()).ToList();

            foreach (var scheme in schemes)
            {
                if (oldSchemes.Contains(scheme)) continue;
                schemesArray.values.Add(new PlistElementString(scheme));
            }

            // // Set encryption usage boolean
            // string encryptKey = "ITSAppUsesNonExemptEncryption";
            // rootDict.SetBoolean(encryptKey, false);

            string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
            if (rootDict.values.ContainsKey(exitsOnSuspendKey))
            {
                rootDict.values.Remove(exitsOnSuspendKey);
            }

            File.WriteAllText(plistPath, plist.WriteToString());
        }

    }

}