using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BB.GameCore.Plugins;
using BlackBears.GameCore.Graphic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Editor
{
    public class BBImageHelper
    {
        [MenuItem("BlackBears/BBImage Helper/Create Params", false)]
        public static void CreateParams()
        {
            BBImageHelperParameters parameters;

            parameters = UnityEditor.AssetDatabase.LoadAssetAtPath<BBImageHelperParameters>(BBImageHelperParameters.path);
            if (parameters) return;

            parameters = ScriptableObject.CreateInstance<BBImageHelperParameters>();
            UnityEditor.AssetDatabase.CreateAsset(parameters, BBImageHelperParameters.path);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }
    }
}