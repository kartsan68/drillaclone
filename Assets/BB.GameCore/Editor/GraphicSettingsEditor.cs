using UnityEditor;

namespace BlackBears.GameCore.Graphic.Editor
{

    public class GameResourcesEditor
    {

        [MenuItem("BlackBears/Atlas Cache/Open App Settings", false, 1)]
        public static void OpenAppGraphicSettings() => UICache.OpenAppSettings();

        [MenuItem("BlackBears/Atlas Cache/Switch Cache %#i", false, 2)]
        public static void SwitchCache() => UICache.ShowSpritesInEdit = !UICache.ShowSpritesInEdit;

    }

}