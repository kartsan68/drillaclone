using UnityEditor;
using UnityEngine;

namespace BlackBears.GameCore.Editor
{
    [CustomEditor(typeof(BBAtlasChecker))]
    public class BBAtlasCheckerEditor : UnityEditor.Editor
    {
        private BBAtlasChecker checker;
        public override void OnInspectorGUI()
        {
            checker = (BBAtlasChecker)target;

            if (GUILayout.Button("Check folder", GUILayout.Height(14)))
            {
                checker.CheckFolder();
            }

            // if (GUILayout.Button("Clear info", GUILayout.Height(14)))
            // {
            //     checker.Clear();
            // }

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorStyles.label.wordWrap = true;
            EditorGUILayout.LabelField(checker.Text);

            EditorGUILayout.EndVertical();
        }
    }
}