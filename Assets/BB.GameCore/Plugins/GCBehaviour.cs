﻿using System;
using System.Collections.Generic;
using BlackBears.GameCore.AppEvents;
using BlackBears.Resolutor;
using UnityEngine;

namespace BlackBears.GameCore
{

    public class GCBehaviour : MonoBehaviour, IAppEvents
    {

        [SerializeField] private GCConfig config;
        [SerializeField] private ResourceCache[] waitingCache;

        private List<IAppPauseListener> pauseListeners = new List<IAppPauseListener>();
        private bool pauseListenersValid;
        private AppEventPriorityComparer<IAppPauseListener> pauseComparer = new AppEventPriorityComparer<IAppPauseListener>();

        private List<IAppResumeListener> resumeListeners = new List<IAppResumeListener>();
        private bool resumeListenersValid;
        private AppEventPriorityComparer<IAppResumeListener> resumeComparer = new AppEventPriorityComparer<IAppResumeListener>();

        private List<IAppQuitListener> quitListeners = new List<IAppQuitListener>();
        private AppEventPriorityComparer<IAppQuitListener> quitComparer = new AppEventPriorityComparer<IAppQuitListener>();

        public bool IsCacheReady
        {
            get
            {
                if (waitingCache == null) return true;
                for (int i = 0; i < waitingCache.Length; i++)
                {
                    var cache = waitingCache[i];
                    if (cache == null || cache.Initialized) continue;
                    return false;
                }
                return true;
            }
        }

        public bool IsAllCacheProcessed
        {
            get
            {
                if (waitingCache == null) return true;
                for (int i = 0; i < waitingCache.Length; i++)
                {
                    var cache = waitingCache[i];
                    if (cache == null) continue;
                    if (cache.State == CacheState.Initialising) return false;
                    if (cache.State == CacheState.NotInitiated) return false;
                }
                return true;
            }
        }

        public bool IsHasErrors
        {
            get 
            {
                if (waitingCache == null) return false;
                for (int i = 0; i < waitingCache.Length; i++)
                {
                    var cache = waitingCache[i];
                    if (cache == null) continue;
                    if (cache.State == CacheState.InitializeFailed) return true;
                }
                return false;
            }
        }

        private void Awake()
        {
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            DontDestroyOnLoad(this);
            #endif

            if (!BBGC.IsInternalInitiated) BBGC.InternalInit(config, this);
        }

        internal void LoadCache()
        {
            if (waitingCache == null) return;
            for (int i = 0; i < waitingCache.Length; i++)
            {
                var cache = waitingCache[i];
                if (cache == null) continue;
                if (!cache.Initialized) cache.Initialize();
            }
        }

        internal string TakeErrors()
        {
            if (waitingCache == null) return string.Empty;
            var sb = new System.Text.StringBuilder();
            foreach(var cache in waitingCache)
            {
                if (cache.State != CacheState.InitializeFailed) continue;
                sb.AppendLine(cache.ErrorMessage);
            }
            return sb.ToString();
        }

        bool IAppEvents.AddAppPauseListener(IAppPauseListener listener)
        {
            bool added = AddListenerToList(listener, pauseListeners);
            if (added) pauseListenersValid = false;
            return added;
        }

        bool IAppEvents.RemoveAppPauseListener(IAppPauseListener listener)
        {
            return RemoveListenerFromList(listener, pauseListeners);
        }

        bool IAppEvents.AddAppResumeListener(IAppResumeListener listener)
        {
            bool added = AddListenerToList(listener, resumeListeners);
            if (added) resumeListenersValid = false;
            return added;
        }

        bool IAppEvents.RemoveAppResumeListener(IAppResumeListener listener)
        {
            return RemoveListenerFromList(listener, resumeListeners);
        }

        bool IAppEvents.AddAppQuitListener(IAppQuitListener listener)
        {
            bool added = AddListenerToList(listener, quitListeners);
            if (added) ValidateListIfNeeded(quitListeners, quitComparer, false);
            return added;
        }

        bool IAppEvents.RemoveAppQuitListener(IAppQuitListener listener)
        {
            return RemoveListenerFromList(listener, quitListeners);
        }

        private void OnApplicationPause(bool paused)
        {
            if (paused)
            {
                ValidateListIfNeeded(pauseListeners, pauseComparer, pauseListenersValid);
                pauseListenersValid = true;
                for (int i = 0; i < pauseListeners.Count; i++) pauseListeners[i].OnAppPaused();
            }
            else
            {
                ValidateListIfNeeded(resumeListeners, resumeComparer, resumeListenersValid);
                resumeListenersValid = true;
                for (int i = 0; i < resumeListeners.Count; i++) resumeListeners[i].OnAppResumed();
            }
        }

        private void OnApplicationQuit()
        {
            for (int i = 0; i < quitListeners.Count; i++)
            {
                var listener = quitListeners[i];
                if (listener != null) listener.OnAppQuit();
            }
        }

        private bool AddListenerToList<TListener>(TListener listener, List<TListener> list) where TListener : class
        {
            if (listener == null || list.Contains(listener)) return false;
            list.Add(listener);
            return true;
        }

        private bool RemoveListenerFromList<TListener>(TListener listener, List<TListener> list) where TListener : class
        {
            return list.Remove(listener);
        }

        private void ValidateListIfNeeded<T>(List<T> list, AppEventPriorityComparer<T> comparer, bool listValid) where T : IAppEventPriority
        {
            if (listValid) return;
            list.Sort(comparer);
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i] == null) list.RemoveAt(i);
                else break;
            }
        }

    }

}