using UnityEngine;
using UnityEngine.Events;

namespace BB.GameCore.Plugins
{

    public class KeyboardUtility : MonoBehaviour
    {

        private static KeyboardUtility instance;

        private float keyboardHeight;
        private FloatUnityEvent onKeyboardHeightRatioChanged = new FloatUnityEvent();

#if UNITY_EDITOR
        public float debugHeight;
#elif UNITY_ANDROID
        private AndroidJavaClass unityClass;
        private AndroidJavaObject activity;
        private AndroidJavaObject unityView;
        private AndroidJavaObject rect;
        private AndroidJavaObject decorView;
#endif

        public static float HeightRatio { get { return instance != null ? instance.keyboardHeight / Screen.height : 0f; } }
        public static FloatUnityEvent OnKeyboardHeightRatioChanged { get { return instance.onKeyboardHeightRatioChanged; } }

        private void Start()
        {
            if (instance != null && instance != this)
            {
                Destroy(this);
                return;
            }
            instance = this;
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            DontDestroyOnLoad(this);
            #endif

#if !UNITY_EDITOR && UNITY_ANDROID
            unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            activity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
            unityView = activity.Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
            rect  = new AndroidJavaObject("android.graphics.Rect");
#endif
        }

        private void OnDestroy()
        {
            if (instance == this) instance = null;
#if !UNITY_EDITOR && UNITY_ANDROID
            if (unityClass != null) unityClass.Dispose();
            if (activity != null) activity.Dispose();
            if (unityView != null) unityView.Dispose();
            if (rect != null) rect.Dispose();
            unityClass = null;
            activity = null;
            unityView = null;
            rect = null;
#endif
        }

        private void Update()
        {
            float height = 0f;
#if UNITY_EDITOR
            height = debugHeight;
#else
            if (TouchScreenKeyboard.visible)
            {
#if UNITY_ANDROID
                if (decorView == null)
                {
                    var b = unityView.Get<AndroidJavaObject>("b");
                    if (b != null)
                    {
                        decorView = b.Call<AndroidJavaObject>("getWindow")
                            .Call<AndroidJavaObject>("getDecorView");
                    }
                }
                unityView.Call("getWindowVisibleDisplayFrame", rect);
                height = (float)(Screen.height - rect.Call<int>("height"));
                if (decorView != null) height += decorView.Call<int>("getHeight");
#elif UNITY_IOS
                height = TouchScreenKeyboard.area.height;
#endif
            }
            else
            {
#if UNITY_ANDROID
                if (decorView != null) 
                {
                    decorView.Dispose();
                    decorView = null;
                }
#endif
            }
#endif

            if (!Mathf.Approximately(keyboardHeight, height))
            {
                keyboardHeight = height;
                onKeyboardHeightRatioChanged.Invoke(HeightRatio);
            }
            else
            {
                keyboardHeight = height;
            }
        }

    }

}