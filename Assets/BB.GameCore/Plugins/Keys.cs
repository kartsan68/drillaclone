namespace BlackBears
{
    internal class Keys
    {

        internal const string safeAreaCoreDataKey = "core";
        internal const string safeAreaGameDataKey = "game";
        internal const string dropSystemTagKey = safeAreaCoreDataKey;

    }
}