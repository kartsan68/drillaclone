﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.GameCore.Features.Notifications
{

    [RequireComponent(typeof(RectTransform), typeof(CanvasGroup))]
    public class NotificationAnimator : MonoBehaviour
    {

        private UnityEvent onHide = new UnityEvent();
        private RectTransform _rectTransform;
        private CanvasGroup _canvasGroup;

        private RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }

        private CanvasGroup CanvasGroup { get { return _canvasGroup ?? (_canvasGroup = GetComponent<CanvasGroup>()); } }

        public bool Animating { get; private set; }

        public UnityEvent OnHide { get { return onHide; } }

        private void Start()
        {
            CanvasGroup.alpha = 0f;
            CanvasGroup.blocksRaycasts = false;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(gameObject);
        }

        internal void Animate()
        {
            if (Animating) return;
            Animating = true;
            CanvasGroup.alpha = 1f;
            CanvasGroup.blocksRaycasts = true;

            LeanTween.moveAnchored(RectTransform, Vector2.zero, 0.2f).setEaseOutQuad();
            LeanTween.delayedCall(gameObject, 2f, () => AnimateHide(false));
        }

        internal void FinishAnimateForced()
        {
            LeanTween.cancel(gameObject);
            AnimateHide(true);
        }

        private void AnimateHide(bool instant)
        {
            var hidePos = new Vector2(RectTransform.anchoredPosition.x, RectTransform.rect.height + 14f);
            LeanTween.moveAnchored(RectTransform, hidePos, 0.2f)
                .setEaseOutQuad()
                .setOnComplete(DisableCanvas);
            LeanTween.delayedCall(gameObject, (instant) ? 0.2f : 0.4f, FinishAnimate);
        }

        private void DisableCanvas()
        {
            CanvasGroup.alpha = 0f;
            CanvasGroup.blocksRaycasts = false;
        }

        private void FinishAnimate()
        {
            Animating = false;
            onHide.Invoke();
        }

    }

}