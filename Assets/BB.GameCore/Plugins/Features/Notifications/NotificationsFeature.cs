﻿using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.Notifications
{

    public sealed class NotificationsFeature : Feature
    {

        private LinkedList<Notification> notifications = new LinkedList<Notification>();
        private NotificationController controller;

        internal NotificationsFeature() : base((int)FeaturePriority.Notifications, "Notifications Feature") { }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            Initialized();
            onSuccess.SafeInvoke();
        }

        public void Inject(NotificationController controller)
        {
            if (this.controller != null)
            {
                this.controller.OnFree.RemoveListener(NotificationShowFinished);
            }

            this.controller = controller;
            if (controller == null) return;
            this.controller.OnFree.AddListener(NotificationShowFinished);
            CheckNotifications();
        }

        public void ShowNotification(Notification notification)
        {
            if (notifications.Count > 0)
            {
                if (notifications.Count == 1 && controller != null && controller.IsBusy)
                {
                    if (notifications.First.Value.Equals(notification)) controller.ForceFinishNotification();
                }
                else if (notifications.Count > 0 && notifications.Last.Value.Equals(notification))
                {
                    notifications.RemoveLast();
                }
            }

            notifications.AddLast(notification);

            CheckNotifications();
        }

        private void CheckNotifications()
        {
            if (notifications.Count == 0 || controller == null || controller.IsBusy) return;

            controller.ShowNotification(notifications.First.Value);
        }

        private void NotificationShowFinished()
        {
            if (notifications.Count == 0) return;

            notifications.RemoveFirst();
            CheckNotifications();
        }

    }

}