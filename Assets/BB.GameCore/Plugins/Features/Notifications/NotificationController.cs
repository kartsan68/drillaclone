using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Notifications
{

    [RequireComponent(typeof(NotificationStyler), typeof(NotificationAnimator))]
    public class NotificationController : MonoBehaviour
    {

        [SerializeField] private Text text;
        [SerializeField] private GameObject background;

        private UnityEvent onFree = new UnityEvent();
        private NotificationStyler _styler;
        private NotificationAnimator _animator;

        internal bool IsBusy { get { return Animator.Animating; } }

        public UnityEvent OnFree { get { return onFree; } }

        private NotificationStyler Styler
        {
            get { return _styler ?? (_styler = GetComponent<NotificationStyler>()); }
        }

        private NotificationAnimator Animator
        {
            get { return _animator ?? (_animator = GetComponent<NotificationAnimator>()); }
        }

        private void Start()
        {
            Animator.OnHide.AddListener(onFree.Invoke);
        }

        internal void ShowNotification(Notification notification)
        {
            if (IsBusy) return;
            background.SetActive(true);
            Styler.Type = notification.type;
            text.text = notification.message;

            Animator.Animate();
        }

        internal void ForceFinishNotification()
        {
            if(IsBusy) Animator.FinishAnimateForced();
        }

    }

}