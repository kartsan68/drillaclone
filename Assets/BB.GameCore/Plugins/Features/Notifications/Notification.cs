namespace BlackBears.GameCore.Features.Notifications
{

    public enum NotificationType
    {
        Positive,
        Warning,
        Information
    }

    public struct Notification
    {

        public readonly NotificationType type;
        public readonly string message;

        public Notification(NotificationType type, string message)
        {
            this.type = type;
            this.message = message;
        }

        public bool Equals(Notification other) { return this.type == other.type && string.Equals(this.message, other.message); }

    }

}