﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.GameCore.Graphic;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Notifications
{

	public class NotificationStyler : MonoBehaviour 
	{

		[SerializeField] private Image icon;
		[SerializeField] private Image decoration;

		[Header("Positive style")]
		[SerializeField] private Color positiveColor = ColorUtils.FromHex(0x59CC76FF);
		[SerializeField] private string positiveIconName;

		[Header("Warning style")]
		[SerializeField] private Color warningColor = ColorUtils.FromHex(0xE64B3CFF);
		[SerializeField] private string warningIconName;

		[Header("Info style")]
		[SerializeField] private Color infoColor = ColorUtils.FromHex(0x2196EFFF);
		[SerializeField] private string infoIconName;

		private NotificationType type = NotificationType.Positive;

		public NotificationType Type
		{
			get { return type; }
			set
			{
				if (value == type) return;
				type = value;
				UpdateType();
			}
		}

		private void Start()
		{
			UpdateType();
		}

		private void UpdateType()
		{
			var atlas = UICache.Instance.TakeAtlas(CoreCache.key);
			if (atlas == null) return;

			switch (type)
			{
				case NotificationType.Positive:
					icon.sprite = atlas[positiveIconName];
					decoration.color = positiveColor;
					break;

				case NotificationType.Warning:
					icon.sprite = atlas[warningIconName];
					decoration.color = warningColor;
					break;
				
				case NotificationType.Information:
					icon.sprite = atlas[infoIconName];
					decoration.color = infoColor;
					break;
			}
		}

	}

}