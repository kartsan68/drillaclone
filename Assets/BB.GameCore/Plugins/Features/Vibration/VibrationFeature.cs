using BlackBears.GameCore.AppEvents;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Vibration
{
    public class VibrationFeature : Feature, ICoreSaveable
    {
        private const string enableKey = "enbl";

        public bool Enable { get; set; } = true;

        string ICoreSaveable.SaveKey => "vbr";

        internal VibrationFeature()
            : base((int)FeaturePriority.Vibration, "Vibration Feature")
        {

        }

        // Слабая вибрация мультиплатформа
        public void SimpleVibrate(long time = 120)
        {
            if (Vibration.HasVibrator() && Enable)
            {
#if UNITY_ANDROID && !UNITY_EDITOR
				Vibration.Vibrate(time);

#elif UNITY_IOS && !UNITY_EDITOR
				Vibration.VibratePop();
#else
                Vibration.Vibrate();
#endif
            }
        }

        // Средняя вибрация мультиплатформа
        public void MediumVibrate()
        {
#if UNITY_IOS && !UNITY_EDITOR
            SimpleVibrate();
#else
            SimpleVibrate(210);
#endif
        }

        // Сильная вибрация мультиплатформа
        public void HighVibrate()
        {
#if UNITY_IOS && !UNITY_EDITOR
            VibratePeek();
#else
            SimpleVibrate(300);
#endif
        }

        // Серия из вибраций мультиплатформа
        public void FewVibrates()
        {
#if UNITY_IOS && !UNITY_EDITOR
            VibrateNope();
#elif UNITY_ANDROID && !UNITY_EDITOR
            Vibrate(new long[] { 0, 120, 60, 120, 60, 120 }, -1);
#else
            SimpleVibrate();
#endif
        }

#if UNITY_IOS && !UNITY_EDITOR
        //Слабая вибрация
        public void VibratePop()
        {
            if(Vibration.HasVibrator() && Enable) Vibration.VibratePop();
        }

        //Сильная вибрация
        public void VibratePeek()
        {
            if(Vibration.HasVibrator() && Enable) Vibration.VibratePeek();
        }

        //Серия из трех слабых вибраций
        public void VibrateNope()
        {
            if(Vibration.HasVibrator() && Enable) Vibration.VibrateNope();
        }
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        //Вибрировать определенное кол-во времени
        public void Vibrate(long milliseconds)
        {
            if(Vibration.HasVibrator() && Enable) Vibration.Vibrate(milliseconds);
        }

        //Вибрировать по определенному паттрену и с определенным кол-вом раз
        public void Vibrate(long[] pattern, int repeat = 0)
        {
            if(Vibration.HasVibrator() && Enable) Vibration.Vibrate(pattern, repeat);
        }

        //Прервать вибрацию
        public void Cancel()
        {
            if(Vibration.HasVibrator() && Enable) Vibration.Cancel();
        }
#endif

        JSONNode ICoreSaveable.GenerateSaveData()
        {
            var json = new JSONObject();
            json[enableKey] = Enable;
            return json;
        }

        void ICoreSaveable.LoadDataNotFinded() { }

        void ICoreSaveable.PrepareFromLoad(JSONNode saveData)
        {
            if (saveData == null || saveData.Tag == JSONNodeType.None) return;

            Enable = saveData[enableKey].AsBool;
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            Initialized();
            onSuccess.SafeInvoke();
        }
    }
}