using System;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Rate
{

    internal class RateState
    {
        
        private const string rateDateKey = "rtdt";
        private const string lateDateKey = "ltdt";

        internal bool gameConnected = false;
        internal long rateDate = -1;
        internal long lateDate = -1;

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[rateDateKey] = rateDate;
            json[lateDateKey] = lateDate;
            return json;
        }

        internal void ParseJson(JSONNode json)
        {
            if (json == null) return;
            rateDate = json[rateDateKey].AsLong;
            lateDate = json[lateDateKey].AsLong;
        }

    }

}