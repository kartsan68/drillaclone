using System.Collections.Generic;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;

namespace BlackBears.GameCore.Features.Rate.Analytics
{

    internal static class AnalyticsExtension
    {
        internal static void OnRate(this IAnalyticsAdapter adapter, int starsCount = -1)
        {
            var additionalProps = new AdditionalProps(day: true, currentSession: true);
            var data = new Dictionary<string, object>();
            data[AnalyticsKeys.starsCountKey] = starsCount != -1 ? starsCount.ToString() : AnalyticsKeys.laterKey;
            adapter.PostEvent(AnalyticsKeys.rateKey, data, additionalProps);
        }
    }
}