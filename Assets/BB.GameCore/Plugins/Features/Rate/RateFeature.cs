﻿using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.GameCore.Features.Save;
using BlackBears.GameCore.Features.SQBA;
using BlackBears.GameCore.Features.Rate.Analytics;
using BlackBears.Utils;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Rate
{

    public sealed class RateFeature : Feature, ICoreSaveable
    {

        private const string saveKey = "rt";

        #pragma warning disable 0414, 0649
        private SQBAFeature sqba;
        private ChronometerFeature chronometer;
        private CoreDefaultsFeature core;
        private SaveFeature saveFeature;
        #pragma warning restore 0414, 0649

        private RateFeatureConfig config;
        private RateState state;

        private IAnalyticsAdapter analytics;


        string ICoreSaveable.SaveKey { get { return saveKey; } }

        private bool IsRatingEnabledOnSQBA 
        {
            get
            {
                #if BBGC_SQBA_FEATURE
                return sqba != null && sqba.RatingEnabled;
                #else
                return false;
                #endif
            }
        }

        internal RateFeature(RateFeatureConfig config) : base((int)FeaturePriority.RateFeature, "Rate Feature")
        {
            this.config = config;
            state = new RateState();
        }

        public void TryToOpenRate()
        {
            if (!state.gameConnected) return;
            if (!IsRatingEnabledOnSQBA) return;

            if (chronometer.SessionTime < config.RateMinSessionTime 
                || chronometer.OnlineTime < config.RateMinOnlineTime 
                || chronometer.AfterResumeTime < config.RateMinSessionTime
                || core.SessionsCount < config.RateMinSessionsCount)
            {
                return;
            }

            bool mustShow = false;
            if (state.lateDate >= 0)
            {
                var lateSpan = chronometer.ActualTime - state.lateDate;
                mustShow = lateSpan > config.LateMinSpan;
            }
            if (!mustShow)
            {
                mustShow = state.lateDate < 0 && state.rateDate < 0;
            }

            if (mustShow)
            {
                var alerts = BBGC.Features.Alerts();
                alerts.AddAlert(new RateAlert(OnRateApp, OnRatePostpone, config.ShowOverlayOnAlerts));
            }
        }

        void ICoreSaveable.PrepareFromLoad(JSONNode saveData)
        {
            state.ParseJson(saveData);
            CheckForRateReset();
        }

        void ICoreSaveable.LoadDataNotFinded() {}

        JSONNode ICoreSaveable.GenerateSaveData() { return state.ToJson(); }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            #if BBGC_SQBA_FEATURE
            sqba = BBGC.Features.SQBA();
            chronometer = BBGC.Features.Chronometer();
            core = BBGC.Features.CoreDefaults();
            saveFeature = BBGC.Features.GetFeature<SaveFeature>();

            core.OnGameConnectedToCore += OnGameConnected;
            #endif

            Initialized();
            onSuccess.SafeInvoke();
        }

        private void OnGameConnected() 
        {
            var core = BBGC.Features.CoreDefaults();
            analytics = core.AnalyticsAdapter;

            state.gameConnected = true; 
        }

        private void CheckForRateReset()
        {
            #if BBGC_SQBA_FEATURE
            if (state.rateDate < 0) return;
            if (state.rateDate < sqba.DayXTimestamp && state.lateDate < 0)
            {
                state.rateDate = -1;
                state.lateDate = chronometer.ActualTime;
            }
            #endif
        }

        private void OnRateApp(int starsCount)
        {
            if (starsCount < config.OpenRateMinStars)
            {
                var alerts = BBGC.Features.Alerts();
                alerts.AddAlert(new BadRateAlert(config.ShowOverlayOnAlerts));
            }
            else
            {
                NativeUtils.OpenRate(BBGC.Features.CoreDefaults().RateData);
            }
            state.rateDate = chronometer.ActualTime;
            state.lateDate = -1;
            analytics.OnRate(starsCount);
            saveFeature.Save();
        }

        private void OnRatePostpone()
        {
            state.rateDate = -1;
            state.lateDate = chronometer.ActualTime;
            analytics.OnRate();
            saveFeature.Save();
        }

    }

}