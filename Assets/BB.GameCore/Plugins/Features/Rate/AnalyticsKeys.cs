namespace BlackBears.GameCore.Features.Rate.Analytics
{

    internal static class AnalyticsKeys
    {
        
        internal const string starsCountKey = "оценка";
        internal const string laterKey = "позже";
        internal const string rateKey = "рейтилка";

    }

}