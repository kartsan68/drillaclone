namespace BlackBears.GameCore.Features.Rate
{

    [System.Serializable]
    public class RateFeatureConfig
    {

        [UnityEngine.SerializeField] private int openRateMinStars = 4;
        [UnityEngine.SerializeField] private long rateMinSessionTime = 20;
        [UnityEngine.SerializeField] private long rateMinOnlineTime = 20 * 60;
        [UnityEngine.SerializeField] private long lateMinSpan = 5 * 24 * 60 * 60;
        [UnityEngine.SerializeField] private long rateMinSessionsCount = 3;
        
        [UnityEngine.Space]
        [UnityEngine.SerializeField] private bool showOverlayOnAlerts;
        
        internal int OpenRateMinStars => openRateMinStars;
        internal long RateMinSessionTime => rateMinSessionTime;
        internal long RateMinOnlineTime => rateMinOnlineTime;
        internal long LateMinSpan => lateMinSpan;
        internal long RateMinSessionsCount => rateMinSessionsCount;
        internal bool ShowOverlayOnAlerts => showOverlayOnAlerts;
        
    }

}