#if BBGC_CLANS_FEATURE

using BlackBears.Clans;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Clans
{

    public sealed class ClansFeature : Feature, ICoreSaveable
    {

        private const string saveKey = "clns";

        private ClansFeatureConfig config;

        internal ClansFeature(ClansFeatureConfig config) 
            : base((int)FeaturePriority.Clans, "Clans Feature")
        {
            this.config = config;
        }

        public ClanCore Core { get; private set; }

        string ICoreSaveable.SaveKey { get { return saveKey; } }

        JSONNode ICoreSaveable.GenerateSaveData() { return Core.ToJson(); }
        void ICoreSaveable.PrepareFromLoad(JSONNode saveData) { Core.InitState(saveData); }
        void ICoreSaveable.LoadDataNotFinded() {}

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var files = BBGC.Features.SQBAFiles();
            var coreDefault = BBGC.Features.CoreDefaults();

            string clanParamsText = files.TextByName(config.ParamsFileName);
            var clanParams = new ClanParams(SimpleJSON.JSON.Parse(clanParamsText));

            var settings = new ClanFullSettings(
                config.Configuration,
                clanParams,
                coreDefault.SoftCurrencyIconPack, 
                coreDefault.HardCurrencyIconPack,
                coreDefault.ScoreIconPack,
                coreDefault.SubscriptionSprites
            );
            Core = ClanCore.Init(settings);

            coreDefault.OnGameConnectedToCore += OnGameConnected;

            initData.onClansFeatureActivated.SafeInvoke();
            Initialized();
            onSuccess.SafeInvoke();
        }

        private void OnGameConnected()
        {
            var core = BBGC.Features.CoreDefaults();
            Core.ConnectWithGame(core.GameAdapter, core.AnalyticsAdapter);
        }

    }

}

#endif