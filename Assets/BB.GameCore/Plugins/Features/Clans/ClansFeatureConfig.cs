namespace BlackBears.GameCore.Features.Clans
{

    [System.Serializable]
    public class ClansFeatureConfig : FeatureConfig
    {
        #if BBGC_CLANS_FEATURE

        [UnityEngine.SerializeField] private BlackBears.Clans.ClansConfiguration configuration;
        [UnityEngine.SerializeField] private string paramsFileName;

        internal BlackBears.Clans.ClansConfiguration Configuration { get { return configuration; } }
        internal string ParamsFileName { get { return paramsFileName; } }

        #endif

    }

}