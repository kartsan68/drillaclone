﻿#if BBGC_LEAGUES_FEATURE

using SimpleJSON;
using BlackBears.GameServer;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.GameServer;
using UnityEngine.Events;
using BlackBears.Utils;

namespace BlackBears.GameCore.Features.Leagues
{

    public sealed class LeaguesFeature : Feature, ISaveable
    {

        private const string saveKey = "lgs";

        private FeatureConfig config;
        private LeagueData data;
        private ChronometerFeature chronometer;

        private State state;

        private IGameAdapter gameAdapter;

        internal LeaguesFeature(FeatureConfig config)
            : base((int)FeaturePriority.Leagues, "Leagues Feature")
        {
            this.config = config;

            state = new State();
        }

        internal IState State => state;

        internal bool IsActive { get { return config.Enabled && data.enabled; } }
        internal bool CanTakeReward
        {
            get
            {
                if (errorRecieved) return false;
                if (State.OldRewardTime == State.RewardTime) return false;
                var actualTime = chronometer.ActualTime;
                if (State.OldRewardTime != 0) return (State.OldRewardTime - actualTime) <= 0;
                else return (State.RewardTime - actualTime) <= 0;
            }
        }

        private bool IsSynchronized
        {
            get
            {
                var actualTime = chronometer.ActualTime;
                return (actualTime - state.lastUpdateTime) <= data.invalidateInterval;
            }
        }

        private bool errorRecieved;

        private UnityEvent onTeleportShow = new UnityEvent();

        public UnityEvent OnTeleportShow => onTeleportShow;

        string ISaveable.SaveKey { get { return saveKey; } }

        JSONNode ISaveable.GenerateSaveData() { return state.ToJson(); }
        void ISaveable.PrepareFromLoad(JSONNode saveData) { state.FromJson(saveData); }
        void ISaveable.LoadDataNotFinded() { }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var coreDefault = BBGC.Features.CoreDefaults();
            data = coreDefault.LeagueData;

            chronometer = BBGC.Features.Chronometer();

            Initialized();
            onSuccess.SafeInvoke();
        }

        public void Synchronize(Block onSuccess, FailBlock onFail)
        {
            if (gameAdapter == null) gameAdapter = BBGC.Features.Clans().Core.State.Game;

            // if (IsSynchronized)
            // {
            //     onSuccess.SafeInvoke();
            //     return;
            // }

            var defaults = BBGC.Features.CoreDefaults();
            var connection = BBGC.Features.GetFeature<GameServerFeature>().Connection;

            if (!State.IsLeaguesActivated)
            {
                connection.ActivateLeagues(string.Equals(defaults.PlayerId, string.Empty) ? gameAdapter.PlayerId : defaults.PlayerId, gameAdapter.Score, gameAdapter.PlayerKey, rn =>
                {
                    Logger.Log(name, $"Activate Success: {rn.success}");
                    state.isLeaguesActivated = true;

                    connection.GetLeague(defaults.PlayerId, r =>
                    {
                        var actualTime = chronometer.ActualTime;
                        OnLeagueLoadFinished(r, actualTime);
                        onSuccess.SafeInvoke();
                    },
                    (c, m) =>
                    {
                        Logger.Log(name, $"Synchronize: {c}; {m}");
                        onFail.SafeInvoke(c, m);
                    });
                },
                (c, m) =>
                {
                    Logger.Log(name, $"Activate Fails: {c}; {m}");
                    onFail.SafeInvoke(c, m);
                    return;
                });
            }
            else
            {
                connection.GetLeague(defaults.PlayerId, r =>
                {
                    var actualTime = chronometer.ActualTime;
                    OnLeagueLoadFinished(r, actualTime);
                    onSuccess.SafeInvoke();
                },
                (c, m) =>
                {
                    Logger.Log(name, $"Synchronize: {c}; {m}");
                    onFail.SafeInvoke(c, m);
                });
            }
        }

        internal void TakeReward()
        {
            if (!CanTakeReward) return;

            if (gameAdapter == null) gameAdapter = BBGC.Features.Clans().Core.State.Game;

            int curType = state.leagueType;

            gameAdapter.LeagueTakeReward(curType);

            OnTeleportShow.Invoke();

            state.oldRewardTime = 0;
            state.rewardTakenCount += 1;
        }

        private void OnLeagueLoadFinished(LeagueResponse response, long actualTime)
        {
            state.playersList = response.PlayersList;
            state.leagueNumber = response.LeagueNum;
            state.leagueType = response.LeagueType;
            state.flagsDelta = response.FlagsDelta;
            state.leagueBoundaries = response.LeagueBoundaries;
            state.lastUpdateTime = actualTime;
            if (response.NextRecalcTime != -1)
            {
                errorRecieved = false;
                if (state.rewardTime == 0)
                {
                    state.rewardTime = response.NextRecalcTime;
                }
                else
                {
                    if (CanTakeReward && state.oldRewardTime == 0) state.oldRewardTime = state.rewardTime;
                    state.rewardTime = response.NextRecalcTime;
                }
            }
            else
            {
                errorRecieved = true;
            }

            if (state.leagueType == 0) // Если 0, это значит что лига фейковая (Игрок еще не участвовал в пересчете сервера)
            {
                ServerPlayer localPlayer = new ServerPlayer();
                localPlayer.Id = gameAdapter.PlayerId;
                localPlayer.Icon = gameAdapter.Icon;
                localPlayer.Nick = (gameAdapter.Nickname == null || gameAdapter.Nickname.Length == 0) ? null : gameAdapter.Nickname;
                localPlayer.Country = gameAdapter.Country;
                localPlayer.Icon = gameAdapter.Icon;
                localPlayer.Score = gameAdapter.Score;
                localPlayer.League = gameAdapter.League;
                state.playersList.Add(localPlayer);

                foreach (var player in data.fakePlayers)
                {
                    state.playersList.Add(player);
                }
            }
            state.playersList.Sort((x, y) => -x.Score.CompareTo(y.Score));

            // if (state.rewardTime == 0 && state.rewardTakenCount == 0)
            // {
            //     state.rewardTime = chronometer.ActualTime + data.firstRewardDelay;
            // }
        }

    }

}
#endif