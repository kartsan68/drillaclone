using System;
using System.IO;
using UnityEngine;

namespace BlackBears.GameCore.Features.SQBAFiles
{

    [System.Serializable]
    public class SQBAFilesFeatureConfig : FeatureConfig
    {

        [SerializeField] private TextAsset[] defaultAssets;

        [SerializeField] private string nameOfFolderForEncryptionFiles = "_SQBAEnycryptFiles";

        public TextAsset[] DefaultAssets => defaultAssets;
        public string NameOfFolderForEncryptionFiles => nameOfFolderForEncryptionFiles;

        internal TextAsset AssetByName(string name)
        {
            foreach (var asset in defaultAssets)
            {
                if (asset == null || !asset.name.Equals(name)) continue;
                return asset;
            }
            return null;
        }

    }

}