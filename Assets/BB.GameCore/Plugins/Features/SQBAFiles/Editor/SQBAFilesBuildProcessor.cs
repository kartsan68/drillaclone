#if BBGC_SQBA_FILES_FEATURE
using System;
using System.IO;
using BlackBears.SQBA.Modules.Files;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace BlackBears.GameCore.Features.SQBAFiles
{
    [Obsolete]
    internal class SQBAFilesBuildProcessor : IPreprocessBuild
    {
        public int callbackOrder { get { return 0; } }

        public void OnPreprocessBuild(BuildTarget target, string path)
        {
            EncryptSQBAFiles();
        }

        public static void EncryptSQBAFiles()
        {
            UnityEngine.Debug.Log("EncryptSQBAFiles");

            var pathToConfig = GetMonoScriptPathFor(typeof(GCConfig));
            var cGCConfig = AssetDatabase.LoadAssetAtPath<GCConfig>(pathToConfig);

            var pathToSQBAConfig = GetMonoScriptPathFor(typeof(BlackBears.SQBA.Configuration));
            var SQBAConfiguration = AssetDatabase.LoadAssetAtPath<BlackBears.SQBA.Configuration>(pathToSQBAConfig);

            if (cGCConfig == null || SQBAConfiguration == null)
            {
                UnityEngine.Debug.LogError("Error SQBAFilesFeatureConfig/SQBA.Configuration not found");
                return;
            }
            
            var SQBAFilesFeatureConfig = cGCConfig.SQBAFiles; 

            if (!SQBAConfiguration.EncryptionFiles) return;

            if (!Directory.Exists($"{Application.dataPath}/Resources")) Directory.CreateDirectory($"{Application.dataPath}/Resources");

            var pathToDir = $"{Application.dataPath}/Resources/{SQBAFilesFeatureConfig.NameOfFolderForEncryptionFiles}";

            if (Directory.Exists(pathToDir)) Directory.Delete(pathToDir, true);
            Directory.CreateDirectory(pathToDir);

            foreach (var asset in SQBAFilesFeatureConfig.DefaultAssets)
            {
                var newPath = $"{pathToDir}/{asset.name}.txt";
                FilesModule.EncryptFile(AssetDatabase.GetAssetPath(asset), null, null, newPath);
                EditorUtility.SetDirty(asset);
            }
            
            AssetDatabase.Refresh();
        }

        private static string GetMonoScriptPathFor(Type type)

        {
            var asset = "";
            var guids = AssetDatabase.FindAssets(string.Format("t:{0}", type.FullName));
            if (guids.Length > 1)
            {
                foreach (var guid in guids)
                {
                    var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                    var filename = Path.GetFileNameWithoutExtension(assetPath);
                    if (filename == type.Name)
                    {
                        asset = guid;
                        break;
                    }
                }
            }
            else if (guids.Length == 1)
            {
                asset = guids[0];
            }
            else
            {
                Debug.LogErrorFormat("Unable to locate {0}", type.FullName);
                return null;
            }
            return AssetDatabase.GUIDToAssetPath(asset);

        }
    }
}
#endif