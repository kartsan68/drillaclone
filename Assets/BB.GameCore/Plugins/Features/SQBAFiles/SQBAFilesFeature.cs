using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Features.SQBAFiles
{

    public sealed class SQBAFilesFeature : Feature
    {

        private SQBAFilesFeatureConfig config;

        internal SQBAFilesFeature(SQBAFilesFeatureConfig config)
            : base((int)FeaturePriority.SQBAFiles, "SQBAFiles Feature")
        {
            this.config = config;
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            Initialized();
            onSuccess.SafeInvoke();
        }

        public string GetFileFromSQBA(string filename)
        {
#if BBGC_SQBA_FILES_FEATURE
            return BlackBears.SQBA.SQBA.GetFile(filename);
#else
            return null;
#endif
        }

        public string TextByName(string filename)
        {
            var file = GetFileFromSQBA(filename);
            if (file != null)
            {
                return file;
            }
            else
            {
#if BBGC_SQBA_FILES_FEATURE
                if (BlackBears.SQBA.SQBA.EncryptionFiles
#if UNITY_EDITOR
                    && false
#endif
                    )
                {
                    var nameWithouExt = Path.GetFileNameWithoutExtension(filename);
                    var path = $"{config.NameOfFolderForEncryptionFiles}/{nameWithouExt}";

                    var textAsset = UnityEngine.Resources.Load<UnityEngine.TextAsset>(path);

                    var text = BlackBears.SQBA.SQBA.DecodeFile(textAsset.text);

                    return text;
                }
                else
#endif
                {
                    var nameWithouExt = Path.GetFileNameWithoutExtension(filename);
                    var asset = config.AssetByName(nameWithouExt);
                    return asset != null ? asset.text : string.Empty;
                }
            }
        }

        public byte[] RawByName(string filename)
        {
#if BBGC_SQBA_FILES_FEATURE
            var file = BlackBears.SQBA.SQBA.GetFile(filename);
            if (file == null) return null;
            else return Encoding.ASCII.GetBytes(file);
#else
            return null;
#endif

        }

        public T GetAssetFromBundleByName<T>(string bundleName, string fileName) where T : UnityEngine.Object
        {
#if UNITY_EDITOR
            string[] assets = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);

            for (int i = 0; i < assets.Length; i++)
            {

                if (assets[i].Contains(fileName))
                {
                    return UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assets[i]);
                }
            }
#else
            string path;
#if BBGC_SQBA_FILES_FEATURE
            path =  BlackBears.SQBA.SQBA.GetFilePath(bundleName);
#endif
            if(path == null) path = Path.Combine(Application.streamingAssetsPath, bundleName);

           
            var bundle = AssetBundle.LoadFromFile(path);
            if (bundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                return null;
            }

            string[] assets = bundle.GetAllAssetNames();

            for (int i = 0; i < assets.Length; i++)
            {
                if (assets[i].Contains(fileName.ToLower()))
                {
                    T asset = bundle.LoadAsset<T>(assets[i]);
                    bundle.Unload(false);
                    return asset;
                }
            }    
#endif

            return null;
        }

    }

}