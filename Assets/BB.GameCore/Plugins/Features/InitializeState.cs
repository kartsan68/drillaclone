namespace BlackBears.GameCore.Features
{

    public enum InitializeState
    {
        NotInitialized,
        Initializing,
        Initialized
    }

}