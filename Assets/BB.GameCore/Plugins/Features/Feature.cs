using System;
using BlackBears.GameCore.Exceptions;

namespace BlackBears.GameCore.Features
{

    public abstract class Feature : IComparable<Feature>
    {

        public readonly int priority;
        public readonly string name;

        protected InitializeState InitState { get; private set; }

        internal Feature(int priority, string name)
        {
            this.priority = priority;
            this.name = name;
        }

        public int CompareTo(Feature other)
        {
            if (other == null) return -1;
            return this.priority - other.priority;
        }

        internal void Initialize(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            switch(InitState)
            {
                case InitializeState.NotInitialized:
                    InitState = InitializeState.Initializing;
                    InitProcess(initData, onSuccess, onFail);
                    break;
                case InitializeState.Initializing: break;
                case InitializeState.Initialized:
                    onSuccess.SafeInvoke();
                    break;                
            }
        }

        protected void Initialized()
        {
            if (InitState != InitializeState.Initializing) return;
            InitState = InitializeState.Initialized;
            Logger.Log(name, "Initialized");
        }

        protected void NotInitialized()
        {
            if (InitState != InitializeState.Initializing) return;
            InitState = InitializeState.NotInitialized;
            Logger.Log(name, "Not initialized");
        }

        protected virtual void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            throw new FeatureNotConnectedException(name);
        }
        
    }

}