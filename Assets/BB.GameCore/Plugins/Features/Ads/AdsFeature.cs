﻿#if BBGC_ADS_FEATURE

using System;
using BlackBears.Ads;
using BlackBears.GameCore.AppEvents;
using SQBAModule = BlackBears.SQBA.SQBA;

namespace BlackBears.GameCore.Features.Ads
{

    public class AdsFeature : Feature, IAppPauseListener, IAppResumeListener
    {

        private IAppEvents appEvents;
        private AdsFeatureConfig config;
        private AdsManager module;

#if UNITY_EDITOR
        internal bool IsRewardedAdsAvailable => true;
        internal bool IsInterstitialAdsAvailable => true;
#else
        internal bool IsRewardedAdsAvailable => module.IsRewardedAdsAvailable();
        internal bool IsInterstitialAdsAvailable => module.IsInterstitialAdsAvailable();
#endif
        
        internal AdsFeature(IAppEvents appEvents, AdsFeatureConfig config)
            : base((int)FeaturePriority.Ads, "Ads Feature")
        {
            this.appEvents = appEvents;
            this.config = config;
        }

        int IAppEventPriority.Priority => (int)AppEventPriority.Ads;

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var sqbaFiles = BBGC.Features.GetFeature<SQBAFiles.SQBAFilesFeature>();
            module = new AdsManager(config.Settings, sqbaFiles.TextByName(config.ParamsFileName), 
                SQBAModule.CurrentLocale());

            appEvents.AddAppPauseListener(this);
            appEvents.AddAppResumeListener(this);

            Initialized();
            onSuccess.SafeInvoke();
        }

        internal void ShowRewarded(Block<string> onPlatformSelected, Block<string> onSuccess, 
            Block onSkip, Block onClick, FailBlock onFail, string source = "unknown") 
        {
            #if UNITY_EDITOR
            onSuccess.SafeInvoke(null);
            #else
            module.ShowRewarded(onPlatformSelected, onSuccess, onSkip, onClick, onFail, source);
            #endif
        }

        internal void ShowInterstitial(Block<string> onPlatformSelected, Block<string> onSuccess,
            Block onClick, FailBlock onFail, string source = "unknown") 
        {
            #if UNITY_EDITOR
            onSuccess.SafeInvoke(null);
            #else
            module.ShowInterstitial(onPlatformSelected, onSuccess, onClick, onFail, source);
            #endif
        }

        void IAppPauseListener.OnAppPaused() => module.OnAppPaused();
        void IAppResumeListener.OnAppResumed() => module.OnAppResumed();

    }

}

#endif