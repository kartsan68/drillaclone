﻿#if BBGC_ADS_FEATURE
using BlackBears.Ads;
#endif

namespace BlackBears.GameCore.Features.Ads
{

    [System.Serializable]
    public class AdsFeatureConfig : FeatureConfig
    {

#if BBGC_ADS_FEATURE

		[UnityEngine.SerializeField] private string paramsFileName;
        [UnityEngine.SerializeField] private AdsSettings settings;

        internal string ParamsFileName { get { return paramsFileName; } }
        internal AdsSettings Settings { get { return settings; } }

#endif

    }

}