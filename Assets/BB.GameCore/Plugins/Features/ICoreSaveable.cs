using SimpleJSON;

namespace BlackBears.GameCore.Features
{

    internal interface ICoreSaveable
    {

        string SaveKey { get; }
        JSONNode GenerateSaveData();
        void PrepareFromLoad(JSONNode saveData);
        void LoadDataNotFinded();

    }

}