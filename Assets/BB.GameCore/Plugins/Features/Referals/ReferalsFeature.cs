﻿#if BBGC_REFERALS_FEATURE

using System;
using BlackBears.Referals;

namespace BlackBears.GameCore.Features.Referals
{

    public sealed class ReferalsFeature : Feature
    {

        private ReferalsFeatureConfig config;

        internal ReferalsFeature(ReferalsFeatureConfig config)
            : base((int)FeaturePriority.Referals, "Referals Feature")
        {
            this.config = config;
        }

        public ReferalsModule Referals { get; private set; }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var files = BBGC.Features.GetFeature<SQBAFiles.SQBAFilesFeature>();
            string paramsText = files.TextByName(config.ParamsFileName);
            var refParams = new ReferalsParameters(SimpleJSON.JSON.Parse(paramsText));

            Referals = ReferalsModule.Init(refParams, config.Secret);

            var coreDefaults = BBGC.Features.CoreDefaults();
            coreDefaults.OnGameConnectedToCore += OnGameConnected;

            Initialized();
            onSuccess.SafeInvoke();
        }

        private void OnGameConnected()
        {
            var core = BBGC.Features.CoreDefaults();
            Referals.OnGameConnected(core.AnalyticsAdapter);
        }

    }

}

#endif