namespace BlackBears.GameCore.Features.Referals
{

    [System.Serializable]
    public class ReferalsFeatureConfig : FeatureConfig
    {

        #if BBGC_REFERALS_FEATURE

        [UnityEngine.SerializeField] private string secret;
        [UnityEngine.SerializeField] private string paramsFileName;

        internal string Secret { get { return secret; } }
        internal string ParamsFileName { get { return paramsFileName; } }

        #endif

    }
}