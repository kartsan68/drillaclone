using System.Collections;
using BlackBears.GameCore.AppEvents;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.Events;

#if BBGC_LOCAL_NOTIFICATIONS
#if UNITY_IOS
using Unity.Notifications.iOS;
using UnityEngine.iOS;
using NotificationServices = UnityEngine.iOS.NotificationServices;
#endif



namespace BlackBears.GameCore.Features.LocalNotifications
{

    public class LocalNotificationFeature : Feature, IAppPauseListener, IAppResumeListener
    {
        private GameNotificationsManager manager;
        private LocalNotificationsFeatureConfig config;

        private bool applicationPaused = false;
        private bool lastAvailable = false;

        private IAppEvents appEvents;

        private BoolUnityEvent onAvailableChanged = new BoolUnityEvent();

        public BoolUnityEvent OnAvailableChanged => onAvailableChanged;
        public GameNotificationsManager Manager => manager;

#if UNITY_IOS && !UNITY_EDITOR
        public bool Available
        {
            get
            {
                return iOSNotificationCenter.GetNotificationSettings().AuthorizationStatus == AuthorizationStatus.Authorized;
            }
        }
#else
        public bool Available { get { return true; } }

#endif


        public int Priority => (int)AppEventPriority.LocalNotification;

        internal LocalNotificationFeature(LocalNotificationsFeatureConfig config, IAppEvents appEvents)
            : base((int)FeaturePriority.LocalNotification, "Local Notification Feature")
        {
            this.config = config;
            this.appEvents = appEvents;
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            Initialized();

            var go = new GameObject("~~Local Notification Feature");
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            Object.DontDestroyOnLoad(go);
#endif
            manager = go.AddComponent<GameNotificationsManager>();
            manager.Mode = config.Mode;
            manager.AutoBadging = config.AutoBadging;
            go.SetActive(true);

            lastAvailable = Available;

            manager.Initialize();

            appEvents.AddAppPauseListener(this);
            appEvents.AddAppResumeListener(this);


            onSuccess.SafeInvoke();
        }

        public void CancelAllNotifications()
        {
            manager.CancelAllNotifications();
        }

        public IGameNotification CreateNotification()
        {
            return manager.CreateNotification();
        }

        public void ScheduleNotification(IGameNotification notification)
        {
            manager.ScheduleNotification(notification);
        }


        /// <summary>
        /// Запрос локальных нотификаций для IOS
        /// </summary>
        /// <param name="isShowSettings"></param>
        /// <returns></returns>
        IEnumerator RequestAuthorization(bool isShowSettings, Block onSuccess = null)
        {
#if UNITY_IOS
            using (var req = new AuthorizationRequest(
                AuthorizationOption.Sound 
                    | AuthorizationOption.Alert 
                    | AuthorizationOption.Badge, true))
            {
                while (!req.IsFinished)
                {
                    yield return null;
                };

                string res = "\n RequestAuthorization: \n";
                res += "\n finished: " + req.IsFinished;
                res += "\n granted :  " + req.Granted;
                res += "\n error:  " + req.Error;
                res += "\n deviceToken:  " + req.DeviceToken;
                Debug.Log(res);

                if (req.Granted)
                {
                    onSuccess?.Invoke();
                    yield break;
                }

                if (isShowSettings) manager.StartCoroutine(WaitForSettings());
            }
#else
            yield return null;
#endif
        }

        public void EnableLocalNotification(bool isShowSettings, Block onSuccess = null)
        {
#if UNITY_IOS 
            if (!Available)
            {
                manager.StartCoroutine(RequestAuthorization(isShowSettings, onSuccess));
                return;
            }
#endif
            onSuccess?.Invoke();
        }

        private IEnumerator WaitForSettings()
        {
            yield return new WaitForSeconds(0.7f);
            if (!applicationPaused) NativeUtils.OpenNotificationSettings();
        }

        void IAppPauseListener.OnAppPaused()
        {
            applicationPaused = true;
        }

        void IAppResumeListener.OnAppResumed()
        {
#if UNITY_IOS
            applicationPaused = false;
            if (lastAvailable != Available)
            {
                onAvailableChanged.Invoke(Available);
                lastAvailable = Available;
            }
#endif
        }
    }
}

#endif