using UnityEngine;

namespace BlackBears.GameCore.Features.LocalNotifications
{

    [System.Serializable]
    public class LocalNotificationsFeatureConfig : FeatureConfig
    {
        #if BBGC_LOCAL_NOTIFICATIONS
        [SerializeField, Tooltip("The operating mode for the notifications manager.")]
		private GameNotificationsManager.OperatingMode mode = GameNotificationsManager.OperatingMode.ClearOnForegrounding;

		[SerializeField, Tooltip(
			"Check to make the notifications manager automatically set badge numbers so that they increment.\n" +
			"Schedule notifications with no numbers manually set to make use of this feature.")]
		private bool autoBadging = true;

        public GameNotificationsManager.OperatingMode Mode => mode;

        public bool AutoBadging => autoBadging;

        #endif
    }
}