using JC = UnityEngine.AndroidJavaClass;
using JO = UnityEngine.AndroidJavaObject;
using JR = UnityEngine.AndroidJavaRunnable;

namespace BlackBears.GameCore.Features.Emulator
{

    internal class EmulatorFeature : Feature
    {

        internal EmulatorFeature()
            : base((int)FeaturePriority.Emulator, "Emulator Feature")
        {
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
#if UNITY_IOS || UNITY_EDITOR
            FinishInit(onSuccess);
#else
            CheckAndroidEmulator(onSuccess);
#endif
        }

        private void FinishInit(Block callback)
        {
            Initialized();
            callback.SafeInvoke();
        }

        private void CheckAndroidEmulator(Block callback)
        {
            FinishInit(callback);
            // using (var unity = new JC("com.unity3d.player.UnityPlayer"))
            // {
            //     JO activity = unity.GetStatic<JO>("currentActivity");
            //     activity.Call("detectEmulator",
            //         new JR(() => OnEmulatorDetected()), //Эмулятор обнаружен
            //         new JR(() => FinishInit(callback))
            //     );
            // }
        }

        private void OnEmulatorDetected()
        {
            var alerts = BBGC.Features.Alerts();
            var localization = BBGC.Features.Localization();
            alerts.AddAlert(new Alerts.ErrorAlert(string.Empty, 
                localization.Get("EMULATOR"), string.Empty, null));
        }

    }

}