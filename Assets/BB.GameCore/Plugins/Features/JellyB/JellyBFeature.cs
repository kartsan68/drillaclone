﻿using SimpleJSON;
using UnityEngine;

namespace BlackBears.GameCore.Features.JellyB
{

    public enum Workspace { Standart, JellyB }

    public sealed class JellyBFeature : Feature, ICoreSaveable
    {

		private const string saveKey = "jellyb";
		private const string workspaceKey = "wrkspc";

        internal JellyBFeature() : base((int)FeaturePriority.JellyB, "JellyB Feature") {}

		public bool IsJellyB { get; private set; }
		public Workspace Workspace { get; private set; }

        string ICoreSaveable.SaveKey => saveKey;

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
		{
			IsJellyB = DetectJellyB();
			Initialized();
			onSuccess.SafeInvoke();
		}

		private bool DetectJellyB()
		{
			#if UNITY_EDITOR
			return false;
			#elif UNITY_IOS
			return DetectJellyBiOS();
			#elif UNITY_ANDROID
			return DetectJellyBAndroid();
			#endif
		}

        void ICoreSaveable.PrepareFromLoad(JSONNode saveData)
        {
			if (saveData == null || saveData.Tag == JSONNodeType.None)
			{
				Workspace = Workspace.Standart;
			}
			else
			{
				Workspace = (Workspace)saveData[workspaceKey].AsInt;
			}
        }

        void ICoreSaveable.LoadDataNotFinded() 
		{ 
			#if UNITY_IOS
			Workspace = IsJellyB ? Workspace.JellyB : Workspace.Standart; 
			#else
			Workspace = Workspace.Standart;
			#endif
		}

        JSONNode ICoreSaveable.GenerateSaveData()
        {
            var json = new JSONObject();
			json[workspaceKey] = (int)Workspace;
			return json;
        }

#if !UNITY_EDITOR && UNITY_IOS
		private bool DetectJellyBiOS()
		{
			return getIsJellyB();
		}

		[System.Runtime.InteropServices.DllImport("__Internal")]
		private static extern bool getIsJellyB();
#endif

#if !UNITY_EDITOR && UNITY_ANDROID
		private bool DetectJellyBAndroid()
		{
			string buildTags = null;
			using(var buildClass = new AndroidJavaClass("android.os.Build"))
			{
				buildTags = buildClass.GetStatic<string>("TAGS");
			}

			if (buildTags != null && buildTags.Contains("test-keys")) return true;

			return canExecuteCommand("/system/xbin/which su")
				|| canExecuteCommand("/system/bin/which su")
				|| canExecuteCommand("/sbin/which su")
				|| canExecuteCommand("which su");
		}

		private bool canExecuteCommand(string command) {

			AndroidJavaObject process = null;
			AndroidJavaObject isr = null;
			AndroidJavaObject br = null;
			try
			{
				process = ProcessFromRuntime(command);
				isr = GetInputStreamReader(process);
				br = new AndroidJavaObject("java.io.BufferedReader", isr);
				string inLine = br.Call<string>("readLine");
				return !string.IsNullOrEmpty(inLine);
			}
			catch(AndroidJavaException e)
			{
				return false;
			}
			finally
			{
				if (process != null) process.Call("destroy");
				DisposeObject(process);
				DisposeObject(isr);
				DisposeObject(br);
			}
		}
		
		private AndroidJavaObject ProcessFromRuntime(string command)
		{
			using(var runtime = new AndroidJavaClass("java.lang.Runtime"))
			{
				var rObj = runtime.CallStatic<AndroidJavaObject>("getRuntime");
				var process = rObj.Call<AndroidJavaObject>("exec", command);
				rObj.Dispose();
				return process;
			}
		}

		private AndroidJavaObject GetInputStreamReader(AndroidJavaObject process)
		{
			using(var input = process.Call<AndroidJavaObject>("getInputStream"))
			{
				return new AndroidJavaObject("java.io.InputStreamReader", input);
			}
		}

		private void DisposeObject(AndroidJavaObject obj)
		{
			if (obj != null) obj.Dispose();
		}

#endif

    }

}