using BlackBears.GameCore.Configurations;

namespace BlackBears.GameCore.Features.CoreDefaults
{

    [System.Serializable]
    public class CoreDefaultsFeatureConfig
    {
        
        [UnityEngine.SerializeField] private string configName = "core_defaults.json";
        [UnityEngine.SerializeField] private SpritePack softCurrencyIconPack;
        [UnityEngine.SerializeField] private SpritePack hardCurrencyIconPack;
        [UnityEngine.SerializeField] private SpritePack scoreIconPack;
        [UnityEngine.SerializeField] SubscriptionSpritesContainer subscriptionSprites;

        internal string ConfigName { get { return configName; } }
        internal SpritePack SoftCurrencyIconPack { get { return softCurrencyIconPack; } }
        internal SpritePack HardCurrencyIconPack { get { return hardCurrencyIconPack; } }
        internal SpritePack ScoreIconPack { get { return scoreIconPack; } }
        internal SubscriptionSpritesContainer SubscriptionSprites { get { return subscriptionSprites; } }

    }

}