using System.Collections.Generic;

namespace BlackBears.GameCore.Features.CoreDefaults.Analytics
{

    public interface IAnalyticsAdapter
    {
        
        void PostEvent(string eventName, Dictionary<string, object> data, AdditionalProps additionalProps = default(AdditionalProps));
        void AddUserProfileCounter(string attributeKey, double delta);
    }

}