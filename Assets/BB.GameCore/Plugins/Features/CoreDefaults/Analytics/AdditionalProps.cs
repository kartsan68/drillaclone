namespace BlackBears.GameCore.Features.CoreDefaults.Analytics
{
    public struct AdditionalProps
    {
        public readonly bool day;
        public readonly bool currentSession;

        public AdditionalProps(bool day = false, bool currentSession = false)
        {
            this.day = day;
            this.currentSession = currentSession;
        }
    }
}