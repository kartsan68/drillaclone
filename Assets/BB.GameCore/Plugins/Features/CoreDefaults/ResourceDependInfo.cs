using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.CoreDefaults
{

    public class ResourceDependInfo
    {
        public readonly string text;
        public readonly List<Sprite> sprites;

        public bool HasText => !string.IsNullOrEmpty(text);
        public bool HasSprites => sprites != null && sprites.Count > 0;

        public ResourceDependInfo(string text = null, List<Sprite> sprites = null)
        {
            this.text = text;
            this.sprites = sprites;
        }

    }

}