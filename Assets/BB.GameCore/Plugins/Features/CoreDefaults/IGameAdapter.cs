using System;
using System.Collections.Generic;
using BlackBears.GameCore.Features.CoreDefaults;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.GameCore
{

    public partial interface IGameAdapter
    {
        
        string PlayerId { get; }
        string Icon { get; }
        string Nickname { get; }
        string Country { get; }

        string ConfigVersion { get; }


        string PlayerKey { get; }

        string CurrencyAmount { get; }
        long League { get; }
        long StartGameDate { get; }
        double Score { get; }
        double WeeklyScore { get; }
        string SubscriptionId { get; }

        double CoinsPerSecond { get; }

        void RegisterPlayer(string nickname, string icon, string country, Block onSuccess, FailBlock onFail);
        void UpdatePlayer(string nickname, string icon, string country, Block onSuccess, FailBlock onFail);
        void PrepareForSpendHardCurrency(PDouble currency, Block onSuccess, FailBlock onFail);
        void AddHardCurrency(PDouble currency);
        void SpendHardCurrency(PDouble currency, PlaceOfSpendResources place);
        bool AddSoftCurrency(PDouble soft);
        
        string AmountToPrettyString(double amount);
    }

}