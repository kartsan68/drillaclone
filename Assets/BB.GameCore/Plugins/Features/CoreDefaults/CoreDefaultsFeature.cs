using BlackBears.Chronometer;
using BlackBears.GameCore.AppEvents;
using BlackBears.GameCore.Configurations;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.GameCore.Features.SQBA;
#if BBGC_LEAGUES_FEATURE
using BlackBears.GameCore.Features.Leagues;
#endif
using BlackBears.Utils;
using SimpleJSON;

namespace BlackBears.GameCore.Features.CoreDefaults
{

    public class CoreDefaultsFeature : Feature, IAppResumeListener, ICoreSaveable
    {

        private const string saveKey = "crdfts";

        private ChronometerFeature chronometer;

        private CoreDefaultsFeatureConfig config;
        private CoreParams coreParams;
        private CoreState state;
        private IAppEvents appEvents;

        public string AppStoreLink { get { return coreParams.appStoreLink; } }
        public RateData RateData { get { return coreParams.rateData; } }

        public string DevStoreId { get { return coreParams.devStoreId; } }

        public string PrivacyUrl { get { return coreParams.privacyUrl; } }
        public string TermsUrl { get { return coreParams.termsUrl; } }
        public string PrivacyPartnersUrl { get { return coreParams.privacyPartnersUrl; } }

        public string VkSupportLink { get { return coreParams.vkSupportLink; } }
        public string SupportMail { get { return coreParams.supportMail; } }
        public string SupportMailSubject { get { return coreParams.supportMailSubject; } }

        public string[] Countries { get { return coreParams.countries; } }

#if BBGC_LEAGUES_FEATURE
        public LeagueData LeagueData { get { return coreParams.leagueData; } }
#endif

        public long SessionsCount { get { return state != null ? state.sessionCount : 0; } }
        public string StartVersion { get { return state != null ? state.startVersion : string.Empty; } }

        public IGameAdapter GameAdapter { get; private set; }
        public IAnalyticsAdapter AnalyticsAdapter { get; private set; }
        public string PlayerId { get { return GameAdapter != null ? GameAdapter.PlayerId : string.Empty; } }

        public SpritePack SoftCurrencyIconPack { get { return config.SoftCurrencyIconPack; } }
        public SpritePack HardCurrencyIconPack { get { return config.HardCurrencyIconPack; } }
        public SpritePack ScoreIconPack { get { return config.ScoreIconPack; } }
        public SubscriptionSpritesContainer SubscriptionSprites { get { return config.SubscriptionSprites; } }

        string ICoreSaveable.SaveKey { get { return saveKey; } }
        int IAppEventPriority.Priority { get { return (int)AppEventPriority.CoreDefault; } }

#if BBGC_SQBA_FEATURE
        public string ShareLink
        {
            get
            {
                var locale = BlackBears.SQBA.SQBA.CurrentLocale();
                if (string.Equals(locale, "ru", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    return coreParams.shareLinkRu;
                }
                return coreParams.shareLink;
            }
        }
#endif

        public event Block OnGameConnectedToCore;

        internal CoreDefaultsFeature(CoreDefaultsFeatureConfig config, IAppEvents appEvents)
            : base((int)FeaturePriority.CoreDefaults, "CoreDefaults Feature")
        {
            this.appEvents = appEvents;
            this.config = config;
            this.state = new CoreState();
        }

        public void ConnectWithGame(IGameAdapter gameAdapter, IAnalyticsAdapter analyticsAdapter)
        {
            ConnectGameAdapter(gameAdapter);
            ConenctAnalyticsAdapter(analyticsAdapter);

            OnGameConnectedToCore.SafeInvoke();
            OnGameConnectedToCore = null;
        }

        public void ConenctAnalyticsAdapter(IAnalyticsAdapter analyticsAdapter)
        {
            if (this.AnalyticsAdapter != null)
            {
                Logger.Warning(name, "Second connect of analytics adapter, don't do this.");
                return;
            }
            if (analyticsAdapter == null)
            {
                Logger.Warning(name, "Don't send 'null' for analytics adapter");
                return;
            }

            this.AnalyticsAdapter = analyticsAdapter;
        }

        void ICoreSaveable.PrepareFromLoad(JSONNode saveData)
        {
            state.FromJson(saveData);
            CheckForNewSession();
            UnityEngine.Debug.LogFormat("Sessions: {0}", state.sessionCount);
        }

        void ICoreSaveable.LoadDataNotFinded() { }

        JSONNode ICoreSaveable.GenerateSaveData() { return state.ToJson(); }


        void IAppResumeListener.OnAppResumed()
        {
            if (GameAdapter == null) return;
            if (TimeModule.IsValid)
            {
                CheckForNewSession();
            }
            else
            {
                state.saveTime = chronometer.LastSaveTime;
                TimeModule.OnChronometerUpdate -= OnChronometerUpdated;
                TimeModule.OnChronometerUpdate += OnChronometerUpdated;
            }
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            chronometer = BBGC.Features.Chronometer();

            JSONNode json = null;
#if BBGC_SQBA_FILES_FEATURE
            var files = BBGC.Features.SQBAFiles();
            json = JSON.Parse(files.TextByName(config.ConfigName));
#endif

            coreParams = new CoreParams(json);

            appEvents.AddAppResumeListener(this);

            Initialized();
            onSuccess.SafeInvoke();
        }

        private void ConnectGameAdapter(IGameAdapter gameAdapter)
        {
            if (this.GameAdapter != null)
            {
                Logger.Warning(name, "Second connect of game adapter, don't do this.");
                return;
            }
            if (gameAdapter == null)
            {
                Logger.Warning(name, "Don't send 'null' for game adapter");
                return;
            }

            this.GameAdapter = gameAdapter;
        }

        private void OnChronometerUpdated(object sender, ChronometerEventArgs args)
        {
            if (args.eventType != ChronometerEventType.Validated) return;
            TimeModule.OnChronometerUpdate -= OnChronometerUpdated;
            CheckForNewSession(state.saveTime);
        }

        private void CheckForNewSession()
        {
            CheckForNewSession(chronometer.LastSaveTime);
        }

        private void CheckForNewSession(long lastSaveTime)
        {
            var actualTime = chronometer.ActualTime;
            if (actualTime - lastSaveTime > coreParams.sessionOfflineTime)
            {
                state.sessionCount += 1;
                chronometer.ResetSessionTime();
            }
        }
    }

}