using SimpleJSON;

namespace BlackBears.GameCore.Features.CoreDefaults
{

    internal class CoreState
    {
        private const string versionFormat = "{0} ({1})";

        private const string sessionCountKey = "sncnt";
        private const string startVersionKey = "strt_vrsn";

        internal PLong sessionCount = 1;

        internal string startVersion = string.Empty;

        internal long saveTime;

        internal JSONNode ToJson()
        {
            var json = new JSONObject();
            json[sessionCountKey] = sessionCount.ToString();
            json[startVersionKey] = startVersion;
            return json;
        }

        internal void FromJson(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None)
            {
                SetVersion();
                return;
            }

            startVersion = json[startVersionKey].Value;
            if(string.IsNullOrEmpty(startVersion)) SetVersion();

            sessionCount = PLong.Parse(json[sessionCountKey].Value);
        }

        private void SetVersion()
        {
            #if UNITY_ANDROID
            startVersion = string.Format(versionFormat, UnityEngine.Application.version, "Android");
            #elif UNITY_IOS
            startVersion = string.Format(versionFormat, UnityEngine.Application.version, "iOS");
            #endif
        }

    }

}