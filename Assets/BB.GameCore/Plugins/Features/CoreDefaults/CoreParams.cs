#if BBGC_LEAGUES_FEATURE
using BlackBears.GameCore.Features.Leagues;
#endif
using BlackBears.Utils;
using SimpleJSON;

namespace BlackBears.GameCore.Features.CoreDefaults
{

    internal class CoreParams
    {

        internal readonly string appStoreLink;
        internal readonly RateData rateData;

        internal readonly string devStoreId;

        internal readonly string privacyUrl;
        internal readonly string termsUrl;
        internal readonly string privacyPartnersUrl;

        internal readonly string vkSupportLink;
        internal readonly string supportMail;
        internal readonly string supportMailSubject;

        internal readonly long sessionOfflineTime;

        internal readonly string[] countries;

        internal readonly string shareLink;
        internal readonly string shareLinkRu;

        #if BBGC_LEAGUES_FEATURE
        internal readonly LeagueData leagueData;
        #endif

        internal CoreParams(JSONNode json)
        {
            if (json == null) return;
            
#if !UNITY_EDITOR && UNITY_IOS
            string linkFormat = json["ios_app_store_link"].Value;
            string appId = json["ios_app_store_id"].Value;
            string reviewParams = json["ios_app_store_review_params"].Value;
            appStoreLink = string.Format(linkFormat, appId, string.Empty);
            rateData = new RateData(string.Format(linkFormat, "%@", reviewParams), appId);

            devStoreId = json["ios_dev_store_id"].Value;
#elif !UNITY_EDITOR && UNITY_ANDROID
            appStoreLink = json["android_app_store_link"].Value;
            rateData = new RateData(appStoreLink);
            
            devStoreId = json["android_dev_store_id"].Value;
#elif UNITY_EDITOR
            appStoreLink = null;
            rateData = default(RateData);
            devStoreId = null;
#endif

            privacyUrl = json["privacy_url"].Value;
            termsUrl = json["terms_url"].Value;
            privacyPartnersUrl = json["privacy_partners_url"].Value;
            supportMail = json["mail_support"].Value;
            supportMailSubject = json["mail_support_subject"].Value;
            vkSupportLink = json["vk_support_link"].Value;

            shareLink = json["share_url"].Value;
            shareLinkRu = json["share_url_ru"].Value;

            sessionOfflineTime = json["session_offline_time"].AsLong;

            var countriesArray = json["included_countries"].AsArray;
            countries = new string[countriesArray.Count];
            for (int i = 0; i < countries.Length; i++) countries[i] = countriesArray[i];
#if BBGC_LEAGUES_FEATURE
            leagueData = new LeagueData(json["leagues"]);
#endif
        }

    }

}