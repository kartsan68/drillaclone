namespace BlackBears.GameCore.Features.CoreDefaults
{
    public enum PlaceOfSpendResources
    {
        Default,
        UpdatePlayer,
        CreateClan,
        UpdateClan
    }
}