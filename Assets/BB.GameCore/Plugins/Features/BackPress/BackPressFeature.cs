﻿using UnityEngine;

namespace BlackBears.GameCore.Features.BackPress
{

    public sealed class BackPressFeature : Feature
    {

        private BackPressBehaviour behaviour;

        internal BackPressFeature() : base((int)FeaturePriority.BackPress, "BackPress Feature") { }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var go = new GameObject(string.Format("~~{0}", name));
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityEngine.Object.DontDestroyOnLoad(go);
            #endif
            behaviour = go.AddComponent<BackPressBehaviour>();
            Disable();

            Initialized();
            onSuccess.SafeInvoke();
        }

        public void Enable() { behaviour.enabled = true; }
        public void Disable() { behaviour.enabled = false; }

        public event Block OnExit { add { behaviour.OnExit += value; } remove { behaviour.OnExit -= value; } }

        public bool AddListener(IBackPressListener listener) { return behaviour.AddListener(listener); }
        public bool RemoveListener(IBackPressListener listener) { return behaviour.RemoveListener(listener); }

    }

}