namespace BlackBears.GameCore.Features.BackPress
{
    public interface IBackPressListener
    {
        bool OnBackPress();
    }
}