using System;
using System.Collections.Generic;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;

namespace BlackBears.GameCore.Features.BackPress
{
    internal class BackPressBehaviour : MonoBehaviour
    {

        private LinkedList<IBackPressListener> listeners = new LinkedList<IBackPressListener>();

        private Alert _alert;
        private AlertsFeature _alerts;

        private Alert Alert
        {
            get
            {
                if (_alert == null)
                {
                    var localization = BBGC.Features.GetFeature<LocalizationFeature>();
                    _alert = new YesNoAlert(AlertType.Negative,
                        localization.Get("BBGC_EXIT_HEADER"),
                        localization.Get("BBGC_EXIT_MESSAGE"),
                        localization.Get("BBGC_EXIT_CONFIRM"),
                        localization.Get("CANCEL"),
                        AcceptClose, null);
                }
                return _alert;
            }
        }

        internal event Block OnExit;

        private AlertsFeature AlertsFeature
        {
            get { return _alerts ?? (_alerts = BBGC.Features.GetFeature<AlertsFeature>()); }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) ProcessBackPress();
        }

        private void ProcessBackPress()
        {
            bool handled = false;

            var nextNode = listeners.First;
            var node = listeners.First;

            while (nextNode != null)
            {
                node = nextNode;
                nextNode = node.Next;
                if (node.Value == null)
                {
                    listeners.Remove(node);
                    continue;
                }

                if (handled = node.Value.OnBackPress()) break;
            }

            if (!handled) AlertsFeature.AddAlert(Alert);
        }

        internal bool AddListener(IBackPressListener listener)
        {
            if (listener == null || listeners.Contains(listener)) return false;
            listeners.AddFirst(listener);
            return true;
        }

        internal bool RemoveListener(IBackPressListener listener)
        {
            return listeners.Remove(listener);
        }

        private void AcceptClose()
        {
            
        OnExit.SafeInvoke();

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

    }
}