using System;
using System.Collections.Generic;
using BlackBears.GameCore.Features;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Features.BBGames
{

    public class BBGamesFeature : Feature
    {

        private const int maxTryCount = 3;

        BBGamesBehaviour behaviour;

        private string dataUrl;
        private int timeoutLoadIcon;

        private bool dataOnLoad;
        private bool _dataLoaded;
        private int tryCount = 0;

        private GameIconsLoader iconsLoader;
        private BannerData[] bannersData;

        private event Block _onBannersLoaded;

        public BannerData this[int index] { get { return bannersData[index]; } }
        public int BannersCount { get { return bannersData != null ? bannersData.Length : 0; } }

        public event Block OnBannersLoaded
        {
            add
            {
                _onBannersLoaded += value;
                TryToStartLoading();
            }
            remove
            {
                _onBannersLoaded -= value;
            }
        }

        private bool IsNeedStartLoading { get { return !dataOnLoad && !_dataLoaded; } }

        public bool DataLoaded
        {
            get { return _dataLoaded; }
            private set
            {
                if (_dataLoaded == value) return;
                _dataLoaded = value;
                _onBannersLoaded.SafeInvoke();
                _onBannersLoaded = null;
            }
        }

        internal BBGamesFeature(string dataUrl, int timeoutLoadIcon)
            : base((int)FeaturePriority.BBGames, "BBGames Feature")
        {
            this.dataUrl = dataUrl;
            this.timeoutLoadIcon = timeoutLoadIcon;
        }

        public void LoadBanner(BannerData bannerData, Block<Texture> onLoad, FailBlock onFail)
        {
            if (bannerData == null) 
            {
                onFail.SafeInvoke();
                return;
            }

            iconsLoader.LoadIcon(bannerData.imagePath, onLoad, onFail);
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var go = new GameObject(string.Format("~~{0}", name));
            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityEngine.Object.DontDestroyOnLoad(go);
            #endif
            behaviour = go.AddComponent<BBGamesBehaviour>();
            iconsLoader = new GameIconsLoader(behaviour);

            Initialized();
            onSuccess.SafeInvoke();
        }

        private void TryToStartLoading()
        {
            if (!IsNeedStartLoading) return;
            dataOnLoad = true;

            behaviour.LoadData(dataUrl, timeoutLoadIcon, OnGameDataLoaded, OnGameDataLoadFailed);
        }

        private void OnGameDataLoaded(ServerResponseData response)
        {
            List<BannerData> banners = new List<BannerData>();
            if (response.BannersData != null)
            {
                for (int i = 0; i < response.BannersData.Length; i++)
                {
                    banners.Add(response.BannersData[i]);
                    response.BannersData[i].Validate();
                }
            }
            this.bannersData = banners.ToArray();
            dataOnLoad = false;
            DataLoaded = true;
        }

        private void OnGameDataLoadFailed(long errorCode, string errorMessage)
        {
            dataOnLoad = false;
            tryCount += 1;
            if (tryCount < maxTryCount && _onBannersLoaded != null)
            {
                TryToStartLoading();
            }
        }

    }

}