using SimpleJSON;

namespace BlackBears.GameCore.Features.BBGames
{
    internal class ServerResponseData
    {

        internal BannerData[] BannersData { get; private set; }

        internal ServerResponseData(JSONNode node)
        {
            var bannersArray = node["response"]["banners"].AsArray;
            BannersData = new BannerData[bannersArray.Count];
            for (int i = 0; i < BannersData.Length; i++)
            {
                BannersData[i] = new BannerData(bannersArray[i]);
            }
        }        

    }
}