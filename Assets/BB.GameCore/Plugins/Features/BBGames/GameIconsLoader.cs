using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.BBGames
{
    internal class GameIconsLoader
    {
        
        private BBGamesBehaviour behaviour;
        private Dictionary<string, IconLoader> iconLoadingByUrl = new Dictionary<string, IconLoader>();

        internal GameIconsLoader(BBGamesBehaviour behaviour)
        {
            this.behaviour = behaviour;
        }

        internal void LoadIcon(string url, Block<Texture> onLoad, FailBlock onFail)
        {
            IconLoader loader;
            iconLoadingByUrl.TryGetValue(url, out loader);
            if(loader == null) 
            {
                loader = new IconLoader();
                iconLoadingByUrl[url] = loader;
            }

            if (loader.Loaded)
            {
                onLoad.SafeInvoke(loader.texture);
                return;
            }

            loader.onTextureLoaded += onLoad;
            loader.onTextureLoadFailed += onFail;
            if (loader.isLoading) return;

            loader.isLoading = true;
            ResourceUnloadManager.BlockUnload(loader);
            behaviour.LoadTexture(url, texture => {
                loader.texture = texture;
                loader.onTextureLoaded.SafeInvoke(texture);
                loader.onTextureLoaded = null;
                loader.onTextureLoadFailed = null;
                loader.isLoading = false;
                ResourceUnloadManager.UnblockUnload(loader);
            }, (c, m) => {
                loader.onTextureLoadFailed.SafeInvoke();
                loader.onTextureLoaded = null;
                loader.onTextureLoadFailed = null;
                loader.isLoading = false;
                ResourceUnloadManager.UnblockUnload(loader);
            });
        }

        private class IconLoader
        {
            
            internal bool isLoading;
            internal Texture texture;
            internal bool Loaded { get { return texture != null; } }

            internal Block<Texture> onTextureLoaded;
            internal FailBlock onTextureLoadFailed;

        }

    }
}