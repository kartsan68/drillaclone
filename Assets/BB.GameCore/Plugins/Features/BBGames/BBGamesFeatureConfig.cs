namespace BlackBears.GameCore.Features.BBGames
{

    [System.Serializable]
    public class BBGamesFeatureConfig
    {

        [UnityEngine.SerializeField] private string androidUrl;
        [UnityEngine.SerializeField] private string iOSUrl;
        [UnityEngine.SerializeField] private int timeoutLoadIcon = 5;

        internal int TimeoutLoadIcon => timeoutLoadIcon;

        internal string DataUrl
        {
            get
            {
#if UNITY_ANDROID
                return androidUrl;
#else
                return iOSUrl;
#endif
            }
        }

    }

}