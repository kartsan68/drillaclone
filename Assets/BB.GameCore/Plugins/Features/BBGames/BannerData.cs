using SimpleJSON;
using UnityEngine;

namespace BlackBears.GameCore.Features.BBGames
{

    public class BannerData
    {

        public readonly int bannerId;
        public readonly string imagePath;
        public readonly string link;
        public readonly string name;
        public readonly string urlScheme;

        private bool isOld;

        public bool IsOld
        {
            get { return isOld; }
            set
            {
                if (isOld == value) return;
                
                isOld = value;
                if (isOld) PlayerPrefs.SetInt(urlScheme, 1);
                else PlayerPrefs.SetInt(urlScheme, 0);
                PlayerPrefs.Save();
            }
        }

        internal BannerData(JSONNode node)
        {
            bannerId = node["banner_id"].AsInt;
            imagePath = node["img"].Value;
            link = node["link"].Value;
            name = node["name"].Value;
            urlScheme = node["url_shema"].Value;
        }

        public void Validate()
        {
            isOld = PlayerPrefs.GetInt(urlScheme, 0) == 1;
        }

    }

}