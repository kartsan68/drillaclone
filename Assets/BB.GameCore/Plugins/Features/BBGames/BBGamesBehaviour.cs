using System;
using System.Collections;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Features.BBGames
{

    internal class BBGamesBehaviour : MonoBehaviour
    {

        internal void LoadData(string dataUrl, int timeoutLoadIcon, Block<ServerResponseData> onSuccess, FailBlock onFail)
        {
            StartCoroutine(LoadDataAsync(dataUrl, timeoutLoadIcon, onSuccess, onFail));
        }

        internal void LoadTexture(string url, Block<Texture> onSuccess, FailBlock onFail)
        {
            StartCoroutine(LoadTextureAsync(url, onSuccess, onFail));
        }

        private IEnumerator LoadDataAsync(string dataUrl, int timeoutLoadIcon, Block<ServerResponseData> onSuccess, FailBlock onFail)
        {
            using(var request = UnityWebRequest.Get(dataUrl))
            {
                request.timeout = timeoutLoadIcon;
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    onFail.SafeInvoke();
                    yield break;
                }

                var text = request.downloadHandler.text;
                var response = new ServerResponseData(JSON.Parse(text));
                onSuccess.SafeInvoke(response);
            }
        }

        private IEnumerator LoadTextureAsync(string url, Block<Texture> onSuccess, FailBlock onFail)
        {
            using(var request = UnityWebRequestTexture.GetTexture(url))
            {
                request.timeout = 5;
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    onFail.SafeInvoke();
                    yield break;
                }

                onSuccess.SafeInvoke(((DownloadHandlerTexture)request.downloadHandler).texture);
            }
        }

    }

}