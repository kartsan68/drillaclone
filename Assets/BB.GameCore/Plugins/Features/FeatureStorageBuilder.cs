using System.Collections.Generic;
using BlackBears.GameCore.AppEvents;
using BlackBears.GameCore.Exceptions;

namespace BlackBears.GameCore.Features
{

    internal class FeatureStorageBuilder
    {

        private GCConfig config;
        private IAppEvents appEvents;

        internal FeatureStorageBuilder(GCConfig config, IAppEvents appEvents)
        {
            this.config = config;
            this.appEvents = appEvents;
        }

        internal FeatureStorage Build()
        {
            if (config == null) throw new NoGameCoreConfigException("Set the GCSettings to GCBehaviour object.");

            var features = new List<Feature>();
            AddAnalyticsFeature(features);
            AddLocalizationFeature(features);
            AddPlayServiceFeature(features);
            AddJellyBFeature(features);
            AddAlertsFeature(features);
            AddNotificationsFeature(features);
            AddBackPressFeature(features);
            AddEmulatorFeature(features);
            AddChronometerFeature(features);
            AddSQBAFeature(features);
            AddSQBAFilesFeature(features);
            AddCoreDefaultsFeature(features);
            AddGDPRFeature(features);
            AddAppUpdateFeature(features);
            AddRateFeature(features);
            AddPurchasesFeature(features);
            AddReferalsFeature(features);
            AddGameServerFeature(features);
            AddBBGamesFeature(features);
            AddDropItemSystemFeature(features);
            AddLeaguesFeature(features);
            AddClansFeature(features);
            AddSaveFeature(features);
            AddVibrationFeature(features);
            AddLocalNotificationFeature(features);

            return new FeatureStorage(features.ToArray());
        }

        private void AddPlayServiceFeature(List<Feature> features)
        {
#if !UNITY_EDITOR &&  UNITY_ANDROID && BBGC_USE_PLAY_SERVICE
            features.Add(new PlayService.PlayServiceFeature());
#endif
        }

        private void AddAnalyticsFeature(List<Feature> features)
        {
#if BBGC_ANALYTICS_FEATURE
            var featureConfig = config.Analytics;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new Analytics.AnalyticsFeature(featureConfig, appEvents));
            }
#endif
        }

        private void AddLocalizationFeature(List<Feature> features)
        {
            var featureConfig = config.Localization;
            features.Add(new Localization.LocalizationFeature(featureConfig));
        }

        private void AddJellyBFeature(List<Feature> features)
        {
            features.Add(new JellyB.JellyBFeature());
        }

        private void AddAlertsFeature(List<Feature> features)
        {
            features.Add(new Alerts.AlertsFeature(config.Alerts));
        }

        private void AddNotificationsFeature(List<Feature> features)
        {
            features.Add(new Notifications.NotificationsFeature());
        }

        private void AddBackPressFeature(List<Feature> features)
        {
            features.Add(new BackPress.BackPressFeature());
        }

        private void AddEmulatorFeature(List<Feature> features)
        {
            features.Add(new Emulator.EmulatorFeature());
        }

        private void AddChronometerFeature(List<Feature> features)
        {
#if BBGC_CHRONOMETER_FEATURE
            var featureConfig = config.Chronometer;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new Chronometer.ChronometerFeature(featureConfig, appEvents));
            }
#endif
        }

        private void AddSQBAFeature(List<Feature> features)
        {
#if BBGC_SQBA_FEATURE
            var featureConfig = config.SQBA;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new SQBA.SQBAFeature(featureConfig));
            }
#endif
        }


        private void AddSQBAFilesFeature(List<Feature> features)
        {
            var featureConfig = config.SQBAFiles;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new SQBAFiles.SQBAFilesFeature(featureConfig));
            }
        }

        private void AddCoreDefaultsFeature(List<Feature> features)
        {
            features.Add(new CoreDefaults.CoreDefaultsFeature(config.CoreDefaults, appEvents));
        }

        private void AddGDPRFeature(List<Feature> features)
        {
            features.Add(new GDPR.GDPRFeature(config.GDPR));
        }

        private void AddAppUpdateFeature(List<Feature> features)
        {
            var featureConfig = config.AppUpdate;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new AppUpdate.AppUpdateFeature());
            }
        }

        private void AddRateFeature(List<Feature> features)
        {
            features.Add(new Rate.RateFeature(config.Rate));
        }

        private void AddPurchasesFeature(List<Feature> features)
        {
#if BBGC_ADS_FEATURE
            var featureConfig = config.Ads;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new Ads.AdsFeature(appEvents, featureConfig));
            }
#endif
        }

        private void AddReferalsFeature(List<Feature> features)
        {
#if BBGC_REFERALS_FEATURE
            var featureConfig = config.Referals;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new Referals.ReferalsFeature(featureConfig));
            }
#endif
        }

        private void AddGameServerFeature(List<Feature> features)
        {
#if BBGC_GAME_SERVER_FEATURE
            var featureConfig = config.GameServer;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new GameServer.GameServerFeature(featureConfig));
            }
#endif
        }

        private void AddBBGamesFeature(List<Feature> features)
        {
            var featureConfig = config.BBGames;
            if (featureConfig != null) features.Add(new BBGames.BBGamesFeature(featureConfig.DataUrl, featureConfig.TimeoutLoadIcon));
        }

        private void AddLeaguesFeature(List<Feature> features)
        {
#if BBGC_LEAGUES_FEATURE
            var featureConfig = config.Leagues;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new Leagues.LeaguesFeature(featureConfig));
            }
#endif
        }

        private void AddClansFeature(List<Feature> features)
        {
#if BBGC_CLANS_FEATURE
            var featureConfig = config.Clans;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new Clans.ClansFeature(featureConfig));
            }
#endif
        }

        private void AddDropItemSystemFeature(List<Feature> features)
        {
#if BBGC_DROP_ITEM_SYSTEM_FEATURE
            var featureConfig = config.DropItemSystem;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new DropItemSystem.DropItemSystemFeature(featureConfig));
            }
#endif
        }

        private void AddSaveFeature(List<Feature> features)
        {
            features.Add(new Save.SaveFeature(appEvents, config.Save));
        }

        private void AddVibrationFeature(List<Feature> features)
        {
            features.Add(new Vibration.VibrationFeature());
        }

        private void AddLocalNotificationFeature(List<Feature> features)
        {
#if BBGC_LOCAL_NOTIFICATIONS
            var featureConfig = config.LocalNotifications;
            if (featureConfig != null && featureConfig.Enabled)
            {
                features.Add(new LocalNotifications.LocalNotificationFeature(featureConfig, appEvents));
            }
#endif
        }

    }

}