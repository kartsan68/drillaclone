namespace BlackBears.GameCore.Features.Save
{

    [System.Serializable]
    public class SaveFeatureConfig
    {

        [UnityEngine.SerializeField] private string salt;
        [UnityEngine.SerializeField] private bool doNotReconnectToTheSaveCloud;

        internal string Salt => salt;
        internal bool DoNotReconnectToTheSaveCloud => doNotReconnectToTheSaveCloud;

    }

}