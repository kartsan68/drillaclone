namespace BlackBears.GameCore.Features.Save
{

    public enum SaveConnectionStatus
    {
        NotConnected = 1,
        InvalidAccount = 2,
        Connected = 3
    }

}