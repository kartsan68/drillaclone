﻿using System;
using System.Collections.Generic;
using System.IO;
using BlackBears.GameCore.AppEvents;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.Localization;
using BlackBears.GameCore.Features;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Events;
using Security = BlackBears.Utils.Security;

namespace BlackBears.GameCore.Features.Save
{

    public class SaveFeature : Feature, IAppResumeListener
    {

        private const string gameKey = "gm";
        private const string featuresKey = "ftrs";
        private const string playerIdKey = "plrId";
        private const string saveCloudHelperKey = "sch";


#if UNITY_EDITOR
        private const string decodedName = "main.decoded";

        private string tempFolderPath = "GameTemp";
        private string decodedPath;

        private string TempFolderPath { get { return string.Format("{0}/", tempFolderPath); } }
        private string DecodedPath { get { return decodedPath ?? (decodedPath = TempFolderPath + decodedName + ".json"); } }
#endif

        private const string fileName = "main.state";
        private const string backupName = "main.state.backup";
        private const string salt = "xu9-jK/%s-Xhf+FA>l|G|dh([n.NAdAN";

        private string filePath;
        private string backupPath;

        private CoreDefaultsFeature core;
        private ChronometerFeature chronometer;
        private IAppEvents appEvents;
        private SaveFeatureConfig config;
        private List<ICoreSaveable> saveables = new List<ICoreSaveable>();
        private UnityEvent onSaveFailed = new UnityEvent();
        private UnityEvent onSaveLoadStart = new UnityEvent();
        private UnityEvent onSaveLoadEnd = new UnityEvent();

        private JSONNode gameSave;
        private string gameSaveFileText;
        private bool rewrite;

        private SaveConnectionStatus _connectionStatus;
        private SaveManager saveManager;

        public SaveStatus SaveStatus { get; private set; }
        public UnityEvent OnSaveFailed => onSaveFailed;
        public UnityEvent OnSaveLoadStart => onSaveLoadStart;
        public UnityEvent OnSaveLoadEnd => onSaveLoadEnd;
        public SaveConnectionStatus ConnectionStatus
        {
            get { return _connectionStatus; }
            private set
            {
                if (_connectionStatus == value) return;
                _connectionStatus = value;
                PlayerPrefs.SetInt(saveCloudHelperKey, (int)_connectionStatus);
                PlayerPrefs.Save();
                OnConnectionStatusChanged.SafeInvoke();
            }
        }

        int IAppEventPriority.Priority => (int)AppEventPriority.Save;

        public event Block OnConnectionStatusChanged;

        public ISaveProvider SaveManager => saveManager;

        public string GameSaveFileText => gameSaveFileText;

        public event Block<string> OnExternalSave;
        
        private string FilePath
        {
            get
            {
                return filePath ??
#if UNITY_EDITOR
                    (filePath = TempFolderPath + fileName + ".txt");
#elif UNITY_ANDROID
                    (filePath = string.Format("{0}/{1}", GetAndroidDataPath(), fileName));                     
#else
                    (filePath = string.Format("{0}/{1}", Application.persistentDataPath, fileName)); 
#endif
            }
        }

        private string BackupPath
        {
            get
            {
                return backupPath ??
#if UNITY_EDITOR
                    (backupPath = TempFolderPath + backupName + ".txt");
#elif UNITY_ANDROID
                    (backupPath = string.Format("{0}/{1}", GetAndroidDataPath(), backupName));                     
#else
                    (backupPath = string.Format("{0}/{1}", Application.persistentDataPath, backupName)); 
#endif
            }
        }

        internal SaveFeature(IAppEvents appEvents, SaveFeatureConfig config) : base((int)FeaturePriority.Save, "Save Feature")
        {
#if UNITY_EDITOR
            System.IO.Directory.CreateDirectory(TempFolderPath);
#endif
            this.appEvents = appEvents;
            this.config = config;
        }

        public JSONNode GetSave(bool clearSave = true)
        {
            var save = gameSave;
            if (clearSave) gameSave = null;
            return save;
        }

        public void PostInit()
        {
            saveManager.PostInit();
        }

        public void Tick(float dt)
        {
            saveManager.Tick(dt);
        }

        public void Save(string externalState = null, string rewriteFile = null, bool externalSave = true)
        {
            if(rewrite) return;

            if (!String.IsNullOrEmpty(rewriteFile)) rewrite = true;

            // if (core.GameAdapter == null)
            // {
            //     Logger.Warning(name, "Connect GameAdapter to BBGC before use saving!");
            //     return;
            // }

            if (!chronometer.IsValid && !chronometer.IsOffline)
            {
                onSaveFailed.Invoke();
                return;
            }

            string state = externalState ?? GenerateState();
            
            gameSaveFileText = EncodeState(state);
            try
            {
                using (StreamWriter writer = File.CreateText(BackupPath))
                {
                    if (String.IsNullOrEmpty(rewriteFile))
                    {
                        writer.Write(gameSaveFileText);
                    }
                    else
                    {
                        gameSaveFileText = rewriteFile;
                        writer.Write(rewriteFile);
                    }
                    writer.Flush();
                }

                if (File.Exists(FilePath)) File.Replace(BackupPath, FilePath, null);
                else File.Move(BackupPath, FilePath);
            }
            catch
            {
                onSaveFailed.Invoke();
            }

            CloudSave(gameSaveFileText);
            if (externalSave) OnExternalSave?.Invoke(state);

            if (externalState != null && externalState.Equals(string.Empty)) File.Delete(FilePath);

#if UNITY_EDITOR
            if (File.Exists(DecodedPath)) File.Delete(DecodedPath);
            using (StreamWriter writer = File.CreateText(DecodedPath))
            {
                writer.Write(state);
                writer.Flush();
            }
#endif
        }

        /// <summary>
        /// Декодируем сохранение
        /// </summary>
        /// <param name="stateEncoded"></param>
        /// <returns></returns>
        private string DecodeState(string stateEncoded)
        {
            string md5 = stateEncoded.Substring(0,Security.md5Length);
            string save = stateEncoded.Substring(Security.md5Length);
            if (!ValidateSave(save, md5))
            {
                return "";
            }

            return Security.FromBase64(save);
        }
        /// <summary>
        /// Сохранение кодируем
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        private string EncodeState(string state)
        {
            var encodedState = Security.ToBase64(state);
            var md5 = GetSaveMD5(encodedState);

            return md5 + encodedState;

        }

        public void InitCloudHelper(Block onSuccess, FailBlock onFail)
        {
            if (ConnectionStatus == SaveConnectionStatus.Connected) return;

            SaveCloudHelper.Init(() =>
            {
                Debug.Log("SaveCloudHelper Init success");
                if (CheckPlayerIdentifier()) onSuccess.SafeInvoke();
                else onFail.SafeInvoke();

            }, () =>
            {
                Debug.Log("SaveCloudHelper Init failed");
                ConnectionStatus = SaveConnectionStatus.NotConnected;
                onFail.SafeInvoke();
            });
        }

        void IAppResumeListener.OnAppResumed()
        {
            if (!SaveCloudHelper.CloudActive) return;
            SaveCloudHelper.CheckPlayerConnection(() =>
            {
                if (!SaveCloudHelper.CloudActive) ConnectionStatus = SaveConnectionStatus.NotConnected;
            });
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            core = BBGC.Features.CoreDefaults();
            chronometer = BBGC.Features.Chronometer();

            foreach (var feature in BBGC.Features.AllFeatures)
            {
                ICoreSaveable saveable;
                if ((saveable = feature as ICoreSaveable) != null) saveables.Add(saveable);
            }

            if (PlayerPrefs.GetInt(saveCloudHelperKey, 0) == (int)SaveConnectionStatus.NotConnected
                && config.DoNotReconnectToTheSaveCloud)
            {
                ConnectionStatus = SaveConnectionStatus.NotConnected;
                FinishInit(onSuccess);
                return;
            }

            SaveCloudHelper.Init(() =>
            {
                Debug.Log("SaveCloudHelper Init success");
                if (CheckPlayerIdentifier()) FinishInit(onSuccess);
                else ShowResetProgressAlert(initData, onSuccess, onFail);

            }, () =>
            {
                Debug.Log("SaveCloudHelper Init failed");
                ConnectionStatus = SaveConnectionStatus.NotConnected;
                FinishInit(onSuccess);
            });

        }

        private void FinishInit(Block onSuccess)
        {
            appEvents.AddAppResumeListener(this);

            LoadState(() =>
            {
                if (SaveStatus != SaveStatus.Valid)
                {
                    foreach (var saveable in saveables)
                    {
                        saveable.LoadDataNotFinded();
                    }
                }
                this.saveManager = new SaveManager(this);
                Initialized();
                onSuccess.SafeInvoke();
            });
        }

        private bool CheckPlayerIdentifier()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            string previousPlayerId = PlayerPrefs.GetString(playerIdKey, null);
            string currentPlayerId = SaveCloudHelper.PlayerId;

            if (string.IsNullOrEmpty(previousPlayerId))
            {
                if (string.IsNullOrEmpty(currentPlayerId)) ConnectionStatus = SaveConnectionStatus.NotConnected;
                else ConnectionStatus = SaveConnectionStatus.Connected;
                return true;
            }
            else 
            {
                if (string.IsNullOrEmpty(currentPlayerId))
                {
                    ConnectionStatus = SaveConnectionStatus.NotConnected;
                    return true;
                }
                else if (string.Equals(previousPlayerId, currentPlayerId))
                {
                    ConnectionStatus = SaveConnectionStatus.Connected;
                    return true;
                }
                else 
                {
                    ConnectionStatus = SaveConnectionStatus.InvalidAccount;
                    return false;
                }
            }
#else
            ConnectionStatus = SaveConnectionStatus.Connected;
            return true;
#endif
        }

        private void ShowResetProgressAlert(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            LocalizationFeature localization = BBGC.Features.GetFeature<LocalizationFeature>();
            AlertsFeature alerts = BBGC.Features.GetFeature<AlertsFeature>();

            alerts.AddAlert(new YesNoAlert(AlertType.Destructive,
                localization.Get("BBGC_WRONG_ACCOUNT_ALERT_HEADER"),
                localization.Get("BBGC_WRONG_ACCOUNT_ALERT_MESSAGE"),
                localization.Get("BBGC_WRONG_ACCOUNT_ALERT_ACTION"),
                localization.Get("BBGC_WRONG_ACCOUNT_ALERT_CLOSE"), () =>
                {
                    PlayerPrefs.SetString(playerIdKey, null);
                    if (File.Exists(FilePath)) File.Delete(FilePath);
                    if (File.Exists(BackupPath)) File.Delete(BackupPath);

                    InitProcess(initData, onSuccess, onFail);
                }, () =>
                {
                    Application.Quit();
                }));
        }

        private void CloudSave(string state)
        {
            if (SaveCloudHelper.CloudActive)
            {
                SaveCloudHelper.Save(state);
                PlayerPrefs.SetString(playerIdKey, SaveCloudHelper.PlayerId);
            }
        }

        private void LoadState(Block onComplete)
        {
            if (!File.Exists(FilePath))
            {
                onSaveLoadStart.Invoke();
                SaveCloudHelper.Load(cloudSave =>
                {
                    onSaveLoadEnd.Invoke();
                    if (!string.IsNullOrEmpty(cloudSave))
                    {
                        string decodeState = DecodeState(cloudSave);

                        Logger.Log("FROM_CLOUD",decodeState);
                        if (string.IsNullOrEmpty(decodeState))
                        {
                            Logger.Log("FROM_CLOUD","not valid");

                            SaveStatus = SaveStatus.Corrupted;
                        }
                        else
                        {
                            Logger.Log("FROM_CLOUD","valid");

                            ReadState(decodeState);
                            SaveStatus = SaveStatus.Valid;
                        }
                    }
                    else
                    {
                        SaveStatus = SaveStatus.NotFinded;
                    }

                    onComplete.SafeInvoke();
                }, () =>
                {
                    onSaveLoadEnd.Invoke();
                    SaveStatus = SaveStatus.NotFinded;
                    onComplete.SafeInvoke();
                });
                return;
            }


            string save;
            using (StreamReader reader = File.OpenText(FilePath))
            {
                save = reader.ReadToEnd();
            }

            
            var decodedSave = DecodeState(save);

            if (!string.IsNullOrEmpty(decodedSave))
            {
                ReadState(decodedSave);
                SaveStatus = SaveStatus.Valid;
                onComplete.SafeInvoke();
            }
            else
            {
                SaveStatus = SaveStatus.Corrupted;
                onComplete.SafeInvoke();
            }
        }

        private string GenerateState()
        {
            var saveState = new JSONObject();

            saveState[gameKey] = saveManager.ToJson();

            var featuresNode = new JSONObject();
            saveState[featuresKey] = featuresNode;

            for (int i = 0; i < saveables.Count; i++)
            {
                featuresNode[saveables[i].SaveKey] = saveables[i].GenerateSaveData();
            }

            return saveState.ToString();
        }

        private void ReadState(string state)
        {
            if (state == null) return;
            var json = JSON.Parse(state);

            var featuresNode = json[featuresKey];
            foreach (var saveable in saveables)
            {
                var featureSave = featuresNode[saveable.SaveKey];
                if (featureSave.Tag == JSONNodeType.None) continue;
                saveable.PrepareFromLoad(featureSave);
            }

            gameSave = json[gameKey];
        }

        private bool ValidateSave(string input, string inMd5)
        {
            var md5 = GetSaveMD5(input);
            return string.Equals(md5, inMd5, System.StringComparison.InvariantCulture);
        }

        private string GetSaveMD5(string input)
        {
            var builder = new System.Text.StringBuilder();
            builder.Append(input).Append(salt).Append(config.Salt);
            return Security.GetMD5Hash(builder.ToString(), Security.MD5Format.upper);
        }

#if UNITY_ANDROID
        private string GetAndroidDataPath()
        {
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject filesDir = currentActivity.Call<AndroidJavaObject>("getFilesDir");
            string path = filesDir.Call<string>("getAbsolutePath");
            unity.Dispose();
            currentActivity.Dispose();
            filesDir.Dispose();
            return path;
        }
#endif

    }

}