﻿using System.Collections.Generic;
using BlackBears.Chronometer;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Save
{

    public class SaveManager : ISaveProvider
    {

        private const long minResaveTime = 60;
        private const float autoSaveTime = 300;

        private SaveFeature saveFeature;

        private SaveState state = new SaveState();

        private bool SaveEnabled => state != null && state.saveBlockers.Count == 0;
        public bool IsFirstLaunch => state.prevSessionTime < 0;

        public long PrevSessionTime => state.prevSessionTime;
        public long SaveTime => state.saveTime;

        public float TimeBetweenSessions => TimeModule.TotalTime - state.prevSessionTime;
        public float TimeFromLastSave
        {
            get
            {
                UnityEngine.Debug.Log( TimeModule.TotalTime + state.saveTime);
                return TimeModule.TotalTime - state.saveTime;
            }
        }

        public SaveManager(SaveFeature saveFeature)
        {
            this.saveFeature = saveFeature;
            var save = saveFeature.GetSave();

            if (save != null) state.ReadData(save);
        }

        public void TryToSave()
        {
            var actualTime = TimeModule.ActualTime;
            if (actualTime - state.saveTime < minResaveTime) return;

            Save();
        }

        public void Save(string externalState = null, bool enableSaveToExternal = true)
        {
            if (!state.initialized && externalState == null) return;
            if (!SaveEnabled) return;
            saveFeature.Save(externalState, externalSave: enableSaveToExternal);
        }

        public void Unload() => state.loaded.Clear();

        public void Tick(float dt)
        {
            if (!state.initialized) return;
            state.autosaveTimer -= dt;
            if (state.autosaveTimer < 0)
            {
                TryToSave();
                state.autosaveTimer = autoSaveTime;
            }
        }

        internal JSONNode ToJson() => ((IEncodable)state).ToJSON();

        public void PostInit()
        {
            state.initialized = true;
            state.autosaveTimer = autoSaveTime;
        }

        SaveData ISaveProvider.GetSaveDataByKey(string key)
        {
            if (key == null) return null;
            for (int i = 0; i < state.loaded.Count; ++i)
            {
                var data = state.loaded[i];
                if (key.Equals(data.key)) return data;
            }
            return null;
        }

        void ISaveProvider.SetSaveables(List<ISaveable> saveable)
        {
            state.saveables.Clear();
            if (saveable == null) return;
            state.saveables.AddRange(saveable);
        }

        void ISaveProvider.SetSaveable(ISaveable saveable)
        {
            if (saveable == null || state.saveables.Contains(saveable)) return;
            state.saveables.Add(saveable);
        }

        bool ISaveProvider.RegisterSaveBlocker(object blocker)
        {
            if (state == null || blocker == null) return false;
            return state.saveBlockers.Add(blocker);
        }

        bool ISaveProvider.UnregisterSaveBlocker(object blocker)
        {
            if (state == null || blocker == null) return false;
            return state.saveBlockers.Remove(blocker);
        }

    }

}