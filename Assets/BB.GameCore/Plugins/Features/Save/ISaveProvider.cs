﻿using System.Collections.Generic;

namespace BlackBears.GameCore.Features.Save
{

    public interface ISaveProvider
    {
        
        bool IsFirstLaunch { get; }

        float TimeBetweenSessions { get; }
        float TimeFromLastSave { get; }

        long PrevSessionTime { get; }
        long SaveTime { get; }
        
        void SetSaveables(List<ISaveable> saveable);
        void SetSaveable(ISaveable saveable);

        SaveData GetSaveDataByKey(string key);

        bool RegisterSaveBlocker(object blocker);
        bool UnregisterSaveBlocker(object blocker);

        void TryToSave();
        void Save(string externalState = null, bool enableSaveToExternal = true);

        void Unload();

    }

}