﻿namespace BlackBears.GameCore.Features.Save
{
    
    public interface ISaveable
    {
        SaveData ToSaveData();
    }

}