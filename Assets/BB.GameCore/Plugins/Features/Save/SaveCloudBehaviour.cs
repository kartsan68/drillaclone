﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.Save
{
	public class SaveCloudBehaviour : MonoBehaviour {

		internal Block<string> saveBlock;
		internal Block failBlock;

		private void OnSaveLoaded(string cloudSave)
		{
			saveBlock.SafeInvoke(cloudSave);
			Destroy(gameObject);
		}

		private void OnLoadFailed()
		{
			failBlock.SafeInvoke();
			Destroy(gameObject);
		}
	}
}