﻿using System;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Save
{
    
    public class SaveData
    {
        private const string keyNodeKey = "key";
        private const string dataNodeKey = "data";

        public readonly string key;
        public readonly JSONNode data;

        public SaveData(string key, JSONNode data)
        {
            this.key = key;
            this.data = data;
        }

        public SaveData(JSONNode node)
        {
            key = node[keyNodeKey].Value;
            data = node[dataNodeKey];
        }

        public JSONNode AsJSONNode()
        {
            JSONObject obj = new JSONObject();
            obj.Add(keyNodeKey, key);
            obj.Add(dataNodeKey, data);
            return obj;
        }

    }
}

