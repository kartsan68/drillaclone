﻿using SimpleJSON;

namespace BlackBears.GameCore.Features.Save
{

    public interface IEncodable
    {

        JSONNode ToJSON();

    }

}