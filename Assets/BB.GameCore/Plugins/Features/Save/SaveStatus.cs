namespace BlackBears.GameCore.Features.Save
{

    public enum SaveStatus
    {
        NotFinded,
        Corrupted, 
        Valid
    }

}