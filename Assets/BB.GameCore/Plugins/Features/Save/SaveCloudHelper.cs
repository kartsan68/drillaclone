﻿using UnityEngine;
using BlackBears.Utils;
using System;

namespace BlackBears.GameCore.Features.Save
{
    internal class SaveCloudHelper
    {
        private static NativeConnection _instance;

        private static NativeConnection Instance => _instance ?? (_instance = new NativeConnection());

		internal static bool CloudActive => Instance.CloudActive;

        internal static string PlayerId => Instance.PlayerId;

		internal static void Init(Block onSuccess, Block onFail)
		{
			Instance.Init(onSuccess, onFail);
		}

        internal static void CheckPlayerConnection(Block onComplete)
        {
            Instance.CheckPlayerConnection(onComplete);
        }

        internal static void Save(string input)
        {
			//TODO: Имя файла
			Instance.Save("saveName", input);
        }

        internal static void Load(Block<string> saveBlock, Block onFail)
        {
			//TODO: Имя файла
			Instance.Load("saveName", saveBlock, onFail);
        }

    }
}
