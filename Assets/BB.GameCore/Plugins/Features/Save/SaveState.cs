﻿using System;
using System.Collections.Generic;
using BlackBears;
using BlackBears.Chronometer;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.GameCore.Features.Save
{

    public class SaveState : IEncodable
    {

        private const int saveVersion = 1;

        private const string timeKey = "time";
        private const string versionKey = "version";
        private const string saveKey = "save";
        private const string deviceIdKey = "deviceId";

        public bool initialized;
        public HashSet<object> saveBlockers = new HashSet<object>();

        public PInt currentVersion = new PInt(saveVersion);

        public PLong prevSessionTime = -1;
        public PInt prevSaveVersion;
        public long saveTime = -1;
        public float autosaveTimer;

        public List<SaveData> loaded = new List<SaveData>();
        public List<ISaveable> saveables = new List<ISaveable>();

        JSONNode IEncodable.ToJSON()
        {
            var json = new JSONObject();

            saveTime = TimeModule.TotalTime;
            json[timeKey] = ((PLong)saveTime).ToString();
            json[versionKey] = currentVersion.ToString();
            json[deviceIdKey] = UnityEngine.SystemInfo.deviceUniqueIdentifier;

            var array = new JSONArray();
            json.Add(saveKey, array);

            foreach (var saveable in saveables)
            {
                var saveData = saveable.ToSaveData();
                array.Add(saveData.AsJSONNode());
            }
            return json;
        }

        public void ReadData(JSONNode data)
        {
            var array = data[saveKey].AsArray;
            var deviceId = data[deviceIdKey];
            bool newDevice = String.IsNullOrEmpty(deviceId) || deviceId != UnityEngine.SystemInfo.deviceUniqueIdentifier;
            prevSessionTime = newDevice ? -1 : PLong.Parse(data[timeKey].Value); 
            prevSaveVersion = PInt.Parse(data[versionKey].Value);

            for (int i = 0; i < array.Count; ++i) loaded.Add(new SaveData(array[i]));
        }

    }

}