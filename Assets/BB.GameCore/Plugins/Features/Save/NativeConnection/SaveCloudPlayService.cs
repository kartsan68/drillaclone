#if !UNITY_EDITOR && UNITY_ANDROID && BBGC_SAVE_CLOUD_PLAY_SERVICE && !BBGC_SAVE_CLOUD


using System;
using System.Text;
using BlackBears.Chronometer;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

namespace BlackBears.GameCore.Features.Save
{
	public partial class NativeConnection
	{

		private const string Tag = "SAVE_PLAY_SERVICE";
		

		private string nameSave = "saveFile";
		private Block<string> successLoadData;
		private Block failLoadData;
		private string dataToSave;

		private void InitSaveCloud(Block onSuccess)
		{
			onSuccess.SafeInvoke();
		}

		/// <summary>
		/// Сохраняем данные на облако
		/// </summary>
		/// <param name="savedData"></param>
		/// <param name="name"></param>
		private void SaveGame(string savedData, string name)
		{

			dataToSave = savedData;
			nameSave = name;
			
			
			PlayGamesPlatform.Instance.SavedGame.OpenWithAutomaticConflictResolution(name, DataSource.ReadCacheOrNetwork,
																					ConflictResolutionStrategy.UseLongestPlaytime, OnOpenFileToWrite);
			
			
		}

		private void OnOpenFileToWrite(SavedGameRequestStatus status, ISavedGameMetadata gameMeta)
		{
			if (status == SavedGameRequestStatus.Success)
			{

				SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
				builder = builder
					.WithUpdatedPlayedTime(TimeSpan.FromMilliseconds(DateTime.Now.Millisecond));

				SavedGameMetadataUpdate updatedMetadata = builder.Build();
				Logger.Log(Tag,"Save to cloud: "+gameMeta.Filename+"\n "+dataToSave);
				PlayGamesPlatform.Instance.SavedGame.CommitUpdate(gameMeta, updatedMetadata,
																Encoding.UTF8.GetBytes(dataToSave), OnSavedGameWritten);
			}
			else
			{
				Logger.Log(Tag,"Fail open to write "+status);

			}
			
		}

		/// <summary>
		/// Читаем данные из облака
		/// </summary>
		private void LoadGameData(string name, Block<string> onSuccess, Block onFail) {
			successLoadData = onSuccess;
			failLoadData = onFail;
			if (PlayGamesPlatform.Instance.IsAuthenticated())
			{
				PlayGamesPlatform.Instance.SavedGame.OpenWithAutomaticConflictResolution(name, DataSource.ReadCacheOrNetwork,
																						ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened);
			}
			else
			{
				failLoadData.SafeInvoke();
			}
		}
		
		
		
		
		private void InvokeInit(Block onSuccess, Block onFail) => InitSaveCloud(onSuccess);
		private string InvokeGetPlayerId() => "1";
		private bool InvokeGetCloudActive() => PlayGamesPlatform.Instance.IsAuthenticated();

		private void InvokeCheckPlayerConnection(Block onComplete) 
		{
			onComplete.SafeInvoke();
		}

		private void InvokeSave(string name, string state) => SaveGame(state, name);

		private void InvokeLoad(string name, Block<string> onSuccess, Block onFail)
		{
			LoadGameData(name,onSuccess,onFail);
		}
		

		private void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata gameMeta)
		{
			if (status == SavedGameRequestStatus.Success) {
				Logger.Log(Tag,"Open save success "+gameMeta.Filename);
				PlayGamesPlatform.Instance.SavedGame.ReadBinaryData(gameMeta, OnSavedGameDataRead);
			}
			else
			{
				Logger.Log(Tag,"Open save fail "+status);

				failLoadData.SafeInvoke();
			}
		}

		private void OnSavedGameWritten (SavedGameRequestStatus status, ISavedGameMetadata game) {
			if (status == SavedGameRequestStatus.Success) {
				Logger.Log(Tag,"Success write save from cloud");
			} else {
				Logger.Log(Tag,"Error write save from cloud: "+status + " "+game);

			}
		}


		private void OnSavedGameDataRead (SavedGameRequestStatus status, byte[] data) {
			if (status == SavedGameRequestStatus.Success)
			{
				var dataRead = Encoding.UTF8.GetString(data);
				
				Logger.Log(Tag,"Open save data read success: \n"+dataRead);

				successLoadData.SafeInvoke(dataRead);
			} else {
				Logger.Log(Tag,"Open save data read fail "+status);

				failLoadData.SafeInvoke();
			}
		}
		
		
	}

	
}
#endif