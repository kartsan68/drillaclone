#if !UNITY_EDITOR && UNITY_IOS && (BBGC_SAVE_CLOUD || BBGC_SAVE_CLOUD_PLAY_SERVICE)

using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackBears.GameCore.Features.Save
{
    public partial class NativeConnection
    {
        private void InvokeInit(Block onSuccess, Block onFail)
        {
            onSuccess.SafeInvoke();
        }

        private string InvokeGetPlayerId() { return null; }

		[DllImport ("__Internal")] private static extern bool BBCloudIsActive();
        private bool InvokeGetCloudActive() { return BBCloudIsActive(); }

        [DllImport ("__Internal")] private static extern void BBSaveToCloud(string saveName, string save);
        private void InvokeSave(string name, string state)
        {
            BBSaveToCloud(name, state);
        }

        private void InvokeCheckPlayerConnection(Block onComplete) => onComplete.SafeInvoke();

		[DllImport ("__Internal")] private static extern void BBLoadFromCloud(string saveName);
        private void InvokeLoad(string name, Block<string> onSuccess, Block onFail)
        {
            GameObject callbackHandler = new GameObject();
			callbackHandler.name = "SaveCloudHandler";
			callbackHandler.AddComponent<SaveCloudBehaviour>().saveBlock = onSuccess;
            callbackHandler.AddComponent<SaveCloudBehaviour>().failBlock = onFail;

            #if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityEngine.Object.DontDestroyOnLoad(callbackHandler);
            #endif

			BBLoadFromCloud(name);
        }
    }
}

#endif