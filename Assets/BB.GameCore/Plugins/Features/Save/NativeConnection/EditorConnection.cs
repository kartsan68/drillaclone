#if UNITY_EDITOR || (!BBGC_SAVE_CLOUD && !BBGC_SAVE_CLOUD_PLAY_SERVICE)

namespace BlackBears.GameCore.Features.Save
{
    public partial class NativeConnection
    {

        private const string logTag = "EditorCloudConnection";
        
        private void InvokeInit(Block onSuccess, Block onFail) => onSuccess.SafeInvoke();
        private string InvokeGetPlayerId() => null;
        private bool InvokeGetCloudActive() => false;

        private void InvokeCheckPlayerConnection(Block onComplete) 
        {
            Logger.Log(logTag, "Check Player cloud connection");
            onComplete.SafeInvoke();
        }

        private void InvokeSave(string name, string state) => Logger.Log(logTag, $"Send save state with name {name} to cloud.");

        private void InvokeLoad(string name, Block<string> onSuccess, Block onFail)
        {
            Logger.Log(logTag, $"Take save state with name {name} from cloud.");
            onFail.SafeInvoke();
        }

    }
}

#endif