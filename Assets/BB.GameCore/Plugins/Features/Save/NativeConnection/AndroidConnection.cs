#if !UNITY_EDITOR && UNITY_ANDROID && BBGC_SAVE_CLOUD && !BBGC_SAVE_CLOUD_PLAY_SERVICE

using UnityEngine;

using JO = UnityEngine.AndroidJavaObject;
using JC = UnityEngine.AndroidJavaClass;
using JE = UnityEngine.AndroidJavaException;
using JP = UnityEngine.AndroidJavaProxy;
using JR = UnityEngine.AndroidJavaRunnable;

namespace BlackBears.GameCore.Features.Save
{

    public partial class NativeConnection
    {

        private JO cloudConnector;

        private void InvokeInit(Block onSuccess, Block onFail)
        {
            if (cloudConnector == null) CreateConnector();
            if (PlayerId != null) 
            {
                onSuccess.SafeInvoke();
                return;
            }
            cloudConnector.Call("signIn", 
                new JR(() => onSuccess.SafeInvoke()), 
                new JR(() => onFail.SafeInvoke()));
        }

        private string InvokeGetPlayerId()
        {
            if (cloudConnector == null) return null;
            return cloudConnector.Call<string>("getPlayerId");
        }

        private bool InvokeGetCloudActive() => InvokeGetPlayerId() != null;

        private void InvokeCheckPlayerConnection(Block onComplete) 
        {
            if (cloudConnector == null) return;
            cloudConnector.Call("checkPlayerConnection", new JR(() => onComplete.SafeInvoke()));
        }

        private void InvokeSave(string name, string state)
        {
            if (cloudConnector == null) return;
            cloudConnector.Call("save", name, state);
        }

        private void InvokeLoad(string name, Block<string> onSuccess, Block onFail)
        {
            if (cloudConnector == null)
            {
                onFail.SafeInvoke();
                return;
            }

            cloudConnector.Call("load", name, new LoadSuccessProxy(onSuccess), 
                new JR(() => onFail.SafeInvoke()));
        }

        private void CreateConnector()
        {
            using (var unity = new JC("com.unity3d.player.UnityPlayer"))
            {
                JO activity = unity.GetStatic<JO>("currentActivity");
                cloudConnector = activity.Call<JO>("getCloudConnector");
                cloudConnector.Call("init");
            }
        }

        private class LoadSuccessProxy : JP
        {

            private Block<string> block;

            public LoadSuccessProxy(Block<string> block) : base("mobi.blackbears.unity.gameCore.StringAction") 
            {
                this.block = block;
            }

            void invoke(string s) { block.SafeInvoke(s); }

        }

    }

}

#endif