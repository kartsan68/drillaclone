namespace BlackBears.GameCore.Features.Save
{

    public partial class NativeConnection
    {

        public string PlayerId => InvokeGetPlayerId();
        public bool CloudActive => InvokeGetCloudActive();

        public void Init(Block onSuccess, Block onFail) => InvokeInit(onSuccess, onFail);
        public void CheckPlayerConnection(Block onComplete) => InvokeCheckPlayerConnection(onComplete);
        public void Save(string name, string state) => InvokeSave(name, state);
        public void Load(string name, Block<string> onSuccess, Block onFail) => InvokeLoad(name, onSuccess, onFail);

    }

}