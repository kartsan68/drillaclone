namespace BlackBears.GameCore.Features
{

    [System.Serializable]
    public class FeatureConfig
    {

        [UnityEngine.SerializeField] private bool enabled;

        internal FeatureConfig() {}

        public bool Enabled { get { return enabled; } }

    }

}