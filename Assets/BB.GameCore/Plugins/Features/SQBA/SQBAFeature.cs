﻿#if BBGC_SQBA_FEATURE

using System;
using BlackBears.SQBA;
using BlackBears.SQBA.Utils;
using BlackBears.SQBA.Modules.Purchasing;
using System.Collections.Generic;

using SQBAModule = BlackBears.SQBA.SQBA;
using SimpleJSON;
using BlackBears.SQBA.General;

#endif

namespace BlackBears.GameCore.Features.SQBA
{

    public sealed class SQBAFeature : Feature
#if BBGC_SQBA_FEATURE
    , ICoreSaveable
#endif
    {
        private const string saveKey = "sqbaft";

#pragma warning disable 0414
        private SQBAFeatureConfig config;
#pragma warning restore 0414


        public SQBAFeatureConfig Config => config;

        #region Initialization
        internal SQBAFeature(SQBAFeatureConfig config) :
            base((int)FeaturePriority.SQBA, "SQBA Feature")
        {
            this.config = config;
        }
        #endregion


#if BBGC_SQBA_FEATURE

        #region Additional Info
        public bool NeedForceUpdate { get { return SQBAModule.NeedForceUpdate; } }
        public bool RatingEnabled { get { return SQBAModule.RatingEnabled; } }
        public bool IsProduction { get { return SQBAModule.IsProduction; } }
        public long DayXTimestamp { get { return SQBAModule.DayXTimestamp; } }
        public string CurrentLocale { get { return SQBAModule.CurrentLocale(); } }
        public bool NoInternetConection => SQBAModule.NoInternetConection;

        string ICoreSaveable.SaveKey => saveKey;

        #endregion

        #region Files
        public string PathToFile(string fileName)
        {
            return SQBAModule.PathToFile(fileName);
        }
        #endregion

        #region Purchases
        public void InitializePurchases(PurchaseSQBAItem[] purchaseItems,
                                        Block onSuccess = null,
                                        FailBlock onFail = null)
        {
            SQBAModule.InitializePurchases(purchaseItems, onSuccess, onFail);
        }

        public void SetGlobalValidationObserver(PurchasesGlobalBlock onSuccess = null,
                                                PurchasesGlobalBlock onCancel = null,
                                                PurchasesGlobalBlock onFail = null)
        {
            SQBAModule.SetGlobalValidationObserver(onSuccess, onCancel, onFail);
        }

        public void GetPurchaseItemsPrices(PurchaseItemBlock onSuccess = null, FailBlock onFail = null)
        {
            SQBAModule.GetPurchaseItemsPrices(onSuccess, onFail);
        }

        public void BuyProduct(PurchaseSQBAItem purchaseItems,
                                Block<PurchaseAnswer> onSuccess = null,
                                Block onCancel = null,
                                FailBlock onFail = null)
        {
            SQBAModule.BuyProduct(purchaseItems, onSuccess, onCancel, onFail);
        }

        public void ProcessPendingProducts(PurchasesGlobalBlock onPending)
        {
            SQBAModule.ProcessPendingProducts(onPending);
        }

        public PurchaserType GetPurchaserType()
        {
            return SQBAModule.GetPurchaserType();
        }

        public void RestorePurchases(Block onSuccess, FailBlock onFail)
        {
            SQBAModule.RestorePurchases(onSuccess, onFail);
        }

        public void ValidatePurchase(PurchaseSQBAItem purchaseItem, FailBlock onFail)
        {
            SQBAModule.ValidatePurchase(purchaseItem, onFail);
        }

        public void CheckPurchaseIsActive(PurchaseSQBAItem purchaseItem, Block<bool> onFinish)
        {
            SQBAModule.CheckPurchaseIsActive(purchaseItem, onFinish);
        }

        public void CheckForActiveSubscriptions(string playerId, Block<List<string>> onSuccess, FailBlock onFail)
        {
            SQBAModule.CheckForActiveSubscriptions(playerId, onSuccess, onFail);
        }

        public void LocalCheckForActiveSubscriptions(Block onSuccess = null, FailBlock onFail = null)
        {
            SQBAModule.LocalCheckForActiveSubscriptions(onSuccess, onFail);
        }
        #endregion

        #region Codes
        public void SubscribeForBonusData(Block<SimpleJSON.JSONNode> dataCallback)
        {
            SQBAModule.SubscribeForBonusData(dataCallback);
        }
        #endregion

        #region GDPR
        public void SignUpPrivacyPolicy(string playerId, string countryCode, Block<GDPRStatus> onSuccess, FailBlock onFail)
        {
            SQBAModule.SignUpPrivacyPolicy(playerId, countryCode, onSuccess, onFail);
        }
        #endregion

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            SQBAModule.Start(config.Configuration, () =>
            {
                Initialized();
                onSuccess.SafeInvoke();
            }, (code, message) =>
            {
                if (SQBAModule.NoInternetConection)
                {
                    Initialized();
                    onSuccess.SafeInvoke();
                }
                else
                {
                    NotInitialized();
                    onFail.SafeInvoke((int)code, code + ": " + message);
                }
            });

        
        }

        JSONNode ICoreSaveable.GenerateSaveData()
        {
            return SQBAModule.ToJson();
        }

        void ICoreSaveable.PrepareFromLoad(JSONNode saveData)
        {
            SQBAModule.FromJson(saveData);
        }

        void ICoreSaveable.LoadDataNotFinded() {}

#endif
    }

}
