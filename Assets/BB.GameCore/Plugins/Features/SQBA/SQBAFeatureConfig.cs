﻿#if BBGC_SQBA_FEATURE
using SQBAConfiguration = BlackBears.SQBA.Configuration;
#endif

namespace BlackBears.GameCore.Features.SQBA
{

    [System.Serializable]
    public class SQBAFeatureConfig : FeatureConfig
    {

        #if BBGC_SQBA_FEATURE

        [UnityEngine.SerializeField] private SQBAConfiguration sqbaConfiguration;

        internal SQBAConfiguration Configuration { get { return sqbaConfiguration; } }

        #endif

    }

}