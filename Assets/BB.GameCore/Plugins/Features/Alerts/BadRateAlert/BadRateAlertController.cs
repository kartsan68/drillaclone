using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{

    internal class BadRateAlertController : AlertController
    {

        [SerializeField] private RectTransform containerTransform;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private GameObject overlay;

        [SerializeField] private Button writeMessageButton;
        [SerializeField] private Button cancelButton;

        internal BadRateAlert alert;

        private AlertsFeature alertsFeature;

        private void Awake()
        {
            alertsFeature = BBGC.Features.Alerts();
            canvasGroup.alpha = 0f;

            writeMessageButton.onClick.AddListener(OnWriteMessageButtonClick);
            cancelButton.onClick.AddListener(OnCancelButtonClick);
        }

        private void OnDestroy()
        {
            LeanTween.cancel(canvasGroup.gameObject);
            LeanTween.cancel(containerTransform.gameObject);
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;
        }

        protected override void ProcessShow()
        {
            if (overlay != null) overlay.SetActive(alert.showOverlay);

            var pos = new Vector2(0, -containerTransform.rect.height);
            containerTransform.anchoredPosition = pos;

            LeanTween.moveAnchored(containerTransform, Vector2.zero, 0.4f)
                .setEaseOutQuad();
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;

            var pos = containerTransform.anchoredPosition;
            pos.y = -containerTransform.rect.height;

            LeanTween.cancel(containerTransform.gameObject);
            LeanTween.moveAnchored(containerTransform, pos, 0.4f)
                .setEaseInQuad()
                .setOnComplete(() => Destroy(gameObject));
        }

        private void OnWriteMessageButtonClick()
        {
            if (!Showed) return;
            
            var core = BBGC.Features.CoreDefaults();
            EmailUtils.SendMailByUrl(core.GameAdapter.PlayerId, core.GameAdapter.ConfigVersion, core.SupportMail, core.SupportMailSubject);
            Hide();
        }

        private void OnCancelButtonClick()
        {
            if (Showed) Hide();
        }

    }

}