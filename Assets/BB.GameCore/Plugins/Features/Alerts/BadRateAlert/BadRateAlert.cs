namespace BlackBears.GameCore.Features.Alerts
{

    public class BadRateAlert : Alert
    {

        public readonly bool showOverlay;

        public BadRateAlert(bool showOverlay = false)
        {
            this.showOverlay = showOverlay;
        }

    }

}