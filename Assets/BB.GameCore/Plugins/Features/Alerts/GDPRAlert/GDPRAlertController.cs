﻿using BlackBears.GameCore.Features.BackPress;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.Utils;
using BlackBears.Utils.Components;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{

    internal class GDPRAlertController : AlertController, IBackPressListener
    {

        [SerializeField] private Button termsButton;
        [SerializeField] private Button privacyButton;
        [SerializeField] private Button signUpButton;
        [SerializeField] private ButtonLoaderAnimator signUpLoader;

        [SerializeField] private CanvasGroup canvasGroup;

        private AlertsFeature alertsFeature;
        private BackPressFeature backPress;
        private CoreDefaultsFeature coreDefaults;

        internal GDPRAlert alert;

        bool IBackPressListener.OnBackPress() { return true; }

        private void Awake()
        {
            this.alertsFeature = BBGC.Features.Alerts();
            this.backPress = BBGC.Features.BackPress();
            this.coreDefaults = BBGC.Features.CoreDefaults();

            canvasGroup.alpha = 0f;

            termsButton.onClick.AddListener(OnTermsButtonClick);
            privacyButton.onClick.AddListener(OnPrivacyButtonClick);
            signUpButton.onClick.AddListener(OnSignUpButtonClick);
        }

        private void OnDestroy()
        {
            if (backPress != null) backPress.RemoveListener(this);
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;

            if(canvasGroup != null) LeanTween.cancel(canvasGroup.gameObject);
        }

        protected override void ProcessShow()
        {
            if (backPress != null) backPress.AddListener(this);

            gameObject.SetActive(true);
            canvasGroup.alpha = 0f;
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (backPress != null) backPress.RemoveListener(this);
            LeanTween.cancel(canvasGroup.gameObject);
            LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f).setOnComplete(() =>
            {
                if (alert != null) alertsFeature.HideAlert(alert);
                alert = null;
                Destroy(this.gameObject);
            });
        }

        private void OnTermsButtonClick() { Application.OpenURL(coreDefaults.TermsUrl); }
        private void OnPrivacyButtonClick() { Application.OpenURL(coreDefaults.PrivacyUrl); }

        private void OnSignUpButtonClick()
        {
            if (alert == null) return;
            signUpButton.interactable = false;
            signUpLoader.StartLoadingAnimation(OnSignUpLoaderShowed);
        }

        private void OnSignUpLoaderShowed()
        {
            alert.signInPostback.SafeInvoke(OnSignInSuccess, OnSignInFailed);
        }

        private void OnSignUpLoaderHided()
        {
            signUpButton.interactable = true;

        }

        private void OnSignInSuccess()
        {
            Hide();
        }

        private void OnSignInFailed(long errorCode, string errorMessage)
        {
            signUpLoader.StopLoadingAnimation(success: false, onStopped: OnSignUpLoaderHided);
            var nofitications = BBGC.Features.GetFeature<NotificationsFeature>();
            var loc = BBGC.Features.Localization();
            nofitications.ShowNotification(new Notification(NotificationType.Warning, loc.Get("BBGC_GDPR_SIGN_UP_ERROR")));
        }

    }

}