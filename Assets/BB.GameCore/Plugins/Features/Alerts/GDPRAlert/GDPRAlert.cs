namespace BlackBears.GameCore.Features.Alerts
{
    
    public class GDPRAlert : Alert
    {

        public readonly Block<Block, FailBlock> signInPostback;

        public GDPRAlert(Block<Block, FailBlock> signInPostback)
        {
            this.signInPostback = signInPostback;
        }

    }

}