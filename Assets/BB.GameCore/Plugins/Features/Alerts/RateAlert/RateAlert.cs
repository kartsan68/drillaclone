namespace BlackBears.GameCore.Features.Alerts
{

    public class RateAlert : Alert
    {
        public readonly Block<int> onAppRated;
        public readonly Block onRatePostponed;
        public readonly bool showOverlay;

        public RateAlert(Block<int> onAppRated, Block onRatePostponed, bool showOverlay = false)
        {
            this.onAppRated = onAppRated;
            this.onRatePostponed = onRatePostponed;
            this.showOverlay = showOverlay;
        }

    }

}