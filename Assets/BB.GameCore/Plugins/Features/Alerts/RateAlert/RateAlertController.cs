﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{

    internal class RateAlertController : AlertController
    {

        [SerializeField] private RectTransform containerTransform;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private GameObject overlay;

        [SerializeField] private Button[] rateButtons;

        [SerializeField] private Button cancelButton;

        internal RateAlert alert;

        private AlertsFeature alertsFeature;

        private void Awake()
        {
            alertsFeature = BBGC.Features.Alerts();
            canvasGroup.alpha = 0f;

            for (int i = 0; i < rateButtons.Length; i++)
            {
                int index = i + 1;
                rateButtons[i].onClick.AddListener(() => OnRateButtonClick(index));
            }
            cancelButton.onClick.AddListener(OnCancelButtonClick);            
        }

        private void OnDestroy()
        {
            LeanTween.cancel(canvasGroup.gameObject);
            LeanTween.cancel(containerTransform.gameObject);
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;
        }

        protected override void ProcessShow()
        {
            if (overlay != null) overlay.SetActive(alert.showOverlay);

            var pos = new Vector2(0, -containerTransform.rect.height);
            containerTransform.anchoredPosition = pos;

            LeanTween.moveAnchored(containerTransform, Vector2.zero, 0.4f)
                .setEaseOutQuad();
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;

            var pos = containerTransform.anchoredPosition;
            pos.y = -containerTransform.rect.height;

            LeanTween.cancel(containerTransform.gameObject);
            LeanTween.moveAnchored(containerTransform, pos, 0.4f)
                .setEaseInQuad()
                .setOnComplete(() => Destroy(gameObject));
        }

        private void OnRateButtonClick(int index)
        {
            if (!Showed) return;
            if (alert != null) alert.onAppRated.SafeInvoke(index);
            Hide();
        }

        private void OnCancelButtonClick()
        {
            if (!Showed) return;
            if (alert != null) alert.onRatePostponed.SafeInvoke();
            Hide();
        }

    }

}