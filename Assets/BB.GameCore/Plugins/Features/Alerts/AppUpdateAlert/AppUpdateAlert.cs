﻿namespace BlackBears.GameCore.Features.Alerts
{

    public class AppUpdateAlert : Alert
    {

        private static AppUpdateAlert _instance;
        public static AppUpdateAlert Instance { get { return _instance ?? (_instance = new AppUpdateAlert()); } }

        private AppUpdateAlert() {}

    }

}