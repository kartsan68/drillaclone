﻿using System;
using BlackBears.GameCore.Features.BackPress;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{

    internal class AppUpdateAlertController : AlertController, IBackPressListener
    {

        public AppUpdateAlert alert;

        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] private Button storeButton;

        private AlertsFeature alertsFeature;
        private BackPressFeature backPress;

        bool IBackPressListener.OnBackPress() { return true; }

        private void Awake()
        {
            alertsFeature = BBGC.Features.Alerts();
            backPress = BBGC.Features.BackPress();
            canvasGroup.alpha = 0f;

            storeButton.onClick.AddListener(OnStoreButtonClick);
        }

        private void OnDestroy()
        {
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;
            LeanTween.cancel(canvasGroup.gameObject);
        }

        protected override void ProcessShow()
        {
            if (backPress != null) backPress.AddListener(this);
            gameObject.SetActive(true);
            LeanTween.cancel(canvasGroup.gameObject);
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (backPress != null) backPress.RemoveListener(this);
            LeanTween.cancel(canvasGroup.gameObject);
            LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f)
                .setOnComplete(() => Destroy(gameObject));
        }

        private void OnStoreButtonClick()
        {
            NativeUtils.OpenApplicationStore(BBGC.Features.CoreDefaults().AppStoreLink);
        }

    }

}