
using UnityEngine;

namespace BlackBears.GameCore.Features.Alerts
{
    [CreateAssetMenu(fileName = "AlertsParams", menuName = "BlackBears/Alerts Params")]
    public class CoreAlertsParams : ScriptableObject
    {
        [UnityEngine.SerializeField] private YesNoAlertController yesNoAlertPrefab;
        [UnityEngine.SerializeField] private RateAlertController rateAlertPrefab;
        [UnityEngine.SerializeField] private BadRateAlertController badRateAlertPrefab;
        [UnityEngine.SerializeField] private ErrorAlertController errorAlertPrefab;
        [UnityEngine.SerializeField] private AppUpdateAlertController appUpdateAlertPrefab;
        [UnityEngine.SerializeField] private GDPRAlertController gdprAlertPrefab;
        [UnityEngine.SerializeField] private AwardAlertController awardAlertPrefab;

        [UnityEngine.SerializeField] private YesNoAlertViewDataParams yesNoAlertViewDataParams;


        internal YesNoAlertController YesNoAlertPrefab { get { return yesNoAlertPrefab; } }
        internal RateAlertController RateAlertPrefab { get { return rateAlertPrefab; } }
        internal BadRateAlertController BadRateAlertPrefab { get { return badRateAlertPrefab; } }
        internal ErrorAlertController ErrorAlertPrefab { get { return errorAlertPrefab; } }
        internal AppUpdateAlertController AppUpdateAlertPrefab { get { return appUpdateAlertPrefab; } }
        internal GDPRAlertController GDPRAlertPrefab { get { return gdprAlertPrefab; } }
        internal AwardAlertController AwardAlertPrefab { get { return awardAlertPrefab; } }

        internal YesNoAlertViewData ViewDataByType(AlertType type)
        {
            var viewsData = yesNoAlertViewDataParams.ViewsData;
            
            if (viewsData == null || viewsData.Length == 0) return null;

            for (int i = 0; i < viewsData.Length; i++)
            {
                if (viewsData[i].AlertType == type) return viewsData[i];
            }
            return viewsData[0];
        }

    }
}
