
using UnityEngine;

namespace BlackBears.GameCore.Features.Alerts
{
    [CreateAssetMenu(fileName = "YesNoAlertViewDataParams", menuName = "BlackBears/YesNoAlertViewDataParams")]
    public class YesNoAlertViewDataParams : ScriptableObject
    {
        [UnityEngine.SerializeField] private YesNoAlertViewData[] viewsData;

        internal YesNoAlertViewData[] ViewsData { get { return viewsData; } }
    }
}