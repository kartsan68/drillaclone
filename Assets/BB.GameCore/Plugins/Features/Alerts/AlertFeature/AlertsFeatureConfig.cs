namespace BlackBears.GameCore.Features.Alerts
{

    [System.Serializable]
    public class AlertsFeatureConfig
    {
        [UnityEngine.SerializeField] private CoreAlertsParams alertsParams;

        internal CoreAlertsParams AlertsParams { get { return alertsParams; } }    

    }

}