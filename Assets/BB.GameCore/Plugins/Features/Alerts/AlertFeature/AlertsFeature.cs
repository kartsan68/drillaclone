﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.Alerts
{

    public sealed class AlertsFeature : Feature
    {

        private RectTransform root;
        private Queue<Alert> alerts = new Queue<Alert>();
        private Alert currentAlert;

        private Queue<YesNoAlertController> yesNoControllers = new Queue<YesNoAlertController>();

        internal AlertsFeature(AlertsFeatureConfig featureConfig) : base((int)FeaturePriority.Alerts, "Alerts Feature")
        {
            this.Config = featureConfig;
        }

        public RectTransform Root
        {
            get { return root; }
            set
            {
                root = value;
                CheckAlerts();
            }
        }

        internal AlertsFeatureConfig Config { get; private set; }

        public void AddAlert(Alert alert)
        {
            if (alert == null) return;

            alerts.Enqueue(alert);
            CheckAlerts();
        }

        internal void HideAlert(Alert alert)
        {
            if (alert == null || currentAlert != alert) return;
            currentAlert = null;
            CheckAlerts();
        }

        internal void ReturnController(YesNoAlertController controller)
        {
            if (controller == null) return;
            yesNoControllers.Enqueue(controller);
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            if (initData != null) root = initData.alertsRoot;
            Initialized();
            onSuccess.SafeInvoke();
        }

        private void CheckAlerts()
        {
            if (Root == null || alerts.Count == 0 || currentAlert != null) return;

            currentAlert = alerts.Dequeue();
            if (currentAlert == null)
            {
                CheckAlerts();
                return;
            }
            var controller = GetController(currentAlert);
            if (controller == null)
            {
                currentAlert = null;
                CheckAlerts();
                return;
            }

            var crt = controller.RectTransform;
            var scale = crt.localScale;
            crt.SetParent(root);
            crt.localScale = scale;
            crt.sizeDelta = Vector2.zero;
            crt.anchoredPosition = Vector2.zero;
            controller.transform.SetAsLastSibling();

            controller.Show();
        }

        private AlertController GetController(Alert alert)
        {
            if (alert is YesNoAlert) return GetYesNoController((YesNoAlert)alert);
            if (alert is RateAlert) return GetRateController((RateAlert)alert);
            if (alert is BadRateAlert) return GetBadRateController((BadRateAlert)alert);
            if (alert is ErrorAlert) return GetErrorController((ErrorAlert)alert);
            if (alert is AppUpdateAlert) return GetAppUpdateController((AppUpdateAlert)alert);
            if (alert is GDPRAlert) return GetGDPRController((GDPRAlert)alert);
            if (alert is BonusAlert) return GetBonusController((BonusAlert)alert);
            return null;
        }

        private YesNoAlertController GetYesNoController(YesNoAlert alert)
        {
            YesNoAlertController controller = null;
            if (yesNoControllers.Count != 0) controller = yesNoControllers.Dequeue();
            else controller = MonoBehaviour.Instantiate(Config.AlertsParams.YesNoAlertPrefab);
            controller.Alert = alert;

            return controller;
        }

        private RateAlertController GetRateController(RateAlert alert)
        {
            var controller = MonoBehaviour.Instantiate(Config.AlertsParams.RateAlertPrefab);
            controller.alert = alert;
            return controller;
        }

        private BadRateAlertController GetBadRateController(BadRateAlert alert)
        {
            var controller = MonoBehaviour.Instantiate(Config.AlertsParams.BadRateAlertPrefab);
            controller.alert = alert;
            return controller;
        }

        private ErrorAlertController GetErrorController(ErrorAlert alert)
        {
            var controller = MonoBehaviour.Instantiate(Config.AlertsParams.ErrorAlertPrefab);
            controller.Alert = alert;
            return controller;
        }

        private AppUpdateAlertController GetAppUpdateController(AppUpdateAlert alert)
        {
            var controller = MonoBehaviour.Instantiate(Config.AlertsParams.AppUpdateAlertPrefab);
            controller.alert = alert;
            return controller;
        }

        private GDPRAlertController GetGDPRController(GDPRAlert alert)
        {
            var controller = MonoBehaviour.Instantiate(Config.AlertsParams.GDPRAlertPrefab);
            controller.alert = alert;
            return controller;
        }

        private AwardAlertController GetBonusController(BonusAlert alert)
        {
            var controller = MonoBehaviour.Instantiate(Config.AlertsParams.AwardAlertPrefab);
            controller.alert = alert;
            return controller;
        }
    }
}