using System.Collections;
using UnityEngine;

namespace BlackBears.GameCore.Features.Alerts
{

    [RequireComponent(typeof(RectTransform))]
    internal abstract class AlertController : MonoBehaviour
    {

        private bool initialized = false;
        protected bool Showed { get; private set; }

        private RectTransform _rectTransform;

        internal RectTransform RectTransform { get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); } }

        protected virtual void Start()
        {
            initialized = true;
            if (Showed) StartCoroutine(PostShow());
        }

        internal void Show() { Show(false); }

        private void Show(bool force)
        {
            if (Showed && !force) return;

            Showed = true;
            if (!initialized) return;
            ProcessShow();
        }

        protected void Hide()
        {
            if (!Showed) return;
            Showed = false;
            ProcessHide();
        }

        protected abstract void ProcessShow();
        protected abstract void ProcessHide();

        private IEnumerator PostShow()
        {
            yield return new WaitForEndOfFrame();
            Show(true);
        }

    }

}