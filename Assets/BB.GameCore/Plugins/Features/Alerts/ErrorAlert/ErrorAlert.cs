namespace BlackBears.GameCore.Features.Alerts
{

    public class ErrorAlert : Alert
    {
        const string  descWithCodeFormat = "{0}\n\n(Error: {1})";
        public readonly string title;
        public readonly string desc;
        public readonly string button;
        public readonly Block onAccept;

        public ErrorAlert(string title, string desc, string button, Block onAccept, long code = 0)
        {
            this.title = title;
            if(code != 0) this.desc = string.Format(descWithCodeFormat, desc, code);
            else this.desc = desc;
            this.button = button;
            this.onAccept = onAccept;
        }

    }

}