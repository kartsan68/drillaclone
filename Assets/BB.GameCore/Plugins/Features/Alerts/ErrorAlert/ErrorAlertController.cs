using BlackBears.GameCore.Features.BackPress;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{

    internal class ErrorAlertController : AlertController, IBackPressListener
    {

        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Text headerText;
        [SerializeField] private Text descriptionText;
        [SerializeField] private Text acceptButtonText;
        [SerializeField] private Button acceptButton;

        private AlertsFeature alertsFeature;
        private BackPressFeature backPress;
        private ErrorAlert alert;

        public ErrorAlert Alert
        {
            get { return alert; }
            set
            {
                alert = value;
                UpdateView();
            }
        }

        private void Awake()
        {
            alertsFeature = BBGC.Features.Alerts();
            backPress = BBGC.Features.BackPress();
            acceptButton.onClick.AddListener(OnAcceptButtonClick);
            canvasGroup.alpha = 0f;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(canvasGroup.gameObject);
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;
        }

        protected override void ProcessShow()
        {
            if (backPress != null) backPress.AddListener(this);
            gameObject.SetActive(true);
            canvasGroup.alpha = 0f;
            LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (backPress != null) backPress.RemoveListener(this);
            LeanTween.cancel(canvasGroup.gameObject);
            LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f).setOnComplete(() =>
            {
                if (alert != null) alertsFeature.HideAlert(alert);
                alert = null;
                Destroy(this.gameObject);
            });
        }

        bool IBackPressListener.OnBackPress() { return true; }

        private void UpdateView()
        {
            if (Alert == null) return;
            headerText.text = Alert.title;
            descriptionText.text = Alert.desc;
            acceptButtonText.text = Alert.button;
            acceptButton.gameObject.SetActive(Alert.onAccept != null);
        }

        private void OnAcceptButtonClick()
        {
            if (alert == null) return;

            alert.onAccept.SafeInvoke();
            Hide();
        }

    }

}