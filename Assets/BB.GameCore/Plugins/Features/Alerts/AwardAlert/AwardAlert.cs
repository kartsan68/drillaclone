﻿namespace BlackBears.GameCore.Features.Alerts
{
	public struct BonusAlertContent
    {
		long currency;
		bool hasOtherStuff;

        public BonusAlertContent(long currency, bool hasOtherStuff = false)
        {
			this.currency = currency;
			this.hasOtherStuff = hasOtherStuff;
        }

        public long Currency { get { return currency; } }
		public bool HasOtherStuff { get { return hasOtherStuff; } }
    }

	public class BonusAlert : Alert 
	{
		public readonly BonusAlertContent content;

		public BonusAlert(BonusAlertContent content)
		{
			this.content = content;
		}
	}
}

