﻿using BlackBears.GameCore.Views;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{
    internal class AwardAlertController : AlertController
    {
		[SerializeField] private CanvasGroup canvasGroup;
		[SerializeField] private Button takeButton;
        [SerializeField] private GameObject currencyRewardContainer;
        [SerializeField] private Text currencyText;
        [SerializeField] private SpritePackView currencyView;

        internal BonusAlert alert;

		private AlertsFeature alertsFeature;

		private void Awake() 
		{
            alertsFeature = BBGC.Features.Alerts();
            currencyView.SetSpritePack(BBGC.Features.CoreDefaults().HardCurrencyIconPack);

			takeButton.onClick.AddListener(OnTakeButtonClick);

			canvasGroup.alpha = 0f;
		}

		private void OnDestroy()
        {
            LeanTween.cancel(canvasGroup.gameObject);

            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;
        }

        protected override void ProcessShow()
        {
            currencyText.text = string.Format("+{0}", alert.content.Currency);
            currencyRewardContainer.SetActive(!alert.content.HasOtherStuff);

			LeanTween.alphaCanvas(canvasGroup, 1f, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (alert != null) alertsFeature.HideAlert(alert);
            alert = null;

			LeanTween.alphaCanvas(canvasGroup, 0f, 0.2f).setOnComplete((() => Destroy(gameObject)));
        }

		private void OnTakeButtonClick()
		{
			if (Showed) Hide();
		}
    }
}
