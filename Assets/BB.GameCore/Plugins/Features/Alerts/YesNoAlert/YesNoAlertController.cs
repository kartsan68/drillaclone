using System;
using System.Collections;
using BlackBears.GameCore.Features.BackPress;
using BlackBears.GameCore.Graphic;
using BlackBears.Resolutor;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.Alerts
{

    [RequireComponent(typeof(RectTransform))]
    internal class YesNoAlertController : AlertController, IBackPressListener
    {

        [SerializeField] private Image alertBack;
        [SerializeField] private Color alertBackColor = ColorUtils.FromHex(0x232C3D80);
        [SerializeField] private RectTransform content;

        [SerializeField] private Text header;
        [SerializeField] private Text message;
        [SerializeField] private Text okButtonText;
        [SerializeField] private Text cancelButtonText;
        [SerializeField] private Image background;
        [SerializeField] private Image okButtonImage;

        [SerializeField] private Button okButton;
        [SerializeField] private Button cancelButton;

        private AlertsFeature alertsFeature;
        private BackPressFeature backPress;
        private Atlas atlas;

        private YesNoAlert alert;

        public YesNoAlert Alert
        {
            get { return alert; }
            set
            {
                alert = value;
                UpdateView();
            }
        }

        private void Awake()
        {
            alertsFeature = BBGC.Features.GetFeature<AlertsFeature>();
            backPress = BBGC.Features.GetFeature<BackPressFeature>();

            atlas = UICache.Instance.TakeAtlas(CoreCache.key);

            okButton.onClick.AddListener(OnOkButtonClick);
            cancelButton.onClick.AddListener(OnCancelButtonClick);
        }

        protected override void Start()
        {
            base.Start();
            alertBack.color = Color.clear;
        }

        private void OnDestroy()
        {
            LeanTween.cancel(content.gameObject);
            LeanTween.cancel(alertBack.gameObject);
        }

        protected override void ProcessShow()
        {
            if (backPress != null) backPress.AddListener(this);

            gameObject.SetActive(true);

            var color = alertBackColor;
            color.a = 0;
            alertBack.color = color;

            content.anchoredPosition = new Vector2(0, -content.rect.height);
            LeanTween.moveAnchored(content, Vector2.zero, 0.2f).setEaseOutQuad();
            LeanTween.alphaGraphic(alertBack, alertBackColor.a, 0.2f);
        }

        protected override void ProcessHide()
        {
            if (backPress != null) backPress.RemoveListener(this);
            LeanTween.moveAnchored(content, new Vector2(0, -content.rect.height), 0.2f).setEaseInQuad();
            LeanTween.alphaGraphic(alertBack, 0f, 0.2f).setOnComplete(() =>
            {
                gameObject.SetActive(false);
                if (alertsFeature != null) alertsFeature.ReturnController(this);
                else Destroy(this.gameObject);
            });
        }

        private void UpdateView()
        {
            if (Alert == null) return;

            header.text = Alert.header ?? string.Empty;
            header.text = header.text.ToUpper();

            message.text = Alert.message;

            okButtonText.text = Alert.actionButtonText ?? string.Empty;
            okButtonText.text = okButtonText.text.ToUpper();

            cancelButtonText.text = Alert.cancelButtonText ?? string.Empty;
            cancelButtonText.text = cancelButtonText.text.ToUpper();

            var viewData = alertsFeature.Config.AlertsParams.ViewDataByType(alert.type);
            if (viewData == null) return;

            header.color = viewData.HeaderColor;
            message.color = viewData.TextColor;
            okButtonText.color = viewData.OkButtonTextColor;
            cancelButtonText.color = viewData.CancelButtonTextColor;
            background.color = viewData.BackgroundColor;

            okButtonImage.sprite = atlas[viewData.ButtonSpriteName];
        }

        private void OnOkButtonClick()
        {
            if (alert == null) return;
            var a = alert;

            alert = null;
            a.onActionCallback.SafeInvoke();
            alertsFeature.HideAlert(a);
            Hide();
        }

        private void OnCancelButtonClick()
        {
            if (alert == null) return;
            var a = alert;

            alert = null;
            a.onCancelCallback.SafeInvoke();
            alertsFeature.HideAlert(a);
            Hide();
        }

        bool IBackPressListener.OnBackPress()
        {
            OnCancelButtonClick();
            return true;
        }

    }

}