﻿using UnityEngine;

namespace BlackBears.GameCore.Features.Alerts
{

	[System.Serializable]
    internal class YesNoAlertViewData
    {

		[SerializeField] private AlertType alertType;

		[SerializeField] private Color headerColor = Color.white;
		[SerializeField] private Color textColor = Color.white;
		[SerializeField] private Color okButtonTextColor = Color.white;
		[SerializeField] private Color cancelButtonTextColor = Color.white;
		[SerializeField] private Color backgroundColor = Color.white;
		[SerializeField] private string buttonSpriteName;

		internal AlertType AlertType { get { return alertType; } }
		internal Color HeaderColor { get { return headerColor; } }
		internal Color TextColor { get { return textColor; } }
		internal Color OkButtonTextColor { get { return okButtonTextColor; } }
		internal Color CancelButtonTextColor { get { return cancelButtonTextColor; } }
		internal Color BackgroundColor { get { return backgroundColor; } }
		internal string ButtonSpriteName { get { return buttonSpriteName; } }

    }

}