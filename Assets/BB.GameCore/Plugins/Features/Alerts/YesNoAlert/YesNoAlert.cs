namespace BlackBears.GameCore.Features.Alerts
{

    public enum AlertType
    {
        Positive,
        Negative,
        Destructive
    }

    public class YesNoAlert : Alert
    {

        public readonly AlertType type;
        public readonly string header;
        public readonly string message;
        public readonly string actionButtonText;
        public readonly string cancelButtonText;

        public readonly Block onActionCallback;
        public readonly Block onCancelCallback;

        public YesNoAlert(AlertType type, string header, string message, string actionButtonText,
            string cancelButtonText, Block onActionCallback, Block onCancelCallback)
        {
            this.type = type;
            this.header = header;
            this.message = message;
            this.actionButtonText = actionButtonText;
            this.cancelButtonText = cancelButtonText;
            this.onActionCallback = onActionCallback;
            this.onCancelCallback = onCancelCallback;
        }

    }
}