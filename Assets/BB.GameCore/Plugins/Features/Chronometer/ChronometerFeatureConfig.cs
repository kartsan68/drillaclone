﻿#if BBGC_CHRONOMETER_FEATURE
using BlackBears.Chronometer;
#endif

namespace BlackBears.GameCore.Features.Chronometer
{

    [System.Serializable]
    public class ChronometerFeatureConfig : FeatureConfig
    {
        #if BBGC_CHRONOMETER_FEATURE

        [UnityEngine.SerializeField] private TimeModuleParams timeModuleParams;

        internal TimeModuleParams TimeModuleParams { get { return timeModuleParams; } }

        #endif
    }

}
