﻿
using System;
using BlackBears.Chronometer;
using BlackBears.GameCore.AppEvents;
using SimpleJSON;

namespace BlackBears.GameCore.Features.Chronometer
{

#if BBGC_CHRONOMETER_FEATURE

    public sealed class ChronometerFeature : Feature, IAppPauseListener,
        IAppResumeListener, ICoreSaveable
    {

        private const string saveKey = "chrnmtr";

        private ChronometerFeatureConfig config;
        private IAppEvents appEvents;
        private Action<bool> pauseHandler;

        private Block initBlock;
        private FailBlock initFailBlock;

        public long ActualTime { get { return TimeModule.ActualTime; } }
        public long SessionTime { get { return TimeModule.SessionTime; } }
        public long OnlineTime { get { return TimeModule.OnlineTime; } }
        public long LastSaveTime { get { return TimeModule.LastSaveTime; } }
        public long AfterResumeTime { get { return TimeModule.AfterResumeTime; } }
        public long TotalTime { get { return TimeModule.TotalTime; } }

        internal ChronometerFeature(ChronometerFeatureConfig config, IAppEvents appEvents)
            : base((int)FeaturePriority.Chronometer, "Chronometer Feature")
        {
            this.config = config;
            this.appEvents = appEvents;
        }

        int IAppEventPriority.Priority => (int)AppEventPriority.Chronometer;

        string ICoreSaveable.SaveKey { get { return saveKey; } }

        public bool IsValid => TimeModule.IsValid;
        public bool IsOffline => TimeModule.IsOffline;
        
        internal void CheckOffline(Block onSuccess)
        {
            TimeModule.Instance.CheckOffline(onSuccess);
        }

        internal void ResetSessionTime()
        {
            TimeModule.Instance.ResetSessionTime();
        }

        void IAppPauseListener.OnAppPaused()
        {
            if (pauseHandler != null) pauseHandler(true);
        }

        void IAppResumeListener.OnAppResumed()
        {
            if (pauseHandler != null) pauseHandler(false);
        }

        JSONNode ICoreSaveable.GenerateSaveData()
        {
            if (TimeModule.Instance == null) return null;
            return TimeModule.Instance.ToJson();
        }

        void ICoreSaveable.PrepareFromLoad(JSONNode saveData)
        {
            if (TimeModule.Instance == null) return;
            TimeModule.Instance.FromJson(saveData);
        }

        void ICoreSaveable.LoadDataNotFinded() { }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            initBlock = onSuccess;
            initFailBlock = onFail;

            if (!TimeModule.IsInitiated) TimeModule.Init(config.TimeModuleParams);
            TimeModule.OnChronometerUpdate += OnChronometerInitUpdate;
            TimeModule.Validate();
        }

        private void OnChronometerInitUpdate(object sender, ChronometerEventArgs args)
        {
            TimeModule.OnChronometerUpdate -= OnChronometerInitUpdate;

            switch (args.eventType)
            {
                case ChronometerEventType.Validated:
                    OnInitializeFinish();
                    break;
                case ChronometerEventType.ValidatedOffline:
                    OnInitializeFinish();
                    break;
                default:
                    OnInitializeFailed(args);
                    break;
            }
        }

        private void OnInitializeFinish()
        {
            var callback = initBlock;
            initBlock = null;
            initFailBlock = null;

            appEvents.AddAppPauseListener(this);
            appEvents.AddAppResumeListener(this);
            pauseHandler = TimeModule.RequestPauseHandler();

            Initialized();
            callback.SafeInvoke();
        }

        private void OnInitializeFailed(ChronometerEventArgs args)
        {
            var callback = initFailBlock;
            initBlock = null;
            initFailBlock = null;
            NotInitialized();
            callback.SafeInvoke((int)args.eventType, name + ": " + args.message);
        }

    }

#endif

}