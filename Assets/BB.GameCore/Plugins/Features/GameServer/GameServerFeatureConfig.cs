namespace BlackBears.GameCore.Features.GameServer
{

    [System.Serializable]
    public class GameServerFeatureConfig : FeatureConfig
    {

        #if BBGC_GAME_SERVER_FEATURE

        [UnityEngine.SerializeField] private BlackBears.GameServer.GameServerConfiguration configuration;
        [UnityEngine.SerializeField] private string paramsFileName;

        internal BlackBears.GameServer.GameServerConfiguration Configuration => configuration;
        internal string ParamsFileName => paramsFileName;

        #endif

    }

}