#if BBGC_GAME_SERVER_FEATURE

using BlackBears.GameServer;

namespace BlackBears.GameCore.Features.GameServer
{

    public sealed class GameServerFeature : Feature
    {

        private GameServerFeatureConfig config;

        internal GameServerFeature(GameServerFeatureConfig config) : base((int)FeaturePriority.GameServer, "GameServer Feature")
        {
            this.config = config;
        }

        public ServerConnection Connection { get; private set; }

        public void Inject(PlayersManager playersManager)
        {
            Connection.Inject(playersManager);
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var files = BBGC.Features.SQBAFiles();
            var paramsText = files.TextByName(config.ParamsFileName);
            var parameters = new GameServerParams(SimpleJSON.JSON.Parse(paramsText));

            this.Connection = ServerConnection.Init(config.Configuration, parameters, BBGC.Features.GetFeature<JellyB.JellyBFeature>());
            Initialized();
            onSuccess.SafeInvoke();
        }

    }

}

#endif