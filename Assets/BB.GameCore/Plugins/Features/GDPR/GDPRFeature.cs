using System.Collections.Generic;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif
using BB.GameCore.Plugins.Features.GDPR;
using BlackBears.GameCore.Features.Alerts;
using UnityEngine;

namespace BlackBears.GameCore.Features.GDPR
{
    public class GDPRFeature : Feature
    {

        private const string gdprShowedKey = "BBGC_GDPR_SHOWED";

        private GDPRFeatureConfig config;
        private GDPRBehaviour behaviour;

        private string playerId;
        private string countryCode;
        private Block onGDPRAccepted;

        internal GDPRFeature(GDPRFeatureConfig config) : base((int)FeaturePriority.GDPR, "GDPR Feature")
        {
            this.config = config;
        }

        public void CheckForGDPR(Block onComplete, string playerId = "1")
        {
            this.playerId = playerId;
            CheckForGDPR(onComplete);
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            var go = new GameObject(string.Format("~~{0}", name));
            behaviour = go.AddComponent<GDPRBehaviour>();
            behaviour.Inject(config.Url, config.ApiKey);
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityEngine.Object.DontDestroyOnLoad(go);
#endif

            Initialized();
            onSuccess.SafeInvoke();
        }

        private void CheckForGDPR(Block onComplete)
        {
#if !BBGC_SQBA_FEATURE
            Debug.Log($"Can't check GDPR for player with id {playerId} because SQBA is not enabled");
            onComplete.SafeInvoke();
#else
            string gdprShowedId = PlayerPrefs.GetString(gdprShowedKey, null);
            bool showed = string.Equals(gdprShowedId, playerId);

            if (showed)
            {
                onComplete.SafeInvoke();
            }
            else
            {
                behaviour.GetCountry(country =>
                {
                    if (IsGDPRCountry(country))
                    {
                        this.countryCode = country;
                        if (onComplete != null) onGDPRAccepted += onComplete;
                        BBGC.Features.Alerts().AddAlert(new GDPRAlert(ApplyGDPR));
                    }
                    else
                    {
                        PlayerPrefs.SetString(gdprShowedKey, playerId);
                        PlayerPrefs.Save();
                        onComplete.SafeInvoke();
                    }
                }, (c, m) =>
                {
                    #if !UNITY_EDITOR
                    if (config.WorkWithoutInternet)
                    {
                        var country = GetLocalCountry();
                        if (IsGDPRCountry(country))
                        {
                            this.countryCode = country;
                            if (onComplete != null) onGDPRAccepted += onComplete;
                            BBGC.Features.Alerts().AddAlert(new GDPRAlert((onSuccess, onFail) =>
                            {
                                onSuccess.SafeInvoke();
                                onGDPRAccepted.SafeInvoke();
                                onGDPRAccepted = null;
                            }));
                        }
                        else
                        {
                            onComplete.SafeInvoke();
                        }

                        return;
                    }
                    #else
                       onComplete.SafeInvoke(); 
                       return;
                    #endif

                    Debug.LogFormat("CheckForGDPR: {0}; {1}.", c, m);

                    var alerts = BBGC.Features.Alerts();
                    var loc = BBGC.Features.Localization();

                    alerts.AddAlert(new ErrorAlert(
                        loc.Get("BBGC_INIT_ERROR_TITLE"),
                        loc.Get("BBGC_INIT_ERROR_DESC"),
                        loc.Get("BBGC_INIT_ERROR_BTN"),
                        () => CheckForGDPR(onComplete, playerId)
                    ));
                });
            }
#endif
        }

        private void ApplyGDPR(Block onSuccess, FailBlock onFail)
        {
#if BBGC_SQBA_FEATURE
            var sqba = BBGC.Features.SQBA();
            sqba.SignUpPrivacyPolicy(playerId, countryCode, status =>
            {
                onSuccess.SafeInvoke();
                onGDPRAccepted.SafeInvoke();
                onGDPRAccepted = null;
                PlayerPrefs.SetString(gdprShowedKey, playerId);
                PlayerPrefs.Save();
            }, onFail);
#endif
        }

        private bool IsGDPRCountry(string country)
        {
            var core = BBGC.Features.CoreDefaults();
            var countries = core.Countries;
            if (countries == null) return false;
            for (int i = 0; i < countries.Length; i++)
            {
                if (string.Equals(country, countries[i])) return true;
            }
            return false;
        }

        private string GetLocalCountry()
        {
            var countryCode = "";
#if UNITY_ANDROID
            using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
            {
                if (cls != null)
                {
                    using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
                    {
                        if (locale != null)
                        {
                            countryCode = locale.Call<string>("getCountry");
                        }
                    }
                }
            }
#elif UNITY_IOS
            var localVal = PreferenceLanguageString();
            countryCode = localVal.Substring(0, 2);
#endif
            return countryCode;
        }

#if UNITY_IOS
        [DllImport("__Internal")]
        extern static public string _getPreferenceLanguageString();
        public string PreferenceLanguageString()
        {
            string language = "";
            language = _getPreferenceLanguageString();
            return language.ToUpper();
        }
#endif

    }
}