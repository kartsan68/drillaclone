using System.Collections;
using BlackBears;
using BlackBears.GameCore.Networking;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

namespace BB.GameCore.Plugins.Features.GDPR
{

    internal class GDPRBehaviour : MonoBehaviour
    {

        private const string secretKey = "key";

        private string url;
        private string secret;

        internal void Inject(string url, string secret)
        {
            this.url = url;
            this.secret = secret;
        }

        internal void GetCountry(Block<string> onSuccess, FailBlock onFail)
        {
            StartCoroutine(GetCountryAsync(onSuccess, onFail));
        }

        private IEnumerator GetCountryAsync(Block<string> onSuccess, FailBlock onFail)
        {
            var requestUrl = string.Format("{0}?{1}={2}", url, secretKey, secret);
            using (var request = UnityWebRequest.Get(requestUrl))
            {
                request.timeout = 5;
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    onFail.SafeInvoke(-1, request.error);
                    yield break;
                }

                var answerNode = JSON.Parse(request.downloadHandler.text);
                var country = answerNode["countryCode"].Value;
                
                onSuccess.SafeInvoke(country);
            }
        }

    }

}