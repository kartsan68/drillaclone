namespace BlackBears.GameCore.Features.GDPR
{
    [System.Serializable]
    public class GDPRFeatureConfig
    {
        
        [UnityEngine.SerializeField] private string url = "https://pro.ip-api.com/json/";
        [UnityEngine.SerializeField] private string apiKey = "LNnxXoQp1TIvyFp";
        [UnityEngine.SerializeField] private bool workWithoutInternet = true;

        internal string Url { get { return url; } }
        internal string ApiKey { get { return apiKey; } }
        internal bool WorkWithoutInternet { get { return workWithoutInternet; } }

    }
}