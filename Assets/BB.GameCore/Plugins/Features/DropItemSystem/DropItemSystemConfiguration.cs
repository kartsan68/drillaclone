using UnityEngine;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    [CreateAssetMenu(fileName = "DropItemSystemConfig", menuName = "BlackBears/DropItems/SystemConfig", order = 1)]
    public class DropItemSystemConfiguration : ScriptableObject
    {

        [SerializeField] private float separationRadius = 50;
        [SerializeField] private float minPauseMoveSpeed = 10f;
        [SerializeField] private float maxPauseMoveSpeed = 15f;

        internal float SeparationRadius { get { return separationRadius; } }
        internal float MinPauseMoveSpeed { get { return minPauseMoveSpeed; } }
        internal float MaxPauseMoveSpeed { get { return maxPauseMoveSpeed; } }

    }

}