﻿#if BBGC_DROP_ITEM_SYSTEM_FEATURE
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    public partial class DropItemSystem : MonoBehaviour
    {

        private const int maxItems = 15;

        private const float pauseBetweenItems = 0.1f;

        private const float appearTime = 0.3f;
        private const float halfAppearTime = appearTime * 0.5f;
        private const float disappearTime = 0.6f;
        private const float halfDisappearTime = disappearTime * 0.5f;
        private const float alphaDisappearTime = disappearTime * 0.05f;

        private DropItemSystemConfiguration config;

        private int amountItems = 0;

        private Camera targetCamera;
        private LinkedList<DropItemReceiver> receivers = new LinkedList<DropItemReceiver>();
        private LinkedList<DropItemViewBuilder> viewBuilders = new LinkedList<DropItemViewBuilder>();
        private HashSet<LTSeq> runningSequences = new HashSet<LTSeq>();

        internal DropItemSystemConfiguration Config { set { config = value; } }

        private void Awake()
        {
            var canvas = GetComponentInParent<Canvas>();
            targetCamera = canvas.worldCamera;
        }

        private void OnDestroy()
        {
            var sequencesArray = new LTSeq[runningSequences.Count];
            runningSequences.CopyTo(sequencesArray);
            foreach (var sequence in sequencesArray) sequence.reset();
            runningSequences.Clear();
        }

        public void DropItem(DropItemData dropItem, DropItemReceiver receiver = null, RectTransform parent = null, int maxItemsAmount = -1)
        {
            if (dropItem == null) return;

            var builder = GetViewBuilder(dropItem);
            if(builder == null) return;

            int aviableAmount = maxItemsAmount == -1 ? maxItems : maxItemsAmount;
            var views = builder.TakeItemsArray(dropItem, aviableAmount - amountItems, parent);
            if (views == null) return;

            var startPos = WorldToTargetLocalPoint(dropItem.GlobalPos, dropItem.TargetCamera, parent ?? builder.Parent);
            receiver = receiver ?? GetDropReceiver(dropItem);
            AnimateViews(views, startPos, receiver, dropItem, parent ?? builder.Parent);
        }

        public void RegisterReceiver(DropItemReceiver receiver)
        {
            if (receiver == null) return;
            receivers.Remove(receiver);
            receivers.AddFirst(receiver);
        }

        public void UnregisterReceiver(DropItemReceiver receiver)
        {
            receivers.Remove(receiver);
        }

        internal void RegisterViewBuilder(DropItemViewBuilder builder)
        {
            if (builder == null) return;
            viewBuilders.Remove(builder);
            viewBuilders.AddFirst(builder);
        }

        internal void UnregisterViewBuilder(DropItemViewBuilder builder)
        {
            viewBuilders.Remove(builder);
        }

        private void AnimateViews(DropItemView[] views, Vector2 startPos, DropItemReceiver receiver, DropItemData dropItem, 
            RectTransform parent = null)
        {
            for (int i = 0; i < views.Length; i++)
            {
                amountItems += 1;

                var itemView = views[i];
                if(itemView == null) continue;
                
                itemView.rectTransform.localPosition = startPos;
                itemView.rectTransform.localScale = Vector3.forward;

                float separationRadius = dropItem.SeparationRadius == 0 ? config.SeparationRadius : dropItem.SeparationRadius;
                Vector2 startOffsetPos = itemView.rectTransform.anchoredPosition + Random.insideUnitCircle * separationRadius;
                float appearPause = (dropItem.PauseBetweenItems == -1 ? pauseBetweenItems : dropItem.PauseBetweenItems) * i;
                float waitNextPauseTime = 0.2f + (dropItem.PauseBetweenItems == -1 ? pauseBetweenItems : dropItem.PauseBetweenItems)
                     * (views.Length - i) + appearPause;

                AnimationSettings settings;
                settings.appearPause = appearPause;
                settings.appearTime = appearTime;
                settings.disappearTime = disappearTime;
                settings.startOffsetPos = startOffsetPos;
                settings.waitNextPauseTime = waitNextPauseTime;
                settings.halfAppearTime = halfAppearTime;
                settings.alphaDisappearTime = alphaDisappearTime;

                //Move
                var waitDelta = waitNextPauseTime * Random.Range(config.MinPauseMoveSpeed, config.MaxPauseMoveSpeed);
                var moveSeq = itemView.moveAnimationSequence(settings, waitDelta, CalculateReceiverPos(receiver, parent));

                moveSeq.append(() => ItemDelivered(dropItem, itemView, receiver));
                moveSeq.append(() => RemoveSequence(moveSeq));

                //Scale
                var scaleSeq = itemView.scaleAnimationSequence(settings, dropItem);
                runningSequences.Add(scaleSeq);

                scaleSeq.append(() => RemoveSequence(scaleSeq));

                //Alpha
                var alphaSeq = itemView.alphaAnimationSequence(settings, dropItem);
                runningSequences.Add(alphaSeq);

                alphaSeq.append(() => RemoveSequence(alphaSeq));
            }
        }

        private void RemoveSequence(LTSeq seq)
        {
            runningSequences.Remove(seq);
        }

        private void ItemDelivered(DropItemData dropItem, DropItemView itemView, DropItemReceiver receiver)
        {
            amountItems -= 1;

            var itemViewBuilder = GetViewBuilder(dropItem);
            if(itemViewBuilder == null) return;
            itemViewBuilder.AddItem(itemView);

            if (itemView?.gameObject == null) return;
            itemView.gameObject.SetActive(false);

            if (receiver != null) receiver.ItemDelivered();
        }

        private DropItemReceiver GetDropReceiver(DropItemData itemData)
        {
            foreach (var receiver in receivers)
            {
                if (!string.Equals(receiver.DropTag, itemData.DropTag)) continue;
                if (!receiver.TypeEquals(itemData)) continue;

                return receiver;
            }

            return null;
        }

        private DropItemViewBuilder GetViewBuilder(DropItemData dropItem)
        {
            foreach (var builder in viewBuilders)
            {
                if (!string.Equals(builder.DropTag, dropItem.DropTag)) continue;
                return builder;
            }

            return null;
        }

        private Vector2 CalculateReceiverPos(DropItemReceiver receiver, RectTransform parent = null)
        {
            if (receiver == null) return Vector2.zero;

            Vector2 pos = receiver.DropPosition;
            if (!receiver.DropPositionIsScreenPoint) pos = WorldToScreenPoint(pos, targetCamera);
            return ScreenToLocalPoint(pos, parent);
        }

        private Vector2 WorldToTargetLocalPoint(Vector3 globalPos, Camera camera, RectTransform parent = null)
        {
            var pos = WorldToScreenPoint(globalPos, camera);
            return ScreenToLocalPoint(pos, parent);
        }

        private Vector2 WorldToScreenPoint(Vector2 pos, Camera camera)
        {
            if (camera == null) camera = targetCamera;
            return RectTransformUtility.WorldToScreenPoint(camera, pos);
        }

        private Vector2 ScreenToLocalPoint(Vector2 pos, RectTransform parent = null)
        {
            Vector2 result;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parent, pos, targetCamera, out result);
            return result;
        }

    }

}
#endif