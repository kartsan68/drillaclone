#if BBGC_DROP_ITEM_SYSTEM_FEATURE

using UnityEngine;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    public sealed class DropItemSystemFeature : Feature
    {

        private DropItemSystemFeatureConfig config;

        private DropItemSystem system;
        private bool isActive;

        public DropItemSystem System => isActive ? system : null;
        public bool IsActive => isActive;

        internal DropItemSystemFeature(DropItemSystemFeatureConfig config)
        : base((int)FeaturePriority.DropItemSystem, "DropItemSystem Feature")
        {
            this.config = config;
        }

        internal void UpdateInfo()
        {
            var systemObjects = GameObject.FindGameObjectsWithTag(config.Tag);
            foreach (var systemObject in systemObjects)
            {
                if (systemObject != null)
                {
                    system = systemObject.GetComponent<DropItemSystem>();
                    if (system != null)
                    {
                        system.Config = config.Configuration;
                        isActive = true;
                        break;
                    }
                }
            }

        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            isActive = false;

            UpdateInfo();

            Initialized();
            onSuccess.SafeInvoke();
        }

    }

}

#endif