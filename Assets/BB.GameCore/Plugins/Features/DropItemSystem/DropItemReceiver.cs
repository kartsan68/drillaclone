#if BBGC_DROP_ITEM_SYSTEM_FEATURE
using System;
using BlackBears;
using UniRx;
using UnityEngine;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    public abstract class DropItemReceiver : MonoBehaviour
    {

        [SerializeField] private bool unregisterOnDisable;
        [SerializeField] private bool autoregister = true;
        [Tooltip("Нужно, если рендер происходит какой-то нестандартной камерой")]
        [SerializeField] private Camera targetCamera;

        private DropItemSystem system;
        private bool registered;

        private Subject<Null> onItemDelivered = new Subject<Null>();

        public abstract string DropTag { get; }
        public bool DropPositionIsScreenPoint { get { return targetCamera != null; } }
        public Vector2 DropPosition
        {
            get
            {
                if (DropPositionIsScreenPoint) return RectTransformUtility.WorldToScreenPoint(targetCamera, transform.position);
                else return transform.position;
            }
        }

        public IObservable<Null> OnItemDelivered { get { return onItemDelivered; } }

        private void Awake()
        {
            system = BBGC.Features.GetFeature<DropItemSystemFeature>().System;
        }

        public abstract bool TypeEquals(DropItemData itemData); 

        public void ItemDelivered()
        {
            onItemDelivered.OnNext(null);
        }

        private void OnEnable()
        {
            if (registered || !autoregister || system == null) return;
            system.RegisterReceiver(this);
            registered = true;
        }

        private void OnDisable()
        {
            if (!registered || !unregisterOnDisable || system == null) return;
            system.UnregisterReceiver(this);
            registered = false;
        }

        private void OnDestroy()
        {
            if (system == null) return;

            system.UnregisterReceiver(this);
        }

    }

}
#endif