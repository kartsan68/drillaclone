#if BBGC_DROP_ITEM_SYSTEM_FEATURE

using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    internal abstract class DropItemViewBuilder : MonoBehaviour
    {

        [SerializeField] private bool unregisterOnDisable;
        [SerializeField] private bool autoregister = true;

        protected Queue<DropItemView> viewPool = new Queue<DropItemView>();
        private bool registered;
        private bool started;
        private DropItemSystem system;

        protected bool isDisposed;

        public abstract string DropTag { get; }
        public abstract RectTransform Parent { get; }

        protected virtual void Start()
        {
            started = true;

            system = BBGC.Features.GetFeature<DropItemSystemFeature>()?.System;
            Register();
        }

        internal DropItemView[] TakeItemsArray(DropItemData itemData, int itemsRange, RectTransform parent = null)
        {
            var itemsCount = Mathf.Min(itemData.RandomItemsCount, itemsRange);
            if (itemsCount <= 0) return null;
            var resultArray = new DropItemView[itemsCount];

            for (int i = 0; i < itemsCount; i++) resultArray[i] = TakeItem(itemData, parent);

            return resultArray;
        }

        internal void AddItem(DropItemView itemView)
        {
            viewPool.Enqueue(itemView);
        }

        internal abstract DropItemView TakeItem(DropItemData itemData, RectTransform parent = null);

        protected void OnEnable()
        {
            if(started) Register();
        }

        protected void OnDisable()
        {
            if (!registered || !unregisterOnDisable || system == null) return;
            system.UnregisterViewBuilder(this);
            registered = false;
        }

        protected void OnDestroy()
        {
            isDisposed = true;
            if (system == null) return;
            system.UnregisterViewBuilder(this);
        }

        private void Register()
        {
            if (registered || !autoregister || system == null) return;
            system.RegisterViewBuilder(this);
            registered = true;
        }

    }

}
#endif
