namespace BlackBears.GameCore.Features.DropItemSystem
{

    [System.Serializable]
    public class DropItemSystemFeatureConfig : FeatureConfig
    {

        #if BBGC_DROP_ITEM_SYSTEM_FEATURE

        [UnityEngine.SerializeField] private DropItemSystemConfiguration configuration;
        [UnityEngine.SerializeField] private string tag;

        internal DropItemSystemConfiguration Configuration { get { return configuration; } }
        internal string Tag { get { return tag; } }

        #endif

    }

}