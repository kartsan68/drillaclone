using UnityEngine;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    public abstract class BaseDropItemData
    {

        protected BaseDropItemData(Vector3 globalPos, Camera targetCamera = null,
            float separationRadius = 0, float pauseBetweenItems = -1)
        {
            this.GlobalPos = globalPos;
            this.TargetCamera = targetCamera;
            this.SeparationRadius = separationRadius;
            this.PauseBetweenItems = pauseBetweenItems;
        }

        public float SeparationRadius { get; private set; }
        public Vector3 GlobalPos { get; private set; }
        public Camera TargetCamera { get; private set; }
        public float PauseBetweenItems { get; private set; }

        public abstract int MinItemsCount { get; }
        public abstract int MaxItemsCount { get; }

        public virtual int RandomItemsCount
        {
            get { return Random.Range(MinItemsCount, MaxItemsCount); }
        }

    }

    public abstract class DropItemData : BaseDropItemData
    {

        protected DropItemData(string itemTag, Vector3 globalPos, Camera targetCamera = null,
            float separationRadius = 0, float pauseBetweenItems = -1)
            : base(globalPos, targetCamera, separationRadius, pauseBetweenItems)
        {
            this.DropTag = itemTag;
        }

        public string DropTag { get; private set; }

    }

}