using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Features.DropItemSystem
{

    internal struct AnimationSettings
    {

        public float appearTime;
        public float appearPause;
        public Vector2 startOffsetPos;
        public float waitNextPauseTime;
        public float disappearTime;
        public float halfAppearTime;
        public float alphaDisappearTime;

    }

    internal class DropItemView
    {

        public readonly GameObject gameObject;
        public readonly RectTransform rectTransform;
        public readonly Image mainImage;
        public readonly CanvasGroup canvasGroup;

        public DropItemView(GameObject gameObject, RectTransform rectTransform, Image mainImage,
            CanvasGroup canvasGroup)
        {
            this.gameObject = gameObject;
            this.rectTransform = rectTransform;
            this.mainImage = mainImage;
            this.canvasGroup = canvasGroup;
        }

        public virtual LTSeq moveAnimationSequence(AnimationSettings settings, float waitDelta, Vector2 receiverPos)
        {
            return ViewAnimation.moveSequence(this, settings, waitDelta, receiverPos);
        }

        public virtual LTSeq scaleAnimationSequence(AnimationSettings settings, DropItemData dropItem = null)
        {
            return ViewAnimation.scaleSequence(this, settings);
        }

        public virtual LTSeq alphaAnimationSequence(AnimationSettings settings, DropItemData dropItem = null)
        {
            return ViewAnimation.alphaSequence(this, settings);
        }

        private class ViewAnimation
        {

            public static LTSeq moveSequence(DropItemView itemView, AnimationSettings settings, float waitDelta, Vector2 receiverPos)
            {
                var sequence = LeanTween.sequence();
                sequence.append(settings.appearPause);
                sequence.append(LeanTween.moveAnchored(itemView.rectTransform, settings.startOffsetPos, settings.appearTime));
                sequence.append(LeanTween.moveAnchoredY(itemView.rectTransform, settings.startOffsetPos.y - waitDelta, settings.waitNextPauseTime));
                sequence.append(() =>
                {
                    if (itemView.gameObject == null) return;
                    LeanTween.moveLocal(itemView.gameObject, receiverPos, settings.disappearTime).setEaseInExpo();
                });
                sequence.append(settings.disappearTime);

                return sequence;
            }

            public static LTSeq scaleSequence(DropItemView itemView, AnimationSettings settings)
            {
                var sequence = LeanTween.sequence();
                sequence.append(settings.appearPause);
                sequence.append(LeanTween.scale(itemView.rectTransform, Vector3.one, settings.halfAppearTime));

                return sequence;
            }

            public static LTSeq alphaSequence(DropItemView itemView, AnimationSettings settings)
            {
                var sequence = LeanTween.sequence();
                sequence.append(settings.appearPause);
                sequence.append(LeanTween.alphaCanvas(itemView.canvasGroup, 1f, settings.halfAppearTime));
                sequence.append(settings.waitNextPauseTime + settings.halfAppearTime + (settings.disappearTime - settings.alphaDisappearTime));
                sequence.append(LeanTween.alphaCanvas(itemView.canvasGroup, 0f, settings.alphaDisappearTime));

                return sequence;
            }

        }

    }

}
