#if BBGC_DROP_ITEM_SYSTEM_FEATURE
using UnityEngine;
using UniRx;
using System;
using BlackBears;

namespace BlackBears.GameCore.Features.DropItemSystem.Fx
{

    public class DropReceiveScaleFx : MonoBehaviour
    {

        [SerializeField] private DropItemReceiver receiver;
        [SerializeField] private Vector3 animScale = new Vector3(1.1f, 1.1f, 1f);
        [SerializeField] private float animTime = 0.2f;

        private Vector3 startScale;

        private IDisposable subscription;
        private LTSeq currentSequence;

        private void Start()
        {
            if (receiver == null) 
            {
                Destroy(this);
                return;
            }

            startScale = transform.localScale;
            receiver.OnItemDelivered.Subscribe(OnItemDeliver);
        }

        private void OnDestroy()
        {
            if (subscription != null) subscription.Dispose();
            subscription = null;
            if (currentSequence != null) currentSequence.reset();
            currentSequence = null;
        }

        private void OnItemDeliver(Null @null)
        {
            if (currentSequence != null) currentSequence.reset();

            currentSequence = LeanTween.sequence();
            currentSequence.append(LeanTween.scale(gameObject, animScale, animTime * 0.5f));
            currentSequence.append(LeanTween.scale(gameObject, startScale, animTime * 0.5f));
            currentSequence.append(AnimationFinished);
        }

        private void AnimationFinished()
        {
            currentSequence = null;
        }

    }

}
#endif