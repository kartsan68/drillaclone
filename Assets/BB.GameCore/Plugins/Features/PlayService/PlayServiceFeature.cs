#if BBGC_USE_PLAY_SERVICE
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using UnityEngine;

namespace BlackBears.GameCore.Features.PlayService
{
	public class PlayServiceFeature : Feature
	{
		public PlayServiceFeature() : base((int) FeaturePriority.PlayService, "Play service feature")
		{
		}

		protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
		{
#if UNITY_ANDROID && BBGC_USE_PLAY_SERVICE

			PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
				// enables saving game progress.
				.EnableSavedGames()
				// registers a callback to handle game invitations received while the game is not running.
				/*.WithInvitationDelegate(lel)
				// registers a callback for turn based match notifications received while the
				// game is not running.
				.WithMatchDelegate(lel)
				// requests the email address of the player be available.
				// Will bring up a prompt for consent.
				.RequestEmail()
				// requests a server auth code be generated so it can be passed to an
				//  associated back end server application and exchanged for an OAuth token.
				.RequestServerAuthCode(false)
				// requests an ID token be generated.  This OAuth token can be used to
				//  identify the player to other services such as Firebase.
				.RequestIdToken()*/
				.Build();

			PlayGamesPlatform.InitializeInstance(config);
			// recommended for debugging:
			PlayGamesPlatform.DebugLogEnabled = true;
			// Activate the Google Play Games platform
			PlayGamesPlatform.Activate();
			//onSuccess.SafeInvoke();
			SignIn(b => { onSuccess.SafeInvoke(); });
#else
			onSuccess.SafeInvoke();
#endif
		}

		/// <summary>
		/// Вход в игровые сервисы Google. 
		/// </summary>
		/// <param name="onSuccess"></param>
		public void SignIn(Block<bool> onSuccess)
		{
			Social.localUser.Authenticate(onSuccess.SafeInvoke);
		}
	}
}
