using UnityEngine;

namespace BlackBears.GameCore.Features.Localization
{

    public enum Modificator
    {
        DontModify, ToUpper, ToLower, ToUpperFirst, ToTitle
    }

    public abstract class Localizator : ScriptableObject
    {
        public abstract string Get(string key, Modificator modificator = Modificator.DontModify, string launguage = null);
        public abstract string Get(string key, long count0, Modificator modificator = Modificator.DontModify, string launguage = null);
        public abstract string Get(string key, long count0, long count1, Modificator modificator = Modificator.DontModify, string launguage = null);
        public abstract string Get(string key, long count0, long count1, long count2, Modificator modificator = Modificator.DontModify, string launguage = null);

        public abstract string Get(string key, string count0, Modificator modificator = Modificator.DontModify, string launguage = null);
        public abstract string Get(string key, string count0, string count1, Modificator modificator = Modificator.DontModify, string launguage = null);
        public abstract string Get(string key, string count0, string count1, string count2, Modificator modificator = Modificator.DontModify, string launguage = null);
    }

}