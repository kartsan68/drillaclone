namespace BlackBears.GameCore.Features.Localization
{

    public class LocalizationFeature : Feature
    {

        private Localizator localizator;

        internal LocalizationFeature(LocalizationFeatureConfig config)
            : base((int)FeaturePriority.Localization, "Localization Feature")
        {
            localizator = config.Localizator;
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            Initialized();
            onSuccess.SafeInvoke();
        }

        
        public string Get(string key, Modificator modificator = Modificator.DontModify, string launguage = null)
        {
            return localizator != null ? localizator.Get(key, modificator, launguage) : key;
        }

        public string Get(string key, string count0, Modificator modificator = Modificator.DontModify, string launguage = null)
        {
            return localizator != null ? localizator.Get(key, count0, modificator, launguage) : key;
        }

        public string Get(string key, long count0, Modificator modificator = Modificator.DontModify, string launguage = null)
        {
            return localizator != null ? localizator.Get(key, count0, modificator, launguage) : key;
        }

        public string Get(string key, string count0, string count1, Modificator modificator = Modificator.DontModify, string launguage = null)
        {
            return localizator != null ? localizator.Get(key, count0, count1, modificator, launguage) : key;
        }
        public string Get(string key, long count0, long count1, Modificator modificator = Modificator.DontModify, string launguage = null)
        {
            return localizator != null ? localizator.Get(key, count0, count1, modificator, launguage) : key;
        }

        public string Get(string key, long count0, long count1,long count2, Modificator modificator = Modificator.DontModify, string launguage = null)
        {
            return localizator != null ? localizator.Get(key, count0, count1, count2, modificator, launguage) : key;
        }

    }

}