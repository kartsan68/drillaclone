using UnityEngine;

namespace BlackBears.GameCore.Features.Localization
{

    [System.Serializable]
    public class LocalizationFeatureConfig
    {
        
        [SerializeField] private Localizator localizator;

        internal Localizator Localizator { get { return localizator; } }

    }

}