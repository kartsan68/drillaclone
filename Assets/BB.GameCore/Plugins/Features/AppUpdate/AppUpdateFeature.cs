using BlackBears.GameCore.Features.Alerts;

namespace BlackBears.GameCore.Features.AppUpdate
{
    public sealed class AppUpdateFeature : Feature
    {

        internal AppUpdateFeature()
            : base((int)FeaturePriority.AppUpdate, "AppUpdate Feature")
        {
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            #if BBGC_SQBA_FEATURE
            var sqba = BBGC.Features.SQBA();
            if (sqba != null && sqba.NeedForceUpdate)
            {
                var alerts = BBGC.Features.Alerts();
                alerts.AddAlert(AppUpdateAlert.Instance);
                return;
            }
            #endif

            FinishInit(onSuccess);
        }

        private void FinishInit(Block callback)
        {
            Initialized();
            callback.SafeInvoke();
        }

    }
}