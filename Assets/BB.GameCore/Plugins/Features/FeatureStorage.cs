﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;


namespace BlackBears.GameCore.Features
{

    public sealed class FeatureStorage
    {

        private InitializeState initState;
        private Feature[] features;
        private MonoBehaviour monoBehaviour;

        internal IEnumerable<Feature> AllFeatures => features;

        internal FeatureStorage(Feature[] features)
        {
            if (features == null)
            {
                this.features = new Feature[0];
            }
            else
            {
                this.features = new Feature[features.Length];
                features.CopyTo(this.features, 0);
                Array.Sort(features);
            }
        }

        public TFeature GetFeature<TFeature>() where TFeature : Feature
        {
            if (features == null || features.Length == 0) return null;

            TFeature founded = null;
            foreach (var plugin in features)
            {
                founded = plugin as TFeature;
                if (founded != null) break;
            }
            return founded;
        }

        internal void Initialize(FeaturesInitData initData, Block onSuccess, FailBlock onFail, MonoBehaviour monoBehaviour)
        {
            if (initState != InitializeState.NotInitialized) return;

            initState = InitializeState.Initializing;

            this.monoBehaviour = monoBehaviour;

            monoBehaviour.StartCoroutine(InitializeFeature(0, initData, onSuccess, (c, m) => OnInitializeFailed(c, m, onFail)));
        }

        private IEnumerator InitializeFeature(int featureIndex, FeaturesInitData initData, 
            Block onSuccess, FailBlock onFail)
        {
            yield return null;
            if (featureIndex >= features.Length)
            {
                initState = InitializeState.Initialized;
                onSuccess.SafeInvoke();
                yield break;
            }

            var feature = features[featureIndex];
            feature.Initialize(initData,
                () => monoBehaviour.StartCoroutine(InitializeFeature(featureIndex + 1, initData, onSuccess, onFail)),
                onFail);
        }

        private void OnInitializeFailed(long errorCode, string errorMessage, FailBlock onFail)
        {
            initState = InitializeState.NotInitialized;
            onFail.SafeInvoke(errorCode, errorMessage);
        }

    }

    public static class FeatureStorageUtils
    {

        #if BBGC_CHRONOMETER_FEATURE
        public static Chronometer.ChronometerFeature Chronometer(this FeatureStorage storage)
        {
            return storage.GetFeature<Chronometer.ChronometerFeature>();
        }
        #endif

        #if BBGC_GAME_SERVER_FEATURE
        public static GameServer.GameServerFeature GameServer(this FeatureStorage storage)
        {
            return storage.GetFeature<GameServer.GameServerFeature>();
        }
        #endif

        #if BBGC_LEAGUES_FEATURE        
        public static Leagues.LeaguesFeature Leagues(this FeatureStorage storage)
        {
            return storage.GetFeature<Leagues.LeaguesFeature>();
        }
        #endif

        #if BBGC_CLANS_FEATURE
        public static Clans.ClansFeature Clans(this FeatureStorage storage)
        {
            return storage.GetFeature<Clans.ClansFeature>();
        }
        #endif

        #if BBGC_SQBA_FEATURE
        public static SQBA.SQBAFeature SQBA(this FeatureStorage storage)
        {
            return storage.GetFeature<SQBA.SQBAFeature>();
        }
        #endif

        public static Analytics.AnalyticsFeature Analytics(this FeatureStorage storage)
        {
            return storage.GetFeature<Analytics.AnalyticsFeature>();
        }

        public static SQBAFiles.SQBAFilesFeature SQBAFiles(this FeatureStorage storage)
        {
            return storage.GetFeature<SQBAFiles.SQBAFilesFeature>();
        }

        public static Alerts.AlertsFeature Alerts(this FeatureStorage storage)
        {
            return storage.GetFeature<Alerts.AlertsFeature>();
        }

        public static Notifications.NotificationsFeature Notifications(this FeatureStorage storage)
        {
            return storage.GetFeature<Notifications.NotificationsFeature>();
        }

        public static Localization.LocalizationFeature Localization(this FeatureStorage storage)
        {
            return storage.GetFeature<Localization.LocalizationFeature>();
        }

        public static BackPress.BackPressFeature BackPress(this FeatureStorage storage)
        {
            return storage.GetFeature<BackPress.BackPressFeature>();
        }

        public static CoreDefaults.CoreDefaultsFeature CoreDefaults(this FeatureStorage storage)
        {
            return storage.GetFeature<CoreDefaults.CoreDefaultsFeature>();
        }

        public static Vibration.VibrationFeature Vibration(this FeatureStorage storage)
        {
            return storage.GetFeature<Vibration.VibrationFeature>();
        }

        #if BBGC_LOCAL_NOTIFICATIONS
        public static LocalNotifications.LocalNotificationFeature LocalNotification(this FeatureStorage storage)
        {
            return storage.GetFeature<LocalNotifications.LocalNotificationFeature>();
        }
        #endif

        public static PlayService.PlayServiceFeature PlayService(this FeatureStorage storage)
        {
            return storage.GetFeature<PlayService.PlayServiceFeature>();
        }
    }

}