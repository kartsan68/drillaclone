using System;
using System.Collections.Generic;
using BlackBears.GameCore.AppEvents;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.Utils;

namespace BlackBears.GameCore.Features.Analytics
{

    public partial class AnalyticsFeature : Feature, IAppPauseListener, IAppQuitListener, IAnalyticsAdapter,IAppResumeListener
    {

        private List<Analytics> analytics;
        private IAppEvents appEvents;

        public int Priority => (int)AppEventPriority.Analytics;
        public IGameAdapter GameAdapter => gameAdapter ?? (gameAdapter = BBGC.Features.CoreDefaults().GameAdapter);


        private ChronometerFeature chronometerFeature;

        private CoreDefaultsFeature coreDefaultsFeature;
        private IGameAdapter gameAdapter;

        private ChronometerFeature ChronometerFeature => chronometerFeature ?? (chronometerFeature = BBGC.Features.GetFeature<ChronometerFeature>());

        private CoreDefaultsFeature CoreDefaultsFeature => coreDefaultsFeature ?? (coreDefaultsFeature = BBGC.Features.GetFeature<CoreDefaultsFeature>());

        private string Day => DateUtils.SecondsToFullDays(ChronometerFeature.TotalTime).ToString();

        private string Session => CoreDefaultsFeature.SessionsCount.ToString();

        internal AnalyticsFeature(AnalyticsFeatureConfig config, IAppEvents appEvents)
            : base((int)FeaturePriority.Analytics, "Analytics Feature")
        {
            this.analytics = config.Analytics;
            this.appEvents = appEvents;
        }

        protected override void InitProcess(FeaturesInitData initData, Block onSuccess, FailBlock onFail)
        {
            if (analytics != null && analytics.Count > 0)
            {
                foreach (var an in analytics)
                {
                    an.Init();
                }
            }
            Initialized();

            appEvents.AddAppPauseListener(this);
            appEvents.AddAppQuitListener(this);
            appEvents.AddAppResumeListener(this);
            onSuccess.SafeInvoke();
        }

        public Dictionary<string, object> CreateDictionaryParameters(bool addBasicParametrs = true)
        {
            var parameters = new Dictionary<string, object>();
            if (addBasicParametrs)
            {
                AddBasicParameters(parameters);
            }
            return parameters;
        }
        
        public virtual void AddBasicParameters(Dictionary<string, object> parameters)
        {
            try
            {
                if(GameAdapter?.PlayerId != null) parameters.Add("player_id", GameAdapter.PlayerId);
                if(GameAdapter?.ConfigVersion != null) parameters.Add("config_version", GameAdapter.ConfigVersion);
            }
            catch (ArgumentException e)
            {
                BlackBears.SQBA.Logger.LogMessage(e.Message);
            }
        }

        public void SendEvent(string message, Dictionary<string, object> parameters)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.SendEvent(message, parameters);
                }
            }
        }

        public void SendEvent(string message)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.SendEvent(message);
                }
                
            }
        }

        public void AddNumberToUserProfile(string key, double value)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.AddNumberToUserProfile(key, value);
                }
            }
        }

        public void AddStringToUserProfile(string key, string value)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.AddStringToUserProfile(key, value);
                }
            }
        }

        public void AddBoolToUserProfile(string key, bool value)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.AddBoolToUserProfile(key, value);
                }
            }
        }

        public void AddCounterToUserProfile(string key, double value)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.AddCounterToUserProfile(key, value);
                }
            }
        }

        public void ReportUserProfile()
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.ReportUserProfile();
                }
            }
        }

        public void SetUserProfileID(string id)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.SetUserProfileID(id);
                }
            }
        }

        public void OnAppResumed()
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.ResumeApp();
                }
            }
        }


        void IAppPauseListener.OnAppPaused()
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.ReportUserProfile();
                }
            }
        }

        void IAppQuitListener.OnAppQuit()
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.ReportUserProfile();
                }
            }
        }

        void IAnalyticsAdapter.PostEvent(string eventName, Dictionary<string, object> data, AdditionalProps additionalProps = default)
        {
            if (analytics == null) return;
            foreach (var an in analytics)
            {
                AddBasicParameters(data);
                AddProps(data, additionalProps);
                an.SendEvent(eventName, data);
            }
            
            
        }

        void IAnalyticsAdapter.AddUserProfileCounter(string attributeKey, double delta)
        {
            if (analytics != null)
            {
                foreach (var an in analytics)
                {
                    an.AddCounterToUserProfile(attributeKey, delta);
                }
            }
        }

        private void AddProps(Dictionary<string, object> data, AdditionalProps additionalProps)
        {
            try
            {
                if (additionalProps.day) data.Add("day", DateUtils.SecondsToFullDays(ChronometerFeature.TotalTime).ToString());
                if (additionalProps.currentSession) data.Add("session", CoreDefaultsFeature.SessionsCount);
            }
            catch (ArgumentException e) 
            { 
                BlackBears.SQBA.Logger.LogMessage(e.Message);
            }
        }
    }

}