using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.Analytics
{

    public abstract class Analytics : ScriptableObject
    {
        private IGameAdapter gameAdapter;
        public IGameAdapter GameAdapter => gameAdapter ?? (gameAdapter = BBGC.Features.CoreDefaults().GameAdapter);

        /// <summary>
        /// Инициализация аналитики
        /// </summary>
        public abstract void Init();
        /// <summary>
        /// Отправляем ивент с параметром
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        public abstract void SendEvent(string name, Dictionary<string, object> parameters);
        /// <summary>
        /// Отправляем ивент без параметра
        /// </summary>
        /// <param name="name"></param>
        public abstract void SendEvent(string name);
        public abstract void AddNumberToUserProfile(string key, double value);
        public abstract void AddStringToUserProfile(string key, string value);
        public abstract void AddBoolToUserProfile(string key, bool value);
        public abstract void AddCounterToUserProfile(string key, double value);
        public abstract void ReportUserProfile();
        public abstract void SetUserProfileID(string id);
        
        public abstract void ResumeApp();
        
    }

}