using System.Collections.Generic;

#if BBGC_SQBA_FEATURE
using BlackBears.SQBA;
using BlackBears.SQBA.Modules.Purchasing;
#endif

namespace BlackBears.GameCore.Features.Analytics
{
    public partial class AnalyticsFeature
    {
        private const string countryKey = "country";
        private const string platformKey = "platform";
        
#if BBGC_SQBA_FEATURE
        public string Country =>  BBGC.Features.SQBA().CurrentLocale;
#else
        public string Country => "undefined";
#endif
        
//      public string Chronometer => BBGC.Features.SQBA().CurrentLocale;

        public void AdsRewRequest(string source)
        {
            var message = CreateDictionaryParameters();
            message.Add("source", source);
            SendEvent("ads_rew_request", message);
        }

        public void AdsRewError(string error, string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add("error", error);
            message.Add(countryKey, Country);
            message.Add(platformKey, platform);
            SendEvent("ads_rew_error", message);
        }

        public void AdsRewNo()
        {
            var message = CreateDictionaryParameters();
            message.Add(countryKey, Country);
            SendEvent("ads_rew_no", message);
        }

        public void AdsRewStart(string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add(platformKey, platform);
            message.Add(countryKey, Country);
            SendEvent("ads_rew_start", message);
        }

        public void AdsRewView(string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add(platformKey, platform);
            message.Add(countryKey, Country);
            SendEvent("ads_rew_view", message);
        }
        public void AdsView(string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add(platformKey, platform);
            message.Add(countryKey, Country);
            SendEvent("ads_view", message);
        }

        public void AdsIntRequest(string source)
        {
            var message = CreateDictionaryParameters();
            message.Add("source", source);
            SendEvent("ads_int_request", message);
        }

        public void AdsIntError(string error, string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add("error", error);
            message.Add(countryKey, Country);
            message.Add(platformKey, platform);
            SendEvent("ads_int_error", message);
        }

        public void AdsIntNo()
        {
            var message = CreateDictionaryParameters();
            message.Add(countryKey, Country);
            SendEvent("ads_int_no", message);
        }

        public void AdsIntStart(string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add(platformKey, platform);
            message.Add(countryKey, Country);
            SendEvent("ads_int_start", message);
        }

        public void AdsIntView(string platform)
        {
            var message = CreateDictionaryParameters();
            message.Add(platformKey, platform);
            message.Add(countryKey, Country);
            SendEvent("ads_int_view", message);
        }

#if BBGC_ANALYTICS_FEATURE && BBGC_SQBA_FEATURE
        public void Purchase(string productId, double amount, string currentHardCurrency,
            string typeOfPurchaser, Dictionary<string, object> additionalInformation, 
            SQBAShopItem shopItem, PurchaseAnswer answer)
        {
            var message = CreateDictionaryParameters();
            message.Add("product_id", productId);
            message.Add("amount", amount.ToString());
            message.Add("day", Day);
            message.Add("session", Session);
            message.Add("current_hard_currency", currentHardCurrency);
            message.Add("type_of_purchaser", typeOfPurchaser);

            if (additionalInformation != null)
            {
                foreach (var item in additionalInformation)
                {
                    message.Add(item.Key, item.Value);
                }
            }

            SendEvent("purchase", message);
            SendBBAPurchaseEvent(shopItem, answer, amount);
        }
#endif

#if BBGC_ANALYTICS_FEATURE && BBGC_SQBA_FEATURE
        private void SendBBAPurchaseEvent(SQBAShopItem shopItem, PurchaseAnswer answer, double amount)
        {
            if (shopItem == null) return;

#if !UNITY_EDITOR && UNITY_ANDROID
            var androidAnswer = answer as AndroidPurchaseAnswer;
            if (androidAnswer == null) return;

            YandexAppMetricaReceipt revenueReceipt = new YandexAppMetricaReceipt();
            revenueReceipt.Signature = androidAnswer.signature;
            revenueReceipt.Data = androidAnswer.responseData;
            YandexAppMetricaRevenue revenue = new YandexAppMetricaRevenue(amount, "USD");
            revenue.Receipt = revenueReceipt;
            revenue.Payload = "{\"source\":\"Google Play\"}";
            revenue.Quantity = 1;
            revenue.ProductID = shopItem.IapName;
            AppMetrica.Instance.ReportRevenue(revenue);
#elif !UNITY_EDITOR && UNITY_IOS

            var iosAnswer = answer as iOSPurchaseAnswer;
            if (iosAnswer == null) return;

            YandexAppMetricaReceipt iosRevenueReceipt = new YandexAppMetricaReceipt();
            iosRevenueReceipt.TransactionID = iosAnswer.transactionId;
            iosRevenueReceipt.Data = iosAnswer.bundleReceipt;
            YandexAppMetricaRevenue iosRevenue = new YandexAppMetricaRevenue(amount, "USD");
            iosRevenue.Receipt = iosRevenueReceipt;
            iosRevenue.Payload = "{\"source\":\"AppStore\"}";
            iosRevenue.Quantity = 1;
            iosRevenue.ProductID = shopItem.IapName;
            AppMetrica.Instance.ReportRevenue(iosRevenue);

#endif
        }
#endif
        public void DayHardCurrency(string day, string amount)
        {
            var message = CreateDictionaryParameters();
            var dayParams = new Dictionary<string, object>();
            dayParams.Add("amount", amount);
            message.Add(day, dayParams);
            SendEvent("day_hard_currency", message);
        }

        public void AddCurrency(string name, string currentAmountCurrency,
            string addAmountCurrency, string typeOfPurchaser,
            Dictionary<string, object> additionalInformation)
        {
            var message = CreateDictionaryParameters();
            message.Add("day", Day);
            message.Add("session", Session);
            message.Add("current_amount_currency", currentAmountCurrency);
            message.Add("add_amount_currency", addAmountCurrency);

            if (additionalInformation != null)
            {
                foreach (var item in additionalInformation)
                {
                    message.Add(item.Key, item.Value);
                }
            }

            SendEvent($"add_{name}_currency", message);
        }

        public void SpendCurrency(string name, string currentAmountCurrency,
            string spendAmountCurrency, string typeOfPurchaser,
            Dictionary<string, object> additionalInformation)
        {
            var message = CreateDictionaryParameters();
            message.Add("day", Day);
            message.Add("session", Session);
            message.Add("current_amount_currency", currentAmountCurrency);
            message.Add("spend_amount_currency", spendAmountCurrency);

            if (additionalInformation != null)
            {
                foreach (var item in additionalInformation)
                {
                    message.Add(item.Key, item.Value);
                }
            }

            SendEvent($"spend_{name}_currency", message);
        }

        public void ReplaceCurrency(string name, string currentAmountCurrency,
            string newAmountCurrency, string typeOfPurchaser,
            Dictionary<string, object> additionalInformation)
        {
            var message = CreateDictionaryParameters();
            message.Add("day", Day);
            message.Add("session", Session);
            message.Add("current_amount_currency", currentAmountCurrency);
            message.Add("new_amount_currency", newAmountCurrency);

            if (additionalInformation != null)
            {
                foreach (var item in additionalInformation)
                {
                    message.Add(item.Key, item.Value);
                }
            }

            SendEvent($"replace_{name}_currency", message);
        }
    }
}