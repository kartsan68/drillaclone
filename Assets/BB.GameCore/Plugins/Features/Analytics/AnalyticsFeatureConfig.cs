using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.GameCore.Features.Analytics
{

    [System.Serializable]
    public class AnalyticsFeatureConfig : FeatureConfig
    {
        [SerializeField] private List<Analytics> analytics;

        internal List<Analytics> Analytics => analytics;
    }

}