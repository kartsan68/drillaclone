using UnityEngine;

namespace BlackBears.GameCore.Features
{

    public class FeaturesInitData
    {

        internal RectTransform alertsRoot;
        internal Block onClansFeatureActivated;
        internal Block<long, string> onError;

    }

}