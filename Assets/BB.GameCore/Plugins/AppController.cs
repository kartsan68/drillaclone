using System;
using System.Collections.Generic;
using System.Threading;
using BlackBears.Bundler;
#if BBGC_CLANS_FEATURE
using BlackBears.Clans.Controller.Fragments;
#endif
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.GDPR;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Features.Save;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.GameCore
{

    public abstract class AppController : MonoBehaviour, IAppPauseListener, IAppResumeListener
    {

        [Range(30, 60)] public int frameRate = 60;
        [SerializeField] private BundlerManager bundler;
        [SerializeField] private List<GameObject> dontDestroyOnLoadList;
        [SerializeField] private bool initOnStart = true;
        [SerializeField] protected ServiceCanvasController serviceCanvas;

        private bool isInitialized;
        private bool isStartInitialized;
        private Block initializationCallback;
        
        private UnityEvent onInitializeComplete = new UnityEvent();

        private IDisposable subscription;
        private SaveFeature saveFeature;
        private IGameAdapter gameAdapter;

        int IAppEventPriority.Priority => (int)BlackBears.GameCore.AppEvents.AppEventPriority.Game;

        protected BundlerManager Bundler => bundler;

        public bool IsInitialized
        {
            get { return isInitialized; }
            private set
            {
                if (isInitialized == value) return;
                isInitialized = value;
                if (isInitialized) onInitializeComplete.Invoke();
            }
        }

        public UnityEvent OnInitalizeComplete => onInitializeComplete;

        public void StartInitialize()
        {
            if(isStartInitialized) return;
            isStartInitialized = true;

            BBGC.Initialize(new FeaturesInitData
            {
                #if BBGC_DROP_ITEM_SYSTEM_FEATURE
                onClansFeatureActivated = serviceCanvas.ActivateDropItemViewBuilder,
                #endif
                alertsRoot = serviceCanvas.Alerts,
                onError = OnErrorInitializeCore
            }, OnCoreInitializeFinished);
        }

        protected virtual void Awake()
        {
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            DontDestroyOnLoad(this);
            foreach (var item in dontDestroyOnLoadList)
            {
                DontDestroyOnLoad(this);
            }
#endif

            Application.targetFrameRate = frameRate;

            var culture = (System.Globalization.CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = culture;
        }

        protected virtual void Update()
        {
            if(saveFeature != null) saveFeature.Tick(Time.deltaTime);
        }

        protected virtual void Start()
        {
            if(initOnStart)  StartInitialize();
        }

        protected virtual void OnErrorInitializeCore(long code, string message) { }

        protected virtual void OnApplicationQuit() 
        { 
            if(!IsInitialized) return;

            if(saveFeature != null) saveFeature.Save();
        }


        private void OnCoreInitializeFinished()
        {
            BBGC.Features.GetFeature<NotificationsFeature>().Inject(serviceCanvas.NotificationController);
            InitializeBundler();
        }

        private void InitializeBundler()
        {
            if(!bundler.ConfigurationIsExist) 
            {
                GameFullInitialize();
                return;
            }
            bundler.OnCachingStateChanged += OnBundlerStateChanged;
            bundler.PrepareCache();
        }

        private void OnBundlerStateChanged(object sender, BundlerEventArgs args)
        {
            if (args.HasError)
            {
                bundler.OnCachingStateChanged -= OnBundlerStateChanged;
                ShowErrorAlert(InitializeBundler);
            }
            else if (args.IsFinished)
            {
                bundler.OnCachingStateChanged -= OnBundlerStateChanged;
                GameFullInitialize();
            }
        }


        private void GameFullInitialize()
        {
            saveFeature = BBGC.Features.GetFeature<SaveFeature>();
            AmountUtils.SetupMultipliersFromString(BBGC.Features.Localization().Get("MULTIPLIERS"));
            
            InitGame((adapter) => 
            {
                gameAdapter = adapter;
                CheckGDRP();
            }, (c, m) =>
            {
               Debug.Log($"GameFullInitialize error. Code - \"{c}\"; Message - \"{m}\"");
               ShowErrorAlert(GameFullInitialize);
            });
        }
        protected abstract void InitGame(Block<IGameAdapter> onSuccess, FailBlock onFail);

        private void CheckGDRP()
        {
            BBGC.Features.GetFeature<GDPRFeature>().CheckForGDPR(OnGDPRChecked, gameAdapter?.PlayerId);
        }

        private void OnGDPRChecked()
        {
            BBGC.Features.GetFeature<SaveFeature>().PostInit();
            BBGC.Features.GetFeature<ChronometerFeature>().CheckOffline(OnOfflineTimeChecked);
        }

        private void OnOfflineTimeChecked()
        {
            var backPressFeature = BBGC.Features.GetFeature<Features.BackPress.BackPressFeature>();
            backPressFeature.Enable();
            backPressFeature.OnExit += OnApplicationQuit;

            IsInitialized = true;

            BBGC.AppEvents.AddAppPauseListener(this);
            BBGC.AppEvents.AddAppResumeListener(this);

            var analytics = BBGC.Features.Analytics();
            BBGC.Features.CoreDefaults().ConnectWithGame(gameAdapter, analytics);
            analytics?.SetUserProfileID(gameAdapter?.PlayerId);

            
            if (saveFeature.SaveManager.IsFirstLaunch)
            {
                analytics?.SendEvent("Post install");
            }
            
            LaunchGame();
        }

        protected abstract void LaunchGame();

#if BBGC_CLANS_FEATURE
        internal void OpenClanFragment(FragmentKind kind)
        {
            LeanTween.cancel(serviceCanvas.ClanCanvasGroup.gameObject);
            serviceCanvas.ClanCanvasGroup.gameObject.SetActive(true);
            serviceCanvas.ClanCanvasGroup.alpha = 1;
            BBGC.Features.Clans().Core.ShowFragment(serviceCanvas.ClansFragment, kind);
        }

        internal void OpenInGameFragment(string fragmentKey, Dictionary<string, object> data, bool checkShowed = true)
        {
            LeanTween.cancel(serviceCanvas.ClanCanvasGroup.gameObject);
            serviceCanvas.ClanCanvasGroup.gameObject.SetActive(true);
            serviceCanvas.ClanCanvasGroup.alpha = 1;
            BBGC.Features.Clans().Core.ShowFragment(serviceCanvas.ClansFragment, fragmentKey, data, checkShowed);
        }

        internal void OpenClans()
        {
            LeanTween.cancel(serviceCanvas.ClanCanvasGroup.gameObject);
            serviceCanvas.ClanCanvasGroup.gameObject.SetActive(true);
            serviceCanvas.ClanCanvasGroup.alpha = 1;
            BBGC.Features.Clans().Core.Show(serviceCanvas.Clans);
        }

        internal void ShowClans()
        {
            LeanTween.cancel(serviceCanvas.ClanCanvasGroup.gameObject);
            serviceCanvas.ClanCanvasGroup.gameObject.SetActive(true);
            LeanTween.alphaCanvas(serviceCanvas.ClanCanvasGroup, 1f, 0.2f);
        }

        internal void HideClans()
        {
            LeanTween.cancel(serviceCanvas.ClanCanvasGroup.gameObject);
            LeanTween.alphaCanvas(serviceCanvas.ClanCanvasGroup, 0f, 0.2f)
                .setOnComplete(() => serviceCanvas.ClanCanvasGroup.gameObject.SetActive(false));
        }
#endif

        void IAppPauseListener.OnAppPaused()
        {
            if (!IsInitialized) return;

            BBGC.Features.GetFeature<SaveFeature>().Save();
            AppPaused();
        }

        protected virtual void AppPaused(){}

        void IAppResumeListener.OnAppResumed()
        {
            if (IsInitialized) AppResumed();
        }

        protected virtual void AppResumed(){}

        protected void ShowErrorAlert(Block callback)
        {
            var loc = BBGC.Features.GetFeature<BlackBears.GameCore.Features.Localization.LocalizationFeature>();//LocalizationUtils.Instance;
            BBGC.Features.Alerts().AddAlert(new ErrorAlert(
                        loc.Get("BBGC_INIT_ERROR_TITLE"),
                        loc.Get("BBGC_INIT_ERROR_DESC"),
                        loc.Get("BBGC_INIT_ERROR_BTN"),
                        callback));
        }



    }
}