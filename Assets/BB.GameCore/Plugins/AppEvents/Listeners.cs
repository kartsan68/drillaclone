using System.Collections.Generic;

namespace BlackBears.GameCore
{

    public interface IAppEventPriority
    {
        int Priority { get; }
    }

    public interface IAppResumeListener : IAppEventPriority
    {
        void OnAppResumed();
    }

    public interface IAppPauseListener : IAppEventPriority
    {
        void OnAppPaused();
    }

    public interface IAppQuitListener : IAppEventPriority
    {
        void OnAppQuit();
    }

    public class AppEventPriorityComparer<T> : IComparer<T> where T : IAppEventPriority
    {
        public int Compare(T x, T y)
        {
            if (x == null) return -1;
            if (y == null) return 1;
            return x.Priority - y.Priority;
        }
    }

}