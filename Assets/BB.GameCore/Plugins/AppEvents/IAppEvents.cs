using BlackBears.GameCore.Features.Chronometer;

namespace BlackBears.GameCore.AppEvents
{

    public interface IAppEvents
    {
        
        bool AddAppResumeListener(IAppResumeListener listener);
        bool RemoveAppResumeListener(IAppResumeListener listener);

        bool AddAppPauseListener(IAppPauseListener listener);
        bool RemoveAppPauseListener(IAppPauseListener listener);

        bool AddAppQuitListener(IAppQuitListener listener);
        bool RemoveAppQuitListener(IAppQuitListener listener);
    }

}