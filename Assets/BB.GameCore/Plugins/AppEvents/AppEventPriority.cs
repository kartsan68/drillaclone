namespace BlackBears.GameCore.AppEvents
{

    public enum AppEventPriority
    {
        Chronometer = -10,
        Save = -9,
        Game = 0,
        LocalNotification,

        CoreDefault,
        Ads,
        Analytics
    }

}