using BlackBears;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Features.DropItemSystem;
using UnityEngine;

namespace BlackBears.GameCore
{

    public class ServiceCanvasController : MonoBehaviour
    {
        [SerializeField] private CanvasGroup clanCanvasGroup;
        [SerializeField] private Canvas alertsCanvas;
        [SerializeField] private RectTransform clans;
        [SerializeField] private RectTransform alerts;
        [SerializeField] private RectTransform clansFragment;
        [SerializeField] private NotificationController notificationController;
        #if BBGC_DROP_ITEM_SYSTEM_FEATURE
        [SerializeField] private DropItemViewBuilder dropItemViewBuilder;
        #endif

        public CanvasGroup ClanCanvasGroup => clanCanvasGroup;
        public RectTransform Clans => clans;
        public RectTransform ClansFragment => clansFragment;
        public RectTransform Alerts => alerts;
        public NotificationController NotificationController => notificationController;
        #if BBGC_DROP_ITEM_SYSTEM_FEATURE
        public Block ActivateDropItemViewBuilder => (() => 
        {
            if(dropItemViewBuilder != null) dropItemViewBuilder.enabled = true;
        });
        #endif


        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(alertsCanvas.gameObject);
        }

    }

}