using UnityEngine;

namespace BlackBears.GameCore.Graphic
{

    [RequireComponent(typeof(ImageScaler))]
    [ExecuteInEditMode]
    public abstract class ScalerKey : MonoBehaviour
    {

        protected abstract string Key { get; }

        private void Awake()
        {
            var scaler = GetComponent<ImageScaler>();
            scaler.key = Key;
            #if UNITY_EDITOR
            if (!Application.isPlaying) return;
            #endif
            Destroy(this);            
        }

        #if UNITY_EDITOR

        private void Update()
        {
            if (!Application.isPlaying) Awake();
        }

        #endif

    }

}