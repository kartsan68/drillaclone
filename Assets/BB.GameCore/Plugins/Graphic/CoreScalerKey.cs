using UnityEngine;

namespace BlackBears.GameCore.Graphic
{

    public class CoreScalerKey : ScalerKey
    {

        protected override string Key => CoreCache.key;

    }

}