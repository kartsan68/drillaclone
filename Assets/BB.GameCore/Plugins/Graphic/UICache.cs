using System.Collections.Generic;
using BlackBears.Resolutor;
using BlackBears.Resolutor.Scaling;
using BlackBears.Utils.Scaling;
using UnityEngine;

namespace BlackBears.GameCore.Graphic
{

    public class UICache
    {

#if UNITY_EDITOR

        public const string internalConfigPath = "Assets/BB.GameCore/Editor/UICacheConfig.asset";
        public const string appConfigPath = "Assets/Editor/UICacheConfig.asset";

        private static GraphicSettings internalSettings;
        private static GraphicSettings appSettings;

        public static bool ShowSpritesInEdit
        {
            get { return InternalSettings.showSprites; }
            set
            {
                InternalSettings.showSprites = value;
                UnityEditor.EditorUtility.SetDirty(InternalSettings);
                UnityEditor.SceneView.RepaintAll();
            }
        }

        private static GraphicSettings InternalSettings
        {
            get
            {
                CheckSettings(ref internalSettings, internalConfigPath);
                return internalSettings;
            }
        }

        private static GraphicSettings AppSettings
        {
            get 
            {
                CheckSettings(ref appSettings, appConfigPath);
                return appSettings;
            }
        }

        public static void OpenAppSettings()
        {
            UnityEditor.Selection.activeObject = AppSettings;
        }

        public static Sprite TakeSpriteInEditor(string key, string spriteName)
        {
            var sprite = TakeSpriteFromSettings(AppSettings, key, spriteName);
            if (sprite == null) sprite = TakeSpriteFromSettings(InternalSettings, key, spriteName);
            return sprite;
        }

        private static Sprite TakeSpriteFromSettings(GraphicSettings settings, string key, string spriteName)
        {
            string[] searchResults = UnityEditor.AssetDatabase.FindAssets(string.Format("{0} t:Sprite", spriteName),
                new string[] { settings.PathByKey(key) });
            if (searchResults == null) return null;

            for (int i = 0; i < searchResults.Length; i++)
            {
                var sprites = UnityEditor.AssetDatabase
                    .LoadAllAssetsAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(searchResults[i]));
                foreach (var sprite in sprites) 
                {
                    var sn = sprite.name;
                    if (sprite.name.EndsWith(spriteName)) 
                    {
                        return sprite as Sprite;
                    }
                }
            }
            return null;
        }

        private static void CheckSettings(ref GraphicSettings settings, string path)
        {
            if (settings) return;
            settings = UnityEditor.AssetDatabase.LoadAssetAtPath<GraphicSettings>(path);
            if (settings) return;
            settings = ScriptableObject.CreateInstance<GraphicSettings>();
            UnityEditor.AssetDatabase.CreateAsset(settings, path);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

#endif

        private static UICache _instance;
        public static UICache Instance { get { return _instance ?? (_instance = new UICache()); } }

        private Dictionary<string, Atlas> atlasByKey = new Dictionary<string, Atlas>();
        private Dictionary<string, IScaleSelector> selectorByKey = new Dictionary<string, IScaleSelector>();

        private UICache() { }

        public void RegisterAtlas(string key, Atlas atlas) { atlasByKey[key] = atlas; }
        public void UnregisterAtlas(string key) { atlasByKey.Remove(key); }

        public void RegisterSelector(string key, IScaleSelector selector) { selectorByKey[key] = selector; }
        public void UnregisterSelector(string key) { selectorByKey.Remove(key); }

        public Atlas TakeAtlas(string key)
        {
            Atlas atlas;
            atlasByKey.TryGetValue(key, out atlas);
            return atlas;
        }

        public IScaleSelector TakeSelector(string key)
        {
            IScaleSelector selector;
            selectorByKey.TryGetValue(key, out selector);
            return selector;
        }

    }

}