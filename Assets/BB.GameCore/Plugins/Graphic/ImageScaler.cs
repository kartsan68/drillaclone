using BlackBears.Resolutor;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Graphic
{

	[RequireComponent(typeof(Image))]
	[ExecuteInEditMode]
	[DisallowMultipleComponent]
    public class ImageScaler : MonoBehaviour
    {

		public string key;
		public string spriteName;

		private Image _image;

		private Image Image { get { return _image ?? (_image = GetComponent<Image>()); } }

		private void Start()
		{
			if (TryToCheckMode()) return;
			if (Image.sprite == null) SetupSprite(UICache.Instance.TakeAtlas(key));
			else Destroy(this);
		}

		private void SetupSprite(Atlas atlas)
		{
			if (atlas == null) return;
			Image.sprite = atlas[spriteName];
			Destroy(this);
		}

		private bool TryToCheckMode()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying) 
			{
				CheckMode();
				return true;
			}
			#endif
			return false;
		}

		#if UNITY_EDITOR

		private void Update()
		{
			if (TryToCheckMode()) return;
		}

		private void CheckMode()
		{
			if (UICache.ShowSpritesInEdit) ShowSprite();
			else HideSprite();
		}

		private void ShowSprite()
		{
			if (Image.sprite != null) return;
			Image.sprite = UICache.TakeSpriteInEditor(key, spriteName);
		}

		private void HideSprite()
		{
			if (Image.sprite == null) return;
			spriteName = Image.sprite.name;
			Image.sprite = null;
		}

		#endif

    }

}