using UnityEngine;

namespace BlackBears.GameCore.Graphic
{

    internal class GameCache : BaseAtlasCache
    {

        public const string key = "game";
        
        protected override string Key => key;

    }

}