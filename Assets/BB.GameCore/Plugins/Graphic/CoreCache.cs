using UnityEngine;

namespace BlackBears.GameCore.Graphic
{

    internal class CoreCache : BaseAtlasCache
    {

        public const string key = "core";
        
        protected override string Key => key;

    }

}