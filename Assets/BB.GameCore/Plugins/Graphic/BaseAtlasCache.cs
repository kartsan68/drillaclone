using System.Collections;
using BlackBears.Bundler;
using BlackBears.Resolutor;
using BlackBears.Resolutor.Scaling;
using BlackBears.Utils.Scaling;
using UnityEngine;

namespace BlackBears.GameCore.Graphic
{

    public abstract class BaseAtlasCache : ResourceCache
    {

        [SerializeField] private string atlasPathTemplate = "Assets/Atlases/{0}";

        private string atlasPath;
        private EditableAtlas atlas;

        protected sealed override IScaleSelector Selector { get { return UICache.Instance.TakeSelector(Key); } }

        protected abstract string Key { get; }

        protected override void Awake()
        {
            atlas = new EditableAtlas();
            UICache.Instance.RegisterAtlas(Key, atlas);
            base.Awake();
        }

        protected virtual void OnDestroy()
        {
            UICache.Instance.UnregisterAtlas(Key);
        }

        protected override IEnumerator LoadResourcesFromBundle(IBundleProxy bundle)
        {
            atlasPath = string.Format(atlasPathTemplate, GraphicScale.FolderName());
            var assetNames = bundle.GetAllAssetNames();
            for (int i = 0; i < assetNames.Length; i++)
            {
                if (IsAtlasAsset(assetNames[i])) yield return LoadToAtlas(assetNames[i], bundle);
                else Debug.LogWarning($"Undefindes asset with name '{assetNames[i]}' in ResourceCache");
            }
        }

        protected virtual bool IsAtlasAsset(string assetName) =>
            assetName.StartsWith(atlasPath, System.StringComparison.InvariantCultureIgnoreCase);

        private IEnumerator LoadToAtlas(string assetName, IBundleProxy bundle)
        {
            var request = bundle.LoadAssetWithSubAssetsAsync<Sprite>(assetName);
            yield return request;
            atlas.AddSprites(request.allAssets);
        }

    }

}