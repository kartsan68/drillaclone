using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace BlackBears.GameCore.Graphic
{
    [RequireComponent(typeof(Image))]
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class BBImage : MonoBehaviour
    {
        public string folderName;
        public string spriteName;
        public Image _image;

        private Sprite sprite;

        private Image Image { get { return _image ?? (_image = GetComponent<Image>()); } }

#if UNITY_EDITOR

        private void OnEnable()
        {
            if (Application.isPlaying) return;
            CheckSprite();
        }

        public void CheckSprite()
        {
            if (Image.sprite == null) return;
            if (Image.sprite == sprite) return;
            sprite = Image.sprite;
            folderName = Path.GetDirectoryName(AssetDatabase.GetAssetPath(sprite));
            spriteName = sprite.name;
        }

        private void Update()
        {
            if (Application.isPlaying) return;

            CheckSprite();
        }

#endif
    }
}