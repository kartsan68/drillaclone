﻿using UnityEngine;

namespace BB.GameCore.Plugins
{

    public class iOSQueriesSchemes : ScriptableObject
    {

        public const string configPath = "Assets/Editor/QueriesSchemes.asset";

        public string[] schemes;

    }

}