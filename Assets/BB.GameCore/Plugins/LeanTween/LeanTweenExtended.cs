﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class LeanTween : MonoBehaviour
{

    [System.Obsolete("Don't use this")]
    public static LTDescr moveByLocal(GameObject gameObject, Vector3 by, float time)
    {
        var localPos = gameObject.transform.localPosition;
        return moveLocal(gameObject, localPos + by, time);
    }

    [System.Obsolete("Don't use this")]
    public static LTDescr moveByLocalX(GameObject gameObject, float by, float time)
    {
        var localX = gameObject.transform.localPosition.x;
        return moveLocalX(gameObject, localX + by, time);
    }

    [System.Obsolete("Don't use this")]
    public static LTDescr moveByLocalY(GameObject gameObject, float by, float time)
    {
        var localY = gameObject.transform.localPosition.y;
        return moveLocalY(gameObject, localY + by, time);
    }

    public static LTDescr moveAnchored(RectTransform rect, Vector2 to, float time)
    {
        var tween = options();
        moveAnchored(tween, to);
        pushNewTween(rect.gameObject, to, time, tween);

        tween.setRect(rect);

        return tween;
    }

    public static LTDescr moveAnchoredX(RectTransform rect, float to, float time)
    {
        var tween = options();
        moveAnchoredX(tween, to);
        pushNewTween(rect.gameObject, new Vector3(to, 0, 0), time, tween);

        tween.setRect(rect);

        return tween;
    }

    public static LTDescr moveAnchoredY(RectTransform rect, float to, float time)
    {
        var tween = options();
        moveAnchoredY(tween, to);
        pushNewTween(rect.gameObject, new Vector3(0, to, 0), time, tween);

        tween.setRect(rect);

        return tween;
    }

    public static LTDescr moveToTransformLocal(Transform item, Transform to, float time)
    {
        var tween = options();
        moveToTransformLocal(tween, to);
        pushNewTween(item.gameObject, default(Vector3), time, tween);

        return tween;
    }

    public static LTDescr moveFromToAnchored(RectTransform rect, Vector2 from, Vector2 to, float time)
    {
        var tween = options();
        moveFromToAnchored(tween, from, to);
        pushNewTween(rect.gameObject, to, time, tween);

        tween.setRect(rect);

        return tween;
    }

    [System.Obsolete("Use alphaGraphic instead.")]
    public static LTDescr alphaImage(Image image, float to, float time)
    {
        var tween = options();
        alphaGraphic(tween, image, to);
        pushNewTween(image.gameObject, new Vector3(to, 0, 0), time, tween);

        return tween;
    }

    public static LTDescr alphaGraphic(Graphic graphic, float to, float time)
    {
        var tween = options();
        alphaGraphic(tween, graphic, to);
        pushNewTween(graphic.gameObject, new Vector3(to, 0, 0), time, tween);

        return tween;
    }

    public static LTDescr sizeMin(LayoutElement layout, Vector2 to, float time)
    {
        var tween = options();
        sizeMin(tween, layout);
        pushNewTween(layout.gameObject, to, time, tween);

        return tween;
    }

    public static LTDescr moveScrollNormalized(ScrollRect scroll, Vector2 to, float time)
    {
        var tween = options();
        moveScrollNormalized(tween, scroll);
        pushNewTween(scroll.gameObject, to, time, tween);

        return tween;
    }

    private static void moveFromToAnchored(LTDescr tween, Vector2 from, Vector2 to)
    {
        tween.type = TweenAction.MOVE_LOCAL;
        tween.initInternal = () =>
        {
            tween.rectTransform.anchoredPosition = from;
            tween.from = from;
        };
        tween.easeInternal = () =>
        {
            LTDescr.newVect = tween.easeMethod();
            tween.rectTransform.anchoredPosition = (Vector2)LTDescr.newVect;
        };
    }

    private static void moveAnchored(LTDescr tween, Vector2 to)
    {
        tween.type = TweenAction.MOVE_LOCAL;
        tween.initInternal = () =>
        {
            tween.from = tween.rectTransform.anchoredPosition;
        };
        tween.easeInternal = () =>
        {
            LTDescr.newVect = tween.easeMethod();
            tween.rectTransform.anchoredPosition = (Vector2)LTDescr.newVect;
        };
    }
    
    private static void moveAnchoredX(LTDescr tween, float to)
    {
        tween.type = TweenAction.MOVE_LOCAL_X;
        tween.initInternal = () =>
        {
            tween.from = tween.rectTransform.anchoredPosition;
        };
        tween.easeInternal = () =>
        {
            LTDescr.newVect = tween.easeMethod();
            var pos = tween.rectTransform.anchoredPosition;
            pos.x = LTDescr.newVect.x;
            tween.rectTransform.anchoredPosition = pos;
        };
    }

    private static void moveAnchoredY(LTDescr tween, float to)
    {
        tween.type = TweenAction.MOVE_LOCAL_Y;
        tween.initInternal = () =>
        {
            tween.from = tween.rectTransform.anchoredPosition;
        };
        tween.easeInternal = () =>
        {
            LTDescr.newVect = tween.easeMethod();
            var pos = tween.rectTransform.anchoredPosition;
            pos.y = LTDescr.newVect.y;
            tween.rectTransform.anchoredPosition = pos;
        };
    }

    private static void moveToTransformLocal(LTDescr tween, Transform to)
    {
        tween.type = TweenAction.MOVE_LOCAL;
        tween.initInternal = () =>
        {
            tween.from = tween.trans.localPosition;
        };
        tween.easeInternal = () =>
        {
            tween.to = to.localPosition;
            tween.diff = tween.to - tween.from;
            LTDescr.newVect = tween.easeMethod();
            tween.trans.localPosition = LTDescr.newVect;
        };
    }

    private static void alphaGraphic(LTDescr tween, Graphic graphic, float to)
    {
        tween.type = TweenAction.ALPHA;
        tween.initInternal = () =>
        {
            tween.fromInternal.x = graphic.color.a;
        };
        tween.easeInternal = () =>
        {
            LTDescr.newVect = tween.easeMethod();
            var color = graphic.color;
            color.a = LTDescr.newVect.x;
            graphic.color = color;
        };
    }

    private static void sizeMin(LTDescr tween, LayoutElement layout)
    {
        tween.type = TweenAction.CANVAS_SIZEDELTA;
        tween.initInternal = () =>
        {
            tween.fromInternal = new Vector3(layout.minWidth, layout.minHeight, 0f);
        };
        tween.easeInternal = () =>
        {
            LTDescr.newVect = tween.easeMethod();
            layout.minWidth = LTDescr.newVect.x;
            layout.minHeight = LTDescr.newVect.y;
        };
    }

    private static void moveScrollNormalized(LTDescr tween, ScrollRect scroll)
    {
        tween.type = TweenAction.MOVE;
        tween.initInternal = () =>
        {
            tween.fromInternal = scroll.normalizedPosition;
        };
        tween.easeInternal = () =>
        {
            scroll.normalizedPosition = tween.easeMethod();
        };
    }

}