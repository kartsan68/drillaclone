namespace BlackBears.GameCore
{

    internal static class Logger
    {

        private const string messageFormat = "{0}: {1}";

        public static void Log(string logTag, string message)
        {
            UnityEngine.Debug.LogFormat(messageFormat, logTag, message);
        }

        public static void Warning(string logTag, string message)
        {
            UnityEngine.Debug.LogWarningFormat(messageFormat, logTag, message);
        }

    }

}