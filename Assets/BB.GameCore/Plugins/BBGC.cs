using System;
using System.Collections;
using BlackBears.GameCore.AppEvents;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Alerts;

namespace BlackBears.GameCore
{

    public sealed class BBGC
    {

        private const string kLogTag = "BBGC";

        private static BBGC instance;

        private FeatureStorage features;
        private GCBehaviour behaviour;

        private BBGC(GCConfig config, GCBehaviour behaviour)
        {
            this.behaviour = behaviour;
            this.features = new FeatureStorageBuilder(config, behaviour).Build();
        }

        public static IAppEvents AppEvents
        {
            get
            {
                if (!IsInternalInitiated)
                {
                    LogNoGCBehaviour();
                    return null;
                }
                return instance.behaviour;
            }
        }

        public static FeatureStorage Features
        {
            get
            {
                if (!IsInternalInitiated)
                {
                    LogNoGCBehaviour();
                    return null;
                }
                return instance.features;
            }
        }

        public static string TemporaryDirectory { get { return UnityEngine.Application.temporaryCachePath; } }

        internal static bool IsInternalInitiated { get { return instance != null; } }

        public static void Initialize(FeaturesInitData initData, Block onInitComplete)
        {
            if (!IsInternalInitiated)
            {
                LogNoGCBehaviour();
                return;
            }

            instance.behaviour.StartCoroutine(instance.IntializyAsync(initData,
                onInitComplete,
                (code, message) =>
                {
                    var loc = Features.Localization();
                    var alerts = Features.Alerts();
                    alerts.AddAlert(new ErrorAlert(
                        loc.Get("BBGC_INIT_ERROR_TITLE"),
                        loc.Get("BBGC_INIT_ERROR_DESC"),
                        loc.Get("BBGC_INIT_ERROR_BTN"),
                        () => Initialize(initData, onInitComplete),
                        code
                    ));
                    initData.onError.SafeInvoke(code, message);
                }));
        }

        public static void Dispose()
        {
            instance = null;
        }

        internal static void InternalInit(GCConfig config, GCBehaviour behaviour)
        {
            if (instance == null) instance = new BBGC(config, behaviour);
        }

        private static void LogNoGCBehaviour()
        {
            Logger.Warning(kLogTag, "Add 'GCBehaviour' component to your root Scene.");
        }

        private IEnumerator IntializyAsync(FeaturesInitData initData, Block onInitComplete, FailBlock onInitFailed)
        {
            behaviour.LoadCache();
            while (!behaviour.IsCacheReady)
            {
                if (behaviour.IsAllCacheProcessed)
                {
                    if (behaviour.IsHasErrors)
                    {
                        var errors = behaviour.TakeErrors();
                        onInitFailed.SafeInvoke(-1, errors);
                    }
                }
                yield return null;
            }
            features.Initialize(initData, onInitComplete, onInitFailed, behaviour);
        }

    }

}