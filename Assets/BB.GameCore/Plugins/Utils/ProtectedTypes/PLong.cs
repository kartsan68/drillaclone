using System;

namespace BlackBears
{
    public struct PLong : IProtectedVariable
    {

        private static Random rnd = new Random();

        private long value;
        private long key;

        public PLong(long value) : this()
        {
            Init(value);
        }

        private PLong(long value, long key)
        {
            this.value = value;
            this.key = key;
        }

        public long Value
        {
            get
            {
                if (key == 0) Init(0L);
                return ~(value ^ key);
            }
        }

        public bool IsNegative => Value < 0;

        private void Init(long value)
        {
            this.value = value;
            this.key = rnd.Next(1000000, 1000000000);

            this.value = ~(value ^ key);
        }

        #region Math
        public static PLong operator +(PLong v) => new PLong(v.Value);
        public static PLong operator -(PLong v) => new PLong(-v.Value);
        public static PLong operator ++(PLong v) => new PLong(v.Value + 1);
        public static PLong operator --(PLong v) => new PLong(v.Value - 1);

        public static PLong operator +(PLong v1, PLong v2) => new PLong(v1.Value + v2.Value);
        public static PLong operator -(PLong v1, PLong v2) => new PLong(v1.Value - v2.Value);

        public static PLong operator *(PLong v1, PLong v2) => new PLong(v1.Value * v2.Value);
        public static PLong operator /(PLong v1, PLong v2) => new PLong(v1.Value / v2.Value);

        public static bool operator ==(PLong v1, PLong v2) => v1.Value == v2.Value;
        public static bool operator !=(PLong v1, PLong v2) => v1.Value != v2.Value;

        public static bool operator <(PLong v1, PLong v2) => v1.Value < v2.Value;
        public static bool operator >(PLong v1, PLong v2) => v1.Value > v2.Value;
        public static bool operator <=(PLong v1, PLong v2) => v1.Value <= v2.Value;
        public static bool operator >=(PLong v1, PLong v2) => v1.Value >= v2.Value;

        public static implicit operator PLong(int amount) => new PLong(amount);
        public static implicit operator PLong(long amount) => new PLong(amount);
        public static explicit operator PLong(float amount) => new PLong((long)amount);
        public static explicit operator PLong(double amount) => new PLong((long)amount);
        public static implicit operator PLong(PInt amount) => new PLong(amount);
        public static explicit operator PLong(PFloat amount) => new PLong((long)amount);
        public static explicit operator PLong(PDouble amount) => new PLong((long)amount);

        public static explicit operator int(PLong amount) => (int)amount.Value;
        public static implicit operator long(PLong amount) => amount.Value;
        public static implicit operator float(PLong amount) => amount.Value;
        public static implicit operator double(PLong amount) => amount.Value;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;
            var other = (PLong)obj;
            return this.Value == other.Value;
        }

        public override int GetHashCode() => Value.GetHashCode();

        #endregion

        #region Serialization
        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}", Value.GetHashCode(), value, key);
        }

        public static PLong Parse(string str)
        {
            if (string.IsNullOrEmpty(str)) return new PLong(0);

            string[] parts = str.Split('|');

            if (parts.Length != 3) return new PLong(0);

            int hashCode = Convert.ToInt32(parts[0]);
            long value = Convert.ToInt64(parts[1]);
            long key = Convert.ToInt64(parts[2]);

            PLong storedInt = new PLong(value, key);
            int storedHashCode = storedInt.Value.GetHashCode();

            if (hashCode == storedHashCode) return new PLong(storedInt.Value);
            return new PLong(0);
        }

        public static bool TryParse(string str, out PLong v)
        {
            try
            {
                string[] parts = str.Split('|');

                int hashCode = Convert.ToInt32(parts[0]);
                long value = Convert.ToInt64(parts[1]);
                long key = Convert.ToInt64(parts[2]);

                PLong storedInt = new PLong(value, key);
                int storedHashCode = storedInt.Value.GetHashCode();

                if (hashCode == storedHashCode) v =  new PLong(storedInt.Value);
                else v = new PLong(0);

                return true;
            }
            catch (Exception)
            {
                v = new PLong(0);
                return false;
            }
        }

        public IProtectedVariable AddValue(IProtectedVariable value)
        {
            return new PLong(Value + ((PLong)value).Value);
        }

        public IProtectedVariable SpendValue(IProtectedVariable value)
        {
            return new PLong(Value - ((PLong)value).Value);
        }

        public string ToValueString()
        {
            return Value.ToString();
        }

        #endregion

    }
}