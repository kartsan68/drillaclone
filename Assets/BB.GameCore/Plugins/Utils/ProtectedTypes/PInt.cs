using System;

namespace BlackBears
{
    public struct PInt : IProtectedVariable
    {

        private static Random rnd = new Random();

        private int value;
        private int key;

        public PInt(int value) : this()
        {
            Init(value);
        }

        private PInt(int value, int key)
        {
            this.value = value;
            this.key = key;
        }

        public int Value
        {
            get
            {
                if (key == 0) Init(0);
                return ~(value ^ key);
            }
        }

        public bool IsNegative => Value < 0;

        private void Init(int value)
        {
            this.value = value;
            this.key = rnd.Next(1000000, 1000000000);

            this.value = ~(value ^ key);
        }

        #region Math
        public static PInt operator +(PInt v) => new PInt(v.Value);
        public static PInt operator -(PInt v) => new PInt(-v.Value);
        public static PInt operator ++(PInt v) => new PInt(v.Value + 1);
        public static PInt operator --(PInt v) => new PInt(v.Value - 1);

        public static PInt operator +(PInt v1, PInt v2) => new PInt(v1.Value + v2.Value);
        public static PInt operator -(PInt v1, PInt v2) => new PInt(v1.Value - v2.Value);

        public static PInt operator *(PInt v1, PInt v2) => new PInt(v1.Value * v2.Value);
        public static PInt operator /(PInt v1, PInt v2) => new PInt(v1.Value / v2.Value);

        public static bool operator ==(PInt v1, PInt v2) => v1.Value == v2.Value;
        public static bool operator !=(PInt v1, PInt v2) => v1.Value != v2.Value;

        public static bool operator <(PInt v1, PInt v2) => v1.Value < v2.Value;
        public static bool operator >(PInt v1, PInt v2) => v1.Value > v2.Value;
        public static bool operator <=(PInt v1, PInt v2) => v1.Value <= v2.Value;
        public static bool operator >=(PInt v1, PInt v2) => v1.Value >= v2.Value;

        public static implicit operator PInt(int amount) => new PInt(amount);
        public static explicit operator PInt(long amount) => new PInt((int)amount);
        public static explicit operator PInt(float amount) => new PInt((int)amount);
        public static explicit operator PInt(double amount) => new PInt((int)amount);
        public static explicit operator PInt(PLong amount) => new PInt((int)amount);
        public static explicit operator PInt(PFloat amount) => new PInt((int)amount);
        public static explicit operator PInt(PDouble amount) => new PInt((int)amount);

        public static implicit operator int(PInt amount) => amount.Value;
        public static implicit operator long(PInt amount) => amount.Value;
        public static implicit operator float(PInt amount) => amount.Value;
        public static implicit operator double(PInt amount) => amount.Value;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;
            var other = (PInt)obj;
            return this.Value == other.Value;
        }

        public override int GetHashCode() => Value.GetHashCode();

        #endregion

        #region Serialization
        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}", Value.GetHashCode(), value, key);
        }

        public static PInt Parse(string str)
        {
            if (string.IsNullOrEmpty(str)) return new PInt(0);

            string[] parts = str.Split('|');

            if (parts.Length != 3) return new PInt(0);

            int hashCode = Convert.ToInt32(parts[0]);
            int value = Convert.ToInt32(parts[1]);
            int key = Convert.ToInt32(parts[2]);

            PInt storedInt = new PInt(value, key);
            int storedHashCode = storedInt.Value.GetHashCode();

            if (hashCode == storedHashCode) return new PInt(storedInt.Value);
            return new PInt(0);
        }

        public static bool TryParse(string str, out PInt v)
        {
            try
            {
                string[] parts = str.Split('|');

                int hashCode = Convert.ToInt32(parts[0]);
                int value = Convert.ToInt32(parts[1]);
                int key = Convert.ToInt32(parts[2]);

                PInt storedInt = new PInt(value, key);
                int storedHashCode = storedInt.Value.GetHashCode();

                if (hashCode == storedHashCode) v = new PInt(storedInt.Value);
                else v = new PInt(0);

                return true;
            }
            catch (Exception)
            {
                v = new PInt(0);
                return false;
            }
        }

        public IProtectedVariable AddValue(IProtectedVariable value)
        {
            return new PInt(Value + ((PInt)value).Value);
        }

        public IProtectedVariable SpendValue(IProtectedVariable value)
        {
            return new PInt(Value - ((PInt)value).Value);
        }

        public string ToValueString()
        {
            return Value.ToString();
        }

        #endregion

    }
}