using System;
using System.Runtime.InteropServices;

namespace BlackBears
{

    [StructLayout(LayoutKind.Explicit)]
    unsafe public struct PFloat : IProtectedVariable
    {

        private static Random rnd = new Random();

        [FieldOffset(0)] private float value;
        [FieldOffset(0)] private int offset;

        [FieldOffset(8)] private int key;

        public PFloat(float value) : this()
        {
            Init(value);
        }

        private PFloat(int offset, int key)
        {
            this.key = key;

            this.value = 0;
            this.offset = offset;
        }

        public float Value
        {
            get
            {
                if (key == 0) Init(0f);
                int i = Offset;
                return *(float*)&i;
            }
        }

        private int Offset
        {
            get
            {
                int l = offset;
                l ^= key;
                return ~l;
            }
        }

        public bool IsNegative => Value < 0;

        private void Init(float value)
        {
            this.key = rnd.Next(1000000, 1000000000);

            this.offset = 0;
            this.value = value;

            this.offset ^= key;
            this.offset = ~offset;
        }

        #region Math
        public static PFloat operator +(PFloat v) => new PFloat(v.Value);
        public static PFloat operator -(PFloat v) => new PFloat(-v.Value);
        public static PFloat operator ++(PFloat v) => new PFloat(v.Value + 1);
        public static PFloat operator --(PFloat v) => new PFloat(v.Value - 1);

        public static PFloat operator +(PFloat v1, PFloat v2) => new PFloat(v1.Value + v2.Value);
        public static PFloat operator -(PFloat v1, PFloat v2) => new PFloat(v1.Value - v2.Value);

        public static PFloat operator *(PFloat v1, PFloat v2) => new PFloat(v1.Value * v2.Value);
        public static PFloat operator /(PFloat v1, PFloat v2) => new PFloat(v1.Value / v2.Value);

        public static bool operator ==(PFloat v1, PFloat v2) => v1.Value == v2.Value;
        public static bool operator !=(PFloat v1, PFloat v2) => v1.Value != v2.Value;

        public static bool operator <(PFloat v1, PFloat v2) => v1.Value < v2.Value;
        public static bool operator >(PFloat v1, PFloat v2) => v1.Value > v2.Value;
        public static bool operator <=(PFloat v1, PFloat v2) => v1.Value <= v2.Value;
        public static bool operator >=(PFloat v1, PFloat v2) => v1.Value >= v2.Value;

        public static implicit operator PFloat(int amount) => new PFloat(amount);
        public static implicit operator PFloat(long amount) => new PFloat(amount);
        public static implicit operator PFloat(float amount) => new PFloat(amount);
        public static explicit operator PFloat(double amount) => new PFloat((float)amount);
        public static implicit operator PFloat(PInt amount) => new PFloat(amount);
        public static implicit operator PFloat(PLong amount) => new PFloat(amount);
        public static explicit operator PFloat(PDouble amount) => new PFloat((float)amount);

        public static explicit operator int(PFloat amount) => (int)amount.Value;
        public static explicit operator long(PFloat amount) => (long)amount.Value;
        public static implicit operator float(PFloat amount) => amount.Value;
        public static implicit operator double(PFloat amount) => amount.Value;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;
            var other = (PFloat)obj;
            return this.Value == other.Value;
        }

        public override int GetHashCode() => Value.GetHashCode();

        #endregion

        #region Serialization
        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}", Offset.GetHashCode(), offset, key);
        }

        public static PFloat Parse(string str)
        {
            if (string.IsNullOrEmpty(str)) return new PFloat(0);

            string[] parts = str.Split('|');

            if (parts.Length != 3) return new PFloat(0);

            int hashCode = Convert.ToInt32(parts[0]);
            int offset = Convert.ToInt32(parts[1]);
            int key = Convert.ToInt32(parts[2]);

            PFloat storedFloat = new PFloat(offset, key);
            int storedHashCode = storedFloat.Offset.GetHashCode();

            if (hashCode == storedHashCode) return new PFloat(storedFloat.Value);
            return new PFloat(0);
        }

        public static bool TryParse(string str, out PFloat v)
        {
            try
            {
                string[] parts = str.Split('|');

                int hashCode = Convert.ToInt32(parts[0]);
                int offset = Convert.ToInt32(parts[1]);
                int key = Convert.ToInt32(parts[2]);

                PFloat storedFloat = new PFloat(offset, key);
                int storedHashCode = storedFloat.Offset.GetHashCode();

                if (hashCode == storedHashCode) v = new PFloat(storedFloat.Value);
                else v = new PFloat(0);

                return true;
            }
            catch (Exception)
            {
                v = new PFloat(0);
                return false;
            }
        }

        public IProtectedVariable AddValue(IProtectedVariable value)
        {
            return new PFloat(Value + ((PFloat)value).Value);
        }

        public IProtectedVariable SpendValue(IProtectedVariable value)
        {
            return new PFloat(Value - ((PFloat)value).Value);
        }

        public string ToValueString()
        {
            return Value.ToString();
        }

        #endregion
    }
}