using System;
using System.Numerics;
using BlackBears;

namespace BB.GameCore.Plugins.Utils.ProtectedTypes
{
    public struct PBigInteger : IProtectedVariable
    {

        private static Random rnd = new Random();

        private BigInteger value;
        private BigInteger key;

        public PBigInteger(BigInteger value) : this()
        {
            Init(value);
        }

        private PBigInteger(BigInteger value, BigInteger key)
        {
            this.value = value;
            this.key = key;
        }

        public BigInteger Value
        {
            get
            {
                if (key == 0) Init(0L);
                return ~(value ^ key);
            }
        }

        private void Init(BigInteger value)
        {
            this.value = value;
            this.key = rnd.Next(1000000, 1000000000);

            this.value = ~(value ^ key);
        }

        #region Math
        public static PBigInteger operator +(PBigInteger v) => new PBigInteger(v.Value);
        public static PBigInteger operator -(PBigInteger v) => new PBigInteger(-v.Value);
        public static PBigInteger operator ++(PBigInteger v) => new PBigInteger(v.Value + 1);
        public static PBigInteger operator --(PBigInteger v) => new PBigInteger(v.Value - 1);

        public static PBigInteger operator +(PBigInteger v1, PBigInteger v2) => new PBigInteger(v1.Value + v2.Value);
        public static PBigInteger operator -(PBigInteger v1, PBigInteger v2) => new PBigInteger(v1.Value - v2.Value);

        public static PBigInteger operator *(PBigInteger v1, PBigInteger v2) => new PBigInteger(v1.Value * v2.Value);
        public static PBigInteger operator /(PBigInteger v1, PBigInteger v2) => new PBigInteger(v1.Value / v2.Value);

        public static bool operator ==(PBigInteger v1, PBigInteger v2) => v1.Value == v2.Value;
        public static bool operator !=(PBigInteger v1, PBigInteger v2) => v1.Value != v2.Value;

        public static bool operator <(PBigInteger v1, PBigInteger v2) => v1.Value < v2.Value;
        public static bool operator >(PBigInteger v1, PBigInteger v2) => v1.Value > v2.Value;
        public static bool operator <=(PBigInteger v1, PBigInteger v2) => v1.Value <= v2.Value;
        public static bool operator >=(PBigInteger v1, PBigInteger v2) => v1.Value >= v2.Value;

        public static implicit operator PBigInteger(int amount) => new PBigInteger(amount);
        public static implicit operator PBigInteger(long amount) => new PBigInteger(amount);
        public static explicit operator PBigInteger(float amount) => new PBigInteger((long)amount);
        public static explicit operator PBigInteger(double amount) => new PBigInteger((long)amount);
        public static implicit operator PBigInteger(PInt amount) => new PBigInteger(new BigInteger(amount));
        public static explicit operator PBigInteger(PFloat amount) => new PBigInteger((long)amount);
        public static explicit operator PBigInteger(PDouble amount) => new PBigInteger((long)amount);

        public static implicit operator BigInteger(PBigInteger amount) => amount.Value;


        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;
            var other = (PBigInteger)obj;
            return this.Value == other.Value;
        }

        public override int GetHashCode() => Value.GetHashCode();

        #endregion

        #region Serialization
        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}", Value.GetHashCode(), value, key);
        }

        public static PBigInteger Parse(string str)
        {
            if (string.IsNullOrEmpty(str)) return new PBigInteger(0);

            string[] parts = str.Split('|');

            if (parts.Length != 3) return new PBigInteger(0);

            BigInteger hashCode = BigInteger.Parse(parts[0]);
            BigInteger value = BigInteger.Parse(parts[1]);
            BigInteger key = BigInteger.Parse(parts[2]);

            PBigInteger storedInt = new PBigInteger(value, key);
            int storedHashCode = storedInt.Value.GetHashCode();

            if (hashCode == storedHashCode) return new PBigInteger(storedInt.Value);
            return new PBigInteger(0);
        }

        public static bool TryParse(string str, out PBigInteger v)
        {
            try
            {
                string[] parts = str.Split('|');

                BigInteger hashCode = BigInteger.Parse(parts[0]);
                BigInteger value = BigInteger.Parse(parts[1]);
                BigInteger key = BigInteger.Parse(parts[2]);

                PBigInteger storedInt = new PBigInteger(value, key);
                int storedHashCode = storedInt.Value.GetHashCode();

                if (hashCode == storedHashCode) v = new PBigInteger(storedInt.Value);
                else v = new PBigInteger(0);
                return true;
            }
            catch (Exception)
            {
                v = new PBigInteger(0);
                return false;
            }
        }

        #endregion

        public bool IsNegative => Value < 0;
        public IProtectedVariable AddValue(IProtectedVariable value)
        {
            return new PBigInteger(Value + ((PBigInteger)value).Value);

        }

        public IProtectedVariable SpendValue(IProtectedVariable value)
        {
            return new PBigInteger(Value - ((PBigInteger)value).Value);

        }

        public string ToValueString()
        {
            return Value.ToString();
        }
    }
}