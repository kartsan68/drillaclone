﻿using System;
using System.Runtime.InteropServices;

namespace BlackBears
{

    [StructLayout(LayoutKind.Explicit)]
    unsafe public struct PDouble : IProtectedVariable
    {
        private static Random rnd = new Random();

        [FieldOffset(0)] private double value;
        [FieldOffset(0)] private long offset;

        [FieldOffset(8)] private long key;

        public PDouble(double value) : this()
        {
            Init(value);
        }

        private PDouble(long offset, long key)
        {
            this.key = key;

            this.value = 0;
            this.offset = offset;
        }

        public double Value
        {
            get
            {
                if (key == 0) Init(0.0);
                long l = Offset;
                return *(double*)&l;
            }
        }

        private long Offset
        {
            get
            {
                long l = offset;
                l ^= key;
                return ~l;
            }
        }

        public bool IsNegative => Value < 0;

        private void Init(double value)
        {
            this.key = rnd.Next(1000000, 1000000000);

            this.offset = 0;
            this.value = value;

            this.offset ^= key;
            this.offset = ~offset;
        }

        #region Math

        public static PDouble operator +(PDouble v) => new PDouble(v.Value);
        public static PDouble operator -(PDouble v) => new PDouble(-v.Value);
        public static PDouble operator ++(PDouble v) => new PDouble(v.Value + 1);
        public static PDouble operator --(PDouble v) => new PDouble(v.Value - 1);

        public static PDouble operator +(PDouble v1, PDouble v2) => new PDouble(v1.Value + v2.Value);
        public static PDouble operator -(PDouble v1, PDouble v2) => new PDouble(v1.Value - v2.Value);

        public static PDouble operator *(PDouble v1, PDouble v2) => new PDouble(v1.Value * v2.Value);
        public static PDouble operator /(PDouble v1, PDouble v2) => new PDouble(v1.Value / v2.Value);

        public static bool operator ==(PDouble v1, PDouble v2) => v1.Value == v2.Value;
        public static bool operator !=(PDouble v1, PDouble v2) => v1.Value != v2.Value;

        public static bool operator <(PDouble v1, PDouble v2) => v1.Value < v2.Value;
        public static bool operator >(PDouble v1, PDouble v2) => v1.Value > v2.Value;
        public static bool operator <=(PDouble v1, PDouble v2) => v1.Value <= v2.Value;
        public static bool operator >=(PDouble v1, PDouble v2) => v1.Value >= v2.Value;

        public static implicit operator PDouble(int amount) => new PDouble(amount);
        public static implicit operator PDouble(long amount) => new PDouble(amount);
        public static implicit operator PDouble(float amount) => new PDouble(amount);
        public static implicit operator PDouble(double amount) => new PDouble(amount);
        public static implicit operator PDouble(PInt amount) => new PDouble(amount);
        public static implicit operator PDouble(PFloat amount) => new PDouble(amount);
        public static implicit operator PDouble(PLong amount) => new PDouble(amount);

        public static explicit operator int(PDouble amount) => (int)amount.Value;
        public static explicit operator long(PDouble amount) => (long)amount.Value;
        public static explicit operator float(PDouble amount) => (float)amount.Value;
        public static implicit operator double(PDouble amount) => amount.Value;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;
            var other = (PDouble)obj;
            return this.Value == other.Value;
        }

        public override int GetHashCode() => Value.GetHashCode();

        #endregion

        #region Serialization

        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}", Offset.GetHashCode(), offset, key);
        }

        public static PDouble Parse(string str)
        {
            if (string.IsNullOrEmpty(str)) return new PDouble(0);

            string[] parts = str.Split('|');

            if (parts.Length != 3) return new PDouble(0);

            int hashCode = Convert.ToInt32(parts[0]);
            long offset = Convert.ToInt64(parts[1]);
            long key = Convert.ToInt64(parts[2]);

            PDouble storedDouble = new PDouble(offset, key);
            int storedHashCode = storedDouble.Offset.GetHashCode();

            if (hashCode == storedHashCode) return new PDouble(storedDouble.Value);
            return new PDouble(0);
        }

        public static bool TryParse(string str, out PDouble v)
        {
            try
            {
                string[] parts = str.Split('|');

                int hashCode = Convert.ToInt32(parts[0]);
                long offset = Convert.ToInt64(parts[1]);
                long key = Convert.ToInt64(parts[2]);

                PDouble storedDouble = new PDouble(offset, key);
                int storedHashCode = storedDouble.Offset.GetHashCode();

                if (hashCode == storedHashCode) v =  new PDouble(storedDouble.Value);
                else v = new PDouble(0);

                return true;
            }
            catch (Exception)
            {
                v = new PDouble(0);
                return false;
            }
        }


        public IProtectedVariable AddValue(IProtectedVariable value)
        {
            return new PDouble(Value + ((PDouble)value).Value);
        }

        public IProtectedVariable SpendValue(IProtectedVariable value)
        {
            return new PDouble(Value - ((PDouble)value).Value);
        }

        public string ToValueString()
        {
            return Value.ToString();
        }

        #endregion
    }

}