namespace BlackBears
{
    public interface IProtectedVariable
    {
        bool IsNegative { get; }

        IProtectedVariable AddValue(IProtectedVariable value);
        IProtectedVariable SpendValue(IProtectedVariable value);
        string ToValueString();
    } 
}