using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackBears.Utils
{

	[RequireComponent(typeof(EventSystem))]
    public class DragCorrector : MonoBehaviour
    {
        public int basePPI = 210;

        void Start()
        {
            var es = GetComponent<EventSystem>();
            es.pixelDragThreshold = es.pixelDragThreshold * (int)Screen.dpi / basePPI;
			Destroy(this);
        }

    }

}