using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UnityEngine.UI
{

    [RequireComponent(typeof(RectTransform))]
    public class PinchZoomUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {

        public float scaleSpeed = 0.001f;

        private const int pinchPointersCount = 2;

        private List<int> pointers = new List<int>();

        private Vector2[] positions = new Vector2[pinchPointersCount];
        private Vector2[] deltas = new Vector2[pinchPointersCount];

        private bool needRecalc = false;
        private bool _detectEnabled = true;
        private float _scaleDiff;

        private UnityEvent onPinchStarted = new UnityEvent();
        private UnityEvent onPinchEnded = new UnityEvent();

        private FloatUnityEvent onScaleDiffChanged = new FloatUnityEvent();

        public bool InPinch { get; private set; }

        public float ScaleDiff
        {
            get { return _scaleDiff; }
            private set
            {
                _scaleDiff = value;
                onScaleDiffChanged.Invoke(_scaleDiff);
            }
        }

        public bool DetectEnabled 
        {
            get { return _detectEnabled; }
            set
            {
                _detectEnabled = value;
                if (!_detectEnabled) pointers.Clear();
            }
        }

        public FloatUnityEvent OnScaleDiffChanged { get { return onScaleDiffChanged; } }
        public UnityEvent OnPinchStarted { get { return onPinchStarted; } }
        public UnityEvent OnPinchEnded { get { return onPinchEnded; } }

        private void OnDestroy()
        {
            if (InPinch) onPinchEnded.Invoke();
            InPinch = false;
        }

        private void OnDisable()
        {
            pointers.Clear();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!DetectEnabled) return;

            pointers.Add(eventData.pointerId);
            if (pointers.Count <= pinchPointersCount)
            {
                var index = pointers.Count - 1;
                positions[index] = eventData.position;
                deltas[index] = eventData.delta;
            }
            if (pointers.Count == pinchPointersCount)
            {
                InPinch = true;
                onPinchStarted.Invoke();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!DetectEnabled) return;

            pointers.Remove(eventData.pointerId);
            if (pointers.Count == (pinchPointersCount - 1))
            {
                InPinch = false;
                onPinchEnded.Invoke();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!DetectEnabled) return;

            var pointerIndex = -1;
            for (var i = 0; i < pinchPointersCount && pointerIndex < 0; i++)
            {
                if (pointers[i] == eventData.pointerId) pointerIndex = i;
            }

            if (pointerIndex < 0) return;

            needRecalc = true;
            positions[pointerIndex] = eventData.position;
            deltas[pointerIndex] = eventData.delta;
        }

        private void LateUpdate()
        {
            if (!InPinch) return;
            if (!needRecalc) return;
            needRecalc = false;

            Vector2 firstPos = positions[0];
            Vector2 secondPos = positions[1];

            var firstPrevPos = firstPos - deltas[0];
            var secondPrevPos = secondPos - deltas[1];

            float prevTouchMagnitude = (firstPrevPos - secondPrevPos).magnitude;
            float touchMagnitude = (firstPos - secondPos).magnitude;

            float deltaMagnitudeDiff = prevTouchMagnitude - touchMagnitude;

            ScaleDiff = deltaMagnitudeDiff * scaleSpeed;
        }

    }

}
