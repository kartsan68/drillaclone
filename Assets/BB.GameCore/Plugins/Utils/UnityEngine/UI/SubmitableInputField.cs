﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{

    [AddComponentMenu("UI/Submitable Input Field", 32)]
    public class SubmitableInputField : InputField
    {

        public bool OnLateUpdateNow { get; private set; }
        public bool OnUpdateSelectedNow { get; private set; }

        public bool IsSubmitNow
        {
            get
            {
                bool keyboardLostFocusOrCanceled = m_Keyboard != null && (m_Keyboard.status == TouchScreenKeyboard.Status.Canceled ||
                    m_Keyboard.status == TouchScreenKeyboard.Status.LostFocus);

                return (!wasCanceled || keyboardLostFocusOrCanceled) && (OnLateUpdateNow || OnUpdateSelectedNow);
            }
        }

        public override void OnUpdateSelected(BaseEventData eventData)
        {
            OnUpdateSelectedNow = true;
            base.OnUpdateSelected(eventData);
            OnUpdateSelectedNow = false;
        }

        protected override void LateUpdate()
        {
            OnLateUpdateNow = true;
            base.LateUpdate();
            OnLateUpdateNow = false;
        }

    }

}