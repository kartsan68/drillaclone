﻿using UnityEngine;

namespace BlackBears.Utils
{

    public static class ColorUtils
    {

        public static Color FromHex(uint color)
        {
            uint mask = 0x000000FF;

            uint r = (color >> 24) & mask;
            uint g = (color >> 16) & mask;
            uint b = (color >> 8) & mask;
            uint a = color & mask;

            return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
        }

        public static Color WithAlpha(this Color color, float alpha) => new Color(color.r, color.g, color.b, alpha);

    }

}