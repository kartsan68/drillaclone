using BlackBears.GameCore.Graphic;
using BlackBears.Utils.Scaling;
using UnityEngine;

namespace BlackBears.Utils
{
    [RequireComponent(typeof(DpiCanvasScaler))]
    [ExecuteInEditMode]
    public class CoreDpiCanvasScalerKey : MonoBehaviour
    {

        private void Awake()
        {
            var scaler = GetComponent<DpiCanvasScaler>();
            scaler.key = CoreCache.key;
            #if UNITY_EDITOR
            if (!Application.isPlaying) return;
            #endif
            Destroy(this);            
        }

        #if UNITY_EDITOR

        private void Update()
        {
            if (!Application.isPlaying) Awake();
        }

        #endif

    }
}