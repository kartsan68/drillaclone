﻿using UnityEngine;

namespace BlackBears.Utils.Scaling
{

    public class ScalePreferences : MonoBehaviour
    {

        public static float MatchWidthOrHeight { get; private set; }

        [SerializeField] private float switchRatio = 0.56f;
        [SerializeField] private bool simpleCompare = false;
        [SerializeField] private Vector2 pref;
        [SerializeField] private float lessMatchValue = 0f;
        [SerializeField] private float greaterMatchValue = 1f;

        private void Awake()
        {
            float ratio = (float)Screen.height / (float)Screen.width;

            if (simpleCompare)
            {
				float prefRatio = pref.y / pref.x;
                MatchWidthOrHeight = ratio > prefRatio ? lessMatchValue : greaterMatchValue;
            }
            else
            {
                MatchWidthOrHeight = ratio > switchRatio ? lessMatchValue : greaterMatchValue;
            }
        }

    }

}