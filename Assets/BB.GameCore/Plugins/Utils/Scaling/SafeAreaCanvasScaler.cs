using BlackBears.GameCore.Graphic;
using BlackBears.Resolutor;
using BlackBears.Resolutor.Scaling;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Utils.Scaling
{

	[ExecuteInEditMode]
	[RequireComponent(typeof(CanvasScaler))]
	public class SafeAreaCanvasScaler : MonoBehaviour, IScaleSelector
	{

		public string key;

		[SerializeField] private Vector2 referenceResolution = new Vector2(320, 568);
		[SerializeField] private float minScale = 2.0f;
		
		private bool started = false;
		private float currentScale;


        public Scale GraphicScale
        {
            get
            {
                int scale = Mathf.Clamp((int)CurrentScale, (int)ScaleUtils.minScale, (int)ScaleUtils.maxScale);
				return (Scale)scale;
            }
        }
        public float CurrentScale { get { return currentScale; } }

        public void DetectScale() { Awake(); }

        private void Awake()
		{
			if (started) return;
			started = true;
			ChangeScale();
		}

		#if UNITY_EDITOR
		private void Update()
		{
			ChangeScale();
		}
		#endif

		private void ChangeScale()
		{
			var screenSize = new Vector2(Screen.width, Screen.height);
            var screenCoefficients = new Vector2(screenSize.x / referenceResolution.x,
                screenSize.y / referenceResolution.y);
            currentScale = Mathf.Min(screenCoefficients.x, screenCoefficients.y);
			if (currentScale < minScale) currentScale = minScale;
		}


    }

}