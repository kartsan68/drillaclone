﻿using BlackBears.GameCore.Graphic;
using BlackBears.Resolutor;
using BlackBears.Resolutor.Scaling;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBears.Utils.Scaling
{

	[ExecuteInEditMode]
	[RequireComponent(typeof(CanvasScaler))]
	public class DpiCanvasScaler : MonoBehaviour, IScaleSelector
	{

		public string key;

		[SerializeField] private Vector2 referenceResolution = new Vector2(320, 568);
		private CanvasScaler scaler;
		private bool started = false;


        public Scale GraphicScale
        {
            get
            {
                int scale = Mathf.Clamp((int)CurrentScale, (int)ScaleUtils.minScale, (int)ScaleUtils.maxScale);
				return (Scale)scale;
            }
        }
        public float CurrentScale { get { return scaler.scaleFactor; } }

        public void DetectScale() { Awake(); }

        private void Awake()
		{
			if (started) return;
			started = true;
			UICache.Instance.RegisterSelector(key, this);
			scaler = GetComponent<CanvasScaler>();
			ChangeScale();
		}

		private void OnDestroy()
		{
			UICache.Instance.UnregisterSelector(key);
		}

		#if UNITY_EDITOR
		private void Update()
		{
			ChangeScale();
		}
		#endif

		private void ChangeScale()
		{
			var screenSize = new Vector2(Screen.width, Screen.height);
            var screenCoefficients = new Vector2(screenSize.x / referenceResolution.x,
                screenSize.y / referenceResolution.y);
            float coefficient = (int)Mathf.Min(screenCoefficients.x, screenCoefficients.y);
			if (coefficient < 2.0f) coefficient = 2.0f;
			scaler.scaleFactor = coefficient;
		}

    }

}