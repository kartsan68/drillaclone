using BlackBears.Resolutor;

namespace BlackBears.Utils.Scaling
{

    public interface IScaleSelector
    {
        Scale GraphicScale { get; }
        float CurrentScale { get; }

        void DetectScale();
    }

}