using BlackBears.GameCore.Graphic;
using BlackBears.Resolutor;
using BlackBears.Resolutor.Scaling;
using UnityEngine;

namespace BlackBears.Utils.Scaling
{

    [ExecuteInEditMode]
    public class DpiPrefsScaler : MonoBehaviour, IScaleSelector
    {

        public string key;
        [SerializeField] private ScaleSelector selectorPrefs;
        #if UNITY_EDITOR
        public bool updateRules;
        #endif

        private bool started;

        public Scale GraphicScale => selectorPrefs.CurrentScale;
        public float CurrentScale
        {
            get 
            {
                if (GraphicScale == Scale.Undefined) return (float)Scale.x2;
                return (float)GraphicScale;
            }
        }

        private void Awake()
        {
			if (started) return;
			started = true;
			UICache.Instance.RegisterSelector(key, this);
            selectorPrefs.DetectScale();
        }

        private void OnDestroy()
        {
			UICache.Instance.UnregisterSelector(key);
        }

        public void DetectScale() => Awake();

        #if UNITY_EDITOR

        private void Update()
        {
            if (!updateRules) return;
            updateRules = false;
            selectorPrefs.DetectScale();
        }

        #endif

    }

}