using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Additional
{

    [RequireComponent(typeof(ScrollRect), typeof(RectTransform))]
    public class ScrollPaginator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,
        IBeginDragHandler, IEndDragHandler
    {

        public float moveNextVelocity = 100;
        public float minVelocity = 100;
        public float maxTimeMoveToPos = 0;
        public float minDistanceToNextItem = 0;
        public bool blockOnContentBounds = false;
        public bool canSkipPagesByFling = true;
        public bool fullPageSize = true;
        public float pageSize;
        public int indexOffset;

        private ScrollRect scrollRect;
        private RectTransform rectTransform;

        private bool onDrag;
        private float startDrag;
        private int animationId;

        private int lockIndex = 0;
        private bool locked = false;

        private int selectedPageIndex = 0;
        private IntUnityEvent onPageSelected = new IntUnityEvent();
        private IntUnityEvent onEndMove = new IntUnityEvent();

        public IntUnityEvent OnPageSelected => onPageSelected;
        public IntUnityEvent OnEndMove => onEndMove;

        private ScrollRect ScrollRect => scrollRect ?? (scrollRect = GetComponent<ScrollRect>());
        private RectTransform RectTransform => rectTransform ?? (rectTransform = GetComponent<RectTransform>());

        public int CurrentPage
        {
            get { return indexOffset - selectedPageIndex; }
            private set
            {
                if (selectedPageIndex == value) return;
                selectedPageIndex = value;
                onPageSelected.Invoke(CurrentPage);
            }
        }

        public void Start() => UpdatePageSize();

        public void LockOnIndex(int index, bool force)
        {
            lockIndex = -index;
            locked = true;
            MoveToIndex(index, force);
        }

        public void Unlock() => locked = false;

        public void MoveToIndex(int index, bool force)
        {
            var pos = ScrollRect.content.anchoredPosition;
            pos.x = IndexToPos(-index);
            if (force)
            {
                CancelAnimation();
                ScrollRect.StopMovement();
                ScrollRect.content.anchoredPosition = pos;
            }
            else
            {
                MoveToPos(pos, moveNextVelocity);
            }
        }

        public void UpdatePageSize()
        {
            if (fullPageSize) pageSize = RectTransform.rect.width;
        }

        public void ResetSelectedPageIndex() => CurrentPage = 0;

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData) => CancelAnimation();

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if (onDrag) return;
            MoveToClosest(0);
        }

        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            onDrag = true;
            startDrag = ScrollRect.content.anchoredPosition.x;
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            onDrag = false;
            var velocity = ScrollRect.velocity.x;
            ScrollRect.velocity = Vector2.zero;

            if (Mathf.Abs(velocity) >= moveNextVelocity)
            {
                if (canSkipPagesByFling) startDrag = ScrollRect.content.anchoredPosition.x;
                if (velocity < 0) MoveToNext(velocity);
                else MoveToPrevious(velocity);
            }
            else
            {
                var distance = startDrag - ScrollRect.content.anchoredPosition.x;

                if ((minDistanceToNextItem != 0) && (Mathf.Abs(distance) > minDistanceToNextItem))
                {
                    if (distance > 0) MoveToNext();
                    else MoveToPrevious();
                }
                else
                {
                    MoveToClosest(velocity);
                }
            }
        }

        private void MoveToNext(float velocity = 1f)
        {
            var pos = ScrollRect.content.anchoredPosition;
            int index = PosToIndex(startDrag);
            pos.x = IndexToPos(index - 1);
            MoveToPos(pos, velocity);
        }

        private void MoveToPrevious(float velocity = 1f)
        {
            var pos = ScrollRect.content.anchoredPosition;
            int index = PosToIndex(startDrag);
            pos.x = IndexToPos(index + 1);
            MoveToPos(pos, velocity);
        }

        private void MoveToClosest(float velocity)
        {
            var pos = ScrollRect.content.anchoredPosition;
            int currentIndex = PosToIndex(pos.x);
            float currentPos = IndexToPos(currentIndex);
            float nextPos = IndexToPos(currentIndex - 1);
            float prevPos = IndexToPos(currentIndex + 1);

            pos.x = ToNearest(pos.x, ToNearest(pos.x, nextPos, currentPos), ToNearest(pos.x, currentPos, prevPos));
            MoveToPos(pos, velocity);
        }

        private void MoveToPos(Vector2 position, float velocity)
        {
            velocity = ValidateVelocity(velocity);
            float time = Mathf.Abs((ScrollRect.content.anchoredPosition.x - position.x) / velocity);
            if(maxTimeMoveToPos != 0) time = Mathf.Min(maxTimeMoveToPos, time);

            CurrentPage = PosToIndex(position.x);

            animationId = LeanTween.moveAnchored(ScrollRect.content, position, time)
                .setEaseOutQuad()
                .setOnComplete(() =>
                {
                    ResetAnimationId();
                    onEndMove.Invoke(CurrentPage);
                }).id;
        }

        private float ValidateVelocity(float velocity) =>
            Mathf.Abs(velocity) > minVelocity ? velocity : minVelocity * Mathf.Sign(velocity);

        private int PosToIndex(float pos) => (int)(pos / pageSize);

        private float IndexToPos(int index)
        {
            if (locked) index = lockIndex;
            float x = index * pageSize;
            if (blockOnContentBounds)
            {
                float contentW = ScrollRect.content.rect.width;

                if (x > 0) x = 0;
                else if (x < -contentW) x = PosToIndex(contentW) * pageSize;
            }
            return x;
        }

        private void CancelAnimation()
        {
            if (animationId >= 0) LeanTween.cancel(animationId);
            ResetAnimationId();
        }

        private void ResetAnimationId() => animationId = -1;

        private float ToNearest(float x, float first, float second)
        {
            var d1 = Mathf.Abs(first - x);
            var d2 = Mathf.Abs(second - x);
            return d1 < d2 ? first : second;
        }

        private void OnDestroy() 
        {
            if(ScrollRect != null) LeanTween.cancel(ScrollRect.content);    
        }

    }

}