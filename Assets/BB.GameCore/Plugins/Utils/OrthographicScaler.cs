﻿using UnityEngine;

namespace BlackBears.Utils
{

    [RequireComponent(typeof(Camera))]
    [ExecuteInEditMode]
    public class OrthographicScaler : MonoBehaviour
    {

        public enum ScaleMode
        {
            ConstantPixelSize,
            ScaleWithScreenSize,
            ConstantPhysicalSize
        }

        [Tooltip("Determines how UI elements in the Canvas are scaled.")]
        [SerializeField] private ScaleMode m_UiScaleMode = ScaleMode.ConstantPixelSize;

        public ScaleMode uiScaleMode { get { return m_UiScaleMode; } set { m_UiScaleMode = value; } }

        [Tooltip("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI.")]
        [SerializeField] protected float m_ReferencePixelsPerUnit = 100;

        public float referencePixelsPerUnit { get { return m_ReferencePixelsPerUnit; } set { m_ReferencePixelsPerUnit = value; } }


        // Constant Pixel Size settings

        [Tooltip("Scales all UI elements in the Canvas by this factor.")]
        [SerializeField] protected float m_ScaleFactor = 1;

        public float scaleFactor { get { return m_ScaleFactor; } set { m_ScaleFactor = value; } }


        // Scale With Screen Size settings

        public enum ScreenMatchMode
        {
            MatchWidthOrHeight = 0,
            Expand = 1,
            Shrink = 2

        }

        [Tooltip("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode.")]
        [SerializeField] protected Vector2 m_ReferenceResolution = new Vector2(800, 600);

        public Vector2 referenceResolution { get { return m_ReferenceResolution; } set { m_ReferenceResolution = value; } }

        [Tooltip("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution.")]
        [SerializeField] protected ScreenMatchMode m_ScreenMatchMode = ScreenMatchMode.MatchWidthOrHeight;

        public ScreenMatchMode screenMatchMode { get { return m_ScreenMatchMode; } set { m_ScreenMatchMode = value; } }

        [Tooltip("Determines if the scaling is using the width or height as reference, or a mix in between.")]
        [Range(0, 1), SerializeField] protected float m_MatchWidthOrHeight = 0;

        public float matchWidthOrHeight { get { return m_MatchWidthOrHeight; } set { m_MatchWidthOrHeight = value; } }

        // The log base doesn't have any influence on the results whatsoever, as long as the same base is used everywhere.
        private const float kLogBase = 2;

        // Constant Physical Size settings

        public enum Unit
        {
            Centimeters,
            Millimeters,
            Inches,
            Points,
            Picas

        }

        [Tooltip("The physical unit to specify positions and sizes in.")]
        [SerializeField] protected Unit m_PhysicalUnit = Unit.Points;

        public Unit physicalUnit { get { return m_PhysicalUnit; } set { m_PhysicalUnit = value; } }

        [Tooltip("The DPI to assume if the screen DPI is not known.")]
        [SerializeField] protected float m_FallbackScreenDPI = 96;

        public float fallbackScreenDPI { get { return m_FallbackScreenDPI; } set { m_FallbackScreenDPI = value; } }

        [Tooltip("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting.")]
        [SerializeField] protected float m_DefaultSpriteDPI = 96;

        public float defaultSpriteDPI { get { return m_DefaultSpriteDPI; } set { m_DefaultSpriteDPI = value; } }

        // General variables

        private new Camera camera;
        private float prevOrthographicSize = 1;
        private float prevReferencePixelsPerUnit = 100;

        public float ScreenScale { get { return prevOrthographicSize * prevReferencePixelsPerUnit; } }
        public Vector2 ScreenSize { get { return new Vector2(Screen.width, Screen.height) / ScreenScale; } }

        private void Awake()
        {
            camera = GetComponent<Camera>();
            Handle();
            var p = transform.localPosition;
            p.x = Screen.width / (2 * ScreenScale);
            p.y = Screen.height / (2 * ScreenScale);
            transform.localPosition = p;
        }

        #if UNITY_EDITOR

        private void Start()
        {
            if (!Application.isPlaying)
            {
                Awake();
                Update();
            }
        }

        #endif

        private void Update()
        {
            Handle();
        }

        protected void OnEnable()
        {
            camera = GetComponent<Camera>();
            Handle();
        }

        protected void OnDisable()
        {
            SetOrthographicSize(1);
            SetReferencePixelsPerUnit(100);
        }

        protected virtual void Handle()
        {
            if (camera == null) return;

            switch (m_UiScaleMode)
            {
                case ScaleMode.ConstantPixelSize:
                    HandleConstantPixelSize();
                    break;
                case ScaleMode.ScaleWithScreenSize:
                    HandleScaleWithScreenSize();
                    break;
                case ScaleMode.ConstantPhysicalSize:
                    HandleConstantPhysicalSize();
                    break;
            }
        }

        protected virtual void HandleConstantPixelSize()
        {
            SetOrthographicSize(m_ScaleFactor);
            SetReferencePixelsPerUnit(m_ReferencePixelsPerUnit);
        }

        protected virtual void HandleScaleWithScreenSize()
        {
            Vector2 screenSize = new Vector2(Screen.width, Screen.height);

            float orthoSize = 0;
            switch (m_ScreenMatchMode)
            {
                case ScreenMatchMode.MatchWidthOrHeight:
                    {
                        // We take the log of the relative width and height before taking the average.
                        // Then we transform it back in the original space.
                        // the reason to transform in and out of logarithmic space is to have better behavior.
                        // If one axis has twice resolution and the other has half, it should even out if widthOrHeight value is at 0.5.
                        // In normal space the average would be (0.5 + 2) / 2 = 1.25
                        // In logarithmic space the average is (-1 + 1) / 2 = 0
                        float logWidth = Mathf.Log(screenSize.x / m_ReferenceResolution.x, kLogBase);
                        float logHeight = Mathf.Log(screenSize.y / m_ReferenceResolution.y, kLogBase);
                        float logWeightedAverage = Mathf.Lerp(logWidth, logHeight, m_MatchWidthOrHeight);
                        orthoSize = Mathf.Pow(kLogBase, logWeightedAverage);
                        break;
                    }
                case ScreenMatchMode.Expand:
                    {
                        orthoSize = Mathf.Min(screenSize.x / m_ReferenceResolution.x, screenSize.y / m_ReferenceResolution.y);
                        break;
                    }
                case ScreenMatchMode.Shrink:
                    {
                        orthoSize = Mathf.Max(screenSize.x / m_ReferenceResolution.x, screenSize.y / m_ReferenceResolution.y);
                        break;
                    }
            }

            SetOrthographicSize(orthoSize);
            SetReferencePixelsPerUnit(m_ReferencePixelsPerUnit);
        }

        protected virtual void HandleConstantPhysicalSize()
        {
            float currentDpi = Screen.dpi;
            float dpi = (currentDpi == 0 ? m_FallbackScreenDPI : currentDpi);
            float targetDPI = 1;
            switch (m_PhysicalUnit)
            {
                case Unit.Centimeters:
                    targetDPI = 2.54f;
                    break;
                case Unit.Millimeters:
                    targetDPI = 25.4f;
                    break;
                case Unit.Inches:
                    targetDPI = 1;
                    break;
                case Unit.Points:
                    targetDPI = 72;
                    break;
                case Unit.Picas:
                    targetDPI = 6;
                    break;
            }

            SetOrthographicSize(dpi / targetDPI);
            SetReferencePixelsPerUnit(m_ReferencePixelsPerUnit * targetDPI / m_DefaultSpriteDPI);
        }

        protected void SetOrthographicSize(float orthographicSize)
        {
            camera.orthographicSize = Screen.height / (2 * orthographicSize);
            prevOrthographicSize = orthographicSize;
        }

        protected void SetReferencePixelsPerUnit(float referencePixelsPerUnit)
        {
            if (referencePixelsPerUnit == prevReferencePixelsPerUnit) return;

            prevReferencePixelsPerUnit = referencePixelsPerUnit;
        }

    }

}