﻿using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Clans.Controller
{

    [RequireComponent(typeof(Text))]
    public class LocalizationController : MonoBehaviour
    {

        [SerializeField] private Modificator modificator = Modificator.DontModify;

        private void Awake()
        {
            var text = GetComponent<Text>();
            text.text = BBGC.Features.Localization().Get(text.text, modificator);
            Destroy(this);
        }

    }

}