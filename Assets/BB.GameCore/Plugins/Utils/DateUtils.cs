using System;
using UnityEngine.UI;

namespace BlackBears.Utils
{

    public static class DateUtils
    {
        public const int secondsInDay = 86400;
        public const int secondsInHour = 3600;
        public static readonly int daysInMonth = 30;
        public static readonly int daysInYear = 365;
        public static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static long UnixTimestamp(this DateTime utc)
        {
            return (long)(utc - epoch).TotalSeconds;
        }

        public static DateTime FromUnixTimestamp(long timestamp)
        {
            return epoch.AddSeconds(timestamp);
        }

        public static TimedMessage SecondsToTimedMessage(long seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return BuildTimedMessage(ts);
        }

        public static TimedMessage ToTimedMessage(TimeSpan ts) { return BuildTimedMessage(ts); }

        public static string SecondsToHHMMSS(long seconds)
        {
            return SecondsToHHMMSS(TimeSpan.FromSeconds(seconds));
        }

        public static string SecondsToHHMMSS(float seconds)
        {
            return SecondsToHHMMSS(TimeSpan.FromSeconds(seconds));
        }

        public static string SecondsToMMSS(float seconds)
        {
            return SecondsToMMSS(TimeSpan.FromSeconds(seconds));
        }

        public static string SecondsToHHMMSS(TimeSpan ts)
        {
            return string.Format("{0:D2}:{1:D2}:{2:D2}", (int)ts.TotalHours, ts.Minutes, ts.Seconds);
        }

        public static string SecondsToMMSS(TimeSpan ts)
        {
            return string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);
        }

        public static int SecondsToFullHours(float seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return (int)ts.TotalHours;
        }
        public static int SecondsToFullMinutes(long seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return (int)ts.TotalMinutes;
        }

        public static int SecondsToFullDays(long seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return (int)ts.TotalDays;
        }

        internal static long GetDayStartTimestamp(long timestamp)
        {
            var time = FromUnixTimestamp(timestamp);
            return UnixTimestamp(time.Date);
        }

        internal static int GetDaysCount(long first, long next)
        {
            return TimeSpan.FromSeconds(next - first).Days;
        }

        internal static long AddDays(long timestamp, int offset)
        {
            var time = FromUnixTimestamp(timestamp);
            return UnixTimestamp(time.AddDays(offset).Date);
        }

        public static TimedMessage SecondsToTimedMessage(float seconds, bool withSeconds = false)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return BuildTimedMessage(ts, withSeconds);
        }

        public static TimedMessage SecondsToTimedMessageWithPrefix(float seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            if ((long)ts.TotalDays > 0) return TimedMessage.BuildDaysMessageWithPrefix((long)ts.TotalDays);
            if ((long)ts.TotalHours > 0) return TimedMessage.BuildHoursMessageWithPrefix((long)ts.TotalHours);
            if ((long)ts.TotalMinutes > 0) return TimedMessage.BuildMinutesMessageWithPrefix((long)ts.TotalMinutes);
            return TimedMessage.BuildSecondsMessageWithPrefix((long)ts.TotalSeconds);
        }

        public static TimedMessage SecondsToHourTimedMessage(float seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return TimedMessage.BuildHoursMessage((long)ts.TotalHours);
        }

        public static TimedMessage SecondsToSecondsTimedMessage(float seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return TimedMessage.BuildSecondsMessage((long)ts.TotalSeconds);
        }

        private static TimedMessage BuildTimedMessage(TimeSpan ts, bool withSeconds = false)
        {
            if ((long)ts.TotalDays > daysInYear) return TimedMessage.BuildYearsMessage((long)ts.TotalDays);
            if ((long)ts.TotalDays > daysInMonth) return TimedMessage.BuildMonthsMessage((long)ts.TotalDays);
            if ((long)ts.TotalDays > 0) return TimedMessage.BuildDaysMessage((long)ts.TotalDays);
            if ((long)ts.TotalHours > 0) return TimedMessage.BuildHoursMessage((long)ts.TotalHours);
            if ((long)ts.TotalMinutes > 0) return TimedMessage.BuildMinutesMessage((long)ts.TotalMinutes);
            if (withSeconds) return TimedMessage.BuildSecondsMessage((long)ts.TotalSeconds);
            else return TimedMessage.BuildJustNowMessage();
        }

    }

    public struct TimedMessage
    {

        public const string yearsKey = "PLURAL_YEARS";
        public const string monthKey = "PLURAL_MONTH";
        public const string daysKey = "PLURAL_DAYS";
        public const string hoursKey = "PLURAL_HOURS";
        public const string minutesKey = "PLURAL_MINUTES";
        public const string secondsKey = "PLURAL_SECONDS";
        public const string justNowKey = "JUST_NOW";


        public readonly string key;
        public readonly long valueCount;

        public static TimedMessage BuildYearsMessage(long daysCount) { return new TimedMessage(yearsKey, daysCount / 365); }
        public static TimedMessage BuildMonthsMessage(long daysCount) { return new TimedMessage(monthKey, daysCount / 30); }
        public static TimedMessage BuildDaysMessage(long daysCount) { return new TimedMessage(daysKey, daysCount); }
        public static TimedMessage BuildHoursMessage(long hourCount) { return new TimedMessage(hoursKey, hourCount); }
        public static TimedMessage BuildMinutesMessage(long minutesCount) { return new TimedMessage(minutesKey, minutesCount); }
        public static TimedMessage BuildSecondsMessage(long secondsCount) { return new TimedMessage(secondsKey, secondsCount); }
        public static TimedMessage BuildJustNowMessage() { return new TimedMessage(justNowKey, 0); }

        public static TimedMessage BuildDaysMessageWithPrefix(long daysCount) { return new TimedMessage("FOR_PLURAL_DAYS", daysCount); }
        public static TimedMessage BuildHoursMessageWithPrefix(long hourCount) { return new TimedMessage("FOR_PLURAL_HOURS", hourCount); }
        public static TimedMessage BuildMinutesMessageWithPrefix(long minutesCount) { return new TimedMessage("FOR_PLURAL_MINUTES", minutesCount); }
        public static TimedMessage BuildSecondsMessageWithPrefix(long secondsCount) { return new TimedMessage("FOR_PLURAL_SECONDS", secondsCount); }


        private TimedMessage(string key, long valueCount)
        {
            this.key = key;
            this.valueCount = valueCount;
        }

        public string ToDebugString() { return string.Format("{0} {1}", key, valueCount); }

    }

}