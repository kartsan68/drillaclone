using System.Collections.Generic;

namespace BlackBears.Utils.Collections
{

    public static class CollectionExtensions
    {

        public static ImmutableList<T> ToImmutable<T>(this List<T> list)
        {
            return new ImmutableList<T>(list);
        }

        public static ImmutableDictionary<TKey, TValue> ToImmutable<TKey, TValue>
            (this Dictionary<TKey, TValue> dictionary)
        {
            return new ImmutableDictionary<TKey, TValue>(dictionary);
        }

        public static int[] Copy(this int[] array)
        {
            if (array == null) return null;
            var newArray = new int[array.Length];
            System.Array.Copy(array, newArray, array.Length);
            return newArray;
        }

        public static float[] Copy(this float[] array)
        {
            if (array == null) return null;
            var newArray = new float[array.Length];
            System.Array.Copy(array, newArray, array.Length);
            return newArray;
        }

    }

}