using System;
using System.Collections;
using System.Collections.Generic;

namespace BlackBears.Utils.Collections
{
    public sealed class ImmutableList<T> : IList<T>
    {

        private List<T> list;

        public ImmutableList(List<T> list)
        {
            if (list == null) throw new NullReferenceException();
            this.list = list;
        }

        public int IndexOf(T item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            throw new System.NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new System.NotImplementedException();
        }

        public T this[int index]
        {
            get { return list[index]; }
            set { throw new System.NotImplementedException(); }
        }

        public void Add(T item)
        {
            throw new System.NotImplementedException();
        }

        public void Clear()
        {
            throw new System.NotImplementedException();
        }

        public bool Contains(T item)
        {
            return list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            throw new System.NotImplementedException();
        }

        public int Count { get { return list.Count; } }

        public bool IsReadOnly { get { return true; } }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)list).GetEnumerator();
        }

    }
}

