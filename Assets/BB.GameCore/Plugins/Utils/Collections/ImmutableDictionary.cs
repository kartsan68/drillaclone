using System;
using System.Collections;
using System.Collections.Generic;

namespace BlackBears.Utils.Collections
{
    public sealed class ImmutableDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {

        private Dictionary<TKey, TValue> dictionary;

        public ImmutableDictionary(Dictionary<TKey, TValue> dictionary)
        {
            if (dictionary == null) throw new NullReferenceException();
            this.dictionary = dictionary;
        }

        public void Add(TKey key, TValue value)
        {
            throw new System.NotImplementedException();
        }

        public bool ContainsKey(TKey key)
        {
            return dictionary.ContainsKey(key);
        }

        public bool Remove(TKey key)
        {
            throw new System.NotImplementedException();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        public TValue this[TKey index]
        {
            get { return dictionary[index]; }
            set { throw new System.NotImplementedException(); }
        }

        public ICollection<TKey> Keys { get { return dictionary.Keys; } }

        public ICollection<TValue> Values { get { return dictionary.Values; } }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new System.NotImplementedException();
        }

        public void Clear()
        {
            throw new System.NotImplementedException();
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            return ((IDictionary<TKey, TValue>)(dictionary)).Contains(item);
        }

        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            ((IDictionary<TKey, TValue>)(dictionary)).CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new System.NotImplementedException();
        }

        public int Count { get { return dictionary.Count; } }

        public bool IsReadOnly { get { return true; } }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)dictionary).GetEnumerator();
        }
    }
}

