using BlackBears.GameCore.Graphic;
using BlackBears.Utils.Scaling;
using UnityEngine;

namespace BlackBears.Utils
{
    [RequireComponent(typeof(SafeAreaCanvasScaler))]
    [ExecuteInEditMode]
    public class GameSimpleCanvasScalerKey : MonoBehaviour
    {

        private void Awake()
        {
            var scaler = GetComponent<SafeAreaCanvasScaler>();
            scaler.key = GameCache.key;
            #if UNITY_EDITOR
            if (!Application.isPlaying) return;
            #endif
            Destroy(this);            
        }

        #if UNITY_EDITOR

        private void Update()
        {
            if (!Application.isPlaying) Awake();
        }

        #endif

    }
}