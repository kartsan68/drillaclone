﻿using System.Collections.Generic;
using UnityEngine;

namespace BlackBears
{

	/// <summary>
	/// Прослойка между методом <c>Resources.UnloadUnusedAssets</c>
	/// и нами. Нужна для того, чтобы Unity не выгружала ресурсы, которые мы только скачали из сети
	/// и не успели даже их взять (по этой причине на Android могут быть краши).
	/// </summary>
    public static class ResourceUnloadManager
    {

		private static int unloadRequestCount = 0;
		private static HashSet<object> unloadBlockers = new HashSet<object>();

		public static void RequestUnload()
		{
			unloadRequestCount += 1;
			TryUnload();
		}

		public static void BlockUnload(object blocker)
		{
			if (blocker == null) return;
			if (unloadBlockers == null) unloadBlockers = new HashSet<object>();
			unloadBlockers.Add(blocker);
		}

		public static void UnblockUnload(object blocker)
		{
			if (unloadBlockers != null)
			{
				unloadBlockers.Remove(blocker);
			}
			TryUnload();
		}

		private static void TryUnload()
		{
			if (unloadRequestCount <= 0) return;
			if (unloadRequestCount >= 3) ClearBlockers();
			if (unloadBlockers != null && unloadBlockers.Count > 0) return;

			unloadRequestCount = 0;
			Resources.UnloadUnusedAssets();
		}

		private static void ClearBlockers()
		{
			if (unloadBlockers == null || unloadBlockers.Count == 0) return;
			//В строке ниже всё правильно, это хак для вычищения от MonoBehaviour классов,
			//которые Unity уничтожила.
			while(unloadBlockers.Remove(null)); 
		}

    }

}