﻿namespace BlackBears.Utils.Components
{

    public class SafeAreaCoreKey : SafeAreaKey { protected override string Key => Keys.safeAreaCoreDataKey; }

}