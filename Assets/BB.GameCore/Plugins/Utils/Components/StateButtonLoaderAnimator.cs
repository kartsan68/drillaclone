﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Utils.Components
{

    internal enum OnStoppedBehaviour
    {
        Hide,
        ShowIndicator,
        Reset
    }

    internal class StateButtonLoaderAnimator : MonoBehaviour
    {

        [SerializeField] private CanvasGroup content;
        [SerializeField] private Button button;

        [SerializeField] private Image loadingIndicator;
        [SerializeField] private Image successIndicator;
        [SerializeField] private Image failIndicator;

        private bool loadingStopping;
        private bool disposed;
        private bool rotationStarted;

        internal bool LoadingStarted { get; private set; }

        private void Start() { ResetState(); }
        private void OnDisable() { ResetState(); }

        private void OnDestroy()
        {
            ResetState();
            disposed = true;
        }

        public void StartLoadingAnimation(Block onStarted = null)
        {
            if (LoadingStarted) return;
            LoadingStarted = true;

            AnimateStartLoading(onStarted);
        }

        public void StopLoadingAnimation(float pauseTime = 0, bool success = true,
            OnStoppedBehaviour behaviour = OnStoppedBehaviour.Hide, Block onStopped = null)
        {
            if (!LoadingStarted) return;
            if (loadingStopping) return;
            loadingStopping = true;

            AnimateStopLoading(pauseTime, success, behaviour, onStopped: () =>
            {
                LoadingStarted = false;
                loadingStopping = false;

                if (onStopped != null) onStopped();
            });
        }

        private void ResetState()
        {
            LeanTween.cancel(gameObject);
            LeanTween.cancel(content.gameObject);
            LeanTween.cancel(button.gameObject);
            LeanTween.cancel(loadingIndicator.gameObject);
            LeanTween.cancel(successIndicator.gameObject);
            if (failIndicator != null) LeanTween.cancel(failIndicator.gameObject);

            SetAlpha(content, 1f);
            SetAlpha(button, 1f);
            SetAlpha(loadingIndicator, 0f);
            SetAlpha(successIndicator, 0f);
            if (failIndicator != null) SetAlpha(failIndicator, 0f);

            LoadingStarted = false;
            loadingStopping = false;
            rotationStarted = false;
        }

        private void AnimateStartLoading(Block onStarted)
        {
            if (disposed) return;

            LeanTween.alphaCanvas(content, 0f, 0.6f).setOnComplete(() =>
            {
                if (!rotationStarted)
                {
                    rotationStarted = true;
                    AnimateLoadingIndicator();
                }

                LeanTween.delayedCall(gameObject, 0.15f, () =>
                {
                    LeanTween.alphaGraphic(loadingIndicator, 1f, 0.6f).setEaseOutQuad()
                        .setOnComplete(() => onStarted.SafeInvoke());
                });
            });
        }

        private void AnimateLoadingIndicator()
        {
            if (disposed) return;

            var currRotationZ = loadingIndicator.rectTransform.localRotation.eulerAngles.z;
            var easing = LeanTweenType.linear;
            if (currRotationZ == 0f) easing = LeanTweenType.easeInQuad;
            if (currRotationZ == 90f) easing = LeanTweenType.easeOutQuad;

            LeanTween.rotateZ(loadingIndicator.gameObject, currRotationZ - 90f, 0.15f)
                .setOnComplete(AnimateLoadingIndicator).setEase(easing);
        }

        private void AnimateStopLoading(float pauseTime, bool success, OnStoppedBehaviour behaviour, Block onStopped)
        {
            if (disposed) return;

            LeanTween.delayedCall(gameObject, pauseTime, () =>
            {
                LeanTween.alphaGraphic(loadingIndicator, 0f, 0.3f).setEaseOutQuad().setOnComplete(() =>
                {
                    LeanTween.cancel(successIndicator.gameObject);
                    if (failIndicator != null) LeanTween.cancel(failIndicator.gameObject);

                    if (success) AnimateOnSuccess(behaviour, onStopped);
                    else AnimateOnFail(behaviour, onStopped);
                });
            });
        }

        private void AnimateOnSuccess(OnStoppedBehaviour behaviour, Block onStopped)
        {
            if (disposed) return;

            LeanTween.alphaGraphic(successIndicator, 1f, 0.3f).setEaseInQuad().setOnComplete(() =>
            {
                LeanTween.delayedCall(gameObject, 0.3f, () => AnimateOnStopBehaviour(behaviour, onStopped));
            });
        }

        private void AnimateOnFail(OnStoppedBehaviour behaviour, Block onStopped)
        {
            if (disposed) return;

            if (failIndicator != null)
            {
                LeanTween.alphaGraphic(failIndicator, 1f, 0.3f).setEaseInQuad().setOnComplete(() =>
                {
                    LeanTween.delayedCall(gameObject, 0.3f, () => AnimateOnStopBehaviour(behaviour, onStopped));
                });
            }
            else
            {
                LeanTween.alphaGraphic(loadingIndicator, 0f, 0.3f).setEaseOutQuad().setOnComplete(() =>
                {
                    LeanTween.alphaCanvas(content, 1f, 0.3f);
                    LeanTween.delayedCall(gameObject, 0.15f, () => onStopped.SafeInvoke());
                });
            }
        }

        private void AnimateOnStopBehaviour(OnStoppedBehaviour behaviour, Block onStopped)
        {
            switch (behaviour)
            {
                case OnStoppedBehaviour.Hide:
                    AnimateHideButton(onStopped);
                    break;
                case OnStoppedBehaviour.Reset:
                    AnimateReset(onStopped);
                    break;
                case OnStoppedBehaviour.ShowIndicator:
                default:
                    onStopped.SafeInvoke();
                    break;
            }
        }

        private void AnimateHideButton(Block onHidden)
        {
            if (disposed) return;

            LeanTween.alphaGraphic(loadingIndicator, 0f, 0.3f).setEaseOutQuad();
            LeanTween.alphaGraphic(successIndicator, 0f, 0.3f).setEaseOutQuad();
            if (failIndicator != null) LeanTween.alphaGraphic(failIndicator, 0f, 0.3f).setEaseOutQuad();
            LeanTween.alphaGraphic(button.targetGraphic, 0f, 0.3f)
                .setEaseOutQuad()
                .setOnComplete(() => onHidden.SafeInvoke());
        }

        private void AnimateReset(Block onReset)
        {
            if (disposed) return;

            LeanTween.alphaGraphic(loadingIndicator, 0f, 0.3f).setEaseOutQuad();
            LeanTween.alphaGraphic(successIndicator, 0f, 0.3f).setEaseOutQuad();
            if (failIndicator != null) LeanTween.alphaGraphic(failIndicator, 0f, 0.3f).setEaseOutQuad();
            LeanTween.alphaCanvas(content, 1f, 0.3f).setEaseInQuad();
            LeanTween.alphaGraphic(button.targetGraphic, 1f, 0.3f)
                .setEaseInQuad()
                .setOnComplete(() => onReset.SafeInvoke());
        }

        #region Helpers

        private void SetAlpha(Image image, float alpha)
        {
            var loadingColor = image.color;
            loadingColor.a = alpha;
            image.color = loadingColor;
        }

        private void SetAlpha(CanvasGroup group, float alpha)
        {
            group.alpha = alpha;
        }

        private void SetAlpha(Button button, float alpha)
        {
            var buttonColor = button.targetGraphic.color;
            buttonColor.a = alpha;
            button.targetGraphic.color = buttonColor;
        }

        #endregion

    }

}


