using BlackBears.Utils.Components;

namespace BlackBears.Utils.Components
{
    public class SafeAreaGameKey : SafeAreaKey { protected override string Key => Keys.safeAreaGameDataKey; }
}