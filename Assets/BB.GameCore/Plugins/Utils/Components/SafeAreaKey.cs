using System.Collections;
using System.Collections.Generic;
using BlackBears.Utils.View.SafeArea;
using UnityEngine;

namespace BlackBears.Utils.Components
{

    [ExecuteInEditMode]
    public abstract class SafeAreaKey : MonoBehaviour
    {

        protected abstract string Key { get; }

        private void Awake()
        {
            bool installed = TryInstallKeyForElement();
            if (!installed) TryInstallKeyForData();
            #if UNITY_EDITOR
            if (!Application.isPlaying) return;
            #endif
			Destroy(this);
        }

        private bool TryInstallKeyForElement()
        {
            var elements = GetComponents<SafeAreaElement>();
            if (elements == null || elements.Length == 0) return false;
            foreach (var element in elements)
            {
                element.dataKey = Key;
            }
            return true;
        }

        private bool TryInstallKeyForData()
        {
            var data = GetComponent<SafeAreaData>();
            if (data == null) return false;
            data.Key = Key;
            return true;
        }

#if UNITY_EDITOR
        private void Update()
        {
            Awake();
        }
#endif

    }

}