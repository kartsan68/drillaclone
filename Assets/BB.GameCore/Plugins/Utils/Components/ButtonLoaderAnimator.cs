﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Utils.Components
{

    public class ButtonLoaderAnimator : MonoBehaviour
    {

        [SerializeField] private CanvasGroup content;
        [SerializeField] private Button button;

        [SerializeField] private Image loadingIndicator;
        [SerializeField] private Image loadedIndicator;

        private bool loadingStopping;
        private bool disposed;

        internal bool LoadingStarted { get; private set; }

        private void Start() { ResetValues(); }
        private void OnDisable() { ResetValues(); }

        private void OnDestroy()
        {
            ResetValues();
            disposed = true;
        }

        public void StartLoadingAnimation(Block showedCallback = null)
        {
            if (LoadingStarted) return;
            LoadingStarted = true;

            AnimateStartLoading(showedCallback);
        }

        public void StopLoadingAnimation(float pauseTime = 0f, bool success = true, bool hideButtonOnSuccess = true,
            Block onStopped = null)
        {
            if (!LoadingStarted) return;
            if (loadingStopping) return;
            loadingStopping = true;

            AnimateStopLoading(pauseTime, success, hideButtonOnSuccess, () =>
            {
                LoadingStarted = false;
                loadingStopping = false;

                if (onStopped != null) onStopped();
            });
        }

        private void ResetValues()
        {
            LeanTween.cancel(button.gameObject);
            LeanTween.cancel(loadedIndicator.gameObject);
            LeanTween.cancel(loadingIndicator.gameObject);
            LeanTween.cancel(content.gameObject);

            var loadingColor = loadingIndicator.color;
            loadingColor.a = 0f;
            loadingIndicator.color = loadingColor;

            var loadedColor = loadedIndicator.color;
            loadedColor.a = 0f;
            loadedIndicator.color = loadedColor;

            content.alpha = 1f;
            var buttonColor = button.targetGraphic.color;
            buttonColor.a = 1f;
            button.targetGraphic.color = buttonColor;

            LoadingStarted = false;
            loadingStopping = false;
        }

        private void AnimateStartLoading(Block showedCallback)
        {
            if (disposed) return;

            StartCoroutine(StartLoadingRoutine(showedCallback));
        }

        private IEnumerator StartLoadingRoutine(Block showedCallback)
        {
            LeanTween.alphaCanvas(content, 0f, 0.6f);
            yield return new WaitForSeconds(0.6f);
            AnimateLoadingIndicator();
            yield return new WaitForSeconds(0.15f);
            LeanTween.alphaGraphic(loadingIndicator, 1f, 0.6f).setEaseOutQuad()
                .setOnComplete(() => showedCallback.SafeInvoke());
        }

        private void AnimateLoadingIndicator()
        {
            if (disposed) return;

            var currRotationZ = loadingIndicator.rectTransform.localRotation.eulerAngles.z;
            var easing = LeanTweenType.linear;
            if (currRotationZ == 0f) easing = LeanTweenType.easeInQuad;
            if (currRotationZ == 90f) easing = LeanTweenType.easeOutQuad;

            LeanTween.rotateZ(loadingIndicator.gameObject, currRotationZ - 90f, 0.15f)
                     .setOnComplete(AnimateLoadingIndicator).setEase(easing);
        }

        private void AnimateStopLoading(float pauseTime, bool success, bool hideButtonOnSuccess, Block onStopped)
        {
            if (disposed) return;

            StartCoroutine(StopLoadingRoutine(pauseTime, success, hideButtonOnSuccess, onStopped));
        }

        private IEnumerator StopLoadingRoutine(float pauseTime, bool success, bool hideButtonOnSuccess, Block onStopped)
        {
            if (pauseTime > 0) yield return new WaitForSeconds(pauseTime);
            LeanTween.alphaGraphic(loadingIndicator, 0f, 0.3f).setEaseOutQuad();
            yield return new WaitForSeconds(0.3f);
            LeanTween.cancel(loadedIndicator.gameObject);
            if (success)
            {
                LeanTween.alphaGraphic(loadedIndicator, 1f, 0.3f).setEaseInQuad();
                yield return new WaitForSeconds(0.6f);
                if (hideButtonOnSuccess) AnimateHideButton(onStopped);
                else onStopped.SafeInvoke();
            }
            else
            {
                LeanTween.alphaGraphic(loadingIndicator, 0f, 0.3f).setEaseOutQuad();
                yield return new WaitForSeconds(0.3f);
                LeanTween.alphaCanvas(content, 1f, 0.3f);
                yield return new WaitForSeconds(0.15f);
                onStopped.SafeInvoke();
            }
        }

        private void AnimateHideButton(Block onHidded)
        {
            LeanTween.alphaGraphic(loadedIndicator, 0f, 0.3f).setEaseOutQuad();
            LeanTween.alphaGraphic(button.targetGraphic, 0f, 0.3f)
                     .setEaseOutQuad()
                     .setOnComplete(() => onHidded.SafeInvoke());
        }

    }

}