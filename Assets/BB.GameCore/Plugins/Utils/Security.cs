﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

using URandom = UnityEngine.Random;

namespace BlackBears.Utils
{

    public static class Security
    {

        public enum MD5Format
        {
            standart,
            upper,
            lower
        }

        public const int md5Length = 32;

        private static readonly string[] keyAlphabet =
        {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "a", "b", "c", "d", "e", "f"
        };

        public static bool EqualHashes(string firstHash, string secondHash)
        {
            return string.Equals(firstHash, secondHash);
        }

        public static string GetMD5Hash(string randKey, string secret, string fileName)
        {
            using (MD5 md5 = MD5.Create())
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    byte[] hash = md5.ComputeHash(fs);
                    string hashString = string.Format("{0}{1}{2}", randKey, secret,
                        ToReadable(hash, MD5Format.lower));

                    byte[] sqbaHash = md5.ComputeHash(Encoding.UTF8.GetBytes(hashString));
                    return string.Format("{0}-{1}", randKey, ToReadable(sqbaHash, MD5Format.lower));
                }
            }
        }

        public static string ToBase64(string input)
        {
            var bytes = Encoding.UTF8.GetBytes(input);
            return System.Convert.ToBase64String(bytes);
        }

        public static string FromBase64(string input)
        {
            var bytes = Convert.FromBase64String(input);
            return Encoding.UTF8.GetString(bytes);
        }

        public static string GetMD5Hash(string input, MD5Format format = MD5Format.standart)
        {
            using (var md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
                return ToReadable(hash, format);
            }
        }

        public static string GetRandKey()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < 6; ++i)
            {
                sb.Append(keyAlphabet[URandom.Range(0, keyAlphabet.Length)]);
            }
            return sb.ToString();
        }

        public static string ToReadable(byte[] hash, MD5Format format)
        {
            string md5 = BitConverter.ToString(hash).Replace("-", "");
            switch (format)
            {
                case MD5Format.lower: return md5.ToLowerInvariant();
                case MD5Format.upper: return md5.ToUpperInvariant();
            }
            return md5;
        }
    }
}
