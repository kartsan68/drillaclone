﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

namespace BlackBears.Utils
{
    public static partial class NativeUtils
    {

        public static void ShareUrl(string url, string textShare, string caption, string image)
        {
            InvokeShareUrl(url, textShare, caption, image);
        }

        public static void OpenStore(string storeId)
        {
            InvokeOpenStore(storeId);
        }

        public static string GetOSVersion()
        {
            return InvokeGetOSVersion();
        }

        public static void OpenImage()
        {

        }

        public static void SendEmail(string mail, string subject, string text)
        {
            InvokeSendEmail(mail, subject, text);
        }

        public static bool IsApplicationInstalled(string urlscheme)
        {
            return InvokeIsApplicationInstalled(urlscheme);
        }

        public static void OpenApplication(string urlscheme, string link)
        {
            InvokeOpenApplication(urlscheme, link);
        }

        public static void OpenUrl(string urlscheme, string link)
        {
            InvokeOpenUrl(urlscheme, link);
        }

        public static void OpenApplicationStore(string link)
        {
            InvokeOpenApplicationStore(link);
        }

        public static void OpenRate(RateData rateData)
        {
            InvokeOpenRate(rateData);
        }
        
        public static void OpenNotificationSettings()
        {
            InvokeOpenNotificationSettings();
        }

    }
}