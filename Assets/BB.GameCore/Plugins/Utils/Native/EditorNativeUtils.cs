#if UNITY_EDITOR

using System;

namespace BlackBears.Utils
{

    public struct RateData {}

    public static partial class NativeUtils
    {

        private static void InvokeShareUrl(string url, string textShare, string caption, string image)
        {
            UnityEngine.Debug.LogFormat("Share message: \"{0}\"; Url: {1}; Caption: \"{2}\"; Image: \"{3}\"",
                textShare, url, caption, image);
        }

        private static void InvokeOpenStore(string storeId)
        {
            UnityEngine.Debug.LogFormat("Open Store with Id: {0}", storeId);
        }

        private static string InvokeGetOSVersion()
        {
            return UnityEngine.SystemInfo.operatingSystem;
        }

        private static void InvokeSendEmail(string mail, string subject, string text)
        {
            UnityEngine.Debug.LogFormat("mail: {0}; subject: {1}; text: {2}.", mail, subject, text);
        }

        private static bool InvokeIsApplicationInstalled(string urlscheme)
        {
            return false;
        }

        private static void InvokeOpenApplication(string urlscheme, string link)
        {
            UnityEngine.Debug.LogFormat("Open application with scheme {0} and link {1}", urlscheme, link);
        }

        private static void InvokeOpenUrl(string urlscheme, string link)
        {
            UnityEngine.Debug.LogFormat("Open url with scheme {0} and link {1}", urlscheme, link);
        }

        private static void InvokeOpenApplicationStore(string link)
        {
            UnityEngine.Debug.LogFormat("Open application store with link {0}", link);
        }

        private static void InvokeOpenRate(RateData rateData)
        {
            UnityEngine.Debug.Log("Open Application Rate");
        }

        private static void InvokeOpenNotificationSettings()
        {
            UnityEngine.Debug.Log("Open notification settings");
        }
    }
}

#endif