﻿#if !UNITY_EDITOR && UNITY_IOS

using System.Runtime.InteropServices;

namespace BlackBears.Utils
{

    public struct RateData
    {
		public readonly string linkFormat;
        public readonly string appId;

        public RateData(string linkFormat, string appId)
        {
            this.linkFormat = linkFormat;
			this.appId = appId;
        }
    }

    public static partial class NativeUtils
    {

 		private struct SocialSharingStruct
  		{
   			public string text;
   			public string url;
   			public string image;
  		}

		[DllImport ("__Internal")] private static extern void BBShowSocialSharing(ref SocialSharingStruct conf);
        private static void InvokeShareUrl(string url, string text, string caption, string image)
		{
			SocialSharingStruct conf = new SocialSharingStruct();
			conf.text = text;
			conf.url = url;
			conf.image = image;
			BBShowSocialSharing(ref conf);
		}

        [DllImport ("__Internal")] private static extern void BBOpenStore(string identifier);
		private static void InvokeOpenStore(string storeId)
        {
			BBOpenStore(storeId);
        }

		[DllImport ("__Internal")] private static extern string BBGetOsVersion();
        private static string InvokeGetOSVersion()
        {
			return BBGetOsVersion();
        }

		[DllImport ("__Internal")] private static extern void BBSendEmail(string mail, string subject, string text);
        private static void InvokeSendEmail(string mail, string subject, string text)
        {
			BBSendEmail(mail, subject, text);
        }

		[DllImport ("__Internal")] private static extern bool BBIsAppInstalled(string urlscheme);
		private static bool InvokeIsApplicationInstalled(string urlscheme)
		{
			return BBIsAppInstalled(urlscheme);
		}

		[DllImport ("__Internal")] private static extern void BBOpenApplication(string urlscheme, string link);
        private static void InvokeOpenApplication(string urlscheme, string link)
        {
			BBOpenApplication(urlscheme, link);
        }
		
        private static void InvokeOpenUrl(string urlscheme, string link)
        {
			BBOpenApplication(urlscheme, link);
        }

		[DllImport ("__Internal")] private static extern void BBOpenApplicationStore(string link);
		private static void InvokeOpenApplicationStore(string link)
		{
			BBOpenApplicationStore(link);
		}

		[DllImport ("__Internal")] private static extern void BBOpenRate(ref RateData rateData);
		private static void InvokeOpenRate(RateData rateData)
        {
			BBOpenRate(ref rateData);
        }

		[DllImport ("__Internal")] private static extern void BBOpenNotificationsSettings();
        private static void InvokeOpenNotificationSettings()
		{
			BBOpenNotificationsSettings();
		}

    }
}

#endif
