#if !UNITY_EDITOR && UNITY_ANDROID

using UnityEngine;

using JO = UnityEngine.AndroidJavaObject;
using JC = UnityEngine.AndroidJavaClass;
using JE = UnityEngine.AndroidJavaException;

namespace BlackBears.Utils
{

    public struct RateData
    {
        public readonly string appId;

        public RateData(string appId)
        {
            this.appId = appId;
        }
    }

    public static partial class NativeUtils
    {

        private static void InvokeShareUrl(string url, string textShare, string caption, string image)
        {
            JC intentClass = new JC("android.content.Intent");
            JO intentObject = new JO("android.content.Intent");

            intentObject.Call<JO>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            intentObject.Call<JO>("setType", "text/plain");
            intentObject.Call<JO>("putExtra",
                intentClass.GetStatic<string>("EXTRA_TEXT"), string.Format("{0} {1}", textShare, url));

            JC unity = new JC("com.unity3d.player.UnityPlayer");
            JO currentActivity = unity.GetStatic<JO>("currentActivity");

            currentActivity.Call("startActivity", intentClass.CallStatic<JO>("createChooser", intentObject, caption));

            currentActivity.Dispose();
            intentObject.Dispose();
            unity.Dispose();
            intentClass.Dispose();
        }

        private static void InvokeOpenStore(string storeId)
        {
            JC unity = new JC("com.unity3d.player.UnityPlayer");
            JC intentC = new JC("android.content.Intent");
            JC uriC = new JC("android.net.Uri");
            JO currentActivity = unity.GetStatic<JO>("currentActivity");
            JO intentO;

            intentO = new JO("android.content.Intent",
                intentC.GetStatic<string>("ACTION_VIEW"),
                uriC.CallStatic<JO>("parse",
                    string.Format("http://play.google.com/store/apps/dev?id={0}", storeId)));
            currentActivity.Call("startActivity", intentO);

            unity.Dispose();
            intentC.Dispose();
            uriC.Dispose();
            currentActivity.Dispose();
            intentO.Dispose();
        }

        private static string InvokeGetOSVersion()
        {
            using (JC buildVersion = new JC("android.os.Build$VERSION"))
            {
                return buildVersion.GetStatic<string>("RELEASE");
            }
        }

        private static void InvokeSendEmail(string mail, string subject, string text)
        {
            JC unity = new JC("com.unity3d.player.UnityPlayer");
            JO currentActivity = unity.GetStatic<JO>("currentActivity");
            JC intentC = new JC("android.content.Intent");

            var emailIntent = CreateEmailIntent(mail, subject, text);
            currentActivity.Call("startActivity", intentC.CallStatic<JO>("createChooser",
                emailIntent, null));

            unity.Dispose();
            currentActivity.Dispose();
            intentC.Dispose();
            emailIntent.Dispose();
        }

        private static JO CreateEmailIntent(string mail, string subject, string text)
        {
            var intentC = new JC("android.content.Intent");
            JC uriC = new JC("android.net.Uri");

            var emailIntent = new JO("android.content.Intent",
                intentC.GetStatic<string>("ACTION_SENDTO"));
            var uriO = uriC.CallStatic<JO>("parse", string.Format("mailto:{0}", mail));
            emailIntent.Call<JO>("setData", uriO);
            emailIntent.Call<JO>("putExtra", intentC.GetStatic<string>("EXTRA_SUBJECT"), subject);

            if (text != null) emailIntent.Call<JO>("putExtra", intentC.GetStatic<string>("EXTRA_TEXT"), text);

            uriO.Dispose();
            intentC.Dispose();

            return emailIntent;
        }

        private static bool InvokeIsApplicationInstalled(string urlScheme)
        {
            if (urlScheme == null) return false;
            JC unity = new JC("com.unity3d.player.UnityPlayer");
            JO activity = unity.GetStatic<JO>("currentActivity");

            bool installed = false;

            using (var intent = new JO("android.content.Intent", "android.intent.action.VIEW"))
            {
                var uricls = new JC("android.net.Uri");
                intent.Call<JO>("setData", uricls.CallStatic<JO>("parse", urlScheme));
                uricls.Dispose();
                var resInfo = activity.Call<JO>("getPackageManager")
                    .Call<JO>("queryIntentActivities", intent, 0);
                installed = !resInfo.Call<bool>("isEmpty");
            }

            unity.Dispose();
            activity.Dispose();
            return installed;
        }

        private static void InvokeOpenApplication(string urlscheme, string link)
        {
            if (!TryOpenByUrlScheme(urlscheme)) OpenApplicationWithAppId(link);
        }

        private static void InvokeOpenUrl(string urlScheme, string link)
        {
            TryOpenByUrl(urlScheme, link);
        }

        private static void InvokeOpenApplicationStore(string link)
        {
            OpenApplicationWithAppId(link);
        }

        private static void InvokeOpenRate(RateData rateData)
        {
            OpenApplicationWithAppId(rateData.appId);
        }

        private static void InvokeOpenNotificationSettings() {}

        private static bool TryOpenByUrlScheme(string urlScheme)
        {
            if (urlScheme == null) return false;

            JC unity = null;
            JO activity = null;
            JO intent = null;

            try
            {
                unity = new JC("com.unity3d.player.UnityPlayer");
                activity = unity.GetStatic<JO>("currentActivity");
                intent = new JO("android.content.Intent", "android.intent.action.VIEW");

                var uricls = new JC("android.net.Uri");
                intent.Call<JO>("setData", uricls.CallStatic<JO>("parse", urlScheme));
                uricls.Dispose();
                var resInfo = activity.Call<JO>("getPackageManager")
                    .Call<JO>("queryIntentActivities", intent, 0);
                if (resInfo.Call<bool>("isEmpty")) return false;

                var activityInfo = resInfo.Call<JO>("get", 0).Get<JO>("activityInfo");
                var name = new JO("android.content.ComponentName",
                    activityInfo.Get<JO>("applicationInfo").Get<string>("packageName"),
                    activityInfo.Get<JO>("name"));

                intent.Dispose();
                intent = new JO("android.content.Intent", "android.intent.action.MAIN");

                intent.Call<JO>("addCategory", "android.intent.category.LAUNCHER");

                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                intent.Call<JO>("setFlags", 268435456 | 2097152);
                intent.Call<JO>("setComponent", name);
                activity.Call("startActivity", intent);
                return true;
            }
            finally
            {
                if (intent != null) intent.Dispose();
                if (unity != null) unity.Dispose();
                if (activity != null) activity.Dispose();
            }
        }

        private static bool TryOpenByUrl(string urlScheme, string link)
        {
            if (urlScheme == null) return false;

            JC unity = null;
            JO activity = null;
            JO intent = null;
            unity = new JC("com.unity3d.player.UnityPlayer");
                activity = unity.GetStatic<JO>("currentActivity");
                var uricls = new JC("android.net.Uri");
            try
            {
                intent = new JO("android.content.Intent", "android.intent.action.VIEW", uricls.CallStatic<JO>("parse", urlScheme));
                activity.Call("startActivity", intent);
                return true;
            }
            catch (JE e)
            {
                intent = new JO("android.content.Intent", "android.intent.action.VIEW", uricls.CallStatic<JO>("parse", link));
                activity.Call("startActivity", intent);
                return true;
            }
            finally
            {
                if (intent != null) intent.Dispose();
                if (unity != null) unity.Dispose();
                if (activity != null) activity.Dispose();
            }
        }


        private static void OpenApplicationWithAppId(string appId)
        {
            JC unity = new JC("com.unity3d.player.UnityPlayer");
            JO activity = unity.GetStatic<JO>("currentActivity");
            JC uriC = new JC("android.net.Uri");
            JO intent = null;
            try
            {
                intent = new JO("android.content.Intent", "android.intent.action.VIEW", 
                    uriC.CallStatic<JO>("parse", string.Format("market://details?id={0}", appId)));
                activity.Call("startActivity", intent);
            }
            catch (JE e)
            {
                intent = new JO("android.content.Intent", "android.intent.action.VIEW", 
                    uriC.CallStatic<JO>("parse", 
                        string.Format("http://play.google.com/store/apps/details?id=", appId)));
                activity.Call("startActivity", intent);
            }
            finally
            {
                unity.Dispose();
                activity.Dispose();
                uriC.Dispose();
                if (intent != null) intent.Dispose();
            }
        }
    }
}

#endif