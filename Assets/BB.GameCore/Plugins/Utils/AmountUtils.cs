﻿using System.Globalization;

using Math = System.Math;

namespace BlackBears.Utils
{

    public static class AmountUtils
    {

        private const string baseMultipliers = "|k|M|G|T|P|E|Z|Y|kY|MY|GY|TY|PY|EY|ZY|YY|kYY|MYY|GYY|TYY|PYY|EYY|ZYY|YYY";
        private const string defaultFormat = "{0:0.##}{1}";

        private static string[] multipliers;
        private static string[] defaultMultipliers;

        public static void SetupMultipliersFromString(string multipliersStr)
        {
            if(!string.IsNullOrEmpty(multipliersStr)) multipliers = multipliersStr.Split('|');
        }

        public static string ToPrettyString(this float amount, bool truncate = false, string format = defaultFormat,
            bool useDefaultMultipliers = false)
        {
            return ((double)amount).ToPrettyString(truncate, format, useDefaultMultipliers);
        }

        public static string ToPrettyString(this double amount, bool truncate = false, string format = defaultFormat,
            bool useDefaultMultipliers = false)
        {

            var currentMultipliers = GetMultipliers(useDefaultMultipliers);

            if (amount < 1000) return amount.ToString("0.");

            int sign = Math.Sign(amount);
            amount = Math.Abs(amount);
            if (truncate) amount = Math.Truncate(amount);

            int multiplier = 0;
            while (amount >= 1000 && (multiplier < currentMultipliers.Length - 1))
            {
                amount *= 0.001;
                multiplier += 1;
            }

            amount *= sign;
            return string.Format(CultureInfo.InvariantCulture, format, amount, currentMultipliers[multiplier]);
        }

        private static string[] GetMultipliers(bool useDefaultMultipliers)
        {
            if (useDefaultMultipliers)
            {
                if (defaultMultipliers == null) defaultMultipliers = baseMultipliers.Split('|');
                return defaultMultipliers;
            }

            if (multipliers == null) return GetMultipliers(true);

            return multipliers;
        }

    }

}