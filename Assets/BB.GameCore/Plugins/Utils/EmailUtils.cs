using UnityEngine;

namespace BlackBears.Utils
{
    
    public static class EmailUtils
    {
        
        public static string GenerateSupportEmailMessage(string playerId,string configVersion = "")
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(string.Format("PlayerID: {0}", playerId));
            sb.AppendLine(string.Format("GameVersion: {0}", Application.version));
            sb.AppendLine(string.Format("OS: {0}", SystemInfo.operatingSystem));
            sb.AppendLine(string.Format("Device: {0}", SystemInfo.deviceModel));
            sb.AppendLine(string.Format("Config version: {0}", configVersion));
            sb.AppendLine();
            sb.AppendLine();
            return sb.ToString();
        }


        public static void SendMailByUrl(string playerId,string configVersion,string mail,string subject)
        {
            string prefix = "mailto:" + mail + "?subject=" + SafeUrl(subject)+"&body=";
            var message = prefix + SafeUrl(GenerateSupportEmailMessage(playerId,configVersion));
            Debug.Log(message);
            Application.OpenURL(message);
        }
        
        private static string SafeUrl(string url)
        {
            return url.Replace(" ", "%20").Replace("\n","%0D%0A");
        }

    }

}