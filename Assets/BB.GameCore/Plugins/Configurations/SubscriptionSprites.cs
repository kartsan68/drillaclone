using UnityEngine;

namespace BlackBears.GameCore.Configurations
{

    [System.Serializable]
    public class SubscriptionSpritesContainer
    {

        [SerializeField] private SubscriptionSpritePack[] sprites;

        public SubscriptionSpritePack this[string subscriptionId] 
        { 
            get { return GetPackBySubscriptionId(subscriptionId); } 
        }

        public SubscriptionSpritePack GetPackBySubscriptionId(string subscriptionId)
        {
            if (sprites == null) return null;
            for(int i = 0; i < sprites.Length; i++)
            {
                if (sprites[i] == null || !string.Equals(subscriptionId, sprites[i].SubscriptionId)) continue;
                return sprites[i];
            }
            return null;
        }

    }

    [System.Serializable]
    public class SubscriptionSpritePack
    {

        [SerializeField] private string subscriptionId;

        [SerializeField] private Sprite simpleSprite;
        [SerializeField] private Sprite outlinedSprite;

        public string SubscriptionId => subscriptionId;
        public Sprite SimpleSprite => simpleSprite;
        public Sprite OutlinedSprite => outlinedSprite;

    }

}