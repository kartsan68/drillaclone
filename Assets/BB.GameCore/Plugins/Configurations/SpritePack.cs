using UnityEngine;

namespace BlackBears.GameCore.Configurations
{

    [System.Serializable]
    public class SpritePack
    {
        [SerializeField] private Sprite bigIcon;
        [SerializeField] private Sprite smallIcon;

        public Sprite BigIcon { get { return bigIcon; } }
        public Sprite SmallIcon { get { return smallIcon; } }

    }

}