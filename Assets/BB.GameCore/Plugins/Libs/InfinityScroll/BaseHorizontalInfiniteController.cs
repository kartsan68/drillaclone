﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.UI.Additional
{

    [RequireComponent(typeof(ScrollRect))]
    public abstract class BaseHorizontalInfiniteController : BaseInfiniteController
    {

        protected sealed override int VectorMainIndex { get { return 0; } }
        protected sealed override int VectorSecondaryIndex { get { return 1; } }

        protected sealed override bool InNormalDirection { get { return true; } }

        protected override void InternalInitiate()
        {
            Scroll.vertical = false;
            Scroll.horizontal = true;
        }

        protected sealed override float ActualSize(Rect rect)
        {
            return rect.width;
        }

        protected sealed override void LayoutItemGroup(LayoutGroup group)
        {
            group.CalculateLayoutInputHorizontal();
            group.SetLayoutHorizontal();
        }

    }

}