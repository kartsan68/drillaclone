﻿using System;

namespace UnityEngine.UI.Additional
{
    
    public abstract class InfiniteAdapter
    {

        public BaseInfiniteController Controller { protected get; set; }

        public virtual int GetTypeByIndex(int index) { return 0; }

        public virtual void BindItem(GameObject itemObject, int index, int type) {}

        public virtual void UnbindItem(GameObject itemObject, int index, int type) {}

        public abstract bool IsValidIndex(int index);

        public abstract GameObject InstantiatePrefab(GameObject prefab);

        public virtual void SetupParent(RectTransform item, RectTransform parent)
        {
            if (item == null || item.parent == parent) return;
            item.SetParent(parent);
        }

    }

}