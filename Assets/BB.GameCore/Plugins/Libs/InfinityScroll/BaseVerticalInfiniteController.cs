﻿using UnityEngine;

namespace UnityEngine.UI.Additional
{

    public abstract class BaseVerticalInfiniteController : BaseInfiniteController
    {

        protected sealed override int VectorMainIndex { get { return 1; } }
        protected sealed override int VectorSecondaryIndex { get { return 0; } }

        protected sealed override bool InNormalDirection { get { return false; } }
        protected sealed override float Sign { get { return -1f; } }

        protected override void InternalInitiate()
        {
            Scroll.vertical = true;
            Scroll.horizontal = false;
        }

        protected sealed override float ActualSize(Rect rect)
        {
            return rect.height;
        }

        protected sealed override void LayoutItemGroup(LayoutGroup group)
        {
            group.CalculateLayoutInputVertical();
            group.SetLayoutVertical();
        }

    }

}