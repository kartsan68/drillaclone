﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.UI.Additional
{

    [RequireComponent(typeof(ScrollRect))]
    public abstract class BaseInfiniteController : MonoBehaviour
    {

        public bool initOnStart = false;
        public float reactSizeValue = 2;

        public float startOverscroll = 0f;

        private float scrollPos;

        private LinkedList<RectTransform> items = new LinkedList<RectTransform>();
        private int minIndex = 0;
        private int maxIndex = 0;

        public bool Initiated { get; private set; }

        public ScrollRect Scroll { get; private set; }

        protected abstract int VectorMainIndex { get; }
        protected abstract int VectorSecondaryIndex { get; }

        protected abstract bool InNormalDirection { get; }
        protected abstract float Sign { get; }

        protected virtual void Awake()
        {
            Scroll = GetComponent<ScrollRect>();
            Scroll.onValueChanged.AddListener(OnScrollValueChanged);
        }

        protected virtual void Start()
        {
            if (initOnStart) Initiate();
        }

        public void Initiate(int index = 0)
        {
            if (Initiated) return;

            Scroll.movementType = ScrollRect.MovementType.Unrestricted;
            InternalInitiate();

            ToIndex(index, true);

            Initiated = true;
        }

        public void Invalidate() => OnScrollValueChanged(Scroll.normalizedPosition);

        public void ToIndex(int index, bool init = false)
        {
            if (!IsValidIndex(index)) return;
            if (!init)
            {
                UnloadItems();
                Scroll.StopMovement();
                var pos = Scroll.content.anchoredPosition;
                pos[VectorMainIndex] = 0f;
                Scroll.content.anchoredPosition = pos;
            }
            maxIndex = index - 1;
            minIndex = index;
            TryAddToEnd();
            TryAddToStart();
            maxIndex = maxIndex >= minIndex ? maxIndex : minIndex;

            scrollPos = 0f;
        }

        public void ForceReturnItem(RectTransform item, int index) => UnloadItem(item, index);

        public bool ForceAddNext() => TryAddToEnd(true);

        public void ToTop() => ToIndex(0);
        public virtual void Reset() => ToTop();

        protected abstract void InternalInitiate();
        protected abstract RectTransform GetItemByIndex(int index);
        protected abstract void UnloadItem(RectTransform item, int index);
        protected abstract void SetupParent(RectTransform item, RectTransform parent);
        protected abstract bool IsValidIndex(int index);
        protected abstract float ActualSize(Rect rect);
        protected abstract void LayoutItemGroup(LayoutGroup group);

        protected abstract void OnItemAddedToStart(RectTransform item);
        protected abstract void OnItemAddedToEnd(RectTransform item);

        private void OnScrollValueChanged(Vector2 value)
        {
            if (!Initiated) return;

            var newPos = Scroll.content.anchoredPosition[VectorMainIndex];

            if (!CanAddToEnd(true))
            {
                var node = items.Last;
                bool stopAtStart = false;
                if (node == null)
                {
                    stopAtStart = true;
                }
                else
                {
                    var item = node.Value;
                    var itemPos = item.anchoredPosition[VectorMainIndex] * Sign;
                    itemPos += ActualSize(item.rect);
                    stopAtStart = itemPos < ActualSize(Scroll.content.rect);
                }
                if (stopAtStart)
                {
                    Scroll.StopMovement();
                    var stopPos = Scroll.content.anchoredPosition;
                    stopPos[VectorMainIndex] = 0;
                    Scroll.content.anchoredPosition = stopPos;
                    return;
                }
            }

            if (InNormalDirection == scrollPos < newPos)
            {
                if (!TryAddToStart()) CheckForStoppingAtStart();
                TryRemoveAtEnd();
                if (!CanAddToEnd(false)) CheckForStoppingAtEnd();
            }
            else
            {
                if (!TryAddToEnd()) CheckForStoppingAtEnd();
                TryRemoveAtStart();
                if (!CanAddToStart()) CheckForStoppingAtStart();
            }
            scrollPos = newPos;
        }

        private void CheckForStoppingAtStart()
        {
            LinkedListNode<RectTransform> node;
            Vector2 stopPos = Scroll.content.anchoredPosition;
            stopPos[VectorMainIndex] = 0;

            if ((node = items.First) != null)
            {
                var item = node.Value;
                var offset = RelativePoint(item.anchoredPosition[VectorMainIndex]) - Sign * startOverscroll;

                if (Sign * offset > 0) stopPos[VectorMainIndex] = Scroll.content.anchoredPosition[VectorMainIndex] - offset;
                else return;
            }
            else
            {
                return;
            }

            Scroll.StopMovement();
            Scroll.content.anchoredPosition = stopPos;
        }

        private void CheckForStoppingAtEnd()
        {
            LinkedListNode<RectTransform> node;
            Vector2 stopPos = Scroll.content.anchoredPosition;
            stopPos[VectorMainIndex] = 0;

            var contentSize = ActualSize(Scroll.content.rect);

            if ((node = items.Last) != null)
            {
                var item = node.Value;

                var offset = RelativePoint(item.anchoredPosition[VectorMainIndex] + Sign * ActualSize(item.rect));
                offset -= Sign * contentSize;

                if (Sign * offset < 0) stopPos[VectorMainIndex] = Scroll.content.anchoredPosition[VectorMainIndex] - offset;
                else return;
            }
            else
            {
                return;
            }

            Scroll.StopMovement();
            Scroll.content.anchoredPosition = stopPos;
        }

        private bool TryAddToStart()
        {
            bool added = false;
            while (CanAddToStart())
            {
                minIndex -= 1;

                RectTransform item = GetItemByIndex(minIndex);
                BasicItemPrepare(item);

                Vector2 pos = Vector2.zero;
                pos[VectorMainIndex] = -ActualSize(item.rect);
                pos = RelativePoint(pos);

                LinkedListNode<RectTransform> firstItem;
                if ((firstItem = items.First) != null)
                {
                    pos[VectorMainIndex] = firstItem.Value.anchoredPosition[VectorMainIndex] - Sign * ActualSize(item.rect);
                    pos[VectorSecondaryIndex] = firstItem.Value.anchoredPosition[VectorSecondaryIndex];
                }

                FinalItemPrepare(item, pos);
                OnItemAddedToStart(item);
                items.AddFirst(item);
                added = true;
            }
            return added;
        }

        private bool TryAddToEnd(bool forceAdd = false)
        {
            bool added = false;
            while (CanAddToEnd(forceAdd))
            {
                forceAdd = false;
                maxIndex += 1;

                RectTransform item = GetItemByIndex(maxIndex);
                BasicItemPrepare(item);

                Vector2 pos = RelativePoint(Vector2.zero);
                LinkedListNode<RectTransform> lastItem;
                if ((lastItem = items.Last) != null)
                {
                    pos[VectorMainIndex] = lastItem.Value.anchoredPosition[VectorMainIndex]
                        + Sign * ActualSize(lastItem.Value.rect);
                    pos[VectorSecondaryIndex] = lastItem.Value.anchoredPosition[VectorSecondaryIndex];
                }

                FinalItemPrepare(item, pos);
                OnItemAddedToEnd(item);
                items.AddLast(item);
                added = true;

            }
            return added;
        }

        private void BasicItemPrepare(RectTransform item)
        {
            SetupParent(item, Scroll.content);
            item.localScale = Vector3.one;
            item.gameObject.SetActive(true);

            var group = item.GetComponent<LayoutGroup>();
            if (group != null)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(item);
                LayoutItemGroup(group);
            }
        }

        private void FinalItemPrepare(RectTransform item, Vector2 position)
        {
            item.anchoredPosition = position;
        }

        private void TryRemoveAtStart()
        {
            while (CanRemoveAtStart())
            {
                var item = items.First.Value;
                items.RemoveFirst();
                minIndex += 1;

                UnloadItem(item, minIndex - 1);
            }
        }

        private void TryRemoveAtEnd()
        {
            while (CanRemoveAtEnd())
            {
                var item = items.Last.Value;
                items.RemoveLast();
                maxIndex -= 1;

                UnloadItem(item, maxIndex + 1);
            }
        }

        private bool CanAddToStart()
        {
            if (!IsValidIndex(minIndex - 1)) return false;

            float pos = 0;
            var size = ActualSize(Scroll.viewport.rect);

            LinkedListNode<RectTransform> firstItem;
            if ((firstItem = items.First) != null)
            {
                pos = firstItem.Value.anchoredPosition[VectorMainIndex];
            }
            pos = RelativePoint(pos);
            return pos * Sign >= size - size * reactSizeValue;
        }

        private bool CanAddToEnd(bool forceAdd)
        {
            if (!IsValidIndex(maxIndex + 1)) return false;
            if (forceAdd) return true;

            float pos = 0;
            var size = ActualSize(Scroll.viewport.rect);

            LinkedListNode<RectTransform> lastItem;
            if ((lastItem = items.Last) != null)
            {
                pos = lastItem.Value.anchoredPosition[VectorMainIndex]
                    + ActualSize(lastItem.Value.rect);
            }
            pos = RelativePoint(pos);
            return pos * Sign <= size * reactSizeValue;
        }

        private bool CanRemoveAtStart()
        {
            LinkedListNode<RectTransform> node;
            if ((node = items.First) != null)
            {
                RectTransform item = node.Value;
                float pos = item.anchoredPosition[VectorMainIndex];
                pos += ActualSize(item.rect) * Sign;
                pos = RelativePoint(pos);
                var size = ActualSize(Scroll.viewport.rect);
                return pos * Sign < size - size * reactSizeValue;
            }
            return false;
        }

        private bool CanRemoveAtEnd()
        {
            LinkedListNode<RectTransform> item;
            if ((item = items.Last) != null)
            {
                float pos = item.Value.anchoredPosition[VectorMainIndex];
                pos = RelativePoint(pos);
                return pos * Sign > reactSizeValue * ActualSize(Scroll.viewport.rect);
            }
            return false;
        }

        private void UnloadItems()
        {
            int index = minIndex;
            foreach (var item in items)
            {
                UnloadItem(item, index);
                index += 1;
            }
            items.Clear();
        }

        private float RelativePoint(float point)
        {
            return point + Scroll.content.anchoredPosition[VectorMainIndex];
        }

        private Vector2 RelativePoint(Vector2 point)
        {
            var p = point;
            p[VectorMainIndex] = RelativePoint(p[VectorMainIndex]);
            return p;
        }

    }

}