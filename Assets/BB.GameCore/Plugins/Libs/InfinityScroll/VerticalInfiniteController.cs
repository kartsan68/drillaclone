﻿using System.Collections.Generic;
using BlackBears;

namespace UnityEngine.UI.Additional
{

    public class VerticalInfiniteController : BaseVerticalInfiniteController
    {

        [SerializeField] private RectTransform[] prefabs;

        private InfiniteAdapter adapter;
        private LinkedList<RectTransform>[] pools;

        private bool onReset = false;

        public InfiniteAdapter Adapter
        {
            get { return adapter; }
            set
            {
                if (adapter == value) return;
                adapter = value;
                if (Initiated) Reset();
            }
        }

        public override void Reset()
        {
            InitAdapter();
            onReset = true;
            base.Reset();
            onReset = false;
            ResourceUnloadManager.RequestUnload();
        }

        protected override void InternalInitiate()
        {
            base.InternalInitiate();
            InitAdapter();
        }

        protected override bool IsValidIndex(int index)
        {
            return Adapter != null ? Adapter.IsValidIndex(index) : false;
        }

        protected override RectTransform GetItemByIndex(int index)
        {
            int type = Adapter.GetTypeByIndex(index);
            RectTransform item = TakeItemFromPoolWithIndex(type);
            Adapter.BindItem(item.gameObject, index, type);
            return item;
        }

         protected override void OnItemAddedToStart(RectTransform item)
        {
            item.SetAsLastSibling();
        }

        protected override void OnItemAddedToEnd(RectTransform item)
        {
            item.SetAsFirstSibling();
        }

        protected override void SetupParent(RectTransform item, RectTransform parent) =>
            adapter.SetupParent(item, parent);

        protected override void UnloadItem(RectTransform item, int index)
        {
            if (onReset || Adapter == null)
            {
                Destroy(item.gameObject);
            }
            else
            {
                int type = Adapter.GetTypeByIndex(index);
                Adapter.UnbindItem(item.gameObject, index, type);
                PutItemToPoolWithIndex(type, item);
            }
        }

        private void InitAdapter()
        {
            if (pools != null) foreach (var pool in pools) ClearPool(pool);
            else pools = new LinkedList<RectTransform>[prefabs.Length];

            if (Adapter != null) Adapter.Controller = this;
        }

        private void ClearPool(LinkedList<RectTransform> pool)
        {
            if (pool == null) return;
            foreach (var item in pool) Destroy(item.gameObject);
            pool.Clear();
        }

        private RectTransform TakeItemFromPoolWithIndex(int type)
        {
            var pool = pools[type];
            if (pool == null || pool.Count == 0)
            {
                return Adapter.InstantiatePrefab(prefabs[type].gameObject).GetComponent<RectTransform>();
            }
            else
            {
                var item = pool.First.Value;
                pool.RemoveFirst();
                return item;
            }
        }

        private void PutItemToPoolWithIndex(int type, RectTransform item)
        {
            var pool = pools[type];
            if (pool == null)
            {
                pool = new LinkedList<RectTransform>();
                pools[type] = pool;
            }
            pool.AddLast(item);
        }

    }

}