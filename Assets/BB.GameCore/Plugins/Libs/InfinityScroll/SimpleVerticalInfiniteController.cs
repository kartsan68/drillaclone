﻿using System.Collections.Generic;

namespace UnityEngine.UI.Additional
{

    public class SimpleVerticalInfiniteController : BaseVerticalInfiniteController
    {

        private SimpleAdapter adapter = new SimpleAdapter();

        protected override void InternalInitiate()
        {
            base.InternalInitiate();
            adapter.Initiate(Scroll.viewport, Scroll.content);
        }

        protected sealed override bool IsValidIndex(int index)
        {
            return adapter.IsValidIndex(index);
        }

        protected sealed override RectTransform GetItemByIndex(int index)
        {
            return adapter.GetItemByIndex(index);
        }

        protected override void OnItemAddedToStart(RectTransform item)
        {
            item.SetAsLastSibling();
        }

        protected override void OnItemAddedToEnd(RectTransform item)
        {
            item.SetAsFirstSibling();
        }

        protected override void UnloadItem(RectTransform item, int index)
        {
            adapter.UnloadItem(item.gameObject);
        }

        protected override void SetupParent(RectTransform item, RectTransform parent) =>
            item.SetParent(parent);

    }

}