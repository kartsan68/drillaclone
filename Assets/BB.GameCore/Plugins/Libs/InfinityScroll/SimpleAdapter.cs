﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI.Additional
{
    
    public sealed class SimpleAdapter
    {

        public List<RectTransform> prefabs = new List<RectTransform>();

        public void Initiate(RectTransform viewport, RectTransform content)
        {
            for (int i = 0; i < content.childCount; )
            {
                var child = (RectTransform)content.GetChild(i);

                prefabs.Add(child);
                child.SetParent(viewport);
                child.gameObject.SetActive(false);
            }
        }

        public bool IsValidIndex(int index)
        {
            return index >= 0 && index < 15;
        }

        public RectTransform GetItemByIndex(int index)
        {
            var prefab = prefabs[index % prefabs.Count];
            var obj = MonoBehaviour.Instantiate<RectTransform>(prefab);
            obj.name = string.Format("Item_{0}", index);
            return obj;
        }

        public void UnloadItem(GameObject item)
        {
            MonoBehaviour.Destroy(item);
        }
    }

}