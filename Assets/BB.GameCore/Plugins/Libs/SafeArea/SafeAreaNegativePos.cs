using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaNegativePos : SafeAreaElement
    {

		[SerializeField, EnumFlags] private SafeAreaSide sides;

        protected override void UpdatePaddings()
        {
            Vector2 pos = Rect.anchoredPosition;

            if ((sides & SafeAreaSide.Top) != 0) pos.y += TopOffset;
            if ((sides & SafeAreaSide.Bottom) != 0) pos.y -= BottomOffset;

            if ((sides & SafeAreaSide.Left) != 0) pos.x -= LeftOffset;
            if ((sides & SafeAreaSide.Right) != 0) pos.x += RightOffset;

            Rect.anchoredPosition = pos;
        }

    }

}