using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaOffset : SafeAreaElement
    {

		[SerializeField, EnumFlags] private SafeAreaSide sides;

        protected override void UpdatePaddings()
        {
            Vector2 offsetMin = Rect.offsetMin;
            Vector2 offsetMax = Rect.offsetMax;

            if ((sides & SafeAreaSide.Top) != 0) offsetMax.y -= TopOffset;
            if ((sides & SafeAreaSide.Bottom) != 0) offsetMin.y += BottomOffset;
            if ((sides & SafeAreaSide.Left) != 0) offsetMin.x += LeftOffset;
            if ((sides & SafeAreaSide.Right) != 0) offsetMax.x -= RightOffset;

            Rect.offsetMin = offsetMin;
            Rect.offsetMax = offsetMax;
        }

    }

}