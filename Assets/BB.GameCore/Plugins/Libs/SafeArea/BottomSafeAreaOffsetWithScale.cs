﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.RectTransform;

namespace BlackBears.Utils.View.SafeArea
{
    public class BottomSafeAreaOffsetWithScale : SafeAreaElement
    {
        private bool changeScale = false;
        protected override void UpdatePaddings()
        {
            changeScale = false;
        }

        private void LateUpdate()
        {
            if (changeScale) return;

            Vector2 offsetMin = Rect.offsetMin;

            var startHeight = Rect.rect.height;

            offsetMin.y += BottomOffset;

            Rect.offsetMin = offsetMin;

            var difference = startHeight / Rect.rect.height;

            Rect.SetSizeWithCurrentAnchors(Axis.Horizontal, Rect.rect.width * difference);
            Rect.SetSizeWithCurrentAnchors(Axis.Vertical, Rect.rect.height * difference);

            Rect.localScale /= difference;

            changeScale = true;

        }
    }

}