using System;
using System.Collections.Generic;

namespace BlackBears.Utils.View.SafeArea
{

    public static class SafeAreaDataContainer
    {

        private static Dictionary<string, SafeAreaData> dataByKey = new Dictionary<string, SafeAreaData>();

        public static void Register(SafeAreaData data)
        {
            if (data != null) dataByKey[data.Key] = data;
        }

        public static void Unregister(SafeAreaData data)
        {
            if (data != null) dataByKey.Remove(data.Key);
        }

        internal static bool TryGetData(string dataKey, out SafeAreaData data)
        {
            return dataByKey.TryGetValue(dataKey, out data);
        }

    }

}