using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(RectTransform))]
    public class BottomSafeAreaNegativePosWithScale : SafeAreaElement
    {
        protected override void UpdatePaddings()
        {
            Vector2 pos = Rect.anchoredPosition;

            var difference = Rect.rect.height / (Rect.rect.height - BottomOffset);

            pos.y -= BottomOffset * difference;

            Rect.anchoredPosition = pos;
        }

    }

}