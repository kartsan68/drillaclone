﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.Utils.View.SafeArea
{

    [System.Flags]
    public enum SafeAreaSide
    {
        Left = 1,
        Right = Left << 1,
        Top = Right << 1,
        Bottom = Top << 1
    }

    public abstract class SafeAreaElement : MonoBehaviour
    {

        public bool inAwake = false;
        public string dataKey;

        protected Rect safeArea;

        private RectTransform rect;
        public float scaleFactor;
        private float bottomSafeAreaFactor;

        private UnityEvent onPaddingsUpdate = new UnityEvent();

        protected RectTransform Rect { get { return rect ?? (rect = GetComponent<RectTransform>()); } }

        protected float TopOffset { get { return (Screen.height - safeArea.yMax) / scaleFactor; } }
        protected float BottomOffset { get { return (safeArea.yMin / scaleFactor) * bottomSafeAreaFactor; } }
        protected float LeftOffset { get { return safeArea.xMin / scaleFactor; } }
        protected float RightOffset { get { return (Screen.width - safeArea.xMax) / scaleFactor; } }

        public UnityEvent OnPaddingsUpdate { get { return onPaddingsUpdate; } }


        private void Awake()
        {
            if (inAwake) UpdateSafeArea();
        }
        private void Start()
        {
            if (!inAwake) UpdateSafeArea();
        }

        protected abstract void UpdatePaddings();

        private void UpdateSafeArea()
        {
            SafeAreaData data;
            if (SafeAreaDataContainer.TryGetData(dataKey, out data))
            {
                safeArea = data.Area;
                scaleFactor = data.ScaleFactor;
                bottomSafeAreaFactor = data.BottomSafeAreaFactor;
            }
            else
            {
                safeArea = Screen.safeArea;
                scaleFactor = 1f;
                bottomSafeAreaFactor = 1f;
            }

            UpdatePaddings();
        }

    }

}