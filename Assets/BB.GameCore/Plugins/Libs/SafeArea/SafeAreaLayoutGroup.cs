﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(LayoutGroup))]
    public class SafeAreaLayoutGroup : SafeAreaElement
    {

		[SerializeField, EnumFlags] private SafeAreaSide sides;

        private LayoutGroup group;

        private LayoutGroup Group { get { return group ?? (group = GetComponent<LayoutGroup>()); } }

        protected override void UpdatePaddings()
        {
            if ((sides & SafeAreaSide.Top) != 0) Group.padding.top += (int)TopOffset;
            if ((sides & SafeAreaSide.Bottom) != 0) Group.padding.bottom += (int)BottomOffset;
            if ((sides & SafeAreaSide.Left) != 0) Group.padding.left += (int)LeftOffset;
            if ((sides & SafeAreaSide.Right) != 0) Group.padding.right += (int)RightOffset;
        }
    }

}