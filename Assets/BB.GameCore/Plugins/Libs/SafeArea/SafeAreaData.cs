﻿using BlackBears.Resolutor.Scaling;
using BlackBears.Utils.Scaling;
using UnityEngine;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(IScaleSelector))]
    public class SafeAreaData : MonoBehaviour
    {

        private IScaleSelector scaleSelector;
        [SerializeField] private string key;
        [SerializeField] private float bottomSafeAreaFactor = 1f;

#if UNITY_EDITOR
        [SerializeField] private Vector2 bottomLeftOffset;
        [SerializeField] private Vector2 topRightOffset;
#endif

        private bool started = false;

        private IScaleSelector ScaleSelector
        {
            get { return scaleSelector ?? (scaleSelector = GetComponent<IScaleSelector>()); }
        }

        private void Start()
        {
            started = true;
            SafeAreaDataContainer.Register(this);
        }

        public string Key
        {
            get { return key; }
            set
            {
                if (started) SafeAreaDataContainer.Unregister(this);
                key = value;
                if (started) SafeAreaDataContainer.Register(this);
            }
        }

        public Rect Area
        {
            get
            {
#if UNITY_EDITOR
                return new Rect(bottomLeftOffset.x, bottomLeftOffset.y,
                    Screen.width - topRightOffset.x - bottomLeftOffset.x,
                    Screen.height - topRightOffset.y - bottomLeftOffset.y);
#elif UNITY_ANDROID

                var safeArea = new Rect(Screen.safeArea);
                var max = Mathf.Max(safeArea.xMin, Screen.width - safeArea.xMax);
                safeArea.xMin = max;
                safeArea.xMax = Screen.width - max;
                return safeArea;

#else
                return Screen.safeArea;
#endif
            }
        }

        public float ScaleFactor
        {
            get
            {
                float scale = 1f;
                var selector = ScaleSelector;
                if (selector != null) scale = (float)selector.CurrentScale;
                return scale;
            }
        }

        public float BottomSafeAreaFactor => bottomSafeAreaFactor;

    }

}