﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(LayoutElement))]
    public class SafeAreaLayoutElementSize : SafeAreaElement
    {

		[SerializeField, EnumFlags] private SafeAreaSide sides;
        private LayoutElement layoutElement;

        private LayoutElement LayoutElement { get { return layoutElement ?? (layoutElement = GetComponent<LayoutElement>()); } }

        protected override void UpdatePaddings()
        {
            var size = new Vector2(LayoutElement.minWidth, LayoutElement.minHeight);

            if ((sides & SafeAreaSide.Top) != 0) size.y += TopOffset;
            if ((sides & SafeAreaSide.Bottom) != 0) size.y += BottomOffset;
            if ((sides & SafeAreaSide.Left) != 0) size.x += LeftOffset;
            if ((sides & SafeAreaSide.Right) != 0) size.x += RightOffset;

            LayoutElement.minWidth = size.x;
            LayoutElement.minHeight = size.y;
        }

    }

}