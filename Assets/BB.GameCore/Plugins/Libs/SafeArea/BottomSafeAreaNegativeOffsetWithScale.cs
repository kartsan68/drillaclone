using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Utils.View.SafeArea
{

    [RequireComponent(typeof(RectTransform))]
    public class BottomSafeAreaNegativeOffsetWithScale : SafeAreaElement
    {

        protected override void UpdatePaddings()
        {
            if(BottomOffset == 0) return;

            Vector2 offsetMin = Rect.offsetMin;

            var difference = Rect.rect.height / (Rect.rect.height - BottomOffset);

            offsetMin.y -= BottomOffset * difference;

            Rect.offsetMin = offsetMin;
        }

    }

}