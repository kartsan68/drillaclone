using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.Utils.View.SafeArea
{

    public class SafeAreaSize : SafeAreaElement
    {

		[SerializeField, EnumFlags] private SafeAreaSide sides;

        protected override void UpdatePaddings()
        {
            var size = Rect.sizeDelta;

            if ((sides & SafeAreaSide.Top) != 0) size.y += TopOffset;
            if ((sides & SafeAreaSide.Bottom) != 0) size.y += BottomOffset;
            if ((sides & SafeAreaSide.Left) != 0) size.x += LeftOffset;
            if ((sides & SafeAreaSide.Right) != 0) size.x += RightOffset;

            Rect.sizeDelta = size;
        }

    }

}