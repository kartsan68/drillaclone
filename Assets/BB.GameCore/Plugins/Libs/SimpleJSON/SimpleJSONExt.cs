using System.Collections.Generic;
using BlackBears;
using BlackBears.Utils.Collections;

namespace SimpleJSON
{

    public static class JSONNodeExtension
    {

        public static string GetStringOrDefault(this JSONNode node, string @default = default(string))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.Value;
        }

        public static int GetIntOrDefault(this JSONNode node, int @default = default(int))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.AsInt;
        }

        public static PInt GetPIntOrDefault(this JSONNode node, PInt @default = default(PInt))
        {
            if (node == null || node.Tag == JSONNodeType.None
                || node.Tag == JSONNodeType.NullValue) return @default;
            return PInt.Parse(node.Value);
        }

        public static double GetDoubleOrDefault(this JSONNode node, double @default = default(double))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.AsDouble;
        }

        public static PDouble GetPDoubleOrDefault(this JSONNode node, PDouble @default = default(PDouble))
        {
            if (node == null || node.Tag == JSONNodeType.None || 
                node.Tag == JSONNodeType.NullValue) return @default;
            return PDouble.Parse(node.Value);
        }

        public static float GetFloatOrDefault(this JSONNode node, float @default = default(float))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.AsFloat;
        }

        public static bool GetBoolOrDefault(this JSONNode node, bool @default = default(bool))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.AsBool;
        }

        public static long GetLongOrDefault(this JSONNode node, long @default = default(long))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.AsLong;
        }

        public static PLong GetPLongOrDefault(this JSONNode node, PLong @default = default(PLong))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return PLong.Parse(node.Value);
        }

        public static ulong GetULongOrDefault(this JSONNode node, ulong @default = default(ulong))
        {
            if (node == null || node.Tag == JSONNodeType.None 
                || node.Tag == JSONNodeType.NullValue) return @default;
            return node.AsULong;
        }

        public static JSONArray ToJSONArray(this IEnumerable<string> list)
        {
            var json = new JSONArray();
            if (list != null) foreach (var item in list) json.Add(item);
            return json;
        }

        public static JSONArray ToJSONArray(this IEnumerable<JSONNode> list)
        {
            var json = new JSONArray();
            if (list != null) foreach (var node in list) json.Add(node);
            return json;
        }

        public static IDictionary<int, float> ToProbabilityDictionary(this JSONNode node)
        {
            if (node == null || node.Tag == JSONNodeType.None) return null;

            var result = new Dictionary<int, float>();
            foreach (var item in node)
                result[int.Parse(item.Key)] = item.Value.AsFloat;
            return result.ToImmutable();
        }

        public static List<T> ToList<T>(this JSONNode json, System.Func<JSONNode, T> converter)
        {
            if (json == null || json.Tag != JSONNodeType.Array || converter == null) return null;

            var result = new List<T>();
            foreach (var item in json.Children) result.Add(converter(item));

            return result;
        }

    }

}