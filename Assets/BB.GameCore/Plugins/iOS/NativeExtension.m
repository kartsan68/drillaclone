//
//  NativeExtension.m
//
//  Created by Ilya Strekalov on 19/04/2018.
//  Copyright © 2018 BlackBears Games. All rights reserved.
//

#import "NativeExtension.h"
#import "UnityAppController.h"
#import "BBCloudHelper.h"
#import <MessageUI/MessageUI.h>

@interface MFMailComposeViewController (extension) <MFMailComposeViewControllerDelegate>
@end

@implementation MFMailComposeViewController (extension)
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end

@interface NativeExtension ()
@end

@implementation NativeExtension

#pragma mark - Sharing

+ (void)shareText:(NSString *)text link:(NSString *)link imageByPath:(NSString *)imagePath
{
    NSMutableArray *shareItems = [NSMutableArray array];
    
    if (text) [shareItems addObject:text];
    if (link) [shareItems addObject:[NSURL URLWithString:link]];
    if (imagePath)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:imagePath])
        {
            NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
            UIImage *image = [UIImage imageWithData:imageData];
            
            if (image) [shareItems addObject:image];
        }
    }
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[shareItems copy] applicationActivities:nil];
    activityVC.excludedActivityTypes = @[
                                         UIActivityTypePrint,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         ];
    
    UIViewController *rootViewController = UnityGetGLViewController();
    
    if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
    {
        CGFloat x = CGRectGetMidX(rootViewController.view.frame);
        CGFloat y = CGRectGetMaxY(rootViewController.view.frame);
        
        activityVC.popoverPresentationController.sourceView = rootViewController.view;
        activityVC.popoverPresentationController.sourceRect = CGRectMake(x, y, 0, 0);
    }
    
    [rootViewController presentViewController:activityVC animated:YES completion:nil];
}

+ (void)openStoreWithITunesItemIdentifier:(NSString *)itemIdentifier
{
    NSString * appStoreLink = [NSString stringWithFormat:@"https://itunes.apple.com/developer/id%@", itemIdentifier];
    [UIApplication.sharedApplication openURL:[NSURL URLWithString:appStoreLink] options:@{} completionHandler:nil];
}

+ (void)sendEmailToMail:(NSString *)mail Subject:(NSString *) subject Text:(NSString *) text
{
    if (![MFMailComposeViewController canSendMail]) return;
    
    MFMailComposeViewController *mailVC = [MFMailComposeViewController new];

    [mailVC setMailComposeDelegate:mailVC];
    [mailVC setSubject:subject];
    [mailVC setMessageBody:text isHTML:NO];
    [mailVC setToRecipients:@[ mail ]];
    
    UIViewController *vc = UnityGetGLViewController();
    [vc presentViewController:mailVC animated:YES completion:nil];
}

+ (bool)isAppInstalledWithUrlScheme:(NSString *)urlScheme
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlScheme]];
}

+ (void)openApplicationWithUrlScheme:(NSString *)urlScheme Link:(NSString *)link
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlScheme]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlScheme]];
    }
    else 
    {
        [self openApplicationStoreWithLink:link];
    }
}

+ (void)openApplicationStoreWithLink:(NSString *)link
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:link]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
    }
}

+ (void)openRateWithLinkFormat:(NSString *)linkFormat appId:(NSString *)appId
{
    NSString* appURL = nil;
    float iOSVersion = [[UIDevice currentDevice].systemVersion floatValue];

    if (iOSVersion > 10.1)
    {
        appURL = [NSString stringWithFormat:linkFormat, appId];
    }
    else if (iOSVersion >= 7.0f && iOSVersion < 7.1f)
    {
        appURL = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/%@/app/id%@", [[NSLocale preferredLanguages] firstObject], appId];
    }
    else
    {
        appURL = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", appId];
    }

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appURL]];
}

+ (void)openNotificationsSettings
{
    [UIApplication.sharedApplication openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

#pragma mark - Unity => Obj-C

void BBShowSocialSharing(struct SocialSharingStruct *sharingStruct)
{
    [NativeExtension shareText:stringByChars(sharingStruct->text)
                          link:stringByChars(sharingStruct->url)
                   imageByPath:stringByChars(sharingStruct->image)];
}

void BBOpenStore(char* identifier)
{
    [NativeExtension openStoreWithITunesItemIdentifier:stringByChars(identifier)];
}

const char* BBGetOsVersion() { return stringToChars(UIDevice.currentDevice.systemVersion); }

void BBSendEmail(char* mail, char* subject, char* text)
{
    [NativeExtension sendEmailToMail:stringByChars(mail) Subject:stringByChars(subject) Text:stringByChars(text)];
}

bool BBIsAppInstalled(char* urlScheme)
{
    return [NativeExtension isAppInstalledWithUrlScheme:stringByChars(urlScheme)];
}

void BBOpenApplication(char* urlScheme, char* link)
{
    [NativeExtension openApplicationWithUrlScheme:stringByChars(urlScheme) Link:stringByChars(link)];
}

void BBOpenApplicationStore(char* link)
{
    [NativeExtension openApplicationStoreWithLink:stringByChars(link)];
}

void BBOpenRate(struct RateDataStruct *rateDataStruct)
{
    [NativeExtension openRateWithLinkFormat:stringByChars(rateDataStruct->linkFormat)
                                      appId:stringByChars(rateDataStruct->appId)];
}

bool BBCloudIsActive()
{
    return [[BBCloudHelper shared] isActive];
}

void BBSaveToCloud(char* saveName, char* save)
{
    [[BBCloudHelper shared] save:stringByChars(save) withName:stringByChars(saveName)];
}

void BBLoadFromCloud(char* saveName)
{
    char *gameObjecName = "SaveCloudHandler";
    [[BBCloudHelper shared] loadForName:stringByChars(saveName) onLoaded:^(NSString *output) {
        UnitySendMessage(gameObjecName, "OnSaveLoaded", stringToChars(output));
    } onFail:^{
        UnitySendMessage(gameObjecName, "OnLoadFailed", "");
    }];
}


void BBOpenNotificationsSettings()
{
    [NativeExtension openNotificationsSettings];
}

#pragma mark - Helpers

NSString *stringByChars(char* chars)
{
    if (!chars) return nil;
    
    NSString *string = [NSString stringWithUTF8String:chars];
    return (string != NULL && string.length > 0) ? string : nil;
}

char* stringToChars(NSString *string)
{
    const char *ch = string.UTF8String;
    
    if (string == NULL) return NULL;
    
    char* res = (char*)malloc(strlen(ch) + 1);
    strcpy(res, ch);
    return res;
}

@end
