//
//  NativeExtension.h
//
//  Created by Ilya Strekalov on 19/04/2018.
//  Copyright © 2018 BlackBears Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NativeExtension : NSObject

struct SocialSharingStruct
{
    char* text;
    char* url;
    char* image;
};

struct RateDataStruct
{
    char* linkFormat;
    char* appId;
};

#ifdef __cplusplus
extern "C"
{
#endif

    void BBShowSocialSharing(struct SocialSharingStruct *sharingStruct);
    void BBOpenStore(char* identifier);
    const char* BBGetOsVersion();
    void BBSendEmail(char* mail, char* subject, char* text);
    bool BBIsAppInstalled(char* urlScheme);
    void BBOpenApplication(char* urlScheme, char* link);
    void BBOpenApplicationStore(char* link);
    void BBOpenRate(struct RateDataStruct *rateDataStruct);
    void BBOpenNotificationsSettings();

    //iCloud
    bool BBCloudIsActive();
    void BBSaveToCloud(char* saveName, char* save);
    void BBLoadFromCloud(char* saveName);

#ifdef __cplusplus
}
#endif

#pragma mark - Helpers

NSString *stringByChars(char* chars);
char* stringToChars(NSString *string);

@end
