#import <Foundation/Foundation.h>

@interface BBCloudHelper : NSObject

@property (readonly) BOOL isActive;

+ (instancetype)shared;

- (void)save:(NSString *)input withName:(NSString *)saveName;
- (void)loadForName:(NSString *)saveName onLoaded:(void (^)(NSString *output))onLoaded onFail:(void (^)())onFail;

@end
