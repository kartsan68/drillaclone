#import "BBCloudHelper.h"
#import "BBKeychainHelper.h"

NSString *kWasSavedBefore   = @"was_saved_before";

static NSTimeInterval kProcessLocalDataDelay        = 5.;
static NSTimeInterval kProcessLocalDataLongDelay    = 15.;

@interface BBCloudHelper ()

@property (readonly, nonatomic) NSUbiquitousKeyValueStore *store;
@property NSString *saveName;

@property (copy) void (^onLoaded)(NSString *output);
@property (copy) void (^onFail)();

@end

@implementation BBCloudHelper

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)shared
{
    static id _sharedInstance;
    if(!_sharedInstance) {
        static dispatch_once_t oncePredicate;
        dispatch_once(&oncePredicate, ^{
            _sharedInstance = [self new];
        });
    }
    return _sharedInstance;
}


#pragma mark -

- (void)save:(NSString *)input withName:(NSString *)saveName
{
    [self.store setString:input forKey:saveName];
    
    BOOL synced = [self.store synchronize];
    if (synced) [BBKeychainHelper setBoolValue:YES forKey:kWasSavedBefore];
}

- (void)loadForName:(NSString *)saveName onLoaded:(void (^)(NSString *output))onLoaded onFail:(void (^)())onFail
{
    self.onLoaded = [onLoaded copy];
    self.onFail = [onFail copy];
    
    self.saveName = saveName;
    
    BOOL wasSavedBefore = [BBKeychainHelper boolValueForKey:kWasSavedBefore];
    [self performSelector:@selector(processStoredData) withObject:nil afterDelay:wasSavedBefore ? kProcessLocalDataLongDelay : kProcessLocalDataDelay];

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector (storeDidChange:)
                                                 name: NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                               object: self.store];
    [self.store synchronize];
}

- (void)processStoredData
{
    [self _unsubscribe];

    NSString *save = [self.store stringForKey:self.saveName];
    if (save)
    {
        if (self.onLoaded) self.onLoaded(save);
    }
    else
    {
        if (self.onFail) self.onFail();
    }
    
    [self _cleanUp];
}

- (void)_cleanUp
{
    self.onFail = nil;
    self.onLoaded = nil;
    self.saveName = nil;
}

- (void)_unsubscribe
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification object:self.store];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}


#pragma mark - Setters / Getters

- (BOOL)isActive
{
    return [self.store synchronize];
}


#pragma mark - Messaging

- (void)storeDidChange:(NSNotification *)notification
{
    [self processStoredData];
}


#pragma mark - Setters / Getters

- (NSUbiquitousKeyValueStore *)store
{
    return NSUbiquitousKeyValueStore.defaultStore;
}

@end
