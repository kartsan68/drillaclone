#import <Foundation/Foundation.h>

@interface BBKeychainHelper : NSObject

+ (BOOL)setBoolValue:(BOOL)value forKey:(NSString *)key;
+ (BOOL)setStringValue:(NSString *)value forKey:(NSString *)key;

+ (BOOL)boolValueForKey:(NSString *)key;
+ (NSString *)stringValueForKey:(NSString *)key;

@end
