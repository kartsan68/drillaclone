#import "BBKeychainHelper.h"
#import <Security/Security.h>

NS_INLINE NSString * NSStringFromCFStringRef(CFStringRef stringRef)
{
    return (__bridge NSString *)stringRef;
}

@implementation NSObject(apply)
+ (instancetype)cast:(id)object { return [object isKindOfClass:self] ? object : nil; }
@end

@implementation BBKeychainHelper

#pragma mark - Access

+ (BOOL)setBoolValue:(BOOL)value forKey:(NSString *)key
{
    return [self _setValue:@(value) forKey:key];
}

+ (BOOL)setStringValue:(NSString *)value forKey:(NSString *)key
{
    return [self _setValue:value forKey:key];
}

+ (BOOL)boolValueForKey:(NSString *)key
{
    return [[self _objectForKey:key] boolValue];
}

+ (NSString *)stringValueForKey:(NSString *)key
{
    return [NSString cast:[self _objectForKey:key]];
}


#pragma mark - Private

+ (OSStatus)_setValue:(id)value forKey:(NSString *)key
{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value];
    CFDictionaryRef query = (__bridge CFDictionaryRef)@{
                                                        NSStringFromCFStringRef(kSecClass)              : NSStringFromCFStringRef(kSecClassGenericPassword),
                                                        NSStringFromCFStringRef(kSecAttrAccount)        : key,
                                                        NSStringFromCFStringRef(kSecValueData)          : data,
                                                        NSStringFromCFStringRef(kSecAttrSynchronizable) : @(YES),
                                                        NSStringFromCFStringRef(kSecAttrLabel)          : [self _applicationName]
                                                        };
    SecItemDelete(query);
    return SecItemAdd(query, nil) == noErr;
}

+ (id)_objectForKey:(NSString *)key
{
    CFDictionaryRef query = (__bridge CFDictionaryRef)@{
                                                        NSStringFromCFStringRef(kSecClass)              : NSStringFromCFStringRef(kSecClassGenericPassword),
                                                        NSStringFromCFStringRef(kSecAttrAccount)        : key,
                                                        NSStringFromCFStringRef(kSecReturnData)         : @(YES),
                                                        NSStringFromCFStringRef(kSecMatchLimit)         : NSStringFromCFStringRef(kSecMatchLimitOne),
                                                        NSStringFromCFStringRef(kSecAttrSynchronizable) : @(YES)
                                                        };
    
    CFTypeRef dataTypeRef = nil;
    OSStatus status = SecItemCopyMatching(query, &dataTypeRef);
    
    return status == noErr ? [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge id)dataTypeRef] : nil;
}

+ (NSString *)_applicationName
{
    NSDictionary *infoDictionary = NSBundle.mainBundle.infoDictionary;
    NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    if (!appName) appName = [infoDictionary objectForKey:@"CFBundleName"];
    if (!appName) appName = [infoDictionary objectForKey:@"CFBundleIdentifier"];
    
    return appName;
}

@end
