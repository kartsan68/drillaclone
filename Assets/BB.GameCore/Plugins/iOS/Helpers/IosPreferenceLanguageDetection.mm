char* cStringCopy(const char* string)
{
   if (string == NULL)
   {
       return NULL;
   }
   char* res = (char*)malloc(strlen(string) + 1);
   strcpy(res, string);
   return res;
}
extern "C"
{
    // -- we define our external method to be in C.
    char* _getPreferenceLanguageString()
    {
     // We can use NSString and go to the c string that Unity wants
     // UTF8String method gets us a c string. Then we have to malloc        //a copy to give to Unity. I reuse a method below that makes it //easy.
    NSString * language =[[NSLocale preferredLanguages]firstObject];     
    return cStringCopy([language UTF8String]);
    }
}