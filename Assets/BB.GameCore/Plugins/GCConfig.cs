using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Alerts;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.Clans;
using BlackBears.GameCore.Features.GameServer;
using BlackBears.GameCore.Features.Ads;
using BlackBears.GameCore.Features.Referals;
using BlackBears.GameCore.Features.SQBA;
using BlackBears.GameCore.Features.SQBAFiles;
using BlackBears.GameCore.Features.BBGames;
using BlackBears.GameCore.Features.Notifications;
using BlackBears.GameCore.Features.Localization;
using BlackBears.GameCore.Features.AppUpdate;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.GDPR;
using BlackBears.GameCore.Features.DropItemSystem;
using UnityEngine;
using BlackBears.GameCore.Features.Save;
using BlackBears.GameCore.Features.Rate;
using System.Runtime.CompilerServices;
using BlackBears.GameCore.Features.Analytics;
using BlackBears.GameCore.Features.Vibration;
using BlackBears.GameCore.Features.LocalNotifications;

[assembly:InternalsVisibleTo("Assembly-CSharp-Editor")]
[assembly:InternalsVisibleTo("Assembly-CSharp-Editor.dll")]


namespace BlackBears.GameCore
{

    [CreateAssetMenu(fileName = "GameCoreConfig", menuName = "BlackBears/GameCore Config", order = 1)]
    public sealed class GCConfig : ScriptableObject
    {
        [SerializeField] private AnalyticsFeatureConfig analytics;
        [SerializeField] private LocalizationFeatureConfig localization;
        [SerializeField] private AlertsFeatureConfig alerts;
        [SerializeField] private ChronometerFeatureConfig chronometer;
        [SerializeField] private SQBAFeatureConfig sqba;
        [SerializeField] private AppUpdateFeatureConfig appUpdate;
        [SerializeField] private SQBAFilesFeatureConfig sqbaFiles;
        [SerializeField] private CoreDefaultsFeatureConfig coreDefaults;
        [SerializeField] private GDPRFeatureConfig gdpr;
        [SerializeField] private RateFeatureConfig rate;
        [SerializeField] private AdsFeatureConfig  ads;
        [SerializeField] private ReferalsFeatureConfig referals;
        [SerializeField] private GameServerFeatureConfig gameServer;
        [SerializeField] private BBGamesFeatureConfig bbGames;
        [SerializeField] private FeatureConfig leagues;
        [SerializeField] private ClansFeatureConfig clans;
        [SerializeField] private DropItemSystemFeatureConfig dropItemSystem;
        [SerializeField] private SaveFeatureConfig save;
        [SerializeField] private LocalNotificationsFeatureConfig localNotifications;

        internal AnalyticsFeatureConfig Analytics { get { return analytics; } }
        internal LocalizationFeatureConfig Localization { get { return localization; } }
        internal AlertsFeatureConfig Alerts { get { return alerts; } }
        internal ChronometerFeatureConfig Chronometer { get { return chronometer; } }
        internal SQBAFeatureConfig SQBA { get { return sqba; } }
        internal SQBAFilesFeatureConfig SQBAFiles { get { return sqbaFiles; } }
        internal CoreDefaultsFeatureConfig CoreDefaults { get { return coreDefaults; } }
        internal GDPRFeatureConfig GDPR { get { return gdpr; } }
        internal AppUpdateFeatureConfig AppUpdate { get { return appUpdate; } }
        internal RateFeatureConfig Rate { get { return rate; } }
        internal AdsFeatureConfig Ads { get { return ads; } }
        internal ReferalsFeatureConfig Referals { get { return referals; } }
        internal GameServerFeatureConfig GameServer { get { return gameServer; } }
        internal BBGamesFeatureConfig BBGames { get { return bbGames; } }
        internal FeatureConfig Leagues { get { return leagues; } }
        internal ClansFeatureConfig Clans { get { return clans; } }
        internal DropItemSystemFeatureConfig DropItemSystem { get { return dropItemSystem; } }
        internal SaveFeatureConfig Save { get { return save; } }
        internal LocalNotificationsFeatureConfig LocalNotifications => localNotifications;

    }

}