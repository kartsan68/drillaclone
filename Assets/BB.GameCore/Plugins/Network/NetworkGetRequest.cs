using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Networking
{

    public class NetworkGetRequest : NetworkRequest
    {

        private Block<JSONNode> onSuccess;

        public NetworkGetRequest(string url, JSONNode data, Block<JSONNode> onSuccess, FailBlock onFail)
            : this(data, onSuccess, onFail)
        {
            this.url = url;
        }

        protected NetworkGetRequest(JSONNode data, Block<JSONNode> onSuccess, FailBlock onFail) : base(data)
        {
            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }

        public override IEnumerator Start()
        {
            if (!Prepared) Prepare();

            request.timeout = 5;
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                ProcessError(request.responseCode, request.error);
            }
            else
            {
                string responseString = request.downloadHandler.text;
                JSONNode responseData = JSON.Parse(responseString);

                long errorCode = responseData["error_code"].AsLong;
                if (errorCode != 0 || responseData["response"].IsNull)
                {
                    ProcessError(errorCode, responseData["error_message"].Value);
                }
                else
                {
                    onSuccess.SafeInvoke(responseData["response"]);
                }
            }
        }

        protected override void Prepare()
        {
            base.Prepare();
            var requestUrl = GenerateUrl(url);
            request = UnityWebRequest.Get(requestUrl);
        }

        private string GenerateUrl(string url)
        {
            var sb = new StringBuilder(url);
            int index = -1;
            foreach(var kvp in Data)
            {
                index += 1;
                sb.Append(index == 0 ? "?" : "&");
                sb.Append(kvp.Key).Append("=").Append(kvp.Value.Value);
            }
            return sb.ToString();
        }

    }

}