﻿namespace BlackBears.GameCore.Networking
{
    public class NetworkFileData
    {
        private string mime;
        private string name;
        private byte[] data;

        public NetworkFileData(byte[] data, string name, string mime)
        {
            this.data = data;
            this.name = name;
            this.mime = mime;
        }

        public string Mime { get { return mime; } }
        public string Name { get { return name; } }
        public byte[] Data { get { return data; } }
    }
}
