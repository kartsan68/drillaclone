using SimpleJSON;

namespace BlackBears.GameCore.Networking
{

    public abstract class DataConverter<T> where T : class
    {

        private T instance;
        private Block<T> onConvert;
        private FailBlock onFail;

        public DataConverter(Block<T> onConvert, FailBlock onFail, T instance = null)
        {
            this.instance = instance;
            this.onConvert = onConvert;
            this.onFail = onFail;
        }

        public void Convert(JSONNode data)
        {
            if (Convert(ref instance, data)) onConvert.SafeInvoke(instance);
            else onFail.SafeInvoke(-1, "Parse server answer error");
        }

        protected abstract bool Convert(ref T instance, JSONNode data);

    }

}