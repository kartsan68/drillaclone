﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Networking
{
    public abstract class NetworkRequest
    {

        protected string url;
        protected UnityWebRequest request;
        protected FailBlock onFail;

        private JSONNode data;
        private Dictionary<string, NetworkFileData> filesData;
        private bool prepared;

        protected NetworkRequest(JSONNode data)
        {
            this.data = data ?? new JSONObject();

            filesData = new Dictionary<string, NetworkFileData>();
        }

        protected bool Prepared { get { return prepared; } }
        protected JSONNode Data { get { return data; } }
        protected Dictionary<string, NetworkFileData> FilesData { get { return filesData; } }

        public void AddRequestValue(string key, JSONNode parameters)
        {
            if (parameters.IsNull || parameters == null) return;

            data.Add(key, parameters);
        }

        public void AddRequestValue(JSONNode parameters)
        {
            if (parameters.IsNull || parameters == null) return;

            data.Add(parameters);
        }

        public void AddRequestValue(string key, byte[] fileData, string name, string mime)
        {
            if (key == null) return;

            NetworkFileData file = new NetworkFileData(fileData, name, mime);
            filesData.Add(key, file);
        }

        public abstract IEnumerator Start();

        protected virtual void Prepare()
        {
            prepared = true;
        }

        protected virtual void ProcessError(long code, string message)
        {
            onFail.SafeInvoke(code, message);
        }

    }
}
