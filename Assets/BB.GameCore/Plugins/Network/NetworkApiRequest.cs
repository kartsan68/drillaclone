using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Networking
{
    public class NetworkApiRequest : NetworkRequest
    {
        
        protected readonly Block<JSONNode> onSuccess;

        private StringContent httpContent;
        public bool useNetHttpClient = false;

        public NetworkApiRequest(string url, JSONNode data, Block<JSONNode> onSuccess, FailBlock onFail)
            : this(data, onSuccess, onFail)
        {
            this.url = url;
        }

        public override IEnumerator Start()
        {
            if (!Prepared) Prepare();

            if (useNetHttpClient)
            {
                var task = PostAsync();
            }
            else
            {
                request.timeout = 5;
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    ProcessError(request.responseCode, request.error);
                }
                else
                {
                    var responseString = request.downloadHandler.text;
                    var responseData = JSON.Parse(responseString);

                    var errorCode = responseData["error_code"].AsLong;
                    if (errorCode != 0 || responseData["response"].IsNull)
                    {
                        var errorMessage = responseData["error_msg"]?.Value ?? responseData["error_message"]?.Value;
                        ProcessError(errorCode, errorMessage);
                    }
                    else
                    {
                        onSuccess.SafeInvoke(responseData["response"]);
                    }
                }
            }
        }

        public async System.Threading.Tasks.Task PostAsync()
        {
            HttpClient client = new HttpClient();

            using (var httpResponseMessage = await client.PostAsync(url, httpContent))
            {
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    var responseString = await httpResponseMessage.Content.ReadAsStringAsync();
                    var responseData = JSON.Parse(responseString);

                    var errorCode = responseData["error_code"].AsLong;
                    if (errorCode != 0 || responseData["response"].IsNull)
                    {
                        var errorMessage = responseData["error_msg"]?.Value ?? responseData["error_message"]?.Value;
                        ProcessError(errorCode, errorMessage);
                    }
                    else
                    {
                        onSuccess.SafeInvoke(responseData["response"]);
                    }
                }
                else
                {
                    ProcessError((int)httpResponseMessage.StatusCode, httpResponseMessage.StatusCode.ToString());
                }
            }
        }

        protected NetworkApiRequest(JSONNode data, Block<JSONNode> onSuccess, FailBlock onFail) : base(data)
        {
            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }

        protected override void Prepare()
        {
            base.Prepare();

            if (FilesData.Count > 0) request = UnityWebRequest.Post(url, EncodeMultipartData());
            else
            {
                if (useNetHttpClient)
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                    var requestContent = EncodeSimpleData();
                    var encodedItems = requestContent.Select(i => WebUtility.UrlEncode(i.Key) + "=" + WebUtility.UrlEncode(i.Value));
                    httpContent = new StringContent(String.Join("&", encodedItems), null, "application/x-www-form-urlencoded");
                }
                else
                {
                    request = UnityWebRequest.Post(url, EncodeSimpleData());
                }
            }
        }

        private List<IMultipartFormSection> EncodeMultipartData()
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            AddTextData(formData, Data);
            AddFilesData(formData, FilesData);

            return formData;
        }

        private void AddTextData(List<IMultipartFormSection> formData, JSONNode data)
        {
            ConvertTextData(data, (k, v) => formData.Add(new MultipartFormDataSection(k, v, "application/json")));
        }

        private void AddFilesData(List<IMultipartFormSection> formData, Dictionary<string, NetworkFileData> filesData)
        {
            foreach (KeyValuePair<string, NetworkFileData> kvp in filesData)
            {
                formData.Add(new MultipartFormFileSection(kvp.Key, kvp.Value.Data, kvp.Value.Name, kvp.Value.Mime));
            }
        }

        private Dictionary<string, string> EncodeSimpleData()
        {
            var dicitonary = new Dictionary<string, string>();
            ConvertTextData(Data, (k, v) => dicitonary[k] = v);
            return dicitonary;
        }

        private void ConvertTextData(JSONNode data, Block<string, string> convert)
        {
            foreach (KeyValuePair<string, JSONNode> kvp in data)
            {
                if (kvp.Value.IsArray)
                {
                    for (int i = 0; i < kvp.Value.AsArray.Count; ++i)
                    {
                        string arrayKey = string.Format("{0}[{1}]", kvp.Key, i);
                        string value = kvp.Value[i];
                        convert.SafeInvoke(arrayKey, value);
                    }
                }
                else
                {
                    string value = kvp.Value.IsObject ? kvp.Value.ToString().Replace("\\\"", "\"") : kvp.Value.Value;
                    convert.SafeInvoke(kvp.Key, value);
                }
            }
        }

    }
}
