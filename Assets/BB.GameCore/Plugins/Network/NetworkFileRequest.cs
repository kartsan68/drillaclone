﻿using System;
using System.Collections;
using System.IO;
using UnityEngine.Networking;

namespace BlackBears.GameCore.Networking
{
    public class NetworkFileRequest : NetworkRequest
    {

        private Block<string> onSuccess;
        private string fileNameTemplate;

        private int tempFileNumber;
        private string tempFileName;

        internal NetworkFileRequest(string fileNameTemplate, string urlString, Block<string> onSuccess, FailBlock onFail) : base(null)
        {
            url = urlString;
            this.fileNameTemplate = fileNameTemplate;

            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }

        protected override void Prepare()
        {
            base.Prepare();

            tempFileName = string.Format("{0}/{1}", BBGC.TemporaryDirectory, GenerateFileName());
            if (File.Exists(tempFileName)) File.Delete(tempFileName);

            request = UnityWebRequest.Get(url);

            request.downloadHandler = new DownloadHandlerFile(tempFileName);
            PrepareFinished(tempFileName);
        }

        public override IEnumerator Start()
        {
            if (!Prepared) Prepare();

            request.timeout = 5;
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                ProcessError(request.responseCode, request.error);
            }
            else
            {
                if (request.downloadHandler.isDone)
                {
                    DownloadFinished();
                    onSuccess.SafeInvoke(tempFileName);
                }
                else
                {
                    ProcessError(-1, "Something went wrong with file-request: " + url);
                }
            }
        }

        protected override void ProcessError(long code, string message)
        {
            if (File.Exists(tempFileName)) File.Delete(tempFileName);

            base.ProcessError(code, message);
        }

        protected virtual string GenerateFileName()
        {
            return string.Format(fileNameTemplate, ++tempFileNumber);
        }

        protected virtual void PrepareFinished(string filename) {}
        protected virtual void DownloadFinished() {}

    }
}
