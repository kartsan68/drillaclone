using BlackBears.GameCore.Configurations;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.GameCore.Views
{
    [RequireComponent(typeof(Image))]
    internal class SpritePackView : MonoBehaviour
    {

        [SerializeField] private bool bigImage;

        private Image image;

        internal void SetSpritePack(SpritePack pack)
        {
            if (image == null) image = GetComponent<Image>();
            
            if (pack == null) image.sprite = null;
            else image.sprite = bigImage ? pack.BigIcon : pack.SmallIcon;
        }

    }
}