﻿using UnityEngine;

namespace BB.GameCore.Plugins
{

    public class AndroidCopyFilesAndDirs : ScriptableObject
    {

        public const string configPath = "Assets/Editor/AndroidCopy.asset";
		public const string internalConfigPath = "Assets/BB.GameCore/Editor/AndroidCopy.asset";

        public CopyData[] data = new CopyData[]
		{
			new CopyData { from = "Plugins/Android/google-services.json", to = "google-services.json" }
		};

		[System.Serializable]
		public class CopyData
		{
			public string from;
			public string to;
		}

    }

}