namespace BlackBears.GameCore.Exceptions
{

    [System.Serializable]
    public class NoGameCoreConfigException : System.Exception
    {
        public NoGameCoreConfigException(string message) : base(message) { }
    }

    [System.Serializable]
    public class FeatureNotConnectedException : System.Exception
    {
        public FeatureNotConnectedException(string message) : base(message) { }
    }

}