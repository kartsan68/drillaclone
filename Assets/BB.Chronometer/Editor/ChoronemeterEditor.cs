﻿using UnityEditor;
using UnityEngine;

namespace BlackBears.Chronometer
{

    public static class ChoronemeterEditor
    {

        [UnityEditor.MenuItem("BlackBears/Create/Chronometer Config")]
        private static void CreateChronometerConfig()
        {
            var asset = ScriptableObject.CreateInstance<TimeModuleParams>();
            string folderPath = "Assets/Resources/";
            System.IO.Directory.CreateDirectory(folderPath);
            AssetDatabase.CreateAsset(asset, folderPath + "Chronometer.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }

    }

}