namespace BlackBears.Chronometer
{

    [System.Serializable]
    public class GameSpeedHackedException : System.Exception
    {
        public GameSpeedHackedException() { }
        public GameSpeedHackedException(string message) : base(message) { }
        public GameSpeedHackedException(string message, System.Exception inner) : base(message, inner) { }
        protected GameSpeedHackedException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

}