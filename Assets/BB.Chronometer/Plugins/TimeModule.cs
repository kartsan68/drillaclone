﻿using System;
using BlackBears.Utils;
using SimpleJSON;
using UnityEngine;
using System.Runtime.InteropServices;

using UnityObject = UnityEngine.Object;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Alerts;

namespace BlackBears.Chronometer
{

    //TODO: Перенести в Feature
    public sealed class TimeModule
    {

        private const string oldSessionsOnlineTimeKey = "ldsnsnntn";
        private const string sessionTimeKey = "ssntm";
        private const string lastSaveTimeKey = "ltsvtm";
        private const string lastSaveTimeSinceSystemStartupKey = "ltsvtmsss";
        private const string totalTimeKey = "tt";
        private const string startOfflineTimeKey = "stofft";

        internal static TimeModule Instance { get; private set; }

        public static bool checkEnabled = true;

        public static bool IsInitiated { get { return Instance != null; } }
        public static bool IsOffline => Instance.moduleParams.WorkWithoutInternet;

        public static bool IsValid { get { return IsInitiated && Instance.valid; } }

        //Время с первого запуска игры (оффлайн тоже учитывается)
        public static long TotalTime
        {
            get
            {
                if (Instance == null) return 0L;
                Instance.CalculateTotalTime();
                return Instance.totalTime.Value;
            }
        }

        public static long ActualTime
        {
            get
            {
                if (IsValid) return Instance.CalculateActualTime();
                return DateTime.UtcNow.UnixTimestamp();
            }
        }

        public static long ServerTime
        {
            get
            {
                if (Instance == null) return 0L;
                return Instance.serverTime;
            }
        }

        public static long SpendedTime
        {
            get
            {
                if (Instance == null) return 0L;
                return Instance.spendedTime;
            }
        }

        public static long SessionTime
        {
            get
            {
                if (Instance == null) return 0L;
                return Instance.sessionTime;
            }
        }

        public static long AfterResumeTime
        {
            get
            {
                if (Instance == null) return 0L;
                return Instance.afterResumeTime;
            }
        }

        public static long OnlineTime
        {
            get
            {
                if (Instance == null) return 0L;
                return Instance.CalculateOnlineTime();
            }
        }

        //Время оффлайна
        public static long TimeFromLastSave
        {
            get
            {
                if (Instance == null) return 0L;

                if (IsValid && (Instance.lastSaveTime != default(PLong)))
                {
                    return TimeModule.ActualTime - Instance.lastSaveTime;
                }
                else
                {
                    if (Instance.lastSaveTimeSinceSystemStartup == default(PLong)) return 0;

                    long difference = TimeSinceSystemStartup - Instance.lastSaveTimeSinceSystemStartup;
                    return difference > 0 ? difference : TimeSinceSystemStartup;
                }
            }
        }

        public static long LastSaveTime
        {
            get { return Instance != null ? Instance.lastSaveTime.Value : 0L; }
        }

        public static event ChronometerEventHandler OnChronometerUpdate
        {
            add
            {
                if (Instance == null) PrintInitFirstMessage();
                else Instance.onChronometerUpdate += value;
            }
            remove
            {
                if (Instance == null) PrintInitFirstMessage();
                else Instance.onChronometerUpdate -= value;
            }
        }

        public static long TimeSinceSystemStartup
        {
            get
            {

#if UNITY_EDITOR
                return DateTime.UtcNow.UnixTimestamp();
#elif UNITY_ANDROID
                using (AndroidJavaClass systemClock = new AndroidJavaClass("android.os.SystemClock"))
                {
                    return systemClock.CallStatic<long>("elapsedRealtime") / 1000;                   
                } 
#elif UNITY_IOS
                return GetUptime();
#endif
            }
        }

#if !UNITY_EDITOR && UNITY_IOS
        [DllImport ("__Internal")] private static extern long GetIOSUptime();
        private static long GetUptime()
        {
            return GetIOSUptime();
        }
#endif

        public static void Init(TimeModuleParams timeParams)
        {
            if (Instance != null) Debug.LogWarning("Повторная попытка инициализировать Хронометр. Не надо так.");
            else Instance = new TimeModule(timeParams);
        }

        public static void Unload()
        {
            if (Instance != null) Instance.Dispose();
            Instance = null;
        }

        public static void Validate()
        {
            if (Instance == null) PrintInitFirstMessage();
            else Instance.ValidateTime();
        }

        public static Action<bool> RequestPauseHandler()
        {
            if (Instance == null)
            {
                PrintInitFirstMessage();
                return null;
            }
            else return Instance.RequestPauseHandlerInternal();
        }

        private static void PrintInitFirstMessage()
        {
            Debug.LogWarning("Перед работой с хронометром вызови 'TimeModule.Init()'");
        }

        private bool valid;
        private long serverTime;
        private long spendedTime;
        private long sessionTime;
        private long afterResumeTime;
        private PLong totalTime = new PLong(0);
        private bool totalTimeCalculated = false;
        private long startOfflineTime = -1;

        private PLong oldSessionsOnlineTime;
        private PLong lastSaveTime = new PLong(0);
        private PLong lastSaveTimeSinceSystemStartup = new PLong(0);

        private float timer = 0f;
        private float checkTimer = 0f;
        private DateTime checkDate;
        private bool resetCheck = true;
        private ChronometerBehaviour chronometer;
        private TimeModuleParams moduleParams;

        private Block checkOfflineOnSuccess;

        event ChronometerEventHandler onChronometerUpdate;

        TimeModule(TimeModuleParams moduleParams)
        {
            Instance = this;
            this.moduleParams = moduleParams;

            var go = new GameObject("~~Chronometer Feature");
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityObject.DontDestroyOnLoad(go);
#endif

            go.SetActive(false);
            var behaviour = go.AddComponent<ChronometerBehaviour>();
            behaviour.RegisterParams(moduleParams);
            go.SetActive(true);
            // startOfflineTime = Convert.ToInt64(PlayerPrefs.GetString(startOfflineTimeKey, Convert.ToString(DateTime.UtcNow.UnixTimestamp())));

        }

        public void ValidateTime()
        {
            valid = false;
            if (chronometer != null) chronometer.RequestServerTime();
            else
            {
                Debug.LogWarning("Внутренняя ошибка - сообщите разработчикам");
                if (onChronometerUpdate != null)
                    onChronometerUpdate(this, new ChronometerEventArgs(ChronometerEventType.InternalError));
            }
        }

        public JSONNode ToJson()
        {
            var json = new JSONObject();
            json[oldSessionsOnlineTimeKey] = new PLong(oldSessionsOnlineTime).ToString();
            json[sessionTimeKey] = new PLong(sessionTime).ToString();

            bool internetIsActive = Application.internetReachability != NetworkReachability.NotReachable;
            lastSaveTime = CalculateActualTime();
            lastSaveTimeSinceSystemStartup = TimeSinceSystemStartup;
            json[lastSaveTimeKey] = IsValid && internetIsActive ? lastSaveTime.ToString() : "NULL";
            json[lastSaveTimeSinceSystemStartupKey] = new PLong(TimeSinceSystemStartup).ToString();
            json[totalTimeKey] = totalTime.ToString();

            json[startOfflineTimeKey] = startOfflineTime;

            return json;
        }

        public void FromJson(JSONNode json)
        {
            if (json == null || json.Tag == JSONNodeType.None) return;
            totalTime = json[totalTimeKey] != null ? PLong.Parse(json[totalTimeKey].Value) : new PLong(0);
            oldSessionsOnlineTime = PLong.Parse(json[oldSessionsOnlineTimeKey].Value);
            lastSaveTime = json[lastSaveTimeKey] != null ?
                (json[lastSaveTimeKey].Value.Contains("NULL") ? default(PLong) : PLong.Parse(json[lastSaveTimeKey].Value)) : new PLong(0);
            lastSaveTimeSinceSystemStartup = json[lastSaveTimeSinceSystemStartupKey] != null ?
                PLong.Parse(json[lastSaveTimeSinceSystemStartupKey].Value) : new PLong(0);

            var sessionNode = json[sessionTimeKey];
            if (sessionNode.Tag != JSONNodeType.None) sessionTime = PLong.Parse(json[sessionTimeKey].Value);

            totalTimeCalculated = false;
            CalculateTotalTime();

            startOfflineTime = json[startOfflineTimeKey] != null ? json[startOfflineTimeKey].AsLong : TotalTime;
        }

        internal void CheckOffline(Block onSuccess)
        {
            if (valid)
            {
                startOfflineTime = TotalTime;
                onSuccess.SafeInvoke();
                checkOfflineOnSuccess = null;
                return;
            }

            if (startOfflineTime == -1 ? false : 
                (moduleParams.MaxOfflineTime != 0 ? moduleParams.MaxOfflineTime <= (TotalTime - startOfflineTime) : false))
            {
                OnChronometerUpdate += OnChronometerUpdateCheckOffline;
                checkOfflineOnSuccess = onSuccess;

                var alerts = BBGC.Features.Alerts();
                var loc = BBGC.Features.Localization();

                alerts.AddAlert(new ErrorAlert(
                    loc.Get("BBGC_INIT_ERROR_TITLE"),
                    loc.Get("BBGC_INIT_ERROR_DESC"),
                    loc.Get("BBGC_INIT_ERROR_BTN"),
                    () => ValidateTime()
                ));
            }
            else
            {
                onSuccess.SafeInvoke();
                checkOfflineOnSuccess = null;
            }

        }

        private void OnChronometerUpdateCheckOffline(object sender, ChronometerEventArgs args)
        {
            OnChronometerUpdate -= OnChronometerUpdateCheckOffline;

            CheckOffline(checkOfflineOnSuccess);
        }
        

        public long CalculateOnlineTime()
        {
            return oldSessionsOnlineTime + sessionTime;
        }

        private long CalculateActualTime()
        {
            return serverTime + spendedTime;
        }

        internal void Dispose()
        {
            if (chronometer != null) UnityObject.Destroy(chronometer.gameObject);
        }

        internal bool RegisterChronometer(ChronometerBehaviour inChronometer)
        {
            if (chronometer != null || inChronometer == null) return false;
            chronometer = inChronometer;
            return true;
        }

        internal void ServerNotAvailable(ChronometerBehaviour inChronometer, string message)
        {
            if (inChronometer != chronometer) return;

            resetCheck = true;
            valid = false;
            serverTime = 0L;
            timer = 0f;
            spendedTime = 0L;

            if (onChronometerUpdate != null)
            {
                onChronometerUpdate(this,
                    moduleParams.WorkWithoutInternet ?
                        new ChronometerEventArgs(ChronometerEventType.ValidatedOffline) :
                        new ChronometerEventArgs(ChronometerEventType.ServerNotAvailable));
            }
        }

        internal void SetServerTime(ChronometerBehaviour inChronometer, long time)
        {
            if (inChronometer != chronometer) return;
            serverTime = time;
            spendedTime = 0L;
            timer = 0f;
            resetCheck = true;
            valid = true;

            if (onChronometerUpdate != null)
                onChronometerUpdate(this, new ChronometerEventArgs(ChronometerEventType.Validated));
        }

        internal void ServerAnswerNotCorrect(ChronometerBehaviour inChronometer)
        {
            if (inChronometer != chronometer) return;

            resetCheck = true;
            valid = false;
            serverTime = 0L;
            timer = 0f;
            spendedTime = 0L;

            if (onChronometerUpdate != null)
            {
                onChronometerUpdate(this,
                    moduleParams.WorkWithoutInternet ?
                        new ChronometerEventArgs(ChronometerEventType.ValidatedOffline) :
                        new ChronometerEventArgs(ChronometerEventType.ServerAnswerNotCorrect));
            }
        }

        internal void Invalidate(ChronometerBehaviour inChronometer)
        {
            if (inChronometer != chronometer) return;
            valid = false;
            serverTime = 0L;
            if (onChronometerUpdate != null)
                onChronometerUpdate(this, new ChronometerEventArgs(ChronometerEventType.Invalidated));
        }

        internal void Tick(ChronometerBehaviour chronometer, float dt, bool isOfflineTick = false)
        {
            if (this.chronometer != chronometer) return;

            timer += dt;
            if (timer >= 1f)
            {
                if (timer < 2f)
                {
                    spendedTime += 1L;
                    sessionTime += 1L;
                    afterResumeTime += 1L;
                    timer -= 1f;
                    totalTime += 1L;
                }
                else
                {
                    long fullSeconds = (long)timer;
                    timer -= fullSeconds;
                    spendedTime += fullSeconds;
                    sessionTime += fullSeconds;
                    afterResumeTime += fullSeconds;
                    totalTime += fullSeconds;
                }
            }

            CheckTick(dt, isOfflineTick);
        }

        internal void ResetCheck(ChronometerBehaviour chronometer)
        {
            if (this.chronometer != chronometer) return;
            resetCheck = true;
        }

        internal void ResetAfterResumeTime()
        {
            afterResumeTime = 0;
        }

        internal void ResetSessionTime()
        {
            oldSessionsOnlineTime = CalculateOnlineTime();
            sessionTime = 0;
        }

        private void CalculateTotalTime()
        {
            if (Instance.totalTimeCalculated) return;
            totalTimeCalculated = true;
            totalTime += TimeFromLastSave;
        }

        private void CheckTick(float dt, bool isOfflineTick)
        {
            if (resetCheck || isOfflineTick)
            {
                checkTimer = 0f;
                checkDate = DateTime.UtcNow;
                resetCheck = false;
            }

            if (!isOfflineTick) checkTimer += dt;
            if (checkTimer < 60 || isOfflineTick) return;

            resetCheck = true;

#if UNITY_EDITOR
            return;
#endif
            
            if (!checkEnabled) return;

            var currentDate = System.DateTime.UtcNow;
            var difference = (currentDate - checkDate).TotalSeconds;

            if (difference < 30)
            {
#if BBGC_USE_FABRIC
                Fabric.Crashlytics.Crashlytics.RecordCustomException("GameSpeedHackedException", 
                    "Accelerated the ticks via GameGuardian or other apps", "Chronometer -> TimeModule -> CheckTick()");
#endif

#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
                // #if UNITY_EDITOR
                //                 Debug.LogError(string.Format("Player try to use speed hack. CurD: {0}; ChD: {1}; Diff: {2}",
                //                     currentDate, checkDate, difference));
                //                 UnityEditor.EditorApplication.isPlaying = false;
                // #elif UNITY_ANDROID
                //                 var ec = new AndroidJavaClass("mobi.blackbears.unity.GameSpeedHackedException");

                //                 ec.CallStatic("outerInvoke", 
                //                     string.Format("Player try to use speed hack. CurD: {0}; ChD: {1}; Diff: {2}", 
                //                         currentDate, checkDate, difference));
                // #elif UNITY_IOS
                //                 throw new GameSpeedHackedException(
                //                     string.Format("Player try to use speed hack. CurD: {0}; ChD: {1}; Diff: {2}", 
                //                         currentDate, checkDate, difference));
                // #endif
                return;
            }
        }

        private Action<bool> RequestPauseHandlerInternal()
        {
            if (chronometer == null) return null;
            if (chronometer.PauseFromExternal) return null;
            Action<bool> res = chronometer.OnExternalPause;
            chronometer.PauseFromExternal = true;
            return res;
        }

    }

}