using System;

namespace BlackBears.Chronometer
{

    public delegate void ChronometerEventHandler(object sender, ChronometerEventArgs args);


    public enum ChronometerEventType
    {
        InternalError,
        Invalidated,
        Validated,
        ServerNotAvailable,
        ServerAnswerNotCorrect,
        ValidatedOffline,
        
    }

    public class ChronometerEventArgs : EventArgs
    {

        public readonly ChronometerEventType eventType;
        public readonly string message;

        internal ChronometerEventArgs(ChronometerEventType eventType, string message = null)
        {
            this.eventType = eventType;
            this.message = message ?? eventType.ToString();
        }

    }

}