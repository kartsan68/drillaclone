using UnityEngine;

namespace BlackBears.Chronometer
{

    [CreateAssetMenu(fileName = "TimeModuleParams", menuName = "BlackBears/TimeModule Params", order = 2)]
    public class TimeModuleParams : ScriptableObject
    {

        [SerializeField] private string url = "http://sqba-mini.qr4.ru/api/gettime";
        [SerializeField] private string saltKey = "rand_key";
        [SerializeField] private string apiKey = "zWVan2RXWJJbWZMs";
        [SerializeField] private long maxOfflinePauseTime = 120;
        [SerializeField] private long maxOfflineTime = 172800;
        [SerializeField] private bool workWithoutInternet = true;

        internal string Url { get { return url; } }
        internal string SaltKey { get { return saltKey; } }
        internal string ApiKey { get { return apiKey; } }
        internal long MaxOfflinePauseTime { get { return maxOfflinePauseTime; } }
        internal long MaxOfflineTime { get { return maxOfflineTime; } }
        internal bool WorkWithoutInternet { get { return workWithoutInternet; } }

    }

}