@interface iOSTimeUtils : NSObject

#ifdef __cplusplus
extern "C" {
#endif
    
    long GetIOSUptime();
    
#ifdef __cplusplus
}
#endif

@end
