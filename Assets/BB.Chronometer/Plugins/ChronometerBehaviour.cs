using System;
using System.Collections;
using System.Text;
using BlackBears.Utils;
using UnityEngine;
using UnityEngine.Networking;

using Random = UnityEngine.Random;

namespace BlackBears.Chronometer
{

    internal sealed class ChronometerBehaviour : MonoBehaviour
    {

        private const string internalErrorText = "Internal error";

        private bool awakeCalled = false;
        private TimeModuleParams moduleParams;
        private long pauseTime;

        internal bool PauseFromExternal { get; set; }

        private void Awake()
        {
            if (!Register())
            {
                Destroy(this);
                return;
            }
            if (moduleParams == null)
                RegisterParams(Resources.Load<TimeModuleParams>("Chronometer"));

            awakeCalled = true;
        }

        internal bool RegisterParams(TimeModuleParams moduleParams)
        {
            if (awakeCalled || this.moduleParams != null || moduleParams == null)
            {
                return false;
            }
            else
            {
                this.moduleParams = moduleParams;
                return true;
            }
        }

        internal void RequestServerTime()
        {
            if (moduleParams == null)
            {
                Debug.LogWarning("Не установлен TimeModuleParams");
                TimeModule.Instance.ServerNotAvailable(this, internalErrorText);
                return;
            }
            StartCoroutine(RequestServerTimeAsync());
        }

        internal void OnExternalPause(bool paused)
        {
            if (PauseFromExternal) OnPause(paused);
        }

        private void Update()
        {
            TimeModule.Instance.Tick(this, Time.deltaTime);
        }

        private void OnApplicationPause(bool paused)
        {
            if (!PauseFromExternal) OnPause(paused);
        }

        private void OnPause(bool paused)
        {
            if (paused)
            {
                pauseTime = TimeModule.TimeSinceSystemStartup;
            }
            else
            {
                long unpauseTime = TimeModule.TimeSinceSystemStartup;
                long offlineTime = unpauseTime - pauseTime;
                TimeModule.Instance.ResetCheck(this);
                TimeModule.Instance.ResetAfterResumeTime();
                if (offlineTime < 0)
                {
                    TimeModule.Instance.Invalidate(this);
                    if(moduleParams.WorkWithoutInternet)
                    {
                        TimeModule.Instance.Tick(this, offlineTime, true);
                        TimeModule.Validate();
                    }
                }
                else if (offlineTime >= moduleParams.MaxOfflinePauseTime)
                {
                    TimeModule.Instance.Invalidate(this);
                    if(moduleParams.WorkWithoutInternet)
                    { 
                        TimeModule.Instance.Tick(this, offlineTime, true);
                        TimeModule.Validate();
                    }
                }
                else
                {
                    TimeModule.Instance.Tick(this, offlineTime, true);
                }
            }
        }

        private bool Register()
        {
            if (TimeModule.Instance == null) return false;
            return TimeModule.Instance.RegisterChronometer(this);
        }

        private IEnumerator RequestServerTimeAsync()
        {
            string salt = Random.Range(0, int.MaxValue).ToString();
            string url = string.Format("{0}?{1}={2}", moduleParams.Url, moduleParams.SaltKey, salt);

            var request = UnityWebRequest.Get(url);
            request.timeout = 5;
            yield return request.SendWebRequest();

            if (request.isNetworkError)
            {
                TimeModule.Instance.ServerNotAvailable(this, request.error);
                yield break;
            }
            ServerAnswer answer = JsonUtility.FromJson<ServerAnswer>(request.downloadHandler.text) as ServerAnswer;

            if (IsValidAnswer(answer, salt))
            {
                TimeModule.Instance.SetServerTime(this, answer.response.Time);
            }
            else
            {
                TimeModule.Instance.ServerAnswerNotCorrect(this);
            }

        }

        private bool IsValidAnswer(ServerAnswer answer, string salt)
        {
            if (answer == null || answer.response == null) return false;
            string inKey = string.Format("{0}{1}{2}", answer.response.time, salt, moduleParams.ApiKey);
            inKey = GenerateMD5(inKey);
            string outKey = answer.response.out_key;

            if (string.Equals(inKey, outKey, StringComparison.InvariantCultureIgnoreCase))
                return answer.response.IsTimeValid;
            else
                return false;

        }

        private string GenerateMD5(string data)
        {
            var bytes = Encoding.UTF8.GetBytes(data);
            var md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(bytes);

            var output = new StringBuilder();
            for (int i = 0; i < hash.Length; ++i) output.Append(Convert.ToString(hash[i], 16).PadLeft(2, '0'));

            return output.ToString().PadLeft(32, '0');
        }

    }

}