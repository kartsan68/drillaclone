namespace BlackBears.Chronometer
{

    [System.Serializable]
    internal class ServerAnswer
    {

        [UnityEngine.SerializeField] internal string action = null;
        [UnityEngine.SerializeField] internal int error_code = 0;

        [UnityEngine.SerializeField] internal Response response = null;

        [System.Serializable]
        internal class Response
        {

            [UnityEngine.SerializeField] internal string time = null;
            [UnityEngine.SerializeField] internal string out_key = null;

            internal bool IsTimeValid
            {
                get
                {
                    if (string.IsNullOrEmpty(time)) return false;
                    long result;
                    return long.TryParse(time, out result);
                }
            }

            internal long Time { get { return long.Parse(time); } }

        }

    }

}