﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;

public class ServiceCanvasController : MonoBehaviour
{
    [SerializeField] private RectTransform alerts;
    [SerializeField] private NotificationController notificationController;

    public RectTransform Alerts => alerts;
    public NotificationController NotificationController => notificationController;

}
