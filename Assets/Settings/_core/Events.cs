public enum InitGameState
{
    Undefined = 0,
    Success = 1,
    BundlerCacheError
}

public class InitializationStep
{

    public readonly InitGameState state;

    public bool IsSuccess => state == InitGameState.Success;
    public bool IsFail => !IsSuccess;

    public InitializationStep(InitGameState state)
    {
        this.state = state;
    }

}
