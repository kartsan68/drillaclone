using System;
using BlackBears;
using BlackBears.Bundler;
using BlackBears.Chronometer;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.DiggIsBig.Configuration;
using UniRx;


public class InitializationController
{
    private bool isSaveCheckEnabled = true;
    private bool isBundlerCheckEnabled = true;
    private bool isCacheCheckEnabled = true;
    private bool isConfigCheckEnabled = true;

    private bool isOnCoreInitialization;
    private bool isOnGameInitialization;

    private Subject<Null> onCoreInitialized = new Subject<Null>();
    private Subject<InitializationStep> onGameInitializeStepChanged = new Subject<InitializationStep>();

    public GameConfigurationManager Configuration { get; private set; }
    // public GameResourceCache GameCache { get; private set; }
    // public BundlerManager Bundler { get; private set; }
    // public SaveManager SaveManager { get; private set; }

    public bool IsOnInitialization => isOnCoreInitialization || isOnGameInitialization;

    public IObservable<Null> OnCoreInitialized => onCoreInitialized;
    public IObservable<InitializationStep> OnGameInitializeStepChanged => onGameInitializeStepChanged;

    public bool IsConfigCheckEnabled
    {
        get { return isConfigCheckEnabled; }
        set
        {
            if (IsOnInitialization) return;
            isConfigCheckEnabled = value;
        }
    }

    public bool IsBundlerCheckEnabled
    {
        get { return isBundlerCheckEnabled; }
        set
        {
            if (IsOnInitialization) return;
            isBundlerCheckEnabled = value;
        }
    }

    public bool IsCacheCheckEnabled
    {
        get { return isCacheCheckEnabled; }
        set
        {
            if (IsOnInitialization) return;
            isCacheCheckEnabled = value;
        }
    }

    public bool IsSaveCheckEnabled
    {
        get { return isSaveCheckEnabled; }
        set
        {
            if (IsOnInitialization) return;
            isSaveCheckEnabled = value;
        }
    }

    public bool IsConfigInitialized => !IsConfigCheckEnabled || Configuration != null;

    // public bool IsBundlerInitialized 
    //     => !IsBundlerCheckEnabled || (Bundler != null && Bundler.CachePrepared);

    // public bool IsCacheInitialized
    //     => !IsCacheCheckEnabled || (GameCache != null && GameCache.State == CacheState.Initialized);

    public bool IsChronometerInitialized => TimeModule.IsValid;
    public bool IsSaveManagerInitialized => !IsSaveCheckEnabled;// || SaveManager != null;

    public void InitializeCore(FeaturesInitData initData)
    {
        if (IsOnInitialization) return;
        isOnCoreInitialization = true;
        BBGC.Initialize(initData, CoreInitialized);
    }
    public void InitializeGame()
    {
        if (IsOnInitialization) return;
        isOnGameInitialization = true;
        CheckNextInitializeStep();
    }

    private void CheckNextInitializeStep()
    {
        // if (!IsConfigInitialized) InitializeConfiguration();
        // if (!IsSaveManagerInitialized) InitializeSaveManager();
        // else if (!IsCacheInitialized) InitializeGameCache();
        // else if (!IsBundlerInitialized) InitializeBundler();
        onGameInitializeStepChanged.OnNext(new InitializationStep(InitGameState.Success));
    }

    private void CoreInitialized()
    {
        isOnCoreInitialization = false;
        onCoreInitialized.OnNext(null);
    }

    // private void InitializeConfiguration()
    // {
    //     var config = BBGC.Features.SQBAFiles().TextByName("defaults.json");
    //     Configuration = new GameConfiguration(config);
    //     CheckNextInitializeStep();
    // }

    private void InitializeSaveManager()
    {
        // SaveManager = new SaveManager();
        // CheckNextInitializeStep();
    }

    // private void InitializeGameCache()
    // {
    //     GameCache = UObject.FindObjectOfType<GameResourceCache>();
    //     CheckNextInitializeStep();
    // }

    // private void InitializeBundler()
    // {
    //     Bundler = UObject.FindObjectOfType<BundlerManager>();
    //     Bundler.OnCachingStateChanged += OnBundlerStateChanged;
    //     Bundler.PrepareCache();
    // }

    // private void OnBundlerStateChanged(object sender, BundlerEventArgs args)
    // {
    //     if (args.HasError)
    //     {
    //         Bundler.OnCachingStateChanged -= OnBundlerStateChanged;
    //         onGameInitializeStepChanged.OnNext(new InitializationStep(InitGameState.BundlerCacheError));
    //     }
    //     else if (args.IsFinished)
    //     {
    //         Bundler.OnCachingStateChanged -= OnBundlerStateChanged;
    //         CheckNextInitializeStep();
    //     }
    // }

}
