using System;

namespace BlackBears.GameCore.Features.LocalNotifications.Unity
{
	public class UnityGameNotification : IGameNotification
	{
		public int? Id { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public string Subtitle { get; set; }
		public string Group { get; set; }
		public int? BadgeNumber { get; set; }
		public DateTime? DeliveryTime { get; set; }
		public bool Scheduled { get; }
		public string SmallIcon { get; set; }
		public string LargeIcon { get; set; }
	}
}