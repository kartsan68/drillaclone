using System;

namespace BlackBears.GameCore.Features.LocalNotifications.Unity
{
	public class UnityNotificationPlatform : IGameNotificationsPlatform<UnityGameNotification>
	{
		public event Action<IGameNotification> NotificationReceived;
		IGameNotification IGameNotificationsPlatform.CreateNotification()
		{
			return CreateNotification();
		}

		public void ScheduleNotification(UnityGameNotification notification)
		{
		}

		public UnityGameNotification CreateNotification()
		{
			return new UnityGameNotification();
		}

		public void ScheduleNotification(IGameNotification gameNotification)
		{
		}

		public void CancelNotification(int notificationId)
		{
		}

		public void DismissNotification(int notificationId)
		{
		}

		public void CancelAllScheduledNotifications()
		{
		}

		public void DismissAllDisplayedNotifications()
		{
		}

		public void OnForeground()
		{
		}

		public void OnBackground()
		{
		}
	}
}