using System;
using BlackBears.GameCore.Features.Localization;
using BlackBears.Utils;
using UnityEngine;

namespace BlackBears.DiggIsBig.Formatter
{
    public class DrillFormatter
    {
        public string MoneyKFormatter(double money)
        {
            return AmountUtils.ToPrettyString(money, false, "{0:0.##}{1}");
            // if (money >= 1000 && money < 1000000)
            // {
            //     double res = (double)money / 1000;
            //     if ((res - (long)res) != 0)
            //     {
            //         return ((double)((long)(res * 100)) / 100).ToString() + "k";
            //     }
            //     else
            //         return ((double)money / 1000).ToString() + "k";
            // }
            // else if (money >= 1000000 && money < 1000000000)
            // {
            //     double res = (double)money / 1000000;
            //     if ((res - (long)res) != 0)
            //         return ((double)((long)(res * 100)) / 100).ToString() + "m";
            //     else
            //         return ((double)money / 1000000).ToString() + "m";
            // }
            // else if (money >= 1000000000 && money < 1000000000000000)
            // {
            //     double res = (double)money / 1000000000;
            //     if ((res - (long)res) != 0)
            //         return ((double)((long)(res * 100)) / 100).ToString() + "t";
            //     else
            //         return ((double)money / 1000000000).ToString() + "t";
            // }
            // else
            // {
            //     return money.ToString();
            // }
        }

        public string TimeFormatter(double second)
        {
            second = Mathf.Ceil((float)second);

            var sec = (second % 60).ToString();
            if (sec.Length == 1)
            {
                string otherNum = "0" + sec;
                sec = otherNum;
            }

            var min = Mathf.Floor((float)second / 60 % 60).ToString();
            if (min.Length == 1)
            {
                string otherNum = "0" + min;
                min = otherNum;
            }

            var hours = Mathf.Floor((float)second / 60 / 60 % 60).ToString();
            if (hours.Length == 1)
            {
                string otherNum = "0" + hours;
                hours = otherNum;
            }

            return hours + ":" + min + ":" + sec;
        }

        public string TimeFormatterForBtn(float second)
        {
            second = Mathf.Ceil(second);

            var sec = (second % 60).ToString();

            sec += " " + DigIsBigLocalizator.GetLocalizationString("TIME_SEC", Modificator.ToUpper) + " ";

            var min = Mathf.Floor(second / 60 % 60).ToString();
            min += " " + DigIsBigLocalizator.GetLocalizationString("TIME_MIN", Modificator.ToUpper) + " ";

            float hour = Mathf.Floor(second / 60 / 60 % 60);
            var hours = hour.ToString();

            if (hour <= 0) hours = "";
            else hours += " " + DigIsBigLocalizator.GetLocalizationString("TIME_HOUR", Modificator.ToUpper);

            return "-" + hours + min + sec;
        }

        public string SecondsToMin(float second) => "-" + (Mathf.Ceil(second / 60)) + " МИН";
    }
}