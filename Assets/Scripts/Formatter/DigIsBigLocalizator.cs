using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Localization;

namespace BlackBears.DiggIsBig.Formatter
{
    public static class DigIsBigLocalizator
    {
        public static string GetLocalizationString(string key, Modificator modificator = Modificator.DontModify) =>
         GameCore.BBGC.Features.Localization().Get(key, modificator);
    }
}