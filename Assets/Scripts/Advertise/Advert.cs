﻿using BlackBears.GameCore;
using BlackBears.GameCore.Features.Ads;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using BlackBears.DiggIsBig.Controllers;
using System;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Advertise
{
    public class Advert : MonoBehaviour
    {
        #region  Var
        [SerializeField] private GameController gameController;

        private GameConfigurationManager gameConfigurationManager;
        private int interstitialTimeDelay;
        private int interstitialStartDelay;
        private int rewardedDelay;
        private CountdownTimer interstitialTimer;
        private CountdownTimer startAdvProtectTimer;
        private CountdownTimer advDelayTimer;
        private CountupTimer lifeWitoutAdvertise;
        private bool interstitialIsReady;
        private bool subscribe;
        private bool startProtected;
        private bool _alreadyPressed = false;
        private AdsFeature adsFeature;
        private long sessionTime;
        #endregion

        #region  MonoBehaviour
        void Awake()
        {
            SubscribeToEvents();
            VarInit();
            startAdvProtectTimer.SetTimer(interstitialStartDelay, () => startProtected = false);
            lifeWitoutAdvertise.StartTimer(() => GameData.GameData.withoutAdv = lifeWitoutAdvertise.GetTime());
            LifeWithoutAdvTimer();
        }
        #endregion

        #region AdvertiseWork
        private void SubscribeToEvents()
        {
            EventManager.Subscribe += SomePremMethod;
            EventManager.CallInterstitialAdvert += ShowInterstitialAdvert;
            EventManager.CallRewardedAdvert += () => ShowRewardedAdvertWithCallBack(() => { });
            EventManager.CallRewardedAdvertWithAction += ShowRewardedAdvertWithCallBack;
        }

        private void LifeWithoutAdvTimer()
        {
            GameData.GameData.withoutAdv = 0;
            lifeWitoutAdvertise.RestartTimer();
        }

        private bool checkState = false;
        private float advUpgradeDelay = 5;
        private bool advUpgradeIsRedy = true;
        private void Update()
        {
            if (adsFeature.IsRewardedAdsAvailable == true /*&& checkState == false*/)
            {
                // checkState = true;
                EventManager.RewardedAdvertiseStatusEvent(adsFeature.IsRewardedAdsAvailable);
            }
            else if (/*checkState == true &&*/ adsFeature.IsRewardedAdsAvailable == false)
            {
                // checkState = false;
                EventManager.RewardedAdvertiseStatusEvent(adsFeature.IsRewardedAdsAvailable);
            }


            if (advUpgradeIsRedy && GameData.GameData.withoutAdv > withoutAdsTimeConditions &&
               ((GameData.GameData.theLastUpgradeOnlineTime > lastUpgradeOnlineTimeConditions &&
               GameData.GameData.firstNeverUpgrade) || GameData.GameData.theLastUpgradeTime >= lastUpgradeTimeConditions))
            {
                advUpgradeIsRedy = false;
                EventManager.CallAdvDetailEvent();
            }
            else if (advUpgradeDelay > GameData.GameData.theLastUpgradeTime && !advUpgradeIsRedy) advUpgradeIsRedy = true;
        }

        private float withoutAdsTimeConditions;
        private float lastUpgradeTimeConditions;
        private float lastUpgradeOnlineTimeConditions;

        private void VarInit()
        {
            sessionTime = Chronometer.TimeModule.SessionTime;
            adsFeature = BBGC.Features.GetFeature<AdsFeature>();
            interstitialTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject, TimerType.InterstitialSpawnTimer);
            interstitialTimer.SetUstopable(true);

            startAdvProtectTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            startAdvProtectTimer.SetUstopable(true);

            advDelayTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            advDelayTimer.SetUstopable(true);

            // EventManager.CallAdvDetailEvent()

            lifeWitoutAdvertise = TimeManager.GetEmptyCountupTimer(this.gameObject);

            interstitialIsReady = true;
            subscribe = false;
            startProtected = true;

            gameConfigurationManager = gameController.GetConfigurationManager();

            interstitialStartDelay = gameConfigurationManager.advertiseData.firstShowTime;
            interstitialTimeDelay = gameConfigurationManager.advertiseData.advInterstitialDelay;
            rewardedDelay = gameConfigurationManager.advertiseData.rewardedIdleTime;

            var conditionsData = gameConfigurationManager.conditionsData;
            withoutAdsTimeConditions = conditionsData.conditions[1].withoutAdsTime;
            lastUpgradeTimeConditions = conditionsData.conditions[1].lastUpgradeTime;
            lastUpgradeOnlineTimeConditions = conditionsData.conditions[1].firstUpOnline;
        }

        private void SomePremMethod(bool sub) => subscribe = sub;

        private void PreAdvertState()
        {
            AdjustEventManager.RewardedStartEvent();
            GameData.GameData.AdvertiseLoad = true;
#if UNITY_IOS
            Time.timeScale = 0;
#endif
        }

        private void PostAdvertState()
        {
            NotificationsManager.ClearAllNotif();
            GameData.GameData.AdvertiseLoad = false;
#if UNITY_IOS
            Time.timeScale = 1;
            EventManager.ClearChildEvent();
#endif
        }

        private void InterstitialAdsReady() => interstitialIsReady = true;

        private void RewardedAdvEnd()
        {
            interstitialIsReady = false;
            interstitialTimer.ResetTimer(interstitialTimeDelay, () => InterstitialAdsReady());
            EventManager.AdsWatchingEndEvent();
            EventManager.SendFBEvent(GameData.GameData.rewardedFBToken);
            AdjustEventManager.RewarderFinishEvent();
            PostAdvertState();
        }

        private void InterstitialAdvert()
        {
#if UNITY_EDITOR
            interstitialIsReady = false;
            LifeWithoutAdvTimer();
            interstitialTimer.ResetTimer(interstitialTimeDelay, () => InterstitialAdsReady());
#else
            interstitialIsReady = false;
            PreAdvertState();
            AdjustEventManager.InterstitialStartEvent();

            adsFeature.ShowInterstitial((p) => Debug.Log(""), _ =>
            {
                AdjustEventManager.InterstitialFinishEvent();
                EventManager.SendFBEvent(GameData.GameData.interstitialFBToken);
                PostAdvertState();
                interstitialTimer.ResetTimer(interstitialTimeDelay, () => InterstitialAdsReady());
            }, () => AdjustEventManager.InterstitialClickEvent(), (c, m) =>
            {
                PostAdvertState();
                interstitialTimer.ResetTimer(interstitialTimeDelay, () => InterstitialAdsReady());
            });
#endif
        }

        public void ShowRewardedAdvertWithCallBack(Action action)
        {
            if (!_alreadyPressed)
            {
#if UNITY_EDITOR
                action.Invoke();
                RewardedAdvEnd();
                LifeWithoutAdvTimer();
#else
            LifeWithoutAdvTimer();
            PreAdvertState();
            if (!subscribe)
            {
                _alreadyPressed = true;
                advDelayTimer.SetTimer(3, () => _alreadyPressed = false);
                adsFeature.ShowRewarded((p) => Debug.Log(" "), _ => { action.Invoke(); RewardedAdvEnd(); },
                 () => PostAdvertState(), () => AdjustEventManager.RewardedClickEvent(), (c, m) => PostAdvertState());
            }
            else
            {
                action.Invoke();
                RewardedAdvEnd();
            }
#endif
            }
        }

        public void ShowInterstitialAdvert()
        {
            if (interstitialIsReady && !subscribe && !startProtected && adsFeature.IsInterstitialAdsAvailable)
            {
                InterstitialAdvert();
            }
        }
        #endregion
    }
}