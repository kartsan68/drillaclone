﻿using BlackBears.DiggIsBig.Cave.Pools;
using UnityEngine;

namespace BlackBears.DiggIsBig.Stone
{

    public class StonePool : MonoBehaviourPool<StoneController>
    {
        private int createdCounter = 0;

        public StonePool(StoneController prefab, int count, Transform poolParent = null)
            : base(prefab, count, poolParent) { }

        protected override StoneController CreateItem(StoneController prototype)
        {
            var item = base.CreateItem(prototype);
            item.Pool = this;
            item.gameObject.name = $"stone_{++createdCounter}";
            return item;
        }
    }

}


