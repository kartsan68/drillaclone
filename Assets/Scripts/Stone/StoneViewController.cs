﻿using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Settings;
using UnityEngine;

namespace BlackBears.DiggIsBig.Stone
{

    public class StoneViewController : MonoBehaviour
    {
        [SerializeField] GameObject colorControlUnit;

        private SpriteRenderer colorControlUnitRenderer;
        private SpriteRenderer mainRenderer;

        private float currentAmplitude;
        private float moveSpeed;
        bool amplitudeReached;

        internal float Scale { get; set; }
        internal float RotationSpeed { get; set; }
        internal Vector2 ShakeAmplitude { get; set; }
        internal Vector2 ShakePeriod { get; set; }

        internal float Size
        {
            get
            {
                var spriteSize = GetComponent<SpriteRenderer>().size;
                var maxDimSize = Mathf.Max(spriteSize.x, spriteSize.y);

                return maxDimSize * Scale;
            }
        }

        private void Awake()
        {
            colorControlUnitRenderer = colorControlUnit.GetComponent<SpriteRenderer>();
            mainRenderer = GetComponent<SpriteRenderer>();
            CheckColor();
        }
        private void Start()
        {
            transform.localScale = new Vector3(Scale, Scale, transform.localScale.z);

            currentAmplitude = Random.Range(ShakeAmplitude.x, ShakeAmplitude.y);
            moveSpeed = currentAmplitude * 2 / Random.Range(ShakePeriod.x, ShakePeriod.y);
            amplitudeReached = false;
        }

        private void CheckColor()
        {
            if (colorControlUnitRenderer.color != mainRenderer.color)
                mainRenderer.color = colorControlUnitRenderer.color;
        }

        private void OnEnable() => CheckColor();

        private void Update()
        {
            if (!Application.isPlaying) transform.localScale = new Vector3(Scale, Scale, transform.localScale.z);

            CheckColor();

            transform.Rotate(0, 0, transform.localRotation.z + RotationSpeed * Time.deltaTime);

            var deltaY = Time.deltaTime * moveSpeed;
            var currentY = transform.localPosition.y;
            var newY = (amplitudeReached) ? currentY + deltaY : currentY - deltaY;

            if (newY <= -currentAmplitude)
            {
                newY = -currentAmplitude;
                amplitudeReached = true;
            }
            else if (newY >= 0)
            {
                newY = 0;

                currentAmplitude = Random.Range(ShakeAmplitude.x, ShakeAmplitude.y);
                moveSpeed = currentAmplitude * 2 / Random.Range(ShakePeriod.x, ShakePeriod.y);
                amplitudeReached = false;
            }

            transform.localPosition = new Vector3(transform.localPosition.x, newY, transform.localPosition.z);
        }

    }

}


