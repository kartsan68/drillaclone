﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Stone
{

    public class StoneEmitterController : MonoBehaviour
    {

        [SerializeField] private StoneController stonePrefab;
        [SerializeField] private BezierCurve[] curves;
        [SerializeField] private Vector2 speedRange;
        [SerializeField] private Vector2 emitIntervalRange;
        [SerializeField] private Vector2 scaleRange;
        [SerializeField] private Vector2 rotationSpeedRange;
        [SerializeField] private Vector2 shakeAmplitude;
        [SerializeField] private Vector2 shakePeriod;
        [SerializeField] private int stoneCount;

        private StonePool pool;
        private float timeElapsed = 0f;
        private float randomInterval;
        private float randomSpeed;
        private float randomScale;
        private float randomRotationSpeed;
        private bool destroyed;
        private float speedMultiplier = 1;

        private void Start()
        {
            Managers.EventManager.AnimationSpeedControl += (a) => speedMultiplier = a;
            pool = new StonePool(stonePrefab, stoneCount, transform);
            UpdateRandomFields();
        }

        private void OnDestroy()
        {
            try
            {
                if (destroyed) return;
                destroyed = true;

                pool.Dispose();
                pool = null;
            }
            catch (System.NullReferenceException e)
            {
                Debug.Log(e);
            }
        }

        private void Update()
        {
            if (pool == null || curves == null || curves.Length <= 0) return;

            timeElapsed += Time.deltaTime;
            if (timeElapsed > randomInterval)
            {
                var controller = pool.TakeItem();
                controller.Curve = curves[Random.Range(0, curves.Length)];
                controller.MoveFinished = false;
                controller.Speed = randomSpeed + (randomSpeed * (3 * speedMultiplier));
                controller.Scale = randomScale;
                controller.ShakeAmplitude = shakeAmplitude;
                controller.RotationSpeed = randomRotationSpeed;
                controller.ShakePeriod = shakePeriod;
                controller.gameObject.SetActive(true);

                timeElapsed = 0f;
                UpdateRandomFields();
            }
        }

        private void UpdateRandomFields()
        {
            randomInterval = Random.Range(emitIntervalRange.x, emitIntervalRange.y);
            randomSpeed = Random.Range(speedRange.x, speedRange.y);
            randomScale = Random.Range(scaleRange.x, scaleRange.y);
            randomRotationSpeed = Random.Range(rotationSpeedRange.x, rotationSpeedRange.y);
        }

    }

}