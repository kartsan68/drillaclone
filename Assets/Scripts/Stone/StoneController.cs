﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Stone
{

    public class StoneController : MonoBehaviour
    {
        [SerializeField] private StoneViewController view;
        private BezierCurve curve;
        private float speed;

        private float positionOnCurve;
        private float curveSpeed;
        private bool moveFinished;

        internal StonePool Pool { private get; set; }

        internal float Speed
        {
            set
            {
                speed = value;
                curveSpeed = speed / curve.length;
            }
        }

        internal BezierCurve Curve
        {
            set
            {
                curve = value;
                curveSpeed = speed / curve.length;
            }
        }

        internal float Scale
        {
            get { return view.Scale; }
            set { view.Scale = value; }
        }

        internal float RotationSpeed
        {
            get { return view.RotationSpeed; }
            set { view.RotationSpeed = value; }
        }

        internal Vector2 ShakeAmplitude
        {
            get { return view.ShakeAmplitude; }
            set { view.ShakeAmplitude = value; }
        }

        internal Vector2 ShakePeriod
        {
            get { return view.ShakePeriod; }
            set { view.ShakePeriod = value; }
        }

        internal bool MoveFinished
        {
            get { return moveFinished; }
            set
            {
                moveFinished = value;
                positionOnCurve = (moveFinished) ? 1f : 0f;

                if (curve != null) transform.localPosition = curve.GetPointAt(positionOnCurve);
            }
        }

        private void Start()
        {
            if (curve == null) return;
            curveSpeed = speed / curve.length;

            positionOnCurve = 0;
            transform.localPosition = curve.GetPointAt(positionOnCurve);
            transform.localScale = new Vector3(Scale, Scale, transform.localScale.z);
        }

        private void Update()
        {
            if (!Application.isPlaying)
            {
                Start();
                return;
            }
            if (curve == null) return;
            curveSpeed = speed / curve.length;

            positionOnCurve = positionOnCurve + Time.deltaTime * curveSpeed;
            if (positionOnCurve >= 1f) MoveFinished = true;
            else transform.localPosition = curve.GetPointAt(positionOnCurve);

            if (MoveFinished) Pool.PutItem(this);
        }

    }

}


