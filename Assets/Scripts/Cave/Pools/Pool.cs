using System.Collections.Generic;

namespace BlackBears.DiggIsBig.Cave.Pools
{
    public abstract class Pool<T> : System.IDisposable where T : class
    {
        private T prototype;
        public List<T> items;

        public Pool(int count)
        {
            items = new List<T>(count);
        }

        protected bool Disposed { get; private set; }

        public virtual void Dispose()
        {
            if (Disposed) return;
            items.Clear();
            items = null;
            Disposed = true;
        }

        public T TakeItem()
        {
            if (items.Count == 0) return CreateItem(prototype);

            var item = items[items.Count - 1];
            items.RemoveAt(items.Count - 1);
            return item;
        }

        public void PutItem(T item)
        {
            if (item == null) return;
            items.Add(item);
            DisableItem(item);
        }

        public void PutItems(IEnumerable<T> inItems)
        {
            if (inItems == null) return;
            foreach (var item in inItems) PutItem(item);
        }

        protected virtual void Initialize(T prototype, int count)
        {
            this.prototype = prototype;
            for (int i = 0; i < count; i++)
            {
                items.Add(CreateItem(prototype));
            }
        }

        protected virtual void DisableItem(T item) { }

        protected abstract T CreateItem(T prototype);

    }

    public sealed class ItemPool<T> : Pool<T> where T : class, IPoolItem, new()
    {

        public ItemPool(T prototype, int count = 0) : base(count)
        {
            Initialize(prototype, count);
        }

        protected override T CreateItem(T prototype)
        {
            var item = new T();
            ((IPoolItem)item).ApplyPrototype(prototype);
            return item;
        }

        protected override void DisableItem(T item) => item.Disable();

    }

    public class MonoBehaviourPool<T> : Pool<T> where T : UnityEngine.MonoBehaviour
    {

        private UnityEngine.Transform root;

        public MonoBehaviourPool(T prefab, int count, UnityEngine.Transform poolParent = null)
            : this(count, poolParent)
        {
            Initialize(prefab, count);
        }

        protected MonoBehaviourPool(int count, UnityEngine.Transform poolParent) : base(count)
        {
            var go = new UnityEngine.GameObject($"{typeof(T).Name}_pool");
            root = go.transform;
            if (poolParent != null) root.SetParent(poolParent, false);
            root.localPosition = UnityEngine.Vector3.zero;
        }

        public override void Dispose()
        {
            if (Disposed) return;
            UnityEngine.Object.Destroy(root);
            base.Dispose();
        }

        protected override void Initialize(T prefab, int count)
        {
            bool prefabActive = prefab.gameObject.activeSelf;
            prefab.gameObject.SetActive(false);

            base.Initialize(UnityEngine.Object.Instantiate<T>(prefab, root, false), count);

            prefab.gameObject.SetActive(prefabActive);
        }

        protected override void DisableItem(T item)
        {
            item.transform.SetParent(root, false);
            item.gameObject.SetActive(false);
        }

        protected override T CreateItem(T prototype) => UnityEngine.Object.Instantiate<T>(prototype, root, false);

    }


}