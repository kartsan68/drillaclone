namespace BlackBears.DiggIsBig.Cave.Pools
{
    public interface IPoolItem
    {
        void ApplyPrototype(IPoolItem prototype);
        void Disable();
    }

}