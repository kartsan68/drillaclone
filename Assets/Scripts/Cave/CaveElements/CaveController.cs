﻿using BlackBears.Utils;
using UnityEngine;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    public class CaveController : MonoBehaviour
    {
        #region Var
        [SerializeField] private OrthographicScaler cameraScaler;
        [SerializeField] private CaveBackgroundController backgroundController;
        [SerializeField] private CaveGroundController groundController1;
        [SerializeField] private CaveGroundController groundController2;
        [SerializeField] private CaveSideController[] sideControllers;
        [SerializeField] private float scrollSpeed;
        #endregion

        #region MonoBegaviour
        private void Awake()
        {
            Injects();
        }

        private void Update()
        {
            Injects();
        }

        private void Injects()
        {
            if (scrollSpeed < 0) scrollSpeed = 0;

            groundController1.Inject(cameraScaler, scrollSpeed);
            groundController2.Inject(cameraScaler, scrollSpeed);

            for (int i = 0; i < sideControllers.Length; i++) sideControllers[i].Inject(cameraScaler, scrollSpeed);

            backgroundController.Inject(cameraScaler, scrollSpeed);
        }
        #endregion

        #region Other
        public void SetCaveScrollSpeed(float speed) => scrollSpeed = speed;
        public float GetCaveScrollSpeed() => scrollSpeed;
        #endregion
    }

}

