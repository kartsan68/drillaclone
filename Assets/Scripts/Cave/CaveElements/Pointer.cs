﻿using System.Collections;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    public class Pointer : MonoBehaviour
    {
        #region Var
        [SerializeField] Slider progressSlider;
        [SerializeField] GameController gameController;

        private GameConfigurationManager gameConfigurationManager;
        private float pointChest;
        private float pointBeam;
        private double deep;
        private int bigBeamSpawnDeep;
        private int startDeep;
        private bool redy = false;
        private UI.ProgressFlasher _progressFlasher;
        #endregion

        #region MonoBehaviour
        private void Start()
        {
            VarInit();
            SubscribeToEvents();
            StartCoroutine(DistanceBeamProcess());
        }

        #endregion

        #region Other
        private void VarInit()
        {
            _progressFlasher = gameObject.GetComponent<UI.ProgressFlasher>();
            gameConfigurationManager = gameController.GetConfigurationManager();
            startDeep = gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber].startKm;
            if ((GameData.GameData.locationNumber + 1) > gameConfigurationManager.locationData.allLocationArray.Length - 1)
                bigBeamSpawnDeep = int.MaxValue; //заглушка
            else
                bigBeamSpawnDeep = gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber + 1].startKm;

            pointChest = 0;
            pointBeam = 0;
        }

        private void SubscribeToEvents()
        {
            EventManager.BeamSpawn += (isBig, l) =>
            {
                if (isBig)
                    progressSlider.gameObject.SetActive(false);
            };
            EventManager.BeamCrash += () => progressSlider.gameObject.SetActive(true);
        }

        private IEnumerator DistanceBeamProcess()
        {
            pointBeam = bigBeamSpawnDeep * 1000;
            while (true)
            {
                yield return null;
                deep = GameData.GameData.deep;
                redy = false;

                SetMinMaxSliderValue(pointBeam);
                while (!redy)
                {
                    yield return null;
                    deep = GameData.GameData.deep;
                    progressSlider.value = (float)deep;
                    if (((((progressSlider.value - progressSlider.minValue) / (progressSlider.maxValue - progressSlider.minValue)) * 100)) > _progressFlasher.flashStartPercentConfine)
                        _progressFlasher.FlashWork(true);
                    else _progressFlasher.FlashWork(false);

                    if (progressSlider.value >= progressSlider.maxValue)
                    {
                        redy = true;
                        if (GameData.GameData.locationNumber == gameConfigurationManager.locationData.allLocationArray.Length - 1)
                        {
                            //TODO: заглушка
                            pointBeam = int.MaxValue;
                        }
                        else
                        {
                            pointBeam = gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber + 1].startKm * 1000;
                        }
                    }
                }
                _progressFlasher.FlashWork(false);
            }
        }

        private void SetMinMaxSliderValue(float max)
        {
            startDeep = gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber].startKm;
            if (!progressSlider.gameObject.activeSelf) progressSlider.gameObject.SetActive(true);

            progressSlider.maxValue = max;
            progressSlider.minValue = startDeep * 1000;
        }
        #endregion
    }
}