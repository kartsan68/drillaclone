﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class CaveElement : MonoBehaviour
    {

        private SpriteRenderer _spriteRenderer;
        private SpriteRenderer SpriteRenderer => _spriteRenderer ?? (_spriteRenderer = GetComponent<SpriteRenderer>());

        internal float SpriteHeight;

        internal void SetSprite(Sprite sprite)
        {
            SpriteRenderer.sprite = sprite;
            SpriteHeight = SpriteRenderer.size.y;
        }

        internal void SetOrderInLayer(int index) => SpriteRenderer.sortingOrder = (index >= 0) ? index : 0;

    }

}
