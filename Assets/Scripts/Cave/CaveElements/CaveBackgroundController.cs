﻿using UnityEngine;
using BlackBears.DiggIsBig.Common;
using BlackBears.Utils;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{

    [RequireComponent(typeof(SpriteRenderer))]
    public class CaveBackgroundController : MonoBehaviour
    {
        #region Var
        [SerializeField] GameObject secondBackground;
        [SerializeField] GameObject thirdBackground;

        private float scrollSpeedModifier = 1;
        private OrthographicScaler cameraScaler;
        private float originalScrollSpeed;
        private float scrollSpeed;
        private float scrollLength;
        private float currentYPos;
        private Vector3 startPos;

        #endregion

        #region MonoBehaviour

        private void Start()
        {
            if (cameraScaler == null) return;
            if (scrollSpeedModifier < 0) scrollSpeedModifier = 0;

            var spriteRenderer = GetComponent<SpriteRenderer>();
            Vector2 topRight = PositioningUtils.GetTopRightPosition(cameraScaler.ScreenScale);

            float minimalSpriteHeight = topRight.y * 2f;
            float originalSpriteHeight = spriteRenderer.sprite.rect.height / spriteRenderer.sprite.pixelsPerUnit;
            float heightMultiplier = Mathf.Ceil(minimalSpriteHeight / originalSpriteHeight);

            heightMultiplier = (heightMultiplier % 2) == 0 ? heightMultiplier : heightMultiplier + 1;

            float spriteHeight = originalSpriteHeight * heightMultiplier;
            float deltaHeight = minimalSpriteHeight - spriteHeight;

            spriteRenderer.size = new Vector2(topRight.x, spriteHeight);

            startPos = new Vector3(topRight.x / 2f, deltaHeight / 2, transform.localPosition.z);
            scrollLength = spriteHeight / 2;
            currentYPos = 0;
            transform.localPosition = startPos;
            scrollSpeed = originalScrollSpeed * scrollSpeedModifier;

            var secondBgSpriteRenderer = secondBackground.GetComponent<SpriteRenderer>();
            secondBgSpriteRenderer.size = new Vector2(topRight.x, spriteHeight);
            float secondBackgroundYPosition = (spriteRenderer.size.y / 2) + (secondBgSpriteRenderer.size.y / 2);
            secondBackground.transform.localPosition = new Vector3(0, secondBackgroundYPosition, 0);

            var thirdBgSpriteRenderer = thirdBackground.GetComponent<SpriteRenderer>();
            thirdBgSpriteRenderer.size = new Vector2(topRight.x, spriteHeight);
            float thirdBackgroundYPosition = (spriteRenderer.size.y) + (thirdBgSpriteRenderer.size.y);
            thirdBackground.transform.localPosition = new Vector3(0, thirdBackgroundYPosition, 0);
        }

        private void Update()
        {
            scrollSpeed = originalScrollSpeed * scrollSpeedModifier;

            currentYPos += Time.deltaTime * scrollSpeed;
            currentYPos = Mathf.Repeat(currentYPos, scrollLength);

            transform.localPosition = startPos + Vector3.up * currentYPos;
        }
        #endregion

        #region Other
        internal void Inject(OrthographicScaler screenScale, float scrollSpeed)
        {
            if (screenScale == null) return;

            cameraScaler = screenScale;

            if (scrollSpeedModifier < 0) scrollSpeedModifier = 0;

            this.originalScrollSpeed = scrollSpeed;
        }
        #endregion
    }
}

