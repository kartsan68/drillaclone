﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Managers;
using UnityEngine.UI;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.Common.GameProcessControl;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    public class Beam : MonoBehaviour
    {
        #region Var
        [SerializeField] private List<Sprite> beamStateSprites;
        [SerializeField] private GameObject[] tapParticles;
        [SerializeField] private float actualHealth;
        [SerializeField] private Text adsText;
        [SerializeField] private float beamThickness;
        [SerializeField] private float digAfterBeamCrash;
        [SerializeField] private float advBeamEndSpeed;
        [SerializeField] private float smallBeamSpeedAfterCrush;

        private float startHealth;
        private float beamThicknessHealth;
        private GameController gameController;
        private float tickRate;
        private float multiplier;
        private float advertiseDamagePercent;
        private Slider progress;
        private Slider explosionSlider;
        private DrillFormatter drillFormatter;
        private bool startTick;
        private bool isBig;
        private CountdownTimer explosionTimer;
        private float explosionTime;
        private bool itsEnd = false;
        private System.Action hideUIAction;

        private Text bigBeamTimeText;
        private GameObject startGasAlert;
        private GameObject fuckupGasAlert;
        private GameObject alertEffect;

        #endregion

        #region MonoBehaviour
        private void Awake()
        {
            VarInit();
            EventManager.SetBeamHealthInfoEvent("0%", true);
            EventManager.SetBeamHealthInfoEvent("0%", false);
        }

        private void Update()
        {
            SetBeamHealthInfo();
            CheckBeamImageState();
        }

        #endregion

        #region BeamWork
        public void SetActualHealth(double actualHealth, bool reset = false)
        {
            if (actualHealth > this.actualHealth && reset && fuckupGasAlert != null) fuckupGasAlert.SetActive(true);
            this.actualHealth = (float)actualHealth;
            if (actualHealth > 0)
                MoveThroughTheBeamOnLoad();
            if (progress != null) progress.value = Percent((float)actualHealth, startHealth);
        }

        public void CrashBeam()
        {
            actualHealth = 0;
            itsEnd = true;
            if (progress != null) progress.value = 0;
            HideAllAlerts();
            EventManager.BeamCrashEvent();
            Destroy(this.gameObject);
        }

        private void SetStartGaseAlert(float gasTime)
        {
            startGasAlert.SetActive(true);
            bigBeamTimeText.text = drillFormatter.TimeFormatter(gasTime);
        }

        private void MoveThroughTheBeamOnLoad()
        {
            float time = startHealth - actualHealth;
            float step = 0;
            EventManager.MoveThroughTheBeamEvent(0);
            if (isBig)
            {
                float health = startHealth;
                float thickness = beamThickness;
                float stepLoad = 0;
                while (health > actualHealth)
                {
                    var lol = GameData.GameData.boerSpeedWithEarthRigidity;
                    health -= lol * 60 * multiplier;
                    stepLoad = thickness / (actualHealth / (GameData.GameData.boerSpeedWithEarthRigidity * 60 * multiplier));
                    thickness -= stepLoad;
                    step += stepLoad;
                }
                beamThicknessHealth = thickness;
                EventManager.MoveThroughTheBeamEvent(-step);
            }
            else
            {
                float health = startHealth;
                float thickness = beamThickness;
                float stepLoad = 0;
                while (health > actualHealth)
                {
                    health -= 1;
                    stepLoad = thickness / health;
                    thickness -= stepLoad;
                    step += stepLoad;
                }
                beamThicknessHealth = thickness;
                EventManager.MoveThroughTheBeamEvent(-step);
            }
        }

        public void SetActualGasTime(double gasTime)
        {
            explosionTimer.SetTime(gasTime);
        }

        public void SetGasLabels(Text bigBeamTimeText, GameObject startGasAlert,
                                 GameObject fuckupGasAlert, GameObject alertEffect)
        {
            this.bigBeamTimeText = bigBeamTimeText;
            this.startGasAlert = startGasAlert;
            this.fuckupGasAlert = fuckupGasAlert;
            this.alertEffect = alertEffect;
        }


        private float adsUpdateTime = 0;
        public void SetBeamHealthInfo()
        {
            adsUpdateTime -= Time.deltaTime;
            if (adsUpdateTime <= 0)
            {
                adsUpdateTime = 3;
                if (isBig)
                {
                    float percent = Percent((startHealth * advertiseDamagePercent), startHealth);
                    if (percent > progress.value) percent = progress.value;

                    adsText.text = "-" + percent + "%";//drillFormatter.TimeFormatterForBtn(actualHealth / (GameData.GameData.boerSpeedWithEarthRigidity * 60 * multiplier) * advertiseDamagePercent);
                }
                else
                {
                    adsText.text = drillFormatter.TimeFormatterForBtn(actualHealth / 1 * advertiseDamagePercent);
                }
            }

            if (isBig)
            {
                bigBeamTimeText.text = drillFormatter.TimeFormatter(explosionTimer.GetTime());
                EventManager.SetBeamHealthInfoEvent(progress.value + "%", isBig, drillFormatter.TimeFormatter(explosionTimer.GetTime()));
            }
            else
            {
                EventManager.SetBeamHealthInfoEvent(drillFormatter.TimeFormatter(actualHealth / 1), isBig);
            }
        }

        public void SetProgressSliderAndAdsText(Slider slider, Text ads)
        {
            adsText = ads;
            progress = slider;
            progress.minValue = 0;
            progress.maxValue = 100;
            progress.value = 100;
        }

        public float GetActualHealth() => actualHealth;
        public float GetGasTime() => explosionTimer.GetTime();

        public void Damaged(bool isAdvertise = false, bool isTap = false)
        {
            if (!itsEnd)
            {
                float damage;

                if (isAdvertise)
                {
                    damage = startHealth * advertiseDamagePercent;
                    if (actualHealth <= damage)
                    {
                        actualHealth = 0;
                        itsEnd = true;
                        StartCoroutine(MoveThroughTheBeamAdv());
                        if (progress != null) progress.value = 0;
                        HideAllAlerts();
                        return;
                    }
                    else
                    {
                        actualHealth -= damage;
                        MoveThroughTheBeamAfterAdv(advertiseDamagePercent);
                    }
                }
                else
                {
                    if (isTap)
                    {
                        ParticleSystem[] systems = tapParticles[Random.Range(0, tapParticles.Length)].GetComponentsInChildren<ParticleSystem>();
                        EventManager.TapOnBeamEvent();
                        foreach (var index in systems) index.Play();
                    }

                    if (isBig) damage = GameData.GameData.boerSpeedWithEarthRigidity * 60 * multiplier;
                    else damage = 1;
                    MoveThroughTheBeam();
                    if (damage < 1) damage = 1;
                    if (actualHealth <= damage)
                    {
                        actualHealth = 0;

                        if (explosionTimer != null)
                            explosionTimer.StopTimer();

                        if (isBig)
                        {
                            EventManager.BeamCrashEvent();
                            Destroy(this.gameObject);
                        }
                        else StartCoroutine(MoveAfterSmallBeam());

                        itsEnd = true;
                        HideAllAlerts();
                    }
                    else
                    {
                        actualHealth -= damage;
                    }
                }

                if (progress != null) progress.value = Percent(actualHealth, startHealth);
            }
        }

        private void HideAllAlerts()
        {
            if (isBig)
            {
                startGasAlert.SetActive(false);
                fuckupGasAlert.SetActive(false);
                alertEffect.SetActive(false);
            }
        }

        private void MoveThroughTheBeamAfterAdv(float percent)
        {
            float step = beamThicknessHealth * percent;
            beamThicknessHealth -= step;
            EventManager.MoveThroughTheBeamEvent(-step);
        }

        private void MoveThroughTheBeam()
        {
            float step;
            if (isBig) step = beamThicknessHealth / (actualHealth / (GameData.GameData.boerSpeedWithEarthRigidity * 60 * multiplier));
            else step = beamThicknessHealth / actualHealth;

            beamThicknessHealth -= step;
            EventManager.MoveThroughTheBeamEvent(-step);
        }

        private IEnumerator MoveAfterSmallBeam()
        {
            float step = smallBeamSpeedAfterCrush;
            while (digAfterBeamCrash > 0)
            {
                digAfterBeamCrash -= step * Time.deltaTime;
                EventManager.MoveThroughTheBeamEvent(-step * Time.deltaTime);
                yield return null;
            }
            EventManager.BeamCrashEvent();
            Destroy(this.gameObject);
        }

        private IEnumerator MoveThroughTheBeamAdv()
        {
            float step = advBeamEndSpeed;
            if (explosionTimer != null)
                explosionTimer.StopTimer();
            //  EventManager.SetBeamHealthInfoEvent("00:00:00", isBig, "00:00:00");
            hideUIAction?.Invoke();
            while (beamThicknessHealth > 0)
            {
                beamThicknessHealth -= step * Time.deltaTime;
                EventManager.MoveThroughTheBeamEvent(-step * Time.deltaTime);
                yield return null;
            }
            if (isBig)
            {
                EventManager.BeamCrashEvent();
                Destroy(this.gameObject);
            }
            else StartCoroutine(MoveAfterSmallBeam());
        }

        public void SetBeamTickMultipler(float mult) => multiplier = mult;

        private int state = 0;
        private void CheckBeamImageState()
        {
            if (Percent(actualHealth, startHealth) <= 100 && Percent(actualHealth, startHealth) > 70 && state != 0)
            {
                state = 0;
                StartCoroutine(FadeSpawn(beamStateSprites[0]));
            }
            else if (Percent(actualHealth, startHealth) <= 70 && Percent(actualHealth, startHealth) > 30 && state != 1)
            {
                state = 1;
                StartCoroutine(FadeSpawn(beamStateSprites[1]));
            }
            else if (Percent(actualHealth, startHealth) <= 30 && Percent(actualHealth, startHealth) > 0 && state != 2)
            {
                state = 2;
                StartCoroutine(FadeSpawn(beamStateSprites[2]));
            }
        }

        private IEnumerator FadeSpawn(Sprite nextSate)
        {
            SpriteRenderer mainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            Color color = new Color();
            Color color2 = new Color(1, 1, 1, 0.7f);
            while (mainSpriteRenderer.color.a > 0.7f)
            {
                yield return null;
                color = mainSpriteRenderer.color;
                color.a -= 0.1f;
                mainSpriteRenderer.color = color;
                if (mainSpriteRenderer.color.a <= 0.7f)
                    mainSpriteRenderer.sprite = nextSate;
            }
            while (color2.a < 1)
            {
                yield return null;
                color2.a += 0.1f;
                mainSpriteRenderer.color = color2;
            }
        }

        private IEnumerator BeamHealthTick()
        {
            while (true)
            {
                if (isBig) tickRate = GameData.GameData.boerSpeedWithEarthRigidity * 60;
                else tickRate = 1;
                yield return new WaitForSeconds(1f);
                Damaged();
            }
        }

        private void ResetBeam()
        {
            EventManager.MoveThroughTheBeamEvent(0);
            adsUpdateTime = 0;
            SetActualHealth(startHealth);
            SetBeamHealthInfo();
            fuckupGasAlert.SetActive(true);
            StartExplosionTimer(explosionTime, explosionSlider);
        }

        #endregion

        #region Other

        public void StartBeamHealthTick(System.Action hideBeamUI, bool isBig, int health = 100)
        {
            hideUIAction = hideBeamUI;
            this.isBig = isBig;
            startHealth = health;
            actualHealth = startHealth;
            startTick = true;
            StartCoroutine(BeamHealthTick());
        }

        public void StartExplosionTimer(float time, Slider explosionSlider)
        {
            this.explosionSlider = explosionSlider;
            explosionSlider.minValue = 0;
            explosionSlider.maxValue = time;

            explosionTime = time;
            if (explosionTimer == null)
            {
                SetStartGaseAlert(time);
                explosionTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
                explosionTimer.SetUstopable(true);
            }
            explosionTimer.SetCallbackTimer(explosionTime, (t) =>
            {
                float perc = Percent((float)t, explosionTime);

                if (perc < 20 && !alertEffect.activeSelf) alertEffect.SetActive(true);
                else if (perc > 20 && alertEffect.activeSelf) alertEffect.SetActive(false);

                explosionSlider.value = (float)t;
            }, ResetBeam);
        }

        public void SetAdvertiseDamagePercent(float percent) => advertiseDamagePercent = percent;

        private void VarInit()
        {
            beamThicknessHealth = beamThickness;
            startTick = false;
            drillFormatter = new DrillFormatter();
            tickRate = 1;
            multiplier = 1;
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            startHealth = actualHealth;
            advertiseDamagePercent = 0.1f;
        }

        private float Percent(float number, float number2) => Mathf.RoundToInt((number / number2) * 100);
        #endregion
    }
}