﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    public class TapSoundObject : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;
        private float lifeTime;
        private bool active = false;

        public void CreateSound(AudioClip clip, float lifeTime, float clipVolume = 0.7f)
        {
            this.lifeTime = lifeTime;
            audioSource.clip = clip;
            audioSource.volume = clipVolume;
            active = true;
            audioSource.Play();
        }

        void Update()
        {
            if (active)
            {
                lifeTime -= Time.deltaTime;
                if (lifeTime <= 0) Destroy(this.gameObject);
            }
        }
    }
}
