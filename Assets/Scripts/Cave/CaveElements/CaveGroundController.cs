﻿using UnityEngine;
using BlackBears.DiggIsBig.Common;
using BlackBears.Utils;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    public class CaveGroundController : MonoBehaviour
    {
        #region Var
        private float scrollSpeedModifier = 1;
        private OrthographicScaler cameraScaler;
        private float scrollSpeed;
        private float scrollHeight;
        private float textureSpeed;
        private Material material;
        #endregion

        #region MonoBehaviour

        private void Update()
        {
            if (material != null)
            {
                textureSpeed = scrollSpeed * scrollSpeedModifier / scrollHeight;
                float newOffset = Mathf.Repeat(material.GetFloat("_Offset") + Time.deltaTime * textureSpeed, 1);
                material.SetFloat("_Offset", newOffset);
            }
        }
        #endregion

        #region Other
        internal void Inject(OrthographicScaler screenScale, float scrollSpeed)
        {
            if (screenScale == null) return;

            cameraScaler = screenScale;

            if (scrollSpeed < 0) scrollSpeed = 0;

            this.scrollSpeed = scrollSpeed;
        }

        public void SwapGroundTexture(Texture texture)
        {
            var meshRender = GetComponent<MeshRenderer>();
            material = new Material(meshRender.material);
            material.mainTexture = texture;
            meshRender.material = material;

            float screenWidth = PositioningUtils.GetTopRightPosition(cameraScaler.ScreenScale).x;
            float textureHeight = material.mainTexture.height;
            scrollHeight = textureHeight;

            var currentScale = transform.localScale;
            transform.localScale = new Vector3(screenWidth, transform.localScale.y, currentScale.z);

            textureSpeed = scrollSpeed * scrollSpeedModifier / scrollHeight;
        }
        #endregion
    }

}

