﻿using System.Collections.Generic;
using UnityEngine;
using BlackBears.DiggIsBig.Common;
using BlackBears.DiggIsBig.Settings;
using BlackBears.Utils;

namespace BlackBears.DiggIsBig.Cave.CaveElements
{
    public class CaveSideController : MonoBehaviour
    {
        #region Var
        [SerializeField] private float minHeightOrigin = 2;
        [SerializeField, Range(0, 1)] private int alignment;
        [SerializeField] private float scrollSpeedModifier;
        [SerializeField] private int orderInLayer;

        private OrthographicScaler cameraScaler;
        private float scrollSpeed;
        private float originalScrollSpeed;
        private Sprite[] sprites;
        private List<CaveElement> parts = new List<CaveElement>();
        private int topElementIndex = 0;
        private float screenHeight;

        #endregion

        #region MonoBehaviour

        private void Update()
        {
            scrollSpeed = originalScrollSpeed * scrollSpeedModifier;
            if (parts.Count <= 0) return;

            float yShift = Time.deltaTime * scrollSpeed;
            for (int i = 0; i < parts.Count; i++)
            {
                CaveElement elem = parts[i];
                var currentPos = elem.transform.localPosition;
                elem.transform.localPosition = new Vector3(currentPos.x, currentPos.y + yShift, currentPos.z);
            }
            var topPart = parts[topElementIndex];
            if (topPart.transform.localPosition.y > screenHeight)
            {
                topPart.SetSprite(sprites[Random.Range(0, sprites.Length)]);

                int nextElementIndex = (topElementIndex + 1) % parts.Count;
                float newTopYPos = parts[nextElementIndex].transform.localPosition.y - topPart.SpriteHeight;
                var oldLocalPosition = topPart.transform.localPosition;
                topPart.transform.localPosition = new Vector3(oldLocalPosition.x, newTopYPos, oldLocalPosition.z);

                topElementIndex = (topElementIndex - 1 + parts.Count) % parts.Count;
            }
        }
        #endregion

        #region Other
        internal void Inject(OrthographicScaler screenScale, float scrollSpeed)
        {
            if (screenScale == null) return;

            cameraScaler = screenScale;

            if (scrollSpeedModifier < 0) scrollSpeedModifier = 0;

            originalScrollSpeed = scrollSpeed;
        }

        private void AlignHorizontal()
        {
            if (cameraScaler == null) return;

            Vector2 topRight = PositioningUtils.GetTopRightPosition(cameraScaler.ScreenScale);
            transform.localPosition = new Vector3(topRight.x * alignment, 1600, transform.localPosition.z);
        }

        public void SwapSidesSprite(Sprite[] sprites)
        {
            this.sprites = sprites;

            if (parts.Count == 0)
            {
                AlignHorizontal();
                screenHeight = PositioningUtils.GetTopRightPosition(cameraScaler.ScreenScale).y;
                float minFillHeight = screenHeight * minHeightOrigin;
                float wallYOffset = minFillHeight - screenHeight;
                float filledHeight = 0f;

                //  подчистить
                var sidesChilds = GetComponentsInChildren<CaveElement>();
                for (int i = 0; i < sidesChilds.Length; i++)
                {
                    Destroy(sidesChilds[i]);
                }

                while (filledHeight < (minFillHeight * 1.3))
                {
                    var go = new GameObject($"Wall_{parts.Count}", typeof(CaveElement));
                    go.transform.SetParent(this.transform, false);
                    go.transform.localPosition = new Vector3(0, filledHeight - wallYOffset, 0);

                    var caveElement = go.GetComponent<CaveElement>();
                    var sprite = sprites[Random.Range(0, sprites.Length)];
                    caveElement.SetSprite(sprite);
                    caveElement.SetOrderInLayer(orderInLayer);
                    filledHeight += caveElement.SpriteHeight;
                    topElementIndex = parts.Count;
                    parts.Add(caveElement);
                }

                scrollSpeed = originalScrollSpeed * scrollSpeedModifier;
            }
            else
            {
                for (int i = 0; i < parts.Count; i++)
                {
                    var sprite = sprites[Random.Range(0, sprites.Length)];
                    parts[i].SetSprite(sprite);
                }
            }
        }
        #endregion

    }

}



