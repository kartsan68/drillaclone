﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Controllers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Cave
{
    public class TouchReactElement : MonoBehaviour
    {
        [SerializeField] private float lifeTime = 2;
        [SerializeField] private GameController gameController;

        private float scrollSpeed;

        void Update()
        {
            lifeTime -= Time.deltaTime;
            if (lifeTime <= 0) Destroy(gameObject);

            if (!gameController.GetPauseState)
            {
                scrollSpeed = gameController.GetCaveSpeed;
                transform.Translate(new Vector3(0, Time.deltaTime * scrollSpeed, 0));
            }
        }
    }
}
