﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Settings
{
    [CreateAssetMenu]
    public class CaveTextures : ScriptableObject
    {
        public Sprite backgroundSprite;
        public Sprite[] wallSprites;
        public Sprite stoneSprite;
        public Texture groundTexture;
    }

}


