﻿using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.GameElements.Elements;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.BBGames;
using BlackBears.GameCore.Features.Vibration;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Settings
{
    public class GameSettings : MonoBehaviour
    {
        [SerializeField] private string VKURL;
        [SerializeField] private string FBURL;
        [SerializeField] private Toggle soundToggle;
        [SerializeField] private Toggle musicToggle;
        [SerializeField] private Toggle vibroToggle;

        [SerializeField] private Vector2 MenuOfflineSize;
        [SerializeField] private Vector2 MenuOnlineSize;
        [SerializeField] private GameObject ourGames;
        [SerializeField] private GameObject settingsBar;
        [SerializeField] OurGameElement ourGameElement;
        [SerializeField] private GameObject scrollView;

        private string BBURL;
        private BlackBears.GameCore.Features.BBGames.BBGamesFeature bbGamesFeature;

        private string MyEscapeURL(string url) => UnityEngine.Networking.UnityWebRequest.EscapeURL(url).Replace("+", "%20");

        private void Start()
        {
            GameData.GameSettingsData.vibrationFeature = BBGC.Features.Vibration();
            EventManager.PlaySoundEvent(Sound.BackgroundMusic);

            vibroToggle.isOn = GameData.GameSettingsData.vibrationFeature.Enable;
            GameData.GameSettingsData.vibration = vibroToggle.isOn;

            string mailBody = "Game version: " + Application.version;
            mailBody += "\n \r" + SystemInfo.operatingSystem;
            mailBody += "\n \r" + SystemInfo.deviceModel;
            string subject = "My Digg Is Big";

#if UNITY_IOS
            mailBody = MyEscapeURL(mailBody);
            subject = MyEscapeURL(subject);
#endif

            BBURL = "mailto:mail@blackbears.mobi?subject=" + subject + "&body=" + mailBody;

            GameData.GameSettingsData.music = true;
            GameData.GameSettingsData.sound = true;
            soundToggle.isOn = true;
            musicToggle.isOn = true;

            var gcData = SaveCreator.GetLoadData(SaveKeys.gameControllerSaveKey).AsArray;
            if (gcData != null)
            {
                bool sound = false;
                bool music = false;

                for (int i = 0; i < gcData.Count; i++)
                {
                    if (gcData[i].HasKey(SaveKeys.soundKey))
                    {
                        sound = gcData[i][SaveKeys.soundKey];
                        GameData.GameSettingsData.sound = sound;
                        soundToggle.isOn = sound;
                    }
                    else if (gcData[i].HasKey(SaveKeys.musicKey))
                    {
                        music = gcData[i][SaveKeys.musicKey];
                        GameData.GameSettingsData.music = music;
                        musicToggle.isOn = music;
                    }
                }
            }

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                settingsBar.GetComponent<RectTransform>().sizeDelta = MenuOfflineSize;
                ourGames.SetActive(false);
            }
            else
            {
                bbGamesFeature = BBGC.Features.GetFeature<BBGamesFeature>();
                if (bbGamesFeature.BannersCount == 0)
                    bbGamesFeature.OnBannersLoaded += OnLoadBanner;
                else
                    OnLoadBanner();
            }
        }

        public void SwapVibroToggle(bool state)
        {
            GameData.GameSettingsData.vibrationFeature.Enable = state;
            GameData.GameSettingsData.vibration = state;
        }

        public void SoundOnOff(bool state) => GameData.GameSettingsData.sound = state;
        public void MusicOnOff(bool state)
        {
            GameData.GameSettingsData.music = state;
            EventManager.MuteMusicEvent();
        }

        public void GoToVk() => Application.OpenURL(VKURL);

        public void GoToFB() => Application.OpenURL(FBURL);

        public void GoToSite() => Application.OpenURL(BBURL);

        private void OnLoadBanner()
        {
            RectTransform rt = settingsBar.GetComponent<RectTransform>();
            rt.sizeDelta = MenuOfflineSize;
            ourGames.SetActive(false);

            if (bbGamesFeature.BannersCount > 0)
            {
                rt.sizeDelta = MenuOnlineSize;
                ourGames.SetActive(true);
                for (int i = 0; i < bbGamesFeature.BannersCount; i++)
                {
                    var bannerData = bbGamesFeature[i];
                    bbGamesFeature.LoadBanner(bannerData, texture =>
                    {
                        var game = Instantiate(ourGameElement, scrollView.transform);
                        game.SetImageAndName(texture, bannerData.name, bannerData.link);
                    }, null);
                }

            }
        }
    }
}
