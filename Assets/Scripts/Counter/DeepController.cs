﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using UnityEngine;
using UnityEngine.UI;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Controllers;

namespace BlackBears.DiggIsBig.Counter
{
    public class DeepController : MonoBehaviour
    {

        [SerializeField] private GameController gameController;
        [SerializeField] private Sprite goldCard;
        [SerializeField] private List<Image> unitList;
        [SerializeField] private Image fakeUnit;
        [SerializeField] private float fakeUnitPos;
        [SerializeField] private float newUnitPos = 25;

        private GameConfigurationManager gameConfigurationManager;
        private double deep;
        private float speed = 0;
        private bool paused;
        private long oldDeep;
        private long actualDeep;
        private Configuration.Data.ResourcesDeepChangeData.ResData[] configDataElements;

        public void TimerPause() => paused = true;
        public void TimerUnpause() => paused = false;
        public void SetTimerSpeed(float speed) => this.speed = speed;

        public void SetDeep(OfflineCalcData ocd)
        {
            this.deep = ocd.deep;
            actualDeep = (long)deep;

            string actualDeepForPaint = actualDeep.ToString();

            if (actualDeepForPaint.Length > unitList.Count)
            {
                Image newImg = Instantiate(unitList[0], new Vector3(unitList[unitList.Count - 1].transform.position.x - newUnitPos, unitList[unitList.Count - 1].transform.position.y, 0), Quaternion.identity, unitList[0].transform.parent);
                newImg.transform.SetAsFirstSibling();
                newImg.sprite = goldCard;
                unitList.Add(newImg);
            }

            SetAllUnitsDataDeep();

            long deepKM = ((long)deep) / 1000;
            CheckKmChanges(deepKM * 1000);

            CheckResFarm();
        }

        private void Awake()
        {
            EventManager.ClearChild += ClearChild;
            EventManager.BeamCrash += ClearChild;
            EventManager.GamePaused += ClearChild;
        }

        private void Start()
        {
            paused = false;
            SetConfig();
        }

        private void Update()
        {
            if (!paused && speed > 0)
            {
                speed /= (1.0f / Time.smoothDeltaTime);
                oldDeep = (long)deep;
                deep += speed;
                actualDeep = (long)deep;
                GameData.GameData.deep = deep;

                if (oldDeep < actualDeep)
                {
                    OtherCheckUnits();
                }
            }
        }

        private void OtherCheckUnits()
        {
            string actualDeepForPaint = actualDeep.ToString();

            if (actualDeepForPaint.Length > unitList.Count)
            {
                Image newImg = Instantiate(unitList[0], new Vector3(unitList[unitList.Count - 1].transform.position.x - newUnitPos, unitList[unitList.Count - 1].transform.position.y, 0), Quaternion.identity, unitList[0].transform.parent);
                newImg.transform.SetAsFirstSibling();
                newImg.sprite = goldCard;
                unitList.Add(newImg);
            }

            for (int i = 0; i < actualDeepForPaint.Length; i++)
            {
                Text textInCard = unitList[i].GetComponentInChildren<Text>();
                string newTextInCard = actualDeepForPaint[actualDeepForPaint.Length - 1 - i].ToString();
                if (!(textInCard.text.Equals(newTextInCard)))
                {
                    textInCard.text = newTextInCard;
                    fakeUnit.GetComponentInChildren<Text>().text = newTextInCard;
                    fakeUnit.sprite = unitList[i].sprite;
                    Instantiate(fakeUnit, new Vector3(unitList[i].transform.position.x, fakeUnitPos, 0), Quaternion.identity, unitList[i].transform);
                }
            }

            long actualDeepKM = (actualDeep / 1000);
            long oldDeepKM = (oldDeep / 1000);

            if (actualDeepKM < oldDeep)
            {
                CheckKmChanges(actualDeepKM * 1000);
            }
        }

        private void ClearChild()
        {
            foreach (var unit in unitList)
            {
                if (unit != null)
                {
                    if (unit.GetComponentsInChildren<TimerUnitWithAnimationController>() != null)
                    {
                        TimerUnitWithAnimationController[] trash = unit.GetComponentsInChildren<TimerUnitWithAnimationController>();

                        if (trash.Length > 0)
                        {
                            foreach (var element in trash)
                                Destroy(element.gameObject);
                        }
                    }
                }
            }
        }

        private void SetConfig()
        {
            gameConfigurationManager = gameController.GetConfigurationManager();

            configDataElements = gameConfigurationManager.resourcesDeepChangeData.resourcesData;
        }

        //TODO: вынести в более подходящий класс
        int counter = 0;
        double oldDeepKM = 0;
        private void CheckKmChanges(double deepKM)
        {
            if (deepKM % 1000 == 0 && deepKM != 0 && counter == 0)
            {
                counter++;
                oldDeepKM = deepKM;

                EventManager.NewKmDeepEvent();
                //изменения добычи ресурсов
                for (int i = 0; i < configDataElements.Length; i++)
                {
                    if (configDataElements[i].kilometer == deepKM / 1000)
                    {
                        EventManager.DeepResourceChangeEvent(configDataElements[i].resourcesCountArr);
                        break;
                    }
                }

                //спавн большого сундука
                foreach (var item in gameConfigurationManager.dropData.drop[1].spawnKM)
                {
                    if (item == (deepKM / 1000))
                    {
                        EventManager.SpawnBigChestEvent();
                        break;
                    }
                }
            }

            if (oldDeepKM < deepKM) counter = 0;
        }

        private void CheckResFarm()
        {
            for (int i = 0; i < configDataElements.Length; i++)
            {
                if (configDataElements[i].kilometer == (int)(deep / 1000))
                {
                    EventManager.DeepResourceChangeEvent(configDataElements[i].resourcesCountArr);
                    break;
                }
            }
        }

        private void SetAllUnitsDataDeep()
        {
            string str = deep.ToString();
            int lenght = str.Length;

            for (int i = 0; i < lenght; i++)
            {
                string numb = str[i].ToString();
                unitList[lenght - 1 - i].GetComponentInChildren<Text>().text = numb;
            }
        }
    }
}

