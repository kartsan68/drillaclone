﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Counter
{
    public class TimerUnitWithAnimationController : MonoBehaviour
    {
        private Animator anim;

        private void Awake() => gameObject.GetComponent<Animator>().Play("SlideDown");

        private void KillSelf() => Destroy(this.gameObject);
    }
}
