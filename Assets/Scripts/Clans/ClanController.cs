﻿using System.Collections;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Notifications;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Clans
{
    public class ClanController : MonoBehaviour
    {
        [SerializeField] private Button _cantUseClanButton;
        [SerializeField] private Button _clanButton;
        [SerializeField] private GameController _gameController;

        private GameConfigurationManager _gameConfigurationManager;
        private bool _allRight;

        private void Start()
        {
            Init();
            CheckAccess();
            StartCoroutine(CheckAccess());
        }

        private void Init()
        {
            _allRight = false;
            _gameConfigurationManager = _gameController.GetConfigurationManager();
            _cantUseClanButton.onClick.AddListener(ShowCantUseClanAlert);
        }

        private void ShowCantUseClanAlert()
        {
            BBGC.Features.GetFeature<NotificationsFeature>().ShowNotification(new Notification(NotificationType.Warning,
             string.Format(DigIsBigLocalizator.GetLocalizationString("CLAN_KM_NOTIF"), (int)(_gameConfigurationManager.clanData.clanUnlockKM / 1000))));
        }

        private IEnumerator CheckAccess()
        {
            while (!_allRight)
            {
                yield return null;

                if (_gameConfigurationManager.clanData != null)
                {
                    if (GameData.GameData.deep >= (_gameConfigurationManager.clanData.clanUnlockKM))
                        ChangeClanButtonState(true);
                    else ChangeClanButtonState(false);
                }
                else ChangeClanButtonState(false);
            }
        }

        private void ChangeClanButtonState(bool state)
        {
            _allRight = state;
            _cantUseClanButton.gameObject.SetActive(!state);
            _clanButton.interactable = state;
        }

    }
}
