﻿using BlackBears.GameServer;
using SimpleJSON;

namespace BlackBears.DiggIsBig.Clans
{
    public class LocalGamePlayer : LocalPlayer
    {

        public LocalGamePlayer() : base() { }
        public LocalGamePlayer(LocalGamePlayer other) : base(other) { }
        public LocalGamePlayer(JSONNode save) : base(save) { }

        internal void UpdateNick(string value) => this.Nick = value;
        internal void UpdateIcon(string value) => this.Icon = value;
        internal void UpdateCountry(string value) => this.Country = value;
        internal void UpdateScore(double value) => this.Score = value;
        internal void UpdateHardCurrency(double value) => this.HardCurrency = value;
        internal void UpdateSubscriptionId(string value) => this.SubscriptionId = value;

    }
}
