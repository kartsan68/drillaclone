﻿using System;
using System.Collections.Generic;
using BlackBears;
using BlackBears.Chronometer;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.GameServer;
using BlackBears.GameCore.Features.Save;
using BlackBears.GameServer;
using SimpleJSON;
using UniRx;
using UnityEngine;
using SQBAModule = BlackBears.SQBA.SQBA;

namespace BlackBears.DiggIsBig.Clans
{
    public class GamePlayersManager : PlayersManager
    {
        private const string saveKey = "playerData";
        private const string playerKey = "player";

        private const string recalcIdKey = "rclc_id";
        private const string updateIdKey = "updt_id";

        private GameServerFeature gameServer;
        private bool validated;
        private string recalcId = string.Empty;
        private string updateId = string.Empty;
        private LocalGamePlayer player;

        public bool IsPlayerValidated => validated;
        public LocalPlayer Player => player;

        private void Save()
        {
            UpdatePlayer();
            List<Save.SaveData> data = new List<Save.SaveData>();

            data.Add(new Save.SaveData(playerKey, player.ToJson()));
            data.Add(new Save.SaveData(recalcIdKey, recalcId));
            data.Add(new Save.SaveData(updateIdKey, updateId));

            SaveCreator.Save(saveKey, data);
        }

        internal GamePlayersManager()
        {
            this.gameServer = BBGC.Features.GameServer();
            JSONNode saveData = SaveCreator.GetLoadData(saveKey);
            if (saveData.AsArray != null) saveData = saveData.AsArray[0];
            EventManager.NewKmDeep += () => UpdatePlayer();
            EventManager.OpenClans += () => UpdatePlayer();
            EventManager.SaveGame += () => { if (IsPlayerValidated) Save(); };

            if (saveData != null && saveData[playerKey] != null)
            {
                player = new LocalGamePlayer(saveData[playerKey]);

                recalcId = saveData[recalcIdKey].GetStringOrDefault(string.Empty);
                updateId = saveData[updateIdKey].GetStringOrDefault(string.Empty);
            }
        }
        internal void UpdatePlayer(string nickname, string icon, string country,
           Block onSuccess, FailBlock onFail)
        {
            if (!validated || player == null) return;

            player.UpdateNick(nickname);
            player.UpdateIcon(icon);
            player.UpdateCountry(country);

            UpdatePlayer(onSuccess, onFail);
        }

        public void UpdatePlayer(Block onSuccess = null, FailBlock onFail = null, int value = -1)
        {
            if (!validated) return;

            if (!TimeModule.IsValid || player == null)
            {
                onFail.SafeInvoke();
                return;
            }

            player.UpdateScore(Math.Round(GameData.GameData.deep));
            player.UpdateWeeklyScore(TimeModule.ActualTime);
            player.UpdateHardCurrency(0);
            player.UpdateSubscriptionId("");

            gameServer.Connection.UpdatePlayer(player, Player.Id, Player.Id, _ => onSuccess.SafeInvoke(), onFail);
        }

        internal void ValidatePlayer(Block onSuccess, FailBlock onFail)
        {
            if (!TimeModule.IsValid && !TimeModule.IsOffline)
            {
                onFail.SafeInvoke();
                return;
            }
            bool isNewPlayer = player == null;
            if (isNewPlayer) player = new LocalGamePlayer();

            player.UpdateScore(Math.Round(GameData.GameData.deep));
            player.UpdateWeeklyScore(TimeModule.ActualTime);
            player.UpdateHardCurrency(0);
            player.UpdateSubscriptionId("");

            if (isNewPlayer)
            {
                if (SQBAModule.NoInternetConection)
                {
                    validated = false;
                    onSuccess.SafeInvoke();
                }
                else
                {
                    gameServer.Connection.CreatePlayer(player, _ =>
                    {
                        validated = true;
                        onSuccess.SafeInvoke();
                    }, onFail);
                }
            }
            else
            {
                validated = true;
                onSuccess.SafeInvoke();
            }
        }

        protected override ServerPlayer CreateInstance() => new ServerPlayer();
    }
}
