﻿using UnityEngine;
using System.Collections;
using UnityEditor;


namespace BlackBears.CaveGenerator
{
    [CustomEditor(typeof(MeshGenerator))]
    public class MeshGanerateEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            MeshGenerator pMesh = (MeshGenerator)target;

            if (GUILayout.Button("SaveMesh"))
            {
                pMesh.SaveMesh();
            }
        }
    }
}
