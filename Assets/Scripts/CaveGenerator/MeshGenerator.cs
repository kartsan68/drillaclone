﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.CaveGenerator
{
    [ExecuteInEditMode]
    public class MeshGenerator : MonoBehaviour
    {
        [SerializeField] private BezierCurve curve;
        [SerializeField] BezierPoint leftStartPoint;
        [SerializeField] BezierPoint rightStartPoint;
        [SerializeField] private string meshName;

        private List<Vector3> points;
        private List<Vector3> pointsForJSON;

        private List<Vector3> meshVertices = new List<Vector3>();
        private List<int> triangles = new List<int>();
        private Vector3 botCentrePosition;
        private Mesh mesh;

        private void Awake() => InitVar();

        private void InitVar()
        {
            triangles = new List<int>();
            meshVertices = new List<Vector3>();
        }
        private void Start()
        {
            GenerateMesh();
            CreateMesh();
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                GenerateMesh();
                CreateMesh();
            }
        }
#endif


        private void GetPoints()
        {
            points = new List<Vector3>();
            pointsForJSON = new List<Vector3>();

            for (int i = 0; i < curve.pointCount; i++)
            {
                points.Add(curve.GetPointAtIndex(i));
                pointsForJSON.Add(curve.GetPointAtIndex(i));
            }
        }
        private void GenerateMesh()
        {
            GetPoints();

            mesh = new Mesh();
            meshVertices.Clear();
            triangles.Clear();

            for (int i = 0; i < points.Count; i++)
            {
                //0
                meshVertices.Add(new Vector3(points[i].x, 0f, 0f));
                //1
                meshVertices.Add(new Vector3(points[i].x, points[i].y, 0f));
                //2
                if (i == (points.Count - 1)) meshVertices.Add(new Vector3(points[i - 1].x, 0f, 0f));
                else meshVertices.Add(new Vector3(points[i + 1].x, 0f, 0f));
                //3
                if (i == (points.Count - 1)) meshVertices.Add(new Vector3(points[i - 1].x, 0f, 0f));
                else meshVertices.Add(new Vector3(points[i + 1].x, points[i + 1].y, 0f));
            }

            for (int i = 0; i < meshVertices.Count; ++i)
            {
                meshVertices[i] = new Vector3(meshVertices[i].x / 320,
                    meshVertices[i].y / 160, 0);
            }



            int trich = 0;
            for (int i = 0; i < points.Count; i++)
            {
                if (i == (points.Count - 1))
                {
                    //1
                    triangles.Add(trich + i + 1);
                    //0
                    triangles.Add(trich + i);
                    //2
                    triangles.Add(trich + i + 2);

                    //2
                    triangles.Add(trich + i + 2);
                    //0
                    triangles.Add(trich + i);
                    //3
                    triangles.Add(trich + i + 3);
                }
                else
                {
                    //0
                    triangles.Add(trich + i);
                    //1
                    triangles.Add(trich + i + 1);
                    //2
                    triangles.Add(trich + i + 2);

                    //2
                    triangles.Add(trich + i + 2);
                    //1
                    triangles.Add(trich + i + 1);
                    //3
                    triangles.Add(trich + i + 3);
                }
                trich += 3;
            }
        }

        private void CreateMesh()
        {
            mesh.Clear();
            mesh.vertices = meshVertices.ToArray();
            mesh.triangles = triangles.ToArray();

            Vector2[] uvs = new Vector2[meshVertices.Count];

            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(meshVertices[i].x, meshVertices[i].y);
            }
            mesh.uv = uvs;

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            GetComponent<MeshFilter>().mesh = mesh;
        }


        private string GenerateJSON()
        {
            JSONObject json = new JSONObject();

            json["time"] = 1488;

            var lArray = new JSONArray();
            json.Add("LeftStartPoint", lArray);
            lArray.Add(leftStartPoint.position.x);
            lArray.Add(leftStartPoint.position.y);
            lArray.Add(leftStartPoint.position.z);

            var rArray = new JSONArray();
            json.Add("RightStartPoint", rArray);
            rArray.Add(rightStartPoint.position.x);
            rArray.Add(rightStartPoint.position.y);
            rArray.Add(rightStartPoint.position.z);

            for (int i = 0; i < pointsForJSON.Count; i++)
            {
                var array = new JSONArray();
                json.Add(i.ToString(), array);

                array.Add(pointsForJSON[i].x);
                array.Add(pointsForJSON[i].y);
                array.Add(pointsForJSON[i].z);
            }

            return json.ToString();
        }

        public void Generate()
        {
            GenerateMesh();
            CreateMesh();
        }

#if UNITY_EDITOR
        public void SaveMesh()
        {
            File.WriteAllText(Application.dataPath + "/GeneratedMeshes/" + meshName + ".json", GenerateJSON());
            AssetDatabase.Refresh();

            AssetDatabase.CreateAsset(mesh, "Assets/GeneratedMeshes/" + meshName + ".mesh");
            AssetDatabase.SaveAssets();
        }
#endif
    }
}