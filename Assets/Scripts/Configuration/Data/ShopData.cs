using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class ShopData
    {
        public ShopContent[] shopContent { get; private set; }

        public ShopData(JSONNode node)
        {
            JSONArray shopArray = node.AsArray;
            shopContent = new ShopContent[shopArray.Count];
            for (int i = 0; i < shopArray.Count; i++)
            {
                shopContent[i] = new ShopContent(shopArray[i]);
            }
        }

        public class ShopContent
        {
            private const string idKey = "id";
            private const string nameKey = "name";
            private const string contentKey = "content";

            public readonly int id;
            public readonly string name;
            public readonly Content content;

            public ShopContent(JSONNode node)
            {
                id = node[idKey].AsInt;
                name = node[nameKey].Value;
                content = new Content(node[contentKey]);
            }

            public class Content
            {
                private const string gameTimeKey = "game_time";
                private const string multiplierKey = "multiplier";
                private const string workTimeKey = "work_time";
                private const string priceKey = "price";
                private const string detailLvlsKey = "detail_lvls";
                private const string chargeCountKey = "charge_count";
                private const string refreshTimeKey = "time_charge_refresh";
                private const string delayKey = "delay";

                public readonly int gameTime;
                public readonly float workTime;
                public readonly float multiplier;
                public readonly float chargeCount;
                public readonly float refreshTime;
                public readonly float price;
                public readonly float delay;
                public readonly DetailLvls detailLvls;

                public Content(JSONNode node)
                {
                    gameTime = node[gameTimeKey].AsInt;
                    workTime = node[workTimeKey].AsFloat;
                    multiplier = node[multiplierKey].AsFloat;
                    price = node[priceKey].AsFloat;
                    detailLvls = new DetailLvls(node[detailLvlsKey]);
                    delay = node[delayKey].AsFloat;
                    chargeCount = node[chargeCountKey].AsFloat;
                    refreshTime = node[refreshTimeKey].AsFloat;
                }

                public class DetailLvls
                {
                    private const string boerKey = "boer";
                    private const string ladleKey = "ladle";
                    private const string engineKey = "engine";
                    private const string storageKey = "storage";

                    public readonly int boerLvl;
                    public readonly int laldleLvl;
                    public readonly int engineLvl;
                    public readonly int storageLvl;

                    public DetailLvls(JSONNode node)
                    {
                        boerLvl = node[boerKey];
                        laldleLvl = node[ladleKey];
                        engineLvl = node[engineKey];
                        storageLvl = node[storageKey];
                    }
                }
            }
        }
    }
}
