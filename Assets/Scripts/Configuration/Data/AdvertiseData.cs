using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class AdvertiseData
    {
        private const string firstShowTimeKey = "first_show_time";
        private const string advDelayKey = "adv_interstitial_delay";
        private const string rewardedIdleKey = "rewarded_idle_time";

        public readonly int firstShowTime;
        public readonly int advInterstitialDelay;
        public readonly int rewardedIdleTime;


        public AdvertiseData(JSONNode node)
        {
            firstShowTime = node[firstShowTimeKey].AsInt;
            advInterstitialDelay = node[advDelayKey].AsInt;
            rewardedIdleTime = node[rewardedIdleKey].AsInt;
        }
    }
}