using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class ClanData
    {
        private const string unlockKMKey = "clan_unlock_km";

        public readonly int clanUnlockKM;
        public ClanData(JSONNode node)
        {
            clanUnlockKM = node[unlockKMKey].AsInt;
        }
    }
}