using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class SpawnData
    {
        private const string resourceSpawnDividerKey = "resource_spawn_divider";
        private const string beamSpawnTimeKey = "beam_spawn_time";
        private const string boxesSpawnTimeKey = "boxes_spawn_time";
        private const string chestsSpawnTimeKey = "chests_spawn_time";
        private const string boosterSpawnTimeKey = "booster_spawn_time";
        private const string boxesSpawnKmKey = "boxes_start_spawn_km";
        private const string chestsSpawnKmKey = "chests_start_spawn_km";
        private const string boosterSpawnKMKey = "booster_start_spawn_km";



        public readonly float resourceSpawnDivider;
        public readonly int beamSpawnTime;
        public readonly int boxesSpawnTime;
        public readonly int boxesKmSpawnStart;
        public readonly int chestsKmSpawnStart;
        public readonly int chestsSpawnTime;
        public readonly int boosterSpawnTime;
        public readonly int boosterSpawnStart;

        public SpawnData(JSONNode node)
        {
            boxesKmSpawnStart = node[boxesSpawnKmKey].AsInt;
            chestsKmSpawnStart = node[chestsSpawnKmKey].AsInt;
            resourceSpawnDivider = node[resourceSpawnDividerKey].AsFloat;
            beamSpawnTime = node[beamSpawnTimeKey].AsInt;
            boxesSpawnTime = node[boxesSpawnTimeKey].AsInt;
            chestsSpawnTime = node[chestsSpawnTimeKey].AsInt;
            boosterSpawnTime = node[boosterSpawnTimeKey].AsInt;
            boosterSpawnStart = node[boosterSpawnKMKey].AsInt;
        }
    }
}