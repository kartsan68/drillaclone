using BlackBears.SQBA;
using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class ShopItemData
    {
        public SQBAShopItem[] shopItems { get; private set; }
        public ShopItemData(JSONNode node)
        {
            JSONArray shopItemArray = node.AsArray;
            shopItems = new ShopItem[shopItemArray.Count];

            for (int i = 0; i < shopItemArray.Count; i++)
            {
                shopItems[i] = new ShopItem(shopItemArray[i], "1");
            }
        }

        public class ShopItem : SQBAShopItem
        {
            private const string iapKey = "iap";
            private const string productTypeKey = "product_type";

            public readonly string iap;
            public readonly int productType;

            public ShopItem(JSONNode node, string playerId) : base(node, playerId)
            {
                iap = node[iapKey].Value;
                productType = node[productTypeKey].AsInt;
            }
        }
    }
}