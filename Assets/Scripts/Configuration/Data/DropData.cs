using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class DropData
    {
        public List<Drop> drop { get; private set; }

        public DropData(JSONNode node)
        {
            JSONArray dropArray = node.AsArray;
            drop = new List<Drop>();

            for (int i = 0; i < dropArray.Count; i++)
            {
                drop.Add(new Drop(dropArray[i]));
            }
        }

        public class Drop
        {
            private const string idKey = "id";
            private const string nameKey = "name";
            private const string typeKey = "type";
            private const string advertiseKey = "advertise";
            private const string gameTimeKey = "game_time";
            private const string spawnBigChestKey = "spawn_km";
            private const string advGameTimeKey = "advertise_game_time";

            public readonly int id;
            public readonly string name;
            public readonly string type;
            public readonly bool advertise;
            public readonly float gameTime;
            public readonly float advGameTime;
            public readonly List<int> spawnKM;

            public Drop(JSONNode node)
            {
                spawnKM = new List<int>();
                id = node[idKey].AsInt;
                name = node[nameKey].Value;
                advertise = node[advertiseKey].AsBool;
                gameTime = node[gameTimeKey].AsInt;
                type = node[typeKey].Value;
                advGameTime = node[advGameTimeKey].AsInt;

                foreach (var index in node[spawnBigChestKey].AsArray)
                {
                    spawnKM.Add(index.Value.AsInt);
                }

            }
        }
    }
}