using SimpleJSON;
using UnityEngine;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class ResourcesDeepChangeData
    {
        public readonly ResData[] resourcesData;
        public ResourcesDeepChangeData(JSONNode node)
        {
            JSONArray array = node.AsArray;

            resourcesData = new ResData[array.Count];

            for (int i = 0; i < array.Count; i++) resourcesData[i] = new ResData(array[i]);
        }

        public class ResData
        {
            private const string kmKey = "kilometer";

            public readonly int kilometer;
            public readonly float[] resourcesCountArr;

            public ResData(JSONNode node)
            {
                kilometer = node[kmKey].AsInt;

                resourcesCountArr = new float[node.Count - 1];

                for (int i = 0; i < resourcesCountArr.Length; i++) resourcesCountArr[i] = node[(i + 1).ToString()];
            }
        }
    }
}