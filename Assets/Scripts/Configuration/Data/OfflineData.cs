using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class OfflineData
    {
        private const string timeKey = "offline_time_for_x2_storage";
        private const string percentKey = "storage_percent_for_x2_sale";

        public readonly long time;
        public readonly int percent;

        public OfflineData(JSONNode node)
        {
            time = node[timeKey].AsLong;
            percent = node[percentKey].AsInt;
        }
    }
}
