using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class ConditionsData
    {
        public Condition[] conditions;
        public ConditionsData(JSONNode node)
        {
            JSONArray conditionsArray = node.AsArray;
            conditions = new Condition[conditionsArray.Count];

            //0 - brill 1 - upgrade
            for (int i = 0; i < conditionsArray.Count; i++)
            {
                conditions[i] = new Condition(conditionsArray[i]);
            }
        }

        public class Condition
        {
            private const string idKey = "id";
            private const string nameKey = "name";
            private const string onlineTimeKey = "online_time";
            private const string dontTouchBrillTimeKey = "dont_touch_brill_time";
            private const string dontSpawnBrillTimeKey = "dont_spawn_brill_time";
            private const string withoutAdsTimeKey = "without_ads_time";
            private const string firstUpOnlineKey = "first_up_online";
            private const string lastUpgradeTimeKey = "last_upgrade_time";

            public readonly int id;
            public readonly string name;
            public readonly float onlineTime;
            public readonly float dontTouchBrillTime;
            public readonly float dontSpawnBrillTime;
            public readonly float withoutAdsTime;
            public readonly float firstUpOnline;
            public readonly float lastUpgradeTime;

            public Condition(JSONNode node)
            {
                id = node[idKey].AsInt;
                name = node[nameKey].Value;
                onlineTime = node[onlineTimeKey].AsFloat;
                dontTouchBrillTime = node[dontTouchBrillTimeKey].AsFloat;
                dontSpawnBrillTime = node[dontSpawnBrillTimeKey].AsFloat;
                withoutAdsTime = node[withoutAdsTimeKey].AsFloat;
                firstUpOnline = node[firstUpOnlineKey].AsFloat;
                lastUpgradeTime = node[lastUpgradeTimeKey].AsFloat;
            }
        }
    }
}