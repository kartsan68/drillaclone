using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class LocationData
    {
        public readonly Location[] allLocationArray;
        public LocationData(JSONNode node)
        {
            JSONArray array = node.AsArray;
            allLocationArray = new Location[array.Count];

            for (int i = 0; i < allLocationArray.Length; i++) allLocationArray[i] = new Location(array[i]);
        }

        public class Location
        {
            private const string idKey = "id";
            private const string rigidityKey = "earth_rigidity_percent";
            private const string rigidityVisualKey = "earth_rigidity_visual";
            private const string speedMultiplierKey = "speed_multiplier";
            private const string prodMultiplierKey = "production_multiplier";
            private const string startKmKey = "start_km";
            private const string kingBeamHealthKey = "king_beam_health";
            private const string kingBeamAdvPercDamageKey = "king_beam_adv_perc_damage";
            private const string smallBeamHealthKey = "small_beam_health";
            private const string smallBeamAdvPercDamageKey = "small_beam_adv_perc_damage";
            private const string constantSpeedKey = "constant_speed";
            private const string constantVisualKey = "constant_visual_speed";
            private const string bigBeamFuckupTimeKey = "big_beam_fuckup_time";

            public readonly int id;
            public readonly float rigidity;
            public readonly float rigidityVisual;
            public readonly int speedMultiplier;
            public readonly int prodMultiplier;
            public readonly int startKm;
            public readonly int kingBeamHealth;
            public readonly float kingBeamAdvPercDamage;
            public readonly int smallBeamHealth;
            public readonly float smallBeamAdvPercDamage;
            public readonly float constantSpeed;
            public readonly float constantVisualSpeed;
            public readonly float bigBeamFuckupTime;


            public Location(JSONNode node)
            {
                id = node[idKey];
                rigidity = node[rigidityKey];
                rigidityVisual = node[rigidityVisualKey];
                speedMultiplier = node[speedMultiplierKey];
                prodMultiplier = node[prodMultiplierKey];
                startKm = node[startKmKey];
                kingBeamHealth = node[kingBeamHealthKey];
                smallBeamHealth = node[smallBeamHealthKey];
                kingBeamAdvPercDamage = node[kingBeamAdvPercDamageKey].AsFloat;
                smallBeamAdvPercDamage = node[smallBeamAdvPercDamageKey].AsFloat;
                constantSpeed = node[constantSpeedKey].AsFloat;
                constantVisualSpeed = node[constantVisualKey].AsFloat;
                bigBeamFuckupTime = node[bigBeamFuckupTimeKey].AsFloat;
            }
        }
    }
}