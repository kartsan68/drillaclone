using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class RouletteData
    {
        //TODO: сделать массивом
        public RouletteElement[] golds { get; private set; }

        public RouletteData(JSONNode node)
        {
            JSONArray rouletteArray = node.AsArray;
            golds = new RouletteElement[rouletteArray.Count];

            for (int i = 0; i < rouletteArray.Count; i++)
            {
                golds[i] = new RouletteElement(rouletteArray[i]);
            }
        }


        public class RouletteElement
        {
            private const string idKey = "id";
            private const string nameKey = "name";
            private const string chanceKey = "chance_percent";
            private const string gameTimeKey = "game_time";

            public readonly int id;
            public readonly string name;
            public readonly int chancePercent;
            public readonly float gameTime;

            public RouletteElement(JSONNode node)
            {
                id = node[idKey].AsInt;
                name = node[nameKey].Value;
                chancePercent = node[chanceKey].AsInt;
                gameTime = node[gameTimeKey].AsInt;
            }
        }
    }
}
