using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class BoostersData
    {
        public Booster[] boosters;
        public BoostersData(JSONNode node)
        {
            JSONArray boostersArray = node.AsArray;
            boosters = new Booster[boostersArray.Count];

            //0 - click 1 - barrel 2 - magnet
            for (int i = 0; i < boostersArray.Count; i++)
            {
                boosters[i] = new Booster(boostersArray[i]);
            }
        }

        public class Booster
        {
            private const string idKey = "id";
            private const string nameKey = "name";
            private const string workTimeKey = "work_time";
            private const string spawnChanceKey = "spawn_chance";
            private const string multiplierKey = "multiplier";

            public readonly int id;
            public readonly string name;
            public readonly float workTime;
            public readonly float spawnChance;
            public readonly float multiplier;

            public Booster(JSONNode node)
            {
                id = node[idKey].AsInt;
                name = node[nameKey].Value;
                workTime = node[workTimeKey].AsFloat;
                spawnChance = node[spawnChanceKey].AsFloat;
                multiplier = node[multiplierKey].AsFloat;
            }
        }
    }
}