using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class DetailData
    {
        private const string boerKey = "Boer";
        private const string ladleKey = "Ladles";
        private const string engineKey = "Engine";
        private const string storageKey = "Storage";

        public readonly Boer[] boer;
        public readonly Ladle[] ladle;
        public readonly Engine[] engine;
        public readonly Storage[] storage;

        public DetailData(JSONNode node)
        {
            boer = new Boer[node[boerKey].AsArray.Count];
            ladle = new Ladle[node[ladleKey].AsArray.Count];
            engine = new Engine[node[engineKey].AsArray.Count];
            storage = new Storage[node[storageKey].AsArray.Count];

            FillInArray(node);
        }

        private void FillInArray(JSONNode node)
        {
            for (int i = 0; i < boer.Length; i++) boer[i] = new Boer(node[boerKey].AsArray[i]);

            for (int i = 0; i < ladle.Length; i++) ladle[i] = new Ladle(node[ladleKey].AsArray[i]);

            for (int i = 0; i < engine.Length; i++) engine[i] = new Engine(node[engineKey].AsArray[i]);

            for (int i = 0; i < storage.Length; i++) storage[i] = new Storage(node[storageKey].AsArray[i]);
        }

        public class Boer
        {
            private const string lvlKey = "lvl";
            private const string drillSpeedKey = "drill_speed";
            private const string visualSpeedKey = "visual_speed";
            private const string offlineSpeedKey = "offline_speed";
            private const string maxWorkTimeKey = "max_work_time";
            private const string chargePerTapKey = "charge_per_tap";
            private const string priceKey = "price";

            public readonly int lvl;
            public readonly float drillSpeed;
            public readonly float visualDrillSpeed;
            public readonly float offlineDrillSpeed;
            public readonly float maxWorkTime;
            public readonly float chargePerTap;
            public readonly PDouble price;

            public Boer(JSONNode node)
            {
                price = new PDouble();
                lvl = node[lvlKey].AsInt;
                drillSpeed = node[drillSpeedKey].AsFloat;
                visualDrillSpeed = node[visualSpeedKey].AsFloat;
                offlineDrillSpeed = node[offlineSpeedKey].AsFloat;
                price = node[priceKey].AsDouble;
                maxWorkTime = node[maxWorkTimeKey].AsFloat;
                chargePerTap = node[chargePerTapKey].AsFloat;
            }
        }

        public class Ladle
        {
            private const string lvlKey = "lvl";
            private const string clickMultiplierKey = "click_multiplier_x";
            private const string multiplierKey = "ladle_multiplier";
            private const string priceKey = "price";

            public readonly int lvl;
            public readonly float multiplier;
            public readonly float clickMultiplier;
            public readonly PDouble price;
            public Ladle(JSONNode node)
            {
                price = new PDouble();
                lvl = node[lvlKey].AsInt;
                multiplier = node[multiplierKey].AsFloat;
                clickMultiplier = node[clickMultiplierKey].AsFloat;
                price = node[priceKey].AsDouble;
            }
        }

        public class Engine
        {
            private const string lvlKey = "lvl";
            private const string priceKey = "price";
            private const string otherDetailKey = "max_other_detail_lvl";
            private const string advCountKey = "adv_count";

            public readonly int lvl;
            public readonly PDouble price;
            public readonly int otherDetailLvl;
            public readonly int advCount;

            public Engine(JSONNode node)
            {
                price = new PDouble();
                lvl = node[lvlKey].AsInt;
                price = node[priceKey].AsDouble;
                otherDetailLvl = node[otherDetailKey].AsInt;
                advCount = node[advCountKey].AsInt;
            }
        }

        public class Storage
        {
            private const string lvlKey = "lvl";
            private const string capacityKey = "capacity";
            private const string priceKey = "price";

            public readonly int lvl;
            public readonly double capacity;
            public readonly PDouble price;

            public Storage(JSONNode node)
            {
                price = new PDouble();
                lvl = node[lvlKey].AsInt;
                capacity = node[capacityKey].AsDouble;
                price = node[priceKey].AsDouble;
            }
        }
    }
}