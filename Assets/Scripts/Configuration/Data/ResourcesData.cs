using SimpleJSON;

namespace BlackBears.DiggIsBig.Configuration.Data
{
    public class ResourcesData
    {
        public readonly Resource[] resources;

        public ResourcesData(JSONNode node)
        {
            JSONArray resArray = node.AsArray;

            resources = new Resource[resArray.Count];

            for (int i = 0; i < resArray.Count; i++) resources[i] = new Resource(resArray[i]);
        }

        public class Resource
        {
            private const string idKey = "id";
            private const string typeKey = "type";
            private const string priceKey = "start_loot_price";
            private const string startCountKey = "start_loot_count";
            private const string unlockKey = "unlock_price";
            private const string openDeepKey = "open_deep_km";
            private const string unlockTimeKey = "unlock_time";
            private const string unlockAdvCountKey = "unlock_adv_count";

            public readonly int id;
            public readonly string type;
            public readonly double lootPrice;
            public readonly double startCount;
            public readonly double unlockPrice;
            public readonly int openDeep;
            public readonly int unlockAdvCount;
            public readonly float unlockTime;

            public Resource(JSONNode node)
            {
                id = node[idKey].AsInt;
                type = node[typeKey].Value;
                lootPrice = node[priceKey].AsDouble;
                startCount = node[startCountKey].AsDouble;
                unlockPrice = node[unlockKey].AsDouble;
                openDeep = node[openDeepKey].AsInt;
                unlockTime = node[unlockTimeKey].AsFloat;
                unlockAdvCount = node[unlockAdvCountKey].AsInt;
            }
        }
    }
}
