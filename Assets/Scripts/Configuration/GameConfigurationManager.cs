﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration.Data;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.DiggIsBig.Configuration
{
    public class GameConfigurationManager
    {
        private const string spawnKey = "Spawn";
        private const string boostersKey = "Boosters";
        private const string rouletteKey = "Roulette";
        private const string dropKey = "Drop";
        private const string shopKey = "Shop";
        private const string resourcesKey = "Resources";
        private const string advertiseKey = "Advertise";
        private const string detailKey = "Detail";
        private const string locationKey = "Location";
        private const string caveKey = "Cave";
        private const string offlineKey = "Offline";
        private const string conditionsKey = "Conditions";
        private const string resourceDeepChangeKey = "Resource_deep_change";
        private const string shopItemDataKey = "Shop_items";
        private const string clanDataKey = "Clan";

        public readonly ClanData clanData;
        public readonly ShopItemData shopItemData;
        public readonly SpawnData spawnData;
        public readonly RouletteData rouletteData;
        public readonly DropData dropData;
        public readonly ShopData shopData;
        public readonly ResourcesData resourcesData;
        public readonly AdvertiseData advertiseData;
        public readonly DetailData detailData;
        public readonly BoostersData boostersData;
        public readonly LocationData locationData;
        public readonly OfflineData offlineData;
        public readonly ResourcesDeepChangeData resourcesDeepChangeData;
        public readonly ConditionsData conditionsData;

        public GameConfigurationManager()
        {
            JSONNode node = GetConfiguration();

            spawnData = new SpawnData(node[spawnKey]);
            rouletteData = new RouletteData(node[rouletteKey]);
            dropData = new DropData(node[dropKey]);
            shopData = new ShopData(node[shopKey]);
            resourcesData = new ResourcesData(node[resourcesKey]);
            advertiseData = new AdvertiseData(node[advertiseKey]);
            detailData = new DetailData(node[detailKey]);
            locationData = new LocationData(node[locationKey]);
            resourcesDeepChangeData = new ResourcesDeepChangeData(node[resourceDeepChangeKey]);
            offlineData = new OfflineData(node[offlineKey]);
            boostersData = new BoostersData(node[boostersKey]);
            conditionsData = new ConditionsData(node[conditionsKey]);
            shopItemData = new ShopItemData(node[shopItemDataKey]);
            clanData = new ClanData(node[clanDataKey]);
        }

        private JSONNode GetConfiguration() => JSON.Parse(LocalizeAndConfigPas.GetConfigurationFile());
    }
}
