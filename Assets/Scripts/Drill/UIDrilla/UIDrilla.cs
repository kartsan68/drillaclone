﻿using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Drill
{
    public class UIDrilla : MonoBehaviour
    {
        [SerializeField] private GameObject ladlesContainer;

        [SerializeField] private SkeletonGraphic engine;
        [SerializeField] private SkeletonGraphic battery;
        [SerializeField] private SkeletonGraphic topHull;
        [SerializeField] private Image midHull;
        [SerializeField] private Image botHull;
        [SerializeField] private Image storage;
        [SerializeField] private Image boer;

        [SerializeField] private Sprite[] hullMidSprites;
        [SerializeField] private Sprite[] hullBotSprites;
        [SerializeField] private Sprite[] storageSprites;
        [SerializeField] private Sprite[] boerSprites;
        [SerializeField] private GameObject[] ladlesSpare;
        [SerializeField] private SkeletonDataAsset[] hullTopSkeleton;
        [SerializeField] private SkeletonDataAsset[] engineSkeletons;
        [SerializeField] private SkeletonDataAsset[] batterySkeleton;

        private int[] swapDetaillvls = { 0, 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52, 55, 58, 61, 64, 67, 70, 73, 76, 79, 82, 85, 88, 200 };

        public void SetDetail(int detailIndex, int detailLvl)
        {
            // 0 - двигатель, 1 - бур, 2 - ковши, 3 - склад
            switch (detailIndex)
            {
                case 0:
                    SetEngine(detailLvl);
                    break;
                case 1:
                    SetBoer(detailLvl);
                    break;
                case 2:
                    SetLadles(detailLvl);
                    break;
                case 3:
                    SetStorage(detailLvl);
                    break;
            }
        }

        public void SetAllDetails(int[] detailsLvl)
        {
            SetEngine(detailsLvl[0] - 1);
            SetBoer(detailsLvl[1] - 1);
            SetLadles(detailsLvl[2] - 1);
            SetStorage(detailsLvl[3] - 1);
        }

        private int[] hulChangeLvls = { 0, 7, 16, 25, 34, 43, 52, 61, 70, 79, 200 };
        private int alredySetElement = 0;
        private void SetEngine(int detailLvl)
        {

            for (int i = 0; i < swapDetaillvls.Length - 1; i++)
            {
                if (detailLvl == swapDetaillvls[i])
                {
                    alredySetElement = i;
                    engine.skeletonDataAsset = engineSkeletons[i];
                    battery.skeletonDataAsset = batterySkeleton[i];
                    ReinitSkeleton(engine);
                    ReinitSkeleton(battery);
                    for (int j = 0; j < hulChangeLvls.Length - 1; j++)
                    {
                        if (detailLvl >= hulChangeLvls[j] && detailLvl < hulChangeLvls[j + 1])
                        {
                            topHull.skeletonDataAsset = hullTopSkeleton[j];
                            midHull.sprite = hullMidSprites[j];
                            botHull.sprite = hullBotSprites[j];
                            ReinitSkeleton(topHull);
                            break;
                        }
                    }
                    break;
                }
                else if (detailLvl < swapDetaillvls[i])
                {
                    i = i - 1;

                    if (i == alredySetElement) break;
                    else alredySetElement = i;

                    engine.skeletonDataAsset = engineSkeletons[i];
                    battery.skeletonDataAsset = batterySkeleton[i];
                    ReinitSkeleton(engine);
                    ReinitSkeleton(battery);
                    for (int j = 0; j < hulChangeLvls.Length - 1; j++)
                    {
                        if (detailLvl >= hulChangeLvls[j] && detailLvl < hulChangeLvls[j + 1])
                        {
                            topHull.skeletonDataAsset = hullTopSkeleton[j];
                            midHull.sprite = hullMidSprites[j];
                            botHull.sprite = hullBotSprites[j];
                            ReinitSkeleton(topHull);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        private void SetBoer(int detailLvl)
        {
            for (int i = 0; i < swapDetaillvls.Length - 1; i++)
            {
                if (detailLvl >= swapDetaillvls[i] && detailLvl < swapDetaillvls[i + 1])
                {
                    boer.sprite = boerSprites[i];
                    break;
                }
            }
        }

        private void SetLadles(int detailLvl)
        {
            for (int i = 0; i < swapDetaillvls.Length - 1; i++)
            {
                if (detailLvl >= swapDetaillvls[i] && detailLvl < swapDetaillvls[i + 1])
                {
                    Destroy(ladlesContainer.transform.GetChild(0).gameObject);
                    Instantiate(ladlesSpare[i], ladlesContainer.transform.position, Quaternion.identity, ladlesContainer.transform);
                    break;
                }
            }
        }

        private void ReinitSkeleton(SkeletonGraphic skeleton)
        {
            skeleton.Clear();
            skeleton.Initialize(true);
        }

        private void SetStorage(int detailLvl)
        {
            for (int i = 0; i < swapDetaillvls.Length - 1; i++)
            {
                if (detailLvl >= swapDetaillvls[i] && detailLvl < swapDetaillvls[i + 1])
                {
                    storage.sprite = storageSprites[i];
                    break;
                }
            }
        }
    }
}
