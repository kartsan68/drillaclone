﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.GameElements.Elements;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Save;
using UnityEngine.UI;
using BlackBears.DiggIsBig.UI;

namespace BlackBears.DiggIsBig.Drill
{
    public class Drill : MonoBehaviour
    {
        #region Var
        [SerializeField] private float tapScale;
        [SerializeField] private Color tapColor;
        [SerializeField] private float tapTime;

        [SerializeField] private Animator detailAnimator;
        [SerializeField] private GameObject detail2DSpot;
        [SerializeField] private GameObject tapBar;
        [SerializeField] private StorageController storageController;
        [SerializeField] private CameraController cameraController;
        [SerializeField] private UpgradeLabelCanvas upgradeLabelCanvas;
        [SerializeField] private GameObject beamMask;
        [SerializeField] private GameObject container;
        [SerializeField] private GameObject animationContainer;
        [SerializeField] private float powerAnimationScale;
        [SerializeField] private float usualAnimationScale;
        [SerializeField] private ParticleSystem[] setParticles;
        [SerializeField] private ParticleSystem[] outParticles;
        [SerializeField] private Animator drillReactionAnimator;

        private double drillSpeed;
        private int actualEffectNumber;
        private GameController gameController;
        private bool speedBoostEffect;
        private bool prodBoostEffect;
        private bool paused;
        private bool beamState;
        private float actualBoost;
        private float animationPowerControl;
        private float animationStorageControl;
        private float actualAnimationScale;
        private Animator drillAnimator;
        private Battery battery;
        private Ladles ladles;
        private Boer boer;
        private Engine engine;
        private Hull hull;
        private Storage storage;
        private bool boerException;
        private bool setDetailState;
        #endregion

        #region Save/Load
        private void Save()
        {
            List<SaveData> data = new List<SaveData>();

            data.Add(new SaveData(SaveKeys.actualBoerSpeedKey, boer.offlineDrillSpeed));
            SaveCreator.Save(SaveKeys.drillSaveKey, data);
        }
        #endregion
        #region MonoBehaviour
        private void Awake()
        {
            SubscribeToEvents();
            VarInit();
            SetSpeed();

            StartCoroutine(IncomeTicker());
            drillAnimator = gameObject.GetComponent<Animator>();
        }
        #endregion
        #region DrillControl
        private void DrillaStart()
        {
            if (!boerException) SetAllAnimationTimeScales((usualAnimationScale + (powerAnimationScale * animationPowerControl)) * animationStorageControl);
        }

        private void DrillaStop()
        {
            if (!boerException) SetAllAnimationTimeScales(0);
        }

        private IEnumerator SetDrillInNormalPosition()
        {
            while (isUpState) yield return null;
            container.transform.localPosition = new Vector3(0, 0, 0);
        }

        private void SetAllAnimationTimeScales(float scales)
        {
            if (scales < 0) scales = 0;

            actualAnimationScale = scales;
            boer.SetAnimationsTimeScale(scales);
            engine.SetAnimationsTimeScale(scales);
            ladles.SetAnimationsTimeScale(scales);
            battery.SetAnimationsTimeScale(scales);
            hull.SetAnimationsTimeScale(scales);

            //проверить
            // detailAnimator.speed = scales;
            if (scales == 0)
                detailAnimator.speed = 0;
            else detailAnimator.speed = 1;
        }

        // private void UpAnimationSpeed()
        // {
        //     animationScale = 1.5f;
        //     SetAllAnimationTimeScales((0.5f + (animationScale * animationPowerControl)) * animationStorageControl);
        // }

        // private void DownAnimationSpeed()
        // {
        //     animationScale = 0.5f;
        //     SetAllAnimationTimeScales((0.5f + (animationScale * animationPowerControl)) * animationStorageControl);
        // }


        private void SetSpeed()
        {
            drillSpeed = ConvertToMeterPerSeconds(boer.drillSpeed);

            gameController.EditDrillSpeed(drillSpeed * actualBoost, ConvertToMeterPerSeconds(boer.visualDrillSpeed), true);

            if (beamState)
            {
                gameController.GamePause();
            }
        }

        private double ConvertToMeterPerSeconds(double speed) => speed / 60;


        #endregion
        #region Coroutines and Up
        private void OnUpgradeCameraMode()
        {
            EventManager.HideUIElementsEvent(false);
            EventManager.UpgradeModeCameraEvent(true);
        }
        private void OffUpgradeCameraMode()
        {
            EventManager.HideUIElementsEvent(true);
            EventManager.UpgradeModeCameraEvent(false);
            EventManager.ClearChildEvent();
        }

        private void DrillOnPos() => drillAnimator.SetTrigger("SetUpDownDrillLocation");

        private IEnumerator IncomeTicker()
        {
            while (true)
            {
                while (paused)
                {
                    if (beamState)
                    {
                        yield return new WaitForSeconds(1);
                        storageController.Income(ladles.ladleMultipler);
                    }
                    else
                    {
                        yield return null;
                    }
                }
                yield return new WaitForSeconds(1);
                storageController.Income(ladles.ladleMultipler);
            }
        }
        //закончено
        private IEnumerator SwapBoerDetail(Boer newBoer, int lvl, int oldlvl)
        {
            StartSwap();
            EventManager.BoerSwapEvent(newBoer.GetComponent<Detail>().lvl);

            yield return new WaitForSeconds(1f);
            upgradeLabelCanvas.StartUpdate(1, lvl, oldlvl);
            yield return new WaitForSeconds(0.2f);

            Boer boer = Instantiate(newBoer, new Vector3(0, -600, 0), Quaternion.identity, animationContainer.transform);
            boer.SetAnimationsTimeScale(0);
            boer.SetAllSetUnsetElements(setParticles[0], outParticles[0], drillReactionAnimator, () => { }, () =>
                {
                    Destroy(this.boer.gameObject);
                    this.boer = boer;
                });

            boer.gameObject.SetActive(false);

            this.boer.SetAllSetUnsetElements(setParticles[0], outParticles[0], drillReactionAnimator,
            () => boer.SetDetail(), () => { });

            this.boer.DisconnectDetail();
            yield return new WaitForSeconds(4.5f);
            SetSpeed();
            this.boer.SetAnimationsTimeScale(actualAnimationScale);
            EndSwap();
        }

        private IEnumerator SwapLadleDetail(Ladles newLadles, int lvl, int oldlvl)
        {
            StartSwap();
            ParticleSystem[] ladlePaticlesSet = new ParticleSystem[] { setParticles[1], setParticles[2] };
            ParticleSystem[] ladlePaticlesOut = new ParticleSystem[] { outParticles[1], outParticles[2] };
            yield return new WaitForSeconds(1f);
            upgradeLabelCanvas.StartUpdate(2, lvl, oldlvl);
            yield return new WaitForSeconds(0.2f);

            Ladles ladles = Instantiate(newLadles, this.ladles.transform.position, Quaternion.identity, detail2DSpot.transform);
            ladles.SetAnimationsTimeScale(0);

            ladles.SetAllSetUnsetElements(ladlePaticlesSet, ladlePaticlesOut, drillReactionAnimator, () => { }, () =>
            {
                Destroy(this.ladles.gameObject);
                this.ladles = ladles;
            });
            ladles.gameObject.SetActive(false);

            this.ladles.SetAllSetUnsetElements(ladlePaticlesSet, ladlePaticlesOut, drillReactionAnimator,
            () => ladles.SetDetail(), () => { });

            this.ladles.DisconnectDetail();
            yield return new WaitForSeconds(4.5f);

            GameData.GameData.actualClickMultiplier = ladles.clickResMultiplier;
            this.ladles.SetAnimationsTimeScale(actualAnimationScale);
            EventManager.LadlesSwapEvent(ladles.ladleMultipler);
            EndSwap();
        }

        private IEnumerator SwapStorageDetail(Storage newStorage, int lvl, int oldlvl)
        {
            StartSwap();

            yield return new WaitForSeconds(1f);
            upgradeLabelCanvas.StartUpdate(3, lvl, oldlvl);
            yield return new WaitForSeconds(0.2f);

            Storage storage = Instantiate(newStorage, this.storage.transform.position, Quaternion.identity, detail2DSpot.transform);
            storage.SetAllSetUnsetElements(setParticles[3], outParticles[3], drillReactionAnimator, () => { }, () =>
            {
                Destroy(this.storage.gameObject);
                this.storage = storage;
            });
            storage.gameObject.SetActive(false);

            this.storage.SetAllSetUnsetElements(setParticles[3], outParticles[3], drillReactionAnimator,
                                                () => storage.SetDetail(), () => { });
            this.storage.DisconnectDetail();

            yield return new WaitForSeconds(4.5f);

            EndSwap();
            EventManager.UpStorageEvent(newStorage.GetComponent<Detail>().lvl);
        }

        private IEnumerator SwapBatteryAndEngineDetail(Battery newBattery, Engine newEngine, Hull newHull, int lvl, int oldlvl)
        {
            StartSwap();
            yield return new WaitForSeconds(1f);
            upgradeLabelCanvas.StartUpdate(0, lvl, oldlvl);
            yield return new WaitForSeconds(0.2f);

            //Hull
            Hull hull = Instantiate(newHull, this.hull.transform.position, Quaternion.identity, detail2DSpot.transform);
            hull.SetAllSetUnsetElements(() => { }, () =>
            {
                Destroy(this.hull.gameObject);
                this.hull = hull;
            });
            hull.gameObject.SetActive(false);

            this.hull.SetAllSetUnsetElements(() => hull.SetDetail(), () => { });
            this.hull.DisconnectDetail();
            //Battery
            Battery battery = Instantiate(newBattery, this.battery.transform.position, Quaternion.identity, detail2DSpot.transform);
            battery.SetAllSetUnsetElements(setParticles[4], outParticles[4], () => { }, () =>
             {
                 Destroy(this.battery.gameObject);
                 this.battery = battery;
             });
            battery.gameObject.SetActive(false);

            this.battery.SetAllSetUnsetElements(setParticles[4], outParticles[4], () => battery.SetDetail(), () => { });
            this.battery.DisconnectDetail();
            //Engine
            Engine engine = Instantiate(newEngine, this.engine.transform.position, Quaternion.identity, detail2DSpot.transform);
            engine.SetAllSetUnsetElements(setParticles[5], outParticles[5], () => { }, () =>
             {
                 Destroy(this.engine.gameObject);
                 this.engine = engine;
             });
            engine.gameObject.SetActive(false);

            this.engine.SetAllSetUnsetElements(setParticles[5], outParticles[5], () => engine.SetDetail(), () => { });
            this.engine.DisconnectDetail();

            hull.SetAnimationsTimeScale(0);
            battery.SetAnimationsTimeScale(0);
            engine.SetAnimationsTimeScale(0);

            yield return new WaitForSeconds(4.5f);
            this.hull.SetAnimationsTimeScale(actualAnimationScale);
            this.battery.SetAnimationsTimeScale(actualAnimationScale);
            this.engine.SetAnimationsTimeScale(actualAnimationScale);

            EndSwap();

            if (lvl == 5 || ((lvl % 3) == 0 && lvl > 5))
                RateManager.RateCall();
        }

        private void SilentSwapBoerDetail(Boer newBoer)
        {
            gameController.GamePause();
            //!!!
            EventManager.BoerSwapEvent(newBoer.GetComponent<Detail>().lvl);
            //!!!
            Boer boer = Instantiate(newBoer, this.boer.transform.position, Quaternion.identity, container.transform);
            Destroy(this.boer.gameObject);
            this.boer = boer;
            boer.SetAnimationsTimeScale(actualAnimationScale);
            SetSpeed();
            DrillOnPosition();
        }

        private void SilentSwapLadleDetail(Ladles newLadles)
        {
            gameController.GamePause();
            Ladles ladles = Instantiate(newLadles, this.ladles.transform.position, Quaternion.identity, detail2DSpot.transform);
            Destroy(this.ladles.gameObject);
            this.ladles = ladles;
            this.ladles.SetAnimationsTimeScale(actualAnimationScale);
            GameData.GameData.actualClickMultiplier = ladles.clickResMultiplier;
            EventManager.LadlesSwapEvent(ladles.ladleMultipler);
            DrillOnPosition();
        }

        private void SilentSwapBatteryAndEngineDetail(Battery newBattery, Engine newEngine, Hull newHull, int lvl)
        {
            gameController.GamePause();
            Battery battery = Instantiate(newBattery, this.battery.transform.position, Quaternion.identity, detail2DSpot.transform);
            Engine engine = Instantiate(newEngine, this.engine.transform.position, Quaternion.identity, detail2DSpot.transform);
            Hull hull = Instantiate(newHull, this.hull.transform.position, Quaternion.identity, detail2DSpot.transform);
            Destroy(this.battery.gameObject);
            Destroy(this.engine.gameObject);
            Destroy(this.hull.gameObject);
            this.hull = hull;
            this.battery = battery;
            this.engine = engine;
            this.hull.SetAnimationsTimeScale(actualAnimationScale);
            this.battery.SetAnimationsTimeScale(actualAnimationScale);
            this.engine.SetAnimationsTimeScale(actualAnimationScale);
            DrillOnPosition();

            if (lvl == 5 || ((lvl % 3) == 0 && lvl > 5))
                RateManager.RateCall();
        }

        private void SilentSwapStorageDetail(Storage newStorage)
        {
            gameController.GamePause();
            Storage storage = Instantiate(newStorage, this.storage.transform.position, Quaternion.identity, detail2DSpot.transform);
            Destroy(this.storage.gameObject);
            this.storage = storage;
            DrillOnPosition();
            EventManager.UpStorageEvent(newStorage.GetComponent<Detail>().lvl);
        }

        #endregion

        #region Other

        public void SwapBoer(Boer newBoer, int lvl, int oldLvl, bool isDetailSwap = true)
        {
            if (isDetailSwap) StartCoroutine(SwapBoerDetail(newBoer, lvl, oldLvl));
            else SilentSwapBoerDetail(newBoer);
        }
        public void SwapLadle(Ladles newLadles, int lvl, int oldLvl, bool isDetailSwap = true)
        {
            if (isDetailSwap) StartCoroutine(SwapLadleDetail(newLadles, lvl, oldLvl));
            else SilentSwapLadleDetail(newLadles);
        }
        public void SwapBatteryAndEngine(Battery newBattery, Engine newEngine, Hull hull, int lvl, int oldLvl, bool isDetailSwap = true)
        {
            if (isDetailSwap) StartCoroutine(SwapBatteryAndEngineDetail(newBattery, newEngine, hull, lvl, oldLvl));
            else SilentSwapBatteryAndEngineDetail(newBattery, newEngine, hull, lvl);
        }
        public void SwapStorage(Storage storage, int lvl, int oldLvl, bool isDetailSwap = true)
        {
            if (isDetailSwap) StartCoroutine(SwapStorageDetail(storage, lvl, oldLvl));
            else SilentSwapStorageDetail(storage);
        }

        public void SwapDetailOnLoad(Boer newBoer, Ladles newLadles, Storage newStorage, Battery newBattery, Engine newEngine, Hull newHull)
        {
            //!!!
            EventManager.BoerSwapEvent(newBoer.GetComponent<Detail>().lvl);
            //!!!

            Boer boer = Instantiate(newBoer, this.boer.transform.position, Quaternion.identity, container.transform);
            Destroy(this.boer.gameObject);
            this.boer = boer;
            boer.SetAnimationsTimeScale(actualAnimationScale);
            SetSpeed();

            Ladles ladles = Instantiate(newLadles, this.ladles.transform.position, Quaternion.identity, detail2DSpot.transform);
            Destroy(this.ladles.gameObject);
            this.ladles = ladles;
            this.ladles.SetAnimationsTimeScale(1);
            GameData.GameData.actualClickMultiplier = ladles.clickResMultiplier;
            EventManager.LadlesSwapEvent(ladles.ladleMultipler);

            Battery battery = Instantiate(newBattery, this.battery.transform.position, Quaternion.identity, detail2DSpot.transform);
            Engine engine = Instantiate(newEngine, this.engine.transform.position, Quaternion.identity, detail2DSpot.transform);
            Hull hull = Instantiate(newHull, this.hull.transform.position, Quaternion.identity, detail2DSpot.transform);
            Destroy(this.battery.gameObject);
            Destroy(this.engine.gameObject);
            Destroy(this.hull.gameObject);
            this.hull = hull;
            this.battery = battery;
            this.engine = engine;
            this.hull.SetAnimationsTimeScale(1);
            this.battery.SetAnimationsTimeScale(1);
            this.engine.SetAnimationsTimeScale(1);

            Storage storage = Instantiate(newStorage, this.storage.transform.position, Quaternion.identity, detail2DSpot.transform);
            Destroy(this.storage.gameObject);
            this.storage = storage;

            DrillOnPosition();

        }

        public void SetFirstLvlConfigData(Boer newBoer, Ladles newLadles, Battery newBattery)
        {
            //!!!
            EventManager.BoerSwapEvent(newBoer.GetComponent<Detail>().lvl);
            //!!!
            boer.drillSpeed = newBoer.drillSpeed;
            boer.offlineDrillSpeed = newBoer.offlineDrillSpeed;
            boer.visualDrillSpeed = newBoer.visualDrillSpeed;

            ladles.ladleMultipler = newLadles.ladleMultipler;
            ladles.clickResMultiplier = newLadles.clickResMultiplier;
            GameData.GameData.actualClickMultiplier = ladles.clickResMultiplier;
            gameController.RecalcCPS();
            SetSpeed();
            drillAnimator.SetTrigger("StartWithoutLoad");
        }
        public void BoerExceptionUnset()
        {
            beamState = false;
            beamMask.SetActive(beamState);
            boerException = false;
            DrillaStart();
        }

        public void BoerExceptionSet()
        {
            beamState = true;
            beamMask.SetActive(beamState);
            boerException = true;
            SetAllAnimationTimeScales(beamTimeScales);
        }

        private void VarInit()
        {
            animationPowerControl = 1f;
            animationStorageControl = 1f;
            actualEffectNumber = 0;
            paused = false;
            beamState = false;
            beamMask.SetActive(beamState);
            boerException = false;
            setDetailState = false;
            speedBoostEffect = false;
            prodBoostEffect = false;
            battery = gameObject.GetComponentInChildren<Battery>();
            hull = gameObject.GetComponentInChildren<Hull>();
            boer = gameObject.GetComponentInChildren<Boer>();
            ladles = gameObject.GetComponentInChildren<Ladles>();
            storage = gameObject.GetComponentInChildren<Storage>();
            GameData.GameData.actualClickMultiplier = ladles.clickResMultiplier;
            actualBoost = 1;
            engine = gameObject.GetComponentInChildren<Engine>();
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        }

        private void StartSwap()
        {
            isUpState = true;
            SetAllAnimationTimeScales(0);
            beamMask.SetActive(false);
            EventManager.UpgradeStateEvent(true);
            setDetailState = true;
            gameController.BlockingTouchAreaTrigger();
            drillAnimator.SetTrigger("SetUpDownDrillLocation");
        }

        private void EndSwap()
        {
            container.transform.Translate(0, afkDeep, 0);
            afkDeep = 0;
            isUpState = false;
            if (beamState) SetAllAnimationTimeScales(beamTimeScales);
            else DrillaStart();

            beamMask.SetActive(beamState);
            EventManager.UpgradeStateEvent(false);
            gameController.BlockingTouchAreaTrigger();
            setDetailState = false;
        }

        private float beamTimeScales = 1f;

        private IEnumerator TapOnBeam()
        {
            if (beamTimeScales != 2.5f)
                beamTimeScales = 2.5f;

            SetAllAnimationTimeScales(beamTimeScales);
            float time = 0;
            while (time < 2)
            {
                yield return null;
                if (beamTimeScales != 2.5f)
                {
                    beamTimeScales = 2.5f;
                    SetAllAnimationTimeScales(beamTimeScales);
                }
                time += Time.deltaTime;

                if (paused && !beamState)
                {
                    DrillaStop();
                    beamTimeScales = 1f;
                }
            }
            beamTimeScales = 1f;

            if (beamState) SetAllAnimationTimeScales(beamTimeScales);
            else if (paused) DrillaStop();
            else DrillaStart();
        }

        private bool isUpState = false;
        private float afkDeep = 0;

        private void MoveThroughTheBeam(float step)
        {
            if (step == 0)
            {
                afkDeep = 0;
                StartCoroutine(SetDrillInNormalPosition());
            }
            else if (isUpState)
            {
                afkDeep += step;
            }
            else
            {
                afkDeep = 0;
                container.transform.Translate(0, step, 0);
            }
        }

        private void SubscribeToEvents()
        {
            EventManager.MoveThroughTheBeam += MoveThroughTheBeam;
            EventManager.SaveGame += Save;
            EventManager.UpgradeAnimationEnd += DrillOnPos;
            EventManager.BeamSpawn += (a, b) => BoerExceptionSet();
            EventManager.BeamCrash += () =>
            {
                StartCoroutine(SetDrillInNormalPosition());
                BoerExceptionUnset();
            };
            EventManager.GamePaused += OnPause;
            EventManager.GameUnpaused += OffPause;
            EventManager.TapOnBeam += () => StartCoroutine(TapOnBeam());
            EventManager.DrillaStartStop += (isStop) =>
            {
                if (!beamState)
                {
                    if (isStop)
                    {
                        if (!beamState) DrillaStop();
                    }
                    else
                    {
                        if (!paused) DrillaStart();
                    }
                }
            };

            EventManager.AnimationSpeedControl += (a) =>
            {
                animationPowerControl = a;
                if (!beamState)
                {
                    if (actualAnimationScale == 0)
                        DrillaStop();
                    else
                        DrillaStart();
                }

            };

            EventManager.AnimationStorageControl += (a) =>
            {
                animationStorageControl = a;
                if (!beamState)
                {
                    if (actualAnimationScale == 0)
                        DrillaStop();
                    else
                        DrillaStart();
                }
            };

            EventManager.PowerCharge += () => StartCoroutine(PushTheDrill());
        }

        private IEnumerator PushTheDrill()
        {
            container.transform.localScale *= tapScale;
            //swap storage color 
            storage.TapReaction(tapColor);
            //swap ladles color
            ladles.TapReaction(tapColor);
            //swap engine color 
            engine.TapReaction(tapColor);
            //swap battery color 
            battery.TapReaction(tapColor);
            //swap hull color 
            hull.TapReaction(tapColor);
            //swap boer color
            boer.TapReaction(tapColor);

            yield return new WaitForSeconds(tapTime);

            container.transform.localScale /= tapScale;
            Color whiteColor = new Color(1f, 1f, 1f, 1f);

            //back storage color 
            storage.TapReaction(whiteColor);
            //back ladles color 
            ladles.TapReaction(whiteColor);
            //back engine color 
            engine.TapReaction(whiteColor);
            //back battery color 
            battery.TapReaction(whiteColor);
            //back hull color 
            hull.TapReaction(whiteColor);
            //back boer color 
            boer.TapReaction(whiteColor);
        }

        private void OffPause()
        {
            paused = false;
            DrillaStart();
        }

        private void OnPause()
        {
            paused = true;
            DrillaStop();
        }
        #endregion
        #region Animation Events
        public void DrillOnPosition()
        {
            if (beamState) gameController.GamePause();
            else gameController.GameUnpause();
        }
        #endregion
    }
}
