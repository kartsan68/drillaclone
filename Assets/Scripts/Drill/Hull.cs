﻿using UnityEngine;
using Spine.Unity;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Drill
{
    public class Hull : MonoBehaviour
    {
        private UnityAction disconnectionAction;
        private UnityAction setAction;
        private SkeletonAnimation hull;
        public float otherDetailMinLvl = 0;
        public float otherDetailMaxLvl = 0;
        private SpriteRenderer middleHull;
        private SpriteRenderer bottomHull;

        public void SetAllSetUnsetElements(UnityAction disconnectionAction, UnityAction setAction)
        {
            this.disconnectionAction = disconnectionAction;
            this.setAction = setAction;
        }
        public void SetAnimationsTimeScale(float scale) => hull.timeScale = scale;

        public void DisconnectDetail() => GetComponent<Animator>().Play("HullMakeOut");

        public void SetDetail()
        {
            this.gameObject.SetActive(true);
            this.transform.localScale = new Vector3(0, 0, 0);
            GetComponent<Animator>().Play("HullSet");
        }

        private void Awake() => InitData();

        private void InitData()
        {
            hull = gameObject.GetComponentInChildren<SkeletonAnimation>();
            var renderers = GetComponentsInChildren<SpriteRenderer>();
            middleHull = renderers[0];
            bottomHull = renderers[1];
        }

        #region AnimationEvents
        public void StartNextHullSet() => disconnectionAction?.Invoke();

        public void SetNormalScale() => this.transform.localScale = new Vector3(1, 1, 1);
        public void EndDisconnectionAnimation()
        { }

        public void EndHullElementsSet() => setAction?.Invoke();
        #endregion

        public void TapReaction(Color color)
        {
            hull.skeleton.SetColor(color);
            middleHull.color = color;
            bottomHull.color = color;
        }
    }
}
