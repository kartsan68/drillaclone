﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Drill
{
    public class Storage : MonoBehaviour
    {
        private ParticleSystem smoke;
        private ParticleSystem spark;
        private UnityAction disconnectionAction;
        private UnityAction setAction;
        private Animator animator;
        private Animator drillAnimator;
        private SpriteRenderer spriteRenderer;


        public void SetAllSetUnsetElements(ParticleSystem smoke, ParticleSystem spark, Animator drillAnimator,
              UnityAction disconnectionAction, UnityAction setAction)
        {
            this.smoke = smoke;
            this.spark = spark;
            this.disconnectionAction = disconnectionAction;
            this.drillAnimator = drillAnimator;
            this.setAction = setAction;
        }

        public void DisconnectDetail() => GetComponent<Animator>().Play("HullMakeOut");

        public void SetDetail()
        {
            this.gameObject.SetActive(true);
            GetComponent<Animator>().Play("HullSet");
        }

        #region AnimationEvents
        public void DisconnectionStorage() { }

        public void EndDisconnectionAnimation() => disconnectionAction?.Invoke();

        public void SetStorage()
        {
            drillAnimator.Play("StorageSetTremor", -1, 0f);
            smoke.Play();
            spark.Play();
        }

        public void EndStorageSet() => setAction?.Invoke();
        #endregion

        public void TapReaction(Color color) => spriteRenderer.color = color;

        private void Awake()
        {
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }
    }
}
