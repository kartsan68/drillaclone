﻿using UnityEngine;
using Spine.Unity;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Drill
{
    public class Battery : MonoBehaviour
    {
        private ParticleSystem smoke;
        private ParticleSystem spark;
        private UnityAction disconnectionAction;
        private UnityAction setAction;
        private SkeletonAnimation battery;

        public void SetAnimationsTimeScale(float scale) => battery.timeScale = scale;

        public void SetAllSetUnsetElements(ParticleSystem smoke, ParticleSystem spark, UnityAction disconnectionAction, UnityAction setAction)
        {
            this.smoke = smoke;
            this.spark = spark;
            this.disconnectionAction = disconnectionAction;
            this.setAction = setAction;
        }

        public void DisconnectDetail() => GetComponent<Animator>().Play("HullMakeOut");

        public void SetDetail()
        {
            this.gameObject.SetActive(true);
            GetComponent<Animator>().Play("HullSet");
        }

        private void Awake() => InitData();

        private void InitData() => battery = gameObject.GetComponentInChildren<SkeletonAnimation>();

        #region AnimationEvents
        public void DisconnectionBattery()
        {

        }

        public void EndDisconnectionAnimation() => disconnectionAction?.Invoke();

        public void SetBattery()
        {
            smoke.Play();
            spark.Play();
        }

        public void EndBatterySet() => setAction?.Invoke();
        #endregion

        public void TapReaction(Color color) => battery.skeleton.SetColor(color);
    }
}
