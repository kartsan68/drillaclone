﻿using UnityEngine;
using Spine.Unity;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Drill
{
    public class Engine : MonoBehaviour
    {
        private ParticleSystem smoke;
        private ParticleSystem spark;
        private UnityAction disconnectionAction;
        private UnityAction setAction;
        private SkeletonAnimation engine;
        public int maxOtherDetailUpgradeLvl;
        public int adsForUpCount;

        public void SetAnimationsTimeScale(float scale) => engine.timeScale = scale;
        public void SetAllSetUnsetElements(ParticleSystem smoke, ParticleSystem spark, UnityAction disconnectionAction, UnityAction setAction)
        {
            this.smoke = smoke;
            this.spark = spark;
            this.disconnectionAction = disconnectionAction;
            this.setAction = setAction;
        }

        public void DisconnectDetail() => GetComponent<Animator>().Play("HullMakeOut");

        public void SetDetail()
        {
            this.gameObject.SetActive(true);
            GetComponent<Animator>().Play("HullSet");
        }

        private void Awake() => InitData();

        private void InitData() => engine = gameObject.GetComponentInChildren<SkeletonAnimation>();

        #region AnimationEvents
        public void DisconnectionEngine()
        {
        }

        public void EndDisconnectionAnimation() => disconnectionAction?.Invoke();
        public void SetEngine()
        {
            smoke.Play();
            spark.Play();
        }

        public void EndEngineSet() => setAction?.Invoke();
        #endregion

        public void TapReaction(Color color) => engine.skeleton.SetColor(color);
    }
}
