﻿using UnityEngine;
using Spine.Unity;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Drill
{
    public class Ladles : MonoBehaviour
    {
        private ParticleSystem[] smoke;
        private ParticleSystem[] spark;
        private UnityAction disconnectionAction;
        private UnityAction setAction;
        private Animator animator;
        private Animator drillAnimator;
        private SkeletonAnimation leftLadle;
        private SkeletonAnimation rightLadle;
        public float ladleMultipler;
        public float clickResMultiplier;

        public void SetAnimationsTimeScale(float scale)
        {
            leftLadle.timeScale = scale;
            rightLadle.timeScale = scale;
        }

        public void SetAllSetUnsetElements(ParticleSystem[] smoke, ParticleSystem[] spark, Animator drillAnimator,
       UnityAction disconnectionAction, UnityAction setAction)
        {
            this.smoke = smoke;
            this.spark = spark;
            this.disconnectionAction = disconnectionAction;
            this.drillAnimator = drillAnimator;
            this.setAction = setAction;
        }

        public void DisconnectDetail() => GetComponent<Animator>().Play("HullMakeOut");

        public void SetDetail()
        {
            this.gameObject.SetActive(true);
            GetComponent<Animator>().Play("HullSet");

        }

        private void Awake()
        {
            InitData();
        }


        private void InitData()
        {
            var skeletonAnimations = gameObject.GetComponentsInChildren<SkeletonAnimation>();
            leftLadle = skeletonAnimations[0];
            rightLadle = skeletonAnimations[1];
        }
        #region AnimationEvents
        // 0 - left, 1 - right
        public void DisconnectionLadle(int ladleIndex) { }

        public void EndDisconnectionAnimation() => disconnectionAction?.Invoke();

        public void SetLadle(int ladleIndex)
        {
            if (ladleIndex == 0) drillAnimator.Play("LadleLeftSetTremor", -1, 0f);
            else drillAnimator.Play("LadleRightSetTremor", -1, 0f);
            smoke[ladleIndex].Play();
            spark[ladleIndex].Play();
        }

        public void EndLadleSet() => setAction?.Invoke();
        #endregion

        public void TapReaction(Color color)
        {
            leftLadle.skeleton.SetColor(color);
            rightLadle.skeleton.SetColor(color);
        }
    }
}
