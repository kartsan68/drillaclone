﻿using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Drill
{
    public class Boer : MonoBehaviour
    {
        private ParticleSystem smoke;
        private ParticleSystem spark;
        private UnityAction disconnectionAction;
        private UnityAction setAction;
        private Animator animator;
        private Animator drillAnimator;
        private Renderer[] renderers;


        public double drillSpeed;
        public double visualDrillSpeed;
        public double offlineDrillSpeed;


        public void SetAllSetUnsetElements(ParticleSystem smoke, ParticleSystem spark, Animator drillAnimator,
        UnityAction disconnectionAction, UnityAction setAction)
        {
            this.smoke = smoke;
            this.spark = spark;
            this.disconnectionAction = disconnectionAction;
            this.drillAnimator = drillAnimator;
            this.setAction = setAction;
        }

        public void DisconnectDetail() => GetComponent<Animator>().Play("HullMakeOut");

        public void SetDetail()
        {
            this.gameObject.SetActive(true);
            GetComponent<Animator>().Play("HullSet");
        }

        public void SetAnimationsTimeScale(float scale)
        {
            animator.speed = scale;
        }

        private void Awake()
        {
            animator = GetComponentsInChildren<Animator>()[1];
            renderers = GetComponentsInChildren<Renderer>();
        }

        #region AnimationEvents
        public void BoerDisconnection() { }

        public void EndBoerDisconnection() => disconnectionAction?.Invoke();

        public void BoerSet()
        {
            drillAnimator.Play("BoerSetTremor", -1, 0f);
            smoke.Play();
            spark.Play();
        }

        public void EndBoerSet() => setAction?.Invoke();

        #endregion

        public void TapReaction(Color color)
        {
            for (int i = 0; i < renderers.Length; i++)
                renderers[i].material.SetColor("_Color", color);
        }
    }
}
