﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.Chronometer;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Analytics;
using BlackBears.GameCore.Features.Save;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlackBears.DiggIsBig.Core
{
    public class ApplicationController : AppController
    {
        [SerializeField] private GameObject loader;
        [SerializeField] private string configName;

        public GameManager gameManager { get; private set; }

        private MySaveManager saveManager;
        private string config;

        protected override void InitGame(Block<IGameAdapter> onSuccess, FailBlock onFail)
        {
            saveManager = new MySaveManager();
            saveManager.ReadSave();
            gameManager = new GameManager();
            EventManager.OpenClans += () => base.OpenClans();
            // gameManager.Initialize(() => onSuccess.SafeInvoke(gameManager));
            // Debug.Log("Pew!");
            // var parameters = BBGC.Features.GetFeature<AnalyticsFeature>().CreateDictionaryParameters(true);
            // BBGC.Features.GetFeature<AnalyticsFeature>().SendEvent("Post install", parameters);
            // Debug.Log("Ok!");

            ValidateLocalPlayer(onSuccess);
        }

        // private void ValidateLocalPlayer(Block<IGameAdapter> onSuccess)
        // {
        //     gameController.ValidateLocalPlayer(() =>
        //     {
        //         onSuccess.SafeInvoke(gameController);
        //         onGameInitialized.OnNext(null);
        //     }, (c, m) =>
        //     {
        //         Debug.Log($"GameFullInitialize error. Code - \"{c}\"; Message - \"{m}\"");
        //         ShowErrorAlert(() => ValidateLocalPlayer(onSuccess));
        //     });
        // }

        protected override void LaunchGame()
        {
            StartCoroutine(Downloader());
        }

        protected override void AppResumed()
        {
            if (!GameData.GameData.AdvertiseLoad)
            {
                EventManager.ApplicationReloadEvent();
            }
        }

        private IEnumerator Downloader()
        {
            var sqbaFiles = BBGC.Features.GetFeature<BlackBears.GameCore.Features.SQBAFiles.SQBAFilesFeature>();
            config = sqbaFiles.TextByName(configName);

            LocalizeAndConfigPas.SetConfigurationFile(config);

            yield return null;
            SceneManager.sceneLoaded += SceneLoaded;
            SceneManager.LoadSceneAsync("Main", LoadSceneMode.Additive);
        }

        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            loader.SetActive(false);
        }

        private void ValidateLocalPlayer(Block<IGameAdapter> onSuccess)
        {
            gameManager.ValidateLocalPlayer(() => onSuccess.SafeInvoke(gameManager), (c, m) =>
                        {
                            Debug.Log($"GameFullInitialize error. Code - \"{c}\"; Message - \"{m}\"");
                            ShowErrorAlert(() => ValidateLocalPlayer(onSuccess));
                        });
        }

    }
}
