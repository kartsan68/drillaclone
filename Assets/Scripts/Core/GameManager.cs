﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.Clans.ResourceShare;
using BlackBears.DiggIsBig.Clans;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.CoreDefaults;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.DiggIsBig.Core
{
    public class GameManager : IGameAdapter
    {
        private GamePlayersManager playersManager;

        public string PlayerId => playersManager.Player.Id;

        public string Icon => playersManager.Player.Icon;

        public string Nickname => playersManager.Player.Nick;

        public string Country => playersManager.Player.Country;

        public string ConfigVersion => "1.0";

        public string PlayerKey => playersManager.Player.PrivateKey;

        public string CurrencyAmount => AmountToPrettyString(GameData.GameData.goldCount);

        public long League => 1;

        public long StartGameDate => Save.SaveCreator.FirstStartTime;

        public double Score => playersManager.Player.Score;

        public double WeeklyScore => playersManager.Player.WeeklyScore;

        public string SubscriptionId => playersManager.Player.SubscriptionId;

        public double CoinsPerSecond => 0;

        public GameManager()
        {
            playersManager = new GamePlayersManager();
            BBGC.Features.GameServer().Inject(playersManager);
            playersManager.UpdatePlayer();
        }

        public void AddHardCurrency(PDouble currency)
        {
            // throw new System.NotImplementedException();
        }

        public bool AddResource(int resourceType, long resourceId, double value)
        {
            return false;
            // throw new System.NotImplementedException();
        }


        public bool AddSoftCurrency(PDouble soft)
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public string AmountToPrettyString(double amount)
        {
            return Utils.AmountUtils.ToPrettyString(amount, true);
        }

        public bool CanSpendResource(int resourceType, long resourceId, double amount)
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public List<GameResource> CreateResourceList()
        {
            return new List<GameResource>();
            // throw new System.NotImplementedException();
        }

        public JSONNode GenerateAdditionalScoreData()
        {
            return new JSONObject();
            // throw new System.NotImplementedException();
        }

        public void GetInGameResourceDependInfo(int resType, long resId, int maxRequired, Block<ResourceDependInfo> onSuccess)
        {
            // throw new System.NotImplementedException();
        }

        public double GetRequestingAmountOfResources(GameResource resource)
        {
            return 0;
            // throw new System.NotImplementedException();
        }

        public void GiveClanBattleReward(int battlePosition)
        {
            // throw new System.NotImplementedException();
        }

        public void GiveClanTopReward(int topPosition)
        {
            // throw new System.NotImplementedException();
        }

        public void Initialize(Block onSuccess)
        {
            onSuccess.Invoke();
        }

        public bool IsAvailableResource(GameResource resource)
        {
            if (resource == null) return false;
            return ((IGameAdapter)this).IsAvailableResource(resource.type, resource.id);
        }

        public bool IsAvailableResource(int resourceType, long resourceId)
        {
            return false;
        }

        public void LeagueTakeReward(int playerLeague)
        {
            // throw new System.NotImplementedException();
        }

        public void PrepareForSpendHardCurrency(PDouble currency, Block onSuccess, FailBlock onFail)
        {
            if (GameData.GameData.goldCount > currency)
            {
                onSuccess.SafeInvoke();
            }
            else
            {
                Managers.EventManager.NotEnoughtMoneyEvent(currency - GameData.GameData.goldCount, () => onSuccess.SafeInvoke(), () => onFail.SafeInvoke());
            }
        }

        public void RegisterPlayer(string nickname, string icon, string country, Block onSuccess, FailBlock onFail)
            => playersManager.UpdatePlayer(nickname, icon, country, onSuccess, onFail);

        public void SpendHardCurrency(PDouble currency, PlaceOfSpendResources place)
        {
            Managers.EventManager.TakeMyMoneyEvent(currency);
        }

        public bool SpendResource(int resourceType, long resourceId, double amount)
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public void UpdatePlayer(string nickname, string icon, string country, Block onSuccess, FailBlock onFail)
            => playersManager.UpdatePlayer(nickname, icon, country, onSuccess, onFail);

        public void ValidateLocalPlayer(Block onSuccess, FailBlock onFail)
            => playersManager.ValidatePlayer(onSuccess, onFail);

        public JSONNode GenerateAdditionalDataForCreateClan()
        {
            return new JSONObject();
            // throw new System.NotImplementedException();
        }
    }
}
