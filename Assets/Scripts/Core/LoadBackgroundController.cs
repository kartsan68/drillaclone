﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Core
{
    public class LoadBackgroundController : MonoBehaviour
    {
        [SerializeField] private GameObject smallBackgroundImage;
        [SerializeField] private GameObject bigBackgroundImage;
        [SerializeField] private GameObject iPadBackground;
        [SerializeField] private Camera cam;


        private void Awake()
        {
            if (cam.aspect > 0.5)
            {
                smallBackgroundImage.SetActive(true);
                bigBackgroundImage.SetActive(false);
                iPadBackground.SetActive(false);
            }
            else
            {
                smallBackgroundImage.SetActive(false);
                bigBackgroundImage.SetActive(true);
                iPadBackground.SetActive(false);

            }
#if UNITY_IOS
            bool deviceIsIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
            if (deviceIsIpad)
            {
                smallBackgroundImage.SetActive(false);
                bigBackgroundImage.SetActive(false);
                iPadBackground.SetActive(true);
            }
#endif
        }
    }
}
