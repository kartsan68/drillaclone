﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.GameElements.Location
{
    public class Location : MonoBehaviour
    {
        [SerializeField] private Sprite locationBackground;
        [SerializeField] private Sprite firstSideSprite;
        [SerializeField] private Sprite secondSideSprite;
        [SerializeField] private Sprite locationPreview;

        private float speedState = 1;
        private float productionState = 1;
        private float constantSpeed;
        private float constantLocationSpeed;


        public Sprite GetBackgroundImage => locationBackground;
        public Sprite GetPreviewImage => locationPreview;
        public Sprite[] GetSides => new Sprite[] { firstSideSprite, secondSideSprite };
        public float EarthRigidity { get; private set; }
        public float VisualEarthRigidity { get; private set; }
        public float ExplodeTime { get; private set; }

        public void SetLocationConfig(DiggIsBig.Configuration.Data.LocationData.Location loc)
        {
            EarthRigidity = loc.rigidity;
            VisualEarthRigidity = loc.rigidityVisual;
            speedState = loc.speedMultiplier;
            productionState = loc.prodMultiplier;
            constantSpeed = loc.constantSpeed;
            constantLocationSpeed = loc.constantVisualSpeed;
            ExplodeTime = loc.bigBeamFuckupTime;
        }

        public void GetLocationStates(out float speedMultiplier, out float productionMultiplier, out float earthRigidity, out float visualEarthRigidity,
                                      out float constSpeed, out float constVisualSpeed)
        {
            speedMultiplier = this.speedState;
            productionMultiplier = this.productionState;
            earthRigidity = EarthRigidity;
            visualEarthRigidity = VisualEarthRigidity;
            constSpeed = constantSpeed / 60;
            constVisualSpeed = constantLocationSpeed / 60;


        }
    }
}
