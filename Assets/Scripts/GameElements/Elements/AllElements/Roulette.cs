﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Advertise;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using BlackBears.DiggIsBig.Common.RandomRange;
using BlackBears.DiggIsBig.Formatter;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class Roulette : MonoBehaviour
    {
        [SerializeField] private Transform[] rouletteElements;
        [SerializeField] Advert advertise;
        [SerializeField] private GameObject tapForStopText;
        [SerializeField] private GameObject rouletteWindow;
        [SerializeField] private GameObject winWindow;
        [SerializeField] private Text winText;
        [SerializeField] private Button winButton;
        [SerializeField] private GameObject[] winImages;
        [SerializeField] private GameObject drum;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private Vector2[] elementsRange;
        [SerializeField] private int countRoundForStop;

        private GameController gameController;
        private float rotationSpeedForWork;
        private Vector2 wheelPosition;
        private bool drumRun = false;
        private bool stopDrum = false;
        private bool stopper;
        private int[] dropChances;
        private float[] gameTimes;
        private bool redy = false;
        private RouletteWinElements winElement;
        private Vector3 angle;
        private int roundCount;
        private DrillFormatter formatter;

        private void Awake()
        {
            formatter = new DrillFormatter();
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            tapForStopText.SetActive(false);
            SetConfig();

            winButton.onClick.AddListener(CatchRouletteWinElement);
        }
        private void SetConfig()
        {
            var rouletteData = gameController.GetConfigurationManager().rouletteData;
            int elementCount = rouletteData.golds.Length;
            dropChances = new int[elementCount];
            gameTimes = new float[elementCount];
            //Шансы выпадения
            for (int i = 0; i < rouletteData.golds.Length; i++)
            {
                dropChances[i] = rouletteData.golds[i].chancePercent;
                gameTimes[i] = rouletteData.golds[i].gameTime;
            }
        }

        private void OnEnable()
        {
            drum.transform.eulerAngles = new Vector3(0, 0, 0);
            foreach (Transform element in rouletteElements) element.transform.eulerAngles = new Vector3(0, 0, 0);
            SetStartState();
            RouletteRun();
        }

        void Update()
        {
            drum.transform.Rotate(0, 0, rotationSpeedForWork * Time.deltaTime);
            foreach (Transform element in rouletteElements) element.Rotate(0, 0, rotationSpeedForWork * Time.deltaTime * -1);

            rotationSpeedForWork *= Random.Range(0.996f, 0.998f);
            angle = drum.transform.rotation.eulerAngles;

            if (angle.z < 180 && !stopper)
            {
                roundCount++;
                stopper = true;
            }
            else if (angle.z > 180 && stopper)
            {
                stopper = false;
            }

            int countRoundForStopUsed = Random.Range(countRoundForStop, countRoundForStop + 2);

            if ((Mathf.DeltaAngle(angle.z, wheelPosition.y) > 10) && (Mathf.DeltaAngle(angle.z, wheelPosition.y) < 50) && roundCount >= countRoundForStop)
            {
                rotationSpeedForWork = 0;
                roundCount = 0;
                stopDrum = false;
                StartCoroutine(ShowWinWindow());
            }
        }

        public void SetStartState()
        {
            stopDrum = false;
            drumRun = false;
            redy = true;
        }

        private IEnumerator ShowWinWindow()
        {
            // foreach (var image in winImages) image.SetActive(false);

            switch (winElement)
            {
                case RouletteWinElements.GoldPack1:
                    //   winImages[0].SetActive(true);
                    winText.text = formatter.MoneyKFormatter((long)(GameData.GameData.CPS * gameTimes[0]));
                    break;
                case RouletteWinElements.GoldPack2:
                    //   winImages[0].SetActive(true);
                    winText.text = formatter.MoneyKFormatter((long)(GameData.GameData.CPS * gameTimes[1]));
                    break;
                case RouletteWinElements.GoldPack3:
                    //   winImages[0].SetActive(true);
                    winText.text = formatter.MoneyKFormatter((long)(GameData.GameData.CPS * gameTimes[2]));
                    break;
                case RouletteWinElements.GoldPack4:
                    //   winImages[0].SetActive(true);
                    winText.text = formatter.MoneyKFormatter((long)(GameData.GameData.CPS * gameTimes[3]));
                    break;
                case RouletteWinElements.GoldPack5:
                    //   winImages[0].SetActive(true);
                    winText.text = formatter.MoneyKFormatter((long)(GameData.GameData.CPS * gameTimes[4]));
                    break;
            }
            yield return new WaitForSeconds(1f);
            rouletteWindow.SetActive(false);
            SetStartState();
            winWindow.SetActive(true);
        }

        private void CatchRouletteWinElement()
        {
            EventManager.RouletteWinEvent(winElement);
        }

        public void RouletteRun()
        {
            if (redy)
            {
                rotationSpeedForWork = rotationSpeed;
                //!!! НЕ Unity рандом, скрипт из Common.
                int elementIndex = RandomRange.Range(new IntRange(1, dropChances[0]),
                                              new IntRange(2, dropChances[1]),
                                              new IntRange(3, dropChances[2]),
                                              new IntRange(4, dropChances[3]),
                                              new IntRange(5, dropChances[4]));
                switch (elementIndex)
                {
                    case 1:
                        winElement = RouletteWinElements.GoldPack1;
                        break;
                    case 2:
                        winElement = RouletteWinElements.GoldPack2;
                        break;
                    case 3:
                        winElement = RouletteWinElements.GoldPack3;
                        break;
                    case 4:
                        winElement = RouletteWinElements.GoldPack4;
                        break;
                    case 5:
                        winElement = RouletteWinElements.GoldPack5;
                        break;
                }

                wheelPosition = elementsRange[elementIndex - 1];
                drumRun = true;
            }
        }
    }
}
