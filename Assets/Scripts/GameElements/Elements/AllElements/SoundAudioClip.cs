﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.GameData;
using UnityEngine;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    [System.Serializable]
    public class SoundAudioClip
    {
        public Sound sound;
        public SoundType type;
        public AudioClip audioClip;
    }
}
