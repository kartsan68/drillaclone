﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Advertise;
using BlackBears.DiggIsBig.Cave.CaveElements;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class BeamSpawner : MonoBehaviour
    {
        [SerializeField] private Vector3 bigBeamSpawnPos;
        [SerializeField] private Vector3 smallBeamSpawnPos;
        [SerializeField] private GameObject boostBar;
        [SerializeField] private GameObject smallBeamGrounds;
        [SerializeField] private GameObject grounds;
        [SerializeField] private Beam smallBeam;
        [SerializeField] private Beam bigBeam;
        [SerializeField] private Button breakBeamButton;
        [SerializeField] private Button adsBreakBeamButton;
        [SerializeField] private Text breakBeamText;
        [SerializeField] private Advert advertising;
        [SerializeField] private GameController gameController;
        [SerializeField] private Slider beamProgress;
        [SerializeField] private Slider bigBeamProgress;
        [SerializeField] private Slider gasProgress;
        [SerializeField] private Text breakBigBeamText;
        [SerializeField] private Text gasText;
        [SerializeField] private GameObject smallBeamContainer;
        [SerializeField] private GameObject bigBeamContainer;
        [SerializeField] private GameObject beamTapBar;
        [SerializeField] private GameObject mainTapBar;
        [SerializeField] private GameObject mainCounterBar;
        [SerializeField] private GameObject mainMoneyContainer;
        [SerializeField] private GameObject mainSettingsButton;
        [SerializeField] private Text textinAdsButton;
        [SerializeField] private Text bigBeamTimeText;
        [SerializeField] private GameObject startGasAlert;
        [SerializeField] private GameObject fuckupGasAlert;
        [SerializeField] private GameObject alertEffect;


        private GameConfigurationManager gameConfigurationManager;
        private bool bigBeamState = false, smallBeamState = false;
        private Beam actualBeam;
        private double actualHealth = 0;
        private double actualGasTime = 0;

        #region Save/Load
        private void Save()
        {
            List<SaveData> data = new List<SaveData>();

            data.Add(new SaveData(SaveKeys.bigBeamStateKey, bigBeamState));
            data.Add(new SaveData(SaveKeys.smallBeamStateKey, smallBeamState));

            if (bigBeamState)
            {
                data.Add(new SaveData(SaveKeys.beamHealthKey, actualBeam.GetActualHealth()));
                data.Add(new SaveData(SaveKeys.gasTimeKey, actualBeam.GetGasTime()));
            }
            else if (smallBeamState)
            {
                data.Add(new SaveData(SaveKeys.beamHealthKey, actualBeam.GetActualHealth()));
                data.Add(new SaveData(SaveKeys.gasTimeKey, 0));
            }

            SaveCreator.Save(SaveKeys.beamSpawnerSaveKey, data);
        }

        private void Load(OfflineCalcData ocd)
        {
            if (GameData.GameData.firstStart)
            {
                if (ocd.bigBeamIsSet)
                {
                    actualHealth = ocd.bigBeamHealth;
                    actualGasTime = ocd.bigBeamGasTime;
                }
                else
                {
                    actualGasTime = 0;
                    actualHealth = 0;
                    if (actualBeam != null)
                    {
                        actualHealth = ocd.smallBeamHealth;
                        actualBeam.SetActualHealth(actualHealth);
                    }
                }
            }
            else
            {
                if ((bigBeamState && !ocd.bigBeamIsSet) || (smallBeamState && !ocd.smallBeamIsSet))
                {
                    if (actualBeam != null)
                    {
                        actualBeam.CrashBeam();
                    }
                    return;
                }

                if (ocd.bigBeamIsSet)
                {
                    if (actualBeam != null)
                    {
                        actualBeam.SetActualHealth(ocd.bigBeamHealth, true);
                        actualBeam.SetActualGasTime(ocd.bigBeamGasTime);
                        actualBeam.transform.position = bigBeamSpawnPos;
                    }
                }
                else if (ocd.smallBeamIsSet)
                {
                    if (actualBeam != null)
                    {
                        actualBeam.SetActualHealth(ocd.smallBeamHealth);
                        actualBeam.transform.position = smallBeamSpawnPos;
                    }
                }
            }
        }

        #endregion

        private void SetBeamHealthInfo(string health, bool isBig, string gas)
        {
            if (isBig)
            {
                gasText.text = gas;
                breakBigBeamText.text = health;
            }
            else breakBeamText.text = health;
        }

        private void Start()
        {
            SubscribeToEvent();
            gameConfigurationManager = gameController.GetConfigurationManager();
        }

        private IEnumerator BigBeamSpawn()
        {
            yield return null;
        }

        private void SubscribeToEvent()
        {
            EventManager.BeamCrash += BeamKill;
            EventManager.BeamSpawn += BeamCreate;
            EventManager.SetBeamHealthInfo += SetBeamHealthInfo;
            EventManager.LoadGame += Load;
            EventManager.SaveGame += Save;
        }

        private bool beamIsSet = false;
        private void BeamCreate(bool isBig, bool cap)
        {
            if (!beamIsSet)
            {
                boostBar.SetActive(false);
                beamTapBar.SetActive(true);
                mainTapBar.SetActive(false);
                mainCounterBar.SetActive(false);
                mainMoneyContainer.SetActive(false);
                mainSettingsButton.SetActive(false);

                if (isBig)
                {
                    beamIsSet = true;
                    bigBeamState = true;
                    ActualBigBeamSpawn();
                }
                else
                {
                    beamIsSet = true;
                    smallBeamState = true;
                    smallBeamGrounds.SetActive(true);
                    ActualSmallBeamSpawn();
                }
                ButtonBeamStatus(true);
                grounds.SetActive(false);
                AddBeamBtnListeners(isBig);
            }
        }

        private void BeamKill()
        {
            actualBeam = null;
            boostBar.SetActive(true);
            beamTapBar.SetActive(false);
            mainTapBar.SetActive(true);
            mainCounterBar.SetActive(true);
            mainMoneyContainer.SetActive(true);
            mainSettingsButton.SetActive(true);
            beamIsSet = false;
            smallBeamState = false;
            bigBeamState = false;
            ButtonBeamStatus(false);
            grounds.SetActive(true);
            smallBeamGrounds.SetActive(false);
            actualHealth = 0;
        }

        private void ActualSmallBeamSpawn()
        {
            if (actualBeam == null)
            {
                smallBeamContainer.SetActive(true);
                bigBeamContainer.SetActive(false);

                actualBeam = Instantiate(smallBeam, smallBeamSpawnPos, Quaternion.identity, transform);
                actualBeam.SetProgressSliderAndAdsText(beamProgress, textinAdsButton);
                actualBeam.SetAdvertiseDamagePercent(gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber].smallBeamAdvPercDamage);
                actualBeam.StartBeamHealthTick(() =>
                {
                    adsBreakBeamButton.gameObject.SetActive(false);
                    breakBeamButton.gameObject.SetActive(false);
                }, false, gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber].smallBeamHealth);

                actualHealth = 0;
            }
        }

        private void ActualBigBeamSpawn()
        {
            if (actualBeam == null)
            {
                smallBeamContainer.SetActive(false);
                bigBeamContainer.SetActive(true);

                actualBeam = Instantiate(bigBeam, bigBeamSpawnPos, Quaternion.identity, transform);
                actualBeam.SetProgressSliderAndAdsText(bigBeamProgress, textinAdsButton);
                actualBeam.SetAdvertiseDamagePercent(gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber + 1].kingBeamAdvPercDamage);
                actualBeam.SetGasLabels(bigBeamTimeText, startGasAlert, fuckupGasAlert, alertEffect);
                actualBeam.StartExplosionTimer(gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber + 1].bigBeamFuckupTime, gasProgress);
                actualBeam.StartBeamHealthTick(() =>
                {
                    adsBreakBeamButton.gameObject.SetActive(false);
                    breakBeamButton.gameObject.SetActive(false);
                }, true, gameConfigurationManager.locationData.allLocationArray[GameData.GameData.locationNumber + 1].kingBeamHealth);

                if (actualHealth != 0) actualBeam.SetActualHealth(actualHealth);
                if (actualGasTime != 0) actualBeam.SetActualGasTime(actualGasTime);
                actualGasTime = 0;
                actualHealth = 0;
            }
        }

        private void ButtonBeamStatus(bool status)
        {
            adsBreakBeamButton.gameObject.SetActive(status);
            breakBeamButton.gameObject.SetActive(status);
            breakBeamText.gameObject.SetActive(status);
        }

        private void AddBeamBtnListeners(bool isBig)
        {
            breakBeamButton.onClick.RemoveAllListeners();
            adsBreakBeamButton.onClick.RemoveAllListeners();
            breakBeamButton.onClick.AddListener(() => actualBeam.Damaged(false, true));
            adsBreakBeamButton.onClick.AddListener(() => advertising.ShowRewardedAdvertWithCallBack(() => actualBeam.Damaged(true)));
        }

    }
}
