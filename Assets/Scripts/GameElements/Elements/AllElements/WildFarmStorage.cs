﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class WildFarmStorage : MonoBehaviour
    {
        [SerializeField] private GameObject wildFarmEffect;
        [SerializeField] private GameObject wildFarmResultAlert;
        [SerializeField] private Button saleAllButton;
        [SerializeField] private Button saleAllAdvertiseButton;
        [SerializeField] private Text farmResultText;

        private Dictionary<Loot, double> lootDictionary;
        private float boost;
        private float clickBoost = 1;

        private bool iKnow = false;

        private void Awake()
        {
            lootDictionary = new Dictionary<Loot, double>();
            EventManager.WildFarmBoostUsed += (a, b) => { boost = a; StartWildFarmMode(); };
            EventManager.WildFarmBoostStop += StopWildFarmMode;
            EventManager.PickUpResources += PickUpResources;
            EventManager.ClickBoostUsed += (boost, time) => clickBoost = boost;
            EventManager.ClickBoostStop += () => clickBoost = 1;
        }

        private void PickUpResources(Loot loot, float ladleMultiplier)
        {
            if (GameData.GameData.wildFarmMode)
            {
                Loot actualLoot = loot;
                foreach (var unit in lootDictionary)
                {
                    if (unit.Key.type == loot.type)
                    {
                        actualLoot = unit.Key;
                        iKnow = true;
                    }
                }

                loot.lootCount *= (GameData.GameData.actualClickMultiplier * clickBoost);
                if (iKnow) lootDictionary[actualLoot] += loot.lootCount;
                else lootDictionary.Add(Instantiate(loot, new Vector3(-1000, -1000, -1000), Quaternion.identity, this.transform), loot.lootCount);

                iKnow = false;
            }
        }

        private void StartWildFarmMode()
        {
            wildFarmEffect.SetActive(true);
        }

        private void StopWildFarmMode()
        {
            string text = "";
            double sum = 0;
            foreach (var unit in lootDictionary)
            {
                text += unit.Key.type.ToString() + ": " + unit.Value + "шт! = " + (unit.Value * unit.Key.lootPrice) + "\n";
                sum += unit.Value * unit.Key.lootPrice;
            }
            text += "Общая сумма добычи = " + sum;

            saleAllButton.interactable = true;
            saleAllButton.onClick.RemoveAllListeners();
            saleAllButton.onClick.AddListener(() =>
            {
                saleAllButton.interactable = false;
                EventManager.GiveMeMoneyEvent((long)sum);
                wildFarmResultAlert.SetActive(false);
            });

            saleAllAdvertiseButton.interactable = true;
            saleAllAdvertiseButton.onClick.RemoveAllListeners();
            saleAllAdvertiseButton.onClick.AddListener(() =>
            {
                saleAllAdvertiseButton.interactable = false;
                EventManager.CallRewardedAdvertWithActionEvent(() =>
               {
                   EventManager.GiveMeMoneyEvent((long)(sum * boost));
                   wildFarmResultAlert.SetActive(false);
               });
            });

            farmResultText.text = text;
            wildFarmEffect.SetActive(false);
            wildFarmResultAlert.SetActive(true);
        }
    }
}
