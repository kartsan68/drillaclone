﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Drill;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Save;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.GameCore.Features.Localization;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class Garage : MonoBehaviour
    {
        [SerializeField] private UIDrilla uiDrilla;
        [SerializeField] private Button mineButton;
        [SerializeField] private GameObject soSmallEngineLvlAlert;
        [SerializeField] private GameController gameController;
        [SerializeField] private Drill.Drill drill;
        [SerializeField] private List<GarageCell> cells;
        [SerializeField] private List<Battery> batterySpare;
        [SerializeField] private List<Ladles> ladlesSpare;
        [SerializeField] private List<Boer> boerSpare;
        [SerializeField] private List<Engine> engineSpare;
        [SerializeField] private List<Storage> storageSpare;
        [SerializeField] private List<Hull> hullSpare;

        private List<int> lvlList;
        private List<bool> statusList;
        private int[] swapDetailLvls = { 0, 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52, 55, 58, 61, 64, 67, 70, 73, 76, 79, 82, 85, 87, 200 };
        private int[] secondDetailLvls = { 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53, 56, 59, 62, 65, 68, 71, 74, 77, 80, 83, 200 };
        private int[] thirdDetailLvls = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 200 };
        // private int[] swapDetailLvls = { 0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 200 };
        // private int[] secondDetailLvls = { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52, 55, 58, 61, 64, 67, 70, 73, 76, 79, 82, 85, 88, 200 };
        // private int[] thirdDetailLvls = { 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 51, 53, 56, 59, 62, 65, 68, 71, 74, 77, 80, 83, 86, 89, 200 };

        private int maxLvl, maxStorageLvl, onlyAdvUpLvl;
        private int redyUpdates;
        private GameConfigurationManager gameConfigurationManager;
        private DrillFormatter drillFormatter;
        private CountupTimer lastupgradeTimer;
        private CountupTimer lastupgradeOnlineTimer;

        #region Save/Load
        private void Save()
        {
            List<SaveData> data = new List<SaveData>();
            data.Add(new SaveData(SaveKeys.engineLvlDataKey, lvlList[0]));
            data.Add(new SaveData(SaveKeys.boerLvlDataKey, lvlList[1]));
            data.Add(new SaveData(SaveKeys.ladleLvlDataKey, lvlList[2]));
            data.Add(new SaveData(SaveKeys.storageLvlDataKey, lvlList[3]));
            data.Add(new SaveData(SaveKeys.theLastUpdateTimeKey, lastupgradeTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.firstGarageAlertShow, GameData.GameData.firstGarageAlertShow));

            SaveCreator.Save(SaveKeys.garageSaveKey, data);

        }

        public void Load(OfflineCalcData ocd)
        {
            Debug.Log("Garage");
            var loadData = SaveCreator.GetLoadData(SaveKeys.garageSaveKey).AsArray;
            var loadData2 = SaveCreator.GetLoadData(SaveKeys.gameControllerSaveKey).AsArray;
            if (loadData != null)
            {
                for (int i = 0; i < loadData2.Count; i++)
                {
                    if (loadData2[i].HasKey(SaveKeys.firstNeverUpgradeDataKey))
                    {
                        GameData.GameData.firstNeverUpgrade = loadData2[i][SaveKeys.firstNeverUpgradeDataKey];
                    }
                    if (loadData2[i].HasKey(SaveKeys.firstSessionDataKey) && GameData.GameData.firstStart)
                    {
                        GameData.GameData.firstSession = loadData2[i][SaveKeys.firstSessionDataKey];
                    }
                }

                for (int i = 0; i < loadData.Count; i++)
                {
                    if (loadData[i].HasKey(SaveKeys.engineLvlDataKey))
                    {
                        lvlList[0] = loadData[i][SaveKeys.engineLvlDataKey];
                    }
                    else if (loadData[i].HasKey(SaveKeys.boerLvlDataKey))
                    {
                        lvlList[1] = loadData[i][SaveKeys.boerLvlDataKey];
                    }
                    else if (loadData[i].HasKey(SaveKeys.ladleLvlDataKey))
                    {
                        lvlList[2] = loadData[i][SaveKeys.ladleLvlDataKey];
                    }
                    else if (loadData[i].HasKey(SaveKeys.storageLvlDataKey))
                    {
                        lvlList[3] = loadData[i][SaveKeys.storageLvlDataKey];
                    }
                    else if (loadData[i].HasKey(SaveKeys.theLastUpdateTimeKey))
                    {
                        if (GameData.GameData.firstNeverUpgrade)
                            lastupgradeTimer.SetTime(0);
                        else
                            lastupgradeTimer.SetTime(loadData[i][SaveKeys.theLastUpdateTimeKey] + Chronometer.TimeModule.TimeFromLastSave);
                    }
                    else if (loadData[i].HasKey(SaveKeys.firstGarageAlertShow))
                    {
                        GameData.GameData.firstGarageAlertShow = loadData[i][SaveKeys.firstGarageAlertShow];
                    }
                }
                if (GameData.GameData.firstStart) SetDetailOnLoad();
                GameData.GameData.lvlDetails = lvlList.ToArray();
            }


        }

        #endregion


        private void Awake()
        {
            SubscribeToEvents();
            VarInit();
            CreateInfo();
            AddButtonEvents();

            lastupgradeTimer.StartTimer(() => GameData.GameData.theLastUpgradeTime = lastupgradeTimer.GetTime());
            lastupgradeOnlineTimer.StartTimer(() => GameData.GameData.theLastUpgradeOnlineTime = lastupgradeOnlineTimer.GetTime());
        }

        private void VarInit()
        {
            gameConfigurationManager = gameController.GetConfigurationManager();
            drillFormatter = new DrillFormatter();
            lastupgradeOnlineTimer = TimeManager.GetEmptyCountupTimer(this.gameObject);
            lastupgradeTimer = TimeManager.GetEmptyCountupTimer(this.gameObject);
            SetDetailConfig();
            maxLvl = boerSpare.Count;
            GameData.GameData.maxDetailsLvl = maxLvl;
            maxStorageLvl = gameConfigurationManager.detailData.storage.Length;
            onlyAdvUpLvl = 3;
            lvlList = new List<int>() { 1, 1, 1, 1 };
            GameData.GameData.lvlDetails = lvlList.ToArray();
            statusList = new List<bool>() { false, false, false, false };
        }

        private void SubscribeToEvents()
        {
            EventManager.SaveGame += Save;
            EventManager.LoadGame += Load;
            EventManager.BuyDetailPack += SetDetailOfChest;
            EventManager.CallAdvDetail += IdleAdsDetail;
            EventManager.ColdStart += () =>
            {
                EventManager.SetActualPowerDataEvent(0);
                StartSetDetails();
            };
        }


        private void IdleAdsDetail()
        {
            int elementForAds = 0;
            List<int> elements = new List<int>();

            for (int i = 0; i < lvlList.Count; i++)
            {
                for (int j = 0; j < lvlList.Count; j++)
                {
                    if (lvlList[elementForAds] > lvlList[j])
                    {
                        elementForAds = j;
                        break;
                    }
                }
            }
            HideAllAdsUpBtns();
            cells[elementForAds].idleAdsUpButton.gameObject.SetActive(true);
        }

        private void HideAllAdsUpBtns()
        {
            foreach (var element in cells) element.idleAdsUpButton.gameObject.SetActive(false);
        }

        private void HideAdsUpBtnAtIndex(int index)
        {
            lastupgradeTimer.RestartTimer();
            lastupgradeOnlineTimer.RestartTimer();
            // EventManager.ResetRewardedDelayTimerEvent();
            cells[index].idleAdsUpButton.gameObject.SetActive(false);
        }

        private void SetDetailConfig()
        {
            var detailConfigData = gameConfigurationManager.detailData;
            SetBoerConfig(detailConfigData.boer);
            SetLadleConfig(detailConfigData.ladle);
            SetEngineConfig(detailConfigData.engine);
            SetStorageConfig(detailConfigData.storage);
        }

        private void SetBoerConfig(DiggIsBig.Configuration.Data.DetailData.Boer[] boerCfgData)
        {
            for (int i = 0; i < boerSpare.Count; i++)
            {
                boerSpare[i].drillSpeed = boerCfgData[i].drillSpeed;
                boerSpare[i].visualDrillSpeed = boerCfgData[i].visualDrillSpeed;
                boerSpare[i].offlineDrillSpeed = boerCfgData[i].offlineDrillSpeed;

                Detail detail = boerSpare[i].GetComponent<Detail>();

                detail.lvl = boerCfgData[i].lvl;
                detail.price = boerCfgData[i].price;
            }
        }

        private void SetLadleConfig(DiggIsBig.Configuration.Data.DetailData.Ladle[] ladlesCfgData)
        {
            for (int i = 0; i < ladlesSpare.Count; i++)
            {
                ladlesSpare[i].ladleMultipler = ladlesCfgData[i].multiplier;
                ladlesSpare[i].clickResMultiplier = ladlesCfgData[i].clickMultiplier;

                Detail detail = ladlesSpare[i].GetComponent<Detail>();

                detail.lvl = ladlesCfgData[i].lvl;
                detail.price = ladlesCfgData[i].price;
            }
        }

        private void SetEngineConfig(DiggIsBig.Configuration.Data.DetailData.Engine[] engineCfgData)
        {
            for (int i = 0; i < engineSpare.Count; i++)
            {
                engineSpare[i].maxOtherDetailUpgradeLvl = engineCfgData[i].otherDetailLvl;
                engineSpare[i].adsForUpCount = engineCfgData[i].advCount;

                Detail detail = engineSpare[i].GetComponent<Detail>();

                detail.lvl = engineCfgData[i].lvl;
                detail.price = engineCfgData[i].price;
            }
        }

        private void SetStorageConfig(DiggIsBig.Configuration.Data.DetailData.Storage[] storageCfgData)
        {
            for (int i = 0; i < storageSpare.Count; i++)
            {
                Detail detail = storageSpare[i].GetComponent<Detail>();

                detail.lvl = storageCfgData[i].lvl;
                detail.price = storageCfgData[i].price;
            }
        }


        private void Update()
        {
            CheckRedyUpdates();
        }

        private void CheckRedyUpdates()
        {
            redyUpdates = 0;
            GameData.GameData.redyUpDetails = new bool[] { false, false, false, false };

            for (int i = 0; i < cells.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        if (lvlList[i] < maxLvl)
                        {
                            if (engineSpare[lvlList[i]].adsForUpCount != 0)
                            {
                                statusList[i] = true;
                                SetBtnStatus(i, true);
                                redyUpdates++;
                                GameData.GameData.redyUpDetails[i] = true;
                                cells[i].SetAdsUpgradeState(true);
                                break;
                            }
                            else if (gameController.SpendMoneyInfo(CheckDetail(i).price))
                            {
                                cells[i].SetAdsUpgradeState(false);
                                statusList[i] = true;
                                SetBtnStatus(i, true);
                                redyUpdates++;
                                GameData.GameData.redyUpDetails[i] = true;
                                break;
                            }
                        }
                        SetBtnStatus(i, false);
                        cells[i].SetAdsUpgradeState(false);
                        statusList[i] = false;
                        break;
                    default:
                        if (lvlList[i] < maxLvl)
                        {
                            if ((gameController.SpendMoneyInfo(CheckDetail(i).price) && lvlList[i] < lvlList[0]/*|| lvlList[i] >= onlyAdvUpLvl*/))
                            {
                                statusList[i] = true;
                                SetBtnStatus(i, true);
                                redyUpdates++;
                                GameData.GameData.redyUpDetails[i] = true;
                                break;
                            }
                            else if (lvlList[i] == lvlList[0])
                            {
                                statusList[i] = true;
                                cells[i].SetNeedEngineLvlState(true, lvlList[0] + 1);
                                // redyUpdates++;
                                break;
                            }
                        }
                        SetBtnStatus(i, false);
                        statusList[i] = false;
                        break;
                }
            }

            EventManager.RedyUpdatesCountEvent(redyUpdates);
        }

        private void SetBtnStatus(int index, bool state) => cells[index].SetInactiveBtnState(!state);

        private Detail CheckDetail(int index)
        {
            switch (index)
            {
                case 0:
                    return engineSpare[lvlList[index]].GetComponent<Detail>();
                case 1:
                    return boerSpare[lvlList[index]].GetComponent<Detail>();
                case 2:
                    return ladlesSpare[lvlList[index]].GetComponent<Detail>();
                case 3:
                    return storageSpare[lvlList[index]].GetComponent<Detail>();
                default:
                    return null;
            }
        }

        private void CreateInfo()
        {
            for (int i = 0; i < cells.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        if (lvlList[i] < maxLvl)
                        {
                            if (engineSpare[lvlList[i]].adsForUpCount != 0)
                            {
                                cells[i].adsText.text = (engineSpare[lvlList[i]].adsForUpCount - redyAdsCount).ToString();
                                SetLvlAndPriceText(i, (engineSpare[lvlList[i]].adsForUpCount - redyAdsCount) + "ads");
                                ResetEngineButton(true);
                            }
                            else
                            {
                                SetLvlAndPriceText(i, drillFormatter.MoneyKFormatter(CheckDetail(i).price));
                                ResetEngineButton(false);
                            }
                        }
                        else
                        {
                            SetLvlAndPriceText(i, "MAX");
                            cells[i].button.onClick.RemoveAllListeners();
                        }
                        break;
                    case 3:
                        if (lvlList[i] < maxLvl) SetLvlAndPriceText(i, drillFormatter.MoneyKFormatter(gameConfigurationManager.detailData.storage[lvlList[3]].price));
                        else
                        {
                            SetLvlAndPriceText(i, "MAX");
                            cells[i].button.onClick.RemoveAllListeners();
                        }
                        break;
                    default:
                        if (lvlList[i] < maxLvl) SetLvlAndPriceText(i, drillFormatter.MoneyKFormatter(CheckDetail(i).price));
                        else
                        {
                            SetLvlAndPriceText(i, "MAX");
                            cells[i].button.onClick.RemoveAllListeners();
                        }
                        break;
                }
            }
        }

        private void SetLvlAndPriceText(int index, string priceText)
        {
            Text priceTextField = cells[index].priceText;
            Text lvlTextField = cells[index].lvlText;

            priceTextField.text = priceText;
            lvlTextField.text = lvlList[index] + " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper); ;
        }

        private void AddButtonEvents()
        {
            //0 - двигатель батарея 1 - бур 2 - ковши 3 - склад
            //  cells[0].GetComponent<Button>().onClick.AddListener(() => UpgradeEngine());

            cells[1].button.onClick.AddListener(() => UpgradeGold(1));
            cells[2].button.onClick.AddListener(() => UpgradeGold(2));
            cells[3].button.onClick.AddListener(() => UpgradeGold(3));

            cells[0].idleAdsUpButton.onClick.AddListener(() => UpgradeAdv(0));
            cells[1].idleAdsUpButton.onClick.AddListener(() => UpgradeAdv(1));
            cells[2].idleAdsUpButton.onClick.AddListener(() => UpgradeAdv(2));
            cells[3].idleAdsUpButton.onClick.AddListener(() => UpgradeAdv(3));
        }

        private void ResetEngineButton(bool adv)
        {
            Button cellBtn = cells[0].button;
            if (adv)
            {
                cellBtn.onClick.RemoveAllListeners();
                cellBtn.onClick.AddListener(() => UpgradeEngine());
            }
            else
            {
                cellBtn.onClick.RemoveAllListeners();
                cellBtn.onClick.AddListener(() => UpgradeGold(0));
            }
        }

        private void ResetButtonEventsToAdvUp(int index)
        {
            cells[index].button.onClick.RemoveAllListeners();
            cells[index].button.onClick.AddListener(() => UpgradeAdv(index));
        }



        // private void UpgradeStorage()
        // {
        //     if (lvlList[3] < maxLvl && CheckUpgrade(3))
        //     {
        //         var res = gameController.SpendMoney(gameConfigurationManager.detailData.storage[lvlList[3]].price);
        //         if (res == 0)
        //         {
        //             // EventManager.CallInterstitialAdvertEvent();
        //             HideAdsUpBtnAtIndex(3);
        //             UpStorageAction();
        //         }
        //         else
        //         {
        //             EventManager.NotEnoughtMoneyEvent(res, () => UpStorageAction());
        //         }
        //     }
        // }

        private void UpStorageAction(bool silentOrFull = true)
        {
            if (!gameController.GetPauseState) gameController.GamePause();
            lvlList[3]++;
            drill.SwapStorage(storageSpare[lvlList[3] - 1], lvlList[3], lvlList[3] - 1, silentOrFull);
            CreateInfo();
            EventManager.CloseWinAdvBlockEvent();
            // mineButton.onClick.Invoke();
        }

        private int redyAdsCount = 0;
        private void UpgradeEngine()
        {
            if (lvlList[0] < maxLvl)
            {
                EventManager.CallRewardedAdvertEvent();
                if (engineSpare[lvlList[0]].adsForUpCount == (redyAdsCount + 1))
                {
                    Upgrade(0, true);
                    redyAdsCount = 0;
                }
                else
                {
                    redyAdsCount++;
                }
                CreateInfo();
                EventManager.CloseWinAdvBlockEvent();
                mineButton.onClick.Invoke();
            }
        }


        private void UpgradeGold(int index)
        {
            Detail detail = CheckDetail(index);

            if ((index.Equals(0) || CheckUpgrade(index)))
            {
                if (lvlList[index] < maxLvl)
                {
                    var res = gameController.SpendMoney(detail.price);
                    if (res == 0)
                    {
                        // EventManager.CallInterstitialAdvertEvent();
                        Upgrade(index, false);
                    }
                    else
                    {
                        EventManager.NotEnoughtMoneyEvent(res, () => UpgradeGold(index), () => { });
                    }
                }
            }
        }

        private void UpgradeAdv(int index)
        {
            if (lvlList[index] < maxLvl)
            {
                if (index.Equals(0) || CheckUpgrade(index))
                {
                    EventManager.CallRewardedAdvertWithActionEvent(() =>
                    {
                        GameData.GameData.firstNeverUpgrade = false;
                        Upgrade(index, true);
                        //  cells[index].idleAdsUpButton.gameObject.SetActive(false);
                    });
                }
            }
        }

        private bool CheckUpgrade(int index)
        {
            if (lvlList[index] > engineSpare[lvlList[0] - 1].maxOtherDetailUpgradeLvl)
            {
                // StartCoroutine(SmallLvlAlert());
                return false;
            }
            else
            {
                return true;
            }
        }

        private IEnumerator SmallLvlAlert()
        {
            soSmallEngineLvlAlert.SetActive(true);
            yield return new WaitForSeconds(1.5f);
            soSmallEngineLvlAlert.SetActive(false);
        }

        private void StartSetDetails()
        {
            drill.SetFirstLvlConfigData(boerSpare[0], ladlesSpare[0], batterySpare[0]);
        }

        private void Upgrade(int index, bool forAdv)
        {
            lvlList[index]++;
            GameData.GameData.lvlDetails = lvlList.ToArray();
            CreateInfo();

            bool silentOrFull = false;
            for (int i = 0; i < swapDetailLvls.Length - 1; i++)
            {
                if (lvlList[index] - 1 == swapDetailLvls[i]) silentOrFull = true;
            }

            if (!gameController.GetPauseState)
            {
                gameController.GamePause();
            }

            if (silentOrFull)
            {
                EventManager.CloseWinAdvBlockEvent();
                mineButton.onClick.Invoke();
            }

            HideAdsUpBtnAtIndex(index);

            switch (index)
            {
                case 0:
                    SwapBetteryAndHullAndEngine(silentOrFull, lvlList[index] - 1);
                    break;
                case 1:
                    drill.SwapBoer(boerSpare[lvlList[index] - 1], lvlList[index], lvlList[index] - 1, silentOrFull);
                    EventManager.SetActualPowerDataEvent(lvlList[index] - 1);
                    break;
                case 2:
                    drill.SwapLadle(ladlesSpare[lvlList[index] - 1], lvlList[index], lvlList[index] - 1, silentOrFull);
                    break;
                case 3:
                    lvlList[index]--;
                    UpStorageAction(silentOrFull);
                    break;
            }
            uiDrilla.SetDetail(index, lvlList[index] - 1);
            CheckDetailProgress();
            EventManager.UpgradeDetailEvent();
        }


        private void SetDetailOnLoad()
        {
            CreateInfo();
            uiDrilla.SetAllDetails(lvlList.ToArray());
            EventManager.SetActualPowerDataEvent(lvlList[1] - 1);
            foreach (var item in hullSpare)
            {
                if (((lvlList[0]) >= item.otherDetailMinLvl) && ((lvlList[0]) <= item.otherDetailMaxLvl))
                {
                    drill.SwapDetailOnLoad(boerSpare[lvlList[1] - 1], ladlesSpare[lvlList[2] - 1], storageSpare[lvlList[3] - 1],
                                           batterySpare[lvlList[0] - 1], engineSpare[lvlList[0] - 1], item);

                    CheckDetailProgress();
                    break;
                }
            }
        }

        private void CheckDetailProgress()
        {
            bool gotcha;
            for (int i = 0; i < cells.Count; i++)
            {
                gotcha = false;

                for (int j = 0; j < swapDetailLvls.Length - 1; j++)
                {
                    if (lvlList[i] - 1 == swapDetailLvls[j])
                    {
                        gotcha = true;
                        cells[i].detailProgress.value = 0;
                        break;
                    }
                }

                if (!gotcha)
                {
                    for (int j = 0; j < secondDetailLvls.Length - 1; j++)
                    {
                        if (lvlList[i] - 1 == secondDetailLvls[j])
                        {
                            gotcha = true;
                            cells[i].detailProgress.value = 0.4f;
                            break; ;
                        }
                    }
                    if (!gotcha)
                    {
                        for (int j = 0; j < thirdDetailLvls.Length - 1; j++)
                        {
                            if (lvlList[i] - 1 == thirdDetailLvls[j])
                            {
                                cells[i].detailProgress.value = 1f;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void SwapBetteryAndHullAndEngine(bool silentOrFull, int oldLvl)
        {
            foreach (var item in hullSpare)
            {
                if (((lvlList[0]) >= item.otherDetailMinLvl) && ((lvlList[0]) <= item.otherDetailMaxLvl))
                {
                    AdjustEventManager.EngineLvlEvent(lvlList[0]);
                    drill.SwapBatteryAndEngine(batterySpare[lvlList[0] - 1], engineSpare[lvlList[0] - 1], item, lvlList[0], oldLvl, silentOrFull);
                    break;
                }
            }
        }

        private void SetDetailOfChest(int[] newlvlList)
        {
            bool[] state = new bool[] { false, false, false, false };
            int[] oldLvlList = lvlList.ToArray();

            for (int i = 0; i < newlvlList.Length; i++)
            {
                if (lvlList[i] < maxLvl)
                {
                    lvlList[i] += newlvlList[i];
                    if (lvlList[i] > maxLvl)
                        lvlList[i] = maxLvl;

                    state[i] = true;
                }
                else
                {
                    lvlList[i] = maxLvl;
                    state[i] = false;
                }
            }

            GameData.GameData.lvlDetails = lvlList.ToArray();
            int redyUpCount = 0;
            foreach (bool element in state)
            {
                if (element) redyUpCount++;
            }

            if (redyUpCount == 0) return;

            // EventManager.ResetRewardedDelayTimerEvent();
            lastupgradeTimer.RestartTimer();
            lastupgradeOnlineTimer.RestartTimer();
            HideAllAdsUpBtns();

            CreateInfo();
            gameController.GamePause();
            for (int i = 0; i < newlvlList.Length; i++)
            {
                if (newlvlList[i] > 0 && i == 0 && lvlList[i] <= maxLvl)
                {
                    uiDrilla.SetDetail(i, lvlList[i] - 1);
                    SwapBetteryAndHullAndEngine(true, oldLvlList[0]);
                }
                else if (newlvlList[i] > 0 && i == 1 && lvlList[i] <= maxLvl)
                {
                    uiDrilla.SetDetail(i, lvlList[i] - 1);
                    drill.SwapBoer(boerSpare[lvlList[1] - 1], lvlList[1], oldLvlList[1]);
                    EventManager.SetActualPowerDataEvent(lvlList[i] - 1);
                }
                else if (newlvlList[i] > 0 && i == 2 && lvlList[i] <= maxLvl)
                {
                    uiDrilla.SetDetail(i, lvlList[i] - 1);
                    drill.SwapLadle(ladlesSpare[lvlList[2] - 1], lvlList[2], oldLvlList[2]);
                }
                else if (newlvlList[i] > 0 && i == 3 && lvlList[i] <= maxLvl)
                {
                    uiDrilla.SetDetail(i, lvlList[i] - 1);
                    drill.SwapStorage(storageSpare[lvlList[3] - 1], lvlList[3], oldLvlList[3]);
                }
            }
            CheckDetailProgress();
        }
    }
}
