﻿using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.Save;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;
using BlackBears.DiggIsBig.UI;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Localization;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class StorageController : MonoBehaviour
    {
        [SerializeField] private ResAlert unlockResAlert;
        [SerializeField] private SaleAllStorageAlert saleAllStorageAlert;
        [SerializeField] private PickUpChestAlert pickUpChestAlert;
        [SerializeField] private StorageUnit resUnitInWindow;
        [SerializeField] private GameObject boxForRes;

        [SerializeField] private GameController gameController;
        [SerializeField] private Slider storageProgress;

        [SerializeField] private Text goldTextInAlert;
        [SerializeField] private Text spaceText;
        [SerializeField] private Text goldText;
        [SerializeField] private Text goldTextInStorage;
        [SerializeField] private Text goldTextInGarage;
        [SerializeField] private Text goldTextInDonateShop;
        [SerializeField] private Text goldTextInBeamTopBar;

        private Dictionary<Loot, PDouble> lootDictionary;

        private Dictionary<Loot, StorageUnit> unitsInWindow;
        private List<StorageUnit> blockingUnits;
        private List<Loot> knownRes;
        private float lootMultiplier;
        private float locationMultiplier;
        private float premiumMultiplier = 1;
        private float clickBoost = 1;
        private PDouble storageStock = 1000;
        private PDouble resCount;
        private PDouble money;
        private double correction;
        private PInt storageLvl = 0;
        private int perc;
        private Chest actualChest;
        private bool beamState;
        private GameConfigurationManager gameConfigurationManager;
        private DrillFormatter drillFormatter;
        private double oldCount;
        private double allResPrice;

        #region Save/Load
        private void Save()
        {
            List<SaveData> data = new List<SaveData>();
            List<SaveData> resData = new List<SaveData>();

            data.Add(new SaveData(SaveKeys.moneyDataKey, money.Value));
            data.Add(new SaveData(SaveKeys.storageStockKey, storageStock.Value));
            data.Add(new SaveData(SaveKeys.setBigChestKey, GameData.GameData.bigChestIsSet));


            foreach (var index in lootDictionary)
            {
                JSONObject json = new JSONObject();
                json[SaveKeys.countKey] = index.Value.Value;
                json[SaveKeys.isUnlockKey] = index.Key.isUnlock;
                json[SaveKeys.farmCountKey] = index.Key.lootCount;
                json[SaveKeys.unlockTimeKey] = index.Key.unlockWorkTime;
                json[SaveKeys.unlockProcessKey] = index.Key.unlockProcessIsStart;

                resData.Add(new SaveData(index.Key.type.ToString(), json));
            }

            data.Add(new SaveData(SaveKeys.resourceKey, resData));

            SaveCreator.Save(SaveKeys.storageSaveKey, data);
        }

        private void Load(OfflineCalcData ocd)
        {
            Debug.Log("Storage");
            var loadData = SaveCreator.GetLoadData(SaveKeys.storageSaveKey).AsArray;
            if (loadData != null)
            {

                for (int i = 0; i < loadData.Count; i++)
                {
                    if (loadData[i].HasKey(SaveKeys.moneyDataKey))
                    {
                        money = loadData[i][SaveKeys.moneyDataKey].AsDouble;
                        CreateInfo();
                    }
                    else if (loadData[i].HasKey(SaveKeys.storageStockKey))
                    {
                        storageStock = loadData[i][SaveKeys.storageStockKey].AsDouble;
                    }
                    else if (loadData[i].HasKey(SaveKeys.resourceKey))
                    {
                        ResourcesInfoLoad(loadData[i][SaveKeys.resourceKey][SaveKeys.resourceKey], ocd);
                    }
                }

            }

            // gameController.PutInfointoTechnicalWindow(ocd, oldCount, storageStock);
        }
        List<Loot> keys;
        private void ResourcesInfoLoad(JSONNode node, OfflineCalcData ocd)
        {
            GameData.GameData.deep = ocd.deep;
            gameController.CreateKnownListInStorage();
            List<double> resCounts = ocd.resourcesCount;

            var loadData = node.AsArray;
            keys = new List<Loot>();
            List<int> iter = new List<int>();

            foreach (var index in lootDictionary)
            {
                for (int j = 0; j < loadData.Count; j++)
                {
                    if (loadData[j].HasKey(index.Key.type.ToString()))
                    {
                        keys.Add(index.Key);
                        iter.Add(j);
                        break;
                    }
                }
            }

            resCount = 0;
            oldCount = 0;

            for (int i1 = 0; i1 < keys.Count; i1++)
            {
                var i = i1;
                var data = loadData[iter[i]][keys[i].type.ToString()];
                lootDictionary[keys[i]] = data[SaveKeys.countKey].AsInt + resCounts[i];
                oldCount += data[SaveKeys.countKey].AsInt;
                keys[i].isUnlock = data[SaveKeys.isUnlockKey].AsBool;
                keys[i].unlockProcessIsStart = data[SaveKeys.unlockProcessKey].AsBool;

                if (keys[i].unlockProcessIsStart)
                {
                    keys[i].unlockWorkTime = ((data[SaveKeys.unlockTimeKey].AsFloat - Chronometer.TimeModule.TimeFromLastSave) > 0) ? (data[SaveKeys.unlockTimeKey].AsFloat - Chronometer.TimeModule.TimeFromLastSave) : 0;

                    unitsInWindow[keys[i]].StartUnlockTimer(keys[i], () => UnlockRes(keys[i], unitsInWindow[keys[i]]));

                    unitsInWindow[keys[i]].ResetUnlockTimerTime(keys[i].unlockWorkTime);
                }
                RefreshResData(new KeyValuePair<Loot, PDouble>(keys[i], lootDictionary[keys[i]]));
                resCount += lootDictionary[keys[i]];
            }

            CreateInfo();
            CheckUnlockResources();

            if (Chronometer.TimeModule.TimeFromLastSave >= gameConfigurationManager.offlineData.time
             && perc >= gameConfigurationManager.offlineData.percent)
            {
                saleAllStorageAlert.ShowHideOfflineCellInStorage(true);
                CreateInfo();
            }
        }

        #endregion
        private void Awake()
        {
            SubscribeToEvents();
            InitVar();
        }

        private void InitVar()
        {
            money = new PDouble();
            resCount = new PDouble();
            storageStock = new PDouble();
            storageLvl = new PInt();
            money = 0;
            drillFormatter = new DrillFormatter();
            beamState = false;
            gameConfigurationManager = gameController.GetConfigurationManager();
            storageStock = gameConfigurationManager.detailData.storage[0].capacity;
            lootMultiplier = 1;
            locationMultiplier = 1;
            knownRes = new List<Loot>();
            unitsInWindow = new Dictionary<Loot, StorageUnit>();
            blockingUnits = new List<StorageUnit>();
            lootDictionary = new Dictionary<Loot, PDouble>();
            resCount = 0;
        }

        private void SubscribeToEvents()
        {
            EventManager.SaveGame += Save;
            EventManager.LoadGame += Load;
            EventManager.BeamSpawn += (a, b) => beamState = true;
            EventManager.BeamCrash += () => beamState = false;
            EventManager.DeepResourceChange += _ => CPSCalc();
            EventManager.UpStorage += UpStorage;
            EventManager.PickUpResources += PickUpRes;
            EventManager.PickUpChest += OpenGoldChest;
            EventManager.Subscribe += SomePremMethod;
            EventManager.RouletteWin += RouletteWin;
            EventManager.PickUpResBox += PickUpResBox;
            EventManager.GiveMeMoney += (money) =>
            {
                this.money += money;
                CreateInfo();
            };

            EventManager.TakeMyMoney += (money) => SpendMoney(money);

            EventManager.LadlesSwap += (ladleMultipler) =>
            {
                GameData.GameData.LadleMultiplier = ladleMultipler * GameData.GameData.LadleMultiplierBoost;
                CPSCalc();
            };

            EventManager.ClickBoostUsed += (boost, time) => clickBoost = boost;
            EventManager.ClickBoostStop += () => clickBoost = 1;
        }

        private void RouletteWin(RouletteWinElements rouletteWinElements)
        {
            var rouletteData = gameConfigurationManager.rouletteData;
            switch (rouletteWinElements)
            {
                case RouletteWinElements.GoldPack1:
                    AddGold(rouletteData.golds[0].gameTime);
                    break;
                case RouletteWinElements.GoldPack2:
                    AddGold(rouletteData.golds[1].gameTime);
                    break;
                case RouletteWinElements.GoldPack3:
                    AddGold(rouletteData.golds[2].gameTime);
                    break;
                case RouletteWinElements.GoldPack4:
                    AddGold(rouletteData.golds[3].gameTime);
                    break;
                case RouletteWinElements.GoldPack5:
                    AddGold(rouletteData.golds[4].gameTime);
                    break;
            }
        }

        private void SomePremMethod(bool sub)
        {
            if (sub)
            {
                premiumMultiplier = 1.5f;
            }
            else
            {
                premiumMultiplier = 1;
            }
        }

        private void CreateInfo()
        {
            //заполненность склада в процентах
            perc = (int)(((double)resCount / (double)storageStock) * 100);
            spaceText.text = perc + "%";

            if (saleAllStorageAlert.alertIsActive)
            {
                allResPrice = 0;
                foreach (var item in lootDictionary)
                {
                    if (item.Key.isUnlock)
                        allResPrice += item.Value * item.Key.lootPrice;
                }

                saleAllStorageAlert.SetPriceText(allResPrice);
                saleAllStorageAlert.SetButtonAction(SaleAllResources);
            }

            EventManager.FullStorageEvent(perc);

            storageProgress.value = (perc);
            string cash = drillFormatter.MoneyKFormatter(money);
            goldText.text = cash;
            goldTextInGarage.text = cash;
            goldTextInStorage.text = cash;
            goldTextInDonateShop.text = cash;
            goldTextInBeamTopBar.text = cash;
            goldTextInAlert.text = cash;
            GameData.GameData.goldCount = money;
        }

        private void SaleAllResources()
        {
            foreach (var item in knownRes)
            {
                if (item.isUnlock)
                {
                    double lootForSaleCount = lootDictionary[item];
                    resCount -= lootForSaleCount;
                    if (resCount < 0) resCount = 0;
                    money += lootForSaleCount * (item.lootPrice * premiumMultiplier) * 2;
                    lootDictionary[item] = 0;
                    RefreshResData(new KeyValuePair<Loot, PDouble>(item, lootDictionary[item]));
                    correction = 0;
                }
            }
            CreateInfo();
        }

        private void AddLootTextUnit(Loot loot, float ladleMultipler)
        {
            foreach (var item in lootDictionary)
            {
                if (loot.type.Equals(item.Key.type))
                {
                    lootDictionary[item.Key] += Mathf.Round((float)((loot.lootCount * lootMultiplier * ladleMultipler * locationMultiplier) - correction));
                    RefreshResData(item);
                    return;
                }
            }
        }

        private void RefreshResData(KeyValuePair<Loot, PDouble> item)
        {
            var resource = unitsInWindow[item.Key];

            var price = drillFormatter.MoneyKFormatter((lootDictionary[item.Key] * item.Key.lootPrice * premiumMultiplier));
            resource.saleResPrice.text = price;
            resource.saleResPriceBlock.text = price;

            resource.resCount.text = drillFormatter.MoneyKFormatter(lootDictionary[item.Key]);

            resource.resCountProgress.maxValue = (float)storageStock;
            resource.resCountProgress.value = (float)lootDictionary[item.Key];

        }

        private void AddDataAndListenerToUnit()
        {
            foreach (var item in unitsInWindow)
            {
                var resource = item.Value;
                resource.resImage.sprite = item.Key.GetResImage();

                string resKey = "res_" + item.Key.type;

                resource.resName.text = DigIsBigLocalizator.GetLocalizationString(resKey.ToUpper(), Modificator.ToUpper);
                resource.saleResButton.onClick.RemoveAllListeners();
                resource.saleResButton.onClick.AddListener(() => SaleAll(item.Key));

                if (resource.unlockButton.gameObject.activeSelf)
                {
                    if (!item.Key.unlockProcessIsStart)
                    {
                        Debug.Log("CHECK THIS SHIT");
                        resource.unlockResPrice.text = DigIsBigLocalizator.GetLocalizationString("STOR_UNIT_UNLOCK_BTN", Modificator.ToUpper);
                        resource.unlockLabelText.text = DigIsBigLocalizator.GetLocalizationString("STOR_UNIT_SEARCH_RES", Modificator.DontModify);

                        resource.unlockButton.onClick.RemoveAllListeners();
                        resource.unlockButton.onClick.AddListener(() =>
                        {
                            item.Key.unlockProcessIsStart = true;
                            resource.StartUnlockTimer(item.Key, () => UnlockRes(item.Key, resource));
                        });
                    }
                    // resource.unlockButton.onClick.RemoveAllListeners();
                    // resource.unlockResPrice.text = drillFormatter.MoneyKFormatter(item.Key.unlockPrice);
                    // resource.unlockButton.onClick.AddListener(() => UnlockRes(item.Key, resource.unlockButton));
                }
            }
        }
        private void CheckUnlockResources()
        {
            bool allBad;
            foreach (var item in unitsInWindow)
            {
                allBad = false;
                if (item.Key.isUnlock && item.Value.BlockState)
                {
                    item.Value.Unblock();
                    SortGoodUnit(item.Value);
                }
                else if (!item.Key.isUnlock)
                {
                    foreach (var index in blockingUnits)
                    {
                        if (index.resName == item.Value.resName) allBad = true;
                    }
                    if (!allBad)
                        blockingUnits.Add(item.Value);
                }
            }
            SortingBlockingUnits();
        }

        private void SortingBlockingUnits()
        {
            if (blockingUnits.Count != 0)
            {
                for (int i = 0; i < blockingUnits.Count; i++)
                {
                    if (i == 0) blockingUnits[i].ShowSeparator(false);
                    else blockingUnits[i].ShowSeparator(true);
                    blockingUnits[i].transform.SetSiblingIndex(i + 1);
                }
            }
        }

        private void SaleAll(Loot loot)
        {
            double lootForSaleCount = lootDictionary[loot];
            resCount -= lootForSaleCount;
            if (resCount < 0) resCount = 0;
            money += lootForSaleCount * (loot.lootPrice * premiumMultiplier);
            lootDictionary[loot] = 0;
            RefreshResData(new KeyValuePair<Loot, PDouble>(loot, lootDictionary[loot]));
            saleAllStorageAlert.ShowHideOfflineCellInStorage(false);
            CreateInfo();
            correction = 0;
        }

        private void SortGoodUnit(StorageUnit unit)
        {

            for (int i = 0; i < blockingUnits.Count; i++)
            {
                if (unit.resName == blockingUnits[i].resName)
                {
                    blockingUnits.RemoveAt(i);
                    break;
                }
            }
            SortingBlockingUnits();

            int index = 0;
            foreach (var element in unitsInWindow)
            {
                if (unit.resName == element.Value.resName)
                {
                    unit.transform.SetSiblingIndex(index + blockingUnits.Count + 1);
                    break;
                }
                index++;
            }
        }

        private void UnlockRes(Loot loot, StorageUnit unit)
        {
            // var res = SpendMoney(loot.unlockPrice);
            // if (res == 0)
            // {
            loot.unlockProcessIsStart = false;
            loot.isUnlock = true;
            unit.Unblock();
            string resKey = "res_" + loot.type;
            unlockResAlert.ShowResAlert(loot.GetResImage(), DigIsBigLocalizator.GetLocalizationString(resKey.ToUpper(), Modificator.ToUpper));
            SortGoodUnit(unit);

            // }
            // else
            // {
            //     EventManager.NotEnoughtMoneyEvent(res, () =>
            //     {
            //         loot.isUnlock = true;
            //         unit.gameObject.SetActive(false);
            //     });
            // }
        }


        private void OpenGoldChest(Loot loot)
        {
            Chest chest = loot.GetComponent<Chest>();
            string gold = drillFormatter.MoneyKFormatter((chest.OpenGoldChest));
            string goldX6 = drillFormatter.MoneyKFormatter((chest.OpenGoldChest * 6));
            actualChest = chest;

            // if (!chest.advertise)
            // {
            //     pickUpChestAlert.OpenChestWithGold(chest.GetChestType, false, gold, () =>
            //       {
            //           OpenGoldChest();
            //       }, () => { });
            // }
            // else
            // {
            pickUpChestAlert.OpenChestWithGold(chest.GetChestType, true, gold, () =>
              OpenGoldChest(), () => OpenGoldChest(6), goldX6);
            // }
        }

        private void PickUpResBox(Loot loot, bool isPickUp)
        {
            if (isPickUp)
            {
                ResBox rb = loot.GetComponent<ResBox>();
                double countResInBox = (rb.resourcesCount * lootMultiplier * locationMultiplier);
                resCount += countResInBox;

                if (resCount > storageStock)
                {
                    correction = resCount - storageStock;
                    resCount = storageStock;
                }
                else
                {
                    correction = 0;
                }

                foreach (var item in lootDictionary)
                {
                    if (rb.resource.type.Equals(item.Key.type))
                    {
                        lootDictionary[item.Key] += (countResInBox - correction);
                        RefreshResData(item);
                        break;
                    }
                }
                rb.ShowLabel(drillFormatter.MoneyKFormatter(countResInBox));
                CreateInfo();
            }
        }

        private void PickUpRes(Loot loot, float ladleMultipler = 1)
        {
            if (!GameData.GameData.wildFarmMode)
            {
                loot.lootCount *= (GameData.GameData.actualClickMultiplier * clickBoost);

                resCount += (loot.lootCount * lootMultiplier * ladleMultipler * locationMultiplier);

                if (resCount > storageStock)
                {
                    correction = resCount - storageStock;
                    resCount = storageStock;
                }
                else
                {
                    correction = 0;
                }

                AddLootTextUnit(loot, ladleMultipler);
                CreateInfo();
            }
        }

        private void addRes(Loot loot, float ladleMultipler = 1)
        {
            resCount += (loot.lootCount * lootMultiplier * ladleMultipler * locationMultiplier);

            if (resCount > storageStock)
            {
                correction = resCount - storageStock;
                resCount = storageStock;
            }
            else
            {
                correction = 0;
            }

            AddLootTextUnit(loot, ladleMultipler);
            CreateInfo();

        }

        private void UpStorage(int lvl)
        {
            storageLvl = lvl - 1;
            if (storageLvl < 0) storageLvl = 0;
            storageStock = gameConfigurationManager.detailData.storage[storageLvl].capacity;

            CreateInfo();
        }

        public void AddKnownRes(Loot res)
        {
            foreach (var index in knownRes)
            {
                if (res.Equals(index))
                {
                    return;
                }
            }

            knownRes.Add(res);
            lootDictionary.Add(res, 0);

            StorageUnit unit = Instantiate(resUnitInWindow, new Vector3(0, 0, 0), Quaternion.identity, boxForRes.transform);
            unitsInWindow.Add(res, unit);

            AddDataAndListenerToUnit();
            CheckUnlockResources();
        }

        #region  PressChestWindowMethods
        public void OpenGoldChest(int multiplier = 1)
        {
            money += actualChest.OpenGoldChest * multiplier;
            CreateInfo();
        }
        #endregion

        public void SetLootMultipler(float newMultiplier) => lootMultiplier = (int)newMultiplier;

        public void SetLocationMultiplier(float newMultiplier) => locationMultiplier = newMultiplier;

        public void Income(float ladleMultipler)
        {
            if (GameData.GameData.LadleMultiplier != ladleMultipler)
            {
                GameData.GameData.LadleMultiplier = ladleMultipler * GameData.GameData.LadleMultiplierBoost;
                CPSCalc();
            }
            foreach (var index in knownRes)
            {
                addRes(index, GameData.GameData.LadleMultiplier);
            }
        }

        public void AddGold(double gameTime)
        {
            gameTime = gameTime * GameData.GameData.CPS;
            actualChest = gameObject.AddComponent<Chest>();
            actualChest.PutSomeGold(gameTime);
            OpenGoldChest();
        }

        public void OpenBigChest()
        {
            var gold = drillFormatter.MoneyKFormatter((GameData.GameData.CPS * gameConfigurationManager.dropData.drop[1].gameTime));
            pickUpChestAlert.OpenChestWithGold(ChestType.BigChest, false, gold, () =>
                {
                    AddGold(gameConfigurationManager.dropData.drop[1].gameTime);
                }, () => { });
            GameData.GameData.bigChestIsSet = false;
        }

        public double SpendMoney(double spend)
        {
            if (spend <= money)
            {
                money -= spend;
                CreateInfo();
                return 0;
            }
            else
            {
                return spend - money;
            }
        }

        public bool SpendMoneyInfo(double spend) => (spend <= money) ? true : false;

        public bool CheckFreeSpace() => (resCount < storageStock) ? true : false;

        public void CPSCalc()
        {
            double cps = 0;
            foreach (var index in lootDictionary)
            {
                cps += (index.Key.lootPrice * index.Key.lootCount * GameData.GameData.actualClickMultiplier);
            }
            GameData.GameData.CPS = cps;
            EventManager.CPSChangedEvent();
        }
    }
}
