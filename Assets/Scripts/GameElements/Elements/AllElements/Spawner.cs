﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.RandomRange;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Drop;
using UnityEngine;
using BlackBears.DiggIsBig.Common;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private Vector2 resTapAreaWithClickBoost;
        [SerializeField] private Vector2 resTapAreaWithoutClickBoost;

        [SerializeField] private float yDestroyLootPosition;
        [SerializeField] private GameObject bigChestButton;
        [SerializeField] private GameController gameController;
        [SerializeField] private Confines confines;
        [SerializeField] private List<GameObject> lootResObjects;
        [SerializeField] private List<Chest> lootChestObjects;
        [SerializeField] private List<Booster> boosterObjects;
        [SerializeField] private GameObject boxWithResources;
        [SerializeField] private GameObject uniqRes;

        [SerializeField] private SpawnResPoint spawnResPointLeft;
        [SerializeField] private SpawnResPoint spawnResPointRight;

        private float resSpawnDivider;
        private float smallChestGameTime;
        private float resBoxGameTime;
        private float resSpawnDelay;
        private float boost;
        private bool pause;
        private bool clickBoostIsActive;
        private GameConfigurationManager gameConfigurationManager;
        private int apperanceResCount;
        private List<Loot> lootSpawnList;
        private Loot lastSpawnBoost;
        public List<GameObject> GetResLootObjects => lootResObjects;


        private void Awake()
        {
            GameData.GameData.bigChestIsSet = false;
            SubscribeToEvents();
        }

        private float caveSpeed = 0;
        private void Update() => caveSpeed = gameController.GetConstVisualSpeed();

        private void CalcAFKTranslate()
        {
            float time = Chronometer.TimeModule.TimeFromLastSave;
            float range = time * caveSpeed;
            float smallestYPos = 2000;

            foreach (Transform tr in transform)
            {
                tr.Translate(new Vector3(0, range, 0));
                if (tr.position.y < smallestYPos) smallestYPos = tr.position.y;
            }

            for (float i = 0; (confines.yMin + i) < smallestYPos; i += resSpawnDivider)
            {
                GameObject loot = Instantiate(lootSpawnList[Random.Range(0, lootSpawnList.Count)].gameObject, new Vector3(Random.Range(confines.xMin, confines.xMax), confines.yMin + i, 1), Quaternion.identity, gameObject.transform);
                loot.GetComponent<Loot>().yDestroyPos = yDestroyLootPosition;
            }
            if (GameData.GameData.clickBoosterIsActive) ClickBoost(true);
        }

        private void Start()
        {
            InitVar();
            StartCoroutine(LootSpawner());
        }

        private void InitVar()
        {
            gameConfigurationManager = gameController.GetConfigurationManager();
            boost = 1;
            pause = false;
            SetConfig();
        }

        private void SetConfig()
        {
            ChestConfigDataSet();
            ResConfigDataSet();
            SetBoostConfig();
        }

        private void SetBoostConfig()
        {
            var boostData = gameConfigurationManager.boostersData;

            //0 - click, 1 - barrel, 2 - magnet, 3 - gold boost

            for (int i = 0; i < boosterObjects.Count; i++)
            {
                boosterObjects[i].multiplier = boostData.boosters[i].multiplier;
                boosterObjects[i].workTime = boostData.boosters[i].workTime;
                boosterObjects[i].dropChance = boostData.boosters[i].spawnChance;
            }
        }
        private void SubscribeToEvents()
        {
            EventManager.SpawnBigChest += SpawnBigChest;
            EventManager.DeepResourceChange += SetResDataOnKm;
            EventManager.ChestSpawn += ChestSpawn;
            EventManager.GamePaused += SpawnStop;
            EventManager.GameUnpaused += SpawnStart;
            EventManager.LoadGame += Load;
            EventManager.BoxSpawn += ResBoxSpawn;
            EventManager.SpawnUniqRes += SpawnUniqRes;
            EventManager.BoosterSpawn += BoosterSpawner;
            EventManager.PickUpBarrel += () => ResFieldGenerator(1);
            EventManager.ClickBoostUsed += (a, b) => ClickBoost(true);
            EventManager.ClickBoostStop += () => ClickBoost(false);
        }

        private void Load(OfflineCalcData ocd)
        {
            GameData.GameData.bigChestIsSet = ocd.bigChestIsSet;

            if (ocd.bigChestIsSet) SpawnBigChest();

            if (!GameData.GameData.firstSession) CalcAFKTranslate();
        }

        private void SpawnStart() => pause = false;
        private void SpawnStop() => pause = true;

        #region  ConfigSet

        //установка конфига сундуков
        private void ChestConfigDataSet()
        {
            var dropConfigData = gameConfigurationManager.dropData;

            lootChestObjects[0].advertise = dropConfigData.drop[0].advertise;
            smallChestGameTime = dropConfigData.drop[0].gameTime;

            resBoxGameTime = dropConfigData.drop[2].gameTime;

            uniqRes.GetComponent<UniqRes>().advGameTime = dropConfigData.drop[3].advGameTime; ;
            uniqRes.GetComponent<UniqRes>().usualGameTime = dropConfigData.drop[3].gameTime; ;
        }

        //установка конфига ресурсов и спавна
        private void ResConfigDataSet()
        {
            var spawnConfigDataSet = gameConfigurationManager.spawnData;

            resSpawnDivider = spawnConfigDataSet.resourceSpawnDivider;

            var resourcesConfigDataSet = gameConfigurationManager.resourcesData.resources;

            for (int i = 0; i < lootResObjects.Count; i++)
            {
                Loot actualLoot = lootResObjects[i].GetComponent<Loot>();

                actualLoot.isAppearance = false;
                actualLoot.isUnlock = false;
                actualLoot.unlockProcessIsStart = false;
                actualLoot.lootPrice = resourcesConfigDataSet[i].lootPrice;
                actualLoot.lootCount = resourcesConfigDataSet[i].startCount;
                actualLoot.unlockPrice = resourcesConfigDataSet[i].unlockPrice;
                actualLoot.openDeep = resourcesConfigDataSet[i].openDeep;
                //time
                actualLoot.unlockTime = resourcesConfigDataSet[i].unlockTime;
                actualLoot.unlockWorkTime = actualLoot.unlockTime;
                actualLoot.unlockAdvCount = resourcesConfigDataSet[i].unlockAdvCount;
            }

            lootResObjects[0].GetComponent<Loot>().isAppearance = true;
            lootResObjects[0].GetComponent<Loot>().isUnlock = true;
        }


        private void SetResDataOnKm(float[] resInfo)
        {
            for (int i = 0; i < lootResObjects.Count; i++)
            {
                Loot actualLoot = lootResObjects[i].GetComponent<Loot>();
                actualLoot.lootCount = resInfo[i];
            }
            CheckRedyLoot();
        }
        #endregion
        #region  Spawn

        private void SpawnBigChest()
        {
            GameData.GameData.bigChestIsSet = true;
            bigChestButton.SetActive(true);
        }

        private IEnumerator LootSpawner()
        {
            while (true)
            {
                if (resSpawnDelay < 7) yield return null;
                else
                {
                    yield return new WaitForSeconds(resSpawnDivider / resSpawnDelay);
                    if (!pause)
                    {
                        GameObject loot = Instantiate(lootSpawnList[Random.Range(0, lootSpawnList.Count)].gameObject, new Vector3(Random.Range(confines.xMin, confines.xMax),
                        Random.Range(confines.yMin, confines.yMax), 1), Quaternion.identity, gameObject.transform);
                        Loot componentLoot = loot.GetComponent<Loot>();
                        componentLoot.yDestroyPos = yDestroyLootPosition;

                        if (clickBoostIsActive) loot.GetComponent<BoxCollider2D>().size = resTapAreaWithClickBoost;
                        else loot.GetComponent<BoxCollider2D>().size = resTapAreaWithoutClickBoost;

                        if (GameData.GameData.goldBoosterIsActive) componentLoot.ResToGold();
                    }
                    else
                    {
                        while (pause)
                        {
                            yield return null;
                        }
                    }
                }
            }
        }

        private void BoosterSpawner()
        {
            //!!! НЕ Unity рандом, скрипт из Common.
            IntRange[] boosters = new IntRange[boosterObjects.Count];
            for (int i = 0; i < boosters.Length; i++)
            {
                boosters[i] = new IntRange(i, boosterObjects[i].dropChance);
            }

            int elementIndex = RandomRange.Range(boosters);

            if (lastSpawnBoost != null)
            {
                if (lastSpawnBoost.type == LootType.WildFarm &&
                    boosterObjects[elementIndex].GetComponent<Loot>().type == LootType.WildFarm)
                {
                    BoosterSpawner();
                    return;
                }

                if (clickBoostIsActive &&
                    boosterObjects[elementIndex].GetComponent<Loot>().type == LootType.WildFarm)
                {
                    BoosterSpawner();
                    return;
                }
            }

            GameObject loot = Instantiate(boosterObjects[elementIndex].gameObject, new Vector3(Random.Range(confines.xMin, confines.xMax), Random.Range(confines.yMin, confines.yMax), 1), Quaternion.identity, gameObject.transform);
            Loot thisLoot = loot.GetComponent<Loot>();

            lastSpawnBoost = thisLoot;
            thisLoot.yDestroyPos = yDestroyLootPosition;
            // var booster = loot.GetComponent<Booster>();
        }

        public void ResFieldGenerator(int countGeneration = 1)
        {
            for (int j = 0; j < countGeneration; j++)
            {
                Vector2 spawnConfines = new Vector2(confines.yMin, yDestroyLootPosition);

                for (float i = 0; (confines.yMin + i) < yDestroyLootPosition; i += resSpawnDivider)
                {
                    GameObject loot = Instantiate(lootSpawnList[Random.Range(0, lootSpawnList.Count)].gameObject, new Vector3(Random.Range(confines.xMin, confines.xMax), Random.Range(i + confines.yMin, i + confines.yMax), 1), Quaternion.identity, gameObject.transform);
                    loot.GetComponent<Loot>().yDestroyPos = yDestroyLootPosition;
                }
            }

            if (GameData.GameData.clickBoosterIsActive) ClickBoost(true);
        }

        private void ChestSpawn()
        {
            GameObject chest;
            chest = Instantiate(lootChestObjects[0].gameObject, new Vector3(Random.Range(confines.xMin, confines.xMax), Random.Range(confines.yMin, confines.yMax), 1), Quaternion.identity, gameObject.transform);
            chest.GetComponent<Chest>().PutSomeGold((long)(smallChestGameTime * GameData.GameData.CPS));
            chest.GetComponent<Loot>().yDestroyPos = yDestroyLootPosition;
        }

        private void ResBoxSpawn()
        {
            GameObject resBox;
            resBox = Instantiate(boxWithResources, new Vector3(Random.Range(confines.xMin, confines.xMax), Random.Range(confines.yMin, confines.yMax), 1), Quaternion.identity, gameObject.transform);
            resBox.GetComponent<Loot>().yDestroyPos = yDestroyLootPosition;

            ResBox box = resBox.GetComponent<ResBox>();
            Loot resForBox = lootSpawnList[Random.Range(0, lootSpawnList.Count)];
            box.PutResourcesInBox(resForBox, (long)(resForBox.lootCount * GameData.GameData.LadleMultiplier * resBoxGameTime));
        }

        private void SpawnUniqRes()
        {
            GameObject res;
            res = Instantiate(uniqRes, new Vector3(Random.Range(confines.xMin, confines.xMax), Random.Range(confines.yMin, confines.yMax), 1), Quaternion.identity, gameObject.transform);
            res.GetComponent<Loot>().yDestroyPos = yDestroyLootPosition;
        }
        #endregion

        private void ClickBoost(bool isActive)
        {
            clickBoostIsActive = isActive;
            GameObject element;
            foreach (Transform child in this.transform)
            {
                element = child.gameObject;
                if (element.CompareTag("Resource"))
                {
                    if (isActive) element.GetComponent<BoxCollider2D>().size = resTapAreaWithClickBoost;
                    else element.GetComponent<BoxCollider2D>().size = resTapAreaWithoutClickBoost;
                }
            }
        }
        public void CheckRedyLoot()
        {
            lootSpawnList = new List<Loot>();
            List<Loot> lootSpawnHelpList = new List<Loot>();

            Loot check;
            int counter = 0;
            for (int i = 0; i < lootResObjects.Count; i++)
            {
                check = lootResObjects[i].GetComponent<Loot>();
                var deepKM = GameData.GameData.deep;

                if (check.openDeep <= (deepKM / 1000) && check.isAppearance.Equals(false))
                {
                    check.isAppearance = true;
                }

                if (check.isAppearance.Equals(true))
                {
                    counter++;
                    lootSpawnHelpList.Add(check);
                }
            }

            //создание списка для спавна
            for (int i = lootSpawnHelpList.Count, j = 0; i > 0; i--)
            {
                if (lootSpawnHelpList[i - 1].lootCount > 0 && j < 3)
                {
                    j++;
                    lootSpawnList.Add(lootSpawnHelpList[i - 1]);
                }
            }

            apperanceResCount = counter;
            counter = 0;

            spawnResPointLeft.SetLootList(lootSpawnHelpList);
            spawnResPointRight.SetLootList(lootSpawnHelpList);
        }

        public void SetSpeedForDependence(float speed) => resSpawnDelay = speed;
    }
}