﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.GameElements.Location;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Controllers
{
    public class LocationController : MonoBehaviour
    {
        [SerializeField] private Location[] locations;
        [SerializeField] private GameObject backgroundObj;
        [SerializeField] private GameController gameController;
        [SerializeField] private GameObject fakeSides;

        [SerializeField] private Image startPosImage;
        [SerializeField] private Image pointPosImage;
        [SerializeField] private Image startPosInBeamImage;
        [SerializeField] private Image pointPosInBeamImage;
        [SerializeField] Text pointText;
        [SerializeField] Text startPosText;
        [SerializeField] Text actualLocationText;
        [SerializeField] Text startPosTextInBeam;
        [SerializeField] Text pointLocationTextInBeam;

        private GameConfigurationManager gameConfigurationManager;
        private GameObject fakeBG;
        private GameObject fakeSidesForSpawn;
        private bool loadBeamSpawn;
        private int lvl;
        private int locationNumber = 0;
        private bool isBigBeam;

        #region Save/Load
        private void Save()
        {
            List<SaveData> data = new List<SaveData>();

            data.Add(new SaveData(SaveKeys.actualLvlDataKey, lvl));
            data.Add(new SaveData(SaveKeys.actualLocationDataKey, locationNumber));

            SaveCreator.Save(SaveKeys.locationSaveKey, data);
        }


        private void Load(OfflineCalcData ocd)
        {
            var loadData = SaveCreator.GetLoadData(SaveKeys.locationSaveKey).AsArray;
            Debug.Log("LocationController");
            if (GameData.GameData.firstStart)
            {
                if (loadData != null)
                {
                    for (int i = 0; i < loadData.Count; i++)
                    {
                        if (loadData[i].HasKey(SaveKeys.actualLvlDataKey))
                        {
                            lvl = loadData[i][SaveKeys.actualLvlDataKey];
                        }
                    }
                    locationNumber = ocd.locationNumber;
                    GameData.GameData.locationNumber = locationNumber;
                    SetLocationConditions();
                    if (ocd.bigBeamIsSet && GameData.GameData.firstStart) loadBeamSpawn = true;
                    SetAllPreview();
                    SetPointText();
                }
            }
        }

        #endregion

        void Start()
        {
            SetConfig();
            SubscribeToEvents();
            lvl = 1;
            isBigBeam = false;
            GameData.GameData.locationNumber = locationNumber;
            SetLocationConditions();
            SetAllPreview();
            SetPointText();
        }
        private void SetConfig()
        {
            gameConfigurationManager = gameController.GetConfigurationManager();

            var locationsData = gameConfigurationManager.locationData;

            for (int i = 0; i < locations.Length; i++)
            {
                locations[i].SetLocationConfig(locationsData.allLocationArray[i]);
            }
        }

        private void SubscribeToEvents()
        {
            EventManager.SaveGame += Save;
            EventManager.LoadGame += Load;
            EventManager.NewKmDeep += CheckBigBeamSpawn;

            EventManager.BeamCrash += BeamKill;
            EventManager.BeamSpawn += BeamCreate;
        }

        private void CheckBigBeamSpawn()
        {
            var deepKM = (long)(GameData.GameData.deep / 1000);

            if (locationNumber == (gameConfigurationManager.locationData.allLocationArray.Length - 1))
            {
                Debug.Log("GameOver");
            }
            else
            {
                if (deepKM == gameConfigurationManager.locationData.allLocationArray[locationNumber + 1].startKm)
                {
                    EventManager.BeamSpawnEvent(true, false);
                }
            }
        }


        private void SetAllPreview()
        {
            startPosImage.sprite = locations[locationNumber].GetPreviewImage;
            pointPosImage.sprite = locations[locationNumber + 1].GetPreviewImage;

            startPosInBeamImage.sprite = locations[locationNumber].GetPreviewImage;
            pointPosInBeamImage.sprite = locations[locationNumber + 1].GetPreviewImage;
        }

        private void SetPointText()
        {
            string lvl = " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);

            startPosText.text = (locationNumber + 1).ToString();
            startPosTextInBeam.text = (locationNumber + 1) + lvl;

            pointText.text = (locationNumber + 2).ToString();
            pointLocationTextInBeam.text = (locationNumber + 2) + lvl;

            actualLocationText.text = (locationNumber + 1) + " " + DigIsBigLocalizator.GetLocalizationString("MAIN_LVL", Modificator.ToUpper);
        }


        private void BeamCreate(bool isBig, bool cap)
        {
            isBigBeam = isBig;

            if (isBig)
            {
                NextBGSpawn();
            }
        }

        private void BeamKill()
        {
            if (isBigBeam)
            {
                Destroy(fakeSidesForSpawn);
                Destroy(fakeBG);
                NextLocationNumber();
                AdjustEventManager.PlayerLvlEvent(locationNumber + 1);
                SetLocationConditions();
                EventManager.NextLocationEvent(locations[locationNumber]);
                SetAllPreview();
                SetPointText();
            }
        }

        private void NextLocationNumber()
        {
            if (locationNumber == locations.Length - 1) Debug.Log("GameOver");
            else locationNumber++;
            lvl++;
            GameData.GameData.locationNumber = locationNumber;
        }

        private void NextBGSpawn()
        {
            if (fakeBG == null)
            {
                GameObject goForSpawn = backgroundObj.transform.GetChild(0).gameObject;
                float yBgPos;
                if (loadBeamSpawn)
                {
                    yBgPos = -560;
                    loadBeamSpawn = false;
                }
                else
                {
                    yBgPos = -900;
#if UNITY_IOS
                    bool deviceIsIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
                    if (deviceIsIpad) yBgPos = -400;
#endif
                }


                fakeBG = Instantiate(goForSpawn, new Vector3(backgroundObj.transform.position.x, yBgPos, 0), Quaternion.identity, gameController.transform);
                fakeBG.GetComponent<SpriteRenderer>().sprite = locations[locationNumber + 1].GetBackgroundImage;
                fakeBG.GetComponent<SpriteRenderer>().sortingOrder = 2;

                fakeSidesForSpawn = Instantiate(fakeSides, new Vector3(0, 0, 0), Quaternion.identity, fakeBG.transform);
                var sprites = fakeSidesForSpawn.GetComponentsInChildren<SpriteRenderer>();

                foreach (var spr in sprites)
                    spr.sprite = locations[locationNumber + 1].GetSides[Random.Range(0, 1)];
            }
        }

        private void SetLocationConditions()
        {
            backgroundObj.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = locations[locationNumber].GetBackgroundImage;
            backgroundObj.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = locations[locationNumber].GetBackgroundImage;
            backgroundObj.GetComponent<SpriteRenderer>().sprite = locations[locationNumber].GetBackgroundImage;

            //установка условий локации
            float prodMult = 0;
            float rigidity = 0;
            float speedMult = 0;
            float visualRigidity = 0;
            float constSpeed = 0;
            float constVisualSpeed = 0;

            locations[locationNumber].GetLocationStates(out speedMult, out prodMult, out rigidity, out visualRigidity, out constSpeed, out constVisualSpeed);
            gameController.SetLocationConditions(speedMult, prodMult, rigidity, visualRigidity, constSpeed, constVisualSpeed);

            EventManager.LocationSwapEvent(locationNumber);
        }
    }
}
