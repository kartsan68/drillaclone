﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Controllers
{
    public class BoostLableController : MonoBehaviour
    {
        [SerializeField] private GameObject clickBoostLabel;
        [SerializeField] private GameObject magnetBoostLabel;
        [SerializeField] private GameObject ladleBoostLabel;
        [SerializeField] private GameObject wildFarmBoostLabel;

        private Text clickBoostText;
        private Text magnetBoostText;
        private Text ladleBoostText;
        private Text wildFarmBoostText;

        private CountdownTimer clickBoostTimer;
        private CountdownTimer magnetBoostTimer;
        private CountdownTimer ladleBoostTimer;
        private CountdownTimer wildFarmBoostTimer;

        private DrillFormatter drillFormatter;

        private void Awake()
        {
            InitVar();
            SubscribeToEvents();
        }

        private void OffAllLabels()
        {
            clickBoostLabel.SetActive(false);
            magnetBoostLabel.SetActive(false);
            ladleBoostLabel.SetActive(false);
            wildFarmBoostLabel.SetActive(false);
        }

        private void InitVar()
        {
            drillFormatter = new DrillFormatter();

            clickBoostText = clickBoostLabel.GetComponentInChildren<Text>();
            magnetBoostText = magnetBoostLabel.GetComponentInChildren<Text>();
            ladleBoostText = ladleBoostLabel.GetComponentInChildren<Text>();
            wildFarmBoostText = wildFarmBoostLabel.GetComponentInChildren<Text>();


            clickBoostTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            clickBoostTimer.SetUstopable(true);

            magnetBoostTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            magnetBoostTimer.SetUstopable(true);

            ladleBoostTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            ladleBoostTimer.SetUstopable(true);

            wildFarmBoostTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            wildFarmBoostTimer.SetUstopable(true);

            OffAllLabels();
        }

        private void SubscribeToEvents()
        {
            // EventManager.ClickBoostUsed += (boost, time) => StartCoroutine(ClickBoostUsed(time));
            // EventManager.MagnetBoostUsed += (boost, time) => StartCoroutine(MagnetBoostUsed(time));
            // EventManager.LadleBoostUsed += (boost, time) => StartCoroutine(LadleBoostUsed(time));
            // EventManager.WildFarmBoostUsed += (boost, time) => StartCoroutine(WildFarmBoostUsed(time));
            // EventManager.LoadGame += (a) => Reload();
        }

        private void Reload()
        {
            if (!GameData.GameData.firstStart)
            {
                var time = clickBoostTimer.GetTime();
                if (time > 0)
                {
                    time -= Chronometer.TimeModule.TimeFromLastSave;
                    if (time < 0) time = 0;
                    clickBoostTimer.ResetTimer(time, () => clickBoostLabel.SetActive(false));
                }
            }
        }

        private IEnumerator ClickBoostUsed(float time)
        {
            clickBoostLabel.SetActive(true);
            clickBoostTimer.ResetTimer(time, () => clickBoostLabel.SetActive(false));
            while (clickBoostTimer.GetTime() > 0)
            {
                yield return null;
                clickBoostText.text = drillFormatter.TimeFormatter(clickBoostTimer.GetTime());
            }
        }

        private IEnumerator MagnetBoostUsed(float time)
        {
            magnetBoostLabel.SetActive(true);
            magnetBoostTimer.ResetTimer(time, () => magnetBoostLabel.SetActive(false));
            while (magnetBoostTimer.GetTime() > 0)
            {
                yield return null;
                magnetBoostText.text = drillFormatter.TimeFormatter(magnetBoostTimer.GetTime());
            }
        }

        private IEnumerator LadleBoostUsed(float time)
        {
            ladleBoostLabel.SetActive(true);
            ladleBoostTimer.ResetTimer(time, () => ladleBoostLabel.SetActive(false));
            while (ladleBoostTimer.GetTime() > 0)
            {
                yield return null;
                ladleBoostText.text = drillFormatter.TimeFormatter(ladleBoostTimer.GetTime());
            }
        }

        private IEnumerator WildFarmBoostUsed(float time)
        {
            wildFarmBoostLabel.SetActive(true);
            wildFarmBoostTimer.ResetTimer(time, () => wildFarmBoostLabel.SetActive(false));
            while (wildFarmBoostTimer.GetTime() > 0)
            {
                yield return null;
                wildFarmBoostText.text = drillFormatter.TimeFormatter(wildFarmBoostTimer.GetTime());
            }
        }
    }
}
