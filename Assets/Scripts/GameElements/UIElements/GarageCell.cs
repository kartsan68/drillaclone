﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class GarageCell : MonoBehaviour
    {
        [SerializeField] private Text upText;
        [SerializeField] private Color redyTextColor;
        [SerializeField] private Color lockTextColor;
        [SerializeField] private Image progressImage;
        [SerializeField] public Slider detailProgress;
        [SerializeField] public Button idleAdsUpButton;
        [SerializeField] public Button button;
        [SerializeField] public Text lvlText;
        [SerializeField] public Text nameText;
        [SerializeField] public Text priceText;
        [SerializeField] public Text adsText;
        [SerializeField] private GameObject inactiveBtnState;
        [SerializeField] private GameObject inactiveBackImage;
        [SerializeField] private GameObject needEngineBackImage;
        [SerializeField] private GameObject activeBackImage;
        [SerializeField] private GameObject adsBtnState;
        [SerializeField] private GameObject moneyBar;
        [SerializeField] private GameObject engineLvlLable;
        [SerializeField] private Text engineLvlText;

        public void SetInactiveBtnState(bool state)
        {
            engineLvlLable.SetActive(false);
            upText.gameObject.SetActive(true);
            moneyBar.SetActive(true);
            inactiveBackImage.SetActive(state);
            needEngineBackImage.SetActive(state);
            inactiveBtnState.SetActive(state);
            if (state)
            {
                upText.color = lockTextColor;
                activeBackImage.SetActive(false);
                progressImage.color = new Color(0.7f, 0.7f, 0.7f);
            }
            else
            {
                upText.color = redyTextColor;
                activeBackImage.SetActive(true);
                progressImage.color = new Color(1, 1, 1);
            }
        }

        public void SetAdsUpgradeState(bool state)
        {
            adsBtnState.SetActive(state);
            moneyBar.SetActive(!state);
        }

        public void SetNeedEngineLvlState(bool state, int needLvl)
        {
            upText.gameObject.SetActive(!state);
            activeBackImage.SetActive(!state);
            needEngineBackImage.SetActive(state);
            inactiveBtnState.SetActive(state);
            engineLvlLable.SetActive(state);
            moneyBar.SetActive(!state);
            inactiveBackImage.SetActive(!state);
            engineLvlText.text = needLvl + " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper); ;
            progressImage.color = new Color(0.7f, 0.7f, 0.7f);
        }
    }
}
