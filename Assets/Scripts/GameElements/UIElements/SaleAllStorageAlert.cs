﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class SaleAllStorageAlert : MonoBehaviour
    {
        [SerializeField] private Button saleAllStorageInStor;
        [SerializeField] private Button saleAllStorageInAlert;
        [SerializeField] private Text priceTextInStor;
        [SerializeField] private Text priceTextInAlert;
        [SerializeField] private Text priceX2TextInStor;
        [SerializeField] private Text priceX2TextInAlert;
        [SerializeField] private GameObject alert;
        [SerializeField] private GameObject cellInStorage;

        private DrillFormatter drillFormatter = new DrillFormatter();

        public bool alertIsActive;

        public void ShowHideOfflineAlert(bool show) => alert.SetActive(show);

        public void ShowHideOfflineCellInStorage(bool show)
        {
            alertIsActive = show;
            cellInStorage.SetActive(show);
        }

        public void SetPriceText(double price)
        {
            priceTextInStor.text = drillFormatter.MoneyKFormatter(price);
            priceX2TextInStor.text = drillFormatter.MoneyKFormatter(price * 2);

            priceTextInAlert.text = drillFormatter.MoneyKFormatter(price);
            priceX2TextInAlert.text = drillFormatter.MoneyKFormatter(price * 2);
        }

        public void PressStorBtn()
        {
            if (alertIsActive)
            {
                ShowHideOfflineAlert(true);
            }
        }

        public void SetButtonAction(UnityEngine.Events.UnityAction action)
        {
            saleAllStorageInStor.onClick.RemoveAllListeners();
            saleAllStorageInAlert.onClick.RemoveAllListeners();



            saleAllStorageInStor.onClick.AddListener(() =>
            {
                EventManager.CallRewardedAdvertWithActionEvent(() =>
                            {
                                action?.Invoke();
                                ShowHideOfflineCellInStorage(false);
                                ShowHideOfflineAlert(false);
                            });
            });
            saleAllStorageInAlert.onClick.AddListener(() =>
            {
                EventManager.CallRewardedAdvertWithActionEvent(() =>
                            {
                                action?.Invoke();
                                ShowHideOfflineCellInStorage(false);
                                ShowHideOfflineAlert(false);
                            });
            });

        }
    }
}
