﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class StorageUnit : MonoBehaviour
    {
        [SerializeField] private GameObject blockState;
        [SerializeField] private GameObject iconBlockState;
        [SerializeField] private GameObject separator;


        private CountdownTimer unlockTimer;
        private DrillFormatter drillFormatter;
        private double actualTime = 0;

        public Text saleResPriceBlock;
        public Text resName;
        public Text resCount;
        public Text saleResPrice;
        public Text unlockResPrice;
        public Slider resCountProgress;
        public Button unlockButton;
        public Button saleResButton;
        public Image resImage;
        public GameObject coin;
        public Text unlockLabelText;
        public bool alreadyStart = false;


        public void Unblock()
        {
            blockState.SetActive(false);
            iconBlockState.SetActive(false);
        }

        public void ShowSeparator(bool show) => separator.SetActive(show);

        public bool BlockState => blockState.activeSelf;

        public void StartUnlockTimer(Drop.Loot loot, Action endAction)
        {
            // if (unlockTimer == null)
            //     unlockTimer = Instantiate(GetComponent<CountdownTimer>(), new Vector3(0, 0, 0), Quaternion.identity);
            Debug.Log("START UNLOCK");
            if (unlockTimer == null)
            {
                unlockTimer = Managers.TimeManager.GetEmptyCountdownTimer();
                // if (unlockTimer == null) unlockTimer = Instantiate(gameObject.GetComponent<CountdownTimer>(), new Vector3(0, 0, 0), Quaternion.identity);
            }


            if (drillFormatter == null)
                drillFormatter = new DrillFormatter();

            alreadyStart = true;
            coin.SetActive(false);
            unlockTimer.SetUstopable(true);
            Debug.Log(DigIsBigLocalizator.GetLocalizationString("STOR_UNIT_STADYING", Modificator.ToUpper));
            unlockLabelText.text = DigIsBigLocalizator.GetLocalizationString("STOR_UNIT_STADYING", Modificator.ToUpper);

            unlockTimer.SetCallbackTimer(loot.unlockTime, (t) =>
            {
                actualTime = t;
                loot.unlockWorkTime = t;
                unlockResPrice.text = drillFormatter.TimeFormatter(t);
            }, () =>
            {
                endAction?.Invoke();
                Destroy(unlockTimer.gameObject);
            });

            unlockButton.onClick.RemoveAllListeners();
            // unlockButton.onClick.AddListener(() =>
            // {
            //     Managers.EventManager.CallRewardedAdvertWithActionEvent(() =>
            //     {
            //         float resistTime = actualTime - (loot.unlockTime / loot.unlockAdvCount);
            //         if (resistTime < 0) resistTime = 0;

            //         ResetUnlockTimerTime(resistTime);
            //     });
            // });
        }


        public void ResetUnlockTimerTime(double time)
        {
            if (unlockTimer != null)
                unlockTimer.SetTime(time);
        }
    }
}
