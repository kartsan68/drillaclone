﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class OurGameElement : MonoBehaviour
    {
        [SerializeField] private Text gameName;
        [SerializeField] private Image image;
        [SerializeField] private Button goToGame;

        public void SetImageAndName(Texture texture, string name, string url)
        {
            gameName.text = CutName(name);
#if UNITY_ANDROID
            goToGame.onClick.AddListener(() => Application.OpenURL("market://details?id=" + url));
#elif UNITY_IOS
            goToGame.onClick.AddListener(() => Application.OpenURL("itms-apps://itunes.apple.com/app/" + url));
#endif
            image.sprite = Sprite.Create((Texture2D)texture,
                    new Rect(0, 0, texture.width, texture.height),
                    new Vector2(0.5f, 0.5f));
        }

        private string CutName(string name)
        {
            string cutElement;
#if UNITY_IOS
            cutElement = "[IOS] "; 
#else
            cutElement = "[ANDROID ID] ";
#endif

            if (name.Contains(cutElement))
            {
                for (int i = 0; i < cutElement.Length; i++)
                {
                    name = name.Remove(0, 1);
                }
            }
            return name;
        }
    }
}
