﻿using System.Collections.Generic;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.GameElements.Elements
{
    public class StorageBrillUnit : MonoBehaviour
    {
        [SerializeField] private UniqRes uniqResLoad;
        [SerializeField] private GameObject elementInStor;
        [SerializeField] private Text usualPrice;
        [SerializeField] private Text usualPrice2;
        [SerializeField] private Text advertisePrice;
        [SerializeField] private Button usualSaleBtn;
        [SerializeField] private Button advertiseSaleBtn;

        private DrillFormatter formatter;
        private long actualSalePrice;
        private long actualAdvSalePrice;
        private bool inDaStor = false;
        private UniqRes uniqResActual;

        private void Awake()
        {
            formatter = new DrillFormatter();
            EventManager.PickUpUniq += PickUpUniq;
            EventManager.SaveGame += Save;
            EventManager.LoadGame += (ocd) => Load();
        }

        private void Save()
        {
            List<SaveData> data = new List<SaveData>();

            data.Add(new SaveData(SaveKeys.brillInStorKey, inDaStor));

            SaveCreator.Save(SaveKeys.brillSaveKey, data);
        }

        private void Load()
        {
            if (GameData.GameData.firstStart)
            {
                if (SaveCreator.GetLoadData(SaveKeys.brillSaveKey).AsArray[0][SaveKeys.brillInStorKey].AsBool)
                {
                    EventManager.PickUpUniqEvent(uniqResLoad);
                }
            }
        }

        private void Update()
        {
            if (inDaStor)
            {
                actualSalePrice = (long)(uniqResActual.usualGameTime * GameData.GameData.CPS);
                actualAdvSalePrice = (long)(uniqResActual.advGameTime * GameData.GameData.CPS);

                advertisePrice.text = formatter.MoneyKFormatter(actualAdvSalePrice);
                usualPrice.text = formatter.MoneyKFormatter(actualSalePrice);
                usualPrice2.text = formatter.MoneyKFormatter(actualSalePrice);
            }
        }

        private void PickUpUniq(UniqRes uniqRes)
        {
            ClearButtonListeners();
            uniqResActual = uniqRes;
            inDaStor = true;
            GameData.GameData.brilliantInTheStorage = inDaStor;

            actualSalePrice = (long)(uniqRes.usualGameTime * GameData.GameData.CPS);
            actualAdvSalePrice = (long)(uniqRes.advGameTime * GameData.GameData.CPS);

            advertisePrice.text = formatter.MoneyKFormatter(actualAdvSalePrice);
            usualPrice.text = formatter.MoneyKFormatter(actualSalePrice);
            usualPrice2.text = formatter.MoneyKFormatter(actualSalePrice);

            advertiseSaleBtn.onClick.AddListener(() => EventManager.CallRewardedAdvertWithActionEvent(() =>
            {
                inDaStor = false;
                GameData.GameData.brilliantInTheStorage = inDaStor;
                elementInStor.SetActive(inDaStor);
                EventManager.GiveMeMoneyEvent(actualAdvSalePrice);
            }));

            usualSaleBtn.onClick.AddListener(() =>
            {
                inDaStor = false;
                GameData.GameData.brilliantInTheStorage = inDaStor;
                elementInStor.SetActive(inDaStor);
                EventManager.GiveMeMoneyEvent(actualSalePrice);
            });

            elementInStor.SetActive(inDaStor);
        }

        private void ClearButtonListeners()
        {
            advertiseSaleBtn.onClick.RemoveAllListeners();
            usualSaleBtn.onClick.RemoveAllListeners();
            usualSaleBtn.gameObject.SetActive(false);
        }

    }
}
