﻿using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameElements.Location;
using BlackBears.DiggIsBig.Managers;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Clans;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Controllers
{
    public class AlertController : MonoBehaviour
    {
        [SerializeField] private GameObject uiElements;
        [SerializeField] private GameObject notEnoughtMoneyScreen;
        [SerializeField] private Button mineButton;
        [SerializeField] private Text notEnoughtMoneyCountText;
        [SerializeField] private GameObject[] soSmallPackStateElement;

        //Next location
        [SerializeField] private GameObject nextLocationScreen;
        [SerializeField] private Text lvlText;
        [SerializeField] private Text slowPercentText;
        [SerializeField] private Image[] previewImages;
        //

        [SerializeField] private GameObject untouchebleArea;

        private DrillFormatter drillFormatter;


        private void Awake()
        {
            EventManager.NotEnoughtMoney += (money, onSuccess, onFail) => NotEnoughtMoney(money);
            EventManager.NextLocation += NextLocation;
            EventManager.HideUIElements += HideUIElements;
            drillFormatter = new DrillFormatter();
            BBGC.Features.GetFeature<ClansFeature>().Core.OnClansShowStateChanged += (state) => untouchebleArea.SetActive(state);
        }



        private void NotEnoughtMoney(double money)
        {
            CloseWinAdvBlockEventTrigger();
            mineButton.onClick.Invoke();
            notEnoughtMoneyScreen.SetActive(true);
            notEnoughtMoneyCountText.text = drillFormatter.MoneyKFormatter(money);

            if (GameData.GameData.smallGoldPackGoldCount >= money) soSmallPackStateElement[0].gameObject.SetActive(false);
            else soSmallPackStateElement[0].gameObject.SetActive(true);

            if (GameData.GameData.mediumGoldPackGoldCount >= money) soSmallPackStateElement[1].gameObject.SetActive(false);
            else soSmallPackStateElement[1].gameObject.SetActive(true);

            if (GameData.GameData.bigGoldPackGoldCount >= money) soSmallPackStateElement[2].gameObject.SetActive(false);
            else soSmallPackStateElement[2].gameObject.SetActive(true);
        }

        private void NextLocation(Location loc)
        {
            nextLocationScreen.SetActive(true);
            previewImages[0].sprite = loc.GetBackgroundImage;
            previewImages[1].sprite = loc.GetSides[0];
            previewImages[2].sprite = loc.GetSides[1];
            slowPercentText.text = "+" + (long)(loc.EarthRigidity * 100) + "%";
            lvlText.text = (GameData.GameData.locationNumber + 1) + " " + DigIsBigLocalizator.GetLocalizationString("MAIN_LVL", Modificator.ToUpper);
        }

        public void HideUIElements(bool isHide)
        {
            if (!isHide) EventManager.HideStorageAlertEvent();
            uiElements.SetActive(isHide);
        }

        public void CloseWinAdvBlockEventTrigger() => EventManager.CloseWinAdvBlockEvent();
    }
}
