﻿using BlackBears.DiggIsBig.Managers;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.Alerts;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.DiggIsBig.Controllers
{
    public class SQBALinkController : MonoBehaviour
    {
        private string coinsKey = "coins";
        private void Awake()
        {
            BBGC.Features.SQBA().SubscribeForBonusData(OnBonusDataCome);
        }

        private void OnBonusDataCome(JSONNode node)
        {

            if (node == null) return;

            var addNode = node["add"];
            if (addNode != null && addNode.Tag == JSONNodeType.Object)
            {
                AddParameters(addNode);
                BonusAlertContent alertContent = new BonusAlertContent(0, true);
                BBGC.Features.Alerts().AddAlert(new BonusAlert(alertContent));
            }
        }

        private void AddParameters(JSONNode node)
        {
            if (node[coinsKey] != null) EventManager.GiveMeMoneyEvent(node[coinsKey].AsDouble);
        }

    }
}
