﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Cave.CaveElements;
using BlackBears.DiggIsBig.Advertise;
using UnityEngine;
using UnityEngine.UI;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Counter;
using BlackBears.DiggIsBig.GameElements.Elements;
using BlackBears.DiggIsBig.GameElements.Location;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Save;
using BlackBears.Chronometer;
using BlackBears.GameCore;
using System;
using BlackBears.GameCore.Features;

namespace BlackBears.DiggIsBig.Controllers
{
    public class GameController : MonoBehaviour
    {
        ///
        public void OpenClans()
        {
            EventManager.OpenClansEvent();
        }
        ///
        #region Var
        [SerializeField] private CaveController caveController;
        [SerializeField] private DeepController deepController;
        [SerializeField] private GameObject stoneEmitter;
        [SerializeField] private StorageController storageController;
        [SerializeField] private Spawner spawner;
        [SerializeField] private GameObject rouletteWindow;
        [SerializeField] private GameObject blockingTouchArea;
        [SerializeField] private ResTapReact resTapReact;
        [SerializeField] private ResTapTextReact resTapTextReact;
        [SerializeField] private GameObject targetStorage;

        private GameConfigurationManager gameConfigurationManager;
        private bool paused;
        private float areaRigidity;
        private float areaVisualRigidity;
        private float locationSpeedBoost;

        private float caveSpeed;
        private float boerSpeed;
        private float drillSpeed;

        private OfflineController offlineController;

        private double usualCaveSpeed;
        private double usualDrillCaveSpeed;
        private float powerControl = 1;
        private float storageControl = 1;
        private float constSpeed;
        private float constVisualSpeed;

        #endregion

        #region Save/Load

        private void Save()
        {
            List<SaveData> data = new List<SaveData>();
            data.Add(new SaveData(SaveKeys.deepDataKey, GameData.GameData.deep));
            data.Add(new SaveData(SaveKeys.soundKey, GameData.GameSettingsData.sound));
            data.Add(new SaveData(SaveKeys.musicKey, GameData.GameSettingsData.music));
            data.Add(new SaveData(SaveKeys.firstNeverUpgradeDataKey, GameData.GameData.firstNeverUpgrade));
            data.Add(new SaveData(SaveKeys.firstSessionDataKey, false));

            SaveCreator.Save(SaveKeys.gameControllerSaveKey, data);
        }


        private void Load(OfflineCalcData ocd)
        {
            Debug.Log("Game Controller");

            CreateKnownListInStorage();
            deepController.SetDeep(ocd);
            if (GameData.GameData.firstStart)
            {
                spawner.ResFieldGenerator();
                GameData.GameData.firstStart = false;
            }
            else EventManager.ReloadTickEvent(TimeModule.TimeFromLastSave);
        }

        #endregion

        #region MonoBeahviour
        private void Awake()
        {
            VarInit();
        }

        private void ApplicationReload()
        {
            offlineController.ReloadGame(TimeModule.TimeFromLastSave);
        }

        private void Start()
        {
            SubscribeToEvents();
            SetTimerSpeed();
            GamePause();
            CreateKnownListInStorage();
            if (SaveCreator.GetLoadData(SaveKeys.gameControllerSaveKey).AsArray == null)
            {
                spawner.ResFieldGenerator();
                GameData.GameData.firstStart = false;
            }

            offlineController = new OfflineController(TimeModule.TimeFromLastSave, gameConfigurationManager);
        }

        private void Update()
        {
            if (!paused) SetTimerSpeed();
        }
        #endregion

        #region SubscribeAndSet
        private void SubscribeToEvents()
        {
            EventManager.ApplicationReload += ApplicationReload;
            EventManager.SaveGame += Save;
            EventManager.LoadGame += Load;

            EventManager.NewKmDeep += CreateKnownListInStorage;
            EventManager.BeamCrash += GameUnpause;
            EventManager.BeamSpawn += (b, l) => GamePause();
            EventManager.PickUpRoulette += () =>
            {
                rouletteWindow.SetActive(true);
                EventManager.HideUIElementsEvent(false);
            };
            EventManager.FullStorage += FullStorageControl;
        }

        private void SetTimerSpeed()
        {
            if (!paused)
            {
                deepController.SetTimerSpeed((constSpeed + (drillSpeed * powerControl)) * storageControl);
                SetCaveSpeed((constVisualSpeed + (usualCaveSpeed * powerControl)) * storageControl);
            }
            else
            {
                caveController.SetCaveScrollSpeed(0);
                deepController.SetTimerSpeed(0);
            }
        }

        private bool startStorageMoveUp = false;
        private bool startStorageMoveDown = false;

        private Coroutine StorageControlMoveDownC;
        private Coroutine StorageControlMoveUpC;


        private void FullStorageControl(int perc)
        {
            if (perc >= 100)
            {
                if (!startStorageMoveDown)
                {
                    startStorageMoveUp = false;
                    startStorageMoveDown = true;

                    if (StorageControlMoveUpC != null) StopCoroutine(StorageControlMoveUpC);
                    StorageControlMoveDownC = StartCoroutine(StorageControlMoveDown());
                }
            }
            else
            {
                if (!paused)
                {
                    EventManager.DrillaStartStopEvent(false);
                    stoneEmitter.SetActive(true);
                }
                if (!startStorageMoveUp)
                {
                    startStorageMoveUp = true;
                    startStorageMoveDown = false;
                    if (StorageControlMoveDownC != null) StopCoroutine(StorageControlMoveDownC);
                    StorageControlMoveUpC = StartCoroutine(StorageControlMoveUp());
                }
            }
        }

        private IEnumerator StorageControlMoveUp()
        {
            while (storageControl != 1)
            {
                storageControl += 0.02f;
                EventManager.AnimationStorageControlEvent(storageControl);
                if (storageControl > 1) storageControl = 1;
                yield return null;
            }
        }

        private IEnumerator StorageControlMoveDown()
        {
            while (storageControl != 0)
            {
                storageControl -= 0.02f;
                EventManager.AnimationStorageControlEvent(storageControl);
                if (storageControl < 0) storageControl = 0;
                yield return null;
            }
            stoneEmitter.SetActive(false);
            EventManager.DrillaStartStopEvent(true);
        }

        private Coroutine PowerControlMoveDownC;
        private Coroutine PowerControlMoveUpC;

        private bool startPowerMoveUp = false;
        private bool startPowerMoveDown = false;

        public void OnPowerSpeed()
        {
            if (!startPowerMoveUp)
            {
                startPowerMoveUp = true;
                startPowerMoveDown = false;

                if (PowerControlMoveDownC != null) StopCoroutine(PowerControlMoveDownC);
                PowerControlMoveUpC = StartCoroutine(PowerControlMoveUp());
            }
        }

        public void OffPowerSpeed()
        {
            if (!startPowerMoveDown)
            {
                startPowerMoveUp = false;
                startPowerMoveDown = true;

                if (PowerControlMoveUpC != null) StopCoroutine(PowerControlMoveUpC);

                PowerControlMoveDownC = StartCoroutine(PowerControlMoveDown());
            }
        }

        private IEnumerator PowerControlMoveUp()
        {
            while (powerControl != 1)
            {
                powerControl += 0.01f;
                //  EventManager.AnimationSpeedControlEvent(powerControl);
                if (powerControl > 1) powerControl = 1;
                yield return null;
            }
        }

        private IEnumerator PowerControlMoveDown()
        {
            while (powerControl != 0)
            {
                powerControl -= 0.01f;
                //  EventManager.AnimationSpeedControlEvent(powerControl);
                if (powerControl < 0) powerControl = 0;
                yield return null;
            }
        }


        private void SetCaveSpeed(double speed)
        {
            if (!paused)
            {
                caveSpeed = (float)speed;
                spawner.SetSpeedForDependence(caveSpeed);
                caveController.SetCaveScrollSpeed(caveSpeed);
            }
        }

        #endregion

        #region Paused
        public void GamePause()
        {
            if (!paused)
            {
                paused = true;
                caveSpeed = 0;
                stoneEmitter.SetActive(false);
                SetTimerSpeed();
                deepController.TimerPause();
                EventManager.GamePausedEvent();
            }
        }


        public void GameUnpause()
        {
            if (paused)
            {
                stoneEmitter.SetActive(true);
                paused = false;
                SetTimerSpeed();
                deepController.TimerUnpause();
                EventManager.GameUnpausedEvent();
                if (storageControl == 0 || powerControl == 0)
                {
                    stoneEmitter.SetActive(false);
                    EventManager.DrillaStartStopEvent(true);
                }
            }
        }

        #endregion

        #region GetData
        public ResTapReact GetFakeReactUnit => resTapReact;
        public ResTapTextReact GetFakeTextReactUnit => resTapTextReact;
        public float GetCaveSpeed => caveSpeed;
        public float GetConstVisualSpeed()
        {
            if (storageControl < 1)
                return 0;
            else
                return constVisualSpeed;
        }
        public bool GetPauseState => paused;

        public GameConfigurationManager GetConfigurationManager()
        {
            if (gameConfigurationManager == null)
            {
                gameConfigurationManager = new GameConfigurationManager();
            }

            return gameConfigurationManager;
        }

        #endregion

        #region LocationCondition

        public void SetLocationConditions(float speedMult, float prodMult, float rigidity,
                                          float visualRigidity, float constSpeed, float constVisualSpeed)
        {
            locationSpeedBoost = speedMult;
            areaRigidity = rigidity;
            areaVisualRigidity = visualRigidity;
            EditDrillSpeed(boerSpeed, usualCaveSpeed);
            storageController.SetLocationMultiplier(prodMult);
            this.constSpeed = constSpeed;
            this.constVisualSpeed = constVisualSpeed;
        }

        #endregion


        #region OtherOperation

        public void OnIOSNotifications() => NotificationsManager.EnableNotification(true);

        public void BlockingTouchAreaTrigger()
        {
            if (blockingTouchArea.activeSelf)
            {
                blockingTouchArea.SetActive(false);
            }
            else
            {
                blockingTouchArea.SetActive(true);
            }
        }

        public void RecalcCPS() => storageController.CPSCalc();

        public void EditDrillSpeed(double speed, double visualSpeed, bool isDrill = false)
        {
            if (isDrill) usualDrillCaveSpeed = visualSpeed;

            usualCaveSpeed = (usualDrillCaveSpeed - usualDrillCaveSpeed * areaVisualRigidity);
            boerSpeed = (float)speed;
            GameData.GameData.boerSpeedWithEarthRigidity = (boerSpeed - boerSpeed * areaRigidity);
            drillSpeed = (boerSpeed - boerSpeed * areaRigidity) * locationSpeedBoost;
            SetTimerSpeed();
        }

        public double SpendMoney(double spend) => storageController.SpendMoney(spend);
        public bool SpendMoneyInfo(double spend) => storageController.SpendMoneyInfo(spend);
        public bool CheckFreeSpace() => storageController.CheckFreeSpace();
        public Spawner GetSpawner() => spawner;

        public void CreateKnownListInStorage()
        {
            spawner.CheckRedyLoot();

            var lootResObjects = spawner.GetResLootObjects;

            foreach (var index in lootResObjects)
            {
                if (index.GetComponent<Loot>().isAppearance)
                {
                    storageController.AddKnownRes(index.GetComponent<Loot>());
                }
            }

            storageController.CPSCalc();
        }

        private void VarInit()
        {
            TimeModule.checkEnabled = false;
            paused = false;
            areaRigidity = 0;
            areaVisualRigidity = 0;
            locationSpeedBoost = 1;
        }

        #endregion
    }
}