﻿using UnityEngine;
using System.Collections;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Common;
using BlackBears.DiggIsBig.Managers;
using UnityEngine.UI;
using BlackBears.DiggIsBig.Cave;
using BlackBears.DiggIsBig.GameData;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace BlackBears.DiggIsBig.Controllers
{
    [System.Serializable]
    public class Confines
    {
        public float yMin, yMax, xMin, xMax;
        public Confines() { }

        public Confines(float yMin, float yMax, float xMin, float xMax)
        {
            this.yMin = yMin;
            this.yMax = yMax;
            this.xMin = xMin;
            this.xMax = xMax;
        }

    }
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private GameObject smallTap;
        [SerializeField] private GameObject bigTap;

        [SerializeField] private Confines confines;
        [SerializeField] float smoothTouchMultipler;
        [SerializeField] float smoothInertiaMultiplier;
        [SerializeField] Transform targetDrilla;
        [SerializeField] private float yCamPos;
        [SerializeField] Slider smoothInertiaMultiplierSlider;
        [SerializeField] Text smoothInertiaMultiplierText;
        [SerializeField] Slider smoothTouchMultiplerSlider;
        [SerializeField] Text smoothTouchMultiplerText;
        [SerializeField] float slideSpeedStopper = 1;
        [SerializeField] Slider stopSpeedSlider;
        [SerializeField] Text stopSpeedText;


        private Vector3 velocity = Vector3.zero;
        private float camStable = 64;
        private Camera mainCam;
        private float camOffset;
        private float speed;
        private GameController gameController;
        private Vector3 move;
        private Vector3 stable;
        private Touch touch;
        private float oldConfinesY;
        private bool beamState;
        private bool bigScreen;
        private float moveCameraSpeed;
        private bool updateStatus = false;

        public void SetMainCamPosition()
        {
            if (mainCam != null)
            {
                if (!bigScreen) mainCam.transform.position = new Vector3(160, 350, -10);
                else mainCam.transform.position = new Vector3(160, 390, -10);
            }
        }

        public void OnBeamMode()
        {
            beamState = true;
            if (bigScreen)
            {
                confines.yMin = 200;
            }
            else
            {
                confines.yMin = 160;
            }
        }

        public void OffBeamMode()
        {
            beamState = false;
            confines.yMin = oldConfinesY;
        }

        public Confines GetConfines() => confines;

        private void Awake()
        {
            ///
            smoothTouchMultiplerSlider.value = smoothTouchMultipler;
            smoothInertiaMultiplierSlider.value = smoothInertiaMultiplier;
            stopSpeedSlider.value = slideSpeedStopper;
            ///
            SubscribeToEvents();
            mainCam = gameObject.GetComponent<Camera>();
            SetIsBigScreen();
        }

        private void SubscribeToEvents()
        {
            EventManager.UpgradeModeCamera += (mode) => updateStatus = mode;
            EventManager.BeamSpawn += (b, l) =>
            {
                if (!beamState)
                {
                    OnBeamMode();
                    if (!l)
                    {
                        StartCoroutine(MoveToDownCamera());
                    }
                }
            };
            EventManager.BeamCrash += () =>
            {
                OffBeamMode();
                SetMainCamPosition();
            };
            EventManager.ClickBoostUsed += (a, b) => GameData.GameData.clickBoosterIsActive = true;
            EventManager.ClickBoostStop += () => GameData.GameData.clickBoosterIsActive = false;
        }
        private void Start()
        {
            move = new Vector3();
            moveCameraSpeed = 10;
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            if (bigScreen) confines.yMin += 40;
        }

        private void Update()
        {

            ///
            smoothTouchMultipler = smoothTouchMultiplerSlider.value;
            smoothTouchMultiplerText.text = smoothTouchMultipler.ToString();

            smoothInertiaMultiplier = smoothInertiaMultiplierSlider.value;
            smoothInertiaMultiplierText.text = smoothInertiaMultiplier.ToString();

            slideSpeedStopper = stopSpeedSlider.value;
            stopSpeedText.text = slideSpeedStopper.ToString();
            ///
#if UNITY_EDITOR
            if (Input.anyKey) GameData.GameData.afkTime = 0;
            else if (!updateStatus) GameData.GameData.afkTime += Time.deltaTime;

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                if (GameData.GameData.clickBoosterIsActive) Instantiate(bigTap, pos, Quaternion.identity).SetActive(true);
                else Instantiate(smallTap, pos, Quaternion.identity).SetActive(true);
                // RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
                RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero);
                bool can = true;
                IsPointerOverUIObject.CheckSoundElement();
                foreach (RaycastHit2D hit in hits)
                {
                    if (hit.collider != null)
                    {
                        if (hit.collider.tag.Equals("UnpermeableUI")) break;
                        else
                        {
                            if (GameData.GameData.clickBoosterIsActive) hit.collider.gameObject.SendMessage("PickUp");
                            else if (!hit.collider.tag.Equals("Resource")) hit.collider.gameObject.SendMessage("PickUp");
                            else if (can)
                            {
                                hit.collider.gameObject.SendMessage("PickUp");
                                can = false;
                            }
                        }
                    }
                }
            }
#endif

            if (updateStatus)
            {
                Vector3 targetPos = targetDrilla.TransformPoint(new Vector3(0, yCamPos, 0));
                transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, 0.1f);
            }
            else
            {
                speed = gameController.GetCaveSpeed;
                //magic camera fade offset
                camOffset = ((speed / 180) + 0.01f * speed) * camStable;
                stable = new Vector3(0, camOffset * Time.deltaTime, 0);

#if UNITY_EDITOR

                move = new Vector3(0, 10 * Time.deltaTime * camStable, 0);

                if (Input.GetKey(KeyCode.UpArrow) && mainCam.transform.position.y < confines.yMax)
                    mainCam.transform.position = mainCam.transform.position + move;
                else if (Input.GetKey(KeyCode.DownArrow) && mainCam.transform.position.y > confines.yMin)
                    mainCam.transform.position = mainCam.transform.position - move;
                else if (mainCam.transform.position.y > confines.yMin && mainCam.transform.position.y < confines.yMax)
                    mainCam.transform.position = mainCam.transform.position + stable;

                CheckTopElements();
#else
                TouchMove();
#endif
            }
        }

        private bool bottomState = true;
        private void CheckTopElements()
        {
            //размер верхних UI элементов
            // if (mainCam.transform.position.y <= confines.yMin && !bottomState)
            // {
            //     bottomState = true;
            //     EventManager.TopElemensIsMaxEvent(bottomState);
            // }
            // else if (mainCam.transform.position.y > confines.yMin && bottomState)
            // {
            //     bottomState = false;
            //     EventManager.TopElemensIsMaxEvent(bottomState);
            // }
        }
        private bool CheckConfines()
        {
            if (mainCam.transform.position.y + stable.y - move.y <= confines.yMin)
            {
                mainCam.transform.position = new Vector3(mainCam.transform.position.x, confines.yMin, mainCam.transform.position.z);
                return true;
            }
            else if (mainCam.transform.position.y + stable.y - move.y >= confines.yMax)
            {
                mainCam.transform.position = new Vector3(mainCam.transform.position.x, confines.yMax, mainCam.transform.position.z);
                return true;
            }
            return false;
        }

        private IEnumerator InertiaCoroutine()
        {

            bool itsTimeToStop = false;
            yield return null;
            while ((touch.phase != TouchPhase.Began) && !itsTimeToStop)
            {

                if (Mathf.Abs(move.y) < slideSpeedStopper)
                {
                    move.y = 0;
                    itsTimeToStop = true;
                    break;
                }

                if (CheckConfines())
                {
                    itsTimeToStop = true;
                    break;
                }

                move *= smoothInertiaMultiplier;

                mainCam.transform.position = mainCam.transform.position - move;

                yield return null;
            }
            move = new Vector3(0, 0, 0);
        }

        private void TouchMove()
        {
            if (Input.touchCount > 0 /*&& !IsPointerOverUIObject.IsPointerOverUIObjectMeth()*/)
            {
                GameData.GameData.afkTime = 0;
                touch = Input.GetTouch(0);

                // if (Input.touchCount > 1)
                // {
                foreach (var t in Input.touches)
                {
                    if (t.phase == TouchPhase.Began)
                    {
                        Vector3 pos = Camera.main.ScreenToWorldPoint(t.position);
                        if (GameData.GameData.clickBoosterIsActive) Instantiate(bigTap, pos, Quaternion.identity).SetActive(true);
                        else Instantiate(smallTap, pos, Quaternion.identity).SetActive(true);
                        // RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
                        RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero);
                        bool can = true;

                        IsPointerOverUIObject.CheckSoundElement();

                        foreach (RaycastHit2D hit in hits)
                        {
                            if (hit.collider != null)
                            {
                                if (GameData.GameData.clickBoosterIsActive) hit.collider.gameObject.SendMessage("PickUp");
                                else if (!hit.collider.tag.Equals("Resource")) hit.collider.gameObject.SendMessage("PickUp");
                                else if (can)
                                {
                                    hit.collider.gameObject.SendMessage("PickUp");
                                    can = false;
                                }

                            }
                        }
                    }
                }


                switch (touch.phase)
                {
                    case TouchPhase.Moved:
                        Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                        move = new Vector3(0, touchDeltaPosition.y * smoothTouchMultipler, 0);
                        transform.Translate(0, -touchDeltaPosition.y * smoothTouchMultipler, 0);
                        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, confines.yMin, confines.yMax), transform.position.z);
                        if (CheckConfines()) break;
                        break;
                    case TouchPhase.Ended:
                        StartCoroutine(InertiaCoroutine());
                        break;
                }
            }
            else GameData.GameData.afkTime += Time.deltaTime;

            if (mainCam.transform.position.y > confines.yMin && mainCam.transform.position.y < confines.yMax)
            {
                mainCam.transform.position = mainCam.transform.position + stable;
            }

            CheckTopElements();
        }

        private void SetIsBigScreen()
        {
            if (Camera.main.aspect > 0.5)
            {
                bigScreen = false;
                mainCam.transform.position = new Vector3(160, confines.yMin, -20);
                oldConfinesY = confines.yMin;
            }
            else
            {
                bigScreen = true;
                confines.yMin += 40;
                oldConfinesY = confines.yMin;
                mainCam.transform.position = new Vector3(160, confines.yMin, -20);
            }
            GameData.GameData.isBigScreen = bigScreen;
        }

        private IEnumerator MoveToDownCamera()
        {
            bool stopCam = false;
            while (!stopCam)
            {
                yield return null;
                mainCam.transform.position -= new Vector3(0, moveCameraSpeed, 0);

                //если ratio экрана не 9:18 
                if (!bigScreen && mainCam.transform.position.y < 160) stopCam = true;
                else if (bigScreen && (mainCam.transform.position.y < 200)) stopCam = true;
            }
        }
    }
}
