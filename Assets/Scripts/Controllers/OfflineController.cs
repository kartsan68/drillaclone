﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using BlackBears.GameCore.Features.Localization;
using SimpleJSON;
using UnityEngine;

public class OfflineController
{
    private int tick;
    private int fullStorTick;
    private double freeStorageSpace;
    private bool bigChestIsSet = false;
    private int actualDeep;
    private JSONArray loadDrillData;
    private JSONArray loadGameControllerData;
    private JSONArray loadShopData;
    private JSONArray loadLocationsData;
    private JSONArray loadStorageData;
    private JSONArray loadSpawnData;
    private JSONArray beamSpawnerData;
    private GameConfigurationManager gameConfigurationManager;
    private double resourcesInStorL;
    private int offlineDeepL;
    private float bigBeamHealthL;
    private float smallBeamHealthL;
    private int smallBeamTimeL;
    private int subscribeTime;
    private int actualLocationNumber;
    private int smallBeamSpawnTick;
    private int chestSpawnTick;
    private int boostSpawnTick;
    private int boxesSpawnTick;
    private int chestDelay;
    private int kmIdForRes;
    private int bigChestSpawnDeep;
    private bool bigBeamIsSetL;
    private bool smallBeamIsSetL;
    private List<double> resCounts;
    private int ladleBoostTime;
    private int ladleChanges;
    private int ladleRefreshTime;
    private bool isReload;
    private float bigBeamGasTime;
    private float bigBeamGasTimeL;
    private float bigBeamGasTimeStart;

    public OfflineController(long tick, GameConfigurationManager gameConfigurationManager)
    {
        if (tick >= int.MaxValue)
            this.tick = int.MaxValue;
        else
            this.tick = (int)tick;

        this.gameConfigurationManager = gameConfigurationManager;

        if (InitSaveData())
        {
            isReload = false;
            CalcOfflineDeep();
            LoadGame();
        }
        else EventManager.ColdStartEvent();

        EventManager.SetNotifications += CalcTimeFromNotifications;
    }

    public void ReloadGame(long tick)
    {
        if (!GameData.AdvertiseLoad && !GameData.BuyLoad)
        {
            if (tick >= int.MaxValue)
                this.tick = int.MaxValue;
            else
                this.tick = (int)tick;

            if (InitResaveData())
            {
                isReload = true;
                CalcOfflineDeep();
                LoadGame();
            }
        }
    }

    private void LoadGame(bool isReload = false)
    {
        OfflineCalcData ocd = new OfflineCalcData(subscribeTime, offlineDeepL, bigBeamHealthL, smallBeamHealthL,
        smallBeamTimeL, resourcesInStorL, bigBeamIsSetL, smallBeamIsSetL, resCounts, actualLocationNumber, chestSpawnTick,
        boxesSpawnTick, chestDelay, kmIdForRes, bigChestIsSet, ladleBoostTime,
        ladleChanges, ladleRefreshTime, boostSpawnTick, bigBeamGasTimeL);

        if (isReload) EventManager.ReloadGameEvent(ocd);
        else EventManager.LoadGameEvent(ocd);
    }

    private bool InitSaveData()
    {
        loadDrillData = SaveCreator.GetLoadData(SaveKeys.drillSaveKey).AsArray;
        loadGameControllerData = SaveCreator.GetLoadData(SaveKeys.gameControllerSaveKey).AsArray;
        loadShopData = SaveCreator.GetLoadData(SaveKeys.shopSaveKey).AsArray;
        loadLocationsData = SaveCreator.GetLoadData(SaveKeys.locationSaveKey).AsArray;
        loadStorageData = SaveCreator.GetLoadData(SaveKeys.storageSaveKey).AsArray;
        loadSpawnData = SaveCreator.GetLoadData(SaveKeys.spawnTimeControllerSaveKey).AsArray;
        beamSpawnerData = SaveCreator.GetLoadData(SaveKeys.beamSpawnerSaveKey).AsArray;

        return ((loadDrillData != null && loadGameControllerData != null && loadShopData != null
             && loadLocationsData != null && loadStorageData != null && loadSpawnData != null
             && beamSpawnerData != null))
             ? true : false;
    }

    private bool InitResaveData()
    {
        loadDrillData = SaveCreator.GetReloadData(SaveKeys.drillSaveKey).AsArray;
        loadGameControllerData = SaveCreator.GetReloadData(SaveKeys.gameControllerSaveKey).AsArray;
        loadShopData = SaveCreator.GetReloadData(SaveKeys.shopSaveKey).AsArray;
        loadLocationsData = SaveCreator.GetReloadData(SaveKeys.locationSaveKey).AsArray;
        loadStorageData = SaveCreator.GetReloadData(SaveKeys.storageSaveKey).AsArray;
        loadSpawnData = SaveCreator.GetReloadData(SaveKeys.spawnTimeControllerSaveKey).AsArray;
        beamSpawnerData = SaveCreator.GetReloadData(SaveKeys.beamSpawnerSaveKey).AsArray;

        return ((loadDrillData != null && loadGameControllerData != null && loadShopData != null
             && loadLocationsData != null && loadStorageData != null && loadSpawnData != null
             && beamSpawnerData != null))
             ? true : false;
    }

    private void CalcBigChestSpawnDeep(float deep)
    {
        foreach (var spawnKm in gameConfigurationManager.dropData.drop[1].spawnKM)
        {
            if ((spawnKm * 1000) > deep)
            {
                bigChestSpawnDeep = spawnKm * 1000;
                break;
            }
        }
    }

    private float GetActualLadlesBoost()
    {
        JSONArray garageData = SaveCreator.GetLoadData(SaveKeys.garageSaveKey).AsArray;
        for (int i = 0; i < garageData.Count; i++)
        {
            if (garageData[i].HasKey(SaveKeys.ladleLvlDataKey))
            {
                return gameConfigurationManager.detailData.ladle[garageData[i][SaveKeys.ladleLvlDataKey] - 1].multiplier;
            }
        }
        return 0;
    }


    #region ClacOfflineDeep

    private float GetActualBoerSpeed()
    {
        for (int i = 0; i < loadDrillData.Count; i++)
        {
            if (loadDrillData[i].HasKey(SaveKeys.actualBoerSpeedKey))
            {
                return loadDrillData[i][SaveKeys.actualBoerSpeedKey];
            }
        }
        return 0;
    }



    private void GetGCData(out int longSpeedTime, out int longSpeedBoost)
    {
        longSpeedTime = 0;
        longSpeedBoost = 0;

        for (int i = 0; i < loadGameControllerData.Count; i++)
        {
            if (loadGameControllerData[i].HasKey(SaveKeys.deepDataKey))
            {
                actualDeep = loadGameControllerData[i][SaveKeys.deepDataKey];
            }
        }

        //время лонг ускорения, премиум подписки и сундука в магазине
        for (int i = 0; i < loadShopData.Count; i++)
        {
            if (loadShopData[i].HasKey(SaveKeys.subscribeDataKey))
            {
                subscribeTime = loadShopData[i][SaveKeys.subscribeDataKey];
            }
            else if (loadShopData[i].HasKey(SaveKeys.chestDelayDataKey))
            {
                chestDelay = loadShopData[i][SaveKeys.chestDelayDataKey];
            }
            else if (loadShopData[i].HasKey(SaveKeys.ladleBoostTimeDataKey)) ladleBoostTime = loadShopData[i][SaveKeys.ladleBoostTimeDataKey];
            else if (loadShopData[i].HasKey(SaveKeys.ladleChangesDataKey)) ladleChanges = loadShopData[i][SaveKeys.ladleChangesDataKey];
            else if (loadShopData[i].HasKey(SaveKeys.ladleRefreshTimeDataKey)) ladleRefreshTime = loadShopData[i][SaveKeys.ladleRefreshTimeDataKey];
        }
    }

    private void GetLocationDataSave()
    {
        for (int i = 0; i < loadLocationsData.Count; i++)
        {
            if (loadLocationsData[i].HasKey(SaveKeys.actualLocationDataKey))
            {
                actualLocationNumber = loadLocationsData[i][SaveKeys.actualLocationDataKey];
                break;
            }
        }
    }

    private void BeamCheck(out bool bigBeamIsSet, out bool smallBeamIsSet, out float beamHP)
    {
        bigBeamIsSet = false;
        smallBeamIsSet = false;
        beamHP = 0;
        for (int i = 0; i < beamSpawnerData.Count; i++)
        {
            if (beamSpawnerData[i].HasKey(SaveKeys.bigBeamStateKey))
            {
                bigBeamIsSet = beamSpawnerData[i][SaveKeys.bigBeamStateKey];
            }
            else if (beamSpawnerData[i].HasKey(SaveKeys.smallBeamStateKey))
            {
                smallBeamIsSet = beamSpawnerData[i][SaveKeys.smallBeamStateKey];
            }
            else if (beamSpawnerData[i].HasKey(SaveKeys.beamHealthKey))
            {
                beamHP = beamSpawnerData[i][SaveKeys.beamHealthKey];
            }
            else if (beamSpawnerData[i].HasKey(SaveKeys.gasTimeKey))
            {
                bigBeamGasTime = beamSpawnerData[i][SaveKeys.gasTimeKey];
            }
        }
    }

    private void GetSpawnData()
    {
        for (int i = 0; i < loadSpawnData.Count; i++)
        {
            if (loadSpawnData[i].HasKey(SaveKeys.smallBeamSpawnTimeKey))
            {
                smallBeamSpawnTick = loadSpawnData[i][SaveKeys.smallBeamSpawnTimeKey];
            }
            else if (loadSpawnData[i].HasKey(SaveKeys.chestsSpawnTimeKey))
            {
                chestSpawnTick = loadSpawnData[i][SaveKeys.chestsSpawnTimeKey];
            }
            else if (loadSpawnData[i].HasKey(SaveKeys.boxesSpawnTimeKey))
            {
                boxesSpawnTick = loadSpawnData[i][SaveKeys.boxesSpawnTimeKey];
            }
            else if (loadSpawnData[i].HasKey(SaveKeys.boostSpawnTimeKey))
            {
                boostSpawnTick = loadSpawnData[i][SaveKeys.boostSpawnTimeKey];
            }

        }
    }

    private double nfFullStorTime = 0, nfNewResTime = 0, nfKingOfBeamTime = 0;
    private bool nfFullStorTimeIsCalc = false, nfNewResTimeIsCalc = false, nfKingOfBeamTimeIsCalc = false;
    private void CalcOfflineDeep(bool notificationCalc = false)
    {
        if (notificationCalc)
        {
            nfFullStorTime = 0;
            nfNewResTime = 0;
            nfKingOfBeamTime = 0;

            nfFullStorTimeIsCalc = false;
            nfNewResTimeIsCalc = false;
            nfKingOfBeamTimeIsCalc = false;
        }

        //получить скорость установленного бура
        float actualBoerSpeed = GetActualBoerSpeed();
        float actualLadleMultiplier = GetActualLadlesBoost();

        //получить время действия и множитель действующих ускорений, глубину на которой находится игрок
        int longSpeedTime = 0;
        int longSpeedBoost = 0;
        GetGCData(out longSpeedTime, out longSpeedBoost);

        //действующее замедление локации
        float actualEarthRigidity = 0;
        actualLocationNumber = 0;
        GetLocationDataSave();
        actualEarthRigidity = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber].rigidity;

        //стоит ли балка
        bool bigBeamIsSet, smallBeamIsSet;
        float beamHP;
        BeamCheck(out bigBeamIsSet, out smallBeamIsSet, out beamHP);

        //получить глубину появления и время появление балок
        int bigBeamSpawnDeep;
        if (actualLocationNumber == (gameConfigurationManager.locationData.allLocationArray.Length - 1))
        {
            //TODO: заглушка
            bigBeamSpawnDeep = int.MaxValue;
        }
        else
        {
            bigBeamSpawnDeep = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber + 1].startKm * 1000;
        }
        int smallBeamSpawnTime = gameConfigurationManager.spawnData.beamSpawnTime;

        //получить время спавна мелкой балки, бустов, сундуков и рулетки
        GetSpawnData();

        //получить здоровье большой и маленькой балки
        float bigBeamHealth, smalBeamHealth, bigBeamStartHealth;
        if (actualLocationNumber == (gameConfigurationManager.locationData.allLocationArray.Length - 1))
        {
            //TODO: заглушка
            bigBeamHealth = int.MaxValue;
            bigBeamStartHealth = int.MaxValue;
            bigBeamGasTimeStart = int.MaxValue;
            if (beamHP != 0 && bigBeamIsSet)
            {
                bigBeamHealth = beamHP;
            }
            smalBeamHealth = int.MaxValue;
            if (beamHP != 0 && smallBeamIsSet)
            {
                smalBeamHealth = beamHP;
            }
        }
        else
        {
            bigBeamHealth = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber + 1].kingBeamHealth;
            bigBeamStartHealth = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber + 1].kingBeamHealth;
            bigBeamGasTimeStart = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber + 1].bigBeamFuckupTime;
            if (beamHP != 0 && bigBeamIsSet)
            {
                bigBeamHealth = beamHP;
            }
            smalBeamHealth = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber].smallBeamHealth;
            if (beamHP != 0 && smallBeamIsSet)
            {
                smalBeamHealth = beamHP;
            }
        }

        //получить количество ресурсов на время выхода, вместимость склада и добыча ресурсов
        //получить времена действия и множители бустов добычи 
        double exitResCount = 0;
        double storageStock = 0;
        List<double> resTicksRate = new List<double>();
        resCounts = new List<double>();

        for (int j = 0; j < loadStorageData.Count; j++)
        {
            if (loadStorageData[j].HasKey(SaveKeys.resourceKey))
            {
                var loadCountData = loadStorageData[j][SaveKeys.resourceKey][SaveKeys.resourceKey];
                for (int i = 0; i < loadCountData.Count; i++)
                {
                    exitResCount += (loadCountData[i][0][SaveKeys.countKey].AsLong);
                    //resTicksRate.Add(loadCountData[i][0][SaveKeys.farmCountKey].AsInt);
                }
            }
            else if (loadStorageData[j].HasKey(SaveKeys.storageStockKey))
            {
                storageStock = loadStorageData[j][SaveKeys.storageStockKey].AsLong;
            }
            else if (loadStorageData[j].HasKey(SaveKeys.setBigChestKey))
            {
                bigChestIsSet = loadStorageData[j][SaveKeys.setBigChestKey];
            }
        }

        //получить свободное место на складе
        freeStorageSpace = storageStock - exitResCount;

        //сколько добыл за оффлайн
        //ресурсы

        double pseudoStorage = 0;
        double resAdd;

        //заполнить массив 
        // for (int i = 0; i < resTicksRate.Count; i++) resCounts.Add(0);

        //рассчет расстояния, на которое продвинется бурило за оффлайн
        int pseudoTick = tick, pseudoLongTick = longSpeedTime,
        pseudoSmallBeamTick = smallBeamSpawnTick;
        int longMultiplier = 1;
        float pseudoBigBeamHealth = bigBeamHealth, pseudoSmallBeamHealth = smalBeamHealth;
        float step, pseudoDeep = actualDeep, pseudoGasTime = bigBeamGasTime;
        List<double> resOpenKM = new List<double>();

        resTicksRate = new List<double>();
        for (int i = 0; i < gameConfigurationManager.resourcesData.resources.Length; i++)
        {
            if (gameConfigurationManager.resourcesData.resources[i].openDeep * 1000 < pseudoDeep)
            {
                resTicksRate.Add(gameConfigurationManager.resourcesData.resources[i].startCount);
            }
            else
            {
                resOpenKM.Add(gameConfigurationManager.resourcesData.resources[i].openDeep);
            }
        }
        resCounts.Add(0);
        for (int i = 0; i < resTicksRate.Count; i++) resCounts.Add(0);

        CalcBigChestSpawnDeep(pseudoDeep);

        while ((pseudoTick != 0 && (pseudoStorage != freeStorageSpace || bigBeamIsSet || pseudoSmallBeamTick <= 0 || isReload)))
        {
            //проверка новых ресурсов и изменения добычи старых
            if (!nfNewResTimeIsCalc && notificationCalc)
            {
                nfNewResTime++;
                for (int h = 0; h < resOpenKM.Count; h++)
                {
                    if ((resOpenKM[h] * 1000) <= pseudoDeep)
                    {
                        nfNewResTimeIsCalc = true;
                        resOpenKM.RemoveAt(h);
                        break;
                    }
                }
            }

            if (pseudoDeep > 1000)
            {


                resTicksRate = new List<double>();
                for (int i = gameConfigurationManager.resourcesDeepChangeData.resourcesData.Length; i > 0; i--)
                {
                    if (gameConfigurationManager.resourcesDeepChangeData.resourcesData[i - 1].kilometer * 1000 <= pseudoDeep)
                    {
                        for (int k = 0; k < gameConfigurationManager.resourcesDeepChangeData.resourcesData[i - 1].resourcesCountArr.Length; k++)
                        {
                            kmIdForRes = i - 1;
                            resTicksRate.Add(gameConfigurationManager.resourcesDeepChangeData.resourcesData[i - 1].resourcesCountArr[k]);
                        }
                        break;
                    }
                }
            }

            while (resTicksRate.Count > resCounts.Count) resCounts.Add(0);

            if (pseudoStorage != freeStorageSpace)
            {
                if (notificationCalc) nfFullStorTime++;
                for (int i = 0; i < resTicksRate.Count; i++)
                {
                    resAdd = resTicksRate[i] * longMultiplier * actualLadleMultiplier;

                    pseudoStorage += resAdd;
                    if (pseudoStorage >= freeStorageSpace)
                    {
                        resAdd -= pseudoStorage - freeStorageSpace;
                        pseudoStorage = freeStorageSpace;
                        resCounts[i] += resAdd;
                        if (notificationCalc)
                        {
                            nfFullStorTimeIsCalc = true;
                            return;
                        }
                        break;
                    }
                    resCounts[i] += resAdd;
                }
            }
            pseudoTick--;
            pseudoLongTick--;

            //действие буста на тике
            longMultiplier = 1;

            if (pseudoLongTick > 0)
            {
                longMultiplier = longSpeedBoost;
            }

            step = (((actualBoerSpeed - actualBoerSpeed * actualEarthRigidity) / 60) * longMultiplier);

            //если попал на  большую балку
            if ((step + pseudoDeep > bigBeamSpawnDeep || bigBeamIsSet))
            {
                if (!nfKingOfBeamTimeIsCalc && notificationCalc) nfKingOfBeamTimeIsCalc = true;

                bigBeamIsSet = true;
                pseudoDeep = bigBeamSpawnDeep;
                pseudoGasTime--;
                if (pseudoGasTime <= 0)
                {
                    // bigBeamGasTimeStart
                    pseudoGasTime = bigBeamGasTimeStart;
                    //bigBeamStartHealth
                    pseudoBigBeamHealth = bigBeamStartHealth;
                }
                else
                {
                    pseudoBigBeamHealth -= (int)(actualBoerSpeed - actualBoerSpeed * actualEarthRigidity);
                    if (pseudoBigBeamHealth <= 0)
                    {
                        actualLocationNumber++;
                        if (actualLocationNumber == (gameConfigurationManager.locationData.allLocationArray.Length - 1))
                        {
                            actualLocationNumber = gameConfigurationManager.locationData.allLocationArray.Length - 1;
                            //TODO: заглушка
                            bigBeamSpawnDeep = int.MaxValue;
                            bigBeamHealth = int.MaxValue;
                        }
                        else
                        {
                            bigBeamSpawnDeep = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber + 1].startKm * 1000;
                            bigBeamHealth = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber + 1].kingBeamHealth;
                        }
                        smalBeamHealth = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber].smallBeamHealth;
                        actualEarthRigidity = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber].rigidity;
                        bigBeamIsSet = false;
                        pseudoBigBeamHealth = bigBeamHealth;
                    }
                }
            }
            //если попал на  маленькую балку
            else if (pseudoSmallBeamTick <= 0)
            {
                smallBeamIsSet = true;
                pseudoSmallBeamHealth--;
                if (pseudoSmallBeamHealth <= 0)
                {
                    smallBeamIsSet = false;
                    pseudoSmallBeamHealth = gameConfigurationManager.locationData.allLocationArray[actualLocationNumber].smallBeamHealth; ;
                    pseudoSmallBeamTick = smallBeamSpawnTime;
                }
            }
            else
            {
                if (notificationCalc) nfKingOfBeamTime++;
                pseudoSmallBeamTick--;

                if (bigChestSpawnDeep <= pseudoDeep && !bigChestIsSet)
                {
                    bigChestIsSet = true;
                    CalcBigChestSpawnDeep(pseudoDeep);
                }

                if (gameConfigurationManager.spawnData.chestsKmSpawnStart * 1000 <= pseudoDeep)
                {
                    if (chestSpawnTick > 0) chestSpawnTick--;
                    else chestSpawnTick = gameConfigurationManager.spawnData.chestsSpawnTime;
                }
                else chestSpawnTick = -1;

                if (gameConfigurationManager.spawnData.boosterSpawnStart * 1000 <= pseudoDeep)
                {
                    if (boostSpawnTick > 0) boostSpawnTick--;
                    else boostSpawnTick = gameConfigurationManager.spawnData.boosterSpawnTime;
                }
                else boostSpawnTick = -1;

                if (gameConfigurationManager.spawnData.boxesKmSpawnStart * 1000 <= pseudoDeep)
                {
                    if (boxesSpawnTick > 0) boxesSpawnTick--;
                    else boxesSpawnTick = gameConfigurationManager.spawnData.boxesSpawnTime;
                }
                else boxesSpawnTick = -1;

                pseudoDeep += step;
            }
            pseudoLongTick--;
        }
        //resCounts - лист с добытыми ресурсами в оффлайне
        //pseudoLongTick - время оставшееся для лонг буста
        //pseudoTick - тик на котором склад переполнился
        //pseudoStorage - сумма добытых ресурсов
        fullStorTick = pseudoTick;
        resourcesInStorL = (freeStorageSpace == pseudoStorage) ? (freeStorageSpace + exitResCount) : (pseudoStorage + exitResCount);


        //pseudoDeep - глубина до которой дошли за оффлайн
        //pseudoLongTick - время оставшееся для лонг буста
        //bigBeamIsSet - стоит ли большая балка
        //smallBeamIsSet - стоит ли маленькая балка
        //pseudoSmallBeamHealth - количество остаточного хп у маленькой балки
        //pseudoBigBeamHealth - количество остаточного хп у большой балки
        //pseudoSmallBeamTick - сколько времени осталось до спавна маленькой балки


        offlineDeepL = (int)pseudoDeep;
        bigBeamHealthL = pseudoBigBeamHealth;
        smallBeamHealthL = pseudoSmallBeamHealth;
        smallBeamTimeL = (pseudoSmallBeamTick > 0) ? pseudoSmallBeamTick : 0;
        bigBeamIsSetL = bigBeamIsSet;
        smallBeamIsSetL = smallBeamIsSet;
        subscribeTime = (subscribeTime - tick > 0) ? subscribeTime - (int)tick : 0;
        chestDelay = (chestDelay - tick > 0) ? chestDelay - (int)tick : 0;

        ladleBoostTime = (ladleBoostTime - tick > 0) ? ladleBoostTime - (int)tick : 0;

        ladleRefreshTime = (ladleRefreshTime - tick > 0) ? ladleRefreshTime - (int)tick : 0;
        bigBeamGasTimeL = pseudoGasTime;
    }
    #endregion

    private void CalcTimeFromNotifications()
    {
        if (!GameData.AdvertiseLoad && !GameData.BuyLoad)
        {
            NotificationsManager.ClearAllNotif();
            tick = 600000;
            InitResaveData();
            CalcOfflineDeep(true);

            if (nfFullStorTime > 0)
            {
                Debug.Log("NOTIFICATION. FULL STORAGE - PUSH");
                NotificationsManager.SetNotification(DigIsBigLocalizator.GetLocalizationString("NOTIF_FULL_TITLE", Modificator.ToUpperFirst), DigIsBigLocalizator.GetLocalizationString("NOTIF_FULL_BODY", Modificator.ToUpperFirst), nfFullStorTime);
            }

            if (nfFullStorTimeIsCalc && nfNewResTimeIsCalc && nfNewResTime > 1)
            {
                Debug.Log("NOTIFICATION. NEW RESOURCE - PUSH");
                NotificationsManager.SetNotification(DigIsBigLocalizator.GetLocalizationString("NOTIF_RES_TITLE", Modificator.ToUpperFirst), DigIsBigLocalizator.GetLocalizationString("NOTIF_RES_BODY", Modificator.ToUpperFirst), nfNewResTime);
            }

            if (nfFullStorTimeIsCalc && nfKingOfBeamTimeIsCalc && nfKingOfBeamTime > 0)
            {
                Debug.Log("NOTIFICATION. BEAM - PUSH");
                NotificationsManager.SetNotification(DigIsBigLocalizator.GetLocalizationString("NOTIF_BEAM_TITLE", Modificator.ToUpperFirst), DigIsBigLocalizator.GetLocalizationString("NOTIF_BEAM_BODY", Modificator.ToUpperFirst), nfKingOfBeamTime);
            }
        }
    }
}
