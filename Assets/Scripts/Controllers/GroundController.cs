﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Cave.CaveElements;
using BlackBears.DiggIsBig.Managers;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.DiggIsBig.Controllers
{
    public class GroundController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem[] ladleParticles;
        [SerializeField] private ParticleSystem[] smallAndBigTouches;
        [SerializeField] private GameObject colorControlUnit;
        [SerializeField] private SpriteRenderer[] beamGrounds;
        [SerializeField] private Sprite[] beamGroundSprites;
        [SerializeField] private GameObject ground;
        [SerializeField] private GameObject ground2;
        [SerializeField] private Texture[] groundTextures;
        [SerializeField] private Mesh[] groundMeshes;
        [SerializeField] private TextAsset[] curveInfo;
        [SerializeField] private Sprite[] firstSideSprites;
        [SerializeField] private Sprite[] secondSideSprites;
        [SerializeField] private BezierCurve leftCurve;
        [SerializeField] private BezierCurve rightCurve;
        [SerializeField] private BezierPoint point;
        [SerializeField] private CaveSideController sideLeft;
        [SerializeField] private CaveSideController sideRight;
        [SerializeField] private TextAsset stonePalette;

        private List<Vector3> leftCurvePoints;
        private List<Vector3> rightCurvePoints;
        private CaveGroundController caveGroundController;
        private CaveGroundController caveGroundController2;
        private List<Color> stonesColorPalette;

        private void Awake()
        {
            caveGroundController = ground.GetComponent<CaveGroundController>();
            caveGroundController2 = ground2.GetComponent<CaveGroundController>();
            ParsePalette();
            SubscribeToEvents();
        }

        private void ParsePalette()
        {
            stonesColorPalette = new List<Color>();

            JSONNode node = JSONNode.Parse(stonePalette.text);
            JSONArray colorsArray = node["Colors"].AsArray;
            for (int i = 0; i < colorsArray.Count; i++)
            {
                JSONArray colorRGB = colorsArray[i][(i + 1).ToString()].AsArray;
                stonesColorPalette.Add(new Color(colorRGB[0], colorRGB[1], colorRGB[2]));
            }
        }

        private void SubscribeToEvents()
        {
            EventManager.BoerSwap += SetCurves;
            EventManager.LocationSwap += SwapCave;
        }
        private void SwapCave(int locationNumber)
        {
            int index = locationNumber;
            if (index >= groundTextures.Length) index = groundTextures.Length - 1;

            caveGroundController.SwapGroundTexture(groundTextures[index]);
            caveGroundController2.SwapGroundTexture(groundTextures[index]);
            SwapBeamGrounds(index);

            sideLeft.SwapSidesSprite(new Sprite[] { firstSideSprites[index], secondSideSprites[index] });
            sideRight.SwapSidesSprite(new Sprite[] { firstSideSprites[index], secondSideSprites[index] });

            colorControlUnit.GetComponent<SpriteRenderer>().color = stonesColorPalette[index];
            //color
            var partMain = ladleParticles[0].main;
            partMain.startColor = stonesColorPalette[index];
            partMain = ladleParticles[1].main;
            partMain.startColor = stonesColorPalette[index];
            partMain = smallAndBigTouches[0].main;
            partMain.startColor = stonesColorPalette[index];
            partMain = smallAndBigTouches[1].main;
            partMain.startColor = stonesColorPalette[index];
        }

        private void SwapBeamGrounds(int index)
        {
            for (int i = 0; i < beamGrounds.Length; i++) beamGrounds[i].sprite = beamGroundSprites[index];
        }

        private void SetCurves(int boerLvl)
        {
            int index = boerLvl - 1;

            if (index >= groundMeshes.Length) index = (groundMeshes.Length - 1);

            SwapMesh(index);
            GetBezierPoints(index);

            ClearOldPoints();
            AddPointsToBezierCurves();
        }

        private void AddPointsToBezierCurves()
        {
            //создать траекторию налево
            //создание новой кривой
            for (int i = 0; i < leftCurvePoints.Count; i++)
            {
                BezierPoint newPoint = Instantiate(point, leftCurvePoints[i], Quaternion.identity, leftCurve.transform);
                leftCurve.AddPoint(newPoint);
            }
            //создать траекторию направо
            //создание новой кривой
            for (int i = 0; i < rightCurvePoints.Count; i++)
            {
                BezierPoint newPoint = Instantiate(point, rightCurvePoints[i], Quaternion.identity, rightCurve.transform);
                rightCurve.AddPoint(newPoint);
            }
        }

        //удалить точки старой кривой
        private void ClearOldPoints()
        {
            ClearPoints(rightCurve, rightCurve.GetComponentsInChildren<BezierPoint>());
            ClearPoints(leftCurve, leftCurve.GetComponentsInChildren<BezierPoint>());
        }
        private void ClearPoints(BezierCurve curve, BezierPoint[] oldBezierPoints)
        {
            for (int i = 0; i < oldBezierPoints.Length; i++)
            {
                curve.RemovePoint(oldBezierPoints[i]);
                Destroy(oldBezierPoints[i].gameObject);
            }
        }

        // Замена маски земли
        private void SwapMesh(int index)
        {
            Mesh mesh = new Mesh();

            if (groundMeshes.Length <= index) mesh = groundMeshes[groundMeshes.Length - 1];
            else mesh = groundMeshes[index];

            ground.GetComponent<MeshFilter>().mesh = mesh;
        }

        // Создание тояек для следования частиц
        private void GetBezierPoints(int index)
        {
            Vector3 leftStartPoint = new Vector3();
            Vector3 rightStartPoint = new Vector3();

            JSONNode node = JSONNode.Parse(curveInfo[index].text);

            var leftPointNode = node["LeftStartPoint"].AsArray;
            var rightPointNode = node["RightStartPoint"].AsArray;

            leftStartPoint = new Vector3(leftPointNode[0], leftPointNode[1], leftPointNode[2]);
            rightStartPoint = new Vector3(rightPointNode[0], rightPointNode[1], rightPointNode[2]);

            //получить оффсет на который нужно поднять точки
            float yPointsOffset = ground.transform.position.y;

            //получить точки левой кривой
            bool check = false;
            leftCurvePoints = new List<Vector3>();
            rightCurvePoints = new List<Vector3>();

            for (int i = node.Count - 4; i >= 0; i--)
            {
                var array = node[i.ToString()].AsArray;

                if (!check)
                {
                    if (leftStartPoint.Equals(new Vector3(array[0], array[1], array[2])))
                    {
                        leftCurvePoints.Add(new Vector3(array[0], array[1] + yPointsOffset, array[2]));
                        check = true;
                    }
                }
                else
                {
                    leftCurvePoints.Add(new Vector3(array[0], array[1] + yPointsOffset, array[2]));
                }
            }
            //получить точки правой кривой
            check = false;

            for (int i = 0; i < (node.Count - 3); i++)
            {
                var array = node[i.ToString()].AsArray;

                if (!check)
                {
                    if (rightStartPoint.Equals(new Vector3(array[0], array[1], array[2])))
                    {
                        rightCurvePoints.Add(new Vector3(array[0], array[1] + yPointsOffset, array[2]));
                        check = true;
                    }
                }
                else
                {
                    rightCurvePoints.Add(new Vector3(array[0], array[1] + yPointsOffset, array[2]));
                }
            }
        }
    }
}