﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

public class EventController : MonoBehaviour
{
    public void EndAdvWatching() => EventManager.AdsWatchingEndEvent();
}
