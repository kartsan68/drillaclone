﻿using System;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.Save;
using UnityEngine;
using BlackBears.GameCore.Features.Ads;
using BlackBears.GameCore;
using System.Collections;

namespace BlackBears.DiggIsBig.Controllers
{
    public class SpawnTimeController : MonoBehaviour
    {
        [SerializeField] private GameController gameController;

        private GameConfigurationManager gameConfigurationManager;
        private CountdownTimer beamTimer;
        private CountdownTimer bigBeamTimer;
        private CountdownTimer chestTimer;
        private CountdownTimer boostCounter;
        private CountdownTimer boxTimer;
        private CountdownTimer boosterSpawnTimer;
        private CountdownTimer spawnDelayTimer;
        private CountdownTimer uniqResDelayTimer;
        private bool boxTimerIsWork;
        private bool boostTimerIsWork;
        private bool chestTimerIsWork;
        private int beamTime;
        private int boxTime;
        private int chestTime;
        private int boosterTime;
        private bool spawnDelayIsEnd = false;
        bool withoutUniqRes = false;
        private AdsFeature adsFeature;
        #region Save/Load

        private void Save()
        {
            List<SaveData> data = new List<SaveData>();

            data.Add(new SaveData(SaveKeys.smallBeamSpawnTimeKey, beamTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.boxesSpawnTimeKey, boxTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.chestsSpawnTimeKey, chestTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.boostSpawnTimeKey, boosterSpawnTimer.GetTime()));

            SaveCreator.Save(SaveKeys.spawnTimeControllerSaveKey, data);
        }

        private void Load(OfflineCalcData ocd)
        {
            Debug.Log("SpawnTime");

            if (ocd.smallBeamIsSet)
            {
                beamTimer.ResetTimer(ocd.smallBeamSpawnTime, () => EventManager.BeamSpawnEvent(false, false));
                EventManager.BeamSpawnEvent(false, false);
            }
            else
            {
                beamTimer.ResetTimer(ocd.smallBeamSpawnTime, () => EventManager.BeamSpawnEvent(false, false));
            }

            if (GameData.GameData.firstStart)
            {
                if (ocd.chestSpawnTick >= 0)
                {
                    chestTimer.ResetTimer(ocd.chestSpawnTick, () =>
                            {
                                EventManager.ChestSpawnEvent();
                                StartChestTimer();
                            });
                }

                if (ocd.boxesSpawnTick >= 0)
                {
                    boxTimer.ResetTimer(ocd.boxesSpawnTick, () =>
                            {
                                EventManager.BoxSpawnEvent();
                                StartBoxTimer();
                            });
                }

                if (ocd.boosterSpawnTick >= 0)
                {
                    boosterSpawnTimer.ResetTimer(ocd.boosterSpawnTick, () =>
                            {
                                EventManager.BoosterSpawnEvent();
                            });
                }
            }
        }

        #endregion
        private void Start()
        {
            SubscribeToEvents();
            SetConfiguratiob();
            InstantiateAllTimers();
            InitVar();
            StartBeamTimer();
            SpawnCheck();
            StartSpawnDelayTimer();
            StartUniqDelayTimer();
            StartCoroutine(CheckUniqResSpawn());
        }

        private void InitVar()
        {
            adsFeature = BBGC.Features.GetFeature<AdsFeature>();
            boxTimerIsWork = false;
            chestTimerIsWork = false;
            boostTimerIsWork = false;
        }

        private void SetConfiguratiob()
        {
            gameConfigurationManager = gameController.GetConfigurationManager();

            var spawnConfigDataSet = gameConfigurationManager.spawnData;

            beamTime = spawnConfigDataSet.beamSpawnTime;
            boxTime = spawnConfigDataSet.boxesSpawnTime;
            chestTime = spawnConfigDataSet.chestsSpawnTime;
            boosterTime = spawnConfigDataSet.boosterSpawnTime;

            var conditionsData = gameConfigurationManager.conditionsData;
            onlineTimeConditions = conditionsData.conditions[0].onlineTime;
            spawnDelayTimeConditions = conditionsData.conditions[0].dontSpawnBrillTime;
            withoutBrillTimeConditions = conditionsData.conditions[0].dontTouchBrillTime;
            withoutAdsTimeConditions = conditionsData.conditions[0].withoutAdsTime;

        }

        private void SubscribeToEvents()
        {
            EventManager.SaveGame += Save;
            EventManager.LoadGame += Load;
            EventManager.NewKmDeep += SpawnCheck;

            EventManager.BeamCrash += StartBeamTimer;
            EventManager.BeamSpawn += (isBig, l) =>
             {
                 if (isBig) beamTimer.StopTimer();
             };

            EventManager.PickUpUniq += (a) => StartUniqDelayTimer();
            EventManager.BoosterSpawn += StartBoosterTimer;
        }

        private void SetBoostMultiplier(Booster booster)
        {
            //Большая балка
            bigBeamTimer.BoostMultiplier(booster.multiplier);
            beamTimer.BoostMultiplier(booster.multiplier);
            chestTimer.BoostMultiplier(2);
            boostCounter.SetTimer(booster.workTime, ResetTimeMultiplier);
        }

        private void ResetTimeMultiplier()
        {
            //Большая балка
            bigBeamTimer.BoostMultiplierReset();

            beamTimer.BoostMultiplierReset();
            chestTimer.BoostMultiplierReset();
        }


        private void InstantiateAllTimers()
        {
            //Большая балка
            bigBeamTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);

            boostCounter = TimeManager.GetEmptyCountdownTimer(this.gameObject);

            boosterSpawnTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            beamTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject, TimerType.SmallBeamSpawnTimer);
            chestTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject, TimerType.ChestSpawnTimer);
            boxTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            spawnDelayTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            uniqResDelayTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
        }

        private void StartBoosterTimer()
        {
            boostTimerIsWork = true;
            boosterSpawnTimer.ResetTimer(boosterTime, () => EventManager.BoosterSpawnEvent());
        }

        private void StartBeamTimer()
        {
            beamTimer.ResetTimer(beamTime, () => EventManager.BeamSpawnEvent(false, false));
        }

        private void StartChestTimer()
        {
            chestTimerIsWork = true;

            chestTimer.SetTimer(chestTime, () =>
          {
              EventManager.ChestSpawnEvent();
              StartChestTimer();
          });
        }

        private void StartBoxTimer()
        {
            boxTimerIsWork = true;

            boxTimer.SetTimer(boxTime, () =>
          {
              EventManager.BoxSpawnEvent();
              StartBoxTimer();
          });
        }

        private void SpawnCheck()
        {
            var deepKM = GameData.GameData.deep / 1000;

            if (gameConfigurationManager.spawnData.chestsKmSpawnStart <= deepKM && !chestTimerIsWork)
                StartChestTimer();

            if (gameConfigurationManager.spawnData.boxesKmSpawnStart <= deepKM && !boxTimerIsWork)
                StartBoxTimer();

            if (gameConfigurationManager.spawnData.boosterSpawnStart <= deepKM && !boostTimerIsWork)
                StartBoosterTimer();
        }

        private void StartSpawnDelayTimer()
        {
            spawnDelayIsEnd = false;
            spawnDelayTimer.ResetTimer(spawnDelayTimeConditions, () => spawnDelayIsEnd = true);
        }

        private void StartUniqDelayTimer()
        {
            withoutUniqRes = false;
            uniqResDelayTimer.ResetTimer(withoutBrillTimeConditions, () => withoutUniqRes = true);
        }

        private float onlineTimeConditions;
        private float spawnDelayTimeConditions;
        private float withoutBrillTimeConditions;
        private float withoutAdsTimeConditions;

        private IEnumerator CheckUniqResSpawn()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.5f);

                if (Chronometer.TimeModule.OnlineTime > onlineTimeConditions && !GameData.GameData.brilliantInTheStorage && spawnDelayIsEnd
                && GameData.GameData.withoutAdv > withoutAdsTimeConditions && adsFeature.IsInterstitialAdsAvailable && adsFeature.IsRewardedAdsAvailable
                && withoutUniqRes)
                {
                    StartSpawnDelayTimer();
                    EventManager.SpawnUniqResEvent();
                }
            }
        }
    }
}
