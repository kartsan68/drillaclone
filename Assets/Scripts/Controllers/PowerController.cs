﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Configuration.Data;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Controllers
{
    public class PowerController : MonoBehaviour
    {
        [SerializeField] private GameObject powerElement;
        [SerializeField] private Slider powerTimeSlider;
        [SerializeField] private GameController gameController;
        [SerializeField] private float chargePerTapMultiplier;
        [SerializeField] private Animator tremorAlertAnimator;


        private GameConfigurationManager gameConfigurationManager;
        private float maxPowerTime;
        private float workPowerTime;
        private float powerTime;
        private bool beamState;
        private bool upgradeState;
        private float chargePerTap;
        private float powerCutMultiplier = 2;
        private float chargePerTapBoost;
        private float chargeValue;

        public void PowerCharge()
        {
            // Vibrate();
            EventManager.PowerChargeEvent();
            tremorAlertAnimator.Play("ChargeBatteryWhenStorageIsFull", -1, 0f);
            powerCutMultiplier = ((200 - powerTimeSlider.value) / 100);
            powerTime += chargePerTap * chargePerTapBoost;
            chargePerTapBoost *= chargePerTapMultiplier;


            powerTimeSlider.value = ((100 * (powerTime - Time.deltaTime) * powerCutMultiplier) / workPowerTime);

            if (powerTime > workPowerTime)
            {
                powerTime = workPowerTime;
                chargePerTapBoost /= chargePerTapMultiplier;
            }
        }

        private void Awake()
        {
            VarInit();
            SubscribeToEvents();
        }


        private void VarInit()
        {
            chargePerTap = 1;
            gameConfigurationManager = gameController.GetConfigurationManager();
            powerTime = 0;
            beamState = false;
            upgradeState = false;
            powerTimeSlider.maxValue = 100;
            powerTimeSlider.minValue = 0;
            powerTimeSlider.value = 0;
            chargePerTapBoost = 1;
        }

        private void SubscribeToEvents()
        {
            EventManager.BeamSpawn += (a, b) => beamState = true;
            EventManager.BeamCrash += () =>
            {
                beamState = false;
            };

            EventManager.UpgradeState += _ => upgradeState = _;
            EventManager.SetActualPowerData += SetActualData;

            EventManager.LoadGame += Load;
        }

        private void Load(OfflineCalcData ocd)
        {
            powerTime -= Chronometer.TimeModule.TimeFromLastSave;
            if (powerTime < 0)
            {
                powerTimeSlider.value = 0;
                powerTime = 0;
            }
            else
            {
                if (workPowerTime == 0)
                {
                    var loadData = Save.SaveCreator.GetLoadData(SaveKeys.garageSaveKey).AsArray;
                    if (loadData != null)
                    {
                        for (int i = 0; i < loadData.Count; i++)
                        {
                            if (loadData[i].HasKey(SaveKeys.boerLvlDataKey)) SetActualData(loadData[i][SaveKeys.moneyDataKey]);
                        }
                    }

                    chargeValue = ((100 * powerTime * powerCutMultiplier) / workPowerTime);

                    powerTimeSlider.value = chargeValue;
                }
            }
        }

        private void SetActualData(int index)
        {
            var detailConfigData = gameConfigurationManager.detailData.boer[index];

            maxPowerTime = detailConfigData.maxWorkTime;
            workPowerTime = maxPowerTime;

            chargePerTap = detailConfigData.chargePerTap;
        }

        private void Update()
        {
            if ((beamState || upgradeState) && powerElement.activeSelf) powerElement.SetActive(false);
            else if (!beamState && !upgradeState && !powerElement.activeSelf) powerElement.SetActive(true);

            if (powerTime > 0)
            {
                EventManager.PowerStateEvent(true);
                if (!upgradeState)
                {
                    powerCutMultiplier = ((200 - powerTimeSlider.value) / 100);

                    powerTime -= Time.deltaTime;

                    if (chargePerTapBoost > 1)
                    {
                        if (powerTime < 2) chargePerTapBoost = 1;
                        else
                            chargePerTapBoost -= ((chargePerTapBoost / 100) * 1f);
                    }
                    else chargePerTapBoost = 1;

                    chargeValue = ((100 * powerTime * powerCutMultiplier) / workPowerTime);

                    powerTimeSlider.value = chargeValue;


                    if (!beamState)
                    {
                        EventManager.AnimationSpeedControlEvent(powerTimeSlider.value / 100);
                        gameController.OnPowerSpeed();
                    }
                }
            }
            else if (!beamState)
            {
                EventManager.PowerStateEvent(false);
                EventManager.AnimationSpeedControlEvent(0);
                gameController.OffPowerSpeed();
            }
            else gameController.OffPowerSpeed();
        }

        private void Vibrate()
        {
            if (GameData.GameSettingsData.vibration)
            {
#if UNITY_ANDROID && !UNITY_EDITOR
				GameData.GameSettingsData.vibrationFeature.Vibrate(200);
#elif UNITY_IOS && !UNITY_EDITOR
			    GameData.GameSettingsData.vibrationFeature.VibratePop();
#else
                GameData.GameSettingsData.vibrationFeature.SimpleVibrate();
#endif
            }
        }
    }
}
