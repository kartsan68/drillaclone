using System.Collections.Generic;

namespace BlackBears.DiggIsBig.GameData
{
    public class OfflineCalcData
    {
        public readonly double chestDelay;
        public readonly double subscribeTime;
        public readonly double deep;
        public readonly double bigBeamHealth;
        public readonly double smallBeamHealth;
        public readonly double smallBeamSpawnTime;
        public readonly double resourcesInStorage;
        public readonly int locationNumber;
        public readonly bool bigBeamIsSet;
        public readonly bool smallBeamIsSet;
        public readonly List<double> resourcesCount;
        public readonly double chestSpawnTick;
        public readonly double boxesSpawnTick;
        public readonly double kmIdForRes;
        public readonly bool bigChestIsSet;

        public readonly double ladleBoostTime;
        public readonly double ladleChanges;
        public readonly double ladleRefreshTime;
        public readonly double boosterSpawnTick;
        public readonly double bigBeamGasTime;

        public OfflineCalcData(double subscribeTime, double deep, double bigBeamHealth,
                               double smallBeamHealth, double smallBeamSpawnTime, double resourcesInStorage,
                               bool bigBeamIsSet, bool smallBeamIsSet, List<double> resourcesCount, int locationNumber,
                               double chestSpawnTick, double boxesSpawnTick, double chestDelay, double kmIdForRes, bool bigChestIsSet,
                            double ladleBoostTime, double ladleChanges, double ladleRefreshTime,
                               double boosterSpawnTick, double bigBeamGasTime)
        {
            this.subscribeTime = subscribeTime;
            this.deep = deep;
            this.bigBeamHealth = bigBeamHealth;
            this.smallBeamHealth = smallBeamHealth;
            this.smallBeamSpawnTime = smallBeamSpawnTime;
            this.resourcesInStorage = resourcesInStorage;
            this.bigBeamIsSet = bigBeamIsSet;
            this.smallBeamIsSet = smallBeamIsSet;
            this.resourcesCount = resourcesCount;
            this.locationNumber = locationNumber;
            this.chestSpawnTick = chestSpawnTick;
            this.boxesSpawnTick = boxesSpawnTick;
            this.chestDelay = chestDelay;
            this.kmIdForRes = kmIdForRes;
            this.bigChestIsSet = bigChestIsSet;
            this.ladleBoostTime = ladleBoostTime;
            this.ladleChanges = ladleChanges;
            this.ladleRefreshTime = ladleRefreshTime;
            this.boosterSpawnTick = boosterSpawnTick;
            this.bigBeamGasTime = bigBeamGasTime;
        }
    }
}