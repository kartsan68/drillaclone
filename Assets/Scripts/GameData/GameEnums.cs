﻿namespace BlackBears.DiggIsBig.GameData
{
    public enum Sound
    {
        PickUpResource,
        TapOnChest,
        TapOnBox,
        BoxExplosion,
        BackgroundMusic,
        BarrelExplosion,
        UsualTap
    }

    public enum SoundType
    {
        Sound,
        Music
    }

    public enum GameBoosterType
    {
        ClickBooster,
        LadleBooster,
        ChargeBooster
    }

    public enum LootType
    {
        Stone = 1,
        Coal,
        Iron,
        Gold,
        Emerald,
        SpeedBooster,
        ProductionBooster,
        Chest,
        ResBox,
        UniqRes,
        Barrel,
        Magnet,
        WildFarm,
        Silver,
        Platinum,
        Uran,
        Cezium,
        Kalifornium,
        Plutonium,
        Radium,
        Iridium,
        Tulium,
        GoldBooster
    }

    public enum ChestType
    {
        noneType,
        ChestWithRoulette,
        SmallChest,
        MediumChest,
        BigChest,
        ChestWithDetail,
        AdsChest,
        GoldPackSmall,
        GoldPackMedium,
        GoldPackBig,
        RouletteGold,
        RouletteRandomDetail
    }

    public enum RouletteWinElements
    {
        GoldPack1,
        GoldPack2,
        GoldPack3,
        GoldPack4,
        GoldPack5
    }

    public enum TimerType
    {
        BoostSpawnTimer,
        ChestSpawnTimer,
        RouletteSpawnTimer,
        InterstitialSpawnTimer,
        SmallBeamSpawnTimer,
        SubscribeTimer,
        SpeedBoosterTimer,
        ProductionBoosterTimer,
        SpeedRouletteBoostTimer,
        SpeedLongBoostTimer,
        ProductionRouletteBoostTimer,
        ProductionLongBoostTimer,
        DefaultTimer
    }

    public enum Screens
    {
        mine,
        storage,
        garage,
        settings,
        shop
    }
}