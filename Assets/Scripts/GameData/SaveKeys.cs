﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.GameData
{
    public static class SaveKeys
    {

        //beam spawner
        public static string bigBeamStateKey = "bbsk";
        public static string beamHealthKey = "bhk";
        public static string gasTimeKey = "gtk";
        public static string smallBeamStateKey = "sbsk";
        public static string beamSpawnerSaveKey = "BSC";

        //game controller
        public static string deepDataKey = "dd";
        public static string firstStartTimeKey = "fst";
        public static string firstNeverUpgradeDataKey = "fnu";
        public static string firstSessionDataKey = "fsss";
        public static string soundKey = "sc";
        public static string musicKey = "mc";
        public static string gameControllerSaveKey = "GC";

        //spawn time controller
        public static string smallBeamSpawnTimeKey = "sbst";
        public static string boxesSpawnTimeKey = "bstk";
        public static string chestsSpawnTimeKey = "cstk";
        public static string boostSpawnTimeKey = "bstk";
        public static string spawnTimeControllerSaveKey = "TSC";

        //shop
        public static string subscribeDataKey = "sbs";
        public static string chestDelayDataKey = "cddk";
        public static string ladleBoostTimeDataKey = "lbtdk";
        public static string ladleRefreshTimeDataKey = "lrtdk";
        public static string firstBoostKey = "fbk";
        public static string uniqPlayPlayerKey = "upp";
        public static string ladleChangesDataKey = "lcdk";

        public static string shopSaveKey = "SHP";

        //drill
        public static string actualBoerSpeedKey = "abs";

        public static string drillSaveKey = "D";

        //garage
        public static string engineLvlDataKey = "eld";
        public static string boerLvlDataKey = "bld";
        public static string ladleLvlDataKey = "lld";
        public static string storageLvlDataKey = "sld";
        public static string theLastUpdateTimeKey = "tlut";
        public static string firstGarageAlertShow = "fgas";
        public static string garageSaveKey = "G";

        //storage
        public static string farmCountKey = "fck";
        public static string unlockTimeKey = "utk";
        public static string unlockProcessKey = "ups";
        public static string isUnlockKey = "nlck";
        public static string countKey = "cnt";
        public static string moneyDataKey = "g";
        public static string resourceKey = "R";
        public static string storageSaveKey = "S";
        public static string setBigChestKey = "sbch";
        public static string storageStockKey = "stock";

        //location
        public static string actualLvlDataKey = "alvld";
        public static string actualLocationDataKey = "ald";
        public static string locationSaveKey = "LC";

        //booster
        public static string clickBoostTimeDataKey = "cbtdk";
        public static string boostersSaveKey = "BSK";

        //brill
        public static string brillInStorKey = "risk";
        public static string brillSaveKey = "brrsk";
    }
}
