using BlackBears.GameCore.Features.Vibration;

namespace BlackBears.DiggIsBig.GameData
{
    public static class GameSettingsData
    {
        public static VibrationFeature vibrationFeature;
        public static bool sound;
        public static bool music;
        public static bool vibration;
    }
}