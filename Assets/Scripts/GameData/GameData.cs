namespace BlackBears.DiggIsBig.GameData
{
    public static class GameData
    {
        public static bool isBigScreen = false;
        public static bool uniqPayUser = true;
        public static string rewardedFBToken = "ads_rew_view";
        public static string interstitialFBToken = "ads_int_view";
        public static float afkTime = 0;
        public static double CPS;
        public static double deep;
        public static float actualClickMultiplier = 1;
        public static float LadleMultiplier = 1;
        public static float LadleMultiplierBoost = 1;
        public static float boerSpeedWithEarthRigidity;
        public static int locationNumber = 0;
        public static bool bigChestIsSet;
        public static double goldCount;
        public static double smallGoldPackGoldCount;
        public static double mediumGoldPackGoldCount;
        public static double bigGoldPackGoldCount;
        public static bool storageIsFull;
        public static double withoutAdv;
        public static bool brilliantInTheStorage;
        public static bool firstStart = true;
        public static bool firstSession = true;
        public static bool firstNeverUpgrade = true;
        public static double theLastUpgradeTime = 0;
        public static double theLastUpgradeOnlineTime = 0;
        public static bool AdvertiseLoad = false;
        public static bool BuyLoad = false;
        public static bool wildFarmMode = false;
        public static int[] lvlDetails;
        public static bool[] redyUpDetails = new bool[] { false, false, false, false };
        public static int maxDetailsLvl;
        public static bool firstGarageAlertShow = true;
        public static bool clickBoosterIsActive = false;
        public static bool goldBoosterIsActive = false;


    }
}