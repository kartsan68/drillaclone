﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class GoldBooster : MonoBehaviour
    {
        private Transform spawner;
        private double workTime = 0;

        public void UseGoldBoost(float workTime, Transform spawner)
        {
            if (GameData.GameData.goldBoosterIsActive)
            {
                EventManager.PickUpSecondBoosterEvent(workTime);
                Destroy(this.gameObject);
            }
            else
            {
                this.spawner = spawner;
                this.workTime = workTime;
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                GameData.GameData.goldBoosterIsActive = true;
                EventManager.ReloadTick += _ => this.workTime -= _;
                EventManager.PickUpSecondBooster += _ => this.workTime += _;
                ConvertResToGold();
            }
        }

        private void Update()
        {
            if (workTime > 0)
            {
                workTime -= Time.deltaTime;
                if (workTime <= 0)
                {
                    GameData.GameData.goldBoosterIsActive = false;
                    Destroy(this.gameObject);
                }
            }
        }

        private void ConvertResToGold()
        {
            foreach (Transform child in this.spawner)
            {
                if (child.gameObject.CompareTag("Resource"))
                {
                    child.GetComponent<Loot>().ResToGold();
                }
            }
        }
    }
}
