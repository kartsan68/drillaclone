using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class Booster : MonoBehaviour
    {
        public float workTime;
        public float multiplier;
        public float dropChance;
        public int effectIndex;
    }
}