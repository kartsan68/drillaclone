﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using Spine.Unity;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class ResBox : MonoBehaviour
    {
        [SerializeField] private SkeletonAnimation boxSkeleton;
        [SerializeField] private ResBoxLabel resBoxLabel;

        private int oldBoxHealth;

        public Loot resource;
        public long resourcesCount;
        public int boxHealth;



        private void Start()
        {
            oldBoxHealth = boxHealth;
            boxSkeleton.AnimationState.SetAnimation(0, "idle_5", true);
        }

        private void PickUpBox()
        {
            EventManager.PickUpResBoxEvent(this.GetComponent<Loot>(), true);
            this.gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
        }


        public bool DamagedBox(int damage)
        {
            boxHealth -= damage;

            float percent = (100 * boxHealth) / oldBoxHealth;
            switch (boxHealth)
            {
                case 4:
                    boxSkeleton.AnimationState.SetAnimation(0, "5-4", false);
                    break;
                case 3:
                    boxSkeleton.AnimationState.SetAnimation(0, "4-3", false);
                    break;
                case 2:
                    boxSkeleton.AnimationState.SetAnimation(0, "3-2", false);
                    break;
                case 1:
                    boxSkeleton.AnimationState.SetAnimation(0, "2-1", false);
                    break;
                case 0:
                    boxSkeleton.AnimationState.SetAnimation(0, "1-0", false);
                    boxSkeleton.AnimationState.End += _ => PickUpBox();
                    return true;
            }

            return false;
        }

        public void ShowLabel(string count)
        {
            var label = Instantiate(resBoxLabel, transform.position, Quaternion.identity, this.gameObject.transform);
            label.PickupResource(resource.GetResImage(), count, this.gameObject);
        }

        public void PutResourcesInBox(Loot resource, long resCount)
        {
            this.resource = resource;
            resourcesCount = resCount;
        }

        public void SetResCount() => resource.lootCount = resourcesCount;
    }
}
