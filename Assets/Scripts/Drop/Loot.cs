﻿using System.Collections;
using BlackBears.DiggIsBig.Common;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class Loot : MonoBehaviour
    {
        [SerializeField] private Sprite goldImage;
        [SerializeField] private Sprite resImage;
        public float yDestroyPos;

        private float scrollSpeed;
        private GameController gameController;
        private bool alreadyTap = false;
        private ResBox resBox;
        private bool isActiveBoost = false;
        private bool goldState = false;

        public SpriteRenderer spriteRenderer;
        public bool isAppearance;
        public bool isUnlock;
        public double lootCount = 1;
        public int openDeep;
        public double lootPrice;
        public double unlockPrice;
        public float unlockTime;
        public double unlockWorkTime;
        public float unlockAdvCount;
        public bool unlockProcessIsStart = false;

        public LootType type;


        void Awake()
        {
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        }

        private void Start()
        {
            if (type.Equals(LootType.ResBox))
            {
                resBox = gameObject.GetComponent<ResBox>();
            }
        }

        private void Update()
        {
            if (!gameController.GetPauseState)
            {
                scrollSpeed = gameController.GetCaveSpeed;
                transform.Translate(new Vector3(0, Time.deltaTime * scrollSpeed, 0));

                if (transform.position.y > yDestroyPos)
                {
                    if (!isActiveBoost)
                    {
                        if (type.Equals(LootType.Chest))
                        {
                            if (gameObject.GetComponent<Chest>().GetChestType.Equals(ChestType.ChestWithRoulette)) EventManager.RouletteIsCrashEvent();
                        }
                        Destroy(this.gameObject);
                    }
                }
            }
        }

        private int childAlive;

        private IEnumerator TapReaction(double goldCount = 0)
        {
            childAlive = 0;
            ResTapTextReact resTapTextReact = gameController.GetFakeTextReactUnit;
            ResTapReact resTapReact = gameController.GetFakeReactUnit;

            ResTapTextReact fakeText = Instantiate(resTapTextReact, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity, this.transform);
            if (!goldState) fakeText.CreateCountResText(gameController, lootCount, Color.white);
            else fakeText.CreateCountResText(gameController, goldCount, Color.yellow);

            double visualLootCount = lootCount;

            if (lootCount > 5) visualLootCount = 5;

            if (!GameData.GameData.clickBoosterIsActive)
                visualLootCount = 1;

            spriteRenderer.enabled = false;

            for (int i = 0; i < visualLootCount; i++)
            {
                if (resTapReact != null)
                {
                    ResTapReact fake = Instantiate(resTapReact, new Vector3(transform.position.x, transform.position.y, -9), Quaternion.identity, this.transform);
                    fake.SetImage(spriteRenderer.sprite);
                    fake.gameObject.SetActive(true);
                    if (!goldState) fake.StartAnimation(this);
                    else fake.StartAnimation(this, false);
                    childAlive++;
                    yield return new WaitForSeconds(Random.Range(0.005f, 0.05f));
                }
                else break;
            }
        }

        private void TapUniqReaction()
        {
            childAlive = 0;
            ResTapTextReact resTapTextReact = gameController.GetFakeTextReactUnit;
            ResTapReact resTapReact = gameController.GetFakeReactUnit;

            ResTapTextReact fakeText = Instantiate(resTapTextReact, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity, this.transform);
            fakeText.CreateCountResText(gameController, lootCount, Color.white);

            ResTapReact fake = Instantiate(resTapReact, new Vector3(transform.position.x, transform.position.y, -9), Quaternion.identity, this.transform);
            fake.SetImage(spriteRenderer.sprite);
            fake.gameObject.SetActive(true);
            fake.StartAnimation(this);
            childAlive++;

            spriteRenderer.enabled = false;
        }

        public void ResToGold()
        {
            spriteRenderer.sprite = goldImage;
            goldState = true;
        }

        public int CheckAllChild() => childAlive;

        public void ChildDestroy() => childAlive--;

        public Sprite GetResImage() => resImage;

        public void PickUpThisRes()
        {
            if (!alreadyTap)
            {
                alreadyTap = true;
                EventManager.PickUpResourcesEvent(this);
                StartCoroutine(TapReaction());
            }
        }

        private void PickUp()
        {
            if (!IsPointerOverUIObject.IsPointerOverUIObjectMeth() && !alreadyTap && gameObject != null)
            {
                switch (gameObject.tag)
                {
                    case "Resource":
                        if (!goldState)
                        {
                            if (GameData.GameData.storageIsFull) EventManager.CantTouchResEvent();

                            if (!GameData.GameData.storageIsFull || GameData.GameData.wildFarmMode)
                            {
                                Vibrate();
                                alreadyTap = true;
                                EventManager.PickUpResourcesEvent(this);
                                StartCoroutine(TapReaction());
                            }
                        }
                        else
                        {
                            Vibrate();
                            alreadyTap = true;
                            double goldCount = lootCount * lootPrice * GameData.GameData.actualClickMultiplier;
                            EventManager.GiveMeMoneyEvent(goldCount);
                            StartCoroutine(TapReaction(goldCount));
                        }
                        break;
                    case "Chest":
                        EventManager.PickUpChestsEvent(this);
                        Destroy(this.gameObject);
                        break;
                    case "ResBox":
                        Vibrate();
                        if (resBox != null)
                            if (resBox.DamagedBox(1)) alreadyTap = true;
                        break;
                    case "UniqRes":
                        Vibrate();
                        alreadyTap = true;
                        EventManager.PickUpUniqEvent(this.GetComponent<UniqRes>());
                        TapUniqReaction();
                        break;
                    case "Boost":
                        Vibrate();
                        isActiveBoost = true;
                        switch (type)
                        {
                            case LootType.Barrel:
                                gameObject.GetComponent<Barrel>().ClickonBarrel();
                                break;
                            case LootType.Magnet:
                                gameObject.GetComponent<Magnet>().UseMagnet(gameObject.GetComponent<Booster>().workTime,
                                                              1600, gameController.GetSpawner().transform);
                                break;
                            case LootType.GoldBooster:
                                gameObject.GetComponent<GoldBooster>().UseGoldBoost(gameObject.GetComponent<Booster>().workTime,
                                                               gameController.GetSpawner().transform);
                                break;
                            default:
                                Debug.Log("Что ты такое?");
                                break;
                        }
                        break;
                }
            }
        }

        private void Vibrate()
        {
            if (GameData.GameSettingsData.vibration)
            {
#if UNITY_ANDROID && !UNITY_EDITOR
				GameData.GameSettingsData.vibrationFeature.Vibrate(100);
#elif UNITY_IOS && !UNITY_EDITOR
			    GameData.GameSettingsData.vibrationFeature.VibratePop();
#else
                GameData.GameSettingsData.vibrationFeature.SimpleVibrate();
#endif
            }
        }
    }
}



