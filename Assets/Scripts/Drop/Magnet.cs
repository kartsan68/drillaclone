﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class Magnet : MonoBehaviour
    {
        [SerializeField] private Confines confines;

        private float workTime = 0;
        private float yMaxPos;
        private Transform spawner;
        private List<GameObject> allResourcesInMine;
        private Coroutine coroutine;


        public void UseMagnet(float workTime, float yMaxPos, Transform spawner)
        {
            EventManager.MagnetBoostUsedEvent(1, workTime);
            this.workTime = workTime;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.spawner = spawner.transform;
            this.yMaxPos = yMaxPos;

            SearchResources();
            coroutine = StartCoroutine(PickUpAllRes());
        }

        private void Update()
        {
            if (workTime > 0)
            {
                workTime -= Time.deltaTime;
                if (workTime <= 0)
                {
                    StopCoroutine(coroutine);
                    Destroy(this.gameObject);
                }
            }
        }

        private IEnumerator PickUpAllRes()
        {
            int counter = 0;
            int maxCount = allResourcesInMine.Count;
            int iter;
            bool stopIt = false;

            while (workTime >= 1)
            {
                if (counter < maxCount && allResourcesInMine.Count >= 1)
                {
                    while (GameData.GameData.storageIsFull) yield return null;

                    iter = allResourcesInMine.Count - 1;
                    counter++;

                    if (allResourcesInMine[iter] != null)
                    {
                        if (allResourcesInMine[iter].transform.position.y >= yMaxPos)
                        {
                            stopIt = true;
                        }
                        else if (allResourcesInMine[iter].transform.position.y >= 500)
                            allResourcesInMine[iter].GetComponent<Loot>().PickUpThisRes();
                    }
                    allResourcesInMine.RemoveAt(iter);
                }
                else if (allResourcesInMine.Count == 0 || stopIt)
                {
                    SearchResources();
                    stopIt = false;
                    counter = 0;
                    maxCount = allResourcesInMine.Count;
                }
                yield return new WaitForSeconds(0.05f);

            }
        }

        private void SearchResources()
        {
            allResourcesInMine = new List<GameObject>();
            foreach (Transform child in this.spawner)
            {
                GameObject element = child.gameObject;
                if (element.CompareTag("Resource") && element.transform.position.x < confines.xMax
                && element.transform.position.x > confines.xMin)
                {
                    allResourcesInMine.Add(element);
                    Sort();
                }
            }
        }

        //пузырьковая сортировка, если будет торомзить заменить
        private void Sort()
        {
            for (int i = 0; i < allResourcesInMine.Count - 1; i++)
            {
                for (int j = 0; j < allResourcesInMine.Count - i - 1; j++)
                {
                    if (allResourcesInMine[j + 1].transform.position.y > allResourcesInMine[j].transform.position.y)
                    {
                        var temp = allResourcesInMine[j + 1];
                        allResourcesInMine[j + 1] = allResourcesInMine[j];
                        allResourcesInMine[j] = temp;
                    }
                }
            }
        }
    }
}
