﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Drop.Managers
{
    public class BoostManager : MonoBehaviour
    {
        [SerializeField] private Button clickBoostButton;
        [SerializeField] private GameController gameController;
        [SerializeField] private Booster[] boosters;
        [SerializeField] private GameObject adsClickBoostLabel;
        [SerializeField] private Slider clickBoostProgress;

        private CountdownTimer clickBoostTimer;
        private BoostLableController boostLabelController;
        private bool clickBoostIsActive = false;
        private bool firstBoostCharge = true;
        private Animator clickBoostButtonAnimator;

        public void Save()
        {
            List<SaveData> data = new List<SaveData>();
            data.Add(new SaveData(SaveKeys.clickBoostTimeDataKey, clickBoostTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.firstBoostKey, firstBoostCharge));
            SaveCreator.Save(SaveKeys.boostersSaveKey, data);
        }

        public void LoadSave()
        {
            var data = SaveCreator.GetLoadData(SaveKeys.boostersSaveKey).AsArray;

            if (data != null)
            {
                float clickBoostTime = 0;

                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].HasKey(SaveKeys.clickBoostTimeDataKey))
                    {
                        clickBoostTime = data[i][SaveKeys.clickBoostTimeDataKey];
                    }
                    if (data[i].HasKey(SaveKeys.firstBoostKey))
                    {
                        firstBoostCharge = data[i][SaveKeys.firstBoostKey];
                    }
                }

                if (firstBoostCharge) adsClickBoostLabel.SetActive(false);
                else adsClickBoostLabel.SetActive(true);

                clickBoostTime -= Chronometer.TimeModule.TimeFromLastSave;

                if (clickBoostTime > 0)
                {
                    EventManager.ClickBoostUsedEvent(boosters[0].multiplier, clickBoostTime);
                    clickBoostTimer.ResetTimer(clickBoostTime, () =>
                    {
                        clickBoostIsActive = false;
                        EventManager.ClickBoostStopEvent();
                    });
                    clickBoostProgress.minValue = 0;
                    clickBoostProgress.maxValue = boosters[0].workTime;
                    clickBoostIsActive = true;
                }
            }
        }

        private void Reload()
        {
            if (!GameData.GameData.firstStart)
            {
                var time = clickBoostTimer.GetTime();
                if (time > 0)
                {
                    clickBoostIsActive = true;
                    time -= Chronometer.TimeModule.TimeFromLastSave;
                    if (time < 0) time = 0;
                    clickBoostTimer.ResetTimer(time, () =>
                     {
                         clickBoostIsActive = false;
                         EventManager.ClickBoostStopEvent();
                     });
                }
            }
        }

        private void Start()
        {
            InitVar();
            SubscribeToEvent();
            LoadSave();
        }
        private void InitVar()
        {
            boostLabelController = gameObject.GetComponent<BoostLableController>();

            clickBoostTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            clickBoostTimer.SetUstopable(true);
            clickBoostTimer.MakeThisTimerCallbackableUncallbackble((t) => clickBoostProgress.value = (float)t, true);

            if (firstBoostCharge) adsClickBoostLabel.SetActive(false);
            else adsClickBoostLabel.SetActive(true);

            clickBoostButtonAnimator = clickBoostButton.GetComponent<Animator>();
        }

        private void RewardedAdvertiseStatus(bool state)
        {
            if (state)
            {

                clickBoostButton.interactable = state;
                if (!firstBoostCharge) adsClickBoostLabel.SetActive(true);
                else adsClickBoostLabel.SetActive(false);
            }
            else
            {
                if (!firstBoostCharge) clickBoostButton.interactable = state;
                adsClickBoostLabel.SetActive(false);
            }


        }

        private void Update()
        {
            if (clickBoostButton.interactable && !clickBoostButtonAnimator.GetBool("isActive") && clickBoostProgress.value == 0)
                clickBoostButtonAnimator.SetBool("isActive", true);
            else if ((!clickBoostButton.interactable || clickBoostProgress.value > 0) && clickBoostButtonAnimator.GetBool("isActive"))
                clickBoostButtonAnimator.SetBool("isActive", false);
        }

        private void SubscribeToEvent()
        {
            // EventManager.PickUpClickBooster += ClickBoost;
            EventManager.SaveGame += Save;
            EventManager.LoadGame += (a) => Reload();
            EventManager.RewardedAdvertiseStatus += RewardedAdvertiseStatus;
        }


        public void ClickBoost()
        {
            clickBoostIsActive = true;
            clickBoostProgress.minValue = 0;
            clickBoostProgress.maxValue = boosters[0].workTime;

            if (firstBoostCharge)
            {
                EventManager.ClickBoostUsedEvent(boosters[0].multiplier, boosters[0].workTime);
                clickBoostTimer.ResetTimer(boosters[0].workTime, () =>
                {
                    clickBoostIsActive = false;
                    EventManager.ClickBoostStopEvent();
                });
                adsClickBoostLabel.SetActive(true);
                firstBoostCharge = false;
            }
            else
            {
                EventManager.CallRewardedAdvertWithActionEvent(() =>
                {
                    EventManager.ClickBoostUsedEvent(boosters[0].multiplier, boosters[0].workTime);
                    clickBoostTimer.ResetTimer(boosters[0].workTime, () =>
                    {
                        clickBoostIsActive = false;
                        EventManager.ClickBoostStopEvent();
                    });
                });
            }
        }

    }
}
