﻿using BlackBears.DiggIsBig.GameData;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class Chest : MonoBehaviour
    {
        [SerializeField] private ChestType type;

        private PDouble goldCount;
        public GameObject detail;
        public bool advertise;

        public GameObject OpenDetailChest => detail;
        public double OpenGoldChest => goldCount;
        public ChestType GetChestType => type;

        public void PutSomeGold(double gold) => goldCount = gold;

        public void PutRoulette() => type = ChestType.ChestWithRoulette;

        public void PutDetail(GameObject detail) => this.detail = detail;


        private void Awake()
        {
            goldCount = new PDouble();
        }

    }
}
