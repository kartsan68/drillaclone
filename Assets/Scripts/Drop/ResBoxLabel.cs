﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class ResBoxLabel : MonoBehaviour
    {
        [SerializeField] private TextMesh resCount;
        [SerializeField] private SpriteRenderer resPreview;
        [SerializeField] private Animator animator;

        private GameObject objectFromDestroy;

        public void PickupResource(Sprite preview, string count, GameObject objectFromDestroy)
        {
            this.objectFromDestroy = objectFromDestroy;
            resCount.text = count;
            resPreview.sprite = preview;
            animator.Play("ShowLabelRes", -1, 0f);
        }

        public void EndAnimation()
        {
            Destroy(objectFromDestroy);
        }


    }
}
