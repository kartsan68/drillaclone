﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;


namespace BlackBears.DiggIsBig.Drop
{
    public class WildFarm : MonoBehaviour
    {
        private bool startBoost = false;
        private float time = 0;
        public void ActivateWildFarmBooster(Booster booster)
        {
            GameData.GameData.wildFarmMode = true;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            time = booster.workTime;
            startBoost = true;
            EventManager.WildFarmBoostUsedEvent(booster.multiplier, booster.workTime);
        }

        private void Update()
        {
            if (startBoost)
            {
                time -= Time.deltaTime;
                if (time <= 0)
                {
                    time = 0;
                    startBoost = false;
                    GameData.GameData.wildFarmMode = false;
                    EventManager.WildFarmBoostStopEvent();
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
