﻿using System.Collections;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class BoostLablePositionCorrector : MonoBehaviour
    {
        [SerializeField] private GameObject speedLabel;
        [SerializeField] private GameObject productLabel;
        [SerializeField] private GameObject speedDonateLable;
        [SerializeField] private GameObject productDonateLable;

        private Vector3 firstPos;
        private Vector3 secondPos;
        private Vector3 thirdPos;
        private Vector3 fourthPos;

        private GameObject[] labels;

        private void Awake() => SetPositions();

        private void SetPositions()
        {
            firstPos = speedLabel.transform.position;
            secondPos = productLabel.transform.position;
            thirdPos = speedDonateLable.transform.position;
            fourthPos = productDonateLable.transform.position;

            labels = new GameObject[] { speedLabel, productLabel, speedDonateLable, productDonateLable };
        }

        private void Update()
        {
            if (speedLabel.activeSelf || productLabel.activeSelf
            || speedDonateLable.activeSelf || productDonateLable.activeSelf)
            {
                CheckPosition();
            }
        }

        private void CheckPosition()
        {
            for (int i = 0; i < labels.Length; i++)
            {
                if (labels[i].activeSelf)
                {
                    switch (i)
                    {
                        case 0:
                            labels[i].transform.position = firstPos;
                            break;
                        case 1:
                            if (!labels[i - 1].activeSelf)
                            {
                                labels[i].transform.position = firstPos;
                            }
                            else
                            {
                                labels[i].transform.position = secondPos;
                            }
                            break;
                        case 2:
                            if (!labels[i - 2].activeSelf && !labels[i - 1].activeSelf)
                            {
                                labels[i].transform.position = firstPos;
                            }
                            else if ((labels[i - 2].activeSelf && !labels[i - 1].activeSelf)
                                 || (!labels[i - 2].activeSelf && labels[i - 1].activeSelf))
                            {
                                labels[i].transform.position = secondPos;
                            }
                            else
                            {
                                labels[i].transform.position = thirdPos;
                            }
                            break;
                        case 3:
                            if (!labels[i - 3].activeSelf && !labels[i - 2].activeSelf && !labels[i - 1].activeSelf)
                            {
                                labels[i].transform.position = firstPos;
                            }
                            else if ((labels[i - 3].activeSelf && !labels[i - 2].activeSelf && !labels[i - 1].activeSelf)
                                 || (!labels[i - 3].activeSelf && !labels[i - 2].activeSelf && labels[i - 1].activeSelf)
                                 || (!labels[i - 3].activeSelf && labels[i - 2].activeSelf && !labels[i - 1].activeSelf))
                            {
                                labels[i].transform.position = secondPos;
                            }
                            else if ((labels[i - 3].activeSelf && labels[i - 2].activeSelf && !labels[i - 1].activeSelf)
                                 || (labels[i - 3].activeSelf && !labels[i - 2].activeSelf && labels[i - 1].activeSelf)
                                 || (!labels[i - 3].activeSelf && labels[i - 2].activeSelf && labels[i - 1].activeSelf))
                            {
                                labels[i].transform.position = thirdPos;
                            }
                            else
                            {
                                labels[i].transform.position = fourthPos;
                            }
                            break;
                    }

                }
            }
        }

    }
}