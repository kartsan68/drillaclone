﻿using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Drop
{
    public class Barrel : MonoBehaviour
    {
        private Animator animator;
        private int barrelHp = 4;

        private void Awake()
        {
            animator = gameObject.GetComponent<Animator>();
        }

        public void ClickonBarrel()
        {
            barrelHp -= 1;
            animator.Play("BarrelAnimation");
            if (barrelHp <= 0)
            {
                EventManager.PickUpBarrelEvent();
                Destroy(this.gameObject);
            }
        }


    }
}
