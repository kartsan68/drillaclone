using System.Collections.Generic;
using BlackBears.SQBA;
using BlackBears.DiggIsBig.Configuration.Data;
using UnityEngine;

namespace BlackBears.DiggIsBig.Shop
{
    public class ShopManager : SQBAShoppingManager
    {
        private Shop shop;

        public ShopManager(Shop shop)
        {
            this.shop = shop;
        }

        //Вызывается при восстановлении подписок(iOS) (в этом списке iap'ы подписок которые востановились)
        protected override void OnActiveSubscriptionsCome(List<string> subscriptions)
        {
            shop.OnActiveSubscriptionsCome(subscriptions);
        }

        //когда подписка кончилась
        protected override void ResetSubscription()
        {
            shop.SubscribeIsEnd();
        }

        //отмена покупки
        protected override void ShopItemCanceled(SQBAShopItem shopItem)
        {
            Debug.Log("cancel");
        }

        //Ошибка при покупке
        protected override void ShopItemFailed(SQBAShopItem shopItem)
        {
            Debug.Log("fail");
        }

        //Вызывается когда покупка прошла успешно
        protected override void ShopItemPurchased(SQBAShopItem shopItem)
        {
            Debug.Log(shopItem.name);
            shop.CheckSubscribe(shopItem);
        }

    }
}