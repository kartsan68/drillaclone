﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BlackBears.DiggIsBig.GameElements.Elements;
using BlackBears.DiggIsBig.Advertise;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Managers;
using UnityEngine.UI;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Save;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.UI;
using UnityEngine.Events;
using BlackBears.SQBA;
using System;
using BlackBears.GameCore.Features.Localization;
using BlackBears.GameCore.Features.Clans;
using BlackBears.GameCore;

namespace BlackBears.DiggIsBig.Shop
{
    public class Shop : MonoBehaviour
    {
        [SerializeField] private Button ladleBoostButton;
        [SerializeField] private GameObject adsLabel;
        [SerializeField] private Slider ladleBoostProgress;
        [SerializeField] private GameObject chestNotification;
        [SerializeField] private PickUpChestAlert pickUpChestAlert;
        [SerializeField] private StorageController storageController;
        [SerializeField] private Advert advertising;
        [SerializeField] private GameController gameController;
        [SerializeField] private Button goToMineButton;
        [SerializeField] private Button goToShopButton;
        [SerializeField] private Text detailPackPriceText;
        [SerializeField] private Text cdTextInAdsChest;
        [SerializeField] private Text smallPackPriceText;
        [SerializeField] private Text smallPackCountText;
        [SerializeField] private Text mediumPackPriceText;
        [SerializeField] private Text mediumPackCountText;
        [SerializeField] private Text bigPackPriceText;
        [SerializeField] private Text bigPackCountText;
        [SerializeField] private Text smallPackPriceText2;
        [SerializeField] private Text smallPackCountText2;
        [SerializeField] private Text mediumPackPriceText2;
        [SerializeField] private Text mediumPackCountText2;
        [SerializeField] private Text bigPackPriceText2;
        [SerializeField] private Text bigPackCountText2;
        [SerializeField] private Text subscribeText;
        [SerializeField] private Text subscribeStateText;
        [SerializeField] private GameObject iosButtonBar;
        [SerializeField] private Button rouletteBtn;
        [SerializeField] private GameObject saleLoader;

        private GameConfigurationManager gameConfigurationManager;
        private CountdownTimer chestDelayTimer;
        private int smalPackGameTime;
        private int mediumPackGameTime;
        private int bigPackGameTime;
        private int AdsChestGameTime;
        private float chestDelay;
        private bool chestIsReady;
        private DrillFormatter drillFormatter;
        private UnityAction actualSuccessAction;
        private UnityAction actualFailAction;
        private double ladleBoost;
        private double ladleBoostTime;
        private double ladleBoostRefreshTime;
        private double ladleBoostChargeCount;
        private double ladleBoostChargeActual;
        float clickBoostTimeS;
        float ladleBoostTimeS;
        private CountdownTimer ladleBoostTimer;
        private CountdownTimer ladleBoostRefreshTimer;
        private ShopManager shopManager;
        private bool configAlredySet = false;
        private SQBAShopItem[] shopItems;
        private bool subscribe = false;
        private bool firstBoostCharge = true;
        private Animator ladleBoostButtonAnimator;

        #region Save/Load
        public void Save()
        {
            List<SaveData> data = new List<SaveData>();
            data.Add(new SaveData(SaveKeys.subscribeDataKey, 0));
            data.Add(new SaveData(SaveKeys.chestDelayDataKey, chestDelayTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.ladleBoostTimeDataKey, ladleBoostTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.ladleChangesDataKey, ladleBoostChargeActual));
            data.Add(new SaveData(SaveKeys.ladleRefreshTimeDataKey, ladleBoostRefreshTimer.GetTime()));
            data.Add(new SaveData(SaveKeys.firstBoostKey, firstBoostCharge));
            data.Add(new SaveData(SaveKeys.uniqPlayPlayerKey, GameData.GameData.uniqPayUser));


            SaveCreator.Save(SaveKeys.shopSaveKey, data);
        }

        public void Load(OfflineCalcData ocd)
        {
            Debug.Log("Shop");

            var loadData = SaveCreator.GetLoadData(SaveKeys.shopSaveKey).AsArray;
            for (int i = 0; i < loadData.Count; i++)
            {
                if (loadData[i].HasKey(SaveKeys.firstBoostKey))
                {
                    firstBoostCharge = loadData[i][SaveKeys.firstBoostKey];
                }
                if (loadData[i].HasKey(SaveKeys.uniqPlayPlayerKey))
                {
                    GameData.GameData.uniqPayUser = loadData[i][SaveKeys.uniqPlayPlayerKey];
                }
            }
            if (firstBoostCharge) adsLabel.SetActive(false);
            else adsLabel.SetActive(true);

            if (ocd.chestDelay > 0)
            {
                chestIsReady = false;
                chestDelayTimer.ResetTimer(ocd.chestDelay, () => chestIsReady = true);
            }
            else
            {
                SetTextInAdsChest(DigIsBigLocalizator.GetLocalizationString("SHOP_OPEN", Modificator.ToUpper));
                chestIsReady = true;
            }


            if (ocd.ladleChanges > 0)
            {
                // ladleBoostButton.interactable = true;
                ladleBoostChargeActual = ocd.ladleChanges;
            }
            else
            {
                // ladleBoostButton.interactable = false;
                ladleBoostChargeActual = 0;
            }

            if (!configAlredySet)
                SetConfig();

            if (ocd.ladleRefreshTime > 0)
            {
                ladleBoostRefreshTimer.ResetTimer(ocd.ladleRefreshTime, () =>
           {
               //    adsLabel.SetActive(true);
               ladleBoostChargeActual = ladleBoostChargeCount;
               RestartRefreshTimer();
           });
            }
            else
            {
                ladleBoostChargeActual = ladleBoostChargeCount;
                // adsLabel.SetActive(true);
                RestartRefreshTimer();
            }


            if (ocd.ladleBoostTime > 0)
            {
                GameData.GameData.LadleMultiplierBoost = (float)ladleBoost;
                GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier * ladleBoost);
                EventManager.LadleBoostUsedEvent(ladleBoost, ocd.ladleBoostTime);
                ladleBoostTimer.ResetTimer(ocd.ladleBoostTime, () =>
                {
                    GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier / ladleBoost);
                    GameData.GameData.LadleMultiplierBoost = 1;
                });
            }
        }
        #endregion

        private void Awake()
        {
            VarInit();
            SetTextInAdsChest(DigIsBigLocalizator.GetLocalizationString("SHOP_OPEN", Modificator.ToUpper));
            chestIsReady = true;
        }
        private void Start()
        {
            SetConfig();
            RestartRefreshTimer();
            SubscribeToEvent();
            ladleBoostProgress.minValue = 0;
            ladleBoostProgress.maxValue = (float)(ladleBoostChargeCount * ladleBoostTime);

#if UNITY_IOS
            iosButtonBar.SetActive(true);
#elif UNITY_ANDROID
            iosButtonBar.SetActive(false);
#endif
        }

        private IEnumerator SetPrices()
        {
            int setPriceINT = 10;
            while (setPriceINT != 0)
            {
                smallPackPriceText.text = shopItems[0].LocalizedPrice;
                mediumPackPriceText.text = shopItems[1].LocalizedPrice;
                bigPackPriceText.text = shopItems[2].LocalizedPrice;
                smallPackPriceText2.text = shopItems[0].LocalizedPrice;
                mediumPackPriceText2.text = shopItems[1].LocalizedPrice;
                bigPackPriceText2.text = shopItems[2].LocalizedPrice;

                detailPackPriceText.text = shopItems[3].LocalizedPrice;
                subscribeText.text = shopItems[4].LocalizedPrice + " " + DigIsBigLocalizator.GetLocalizationString("SHOP_MONTH", Modificator.ToUpper);

                if (subscribe) subscribeStateText.text = DigIsBigLocalizator.GetLocalizationString("SHOP_ACTIVE", Modificator.ToUpper);
                else subscribeStateText.text = DigIsBigLocalizator.GetLocalizationString("SHOP_ON", Modificator.ToUpper);

                setPriceINT--;
                yield return new WaitForSeconds(1f);
            }
        }

        private void Update()
        {
            if (!chestIsReady)
            {
                chestNotification.SetActive(false);
                SetTextInAdsChest(drillFormatter.TimeFormatter(chestDelayTimer.GetTime()));
            }
            else
            {
                chestNotification.SetActive(true);
                SetTextInAdsChest(DigIsBigLocalizator.GetLocalizationString("SHOP_OPEN", Modificator.ToUpper));
            }

            ladleBoostTimeS = ladleBoostTimer.GetTime();

            if (ladleBoostTimeS > 0)
            {
                if (ladleBoostButtonAnimator.GetBool("isActive")) ladleBoostButtonAnimator.SetBool("isActive", false);
                ladleBoostProgress.value = ladleBoostTimeS;
            }
            else
            {
                if (!ladleBoostButtonAnimator.GetBool("isActive") && ladleBoostButton.interactable) ladleBoostButtonAnimator.SetBool("isActive", true);
                ladleBoostProgress.value = 0;
            }

            if (!ladleBoostButton.interactable && ladleBoostButtonAnimator.GetBool("isActive")) ladleBoostButtonAnimator.SetBool("isActive", false);
        }

        private void RestartRefreshTimer()
        {
            ladleBoostRefreshTimer.ResetTimer(ladleBoostRefreshTime, () =>
            {
                // adsLabel.SetActive(true);
                ladleBoostChargeActual = ladleBoostChargeCount;
                RestartRefreshTimer();
            });
        }

        private void VarInit()
        {
            ladleBoostTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            ladleBoostTimer.SetUstopable(true);

            ladleBoostRefreshTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            ladleBoostRefreshTimer.SetUstopable(true);

            drillFormatter = new DrillFormatter();

            chestDelayTimer = TimeManager.GetEmptyCountdownTimer(this.gameObject);
            chestDelayTimer.SetUstopable(true);

            EventManager.SetTimerEvent(GameBoosterType.LadleBooster, ladleBoostTimer);

            if (firstBoostCharge)
                adsLabel.SetActive(false);
            else adsLabel.SetActive(true);

            ladleBoostButtonAnimator = ladleBoostButton.GetComponent<Animator>();
        }

        private void SubscribeToEvent()
        {
            EventManager.NotEnoughtMoney += NotEnoughtMoney;
            EventManager.SaveGame += Save;
            EventManager.LoadGame += Load;
            EventManager.CPSChanged += SetShopData;
            EventManager.RewardedAdvertiseStatus += RewardedAdvertiseStatus;
            BBGC.Features.GetFeature<ClansFeature>().Core.OnClansShowStateChanged += (state) =>
            {
                if (state) GoToMineButtonPress();
            };
        }

        private void SetConfig()
        {
            gameConfigurationManager = gameController.GetConfigurationManager();

            var shopData = gameConfigurationManager.shopData;

            shopManager = new ShopManager(this);
            shopItems = gameConfigurationManager.shopItemData.shopItems;
            shopManager.Init(shopItems);

            /*0 - advGoldChest 1 - detailPack 2 - smallGoldPack 3 - mediumGoldPack
                       4 - bigGoldPack 5  - subscribe 6 - ladle boost*/

            ladleBoost = shopData.shopContent[6].content.multiplier;
            ladleBoostTime = shopData.shopContent[6].content.workTime;
            ladleBoostRefreshTime = shopData.shopContent[6].content.refreshTime;
            ladleBoostChargeCount = shopData.shopContent[6].content.chargeCount;
            ladleBoostChargeActual = ladleBoostChargeCount;

            smalPackGameTime = shopData.shopContent[2].content.gameTime;

            mediumPackGameTime = shopData.shopContent[3].content.gameTime;

            bigPackGameTime = shopData.shopContent[4].content.gameTime;

            AdsChestGameTime = shopData.shopContent[0].content.gameTime;
            chestDelay = shopData.shopContent[0].content.delay;

            StartCoroutine(SetPrices());
            configAlredySet = true;
        }

        private void RewardedAdvertiseStatus(bool state)
        {
            if (state)
            {
                if (ladleBoostChargeActual > 0 && !firstBoostCharge) adsLabel.SetActive(true);
                else adsLabel.SetActive(false);
                rouletteBtn.interactable = true;
                ladleBoostButton.interactable = true;
            }
            else
            {
                rouletteBtn.interactable = false;
                adsLabel.SetActive(false);
                if (!firstBoostCharge) ladleBoostButton.interactable = false;
            }
        }

        private void SetShopData()
        {
            var cps = GameData.GameData.CPS;

            if (cps < 2) cps = 2;

            GameData.GameData.smallGoldPackGoldCount = smalPackGameTime * cps;
            smallPackCountText.text = drillFormatter.MoneyKFormatter((smalPackGameTime * cps));

            GameData.GameData.mediumGoldPackGoldCount = mediumPackGameTime * cps;
            mediumPackCountText.text = drillFormatter.MoneyKFormatter((mediumPackGameTime * cps));

            GameData.GameData.bigGoldPackGoldCount = bigPackGameTime * cps;
            bigPackCountText.text = drillFormatter.MoneyKFormatter((bigPackGameTime * cps));

            smallPackCountText2.text = drillFormatter.MoneyKFormatter((smalPackGameTime * cps));

            mediumPackCountText2.text = drillFormatter.MoneyKFormatter((mediumPackGameTime * cps));

            bigPackCountText2.text = drillFormatter.MoneyKFormatter((bigPackGameTime * cps));
        }

        private void SetTextInAdsChest(string text) => cdTextInAdsChest.text = text;

        private void NotEnoughtMoney(double money, UnityAction onSuccess, UnityAction onFail)
        {
            actualSuccessAction = onSuccess;
            actualFailAction = onFail;
        }

        private void PurchaseItem(SQBAShopItem shopItem, UnityAction onSuccess, UnityAction onFaild)
        {
            GameData.GameData.BuyLoad = true;
            saleLoader.SetActive(true);
            shopManager.PurchaseShopItem(shopItem, () =>
                          {
                              AdjustEventManager.RevenueAllEvent(shopItem.DollarEquivalent, "USD");
                              if (GameData.GameData.uniqPayUser)
                              {
                                  GameData.GameData.uniqPayUser = false;
                                  AdjustEventManager.UniquePlayerEvent();
                              }
                              onSuccess?.Invoke();
                              GameData.GameData.BuyLoad = false;
                              saleLoader.SetActive(false);
                          },
                           () =>
                          {
                              GameData.GameData.BuyLoad = false;
                              saleLoader.SetActive(false);
                          },
                           (a, b) =>
                          {
                              actualFailAction?.Invoke();
                              GameData.GameData.BuyLoad = false;
                              saleLoader.SetActive(false);
                          }, new Dictionary<string, object>());
        }

        #region  Buttons

        public void ClearActualAction()
        {
            actualFailAction = null;
            actualSuccessAction = null;
        }

        public void LadlesBoost()
        {
            if (firstBoostCharge)
            {
                firstBoostCharge = false;
                adsLabel.SetActive(true);

                if (ladleBoostTimer.GetTime() > 0)
                {
                    double sumTime = ladleBoostTime + ladleBoostTimer.GetTime();
                    if (sumTime > ladleBoostProgress.maxValue) sumTime = ladleBoostProgress.maxValue;

                    EventManager.LadleBoostUsedEvent(GameData.GameData.LadleMultiplier * ladleBoost, sumTime);
                    ladleBoostTimer.ResetTimer(sumTime, () =>
                    {
                        GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier / ladleBoost);
                        GameData.GameData.LadleMultiplierBoost = 1;
                    });
                }
                else
                {
                    GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier * ladleBoost);
                    GameData.GameData.LadleMultiplierBoost = (float)ladleBoost;
                    EventManager.LadleBoostUsedEvent(GameData.GameData.LadleMultiplier * ladleBoost, ladleBoostTime);
                    ladleBoostTimer.ResetTimer(ladleBoostTime, () =>
                    {
                        GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier / ladleBoost);
                        GameData.GameData.LadleMultiplierBoost = 1;
                    });
                }
            }
            // if (ladleBoostChargeActual > 0)
            // {
            else
            {
                EventManager.CallRewardedAdvertWithActionEvent(() =>
                {
                    // ladleBoostChargeActual--;

                    // if (ladleBoostChargeActual <= 0 )
                    //     adsLabel.SetActive(false);

                    if (ladleBoostTimer.GetTime() > 0)
                    {
                        double sumTime = ladleBoostTime + ladleBoostTimer.GetTime();
                        if (sumTime > ladleBoostProgress.maxValue) sumTime = ladleBoostProgress.maxValue;

                        EventManager.LadleBoostUsedEvent(GameData.GameData.LadleMultiplier * ladleBoost, sumTime);
                        ladleBoostTimer.ResetTimer(sumTime, () =>
                        {
                            GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier / ladleBoost);
                            GameData.GameData.LadleMultiplierBoost = 1;
                        });
                    }
                    else
                    {
                        GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier * ladleBoost);
                        GameData.GameData.LadleMultiplierBoost = (float)ladleBoost;
                        EventManager.LadleBoostUsedEvent(GameData.GameData.LadleMultiplier * ladleBoost, ladleBoostTime);
                        ladleBoostTimer.ResetTimer(ladleBoostTime, () =>
                        {
                            GameData.GameData.LadleMultiplier = (float)(GameData.GameData.LadleMultiplier / ladleBoost);
                            GameData.GameData.LadleMultiplierBoost = 1;
                        });
                    }
                });
            }
            // }
        }

        public void GoToShopButtonPress() => goToShopButton.onClick.Invoke();
        public void GoToMineButtonPress() => goToMineButton.onClick.Invoke();

        public void BuyGoldPack1()
        {
            PurchaseItem(shopItems[0], () =>
            {
                string gold = drillFormatter.MoneyKFormatter((long)(smalPackGameTime * GameData.GameData.CPS));
                if (actualSuccessAction != null)
                {
                    pickUpChestAlert.OpenChestWithGold(ChestType.GoldPackSmall, false, gold, () =>
                    {
                        GoToMineButtonPress();
                        storageController.AddGold(smalPackGameTime);
                        actualSuccessAction.Invoke();
                        actualSuccessAction = null;
                    }, () => { });
                }
                else
                {
                    pickUpChestAlert.OpenChestWithGold(ChestType.GoldPackSmall, false, gold, () =>
                    {
                        GoToMineButtonPress();
                        storageController.AddGold(smalPackGameTime);
                    }, () => { });
                }
            }, actualFailAction);
        }

        public void BuyGoldPack2()
        {
            PurchaseItem(shopItems[1], () =>
           {
               string gold = drillFormatter.MoneyKFormatter((long)(mediumPackGameTime * GameData.GameData.CPS));

               if (actualSuccessAction != null)
               {
                   pickUpChestAlert.OpenChestWithGold(ChestType.GoldPackMedium, false, gold, () =>
                   {
                       GoToMineButtonPress();
                       storageController.AddGold(mediumPackGameTime);
                       actualSuccessAction.Invoke();
                       actualSuccessAction = null;
                   }, () => { });
               }
               else
               {
                   pickUpChestAlert.OpenChestWithGold(ChestType.GoldPackMedium, false, gold, () =>
                   {
                       GoToMineButtonPress();
                       storageController.AddGold(mediumPackGameTime);
                   }, () => { });
               }

           }, actualFailAction);
        }

        public void BuyGoldPack3()
        {
            PurchaseItem(shopItems[2], () =>
           {
               string gold = drillFormatter.MoneyKFormatter((long)(bigPackGameTime * GameData.GameData.CPS));

               if (actualSuccessAction != null)
               {
                   pickUpChestAlert.OpenChestWithGold(ChestType.GoldPackBig, false, gold, () =>
                   {
                       GoToMineButtonPress();
                       storageController.AddGold(bigPackGameTime);
                       actualSuccessAction.Invoke();
                       actualSuccessAction = null;
                   }, () => { });
               }
               else
               {
                   pickUpChestAlert.OpenChestWithGold(ChestType.GoldPackBig, false, gold, () =>
                   {
                       GoToMineButtonPress();
                       storageController.AddGold(bigPackGameTime);
                   }, () => { });
               }
           }, actualFailAction);
        }

        public void RouletteChest()
        {
            if (chestIsReady)
            {
                advertising.ShowRewardedAdvertWithCallBack(() =>
                {
                    GoToMineButtonPress();
                    chestIsReady = false;
                    chestDelayTimer.SetTimer(chestDelay, () => chestIsReady = true);
                    EventManager.PickUpRouletteEvent();
                });
            }
        }

        [SerializeField] private Text engineLvlBoost;
        [SerializeField] private Text boerLvlBoost;
        [SerializeField] private Text ladlesLvlBoost;
        [SerializeField] private Text storageLvlBoost;

        public void SetLvlUpTexts()
        {
            var detailPackData = gameConfigurationManager.shopData.shopContent[1].content.detailLvls;
            string lvl = " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);
            engineLvlBoost.text = "+" + detailPackData.engineLvl + lvl;
            boerLvlBoost.text = "+" + detailPackData.engineLvl + lvl;
            ladlesLvlBoost.text = "+" + detailPackData.engineLvl + lvl;
            storageLvlBoost.text = "+" + detailPackData.engineLvl + lvl;
        }

        public void BuyDetailChest()
        {
            PurchaseItem(shopItems[3], () =>
                      {
                          var detailPackData = gameConfigurationManager.shopData.shopContent[1].content.detailLvls;
                          int[] detailsLvl = new int[] { detailPackData.engineLvl, detailPackData.boerLvl, detailPackData.laldleLvl, detailPackData.storageLvl };
                          pickUpChestAlert.OpenChestWithFullDetails(detailsLvl, () =>
                          {
                              EventManager.BuyDetailPackEvent(detailsLvl);
                              GoToMineButtonPress();
                          });
                      }, actualFailAction);
        }

        public void Subscribe()
        {
            if (!subscribe)
            {
                PurchaseItem(shopItems[4], () =>
               {
                   GoToMineButtonPress();
                   subscribeStateText.text = DigIsBigLocalizator.GetLocalizationString("SHOP_ACTIVE", Modificator.ToUpper);
                   subscribe = true;
                   EventManager.SubscribeEvent(subscribe);
               }, actualFailAction);
            }
        }

        public void CheckSubscribe(SQBAShopItem shopItem)
        {
            if (shopItem.id == shopItems[4].id)
            {
                subscribeStateText.text = DigIsBigLocalizator.GetLocalizationString("SHOP_ACTIVE", Modificator.ToUpper);
                subscribe = true;
                EventManager.SubscribeEvent(subscribe);
            }
        }

        public void RestorePurchase()
        {
            saleLoader.SetActive(true);
            shopManager.RestorePurchases(
                () =>
                {
                    Debug.Log("RESTORE PURCHASE");
                    saleLoader.SetActive(false);
                },
                (a, b) =>
                {
                    Debug.Log("RESTORE FAILD");
                    saleLoader.SetActive(false);
                }
            );
        }

        public void TermsOfUse() => Application.OpenURL("https://blackbears.mobi/legal/us/terms");

        public void PrivacyPolicy() => Application.OpenURL("https://blackbears.mobi/legal/us/privacy");

        public void OnActiveSubscriptionsCome(List<string> subscriptions)
        {
            if (subscriptions != null)
            {
                foreach (var n in subscriptions)
                {
                    if (shopManager.FindShopItemByIapName(n).id == shopItems[4].id)
                    {
                        subscribeStateText.text = DigIsBigLocalizator.GetLocalizationString("SHOP_ACTIVE", Modificator.ToUpper);
                        subscribe = true;
                        EventManager.SubscribeEvent(subscribe);
                    }
                }
            }
        }

        public void SubscribeIsEnd()
        {
            subscribeStateText.text = DigIsBigLocalizator.GetLocalizationString("SHOP_ON", Modificator.ToUpper);
            subscribe = false;
            EventManager.SubscribeEvent(subscribe);
        }
        #endregion
    }
}
