﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Tutorial
{
    public class LootTremorer : MonoBehaviour
    {
        [SerializeField] private Vector2 xTremor = new Vector2(-5, 5);
        [SerializeField] private Vector2 yTremor = new Vector2(-5, 5);
        [SerializeField] private float speed = 100;
        [SerializeField] private float scaleSpeed = 0.5f;
        [SerializeField] private float time = 0.5f;

        private Vector3 startPosition = new Vector3(0, 0, 0);

        private void Start()
        {
            // startPosition = transform.position;
            StartCoroutine(Scale());
        }

        private IEnumerator Move()
        {
            float t;
            Vector3 moveVector;
            while (true)
            {
                t = time / 2;
                moveVector = new Vector3(Random.Range(xTremor.x, xTremor.y), Random.Range(yTremor.x, yTremor.y), 0);
                while (t > 0)
                {
                    yield return null;
                    t -= Time.deltaTime;
                    transform.Translate(moveVector * Time.deltaTime);
                }
                t = time;
                while (t > 0)
                {
                    yield return null;
                    t -= Time.deltaTime;
                    transform.Translate(-moveVector * Time.deltaTime);
                }
                t = time / 2;
                while (t > 0)
                {
                    yield return null;
                    t -= Time.deltaTime;
                    transform.Translate(moveVector * Time.deltaTime);
                }
            }
        }

        private IEnumerator Scale()
        {
            float t;
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(1, 3));
                t = time / 3;
                while (t > 0)
                {
                    yield return null;
                    t -= Time.deltaTime;
                    transform.localScale += new Vector3(scaleSpeed * 3 * Time.deltaTime, scaleSpeed * 3 * Time.deltaTime, 0);
                }

                t = time;
                while (t > 0)
                {
                    yield return null;
                    t -= Time.deltaTime;
                    transform.localScale += new Vector3(-scaleSpeed * Time.deltaTime, -scaleSpeed * Time.deltaTime, 0);
                }

                transform.localScale = new Vector3(1, 1, 1);
            }
        }
    }
}
