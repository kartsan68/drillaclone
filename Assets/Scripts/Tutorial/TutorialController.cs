﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Drop;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Tutorial
{
    public class TutorialController : MonoBehaviour
    {
        [SerializeField] private float redyAfkTime = 3;
        [SerializeField] private float engineTriggerLvl = 3;
        [SerializeField] private Vector2 raycastSize = new Vector2(100, 100);
        [SerializeField] private GameObject spawner;
        [SerializeField] private float upgradeMoveSpeed = 600;
        [SerializeField] private GameObject upgradeWindow;
        [SerializeField] private GameObject storageWindow;
        [SerializeField] private GameObject shopWindow;
        [SerializeField] private GameObject PhysicalTutorialHandPrefab;
        [SerializeField] private GameObject UITutorialHandPrefab;

        [SerializeField] private RectTransform[] upgradeDetailPosition;
        [SerializeField] private RectTransform storageButtonPosition;
        [SerializeField] private RectTransform upgradeButtonPosition;
        [SerializeField] private RectTransform chargeButtonPosition;
        [SerializeField] private RectTransform kickBeamPosition;
        [SerializeField] private RectTransform tapBar;

        private Camera cam;
        private bool tutorialIsActive = true;
        private bool tutorialAlredyWork = false;
        private int redyIndex = 0;
        private GameObject activeHand;
        private float delay = 0f;
        private bool beamState;
        private float upgradeDelay = 3f;
        private bool delayIsEnd = true;
        //0 - upgrade , 1 - storage , 2 - beamKick, 3 - power 4 - mineral
        private bool[] actual = new bool[5];


        private void Start()
        {
            SubscribeToEvents();
            cam = Camera.main;
            delay = upgradeDelay;
            StartCoroutine(TutorController());
        }

        private void Update()
        {
            delay += Time.deltaTime;
            UpgradeDelayCheck();
        }

        private void SubscribeToEvents()
        {
            EventManager.FullStorage += (perc) =>
            {
                if (perc == 100 && !upgradeWindow.activeSelf) actual[1] = true;
                else actual[1] = false;
            };
            EventManager.PowerState += (state) =>
            {
                if (actual[4] && !state && !upgradeWindow.activeSelf) actual[3] = true;
                if ((!actual[4] && state) || (actual[4] && state)) actual[3] = false;
            };
            EventManager.BeamSpawn += (a, b) => beamState = true;
            EventManager.BeamCrash += () => beamState = false;
            EventManager.UpgradeDetail += () =>
            {
                GameData.GameData.afkTime = 0;
                delay = 0;
            };
            EventManager.UpgradeModeCamera += (a) =>
            {
                GameData.GameData.afkTime = 0;
                delay = 0;
            };
        }

        private IEnumerator TutorController()
        {
            while (true)
            {
                yield return null;
                CheckAfkTime();
                if (!tutorialAlredyWork)
                {
                    for (int i = 0; i < actual.Length; i++)
                    {
                        if (actual[i])
                        {
                            tutorialAlredyWork = true;
                            switch (i)
                            {
                                case 0:
                                    StartCoroutine(UpgradeTutorial());
                                    break;
                                case 1:
                                    StartCoroutine(FullStorageTutorial());
                                    break;
                                case 2:
                                    StartCoroutine(BeamKickTutorial());
                                    break;
                                case 3:
                                    StartCoroutine(LowPowerTutorial());
                                    break;
                                case 4:
                                    StartCoroutine(ResourceClickTutorial());
                                    break;
                            }
                            break;
                        }
                    }
                }
            }
        }

        private IEnumerator FullStorageTutorial()
        {
            float time = 0;
            activeHand = Instantiate(UITutorialHandPrefab, storageButtonPosition.localPosition, Quaternion.identity, tapBar);
            activeHand.SetActive(false);

            while (actual[1] && !actual[0])
            {
                yield return null;

                if (time < 3)
                {
                    time += Time.deltaTime;
                }
                else
                {
                    if (storageWindow.activeSelf || shopWindow.activeSelf || upgradeWindow.activeSelf) activeHand.SetActive(false);
                    else
                    {
                        activeHand.transform.localPosition = storageButtonPosition.localPosition;
                        activeHand.SetActive(true);
                    }
                }
            }
            Destroy(activeHand);
            tutorialAlredyWork = false;
        }

        private IEnumerator UpgradeTutorial()
        {
            float time = 0;
            activeHand = Instantiate(UITutorialHandPrefab, upgradeButtonPosition.localPosition, Quaternion.identity, tapBar);
            activeHand.SetActive(false);

            while (actual[0] && delayIsEnd)
            {
                yield return null;

                if (time < upgradeDelay)
                {
                    time += Time.deltaTime;
                }
                else
                {
                    if (storageWindow.activeSelf || shopWindow.activeSelf) activeHand.SetActive(false);
                    else activeHand.SetActive(true);
                    if (upgradeWindow.activeSelf)
                    {
                        activeHand.transform.position = Vector3.MoveTowards(activeHand.transform.position,
                        new Vector3(upgradeDetailPosition[redyIndex].position.x, upgradeDetailPosition[redyIndex].position.y, 0), upgradeMoveSpeed * Time.deltaTime);
                    }
                    else
                    {
                        activeHand.transform.localPosition = upgradeButtonPosition.localPosition;
                    }
                }
            }
            Destroy(activeHand);
            tutorialAlredyWork = false;
        }

        private IEnumerator LowPowerTutorial()
        {
            float scaleCorrection = 1.73f;

            activeHand = Instantiate(UITutorialHandPrefab, chargeButtonPosition.position, Quaternion.identity, chargeButtonPosition);
            activeHand.transform.GetChild(0).localScale = new Vector3(scaleCorrection, scaleCorrection, 0);

            while (actual[3] && !actual[0] && !actual[1] && !beamState && GameData.GameData.afkTime >= redyAfkTime)
            {
                yield return null;
            }
            Destroy(activeHand);
            tutorialAlredyWork = false;
        }

        private IEnumerator BeamKickTutorial()
        {
            activeHand = Instantiate(UITutorialHandPrefab, kickBeamPosition.position, Quaternion.identity, kickBeamPosition);

            while (actual[2] && beamState && !actual[0] && !actual[1])
            {
                yield return null;
            }
            Destroy(activeHand);
            tutorialAlredyWork = false;
        }

        private IEnumerator ResourceClickTutorial()
        {
            bool searchElement = false;
            RaycastHit2D[] raycastHit = new RaycastHit2D[100];
            ContactFilter2D cf = new ContactFilter2D();
            Transform target = null;
            int elementFromTutor;

            while (actual[4] && !actual[0] && !actual[1] && !actual[3] && !beamState)
            {
                yield return null;
                if (!searchElement)
                {
                    raycastHit = new RaycastHit2D[100];
                    int elementsCount = Physics2D.BoxCast(cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2)), raycastSize,
                    0, cam.transform.forward, cf.NoFilter(), raycastHit);
                    if (elementsCount > 0)
                    {
                        elementFromTutor = Random.Range(0, elementsCount);
                        target = raycastHit[elementFromTutor].transform;
                        activeHand = Instantiate(PhysicalTutorialHandPrefab, target.position, Quaternion.identity);
                        searchElement = true;
                    }
                }
                else if (target == null)
                {
                    if (activeHand != null)
                        Destroy(activeHand);
                    searchElement = false;
                }
                else if (target.position.y >= 1500)
                {
                    // if (activeHand != null)
                    Destroy(activeHand);
                    searchElement = false;
                }
                else if (activeHand == null) searchElement = false;
                else if (target != null) activeHand.transform.position = target.position;
            }
            if (activeHand != null)
                Destroy(activeHand);

            tutorialAlredyWork = false;
        }

        private void CheckAfkTime()
        {
            if (GameData.GameData.afkTime >= redyAfkTime) actual[4] = true;
            else actual[4] = false;

            bool redy = false;
            for (int i = 0; i < GameData.GameData.redyUpDetails.Length; i++)
            {
                if (GameData.GameData.redyUpDetails[i] && delayIsEnd)
                {
                    redyIndex = i;
                    redy = true;
                    break;
                }
            }
            actual[0] = redy;

            if (actual[4] && beamState) actual[2] = true;
            else actual[2] = false;
        }

        private void UpgradeDelayCheck()
        {
            if (delay >= upgradeDelay && GameData.GameData.lvlDetails[0] > engineTriggerLvl) delayIsEnd = true;
            else if (GameData.GameData.lvlDetails[0] <= engineTriggerLvl)
            {
                if (delay >= redyAfkTime) delayIsEnd = true;
                else delayIsEnd = false;
            }
            else delayIsEnd = false;
        }
    }
}
