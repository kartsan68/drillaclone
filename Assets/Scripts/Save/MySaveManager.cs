using UnityEngine;
using BlackBears.GameCore.Features.Save;
using BlackBears.GameCore;
using SimpleJSON;
using BlackBears.DiggIsBig.Managers;
using BlackBears.DiggIsBig.Save;

namespace BlackBears.DiggIsBig
{
    public class MySaveManager : ISaveable
    {
        private SaveFeature saveFeature;
        private ISaveProvider saveManager;

        public JSONNode GetSave { get; protected set; }

        public MySaveManager()
        {
            saveFeature = BBGC.Features.GetFeature<SaveFeature>();
            saveManager = saveFeature.SaveManager;
            saveManager.SetSaveable(this);
        }

        public void ReadSave()
        {
            if (!saveManager.IsFirstLaunch)
            {
                GetSave = saveManager.GetSaveDataByKey("game").data;
                Save.SaveCreator.Load(GetSave);
            }
            else
                Save.SaveCreator.SetFirstStartTime(saveManager);
        }


        public GameCore.Features.Save.SaveData ToSaveData()
        {
            EventManager.SaveGameEvent();
            var json = SaveCreator.save;
            return new BlackBears.GameCore.Features.Save.SaveData("game", json);
        }
    }
}
