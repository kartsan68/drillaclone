using System.Collections.Generic;
using BlackBears.Chronometer;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Save;
using BlackBears.DiggIsBig.Managers;
using SimpleJSON;
using UnityEngine;

namespace BlackBears.DiggIsBig.Save
{
    public static class SaveCreator
    {
        private static int redySaveCount = 0;
        private static List<SaveData> dataForSave = new List<SaveData>();
        public static long saveTime = -1;
        private static bool downloadReady = false;

        public static JSONNode save { get; private set; }
        public static JSONNode resave { get; private set; }
        public static long FirstStartTime { get; private set; }

        public static void Save(string key, List<SaveData> saveDataList)
        {
            downloadReady = false;
            redySaveCount++;

            SaveData saveData = new SaveData(key, saveDataList);
            dataForSave.Add(saveData);

            if (redySaveCount == EventManager.GetSaveSubscribersCount())
            {
                JSONObject json = new JSONObject();

                saveTime = TimeModule.ActualTime;
                json["time"] = saveTime.ToString();
                json["version"] = 1;
                json[GameData.SaveKeys.firstStartTimeKey] = FirstStartTime;

                var array = new JSONArray();
                json.Add("save", array);

                foreach (var saveable in dataForSave)
                {
                    array.Add(saveable.data);
                }

                save = json;
                resave = json;

                redySaveCount = 0;
                dataForSave = new List<SaveData>();
                EventManager.SetNotificationsEvent();
            }
        }

        public static void Load(JSONNode readySave)
        {
            save = readySave;
            FirstStartTime = save != null ? save[GameData.SaveKeys.firstStartTimeKey].AsLong : TimeModule.ActualTime;
            downloadReady = true;
        }

        public static void SetFirstStartTime(ISaveProvider saveManager)
        {
            if (saveManager.IsFirstLaunch)
                FirstStartTime = TimeModule.ActualTime;
        }

        public static JSONNode GetReloadData(string key)
        {
            downloadReady = true;
            if (resave != null)
            {
                var data = resave["save"].AsArray;
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].HasKey(key))
                    {
                        return data[i][key];
                    }
                }
            }
            return new JSONObject();
        }

        public static JSONNode GetLoadData(string key)
        {
            if (downloadReady)
            {
                if (save != null)
                {
                    var data = save["save"].AsArray;
                    if (data != null)
                    {
                        for (int i = 0; i < data.Count; i++)
                        {
                            if (data[i].HasKey(key))
                            {
                                return data[i][key];
                            }
                        }
                    }
                }
            }
            else
                Debug.Log("Save not found!");
            return new JSONObject();
        }
    }
}