﻿using System.Collections.Generic;
using SimpleJSON;

namespace BlackBears.DiggIsBig.Save
{
    [System.Serializable]
    public class SaveData
    {
        public readonly string key;
        public readonly JSONNode data;

        public SaveData(string key, JSONNode data)
        {
            this.key = key;
            this.data = data;
        }

        public SaveData(string key, List<SaveData> data)
        {
            this.key = key;

            JSONObject json = new JSONObject();
            var array = new JSONArray();

            json.Add(key, array);

            foreach (var index in data)
            {
                array.Add(index.AsJSONNode());
            }

            this.data = json;
        }

        public JSONNode AsJSONNode()
        {
            JSONObject obj = new JSONObject();
            obj.Add(key, data);
            return obj;
        }
    }
}
