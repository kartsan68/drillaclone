﻿using BlackBears.GameCore;
using BlackBears.GameCore.Features.Rate;

namespace BlackBears.DiggIsBig.Managers
{
    public static class RateManager
    {
        public static void RateCall() => BBGC.Features.GetFeature<RateFeature>().TryToOpenRate();
    }
}
