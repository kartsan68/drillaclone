﻿using System;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Configuration.Data;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.GameElements.Elements;
using BlackBears.DiggIsBig.Drop;
using UnityEngine;
using UnityEngine.Events;
using BlackBears.DiggIsBig.GameElements.Location;

namespace BlackBears.DiggIsBig.Managers
{
    public class EventManager
    {
        public static event Action OpenClans;
        public static event Action<double> PickUpSecondBooster;
        public static event Action<double> ReloadTick;
        public static event Action<string> SendFB;
        public static event Action UpgradeDetail;
        public static event Action ApplicationReload;
        public static event Action<bool> PowerState;
        public static event Action SetNotifications;
        public static event Action ResetRewardedDelayTimer;
        public static event Action<double> GiveMeMoney;
        public static event Action<double> TakeMyMoney;
        public static event Action<int> SetActualPowerData;
        public static event Action<string, bool, string> SetBeamHealthInfo;
        public static event Action<double, UnityAction, UnityAction> NotEnoughtMoney;
        public static event Action<Location> NextLocation;
        public static event Action<bool> Subscribe;
        public static event Action<float> AnimationSpeedControl;
        public static event Action<float> AnimationStorageControl;
        public static event Action<bool> UpgradeState;
        public static event Action<bool> UpgradeModeCamera;
        public static event Action<bool> HideUIElements;
        public static event Action<bool> TopElemensIsMax;
        public static event Action<bool, bool> BeamSpawn;
        public static event Action PickUpRoulette;
        public static event Action MuteMusic;
        public static event Action<Sound> PlaySound;
        public static event Action HideStorageAlert;
        public static event Action PowerCharge;
        public static event Action<float> LadlesSwap;
        public static event Action<float, float> ClickBoostUsed;
        public static event Action<float, float> MagnetBoostUsed;
        public static event Action<double, double> LadleBoostUsed;
        public static event Action<float, float> WildFarmBoostUsed;
        public static event Action WildFarmBoostStop;
        public static event Action ClickBoostStop;
        public static event Action PickUpBarrel;
        public static event Action CantTouchRes;
        public static event Action BoxSpawn;
        public static event Action SpawnUniqRes;
        public static event Action BoosterSpawn;
        // public static event Action PickUpClickBooster;
        public static event Action<bool> RewardedAdvertiseStatus;
        public static event Action<Action> CallRewardedAdvertWithAction;
        public static event Action CallAdvDetail;
        public static event Action<Loot, bool> PickUpResBox;
        public static event Action CloseWinAdvBlock;
        public static event Action ClearChild;
        public static event Action SaveGame;
        public static event Action ColdStart;
        public static event Action TapOnBeam;
        public static event Action CallInterstitialAdvert;
        public static event Action CallRewardedAdvert;
        public static event Action UpgradeAnimationEnd;
        public static event Action<OfflineCalcData> LoadGame;
        public static event Action<OfflineCalcData> ReloadGame;
        public static event Action SpawnBigChest;
        public static event Action ChestSpawn;
        public static event Action<bool> DrillaStartStop;
        public static event Action RouletteIsCrash;
        public static event Action BeamCrash;
        public static event Action NewKmDeep;
        public static event Action GamePaused;
        public static event Action GameUnpaused;
        public static event Action AdsWatchingEnd;
        public static event Action CPSChanged;
        public static event Action<UniqRes> PickUpUniq;
        public static event Action<int> FullStorage;
        public static event Action<int> UpStorage;
        public static event Action<int> BoerSwap;
        public static event Action<int> LocationSwap;
        public static event Action<int> RedyUpdatesCount;

        public static event Action<int[]> BuyDetailPack;
        public static event Action<int> SetLoadSpeedEffect;

        public static event Action<float> MoveThroughTheBeam;
        public static event Action<double> BeamDistance;
        public static event Action StopSpeedBoostEffect;

        public static event Action<Loot> PickUpChest;

        public static event Action<Loot, float> PickUpResources;

        public static event Action<RouletteWinElements> RouletteWin;

        public static event Action<GameBoosterType, CountdownTimer> SetTimer;

        public static event Action<float[]> DeepResourceChange;

        public static int GetSaveSubscribersCount() => SaveGame.GetInvocationList().Length;

        #region PickUpZone

        public static void PickUpUniqEvent(UniqRes uniqRes) => PickUpUniq?.Invoke(uniqRes);
        public static void PickUpBarrelEvent() => PickUpBarrel?.Invoke();

        public static void GiveMeMoneyEvent(double money) => GiveMeMoney?.Invoke(money);
        public static void TakeMyMoneyEvent(double money) => TakeMyMoney?.Invoke(money);
        // public static void PickUpClickBoosterEvent() => PickUpClickBooster?.Invoke();

        public static void PickUpResourcesEvent(Loot res) => PickUpResources?.Invoke(res, GameData.GameData.LadleMultiplier);

        public static void PickUpChestsEvent(Loot chest)
        {
            switch (chest.GetComponent<Chest>().GetChestType)
            {
                case ChestType.SmallChest:
                    PickUpChest?.Invoke(chest);
                    break;
                case ChestType.MediumChest:
                    PickUpChest?.Invoke(chest);
                    break;
                case ChestType.BigChest:
                    PickUpChest?.Invoke(chest);
                    break;
                case ChestType.ChestWithRoulette:
                    PickUpRoulette?.Invoke();
                    break;
            }
        }

        public static void PickUpRouletteEvent() => PickUpRoulette?.Invoke();

        public static void PickUpResBoxEvent(Loot resBox, bool isPickUp) => PickUpResBox?.Invoke(resBox, isPickUp);


        #endregion

        #region SpawnZone

        public static void BoosterSpawnEvent() => BoosterSpawn?.Invoke();
        public static void BeamSpawnEvent(bool isBig, bool isLoad) => BeamSpawn?.Invoke(isBig, isLoad);
        public static void BeamCrashEvent() => BeamCrash?.Invoke();
        public static void ChestSpawnEvent() => ChestSpawn?.Invoke();
        public static void SpawnBigChestEvent() => SpawnBigChest?.Invoke();
        public static void RouletteIsCrashEvent() => RouletteIsCrash?.Invoke();
        public static void NextLocationEvent(Location location) => NextLocation?.Invoke(location);
        public static void BoxSpawnEvent() => BoxSpawn?.Invoke();
        public static void SpawnUniqResEvent() => SpawnUniqRes?.Invoke();

        #endregion

        #region  Control
        public static void OpenClansEvent() => OpenClans?.Invoke();
        public static void PickUpSecondBoosterEvent(double tick) => PickUpSecondBooster?.Invoke(tick);
        public static void ReloadTickEvent(double tick) => ReloadTick?.Invoke(tick);
        public static void ColdStartEvent() => ColdStart?.Invoke();
        public static void UpgradeDetailEvent() => UpgradeDetail?.Invoke();
        public static void PowerStateEvent(bool state) => PowerState?.Invoke(state);
        public static void PlaySoundEvent(Sound sound) => PlaySound?.Invoke(sound);
        public static void MuteMusicEvent() => MuteMusic?.Invoke();
        public static void SendFBEvent(string token) => SendFB?.Invoke(token);
        public static void SetNotificationsEvent() => SetNotifications?.Invoke();
        public static void PowerChargeEvent() => PowerCharge?.Invoke();
        public static void HideStorageAlertEvent() => HideStorageAlert?.Invoke();
        public static void CantTouchResEvent() => CantTouchRes?.Invoke();
        public static void MoveThroughTheBeamEvent(float step) => MoveThroughTheBeam?.Invoke(step);
        public static void DrillaStartStopEvent(bool isStop) => DrillaStartStop?.Invoke(isStop);
        public static void LadlesSwapEvent(float ladleMultipler) => LadlesSwap?.Invoke(ladleMultipler);
        public static void RewardedAdvertiseStatusEvent(bool state) => RewardedAdvertiseStatus?.Invoke(state);
        public static void ClickBoostUsedEvent(float boost, float time) => ClickBoostUsed?.Invoke(boost, time);
        public static void MagnetBoostUsedEvent(float boost, float time) => MagnetBoostUsed?.Invoke(boost, time);
        public static void LadleBoostUsedEvent(double boost, double time) => LadleBoostUsed?.Invoke(boost, time);
        public static void WildFarmBoostUsedEvent(float boost, float time) => WildFarmBoostUsed?.Invoke(boost, time);
        public static void WildFarmBoostStopEvent() => WildFarmBoostStop?.Invoke();
        public static void TapOnBeamEvent() => TapOnBeam?.Invoke();
        public static void ClickBoostStopEvent() => ClickBoostStop?.Invoke();
        public static void AnimationSpeedControlEvent(float multiplier) => AnimationSpeedControl?.Invoke(multiplier);
        public static void AnimationStorageControlEvent(float multiplier) => AnimationStorageControl?.Invoke(multiplier);
        public static void UpgradeStateEvent(bool state) => UpgradeState?.Invoke(state);
        public static void SetActualPowerDataEvent(int index) => SetActualPowerData?.Invoke(index);
        public static void CallAdvDetailEvent() => CallAdvDetail?.Invoke();
        public static void SetLoadSpeedEffectEvent(int effect) => SetLoadSpeedEffect?.Invoke(effect);
        public static void StopSpeedBoostEffectEvent() => StopSpeedBoostEffect?.Invoke();
        public static void GamePausedEvent() => GamePaused?.Invoke();
        public static void SaveGameEvent() => SaveGame?.Invoke();
        public static void CallInterstitialAdvertEvent() => CallInterstitialAdvert?.Invoke();
        public static void CallRewardedAdvertEvent() => CallRewardedAdvert?.Invoke();
        public static void CallRewardedAdvertWithActionEvent(Action action) => CallRewardedAdvertWithAction?.Invoke(action);
        public static void CloseWinAdvBlockEvent() => CloseWinAdvBlock?.Invoke();
        public static void LoadGameEvent(OfflineCalcData ocd) => LoadGame?.Invoke(ocd);
        public static void ReloadGameEvent(OfflineCalcData ocd) => ReloadGame?.Invoke(ocd);
        public static void GameUnpausedEvent() => GameUnpaused?.Invoke();
        public static void SubscribeEvent(bool sub) => Subscribe?.Invoke(sub);
        public static void TopElemensIsMaxEvent(bool state) => TopElemensIsMax?.Invoke(state);
        public static void HideUIElementsEvent(bool isVisible) => HideUIElements?.Invoke(isVisible);
        public static void AdsWatchingEndEvent() => AdsWatchingEnd?.Invoke();
        public static void UpStorageEvent(int lvl) => UpStorage?.Invoke(lvl);
        public static void SetBeamHealthInfoEvent(string info, bool isBig, string secondInfo = "") => SetBeamHealthInfo?.Invoke(info, isBig, secondInfo);
        public static void NotEnoughtMoneyEvent(double money, UnityAction onSuccess, UnityAction onFail) => NotEnoughtMoney?.Invoke(money, onSuccess, onFail);
        public static void NewKmDeepEvent() => NewKmDeep?.Invoke();
        public static void CPSChangedEvent() => CPSChanged?.Invoke();
        public static void ClearChildEvent() => ClearChild?.Invoke();
        public static void UpgradeAnimationEndEvent() => UpgradeAnimationEnd?.Invoke();
        public static void BoerSwapEvent(int boerLvl) => BoerSwap?.Invoke(boerLvl);
        public static void LocationSwapEvent(int locationLvl) => LocationSwap?.Invoke(locationLvl);
        public static void FullStorageEvent(int perc) => FullStorage?.Invoke(perc);
        public static void RedyUpdatesCountEvent(int count) => RedyUpdatesCount?.Invoke(count);
        public static void SetTimerEvent(GameBoosterType gameBoosterType, CountdownTimer timer) => SetTimer?.Invoke(gameBoosterType, timer);

        #endregion

        #region  Distance
        public static void BeamDistanceEvent(double data) => BeamDistance?.Invoke(data);

        public static void DeepResourceChangeEvent(float[] resourcesCount) => DeepResourceChange?.Invoke(resourcesCount);
        #endregion

        #region OtherGameEvents
        public static void BuyDetailPackEvent(int[] arr) => BuyDetailPack?.Invoke(arr);
        public static void RouletteWinEvent(RouletteWinElements element) => RouletteWin?.Invoke(element);
        public static void UpgradeModeCameraEvent(bool mode) => UpgradeModeCamera?.Invoke(mode);

        public static void ApplicationReloadEvent() => ApplicationReload?.Invoke();


        #endregion
    }
}
