﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.GameCore;
using BlackBears.GameCore.Features;
using BlackBears.GameCore.Features.LocalNotifications;
using UnityEngine;

namespace BlackBears.DiggIsBig.Managers
{
    public static class NotificationsManager
    {
        public static bool Available => BBGC.Features.LocalNotification().Available;
        public static void SetNotification(string title, string body, double time)
        {
            var notificationFeature = BBGC.Features.LocalNotification();
            bool redy = true;
#if UNITY_IOS
            if (!notificationFeature.Available) redy = false;
#endif
            if (redy)
            {
                var notification = notificationFeature.CreateNotification();
                notification.Title = title;
                notification.Body = body;
#if UNITY_ANDROID
                notification.SmallIcon = "icon_0s";
#endif
                notification.DeliveryTime = DateTime.Now.AddSeconds(time);
                notificationFeature.ScheduleNotification(notification);
            }
        }

        public static void EnableNotification(bool state)
        {
#if UNITY_IOS
            var notificationFeature = BBGC.Features.LocalNotification();
            notificationFeature.EnableLocalNotification(state);
#endif
        }

        public static void ClearAllNotif()
        {
            bool redy = true;
            var notificationFeature = BBGC.Features.LocalNotification();
#if UNITY_IOS
            if (!notificationFeature.Available) redy = false;
#endif
            if (redy)
                notificationFeature.CancelAllNotifications();
        }
    }
}
