﻿using System.Collections.Generic;
using BlackBears.DiggIsBig.Cave.CaveElements;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.GameElements.Elements;
using UnityEngine;

namespace BlackBears.DiggIsBig.Managers
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private float backgroundVolume = 1f;
        [SerializeField] private SoundAudioClip[] audioClips;
        [SerializeField] private TapSoundObject tapSoundObject;


        private AudioSource backgroundMusic;
        private List<AudioSource> soundObjects;


        private void Start()
        {
            EventManager.MuteMusic += MuteMusic;
            EventManager.PlaySound += PlaySound;
            soundObjects = new List<AudioSource>();
        }

        private void MuteMusic()
        {
            if (GameData.GameSettingsData.music) backgroundMusic.volume = backgroundVolume;
            else backgroundMusic.volume = 0.0f;
        }

        public void PlaySound(Sound sound)
        {
            switch (sound)
            {
                case Sound.BackgroundMusic:
                    if (backgroundMusic == null)
                    {
                        backgroundMusic = gameObject.AddComponent<AudioSource>();
                        backgroundMusic.clip = audioClips[0].audioClip;
                        backgroundMusic.playOnAwake = true;
                        backgroundMusic.loop = true;

                        if (GameData.GameSettingsData.music) backgroundMusic.volume = backgroundVolume;
                        else backgroundMusic.volume = 0.0f;

                        backgroundMusic.Play();

                    }
                    break;
                case Sound.BarrelExplosion:
                    break;
                case Sound.BoxExplosion:
                    break;
                case Sound.PickUpResource:
                    break;
                case Sound.TapOnBox:
                    break;
                case Sound.TapOnChest:
                    break;
                case Sound.UsualTap:
                    if (GameSettingsData.sound)
                    {
                        var tap = Instantiate(tapSoundObject, transform.position, Quaternion.identity);
                        tap.CreateSound(audioClips[1].audioClip, 0.3f);
                    }
                    break;
            }
        }
    }
}
