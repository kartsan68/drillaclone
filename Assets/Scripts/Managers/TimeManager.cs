﻿using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Save;
using UnityEngine;

namespace BlackBears.DiggIsBig.Managers
{
    public class TimeManager : MonoBehaviour
    {
        [SerializeField] private CountdownTimer countdownTimerPrefab;
        [SerializeField] private CountupTimer countupTimerPrefab;
        private List<CountdownTimer> timersList = new List<CountdownTimer>();

        private static TimeManager instance;

        private void Awake()
        {
            instance = this;
        }

        public static CountdownTimer GetEmptyCountdownTimer(GameObject parent = null, TimerType timerType = TimerType.DefaultTimer)
        {
            CountdownTimer cd;
            if (parent == null) cd = Instantiate(instance.countdownTimerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            else cd = Instantiate(instance.countdownTimerPrefab, new Vector3(0, 0, 0), Quaternion.identity, parent.transform);

            cd.timerType = timerType;
            cd.name = timerType.ToString();
            instance.timersList.Add(cd);
            return cd;
        }

        public static CountupTimer GetEmptyCountupTimer(GameObject parent) => Instantiate(instance.countupTimerPrefab, new Vector3(), Quaternion.identity, parent.transform);
    }
}
