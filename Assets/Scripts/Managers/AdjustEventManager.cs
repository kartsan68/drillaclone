﻿using System.Collections;
using System.Collections.Generic;
using com.adjust.sdk;
using UnityEngine;

namespace BlackBears.DiggIsBig.Managers
{
    public static class AdjustEventManager
    {
        private static string IT_start = "34jrbv";
        private static string IT_finish = "h32vw0";
        private static string IT_click = "6lx549";
        private static string RW_start = "eblre0";
        private static string RW_click = "i4wog1";
        private static string RW_finish = "xu6jx1";
        private static string revenue_all = "q87te1";
        private static string unique_pu = "2n0ssd";
        private static string lvl_2player = "vmbgcc";
        private static string lvl_3player = "97qde3";
        private static string lvl_10player = "vvkg75";
        private static string lvl_15player = "18gytu";
        private static string lvl_2engine = "fbfojy";
        private static string lvl_3engine = "vt97pv";
        private static string lvl_6engine = "4l5lok";
        private static string lvl_9engine = "j6jl10";
        private static string lvl_15engine = "rb44li";

        public static void InterstitialStartEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(IT_start);
            Adjust.trackEvent(adjustEvent);
        }

        public static void InterstitialFinishEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(IT_finish);
            Adjust.trackEvent(adjustEvent);
        }

        public static void InterstitialClickEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(IT_click);
            Adjust.trackEvent(adjustEvent);
        }

        public static void RewardedStartEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(RW_start);
            Adjust.trackEvent(adjustEvent);
        }

        public static void RewarderFinishEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(RW_finish);
            Adjust.trackEvent(adjustEvent);
        }

        public static void RewardedClickEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(RW_click);
            Adjust.trackEvent(adjustEvent);
        }

        public static void RevenueAllEvent(double amount, string currency)
        {
            AdjustEvent adjustEvent = new AdjustEvent(revenue_all);
            adjustEvent.setRevenue(amount, currency);
            Adjust.trackEvent(adjustEvent);
        }

        public static void UniquePlayerEvent()
        {
            AdjustEvent adjustEvent = new AdjustEvent(unique_pu);
            Adjust.trackEvent(adjustEvent);
        }

        public static void PlayerLvlEvent(int lvl)
        {
            string workToken = "null";
            switch (lvl)
            {
                case 2:
                    workToken = lvl_2player;
                    break;
                case 3:
                    workToken = lvl_3player;
                    break;
                case 10:
                    workToken = lvl_10player;
                    break;
                case 15:
                    workToken = lvl_15player;
                    break;
            }
            if (!workToken.Equals("null"))
            {
                AdjustEvent adjustEvent = new AdjustEvent(workToken);
                Adjust.trackEvent(adjustEvent);
            }
        }

        public static void EngineLvlEvent(int lvl)
        {
            string workToken = "null";
            switch (lvl)
            {
                case 2:
                    workToken = lvl_2engine;
                    break;
                case 3:
                    workToken = lvl_3engine;
                    break;
                case 6:
                    workToken = lvl_6engine;
                    break;
                case 9:
                    workToken = lvl_9engine;
                    break;
                case 15:
                    workToken = lvl_15engine;
                    break;
            }
            if (!workToken.Equals("null"))
            {
                AdjustEvent adjustEvent = new AdjustEvent(workToken);
                Adjust.trackEvent(adjustEvent);
            }
        }
    }
}