﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Common.GameProcessControl;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Common
{
    public class SpawnResPoint : MonoBehaviour
    {
        [SerializeField] private ResTapReact resTapReact;
        [SerializeField] private float lootCount = 5;

        private List<Drop.Loot> lootList;
        private bool paused = false;

        private void Awake()
        {
            EventManager.GamePaused += () => paused = true;
            EventManager.GameUnpaused += () => paused = false;
        }
        private void Start()
        {
            StartCoroutine(TapReaction());
        }
        public void SetLootList(List<Drop.Loot> list)
        {
            lootList = list;
        }

        private IEnumerator TapReaction()
        {
            while (true)
            {
                yield return new WaitForSeconds(5f);

                while (GameData.GameData.storageIsFull || paused) yield return null;

                for (int i = 0; i < lootCount; i++)
                {
                    yield return new WaitForSeconds(0.01f);
                    ResTapReact fake = Instantiate(resTapReact, new Vector3(transform.position.x, transform.position.y, -9), Quaternion.identity, this.transform);
                    fake.SetImage(lootList[Random.Range(0, lootList.Count)].spriteRenderer.sprite);
                    fake.gameObject.SetActive(true);
                    fake.StartAnimation();
                }
            }
        }
    }
}
