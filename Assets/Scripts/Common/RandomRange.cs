﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Common.RandomRange
{
    public struct IntRange
    {
        public int Index;
        public float Weight;

        public IntRange(int index, float weight)
        {
            Index = index;
            Weight = weight;
        }
    }

    public static class RandomRange
    {
        public static int Range(params IntRange[] ranges)
        {
            if (ranges.Length == 0) throw new System.ArgumentException("At least one range must be included.");
            if (ranges.Length == 1) return ranges[0].Index;

            float total = 0f;
            for (int i = 0; i < ranges.Length; i++) total += ranges[i].Weight;

            float r = Random.value;
            float s = 0f;

            int cnt = ranges.Length - 1;
            for (int i = 0; i < cnt; i++)
            {
                s += ranges[i].Weight / total;
                if (s >= r)
                {
                    return ranges[i].Index;
                }
            }

            return ranges[cnt].Index;
        }
    }
}
