﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class UnderUITargeter : MonoBehaviour
    {
        [SerializeField] private Camera cam;
        [SerializeField] private RectTransform targetTransform;

        private Vector2 screenToWorldPosition;

        private void Update()
        {
            screenToWorldPosition = cam.ScreenToWorldPoint(targetTransform.transform.position);
            transform.position = screenToWorldPosition;
        }
    }
}
