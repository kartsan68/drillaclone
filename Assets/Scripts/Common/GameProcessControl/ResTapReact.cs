﻿using System.Collections;
using BlackBears.DiggIsBig.Controllers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class ResTapReact : MonoBehaviour
    {
        [SerializeField] private Transform storageTarget;
        [SerializeField] private Transform goldTarget;
        [SerializeField] private Confines confines = new Confines(-20, 45, -30, 30);
        [SerializeField] private float moveUpSpeedAfterTap = 150;
        [SerializeField] private float moveToTargetSpeedAfterTap = 700;
        [SerializeField] private float toStorDelay = 0.2f;

        private Drop.Loot parent;
        private Transform actualTarget;
        private bool isRes;



        public void SetImage(Sprite sprite) => gameObject.GetComponent<SpriteRenderer>().sprite = sprite;

        public void StartAnimation(Drop.Loot parent, bool isRes = true)
        {
            this.isRes = isRes;
            if (isRes) actualTarget = storageTarget;
            else actualTarget = goldTarget;
            this.parent = parent;
            StartCoroutine(MoveUp(confines, moveUpSpeedAfterTap, moveToTargetSpeedAfterTap));
        }

        public void StartAnimation()
        {
            actualTarget = storageTarget;
            StartCoroutine(MoveUp(confines, moveUpSpeedAfterTap, moveToTargetSpeedAfterTap));
        }

        private bool alredyBounced = false;
        private int tick = 10;
        private bool MoveToPosition(float speed, Vector3 targetPosition)
        {
            float step = speed * Time.deltaTime;

            if (alredyBounced)
            {
                tick--;
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, step / 4);
                if (tick == 0) return true;
                return Vector3.Distance(transform.position, targetPosition) < 0.001f;
            }
            else
            {
                Vector3 bounsVector = new Vector3(0, step, 0);
                transform.position = Vector3.MoveTowards(transform.position, targetPosition + bounsVector, step);
                if (Vector3.Distance(transform.position, targetPosition + bounsVector) < 0.001f) alredyBounced = true;
                return false;
            }

        }

        private bool MoveToEndPosition(float speed, Vector3 targetPosition)
        {
            float step = speed * Time.deltaTime;

            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
            return Vector3.Distance(transform.position, targetPosition) < 0.001f;

        }

        private IEnumerator MoveUp(Confines confines, float speedUp, float speedToTarget)
        {
            Vector3 positionUp;
            if (isRes) positionUp = new Vector3(transform.position.x + Random.Range(confines.xMin, confines.xMax), transform.position.y + Random.Range(confines.yMin, confines.yMax), 0);
            else positionUp = new Vector3(transform.position.x + Random.Range(confines.xMin, confines.xMax), transform.position.y - Random.Range(confines.yMin, confines.yMax), 0);
            while (true)
            {
                yield return null;
                if (MoveToPosition(speedUp, positionUp))
                {
                    yield return new WaitForSeconds(toStorDelay);
                    StartCoroutine(MoveToTarget(speedToTarget));
                    break;
                }
            }
        }

        private IEnumerator MoveToTarget(float speed)
        {
            alredyBounced = false;
            while (true)
            {
                yield return null;

                if (MoveToEndPosition(speed, actualTarget.position)) break;
            }

            if (parent != null)
            {
                parent.ChildDestroy();
                if (parent.CheckAllChild() <= 0) Destroy(parent.gameObject);
            }


            Destroy(this.gameObject);
        }
    }
}