﻿using System.Collections;
using BlackBears.DiggIsBig.Controllers;
using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class ResTapTextReact : MonoBehaviour
    {
        private GameController gameController;
        private bool start = false;
        private TextMesh textMesh;

        public void CreateCountResText(GameController gameController, double countOfRes, Color color)
        {
            this.gameController = gameController;
            start = true;
            textMesh = this.GetComponent<TextMesh>();
            textMesh.text = countOfRes.ToString();
            textMesh.color = color;
            StartCoroutine(Dissolution());
        }

        private void Update()
        {
            if (start)
            {
                transform.position += new Vector3(0, Time.deltaTime * gameController.GetCaveSpeed, 0);
            }
        }

        private IEnumerator Dissolution()
        {
            float dissolate = 0.035f;

            Color color = textMesh.color;
            while (textMesh.color.a > 0)
            {
                yield return null;
                color.a -= dissolate;
                textMesh.color = color;
            }
            Destroy(this);
        }
    }
}
