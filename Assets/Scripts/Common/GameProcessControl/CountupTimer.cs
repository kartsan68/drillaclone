﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class CountupTimer : MonoBehaviour
    {
        private double time;
        private bool startTimer;
        private Dictionary<int, UnityAction> checkPoint;
        private double oldTime;
        private UnityAction action;

        private void Awake()
        {
            checkPoint = new Dictionary<int, UnityAction>();
        }

        private void Update()
        {
            if (startTimer)
            {
                oldTime = time;
                time += Time.deltaTime;
                if ((long)oldTime < (long)time)
                {
                    action.Invoke();
                    CheckPoints();
                }

                if (time == 99999999) time = 0;
            }
        }

        private void CheckPoints()
        {
            long time = (long)this.time;

            foreach (var item in checkPoint)
            {
                if (time == item.Key)
                {
                    item.Value.Invoke();
                }
            }
        }

        public void StartTimer(UnityAction action)
        {
            this.action = action;
            startTimer = true;
        }
        public void RestartTimer() => time = 0;
        public double GetTime() => time;
        public void SetTime(long time) => this.time = time;
        public void AddCheckPoint(int time, UnityAction action) => checkPoint.Add(time, action);
    }
}
