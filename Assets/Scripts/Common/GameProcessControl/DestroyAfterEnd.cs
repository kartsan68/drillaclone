﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class DestroyAfterEnd : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) => Destroy(animator.gameObject, stateInfo.length);
    }
}
