﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class StartAnimationWhenActive : MonoBehaviour
    {
        private void OnEnable() => gameObject.GetComponent<Animator>().SetTrigger("PickUp");
    }
}
