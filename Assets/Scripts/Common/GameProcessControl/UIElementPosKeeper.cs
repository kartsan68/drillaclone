﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class UIElementPosKeeper : MonoBehaviour
    {
        [SerializeField] private bool isBeam;
        [SerializeField] private GameObject target;
        [SerializeField] private float XOffset;
        [SerializeField] private float YOffset;

        void Update()
        {
            if (gameObject.activeSelf)
            {
                if (isBeam) transform.position = new Vector3(Screen.width / 2 + XOffset, Camera.main.WorldToScreenPoint(target.transform.localPosition).y + YOffset, 0);
                else transform.position = new Vector3(Camera.main.WorldToScreenPoint(target.transform.localPosition).x + XOffset, Camera.main.WorldToScreenPoint(target.transform.localPosition).y + YOffset, 0);
            }
        }
    }
}
