﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public static class LocalizeAndConfigPas
    {
        static private string configuration;
        public static void SetConfigurationFile(string conf) => configuration = conf;
        public static string GetConfigurationFile() => configuration;

    }
}