﻿using System;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.Common.GameProcessControl
{
    public class CountdownTimer : MonoBehaviour
    {
        private bool startTimer;
        private double times;
        private Action methodForCall;
        private Action<double> callbackPerSecondMethod;
        private float multiplier;
        private bool paused;
        private bool timerStop;
        private bool isBoost;
        private bool isBeam;
        private bool storageIsFull;
        private bool isCallbackTimer;
        private bool isUnstopableTimer;
        private Slider timerSlider;

        public TimerType timerType;

        public void BoostMultiplier(float multiplier) => this.multiplier = multiplier;

        public void BoostMultiplierReset() => multiplier = 1;

        public void SetUstopable(bool isUnstopableTimer) => this.isUnstopableTimer = isUnstopableTimer;

        public void SetTimer(float time, Action someMethod, bool isBoost = false, bool isBeam = false)
        {
            if (isBoost == true) this.isBoost = isBoost;
            if (isBeam == true) this.isBeam = isBeam;

            timerStop = false;
            times += time;
            startTimer = true;
            methodForCall = someMethod;

            if (timerSlider != null)
            {
                timerSlider.minValue = 0;
                timerSlider.maxValue = (float)times;
                timerSlider.value = (float)times;
            }
        }

        public void ResetTimer(double time, Action someMethod)
        {
            timerStop = false;
            SetTime(time);
            startTimer = true;
            methodForCall = someMethod;

            if (timerSlider != null)
            {
                timerSlider.minValue = 0;
                timerSlider.maxValue = (float)times;
                timerSlider.value = (float)times;
            }
        }

        public void StopTimer() => timerStop = true;

        public void StartTimer() => timerStop = false;

        public void SetCallbackTimer(float time, Action<double> perSecondCallback, Action endCallback)
        {
            timerStop = false;
            SetTime(time);
            startTimer = true;
            methodForCall = endCallback;

            callbackPerSecondMethod = perSecondCallback;
            isCallbackTimer = true;
        }

        public void MakeThisTimerCallbackableUncallbackble(Action<double> perSecondCallback, bool callbackble)
        {
            isCallbackTimer = callbackble;
            if (callbackble)
            {
                callbackPerSecondMethod = perSecondCallback;
            }
        }

        public void SetProgress(Slider slider)
        {
            timerSlider = slider;
            timerSlider.minValue = 0;
            timerSlider.maxValue = (float)times;
        }

        public void SetProgressMaxValue() => timerSlider.maxValue = (float)times;

        public void ShowProgressBar(bool isShow) => timerSlider.gameObject.SetActive(isShow);
        public float GetTime() => (float)times;

        private void Awake()
        {
            InitVar();
            SubscribeToEvents();
            startTimer = false;
            times = 0;
        }

        private void InitVar()
        {
            multiplier = 1;
            paused = false;
            timerStop = false;
            isBoost = false;
            isBeam = false;
            storageIsFull = false;
            isCallbackTimer = false;
            timerType = TimerType.DefaultTimer;
        }

        private void SubscribeToEvents()
        {
            EventManager.GamePaused += () => paused = true;
            EventManager.GameUnpaused += () => paused = false;
            EventManager.FullStorage += FullStorage;
        }

        private void FullStorage(int perc)
        {
            if (perc >= 100) storageIsFull = true;
            else storageIsFull = false;
        }

        private void Update()
        {
            if (startTimer && (!paused || isBoost || isUnstopableTimer) && (!storageIsFull || isUnstopableTimer)
             && !timerStop)
            {
                if (timerSlider != null)
                {
                    timerSlider.value = (float)times;
                }

                if (times > 0)
                {
                    times -= Time.deltaTime * multiplier;
                    if (isCallbackTimer)
                    {
                        callbackPerSecondMethod(times);
                    }
                    if (isBeam) EventManager.BeamDistanceEvent(times);
                }
                if (times <= 0)
                {
                    times = 0;
                    startTimer = false;
                    methodForCall();
                }
            }
        }

        public void SetTime(double time) => times = time;
    }
}
