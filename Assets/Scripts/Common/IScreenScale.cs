﻿namespace BlackBears.DiggIsBig.Common
{

    public interface IScreenScale
    {
        float ScreenScale { get; }
    }

}

