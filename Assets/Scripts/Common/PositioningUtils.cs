﻿using UnityEngine;

namespace BlackBears.DiggIsBig.Common
{

    public static class PositioningUtils
    {
        private static Vector2 BottomLeft { get { return default(Vector2); } }
        private static Vector2 TopLeft { get { return new Vector2(0, Screen.height); } }
        private static Vector2 TopRight { get { return new Vector2(Screen.width, Screen.height); } }
        private static Vector2 BottomRight { get { return new Vector2(Screen.width, 0); } }
        private static Vector2 Center { get { return TopRight / 2f; } }

        #region Common

        public static Vector2 GetBottomLeftPosition(float scale) { return BottomLeft / scale; }
        public static Vector2 GetTopLeftPosition(float scale) { return TopLeft / scale; }
        public static Vector2 GetTopRightPosition(float scale) { return TopRight / scale; }
        public static Vector2 GetBottomRightPosition(float scale) { return BottomRight / scale; }

        #endregion Common

        #region SpriteRenderer

        public static void ToBottomLeft(this SpriteRenderer renderer, float scale = 1f)
        {
            renderer.ToBottomLeft(default(Vector2), scale);
        }

        public static void ToBottomLeft(this SpriteRenderer renderer, Vector2 bottomLeft, float scale = 1f)
        {
            Vector3 pos = renderer.sprite.GetBottomLeftPosition(bottomLeft, scale);
            var t = renderer.transform;
            pos.z = t.localPosition.z;
            t.localPosition = pos;
        }

        public static void ToBottomRight(this SpriteRenderer renderer, float scale = 1f)
        {
            renderer.ToBottomRight(new Vector2(Screen.width, 0), scale);
        }

        public static void ToBottomRight(this SpriteRenderer renderer, Vector2 bottomRight, float scale = 1f)
        {
            Vector3 pos = renderer.sprite.GetBottomRightPosition(bottomRight, scale);
            var t = renderer.transform;
            pos.z = t.localPosition.z;
            t.localPosition = pos;
        }

        public static void ToTopLeft(this SpriteRenderer renderer, float scale = 1f)
        {
            renderer.ToTopLeft(new Vector2(0, Screen.height), scale);
        }

        public static void ToTopLeft(this SpriteRenderer renderer, Vector2 topLeft, float scale = 1f)
        {
            Vector3 pos = renderer.sprite.GetTopLeftPosition(topLeft, scale);
            var t = renderer.transform;
            pos.z = t.localPosition.z;
            t.localPosition = pos;
        }

        public static void ToTopRight(this SpriteRenderer renderer, float scale = 1f)
        {
            renderer.ToTopRight(new Vector2(Screen.width, Screen.height), scale);
        }

        public static void ToTopRight(this SpriteRenderer renderer, Vector2 topRight, float scale = 1f)
        {
            Vector3 pos = renderer.sprite.GetTopRightPosition(topRight, scale);
            var t = renderer.transform;
            pos.z = t.localPosition.z;
            t.localPosition = pos;
        }

        #endregion SpriteRenderer

        #region Sprite

        public static Vector2 GetBottomLeftPosition(this Sprite sprite, float screenScale, float scale = 1f)
        {
            return sprite.GetBottomLeftPosition(BottomLeft, screenScale, scale);
        }

        public static Vector2 GetBottomLeftPosition(this Sprite sprite, Vector2 bottomLeft, float screenScale, float scale = 1)
        {
            var result = bottomLeft / screenScale;
            var ppu = sprite.pixelsPerUnit;

            result.x += (sprite.pivot.x / ppu) * scale;
            result.y += (sprite.pivot.y / ppu) * scale;

            return result;
        }

        public static Vector2 GetBottomRightPosition(this Sprite sprite, float screenScale, float scale = 1)
        {
            return sprite.GetBottomRightPosition(BottomRight, screenScale, scale);
        }

        public static Vector2 GetBottomRightPosition(this Sprite sprite, Vector2 bottomRight, float screenScale, float scale = 1)
        {
            var result = bottomRight / screenScale;
            var ppu = sprite.pixelsPerUnit;

            result.x -= ((sprite.rect.width - sprite.pivot.x) / ppu) * scale;
            result.y += (sprite.pivot.y / ppu) / scale;

            return result;
        }

        public static Vector2 GetTopLeftPosition(this Sprite sprite, float screenScale, float scale = 1f)
        {
            return sprite.GetTopLeftPosition(TopLeft, screenScale, scale);
        }

        public static Vector2 GetTopLeftPosition(this Sprite sprite, Vector2 topLeft, float screenScale, float scale = 1f)
        {
            var result = topLeft / screenScale;
            var ppu = sprite.pixelsPerUnit;

            result.x += (sprite.pivot.x / ppu) * scale;
            result.y -= ((sprite.rect.height - sprite.pivot.y) / ppu) * scale;

            return result;
        }

        public static Vector2 GetTopRightPosition(this Sprite sprite, float screenScale, float scale = 1f)
        {
            return sprite.GetTopRightPosition(TopRight, screenScale, scale);
        }

        public static Vector2 GetTopRightPosition(this Sprite sprite, Vector2 topRight, float screenScale, float scale = 1f)
        {
            var result = topRight / screenScale;
            var ppu = sprite.pixelsPerUnit;

            result.x -= ((sprite.rect.width - sprite.pivot.x) / ppu) * scale;
            result.y -= ((sprite.rect.height - sprite.pivot.y) / ppu) * scale;

            return result;
        }

        public static Vector2 GetCenterPosition(this Sprite sprite, float screenScale, float scale = 1f)
        {
            return sprite.GetCenterPosition(Center, screenScale, scale);
        }

        public static Vector2 GetCenterPosition(this Sprite sprite, Vector2 center, float screenScale, float scale = 1f)
        {
            var result = center / screenScale;
            var ppu = sprite.pixelsPerUnit;

            result.x -= ((sprite.rect.width * 0.5f - sprite.pivot.x) / ppu) * scale;
            result.y -= ((sprite.rect.height * 0.5f - sprite.pivot.y) / ppu) * scale;

            return result;
        }

        #endregion

    }

}