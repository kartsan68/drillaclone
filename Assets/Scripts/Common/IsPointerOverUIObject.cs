﻿using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine;
using BlackBears.DiggIsBig.Managers;

namespace BlackBears.DiggIsBig.Common
{
    public static class IsPointerOverUIObject
    {
        public static bool IsPointerOverUIObjectMeth()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (RaycastResult element in results)
            {
                if (element.gameObject.CompareTag("UnpermeableUI"))
                    return true;
                if (element.gameObject.CompareTag("PermeableUI"))
                    return false;

            }
            return results.Count > 0;
        }

        public static void CheckSoundElement()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (RaycastResult element in results)
            {
                if (element.gameObject.CompareTag("UnpermeableUI"))
                {
                    EventManager.PlaySoundEvent(GameData.Sound.UsualTap);
                    return;
                }
            }
        }
    }
}
