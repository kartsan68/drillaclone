﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;

namespace BlackBears.DiggIsBig.UI
{
    public class UpgradeLabelCanvas : MonoBehaviour
    {
        [SerializeField] private UpgradeLabel[] labels;

        public void StartUpdate(int elementIndex, int elementLvl, int oldLvl)
        {
            labels[elementIndex].lvlText.text = oldLvl + " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);
            labels[elementIndex].lvl = elementLvl;
            labels[elementIndex].animator.SetTrigger("ShowLabel");
        }


    }
}
