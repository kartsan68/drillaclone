﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BlackBears.DiggIsBig.UI.Animation
{
    public class MoveCoin : MonoBehaviour
    {
        [SerializeField] private Vector2 maxSpawnRange;
        [SerializeField] private Vector2 minSpawnRange;
        [SerializeField] private RectTransform coinTarget;
        [SerializeField] private RectTransform coinStartPosition;
        [SerializeField] private GameObject coinPrefab;
        [SerializeField] private float coinSpeed;
        [SerializeField] private float coinToPositionSpeed;
        [SerializeField] private float stopOnSpawnPositionDelay;
        [SerializeField] private float spawnDelay;

        private float coinCount;
        private Action endAction;
        private List<Transform> coins;

        public void StartMoveCoinAnimation(Action endAction, float coinCount)
        {
            coins = new List<Transform>();
            this.coinCount = coinCount;
            this.endAction = endAction;
            StartCoroutine(MoveToTargetStart());
        }

        private IEnumerator MoveToTargetStart()
        {
            int count = 0;

            while (count <= coinCount)
            {
                Transform tr = Instantiate(coinPrefab, coinStartPosition.position, Quaternion.identity, coinStartPosition).GetComponent<Transform>();
                coins.Add(tr);
                StartCoroutine(MoveToTarget(tr));
                count++;
                yield return new WaitForSeconds(spawnDelay);
            }
            while (coins.Count > 0) yield return null;
            endAction.Invoke();
        }

        private IEnumerator MoveToTarget(Transform tr)
        {
            Vector3 spawnPos = new Vector3(tr.position.x + Random.Range(minSpawnRange.x, maxSpawnRange.x), tr.position.y + Random.Range(minSpawnRange.y, maxSpawnRange.y), 0);
            while (!MoveToSpawnPosition(tr, spawnPos)) yield return null;
            yield return new WaitForSeconds(stopOnSpawnPositionDelay);
            while (!MoveToPosition(tr)) yield return null;
            coins.Remove(tr);
            Destroy(tr.gameObject);
        }

        private bool MoveToPosition(Transform tr)
        {
            tr.position = Vector3.MoveTowards(tr.position, coinTarget.position, coinSpeed * Time.deltaTime);
            return Vector3.Distance(tr.position, coinTarget.position) < 0.001f;
        }

        private bool MoveToSpawnPosition(Transform tr, Vector3 targetPos)
        {
            tr.position = Vector3.MoveTowards(tr.position, targetPos, coinToPositionSpeed * Time.deltaTime);
            return Vector3.Distance(tr.position, targetPos) < 0.001f;
        }


    }
}
