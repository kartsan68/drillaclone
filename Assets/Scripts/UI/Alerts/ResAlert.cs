﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.UI
{
    public class ResAlert : MonoBehaviour
    {
        [SerializeField] private Image resImage;
        [SerializeField] private Text resName;
        [SerializeField] private GameObject alert;
        [SerializeField] private Button storageButton;

        public void ShowResAlert(Sprite resSprite, string name)
        {
            resImage.sprite = resSprite;
            resName.text = name;
            alert.SetActive(true);
        }

        public void PressStorageButton() => storageButton.onClick.Invoke();
    }
}