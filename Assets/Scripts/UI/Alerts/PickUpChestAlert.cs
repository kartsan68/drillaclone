﻿using System;
using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Advertise;
using BlackBears.DiggIsBig.Configuration;
using BlackBears.DiggIsBig.Controllers;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.UI
{
    public class PickUpChestAlert : MonoBehaviour
    {
        [SerializeField] private float pack1CoinCount;
        [SerializeField] private float pack2CoinCount;
        [SerializeField] private float pack3CoinCount;
        [SerializeField] private float chestCoinCount;

        [SerializeField] private Animation.MoveCoin chestMoveCoin;
        [SerializeField] private Animation.MoveCoin packMoveCoin;
        [SerializeField] private GameObject moneyTopLabel;
        [SerializeField] private GameObject innerBar;
        [SerializeField] private GameObject ChestImageAnim;
        [SerializeField] private GameObject ChestImageGold;
        [SerializeField] private GameObject goldElement;

        [SerializeField] private GameObject smallBtnContainer;
        [SerializeField] private Button smallCatchBtn;
        [SerializeField] private Button smallAdvBtn;
        [SerializeField] private Text goldCountInButton;

        [SerializeField] private GameController gameController;

        [SerializeField] private Advert advertise;
        [SerializeField] private GameObject alert;
        [SerializeField] private Button catchButton;
        //0 - двигатель, 1 - бур, 2 - ковши, 3 - склад, 4 - деньги
        [SerializeField] private GameObject[] elementsInChest;
        [SerializeField] private Image chestImage;
        [SerializeField] private Sprite smallChestSprite;
        [SerializeField] private Sprite mediumChestSprite;
        [SerializeField] private Sprite bigChestSprite;
        [SerializeField] private Sprite detailChestSprite;
        [SerializeField] private Sprite adsChestSprite;
        [SerializeField] private Sprite smallPackSprite;
        [SerializeField] private Sprite mediumPackSprite;
        [SerializeField] private Sprite bigPackSprite;
        [SerializeField] private Sprite rouletteGoldSprite;
        [SerializeField] private Sprite rouletteRandomDetailSprite;
        [SerializeField] private Vector2 usualSize;
        [SerializeField] private Vector2 smallChestSize;

        private Animator alertAnimator;
        private Text lvlTextInEngineLable;
        private Text lvlTextInBoerLable;
        private Text lvlTextInLadleLable;
        private Text lvlTextInStorageLable;
        private Text moneyCount;
        private Text moneyCountGoldChest;
        private DrillFormatter drillFormatter;
        private GameConfigurationManager gameConfigurationManager;


        public void OpenChestWithGold(ChestType chestType, bool withAdvert, string goldCount, Action action, Action advAction, string goldCountX6 = "")
        {
            ClearAll();
            AddListenerToButton(action, true);
            moneyCount.text = goldCount;
            moneyCountGoldChest.text = goldCount;
            elementsInChest[4].SetActive(true);
            moneyTopLabel.SetActive(true);
            float coinCount = 0;

            switch (chestType)
            {
                case ChestType.SmallChest:
                    smallBtnContainer.gameObject.SetActive(true);
                    chestImage.rectTransform.sizeDelta = smallChestSize;
                    chestImage.sprite = smallChestSprite;
                    coinCount = pack1CoinCount;
                    break;
                case ChestType.MediumChest:
                    catchButton.gameObject.SetActive(true);
                    chestImage.rectTransform.sizeDelta = smallChestSize;
                    chestImage.sprite = mediumChestSprite;
                    coinCount = pack2CoinCount;
                    break;
                case ChestType.BigChest:
                    catchButton.gameObject.SetActive(true);
                    chestImage.sprite = bigChestSprite;
                    coinCount = pack3CoinCount;
                    break;
                case ChestType.GoldPackSmall:
                    catchButton.gameObject.SetActive(true);
                    chestImage.sprite = smallPackSprite;
                    coinCount = pack1CoinCount;
                    break;
                case ChestType.GoldPackMedium:
                    catchButton.gameObject.SetActive(true);
                    chestImage.sprite = mediumPackSprite;
                    coinCount = pack2CoinCount;
                    break;
                case ChestType.GoldPackBig:
                    catchButton.gameObject.SetActive(true);
                    chestImage.sprite = bigPackSprite;
                    coinCount = pack3CoinCount;
                    break;
                case ChestType.RouletteGold:
                    catchButton.gameObject.SetActive(true);
                    chestImage.sprite = rouletteGoldSprite;
                    break;
            }
            Action act = () =>
            {
                HideAllButtons();
                packMoveCoin.StartMoveCoinAnimation(() => alert.SetActive(false), coinCount);
            };

            if (withAdvert)
            {
                innerBar.SetActive(false);
                ChestImageAnim.SetActive(false);
                ChestImageGold.SetActive(true);
                goldElement.SetActive(true);

                smallCatchBtn.GetComponent<Text>().text = DigIsBigLocalizator.GetLocalizationString("BTN_CATCH", Modificator.ToUpper) + " " + goldCount;
                //moneyCount.text = goldCountX4;
                goldCountInButton.text = goldCountX6;
                smallAdvBtn.onClick.AddListener(() =>
                {
                    advertise.ShowRewardedAdvertWithCallBack(() =>
                    {
                        advAction.Invoke();
                        HideAllButtons();
                        chestMoveCoin.StartMoveCoinAnimation(() => alert.SetActive(false), coinCount * 2);
                    });
                });

                smallCatchBtn.gameObject.SetActive(false);
                StartCoroutine(ShowSmallButton());
                coinCount = chestCoinCount;
                act = () =>
                {
                    HideAllButtons();
                    chestMoveCoin.StartMoveCoinAnimation(() => alert.SetActive(false), coinCount);
                };
            }

            catchButton.onClick.AddListener(() => act.Invoke());
            smallCatchBtn.onClick.AddListener(() => act.Invoke());

            alert.SetActive(true);
        }

        public void OpenChestWithRandomDetail(int detailIndex, int detailLvl, Action action)
        {
            ClearAll();
            AddListenerToButton(action);
            catchButton.gameObject.SetActive(true);
            elementsInChest[detailIndex].SetActive(true);
            string lvl = " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);
            switch (detailIndex)
            {
                case 0:
                    lvlTextInEngineLable.text = "+" + detailLvl + lvl;
                    break;
                case 1:
                    lvlTextInBoerLable.text = "+" + detailLvl + lvl;
                    break;
                case 2:
                    lvlTextInLadleLable.text = "+" + detailLvl + lvl;
                    break;
                case 3:
                    lvlTextInStorageLable.text = "+" + detailLvl + lvl;
                    break;
            }
            chestImage.sprite = rouletteRandomDetailSprite;
            OpenAlert();
        }

        public void OpenChestWithFullDetails(int[] detailsLvl, Action action)
        {
            ClearAll();
            AddListenerToButton(action);
            catchButton.gameObject.SetActive(true);
            double goldCount = 0;
            for (int i = 0; i < detailsLvl.Length; i++)
            {
                if ((GameData.GameData.lvlDetails[i] + detailsLvl[i]) > GameData.GameData.maxDetailsLvl)
                {
                    switch (i)
                    {
                        case 0:
                            goldCount += (gameConfigurationManager.detailData.engine[GameData.GameData.maxDetailsLvl - 1].price * 0.4f);
                            break;
                        case 1:
                            goldCount += (gameConfigurationManager.detailData.boer[GameData.GameData.maxDetailsLvl - 1].price * 0.4f);
                            break;
                        case 2:
                            goldCount += (gameConfigurationManager.detailData.ladle[GameData.GameData.maxDetailsLvl - 1].price * 0.4f);
                            break;
                        case 3:
                            goldCount += (gameConfigurationManager.detailData.storage[GameData.GameData.maxDetailsLvl - 1].price * 0.4f);
                            break;
                    }

                    elementsInChest[i].SetActive(false);
                }
                else
                {
                    elementsInChest[i].SetActive(true);
                }
            }

            if (goldCount > 0)
            {
                moneyCount.text = drillFormatter.MoneyKFormatter((long)goldCount);
                elementsInChest[4].SetActive(true);
                AddListenerToButton(() =>
                {
                    EventManager.GiveMeMoneyEvent((long)goldCount);
                });
            }
            string lvl = " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);
            lvlTextInEngineLable.text = "+" + detailsLvl[0] + lvl;
            lvlTextInBoerLable.text = "+" + detailsLvl[1] + lvl;
            lvlTextInLadleLable.text = "+" + detailsLvl[2] + lvl;
            lvlTextInStorageLable.text = "+" + detailsLvl[3] + lvl;

            chestImage.sprite = detailChestSprite;

            OpenAlert();
        }

        public void OpenChestWithSomeDetailsAndGold(int detailIndex, int detailLvl, string goldCount, Action action)
        {
            ClearAll();
            AddListenerToButton(action);
            catchButton.gameObject.SetActive(true);
            moneyCount.text = goldCount;
            elementsInChest[4].SetActive(true);

            elementsInChest[detailIndex].SetActive(true);
            string lvl = " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);
            switch (detailIndex)
            {
                case 0:
                    lvlTextInEngineLable.text = "+" + detailLvl + lvl;
                    break;
                case 1:
                    lvlTextInBoerLable.text = "+" + detailLvl + lvl;
                    break;
                case 2:
                    lvlTextInLadleLable.text = "+" + detailLvl + lvl;
                    break;
                case 3:
                    lvlTextInStorageLable.text = "+" + detailLvl + lvl;
                    break;
            }

            chestImage.sprite = adsChestSprite;

            OpenAlert();
        }

        private void HideAllButtons()
        {
            smallBtnContainer.gameObject.SetActive(false);
            catchButton.gameObject.SetActive(false);
        }

        private void ClearAll()
        {
            innerBar.SetActive(true);
            ChestImageAnim.SetActive(true);
            ChestImageGold.SetActive(false);
            goldElement.SetActive(false);
            chestImage.rectTransform.sizeDelta = usualSize;
            foreach (var index in elementsInChest) index.SetActive(false);
            catchButton.onClick.RemoveAllListeners();
            catchButton.gameObject.SetActive(false);
            moneyTopLabel.SetActive(false);
            smallCatchBtn.onClick.RemoveAllListeners();

            smallAdvBtn.onClick.RemoveAllListeners();
            smallBtnContainer.gameObject.SetActive(false);
        }

        private void OpenAlert()
        {
            alert.SetActive(true);
            alertAnimator.SetTrigger("OpenChest");
        }

        private void AddListenerToButton(Action action, bool isGold = false)
        {
            catchButton.onClick.AddListener(() => action.Invoke());
            if (!isGold)
                catchButton.onClick.AddListener(() => alert.SetActive(false));

            smallCatchBtn.onClick.AddListener(() => action.Invoke());
            if (!isGold)
                smallCatchBtn.onClick.AddListener(() => alert.SetActive(false));
        }

        private void Awake()
        {
            alertAnimator = alert.GetComponent<Animator>();
            gameConfigurationManager = gameController.GetConfigurationManager();
            lvlTextInEngineLable = elementsInChest[0].GetComponentInChildren<Text>();
            lvlTextInBoerLable = elementsInChest[1].GetComponentInChildren<Text>();
            lvlTextInLadleLable = elementsInChest[2].GetComponentInChildren<Text>();
            lvlTextInStorageLable = elementsInChest[3].GetComponentInChildren<Text>();
            moneyCount = elementsInChest[4].GetComponentInChildren<Text>();
            moneyCountGoldChest = goldElement.GetComponentInChildren<Text>();
            EventManager.RewardedAdvertiseStatus += RewardedAdvertiseStatus;
            drillFormatter = new DrillFormatter();
        }

        private void RewardedAdvertiseStatus(bool state)
        {
            smallAdvBtn.interactable = state;
        }

        private IEnumerator ShowSmallButton()
        {
            float delay = 0.8f;
            yield return new WaitForSeconds(delay);
            smallCatchBtn.gameObject.SetActive(true);
        }
    }
}
