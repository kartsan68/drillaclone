﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;

namespace BlackBears.DiggIsBig.UI.Controllers
{
    public class TopBarController : MonoBehaviour
    {
        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            EventManager.TopElemensIsMax += TopElementsControl;
        }

        private void TopElementsControl(bool isMax)
        {
            if (isMax) animator.SetTrigger("SmallToBigTrigger");
            else animator.SetTrigger("BigToSmallTrigger");
        }
    }
}
