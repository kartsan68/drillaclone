﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.UI.Controllers
{
    public class StorageProgressManager : MonoBehaviour
    {
        //счетчик и фон на главном экране шахты
        [SerializeField] private GameObject fullStorageBack;
        [SerializeField] private GameObject fullStorageCounter;
        [SerializeField] private GameObject usualStorageCounter;
        [SerializeField] private Text storageText;
        //счетчик агрейдов на главном экране шахты
        [SerializeField] private GameObject usualUpCounter;
        [SerializeField] private GameObject fullUpCounter;
        [SerializeField] private Text upgradeText;

        [SerializeField] private GameObject fullStorAlert;
        [SerializeField] private GameObject redProgress;
        [SerializeField] private GameObject usualProgress;
        [SerializeField] private Animator storageAlertAnimator;
        private bool storageWillBeUp = false;
        private int oldPerc;

        private void Awake()
        {
            SubscribeToEvent();
        }

        private void SubscribeToEvent()
        {
            EventManager.FullStorage += CheckStorageState;
            EventManager.RedyUpdatesCount += CheckUpCount;
            EventManager.CantTouchRes += () => storageAlertAnimator.Play("FullStorageShow", -1, 0f);
            EventManager.HideStorageAlert += () =>
            {
                alertIsActive = false;
                storageAlertAnimator.gameObject.transform.localScale = new Vector3(0, 0, 0);
            };
        }

        //счетчик апгрейдов
        private void CheckUpCount(int count)
        {
            if (count == 0)
            {
                upgradeText.gameObject.SetActive(false);
                usualUpCounter.SetActive(false);
                fullUpCounter.SetActive(false);
                return;
            }

            // if (count < 4)
            // {
            //     upgradeText.gameObject.SetActive(true);
            //     upgradeText.text = count.ToString();
            //     usualUpCounter.SetActive(true);
            //     fullUpCounter.SetActive(false);
            //     return;
            // }

            // if (count == 4)
            else
            {
                upgradeText.gameObject.SetActive(true);
                upgradeText.text = count.ToString();
                usualUpCounter.SetActive(false);
                fullUpCounter.SetActive(true);
                return;
            }
        }

        private bool alertIsActive = false;
        private void CheckStorageState(int perc)
        {
            oldPerc = perc;
            storageText.text = perc.ToString() + "%";

            if (perc == 100)
            {
                redProgress.SetActive(true);
                usualProgress.SetActive(false);
                GameData.GameData.storageIsFull = true;
#if UNITY_IOS
                if (!NotificationsManager.Available)
                    fullStorAlert.SetActive(true);
                else
                    fullStorAlert.SetActive(false);
#endif
                fullStorageBack.SetActive(true);
                fullStorageCounter.SetActive(true);
                usualStorageCounter.SetActive(false);

                if (!alertIsActive)
                {
                    alertIsActive = true;
                    storageAlertAnimator.SetTrigger("ShowFullStorageAlert");
                }

            }
            else
            {
                redProgress.SetActive(false);
                usualProgress.SetActive(true);
                GameData.GameData.storageIsFull = false;
#if UNITY_IOS
                fullStorAlert.SetActive(false);
#endif
                fullStorageBack.SetActive(false);
                fullStorageCounter.SetActive(false);
                usualStorageCounter.SetActive(true);
                if (alertIsActive)
                {
                    alertIsActive = false;
                    storageAlertAnimator.SetTrigger("HideFullStorageAlert");
                }
            }

        }
    }
}
