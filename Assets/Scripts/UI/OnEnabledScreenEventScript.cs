﻿using BlackBears.DiggIsBig.Advertise;
using BlackBears.DiggIsBig.GameData;
using BlackBears.DiggIsBig.Managers;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBears.DiggIsBig.UI
{
    public class OnEnabledScreenEventScript : MonoBehaviour
    {
        [SerializeField] private Screens screen;
        [SerializeField] private GameObject garageInfoAlert;

        private bool firstGarageAlertShow;

        private bool youShallNotPass = false;
        private bool mineResetter = true;


        public void AdvProtection() => youShallNotPass = true;

        private void Awake()
        {
            EventManager.CloseWinAdvBlock += AdvProtection;
        }


        private void OnEnable()
        {
            firstGarageAlertShow = GameData.GameData.firstGarageAlertShow;
            if (screen == Screens.garage && firstGarageAlertShow)
            {
                firstGarageAlertShow = false;
                GameData.GameData.firstGarageAlertShow = false;
                garageInfoAlert.SetActive(true);
            }

            youShallNotPass = false;
            mineResetter = true;
        }

        private void OnDisable()
        {
            if (screen == Screens.garage) garageInfoAlert.SetActive(false);

            //ПОПРОСИЛИ УБРАТЬ ИНТЕРСТИШИАЛ ПРИ ПЕРЕХОДЕ, ДЛЯ ТОГО ЧТО БЫ ВЕРНУТЬ - РАСКОММЕНТИТЬ
            // if (!mineResetter && !youShallNotPass) EventManager.CallInterstitialAdvertEvent();
        }

        public void MineResetter() => mineResetter = false;
    }
}
