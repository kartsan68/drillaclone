﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.UI
{
    public class ProgressFlasher : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Color flashColor;
        [Range(0, 1)]
        [SerializeField] private float colorSwapDelay;
        [SerializeField] private int flashCountBeforeDelay;
        [SerializeField] private float fleshDelay;
        [Range(1, 99)]
        [SerializeField] public float flashStartPercentConfine;

        private bool _isOn = false;
        private int _flashCount = 0;

        public void FlashWork(bool isOn) => _isOn = isOn;

        private void Start()
        {
            Debug.Log(gameObject.name);
            StartCoroutine(Flasher());
        }
        private IEnumerator Flasher()
        {
            _flashCount = 0;
            while (true)
            {
                while (!_isOn)
                {
                    if (_image.color == Color.white) yield return null;
                    else
                    {
                        yield return null;
                        _image.color = Color.white;
                    }
                }

                while (_flashCount < flashCountBeforeDelay)
                {
                    _image.color = flashColor;
                    yield return new WaitForSeconds(colorSwapDelay);
                    _image.color = Color.white;
                    yield return new WaitForSeconds(colorSwapDelay);
                    _flashCount++;
                }
                _flashCount = 0;
                yield return new WaitForSeconds(fleshDelay);
            }
        }
    }
}
