﻿using System.Collections;
using System.Collections.Generic;
using BlackBears.DiggIsBig.Formatter;
using BlackBears.DiggIsBig.Managers;
using BlackBears.GameCore.Features.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.UI
{
    public class UpgradeLabel : MonoBehaviour
    {
        public Animator animator;
        public Text lvlText;
        public int lvl;

        private void LvlUp()
        {
            lvlText.text = lvl.ToString();
            animator.SetTrigger("UpTrigger");
        }

        private void EndAnimations() => EventManager.UpgradeAnimationEndEvent();

        private void TextWithUr() => lvlText.text = lvl + " " + DigIsBigLocalizator.GetLocalizationString("BEAM_PRE_LVL", Modificator.ToUpper);
    }
}
