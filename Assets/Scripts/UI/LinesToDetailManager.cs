﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBears.DiggIsBig.UI
{
    public class LinesToDetailManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] greenLinesToDetails;

        [SerializeField] private GameObject[] yellowLinesToDetails;

        [SerializeField] private GameObject[] grayLinesToDetails;

        [SerializeField] private GameObject[] inactiveFields;
        [SerializeField] private GameObject[] adsFields;


        public void ButtonDownEvent(int index)
        {
            if (inactiveFields[index].activeSelf) grayLinesToDetails[index].SetActive(true);
            else if (adsFields[index].activeSelf) yellowLinesToDetails[index].SetActive(true);
            else greenLinesToDetails[index].SetActive(true);
        }

        public void ButtonUpEvent(int index)
        {
            grayLinesToDetails[index].SetActive(false);
            yellowLinesToDetails[index].SetActive(false);
            greenLinesToDetails[index].SetActive(false);
        }
    }
}
