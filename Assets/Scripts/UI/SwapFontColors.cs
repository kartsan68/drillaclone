﻿using UnityEngine;
using UnityEngine.UI;

namespace BlackBears.DiggIsBig.UI
{
    public class SwapFontColors : MonoBehaviour
    {
        public void ColorIsBlack(bool isBlack)
        {
            if (isBlack)
                GetComponent<Text>().color = Color.black;
            else
                GetComponent<Text>().color = Color.white;
        }
    }
}
