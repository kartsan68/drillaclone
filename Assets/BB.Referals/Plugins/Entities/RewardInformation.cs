using System;
using System.Collections.Generic;
using BlackBears.Utils.Collections;
using SimpleJSON;

namespace BlackBears.Referals
{

    public class RewardInformation
    {

        public readonly IList<RewardedItem> rewardedItems;
        public readonly IList<long> clickTimestamps;

        public int NotTakenCount { get; private set; }

        internal RewardInformation(JSONNode data)
        {
            List<RewardedItem> rewardedItems = new List<RewardedItem>();
            int notTakenCount = 0;
            foreach(JSONNode itemData in data["rewarded_list"].AsArray)
            {
                var item = new RewardedItem(itemData);
                rewardedItems.Add(item);
                if (!item.RewardTaken) notTakenCount += 1;
            }
            NotTakenCount = notTakenCount;
            this.rewardedItems = rewardedItems.ToImmutable();

            List<long> clickTimestamps = new List<long>();
            foreach(JSONNode timestampData in data["clicked_list"].AsArray) 
                clickTimestamps.Add(timestampData["click_timestamp"].AsLong);
            this.clickTimestamps = clickTimestamps.ToImmutable();
        }

        internal void Validate(RewardTakeAnswer answer)
        {
            if (answer == null || answer.rewards == null || answer.rewardCount == 0) return;
            var toValidateById = new Dictionary<string, RewardedItem>();
            foreach(var reward in answer.rewards) toValidateById[reward.InstallationId] = reward;

            int notTakenCount = 0;
            RewardedItem newReward;
            foreach(var reward in rewardedItems)
            {
                if (toValidateById.TryGetValue(reward.InstallationId, out newReward) && newReward != null)
                {
                    reward.Validate(newReward);
                }
                if (!reward.RewardTaken) notTakenCount += 1;
            }
            NotTakenCount = notTakenCount;
        }

    }

}