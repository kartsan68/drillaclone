using BlackBears.GameCore.Networking;
using SimpleJSON;

namespace BlackBears.Referals
{

    internal class LinkConverter : DataConverter<string>
    {

        internal LinkConverter(Block<string> onConvert, FailBlock onFail) : base(onConvert, onFail, null) { }

        protected override bool Convert(ref string instance, JSONNode data)
        {
            instance = data["link"].Value;
            return true;
        }
    }

    internal class RewardInformationConverter : DataConverter<RewardInformation>
    {

        internal RewardInformationConverter(Block<RewardInformation> onConvert, FailBlock onFail)
            : base(onConvert, onFail, null) { }

        protected override bool Convert(ref RewardInformation instance, JSONNode data)
        {
            instance = new RewardInformation(data);
            return true;
        }

    }

    internal class RewardTakeAnswerConverter : DataConverter<RewardTakeAnswer>
    {

        private readonly string keyFormat;

        internal RewardTakeAnswerConverter(Block<RewardTakeAnswer> onConvert, FailBlock onFail,
            string keyFormat) : base(onConvert, onFail, null)
        {
            this.keyFormat = keyFormat;
        }

        protected override bool Convert(ref RewardTakeAnswer instance, JSONNode data)
        {
            var answer = new RewardTakeAnswer(data);
            string outKey = data["out_key"].Value;
            string inKey = BlackBears.Utils.Security.GetMD5Hash(string.Format(keyFormat, answer.rewardCount));

            if (string.Equals(inKey, outKey, System.StringComparison.InvariantCultureIgnoreCase))
            {
                instance = answer;
                return true;
            }
            return false;
        }

    }

}