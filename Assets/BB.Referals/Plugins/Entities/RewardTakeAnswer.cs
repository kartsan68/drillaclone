using System.Collections.Generic;
using BlackBears.Utils.Collections;
using SimpleJSON;

namespace BlackBears.Referals
{

    public class RewardTakeAnswer
    {

        internal readonly int rewardCount;
        internal readonly IList<RewardedItem> rewards;

        internal RewardTakeAnswer(JSONNode data)
        {
            rewardCount = data["reward_count"].AsInt;

            var rewards = new List<RewardedItem>();
            foreach(JSONNode rewardedData in data["rewards"].AsArray) 
                rewards.Add(new RewardedItem(rewardedData));
                
            this.rewards = rewards.ToImmutable();
        }

    }

}