using System;
using SimpleJSON;

namespace BlackBears.Referals
{

    public class RewardedItem
    {

        public string InstallerId { get; private set; } 
        public long InstallTimestamp { get; private set; }
        public string InstallationId { get; private set; }

        public bool RewardTaken { get; private set; }

        internal RewardedItem(JSONNode node)
        {
            InstallerId = node["installer_id"].GetStringOrDefault();
            InstallTimestamp = node["install_timestamp"].GetLongOrDefault();
            InstallationId = node["installation_id"].GetStringOrDefault();

            RewardTaken = node["status"].GetBoolOrDefault();
        }

        public void Take() { RewardTaken = true; }

        internal void Validate(RewardedItem other)
        {
            if (other == null || !string.Equals(InstallationId, other.InstallationId)) return;
            if (other.InstallerId != null) InstallerId = other.InstallerId;
            if (other.InstallTimestamp > 0) InstallTimestamp = other.InstallTimestamp;
            RewardTaken = other.RewardTaken;
        }

    }

}