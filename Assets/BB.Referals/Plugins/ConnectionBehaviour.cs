using System;
using BlackBears.GameCore.Networking;
using UnityEngine;

using BlackSec = BlackBears.Utils.Security;

namespace BlackBears.Referals
{

    internal class ConnectionBehaviour : MonoBehaviour
    {

        internal string server;
        internal string secret;

        internal void GetLink(string playerId, Block<string> onSuccess, FailBlock onFail)
        {
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "Player Id is not valid");
                return;
            }

            var converter = new LinkConverter(onSuccess, onFail);
            string url = GenerateUrl("player/get-link");
            var request = new NetworkGetRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);

            StartCoroutine(request.Start());
        }

        internal void GetRewardInformation(string playerId, Block<RewardInformation> onSuccess, FailBlock onFail)
        {
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "Player Id is not valid");
                return;
            }

            var converter = new RewardInformationConverter(onSuccess, onFail);
            string url = GenerateUrl("player/get-reward-list");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            AddKeyToRequest(playerId, request);

            StartCoroutine(request.Start());
        }

        internal void TakeReward(string playerId, string rewardId, Block<RewardTakeAnswer> onSuccess, FailBlock onFail)
        {
            if (playerId == null || rewardId == null)
            {
                onFail.SafeInvoke(-1, "Incoming data is not valid");
                return;
            }

            string randKey = BlackSec.GetRandKey();
            var keyFormat = new System.Text.StringBuilder();
            keyFormat.Append(randKey).Append("verify").Append(playerId).Append("{0}").Append(secret);
            var converter = new RewardTakeAnswerConverter(onSuccess, onFail, keyFormat.ToString());
            string url = GenerateUrl("player/take-reward-for-list");
            var request = new NetworkApiRequest(url, null, converter.Convert, onFail);
            request.AddRequestValue("player_id", playerId);
            request.AddRequestValue("ids[0]", rewardId);
            AddKeyToRequest(rewardId + playerId, request, randKey);

            StartCoroutine(request.Start());
        }

        private void AddKeyToRequest(string keyParam, NetworkApiRequest request, string randKey = null)
        {
            if (randKey == null) randKey = BlackSec.GetRandKey();
            string fullKey = string.Format("{0}{1}{2}", randKey, keyParam, secret);
            string key = BlackSec.GetMD5Hash(fullKey, BlackSec.MD5Format.lower);

            request.AddRequestValue("rand_key", randKey);
            request.AddRequestValue("key", key);
        }

        private string GenerateUrl(string command)
        {
            return string.Format("{0}/{1}", server, command);
        }

    }

}