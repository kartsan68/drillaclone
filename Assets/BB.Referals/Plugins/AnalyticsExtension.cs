using System.Collections.Generic;
using BlackBears.GameCore.Features.CoreDefaults;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;

namespace BlackBears.Referals.Analytics
{

    internal static class AnalyticsExtension
    {
        internal static void AddInvitedFriendToUserProfile(this IAnalyticsAdapter adapter)
        {
            adapter.AddUserProfileCounter(AnalyticsKeys.invitedFriendsKey, 1);
        }
    }
}