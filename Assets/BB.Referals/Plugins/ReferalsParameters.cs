using SimpleJSON;

namespace BlackBears.Referals
{
    public class ReferalsParameters
    {

        public readonly string url;
        public readonly double inviteReward;
        public readonly long reloadPeriod;

        public ReferalsParameters(JSONNode node)
        {
            url = node["url"].Value;
            inviteReward = node["invite_reward"].AsDouble;
            reloadPeriod = node["reload_period"].AsLong;
        }

    }
}