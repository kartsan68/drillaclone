using System;
using BlackBears.GameCore;
using BlackBears.GameCore.Features.Chronometer;
using BlackBears.GameCore.Features.CoreDefaults.Analytics;
using BlackBears.Referals.Analytics;
using UnityEngine;
using Logger = BlackBears.GameCore.Logger;

namespace BlackBears.Referals
{

    public class ReferalsModule : IDisposable
    {

        private const string logTag = "ReferalsModule";

        private static ReferalsModule instance;

        private ConnectionBehaviour connection;
        private ReferalsParameters parameters;

        private RewardInformation rewardInformation;
        private long rewardInfoLoadTime;

        private IAnalyticsAdapter analytics;

        private ChronometerFeature chronometer;

        private ReferalsModule(ReferalsParameters parameters, string secret)
        {
            var go = new GameObject("~~ReferalsServer");
#if BBGC_USE_DONT_DESTROY_ON_LOAD
            UnityEngine.Object.DontDestroyOnLoad(go);
#endif

            go.SetActive(false);
            connection = go.AddComponent<ConnectionBehaviour>();
            connection.server = parameters.url;
            connection.secret = secret;
            go.SetActive(true);

            chronometer = BBGC.Features.GetFeature<ChronometerFeature>();
            this.parameters = parameters;
        }

        internal static ReferalsModule Init(ReferalsParameters refParams, string secret)
        {
            if (instance == null) instance = new ReferalsModule(refParams, secret);
            else Logger.Warning(logTag, "'ReferalsModule.Init' called twice.");

            return instance;
        }

        public string ConnectionLink { get; private set; }
        public bool HasConnectionLink { get { return !string.IsNullOrEmpty(ConnectionLink); } }

        public double InviteReward { get { return parameters.inviteReward; } }

        public void Dispose()
        {
            instance = null;
        }
        
        public void LoadLink(string playerId, Block<string> onSuccess, FailBlock onFail)
        {
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "Incorrect PlayerId");
                return;
            }

            if (HasConnectionLink)
            {
                onSuccess.SafeInvoke(ConnectionLink);
                return;
            }

            connection.GetLink(playerId, link =>
            {
                ConnectionLink = link;
                onSuccess.SafeInvoke(link);
            }, onFail);
        }

        public void LoadRewardInformation(string playerId, Block<RewardInformation> onSuccess, FailBlock onFail)
        {
            if (playerId == null)
            {
                onFail.SafeInvoke(-1, "Incorrect PlayerId");
                return;
            }

            long lastUpdateTime = chronometer.ActualTime - rewardInfoLoadTime;
            if (rewardInformation == null || lastUpdateTime >= parameters.reloadPeriod)
            {
                connection.GetRewardInformation(playerId, information =>
                {
                    rewardInfoLoadTime = chronometer.ActualTime;
                    rewardInformation = information;
                    onSuccess.SafeInvoke(rewardInformation);
                }, onFail);
            }
            else
            {
                onSuccess.SafeInvoke(rewardInformation);
            }
        }

        public void TakeReward(string playerId, string installationId,
            Block<RewardTakeAnswer> onSuccess, FailBlock onFail)
        {
            if (playerId == null || installationId == null)
            {
                onFail.SafeInvoke(-1, "Incorrect incoming data");
                return;
            }
            connection.TakeReward(playerId, installationId, answer =>
            {
                if (rewardInformation != null) rewardInformation.Validate(answer);
                onSuccess.SafeInvoke(answer);
                analytics.AddInvitedFriendToUserProfile();
            }, onFail);
        }

        internal double CalculateReward(int rewardCount) => Math.Max(0, rewardCount * InviteReward);

        internal void OnGameConnected(IAnalyticsAdapter analytics) => this.analytics = analytics;

    }

}